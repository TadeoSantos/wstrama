package pe.com.mapfre.serviciosctr.infraestructura.datos.repositorios;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import pe.com.mapfre.serviciosctr.dominio.entidades.jpa.CabeceraLogOrquestador;
import pe.com.mapfre.serviciosctr.infraestructura.datos.entidades.CabeceraLogOrquestadorDTO;
import pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.ICabeceraLogOrquestadorRepository;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.UUIDUtil;

/**
 * La Class CabeceraLogOrquestadorDaoImpl.
 * <ul>
 * <li>Copyright 2020 MAPFRE -
 * MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BUILDSOFT
 * @version 1.0, Thu Jan 30 17:28:16 COT 2020
 * @since SCTR-CORE 1.0
 */
@Repository
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class CabeceraLogOrquestadorRepository extends  GenericRepository<String, CabeceraLogOrquestador> implements ICabeceraLogOrquestadorRepository  {

    /* (non-Javadoc)
     * @see pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.CabeceraLogOrquestadorDaoLocal#listarCabeceraLogOrquestador(pe.com.mapfre.serviciosctr.aplicacion.entidades.jpa.CabeceraLogOrquestador)
     */  
    @Override	 
    public List<CabeceraLogOrquestador> listarCabeceraLogOrquestador(CabeceraLogOrquestadorDTO cabeceraLogOrquestador) {
        Query query = generarQueryListaCabeceraLogOrquestador(cabeceraLogOrquestador, false);
        return query.getResultList();
    }   
   
    /**
     * Generar query lista CabeceraLogOrquestador.
     *
     * @param CabeceraLogOrquestadorDTO el cabeceraLogOrquestador
     * @param esContador el es contador
     * @return the query
     */
    private Query generarQueryListaCabeceraLogOrquestador(CabeceraLogOrquestadorDTO cabeceraLogOrquestador, boolean esContador) {
        Map<String, Object> parametros = new HashMap<>();
        StringBuilder jpaql = new StringBuilder();
        if (esContador) {
            jpaql.append(" select count(o.idLogCab) from CabeceraLogOrquestador o where 1=1 ");
        } else {
            jpaql.append(" select o from CabeceraLogOrquestador o where 1=1 ");           
        }
        if (!StringUtil.isNullOrEmpty(cabeceraLogOrquestador.getServicioInvocado())) {
			jpaql.append(" and upper(o.servicioInvocado) like :servicioInvocado ");
			parametros.put("servicioInvocado", "%" + cabeceraLogOrquestador.getServicioInvocado().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(cabeceraLogOrquestador.getOperacionInvocada())) {
			jpaql.append(" and upper(o.operacionInvocada) like :operacionInvocada ");
			parametros.put("operacionInvocada", "%" + cabeceraLogOrquestador.getOperacionInvocada().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(cabeceraLogOrquestador.getFechaInvocacion())) {
			jpaql.append(" and o.fechaInvocacion = :fechaInvocacion ");
			parametros.put("fechaInvocacion", cabeceraLogOrquestador.getFechaInvocacion());
		}
		if (!StringUtil.isNullOrEmpty(cabeceraLogOrquestador.getUsuarioInvocacion())) {
			jpaql.append(" and upper(o.usuarioInvocacion) like :usuarioInvocacion ");
			parametros.put("usuarioInvocacion", "%" + cabeceraLogOrquestador.getUsuarioInvocacion().toUpperCase() + "%");
		}
        if (!esContador) {
            jpaql.append(" ORDER BY 1 ");
        }
        return createQuery(jpaql.toString(), parametros);
    }

    /* (non-Javadoc)
     * @see pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.CabeceraLogOrquestadorDaoLocal#contarListar{entity.getClassName()}(pe.com.mapfre.serviciosctr.aplicacion.entidades.jpa.CabeceraLogOrquestadorDTO)
     */
	@Override
    public int contarListarCabeceraLogOrquestador(CabeceraLogOrquestadorDTO cabeceraLogOrquestador) {
        int resultado = 0;
        try {
            Query query = generarQueryListaCabeceraLogOrquestador(cabeceraLogOrquestador, true);
            resultado = ((Long) query.getSingleResult()).intValue();
        } catch (Exception e) {
            resultado = 0;
        }
        return resultado;
    }
    /* (non-Javadoc)
     * @see pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.CabeceraLogOrquestadorDaoLocal#generarIdCabeceraLogOrquestador()
     */
	 @Override
    public String generarIdCabeceraLogOrquestador() {
        return UUIDUtil.generarElementUUID();
    }
   
}