package pe.com.mapfre.serviciosctr.infraestructura.datos.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

 

/**
 * La Class CabeceraCotizacionDTO.
 * <ul>
 * <li>Copyright 2020 MAPFRE -
 * MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BUILDSOFT
 * @version 1.0, Thu Jan 30 19:20:46 COT 2020
 * @since SCTR-CORE 1.0
 */
public class CabeceraCotizacionDTO  implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id cotizacion. */
    private String idCotizacion;
   
    /** El cabecera log orquestador. */
    private String idLogCab;
   
    /** El nro sol o i m. */
    private String nroSolOIM;
   
    /** El nro cotiz tron. */
    private String nroCotizTron;
    
    private String nroCotizTronEnlace;
    
    private String nroPolizaEnlace;
   
    /** El anio mes. */
    private Long anioMes;
   
    /** El dia. */
    private Long dia;
   
    /** El estado. */
    private String estado;
   
    /** El fecha reg. */
    private Date fechaReg;
   
    /** El usuario reg. */
    private String usuarioReg;
   
    /** El cod cia. */
    private Long codCia;
   
    /** El cod ramo. */
    private Long codRamo;
    
    private String nroPoliza;
   
    /** El usuario modifica. */
    private String usuarioModifica;
   
    /** El fecha modifica. */
    private Date fechaModifica;
    
    /**
     * Instancia un nuevo cabecera cotizacionDTO.
     */
    public CabeceraCotizacionDTO() {
		super();
    }
   
   
    //get y set
    /**
     * Obtiene id cotizacion.
     *
     * @return id cotizacion
     */
     public String getIdCotizacion() {
        return this.idCotizacion;
    }
    /**
     * Establece el id cotizacion.
     *
     * @param idCotizacion el new id cotizacion
     */
    public void setIdCotizacion(String idCotizacion) {
        this.idCotizacion = idCotizacion;
    }
    /**
     * Obtiene cabecera log orquestador.
     *
     * @return cabecera log orquestador
     */
     public String getIdLogCab() {
        return this.idLogCab;
    }
    /**
     * Establece el cabecera log orquestador.
     *
     * @param idLogCab el new cabecera log orquestador
     */
    public void setIdLogCab(String idLogCab) {
        this.idLogCab = idLogCab;
    }
    /**
     * Obtiene nro sol o i m.
     *
     * @return nro sol o i m
     */
     public String getNroSolOIM() {
        return this.nroSolOIM;
    }
    /**
     * Establece el nro sol o i m.
     *
     * @param nroSolOIM el new nro sol o i m
     */
    public void setNroSolOIM(String nroSolOIM) {
        this.nroSolOIM = nroSolOIM;
    }
    /**
     * Obtiene nro cotiz tron.
     *
     * @return nro cotiz tron
     */
     public String getNroCotizTron() {
        return this.nroCotizTron;
    }
    /**
     * Establece el nro cotiz tron.
     *
     * @param nroCotizTron el new nro cotiz tron
     */
    public void setNroCotizTron(String nroCotizTron) {
        this.nroCotizTron = nroCotizTron;
    }
    /**
     * Obtiene anio mes.
     *
     * @return anio mes
     */
     public Long getAnioMes() {
        return this.anioMes;
    }
    /**
     * Establece el anio mes.
     *
     * @param anioMes el new anio mes
     */
    public void setAnioMes(Long anioMes) {
        this.anioMes = anioMes;
    }
    /**
     * Obtiene dia.
     *
     * @return dia
     */
     public Long getDia() {
        return this.dia;
    }
    /**
     * Establece el dia.
     *
     * @param dia el new dia
     */
    public void setDia(Long dia) {
        this.dia = dia;
    }
    /**
     * Obtiene estado.
     *
     * @return estado
     */
     public String getEstado() {
        return this.estado;
    }
    /**
     * Establece el estado.
     *
     * @param estado el new estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
    /**
     * Obtiene fecha reg.
     *
     * @return fecha reg
     */
     public Date getFechaReg() {
        return this.fechaReg;
    }
    /**
     * Establece el fecha reg.
     *
     * @param fechaReg el new fecha reg
     */
    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }
    /**
     * Obtiene usuario reg.
     *
     * @return usuario reg
     */
     public String getUsuarioReg() {
        return this.usuarioReg;
    }
    /**
     * Establece el usuario reg.
     *
     * @param usuarioReg el new usuario reg
     */
    public void setUsuarioReg(String usuarioReg) {
        this.usuarioReg = usuarioReg;
    }
    
    /**
     * Obtiene cod cia.
     *
     * @return cod cia
     */
     public Long getCodCia() {
        return this.codCia;
    }
    /**
     * Establece el cod cia.
     *
     * @param codCia el new cod cia
     */
    public void setCodCia(Long codCia) {
        this.codCia = codCia;
    }
    /**
     * Obtiene cod ramo.
     *
     * @return cod ramo
     */
     public Long getCodRamo() {
        return this.codRamo;
    }
    /**
     * Establece el cod ramo.
     *
     * @param codRamo el new cod ramo
     */
    public void setCodRamo(Long codRamo) {
        this.codRamo = codRamo;
    }
   
    /**
     * Obtiene usuario modifica.
     *
     * @return usuario modifica
     */
     public String getUsuarioModifica() {
        return this.usuarioModifica;
    }
    /**
     * Establece el usuario modifica.
     *
     * @param usuarioModifica el new usuario modifica
     */
    public void setUsuarioModifica(String usuarioModifica) {
        this.usuarioModifica = usuarioModifica;
    }
    /**
     * Obtiene fecha modifica.
     *
     * @return fecha modifica
     */
     public Date getFechaModifica() {
        return this.fechaModifica;
    }
    /**
     * Establece el fecha modifica.
     *
     * @param fechaModifica el new fecha modifica
     */
    public void setFechaModifica(Date fechaModifica) {
        this.fechaModifica = fechaModifica;
    }

	public String getNroPoliza() {
		return nroPoliza;
	}


	public void setNroPoliza(String nroPoliza) {
		this.nroPoliza = nroPoliza;
	}


	public String getNroCotizTronEnlace() {
		return nroCotizTronEnlace;
	}


	public void setNroCotizTronEnlace(String nroCotizTronSalud) {
		this.nroCotizTronEnlace = nroCotizTronSalud;
	}

	public String getNroPolizaEnlace() {
		return nroPolizaEnlace;
	}


	public void setNroPolizaEnlace(String nroPolizaEnlace) {
		this.nroPolizaEnlace = nroPolizaEnlace;
	}


	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idCotizacion == null) ? 0 : idCotizacion.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        CabeceraCotizacionDTO other = (CabeceraCotizacionDTO) obj;
        if (idCotizacion == null) {
            if (other.idCotizacion != null) {
                return false;
            }
        } else if (!idCotizacion.equals(other.idCotizacion)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "CabeceraCotizacionDTO [idCotizacion=" + idCotizacion + "]";
    }
   
}