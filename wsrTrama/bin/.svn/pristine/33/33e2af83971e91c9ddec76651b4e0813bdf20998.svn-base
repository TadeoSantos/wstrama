package pe.com.mapfre.serviciosctr.infraestructura.vertical.entidades;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <ul>
 * <li>Copyright 2014 MAPFRE. Todos los derechos reservados.</li>
 * </ul> 
 * 
 * La Class CorreoVO.
 *
 * @author Tecnocom Per&uacute; S.A.C.
 * @version 1.0 , 06/04/2015
 * @since MYTRON-MAPFRE 1.0
 */
public class CorreoVO implements Serializable {

	/** El asunto. */
	private String asunto;
	
	/** El contenido. */
	private String contenido;
	
    /** La Constante serialVersionUID. */
    private static final long serialVersionUID = 1L;
     
   
    /** La lista destinatarios. */
    private List<DetalleCorreoVO> detalleCorreo;
    
    /** La uuid. */
    private String UUID = null;
    
    /** La archivos adjuntos. */
    private File[] archivosAdjuntos;
    
   private Map<String,String> parametrosMap = new HashMap<String, String>();
   
   private Map<String,File> imageMap = new HashMap<String, File>();

    /**
     * Instantiates a new correo vo.
     *
     * @param asunto the asunto
     * @param contenido the contenido
     * @param detalleCorreo the detalle correo
     */
    public CorreoVO(String asunto, String contenido,
			List<DetalleCorreoVO> detalleCorreo) {
		super();
		this.asunto = asunto;
		this.contenido = contenido;
		this.detalleCorreo = detalleCorreo;
	}

	/**
     * Instancia un nuevo mensaje vo.
     */
    public CorreoVO() {

    }

    //get y set
    
    /**
     * Obtiene uuid.
     *
     * @return uuid
     */
    public String getUUID() {
		return UUID;
	}
    
    /**
     * Modificar uuid.
     *
     * @param UUID el uuid
     */
    public void modificarUUID(String UUID ) {
		this.UUID = UUID;
	}
    
    /**
	 * Obtiene contenido.
	 *
	 * @return contenido
	 */
	 public String getContenido() {
		return this.contenido;
	}
	
	 

	/**
	 * Establece el contenido.
	 *
	 * @param contenido el new contenido
	 */
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}
	
	/**
	 * Obtiene asunto.
	 *
	 * @return asunto
	 */
	 public String getAsunto() {
		return this.asunto;
	}
	/**
	 * Establece el asunto.
	 *
	 * @param asunto el new asunto
	 */
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}
    
    /**
     * Obtiene detalle correo.
     *
     * @return detalle correo
     */
    public List<DetalleCorreoVO> getDetalleCorreo() {
		return detalleCorreo;
	}


	/**
	 * Establece el detalle correo.
	 *
	 * @param detalleCorreo el new detalle correo
	 */
	public void setDetalleCorreo(List<DetalleCorreoVO> detalleCorreo) {
		this.detalleCorreo = detalleCorreo;
	}

	/**
	 * Obtiene archivos adjuntos.
	 *
	 * @return archivos adjuntos
	 */
	public File[] getArchivosAdjuntos() {
		return archivosAdjuntos;
	}

	/**
	 * Establece el archivos adjuntos.
	 *
	 * @param archivosAdjuntos el new archivos adjuntos
	 */
	public void setArchivosAdjuntos(File[] archivosAdjuntos) {
		this.archivosAdjuntos = archivosAdjuntos;
	}

	public Map<String, String> getParametrosMap() {
		return parametrosMap;
	}

	public void setParametrosMap(Map<String, String> parametrosMap) {
		this.parametrosMap = parametrosMap;
	}

	public Map<String, File> getImageMap() {
		return imageMap;
	}

	public void setImageMap(Map<String, File> imageMap) {
		this.imageMap = imageMap;
	}
	

}
