package pe.com.mapfre.serviciosctr.infraestructura.datos.repositorios;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import oracle.jdbc.OracleTypes;
import pe.com.mapfre.serviciosctr.dominio.entidades.jpa.CabeceraCotizacion;
import pe.com.mapfre.serviciosctr.infraestructura.datos.entidades.CabeceraCotizacionDTO;
import pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.ICabeceraCotizacionRepository;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.entidades.ScriptSqlResulJDBCVO;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.GenericJDBC;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.StringUtil;

/**
 * La Class CabeceraCotizacionDaoImpl.
 * <ul>
 * <li>Copyright 2020 MAPFRE - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BUILDSOFT
 * @version 1.0, Thu Jan 30 17:28:16 COT 2020
 * @since SCTR-CORE 1.0
 */
@Repository
@Transactional(propagation=Propagation.REQUIRES_NEW)
public class CabeceraCotizacionRepository extends GenericRepository<String, CabeceraCotizacion>
		implements ICabeceraCotizacionRepository {

	@Autowired
	private GenericJDBC genericJDBC;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.
	 * CabeceraCotizacionDaoLocal#listarCabeceraCotizacion(pe.com.mapfre.
	 * serviciosctr.aplicacion.entidades.jpa.CabeceraCotizacion)
	 */
	@Override
	public List<CabeceraCotizacion> listarCabeceraCotizacion(CabeceraCotizacionDTO cabeceraCotizacion) {
		Query query = generarQueryListaCabeceraCotizacion(cabeceraCotizacion, false);
		return query.getResultList();
	}

	/**
	 * Generar query lista CabeceraCotizacion.
	 *
	 * @param CabeceraCotizacionDTO
	 *            el cabeceraCotizacion
	 * @param esContador
	 *            el es contador
	 * @return the query
	 */
	private Query generarQueryListaCabeceraCotizacion(CabeceraCotizacionDTO cabeceraCotizacion, boolean esContador) {
		Map<String, Object> parametros = new HashMap<>();
		StringBuilder jpaql = new StringBuilder();
		if (esContador) {
			jpaql.append(" select count(o.idCotizacion) from CabeceraCotizacion o where 1=1 ");
		} else {
			jpaql.append(" select o from CabeceraCotizacion o where 1=1 ");
		}
		if (!StringUtil.isNullOrEmpty(cabeceraCotizacion.getIdCotizacion())) {
			jpaql.append(" and o.idCotizacion =:idCotizacion ");
			parametros.put("idCotizacion", cabeceraCotizacion.getIdCotizacion());
		}
		if (!StringUtil.isNullOrEmpty(cabeceraCotizacion.getNroSolOIM())) {
			jpaql.append(" and o.nroSolOIM =:nroSolOIM ");
			parametros.put("nroSolOIM",cabeceraCotizacion.getNroSolOIM() );
		}
		if (!StringUtil.isNullOrEmpty(cabeceraCotizacion.getNroCotizTron())) {
			jpaql.append(" and upper(o.nroCotizTron) = :nroCotizTron ");
			parametros.put("nroCotizTron",  cabeceraCotizacion.getNroCotizTron().toUpperCase() );
		}
		if (!StringUtil.isNullOrEmpty(cabeceraCotizacion.getEstado())) {
			jpaql.append(" and upper(o.estado) like :estado ");
			parametros.put("estado", "%" + cabeceraCotizacion.getEstado().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(cabeceraCotizacion.getFechaReg())) {
			jpaql.append(" and o.fechaReg = :fechaReg ");
			parametros.put("fechaReg", cabeceraCotizacion.getFechaReg());
		}
		if (!StringUtil.isNullOrEmpty(cabeceraCotizacion.getUsuarioReg())) {
			jpaql.append(" and upper(o.usuarioReg) like :usuarioReg ");
			parametros.put("usuarioReg", "%" + cabeceraCotizacion.getUsuarioReg().toUpperCase() + "%");
		}
		if (!esContador) {
			jpaql.append(" ORDER BY 1 ");
		}
		return createQuery(jpaql.toString(), parametros);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.
	 * CabeceraCotizacionDaoLocal#contarListar{entity.getClassName()}(pe.com.mapfre.
	 * serviciosctr.aplicacion.entidades.jpa.CabeceraCotizacionDTO)
	 */
	@Override
	public int contarListarCabeceraCotizacion(CabeceraCotizacionDTO cabeceraCotizacion) {
		int resultado = 0;
		try {
			Query query = generarQueryListaCabeceraCotizacion(cabeceraCotizacion, true);
			resultado = ((Long) query.getSingleResult()).intValue();
		} catch (Exception e) {
			resultado = 0;
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.
	 * CabeceraCotizacionDaoLocal#generarIdCabeceraCotizacion()
	 */
	@Override
	public String generarIdCabeceraCotizacion(Date fechaActual,String codigoRamo) {
		String resultado = "1";
		try {
			resultado = FechaUtil.obtenerFechaFormatoPersonalizado(fechaActual, "yyyyMM") + FechaUtil.obtenerFechaFormatoPersonalizado(fechaActual, "dd") + "00001" ;
			
			List<Object> parametroInType = new ArrayList<>();
			if (StringUtil.isNotNullOrBlank(codigoRamo)) {
				parametroInType.add(codigoRamo);
			} else {
				parametroInType.add("702");
			}
			
			List<Integer> parametroOutType = new ArrayList<>();
			parametroOutType.add(OracleTypes.VARCHAR);
			parametroOutType.add(OracleTypes.VARCHAR);
			parametroOutType.add(OracleTypes.VARCHAR);
			
			Map<String, Object> parametrosType = new HashMap<>();
			
			ScriptSqlResulJDBCVO sqEjecutado = genericJDBC.executeQuerySP("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_SECUENCIADOR", null, parametroOutType, parametroInType, parametrosType);
			if (!sqEjecutado.isTieneError() && !sqEjecutado.getListaData().isEmpty()) {
					if("0".equals(sqEjecutado.getListaData().get(1).get("resultado2") + "")) {
						resultado = sqEjecutado.getListaData().get(0).get("resultado1") + "";
					}
			}
		} catch (Exception e) {
			logger.error("generarIdCabeceraCotizacion.error", e);
		}
		
		return resultado;
	}

}