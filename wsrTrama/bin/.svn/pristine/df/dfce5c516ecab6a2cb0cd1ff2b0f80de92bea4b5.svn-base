package pe.com.mapfre.serviciosctr.infraestructura.vertical.servicios;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import pe.com.mapfre.serviciosctr.infraestructura.vertical.entidades.CorreoVO;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.entidades.DetalleCorreoVO;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.entidades.EMail;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.entidades.SMTPAuthenticator;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.interfaces.IEmailService;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.type.TipoDetalleCorreoType;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.StringUtil;

@Service
public class EMailService implements IEmailService{
	
	@Autowired
    JavaMailSender mailSender;
 
 	final Logger log = LoggerFactory.getLogger(this.getClass());
	@Override
    public void enviarEmail(EMail mail) {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        try {
            MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
            mimeMessageHelper.setSubject(mail.getMailSubject());
            mimeMessageHelper.setFrom(mail.getMailFrom());
            mimeMessageHelper.setTo(mail.getMailTo());
            mimeMessageHelper.setText(mail.getMailContent(),mail.isHtml());
            if(mail.getAttachments() != null) {
            	for (Object attachment: mail.getAttachments()) {
                    File file = ((ClassPathResource)attachment).getFile();
                    mimeMessageHelper.addAttachment(file.getName(), file);
                }
            }
            mailSender.send(mimeMessageHelper.getMimeMessage());
        } catch (MessagingException e) {
        	log.error("MessagingException: ",e);
        } catch (IOException e) {
        	log.error("IOException: ",e);
		} catch (Exception e) {
        	log.error("Exception: ",e);
		}
    }
	
	@Override
	public String enviarCorreoElectronico(final CorreoVO correo, Map<String, String> paramsCorreo) {
		String resultado = "";
		// verificar que los datos sean correctos
		if (correo.getDetalleCorreo().size() > 0) {
			if (!correo.getAsunto().isEmpty()) {
				if (!correo.getContenido().isEmpty()) {
					try {
						String pwrMail = null;
						if (!StringUtil.isNullOrEmpty(paramsCorreo.get("mail.username"))) {
							pwrMail = paramsCorreo.get("mail.username");
						}
						String pwrName = null;
						if (!StringUtil.isNullOrEmpty(paramsCorreo.get("mail.from"))) {
							pwrName = paramsCorreo.get("mail.from");
						}else {
							pwrName = paramsCorreo.get("mail.username");
						}
						
						String pwrPass = null;
						if (!StringUtil.isNullOrEmpty(paramsCorreo.get("mail.password"))) {
							pwrPass = paramsCorreo.get("mail.password");
						}
						Properties props = new Properties();
						if (!StringUtil.isNullOrEmpty(paramsCorreo.get("mail.host"))) {
							props.setProperty("mail.smtp.host",paramsCorreo.get("mail.host"));
						}
						if (!StringUtil.isNullOrEmpty(paramsCorreo.get("mail.port"))) {
							props.setProperty("mail.smtp.port",paramsCorreo.get("mail.port"));
						}
						if (!StringUtil.isNullOrEmpty(pwrMail)) {
								props.setProperty("mail.smtp.user", pwrMail);
						}
						if (!StringUtil.isNullOrEmpty(paramsCorreo.get("mail.smtp.auth"))) {
							props.setProperty("mail.smtp.auth",paramsCorreo.get("mail.smtp.auth"));
						}
						if (!StringUtil.isNullOrEmpty(paramsCorreo.get("mail.smtp.starttls.enable"))) {
							props.setProperty("mail.smtp.starttls.enable", paramsCorreo.get("mail.smtp.starttls.enable"));
						}
						SMTPAuthenticator authenticator = null;
						if (!StringUtil.isNullOrEmpty(pwrMail) && !StringUtil.isNullOrEmpty(pwrPass)) {
							authenticator = new SMTPAuthenticator(pwrMail, pwrPass);
						}
						Session session = null;
						if (authenticator != null) {
							session =  Session.getInstance(props,authenticator);
						} else {
							session =  Session.getDefaultInstance(props,null);
						}
						MimeMessage messageToClient = new MimeMessage(session);
						messageToClient.setFrom(new InternetAddress(pwrMail, pwrName));

						Multipart multipart = new MimeMultipart();
						MimeBodyPart cuerpoCorreo = new MimeBodyPart();
						
						for (DetalleCorreoVO detCorreo : correo.getDetalleCorreo()) {
							String clientMail = detCorreo.getEmail();
							if (TipoDetalleCorreoType.DESTINATARIO.getKey().equals(detCorreo.getTipo())) {
								messageToClient.addRecipient(Message.RecipientType.TO, new InternetAddress(clientMail,clientMail));
							}
							if (TipoDetalleCorreoType.CON_COPIA.getKey().equals(detCorreo.getTipo())) {
								messageToClient.addRecipient(Message.RecipientType.CC, new InternetAddress(clientMail,clientMail));
							}
							if (TipoDetalleCorreoType.CON_COPIA_OCULTA.getKey().equals(detCorreo.getTipo())) {
								messageToClient.addRecipient(Message.RecipientType.BCC, new InternetAddress(clientMail,clientMail));
							}
						}
						messageToClient.setSubject(correo.getAsunto());
						cuerpoCorreo.setContent(correo.getContenido(),"text/html");
						multipart.addBodyPart(cuerpoCorreo);
						
						File[] archivosAdjuntos = correo.getArchivosAdjuntos();

						if (archivosAdjuntos != null && archivosAdjuntos.length > 0) {
							int index = 1;
							for (File archivo : archivosAdjuntos) {
								MimeBodyPart adjuntoCorreo = new MimeBodyPart();
								adjuntoCorreo.attachFile(archivo);
								String key = "reName" + index;
								if (correo.getParametrosMap().containsKey(key)) {
									adjuntoCorreo.setFileName(correo.getParametrosMap().get(key));	
								} else {
									adjuntoCorreo.setFileName(archivo.getName());	
								}
								multipart.addBodyPart(adjuntoCorreo);
								index++;
							}
						}
						//Imagenes en el correo
						for (Entry<String, File> temp : correo.getImageMap().entrySet() ) {
							MimeBodyPart imagePart = new MimeBodyPart();
			                imagePart.setHeader("Content-ID", "<" + temp.getKey() + ">");
			                imagePart.setDisposition(MimeBodyPart.INLINE);
			                try {
			                    imagePart.attachFile(temp.getValue());
			                } catch (IOException ex) {
			                    ex.printStackTrace();
			                }
			                multipart.addBodyPart(imagePart);
						}
						messageToClient.setContent(multipart);
						messageToClient.setSentDate(new Date());
						if (authenticator != null) {
							Transport t = session.getTransport("smtp");
							t.connect(pwrMail, pwrPass);
							t.sendMessage(messageToClient,messageToClient.getAllRecipients());
							t.close();
						} else {
							log.error("Error: authenticator is null");
						}
						log.info("Envio correo correctamente");
					} catch (MessagingException ex) {
						log.error("Error MensajeriaServiceImpl.enviarCorreoElectronico(final CorreoVO correo) no se pudo enviar el correo " + correo.getUUID() + " MessagingException ex : " + ex.getMessage());
						resultado = ex.getMessage();
					} catch (Exception e) {
						log.error("Error MensajeriaServiceImpl.enviarCorreoElectronico(final CorreoVO correo) no se pudo enviar el correo " + correo.getUUID() + " Exception e : " + e.getMessage());
						resultado = e.getMessage();
					}
				} else {
					//registrar fallo
					log.error("Error MensajeriaServiceImpl.enviarCorreoElectronico(final CorreoVO correo) no tiene contenido");
				}
			} else {
				//no tiene asunto
				log.error("Error MensajeriaServiceImpl.enviarCorreoElectronico(final CorreoVO correo) no tiene asunto");
			}
		} else {
			log.error("Error MensajeriaServiceImpl.enviarCorreoElectronico(final CorreoVO correo) no tiene destinatario");
		}
     return resultado;
	}
	
	
}
