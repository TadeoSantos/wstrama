package pe.com.mapfre.serviciosctr.dominio.entidades.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class DetalleLogOrquestador.
 * <ul>
 * <li>Copyright 2020 MAPFRE -
 * MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BUILDSOFT
 * @version 1.0, Thu Jan 30 17:28:16 COT 2020
 * @since SCTR-CORE 1.0
 */
@Entity
@Table(name = "SCTR_SO_DET_LOG_ORQ", schema = ConfiguracionEntityManagerUtil.ESQUEMA_TRON2000)
public class DetalleLogOrquestador implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id log det. */
    @Id
    @Column(name = "ID_LOG_DET" , length = 32)
    private String idLogDet;
   
    /** El cabecera log orquestador. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_LOG_CAB", referencedColumnName = "ID_LOG_CAB")
    private CabeceraLogOrquestador cabeceraLogOrquestador;
   
    /** El trama enviada. */
   // @Column(name = "TRAMA_ENVIADA")
   // private String tramaEnviada;
   
    /** El fecha reg. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FECHA_REG")
    private Date fechaReg;
   
    /** El tipo. */
    @Column(name = "TIPO" , length = 1)
    private String tipo;
   
    /**
     * Instancia un nuevo detalle log orquestador.
     */
    public DetalleLogOrquestador() {
		super();
    }
   
    /**
     * Instancia un nuevo detalle log orquestador.
     *
     * @param idLogDet el id log det
     * @param cabeceraLogOrquestador el cabecera log orquestador
     * @param tramaEnviada el trama enviada
     * @param fechaReg el fecha reg
     * @param tipo el tipo
     */
    public DetalleLogOrquestador(String idLogDet, CabeceraLogOrquestador cabeceraLogOrquestador, Date fechaReg, String tipo ) {
        super();
        this.idLogDet = idLogDet;
        this.cabeceraLogOrquestador = cabeceraLogOrquestador;
        this.fechaReg = fechaReg;
        this.tipo = tipo;
    }
   
    //get y set
    /**
     * Obtiene id log det.
     *
     * @return id log det
     */
     public String getIdLogDet() {
        return this.idLogDet;
    }
    /**
     * Establece el id log det.
     *
     * @param IdLogDet el new id log det
     */
    public void setIdLogDet(String IdLogDet) {
        this.idLogDet = IdLogDet;
    }
    /**
     * Obtiene cabecera log orquestador.
     *
     * @return cabecera log orquestador
     */
     public CabeceraLogOrquestador getCabeceraLogOrquestador() {
        return this.cabeceraLogOrquestador;
    }
    /**
     * Establece el cabecera log orquestador.
     *
     * @param cabeceraLogOrquestador el new cabecera log orquestador
     */
    public void setCabeceraLogOrquestador(CabeceraLogOrquestador cabeceraLogOrquestador) {
        this.cabeceraLogOrquestador = cabeceraLogOrquestador;
    }
   
    /**
     * Obtiene fecha reg.
     *
     * @return fecha reg
     */
     public Date getFechaReg() {
        return this.fechaReg;
    }
    /**
     * Establece el fecha reg.
     *
     * @param fechaReg el new fecha reg
     */
    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }
    /**
     * Obtiene tipo.
     *
     * @return tipo
     */
     public String getTipo() {
        return this.tipo;
    }
    /**
     * Establece el tipo.
     *
     * @param tipo el new tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idLogDet == null) ? 0 : idLogDet.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DetalleLogOrquestador other = (DetalleLogOrquestador) obj;
        if (idLogDet == null) {
            if (other.idLogDet != null) {
                return false;
            }
        } else if (!idLogDet.equals(other.idLogDet)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DetalleLogOrquestador [IdLogDet=" + idLogDet + "]";
    }
   
}