package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
@Local
public interface CanalCombosDaoLocal  extends IGenericRepository<String,ListaItemsVO> {
	
	ListaItemsVO listar(ListaItemsVO filtro);

	int contar(ListaItemsVO filtro);
	
}