package pe.com.mapfre.serviciotrama._presentacion.api.controladores;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import pe.com.mapfre.serviciotrama._presentacion.api.entity.vo.RespuestaWSVO;
import pe.com.mapfre.serviciotrama.application.service.IConfiguracionAppService;
import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.SelectItemVO;

/**
 * La Class ListaItemsRestController.
 * <ul>
 * <li>Copyright 2021 MAPFRE - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft
 * @version 1.0, Mon Sep 06 17:54:31 COT 2021
 * @since MYTRON 1.0
 */
@Stateless
@Api(value = "/grupoItems", tags = { "Configuracion items" })
@SwaggerDefinition(tags = { @Tag(name = "Configuracion items", description = "Servicios configuracion de items") })
@Path("/grupoItems")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ListaItemsController extends GenericServiceRestImpl {

	private final Logger logger = Logger.getLogger(this.getClass());

	@EJB
	private IConfiguracionAppService servicioApp;
	
	@GET
	@Path("/{codigoId}/items")
	public Response paginadorByCodigo(@Context HttpHeaders httpHeaders, @Context UriInfo info) {
		RespuestaWSVO<ListaItemsVO> resultado = new RespuestaWSVO<>();
		try {
			ListaItemsVO filtro = to(info);
			if (filtro.getOffset() > 0) {
				if (filtro.getStartRow() == 0)
					resultado.setContador(servicioApp.contarItems(filtro));

				if (resultado.isData() || filtro.getStartRow() > 0) {
					resultado.setListaResultado(servicioApp.listarItems(filtro));
				}
			} else {
				resultado.setListaResultado(servicioApp.listarItems(filtro));
			}
		} catch (Exception e) {
			logger.error("Error ", e);
			parsearResultadoError(e, resultado);
		}
		return respuesta(resultado, null);
	}

	@GET
	@Path("/paginador")
	public Response paginador(@Context HttpHeaders httpHeaders, @Context UriInfo info) {
		RespuestaWSVO<ListaItemsVO> resultado = new RespuestaWSVO<>();
		try {
			ListaItemsVO filtro = to(info);
			if (filtro.getOffset() > 0) {// lupas pagiandos
				if (filtro.getStartRow() == 0)
					resultado.setContador(servicioApp.contarItems(filtro));

				if (resultado.isData() || filtro.getStartRow() > 0) {
					resultado.setListaResultado(servicioApp.listarItems(filtro));
				}
			} else {// solo combos all data
				resultado.setListaResultado(servicioApp.listarItems(filtro));
				resultado.setContador(resultado.getListaResultado().size());
			}
		} catch (Exception e) {
			logger.error("Error ", e);
			parsearResultadoError(e, resultado);
		}
		return respuesta(resultado, null);
	}

	private ListaItemsVO to(@Context UriInfo info) {
		ListaItemsVO resultado = toRest(info, ListaItemsVO.class);
		resultado.setFiltro(toRest(info, SelectItemVO.class));
		if (info.getQueryParameters().containsKey("codigo")) {
			resultado.setListaCodigo(info.getQueryParameters().get("codigo"));
			resultado.setCodigo(info.getQueryParameters().getFirst("codigo"));
		}else if (info.getQueryParameters().containsKey("grupo")) {
			resultado.setListaGrupo(info.getQueryParameters().get("grupo"));
		}
		return resultado;
	}
}