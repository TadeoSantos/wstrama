package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ConfiguracionTramaDetalle.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_CONFIG_TRAMA_DET", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ConfiguracionTramaDetalle implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id configurador trama detalle. */
    @Id
    @Column(name = "N_ID_CONFIG_TRAMA_DET" , length = 18)
    private Long idConfiguradorTramaDetalle;
   
    /** El configuracion trama. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CONFIG_TRAMA", referencedColumnName = "N_ID_CONFIG_TRAMA")
    private ConfiguracionTrama configuracionTrama;
   
    /** El nombre campo. */
    @Column(name = "C_NOMBRE_CAMPO" , length = 150)
    private String nombreCampo;
   
    /** El descripcion campo. */
    @Column(name = "C_DESCRIPCION_CAMPO" , length = 200)
    private String descripcionCampo;    
    
    /** El obligatorio. */
    @Column(name = "C_OBLIGATORIO" , length = 1)
    private Character obligatorio;
   
    /** El tipo campo. */
    @Column(name = "C_TIPO_CAMPO" , length = 20)
    private String tipoCampo;
   
    /** El posicion campo inicial. */
    @Column(name = "N_POSICION_CAMPO_INICIAL" , length = 20)
    private Long posicionCampoInicial;
   
    /** El posicion campo final. */
    @Column(name = "N_POSICION_CAMPO_FINAL" , length = 20)
    private Long posicionCampoFinal;
   
    /** El valor defecto campo. */
    @Column(name = "C_VALOR_DEFEC_CAMP" , length = 150)
    private String valorDefectoCampo;
   
    /** El formato campo. */
    @Column(name = "C_FORMATO_CAMPO" , length = 150)
    private String formatoCampo;
   
    /** El nombe campo tabla. */
    @Column(name = "C_CAMPO_ASOC_TABLA" , length = 150)
    private String nombeCampoTabla;
   
    /** El orden. */
    @Column(name = "N_ORDEN" , length = 18)
    private Long orden;
   
    /** El fila data. */
    @Column(name = "N_FILA_DATA" , length = 18)
    private Long filaData;
   
    /** El flag campo agrupador. */
    @Column(name = "C_FLAG_CAMP_AGRUPADOR" , length = 1)
    private Character flagCampoAgrupador;
   
    /** El flag campo no leido trama. */
    @Column(name = "C_FLAG_CAMP_NOLEIDOTRAMA" , length = 1)
    private Character flagCampoNoLeidoTrama;
   
    /** El flag campo negocio trama. */
    @Column(name = "C_FLAG_CAMP_NEGOCIO" , length = 1)
    private Character flagCampoNegocio;
    
    /** El longitud. */
    @Column(name = "N_LONGITUD" , length = 18)
    private Long longitud;
   
    /** El campo fijo. */
    @Column(name = "N_CAMPO_FIJO" , length = 18)
    private Long campoFijo;
   
    /** El campo asociado. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CAMPO_ASOCIADO_MATCH_INI", referencedColumnName = "N_ID_CONFIG_TRAMA_DET")
    private ConfiguracionTramaDetalle campoAsociadoMatchInicio;
    
    /** El campo asociado. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CAMPO_ASOCIADO_MATCH_FIN", referencedColumnName = "N_ID_CONFIG_TRAMA_DET")
    private ConfiguracionTramaDetalle campoAsociadoMatchFin;
    
    /** El campo asociado. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CAMPO_ASOCIADO", referencedColumnName = "N_ID_CONFIG_TRAMA_DET")
    private ConfiguracionTramaDetalle campoAsociado;
    
    /** The es persistente. */
    @Column(name = "C_ESPERSISTENCE" , length = 1)
    private Character  esPersistente;
   
    /** El tipo homologacion. */
    @Column(name = "N_TIPO_HOMOLOGACION" ,  length = 18)
    private Long tipoHomologacion;
   
    /** El campo dependiente. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CAMPO_DEPENDIENTE", referencedColumnName = "N_ID_CONFIG_TRAMA_DET")
    private ConfiguracionTramaDetalle campoDependiente;
   
    /** El campo asociado configuracion trama detalle list. */
    @OneToMany(mappedBy = "campoAsociado", fetch = FetchType.LAZY)
    private List<ConfiguracionTramaDetalle> campoAsociadoConfiguracionTramaDetalleList = new ArrayList<ConfiguracionTramaDetalle>();
    
    /** La regla negocio. */
    @Column(name = "C_REGLA_NEG" )
    private String reglaNegocio;
    
    /**
     * Instancia un nuevo configuracion trama detalle.
     */
    public ConfiguracionTramaDetalle() {
    }
   
   
    /**
     * Instancia un nuevo configuracion trama detalle.
     *
     * @param idConfiguradorTramaDetalle el id configurador trama detalle
     * @param configuracionTrama el configuracion trama
     * @param nombreCampo el nombre campo
     * @param descripcionCampo el descripcion campo
     * @param obligatorio el obligatorio
     * @param tipoCampo el tipo campo
     * @param posicionCampoInicial el posicion campo inicial
     * @param posicionCampoFinal el posicion campo final
     * @param valorDefectoCampo el valor defecto campo
     * @param formatoCampo el formato campo
     * @param nombeCampoTabla el nombe campo tabla
     * @param orden el orden
     * @param filaData el fila data
     * @param flagCampoAgrupador el flag campo agrupador
     * @param flagCampoNoLeidoTrama el flag campo no leido trama
     * @param longitud el longitud
     * @param campoFijo el campo fijo
     * @param campoAsociado el campo asociado
     * @param tipoHomologacion the tipo homologacion
     * @param campoDependiente the campo dependiene
     */
    public ConfiguracionTramaDetalle(Long idConfiguradorTramaDetalle, ConfiguracionTrama configuracionTrama,String nombreCampo,String descripcionCampo, Character obligatorio, String tipoCampo, Long posicionCampoInicial, Long posicionCampoFinal, String valorDefectoCampo, String formatoCampo, String nombeCampoTabla, Long orden, Long filaData, Character flagCampoAgrupador, Character flagCampoNoLeidoTrama, Long longitud, Long campoFijo,ConfiguracionTramaDetalle campoAsociado,Long tipoHomologacion,ConfiguracionTramaDetalle campoDependiente) {
        super();
        this.idConfiguradorTramaDetalle = idConfiguradorTramaDetalle;
        this.configuracionTrama = configuracionTrama;
        this.nombreCampo = nombreCampo;
        this.descripcionCampo = descripcionCampo;
        this.obligatorio = obligatorio;
        this.tipoCampo = tipoCampo;
        this.posicionCampoInicial = posicionCampoInicial;
        this.posicionCampoFinal = posicionCampoFinal;
        this.valorDefectoCampo = valorDefectoCampo;
        this.formatoCampo = formatoCampo;
        this.nombeCampoTabla = nombeCampoTabla;
        this.orden = orden;
        this.filaData = filaData;
        this.flagCampoAgrupador = flagCampoAgrupador;
        this.flagCampoNoLeidoTrama = flagCampoNoLeidoTrama;
        this.longitud = longitud;
        this.campoFijo = campoFijo;
        this.campoAsociado = campoAsociado;
        this.tipoHomologacion = tipoHomologacion;
        this.campoDependiente = campoDependiente;
    }
  
	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idConfiguradorTramaDetalle == null) ? 0 : idConfiguradorTramaDetalle.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConfiguracionTramaDetalle other = (ConfiguracionTramaDetalle) obj;
        if (idConfiguradorTramaDetalle == null) {
            if (other.idConfiguradorTramaDetalle != null) {
                return false;
            }
        } else if (!idConfiguradorTramaDetalle.equals(other.idConfiguradorTramaDetalle)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConfiguracionTramaDetalle [idConfiguradorTramaDetalle=" + idConfiguradorTramaDetalle + "]";
    }
   
}