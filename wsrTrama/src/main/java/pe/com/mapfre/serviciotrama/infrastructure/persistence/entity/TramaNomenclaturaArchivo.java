package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class TramaNomenclaturaArchivo.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */	
@Getter
@Setter
@Entity
@Table(name = "SGSM_TRA_NOMEAR", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class TramaNomenclaturaArchivo implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id trama nomenclatura archivo. */
    @Id
    @Column(name = "N_ID_TRA_NOMEN_ARCHI" , length = 18)
    private Long idTramaNomenclaturaArchivo;
   
    /** El canal. */
    @Column(name = "C_CANAL" , length = 50)
    private String canal;
   
    /** El producto. */
    @Column(name = "C_PRODUCTO" , length = 50)
    private String producto;
   
    /** El tipo archivo. */
    @Column(name = "C_TIPO_ARCHIVO" , length = 20)
    private String tipoArchivo;
   
    /** El tipo trama. */
    @Column(name = "N_TIPO_TRAMA" , length = 18)
    private Long tipoTrama;
   
    /** El nombre. */
    @Column(name = "C_NOMBRE" , length = 50)
    private String nombre;
    
    /** El nombre. */
    @Column(name = "C_ACRONIMO_TIPO_TRAMA" , length = 4)
    private String acronimoTipoTrama;
   
    /** El trama nomenclatura archivo configuracion trama list. */
    @OneToMany(mappedBy = "tramaNomenclaturaArchivo", fetch = FetchType.LAZY)
    private List<ConfiguracionTrama> tramaNomenclaturaArchivoConfiguracionTramaList = new ArrayList<ConfiguracionTrama>();
    
    /** El trama nomenclatura archivo trama nomenclatura archivo detalle list. */
    @OneToMany(mappedBy = "tramaNomenclaturaArchivo", fetch = FetchType.LAZY)
    private List<TramaNomenclaturaArchivoDetalle> tramaNomenclaturaArchivoTramaNomenclaturaArchivoDetalleList = new ArrayList<TramaNomenclaturaArchivoDetalle>();
    
    /**
     * Instancia un nuevo trama nomenclatura archivo.
     */
    public TramaNomenclaturaArchivo() {
    }
   
   
    /**
     * Instancia un nuevo trama nomenclatura archivo.
     *
     * @param idTramaNomenclaturaArchivo el id trama nomenclatura archivo
     * @param canal el canal
     * @param producto el producto
     * @param tipoArchivo el tipo archivo
     * @param tipoTrama el tipo trama
     * @param nombre el nombre
     */
    public TramaNomenclaturaArchivo(Long idTramaNomenclaturaArchivo, String canal, String producto, String tipoArchivo, Long tipoTrama, String nombre ) {
        super();
        this.idTramaNomenclaturaArchivo = idTramaNomenclaturaArchivo;
        this.canal = canal;
        this.producto = producto;
        this.tipoArchivo = tipoArchivo;
        this.tipoTrama = tipoTrama;
        this.nombre = nombre;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idTramaNomenclaturaArchivo == null) ? 0 : idTramaNomenclaturaArchivo.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TramaNomenclaturaArchivo other = (TramaNomenclaturaArchivo) obj;
        if (idTramaNomenclaturaArchivo == null) {
            if (other.idTramaNomenclaturaArchivo != null) {
                return false;
            }
        } else if (!idTramaNomenclaturaArchivo.equals(other.idTramaNomenclaturaArchivo)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TramaNomenclaturaArchivo [idTramaNomenclaturaArchivo=" + idTramaNomenclaturaArchivo + "]";
    }
   
}