package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class PullConexion.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_ORQ_PUL_CONEX", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class PullConexion implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id pull conexion. */
    @Id
    @Column(name = "N_ID_PULL_CONEX" , length = 18)
    private Long idPullConexion;
   
    /** El nombre data source. */
    @Column(name = "C_PROC_SQL" , length = 150)
    private String nombreDataSource;
   
    /** El driver. */
    @Column(name = "C_DESC_DRI_CONEX" , length = 150)
    private String driver;
   
    /** El url. */
    @Column(name = "C_URL_CONEX" , length = 300)
    private String url;
   
    /** El usuario. */
    @Column(name = "C_USR_CONEX" , length = 50)
    private String usuario;
   
    /** El clave. */
    @Column(name = "C_PASS_CONEX" , length = 50)
    private String clave;
    
    /** La estado. */
    @Column(name = "C_ESTADO" , length = 1)
    private String estado;
    
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
   
    /** El pull conexion actividad flujo list. */
    @OneToMany(mappedBy = "pullConexion", fetch = FetchType.LAZY)
    private List<ActividadFlujo> pullConexionActividadFlujoList = new ArrayList<ActividadFlujo>();
    
    /**
     * Instancia un nuevo pull conexion.
     */
    public PullConexion() {
    }
   
   
    /**
     * Instancia un nuevo pull conexion.
     *
     * @param idPullConexion el id pull conexion
     * @param nombreDataSource el nombre data source
     * @param driver el driver
     * @param url el url
     * @param usuario el usuario
     * @param clave el clave
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public PullConexion(Long idPullConexion, String nombreDataSource, String driver, String url, String usuario, String clave, String estado, String usuarioCrea, Date fechaActualizacion ) {
        super();
        this.idPullConexion = idPullConexion;
        this.nombreDataSource = nombreDataSource;
        this.driver = driver;
        this.url = url;
        this.usuario = usuario;
        this.clave = clave;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
        this.estado = estado;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idPullConexion == null) ? 0 : idPullConexion.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PullConexion other = (PullConexion) obj;
        if (idPullConexion == null) {
            if (other.idPullConexion != null) {
                return false;
            }
        } else if (!idPullConexion.equals(other.idPullConexion)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PullConexion [idPullConexion=" + idPullConexion + "]";
    }
   
}