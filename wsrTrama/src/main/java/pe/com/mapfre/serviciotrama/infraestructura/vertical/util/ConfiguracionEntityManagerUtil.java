package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

public class ConfiguracionEntityManagerUtil {
public static final String ESQUEMA_TRON2000 = "TRON2000";
/** La Constante NMS_MODEL. */
public static final String PWR_MODEL = "pe.gob.mapfre.pwr.jpa";

/** La Constante PWR_MODEL_AUTOMATIC. */
public static final String PWR_MODEL_AUTOMATIC = "pe.gob.mapfre.pwr.jpa.automatic";

/** La Constante PWR_MODEL_INTEGRATION_TRON2000. *///TRON2000
public static final String PWR_MODEL_INTEGRATION_TRON2000 = "pe.com.mapfre.serviciotrama.pu.tron2000";

/** La Constante PWR_MODEL_INTEGRATION_TRON2000_AUTOMATIC. *///TRON2000
public static final String PWR_MODEL_INTEGRATION_TRON2000_AUTOMATIC = "pe.gob.mapfre.pwr.jpa.integration.tron2000.automatic";

/** La Constante PWR_MODEL_INTEGRATION_CUBOS. *///CUBOS
public static final String PWR_MODEL_INTEGRATION_CUBOS = "pe.gob.mapfre.pwr.jpa.integration.cubos";


/** La Constant ESQUEMA_REPORT. */
public static final String ESQUEMA_REPORT = "PSTWEB";

/** La Constant ESQUEMA_INTEGRATION_CUBOS. */
public static final String ESQUEMA_INTEGRATION_TRON2000 = "TRON2000";


public static final String ESQUEMA_INTEGRATION_OIM = "OIM";

/** La Constant ESQUEMA_INTEGRATION_CUBOS. */
public static final String ESQUEMA_INTEGRATION_CUBOS = "CUBOS";

/** La Constant ESQUEMA_INTEGRACION_PROCESOS **/
public static final String ESQUEMA_INTEGRATION_PROCESOS = "TRON2000";
public static final String ESQUEMA_INTEGRATION_SPEED400MF ="speed400mf";
}
