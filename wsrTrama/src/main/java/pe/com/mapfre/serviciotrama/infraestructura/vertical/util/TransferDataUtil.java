package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.ScriptSqlResulJDBCVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.AtributoEntityVO;

/**
 * La Class TransferDataUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class TransferDataUtil extends TransferDataObjectValidarUtil implements Serializable {

	private static final String SERIAL_VERSION_UID = "serialVersionUID";

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** El log. */
	private static Logger log = Logger.getLogger(TransferDataUtil.class);

	/**
	 * Instancia un nuevo data export excel.
	 */
	public TransferDataUtil() {
		//
	}

	public static <T> T toPojo(Object ressul, Class<T> entityClassVO) {
		String className = "";
		if (ressul == null) {
			return null;
		}
		try {
			T resultado = entityClassVO.getDeclaredConstructor().newInstance();
			className = ressul.getClass().getName();
			String handlerHibernate = obtenerHandlerHibernate(className);
			if (className.contains(handlerHibernate)) {
				int indexOf = className.indexOf(handlerHibernate);
				className = className.substring(0, indexOf);
				Hibernate.initialize(ressul);
				if (ressul instanceof HibernateProxy) {
					ressul = ((HibernateProxy) ressul).getHibernateLazyInitializer().getImplementation();
				}
			}
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			Map<String, Integer> fieldHerenciaMap = fieldHerenciaMap(ressul);
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (!SERIAL_VERSION_UID.equalsIgnoreCase(objAtr.getNombreAtributo())) {
					try {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						setField(f, resultado, fValue.get(ressul));
					} catch (Exception e) {
						//
					}

				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toPojo() al parsear class name = " + className + ", "
					+ entityClassVO.getName() + "  " + e.getMessage());
		}
		return null;
	}

	/**
	 * Transfer objeto entity get rest dto.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param info
	 *            el info
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the t
	 */
	public static <T> T toRestDTO(@Context UriInfo info, Class<T> entityClassDTO) {
		T resultado = null;
		try {
			if (info == null) {
				return null;
			}
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassDTO);
			resultado = entityClassDTO.getDeclaredConstructor().newInstance();
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);
			for (AtributoEntityVO objAtr : listaAtributos) {
				Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
				Object value = obtenerValor(info.getQueryParameters().getFirst(objAtr.getNombreAtributo()), objAtr,false);
				setField(f, resultado, value);
			}

		} catch (Exception e) {
			log.error("Error TransferDataUtil.toRestDTO() al parsear " + entityClassDTO.getName() + "  "
					+ e.getMessage());
		}
		return resultado;
	}

	public static Map<String, Object> toGetRestMap(@Context UriInfo info) {
		Map<String, Object> resultado = new HashMap<>();
		try {
			if (info == null) {
				return resultado;
			}
			for (String key : info.getQueryParameters().keySet()) {
				resultado.put(key.toUpperCase(), info.getQueryParameters().get(key));
			}

		} catch (Exception e) {
			log.error("Error TransferDataObjectUtil.transferObjetoEntityGetRestMap " + e.getMessage());
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity historial.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the t
	 */
	public static <T> T toHistorial(Object ressul, Class<T> entityClassDTO) {
		try {
			if (ressul == null) {
				return null;
			}
			T resultado = entityClassDTO.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass().getName());
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (objAtr.isColumn()) {
					try {
						Field f = getField(resultado, objAtr.getNombreAtributo());
						Field fValue = getField(ressul, objAtr.getNombreAtributo());
						setField(f, resultado, fValue.get(ressul));
					} catch (Exception e) {
						//
					}
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toHistorial() al parsear " + entityClassDTO.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	/**
	 * Transfer objeto entity dto list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param <E>
	 *            el tipo de elemento
	 * @param ressulList
	 *            el ressul list
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the list
	 */
	public static <T, E> List<T> toList(List<E> ressulList, Class<T> entityClassDTO) {
		List<T> resultado = new ArrayList<>();
		if (ressulList == null) {
			return resultado;
		}
		for (Object ressul : ressulList) {
			T resultadoTemp = toDTO(ressul, entityClassDTO);
			resultado.add(resultadoTemp);
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity dto list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param <E>
	 *            el tipo de elemento
	 * @param ressulList
	 *            el ressul list
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the list
	 */
	public static <T, E> List<T> toList(List<E> ressulList, Class<T> entityClassDTO, String... entityClasess) {
		List<T> resultado = new ArrayList<>();
		if (ressulList == null) {
			return resultado;
		}
		for (Object ressul : ressulList) {
			T resultadoTemp = toDTO(ressul, entityClassDTO, entityClasess);
			resultado.add(resultadoTemp);
		}
		return resultado;
	}

	private static <T> T transferObjetoEntityDTOPK(Object ressul, Class<T> entityClassDTO, String... entityClasess) {
		return toEntityDTO(ressul, entityClassDTO, true, entityClasess);
	}

	public static <T> T toDTO(Object ressul, Class<T> entityClassDTO, String... entityClasess) {
		return toEntityDTO(ressul, entityClassDTO, false, entityClasess);
	}

	/**
	 * Transfer objeto entity dto.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the t
	 */
	private static <T> T toEntityDTO(Object ressul, Class<T> entityClassDTO, boolean esPK, String... entityClasess) {
		String className = "";
		if (ressul == null) {
			return null;
		}

		try {

			T resultado = entityClassDTO.getDeclaredConstructor().newInstance();
			className = ressul.getClass().getName();
			String handlerHibernate = obtenerHandlerHibernate(className);
			if (className.contains(handlerHibernate)) {
				int indexOf = className.indexOf(handlerHibernate);
				className = className.substring(0, indexOf);
				Hibernate.initialize(ressul);
				if (ressul instanceof HibernateProxy) {
					ressul = ((HibernateProxy) ressul).getHibernateLazyInitializer().getImplementation();
				}
			}
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(className);
			Map<String, Integer> fieldHerenciaMap = fieldHerenciaMap(ressul);
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (esPK) {
					if (objAtr.isColumn() && objAtr.isEsPK()) {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						setField(f, resultado, fValue.get(ressul));
						break;
					}
				} else {
					if (objAtr.isColumn() || objAtr.isTransient()) {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						setField(f, resultado, fValue.get(ressul));
					}
				}

			}
			if (entityClasess != null && entityClasess.length > 0) {
				for (String clasesPojoTemp : entityClasess) {
					boolean isSubClase = clasesPojoTemp.contains(":");
					String[] entitySubClasess = null;
					if (isSubClase) {
						String[] dataTempClase = clasesPojoTemp.split(":");
						clasesPojoTemp = dataTempClase[0];
						String dataTempArray = dataTempClase[1];
						if (dataTempArray.contains("{")) {
							int indexOf = dataTempArray.indexOf("{");
							int lastIndexOf = dataTempArray.lastIndexOf("}");
							dataTempArray = dataTempArray.substring(indexOf + 1, lastIndexOf);
						}

						String[] entitySubClasessTemp = dataTempArray.split(";", -1);
						if (entitySubClasessTemp != null && entitySubClasessTemp.length > 0) {
							entitySubClasess = new String[entitySubClasessTemp.length];
							int index = 0;
							for (String dataTemp : entitySubClasessTemp) {
								if (!dataTemp.contains("{") && !dataTemp.contains("}")) {
									entitySubClasess[index] = dataTemp;
									index++;
								}
							}
						} else {
							if (entitySubClasessTemp != null) {
								entitySubClasess = new String[1];
								String dataTemp = entitySubClasessTemp[0];
								int indexOf = dataTemp.indexOf("{");
								int lastIndexOf = dataTemp.lastIndexOf("}");
								dataTemp = dataTemp.substring(indexOf + 1, lastIndexOf);
								entitySubClasess[0] = dataTemp;
							}
						}

					}
					String clasesPojo = clasesPojoTemp;
					boolean esTansferSoloPK = false;
					if (clasesPojoTemp.contains("@PK@")) {
						esTansferSoloPK = true;
						clasesPojo = clasesPojoTemp.substring(0, clasesPojoTemp.indexOf("@PK@"));
					}

					try {
						Field f = getField(resultado, clasesPojo);
						if (f != null) {
							Field fValue = getField(ressul, clasesPojo);
							Object valueTransfer = fValue.get(ressul);
							if (valueTransfer == null) {
								valueTransfer = fValue.getType().getDeclaredConstructor().newInstance();
							}
							Object value = null;
							if (esTansferSoloPK) {
								value = transferObjetoEntityDTOPK(valueTransfer, f.getType());
							} else {
								if (entitySubClasess != null) {
									value = toDTO(valueTransfer, f.getType(), entitySubClasess);
								} else {
									value = toDTO(valueTransfer, f.getType());
								}

							}
							setField(f, resultado, value);
						}
					} catch (Exception e) {
						//
					}

				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntityDTO() al parsear class name = " + className + ", "
					+ entityClassDTO.getName() + "  " + e.getMessage());
		}
		return null;
	}

	public static <T> T to(Object ressul, Class<T> entityClassEntity, boolean isFiltro, String... entityClasess) {
		return toEntity(ressul, entityClassEntity, false, isFiltro, entityClasess);
	}

	private static <T> T transferObjetoEntityPK(Object ressul, Class<T> entityClassEntity, boolean isFiltro,
			String... entityClasess) {
		return toEntity(ressul, entityClassEntity, true, isFiltro, entityClasess);
	}

	/**
	 * Transfer objeto entity.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the t
	 */
	public static <T> T toEntity(Object ressul, Class<T> entityClassEntity, boolean esPK, boolean isFiltro,
			String... entityClasess) {
		if (ressul == null) {
			return null;
		}
		try {

			T resultado = entityClassEntity.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity.getName());
			if (!esPK) {
				entityClasess = obtenerListaAtributosPK(entityClassEntity, entityClasess);
			}
			Map<String, Integer> fieldHerenciaMap = fieldHerenciaMap(ressul);
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);

			for (AtributoEntityVO objAtr : listaAtributos) {
				if (esPK) {
					if ((objAtr.isColumn() || objAtr.isTransient()) && objAtr.isEsPK()) {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						Object value = fValue.get(ressul);
						if (!StringUtil.isNullOrEmpty(value)) {
							setField(f, resultado, value);
						} else {
							if (!isFiltro) {
								resultado = null;
							}
						}
						break;
					}

				} else {
					if (objAtr.isColumn() || objAtr.isTransient()) {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						setField(f, resultado, fValue.get(ressul));
					}
				}
			}
			if (entityClasess != null && entityClasess.length > 0) {
				for (String clasesPojoTemp : entityClasess) {
					boolean isSubClase = clasesPojoTemp.contains(":");
					String[] entitySubClasess = null;
					if (isSubClase) {
						String[] dataTempClase = clasesPojoTemp.split(":");
						clasesPojoTemp = dataTempClase[0];
						String dataTempArray = dataTempClase[1];
						if (dataTempArray.contains("{")) {
							int indexOf = dataTempArray.indexOf("{");
							int lastIndexOf = dataTempArray.lastIndexOf("}");
							dataTempArray = dataTempArray.substring(indexOf + 1, lastIndexOf);
						}

						String[] entitySubClasessTemp = dataTempArray.split(";", -1);
						if (entitySubClasessTemp != null && entitySubClasessTemp.length > 0) {
							entitySubClasess = new String[entitySubClasessTemp.length];
							int index = 0;
							for (String dataTemp : entitySubClasessTemp) {
								if (!dataTemp.contains("{") && !dataTemp.contains("}")) {
									entitySubClasess[index] = dataTemp;
									index++;
								}
							}
						} else {
							if (entitySubClasessTemp != null) {
								entitySubClasess = new String[1];
								String dataTemp = entitySubClasessTemp[0];
								int indexOf = dataTemp.indexOf("{");
								int lastIndexOf = dataTemp.lastIndexOf("}");
								dataTemp = dataTemp.substring(indexOf + 1, lastIndexOf);
								entitySubClasess[0] = dataTemp;
							}
						}

					}
					String clasesPojo = clasesPojoTemp;
					boolean esTansferSoloPK = false;
					if (clasesPojoTemp.contains("@PK@")) {
						esTansferSoloPK = true;
						clasesPojo = clasesPojoTemp.substring(0, clasesPojoTemp.indexOf("@PK@"));
					}

					try {
						Field f = getField(resultado, clasesPojo);
						if (f != null) {
							Field fValue = getField(ressul, clasesPojo);
							Object valueTransfer = fValue.get(ressul);
							if (valueTransfer == null) {
								valueTransfer = fValue.getType().getDeclaredConstructor().newInstance();
							}
							Object value = null;
							if (esTansferSoloPK) {
								value = transferObjetoEntityPK(valueTransfer, f.getType(), isFiltro);
							} else {
								if (entitySubClasess != null) {
									value = to(valueTransfer, f.getType(), isFiltro, entitySubClasess);
								} else {
									value = to(valueTransfer, f.getType(), isFiltro);
								}

							}
							setField(f, resultado, value);
						}
					} catch (Exception e) {
						//
					}

				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntity() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	public static <T> T toEntityVO(Object ressul, Class<T> entityClassEntity) {
		if (ressul == null) {
			return null;
		}
		try {
			T resultado = entityClassEntity.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity.getName());
			for (AtributoEntityVO objAtr : listaAtributos) {
				try {
					if (objAtr.isColumn()) {
						Field f = getField(resultado, objAtr.getNombreAtributo());
						Field fValue = getField(ressul, objAtr.getNombreAtributo());
						setField(f, resultado, fValue.get(ressul));
					}
				} catch (Exception e) {
					//
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.transferObjetoEntity() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	public static Map<String, Object> toEntityAtributeMap(Object ressul) {
		return toMap(ressul, false, false);

	}

	/**
	 * Transfer objeto entity atribute map.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @return the map
	 */
	public static Map<String, Object> toMap(Object ressul, boolean isNative, boolean isCargarManyToOne) {
		if (ressul == null) {
			return Collections.emptyMap();
		}
		try {
			Map<String, Object> resultado = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			resultado.putAll(obtenerEntityAtributeMap(listaAtributos, ressul, isNative, false, false));
			if (isCargarManyToOne) {
				listaAtributos = obtenerListaAtributosManyToOne(ressul.getClass());
				resultado.putAll(obtenerEntityAtributeMap(listaAtributos, ressul, isNative, true, false));
			}
			return resultado;
		} catch (Exception e) {
			log.error(
					"Error TransferDataUtil.toMap() al parsear " + ressul.getClass().getName() + "  " + e.getMessage());
		}
		return Collections.emptyMap();
	}

	private static Map<String, Object> obtenerEntityAtributeMap(List<AtributoEntityVO> listaAtributos, Object ressul,
			boolean isNative, boolean isManyToOne, boolean isPK) throws Exception {
		Map<String, Object> resultado = new HashMap<>();
		for (AtributoEntityVO objAtr : listaAtributos) {
			if (!StringUtil.isNullOrEmpty(objAtr.getNombreColumna()) || objAtr.isPKCompuesta()) {
				if (!isManyToOne || isPK) {
					if (isPK) {
						if (objAtr.isEsPK() && !objAtr.isPKCompuesta()) {
							Field fValue = getField(ressul, objAtr.getNombreAtributo());
							Object value = fValue.get(ressul);
							String key = isNative ? objAtr.getNombreColumna() : objAtr.getNombreAtributo();
							if (value == null) {
								resultado.put(key, "");
							} else {
								resultado.put(key, value);
							}
							break;
						} else {
							if (objAtr.isEsPK() && objAtr.isPKCompuesta()) {
								Field fValue = getField(ressul, objAtr.getNombreAtributo());
								Object value = fValue.get(ressul);
								if (value == null) {
									value = fValue.getType().getDeclaredConstructor().newInstance();
								}
								resultado.putAll(toMap(value, isNative, false));
								break;
							}

						}
					} else {
						if (!objAtr.isPKCompuesta()) {
							Field fValue = getField(ressul, objAtr.getNombreAtributo());
							Object value = fValue.get(ressul);
							String key = isNative ? objAtr.getNombreColumna() : objAtr.getNombreAtributo();
							if (value == null) {
								resultado.put(key, "");
							} else {
								resultado.put(key, value);
							}
						} else {
							Field fValue = getField(ressul, objAtr.getNombreAtributo());
							Object value = fValue.get(ressul);
							if (value == null) {
								value = fValue.getType().getDeclaredConstructor().newInstance();
							}
							resultado.putAll(toMap(value, isNative, false));
						}
					}

				} else {
					Field fValue = getField(ressul, objAtr.getNombreAtributo());
					Object valueTransfer = fValue.get(ressul);
					List<AtributoEntityVO> listaAtributosManyToOne = obtenerListaAtributos(valueTransfer.getClass());
					Map<String, Object> valueMap = obtenerEntityAtributeMap(listaAtributosManyToOne, valueTransfer,
							isNative, false, true);
					Object value = valueMap.get(getAtributoPK(listaAtributosManyToOne, isNative));
					String key = isNative ? objAtr.getNombreColumna() : objAtr.getNombreAtributo();
					if (value == null) {
						resultado.put(key, "");
					} else {
						resultado.put(key, value);
					}
				}

			}
		}
		return resultado;
	}

	private static String getAtributoPK(List<AtributoEntityVO> listaAtributosManyToOne, boolean isNative) {
		String resultado = "";
		for (AtributoEntityVO objAtr : listaAtributosManyToOne) {
			if (!StringUtil.isNullOrEmpty(objAtr.getNombreColumna()) || objAtr.isEsPK()) {
				resultado = isNative ? objAtr.getNombreColumna() : objAtr.getNombreAtributo();
				break;
			}
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity campos map.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @return the map
	 */
	public static Map<String, Object> toEntityMap(Object ressul) {// OPTIMIZA$R
		return toMap(ressul, true, false);
		/*
		 * if (ressul == null) { return Collections.emptyMap(); } try { Map<String,
		 * Object> resultado = new HashMap<>(); List<AtributoEntityVO> listaAtributos =
		 * obtenerListaAtributos(ressul.getClass()); for (AtributoEntityVO objAtr :
		 * listaAtributos) { if (!StringUtil.isNullOrEmpty(objAtr.getNombreColumna()) ||
		 * objAtr.isPKCompuesta()) { if (!objAtr.isPKCompuesta()) { Field fValue =
		 * getField(ressul, objAtr.getNombreAtributo()); Object value =
		 * fValue.get(ressul); if (value == null) {
		 * resultado.put(objAtr.getNombreColumna(), ""); } else {
		 * resultado.put(objAtr.getNombreColumna(), value); } } else { Field fValue =
		 * getField(ressul, objAtr.getNombreAtributo()); Object value =
		 * fValue.get(ressul); if (value == null) { value =
		 * fValue.getType().getDeclaredConstructor().newInstance(); }
		 * resultado.putAll(toEntityMap(value));
		 * 
		 * } } } return resultado; } catch (Exception e) {
		 * log.error("Error TransferDataUtil.toEntityMap() al parsear " +
		 * ressul.getClass().getName() + "  " + e.getMessage()); } return
		 * Collections.emptyMap();
		 */
	}

	/**
	 * Transfer objeto vo atribute map.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param listaObjeto
	 *            el lista objeto
	 * @param listaAtributo
	 *            el lista atributo
	 * @param isExcluir
	 *            el is excluir
	 * @return the map
	 */
	public static Map<String, Map<String, Object>> toVOMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir) {
		if (ressul == null) {
			return Collections.emptyMap();
		}
		try {
			if (listaAtributo == null) {
				listaAtributo = new ArrayList<>();
			}
			if (isExcluir && !listaAtributo.contains(SERIAL_VERSION_UID)) {
				listaAtributo.add(SERIAL_VERSION_UID);
			}
			if (listaObjeto == null) {
				listaObjeto = new HashMap<>();
			}
			Map<String, Map<String, Object>> resultado = new HashMap<>();
			Map<String, Object> resultadoValue = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			for (AtributoEntityVO objAtr : listaAtributos) {
				boolean isObtenerAtributo = false;
				if (isExcluir) {
					isObtenerAtributo = !listaAtributo.contains(objAtr.getNombreAtributo());
				} else {
					isObtenerAtributo = listaAtributo.contains(objAtr.getNombreAtributo());
				}
				if (isObtenerAtributo) {
					try {
						Field fValue = getField(ressul, objAtr.getNombreAtributo());
						Object value = fValue.get(ressul);
						if (value != null) {
							if (!listaObjeto.containsKey(objAtr.getNombreAtributo())) {
								if (!StringUtil.isNullOrEmpty(value)) {
									if (!objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
										resultadoValue.put(objAtr.getNombreAtributo(), value);
									} else {
										resultadoValue.put(objAtr.getNombreAtributo(),
												FechaUtil.obtenerFechaFormatoCompleto((Date) value));
									}
								}
							} else {
								resultadoValue.put(objAtr.getNombreAtributo(), ARTIFICIO_CLASS);
								resultado.putAll(
										toVOMap(value, null, listaObjeto.get(objAtr.getNombreAtributo()), false));
							}
						}
					} catch (Exception e) {
						//
					}
				}
			}
			if (resultadoValue.size() > 0) {
				resultado.put(ressul.getClass().getName(), resultadoValue);
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toVOMap() al parsear " + ressul.getClass().getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyMap();
	}

	public static <T> Map<String, Map<String, Object>> toVOMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir, String claseNamePadre, String grupoList) {
		try {
			if (ressul == null) {
				return null;
			}
			if (listaAtributo == null) {
				listaAtributo = new ArrayList<>();
			}
			if (isExcluir) {
				if (!listaAtributo.contains("serialVersionUID")) {
					listaAtributo.add("serialVersionUID");
				}
			}
			if (listaObjeto == null) {
				listaObjeto = new HashMap<>();
			}
			Map<String, Map<String, Object>> resultado = new HashMap<>();
			Map<String, Object> resultadoValue = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(ressul.getClass());
			boolean soloInclurirAtributosEnviados = listaObjeto.containsKey(ressul.getClass().getName());
			List<String> listaAtributoEnviado = new ArrayList<>();
			if (soloInclurirAtributosEnviados) {
				listaAtributoEnviado = listaObjeto.get(ressul.getClass().getName());
			}
			Map<String, Integer> fieldHerenciaMap = fieldHerenciaMap(ressul);
			for (AtributoEntityVO objAtr : listaAtributos) {
				String nombreAtributo = objAtr.getNombreAtributo().toLowerCase();
				boolean isObtenerAtributo = false;
				if (isExcluir) {
					isObtenerAtributo = !listaAtributo.contains(nombreAtributo);
				} else {
					isObtenerAtributo = listaAtributo.contains(nombreAtributo);
				}
				if (isObtenerAtributo && soloInclurirAtributosEnviados) {
					isObtenerAtributo = listaAtributoEnviado.contains(nombreAtributo);
				}
				if (isObtenerAtributo) {
					try {
						// Field fValue =
						// ressul.getClass().getDeclaredField(objAtr.getNombreAtributo());
						/*
						 * if
						 * (objAtr.getNombreAtributo().toLowerCase().contains("riesgoSCTR".toLowerCase()
						 * )) { System.out.println("siiiiiiii"); }
						 */
						boolean isLista = false;
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						fValue.setAccessible(true);
						Object value = fValue.get(ressul);
						if (value != null) {
							isLista = value.getClass().isAssignableFrom(ArrayList.class);
							if (!listaObjeto.containsKey(nombreAtributo)) {
								String keyRecursive = claseNamePadre + "." + nombreAtributo;
								if (!listaObjeto.containsKey(keyRecursive)) {
									if (!StringUtil.isNullOrEmpty(value)) {
										if (!objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
											if (isLista) {
												int cantidad = 0;
												for (Object objList : (ArrayList) value) {
													cantidad++;
													resultadoValue.put(
															objAtr.getNombreAtributo() + "${List}" + cantidad, objList);
												}
											} else {
												resultadoValue.put(objAtr.getNombreAtributo(), value);
											}

										} else {
											resultadoValue.put(objAtr.getNombreAtributo(),
													FechaUtil.obtenerFechaFormatoCompleto((Date) value));
										}
									}
								} else {
									if (isLista) {
										int cantidad = 0;
										for (Object objList : (ArrayList) value) {
											cantidad++;
											resultadoValue.put(objAtr.getNombreAtributo() + "" + cantidad,
													objList.getClass().getName());
											resultado
													.putAll(toVOMap(objList, listaObjeto, listaObjeto.get(keyRecursive),
															false, nombreAtributo, "" + cantidad));
										}

									} else {
										resultadoValue.put(objAtr.getNombreAtributo(),
												objAtr.getClasssAtributoType().getName());
										resultado.putAll(toVOMap(value, listaObjeto, listaObjeto.get(keyRecursive),
												false, nombreAtributo, ""));
									}

								}

							} else {
								if (isLista) {
									int cantidad = 0;
									for (Object objList : (ArrayList) value) {
										cantidad++;
										resultadoValue.put(objAtr.getNombreAtributo(), objList.getClass().getName());
										resultado.putAll(toVOMap(objList, listaObjeto, listaObjeto.get(nombreAtributo),
												false, nombreAtributo, "" + cantidad));
									}
								} else {
									resultadoValue.put(objAtr.getNombreAtributo(),
											objAtr.getClasssAtributoType().getName());
									resultado.putAll(toVOMap(value, listaObjeto, listaObjeto.get(nombreAtributo), false,
											nombreAtributo, ""));
								}

							}
						}
					} catch (Exception e) {
						// log.error("error convertir " + objAtr.getNombreAtributo());
					}
				}
			}
			if (resultadoValue.size() > 0) {
				resultado.put(ressul.getClass().getName() + grupoList, resultadoValue);
			}
			return resultado;
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityAtributeMap(Object ressul,Class<T> entityClassEntity) al parsear "
							+ ressul.getClass().getName() + "  " + e.getMessage());
		}
		return null;
	}

	public static Map<String, Map<String, Object>> toFiltroMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir, String nombreClase, Map<String, String> fechaFormatoMap) {
		if (ressul == null) {
			return Collections.emptyMap();
		}
		try {

			if (listaAtributo == null) {
				listaAtributo = new ArrayList<>();
			}
			if (isExcluir && !listaAtributo.contains(SERIAL_VERSION_UID)) {
				listaAtributo.add(SERIAL_VERSION_UID);
			}
			if (listaObjeto == null) {
				listaObjeto = new HashMap<>();
			}
			Map<String, Map<String, Object>> resultado = new HashMap<>();
			Map<String, Object> resultadoValue = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			for (AtributoEntityVO objAtr : listaAtributos) {
				boolean isObtenerAtributo = false;
				if (isExcluir) {
					isObtenerAtributo = !listaAtributo.contains(objAtr.getNombreAtributo());
				} else {
					isObtenerAtributo = listaAtributo.contains(objAtr.getNombreAtributo());
				}
				if (isObtenerAtributo) {
					try {
						Field fValue = getField(ressul, objAtr.getNombreAtributo());
						Object value = fValue.get(ressul);
						StringBuilder nombreAtributoFinal = new StringBuilder();
						if (!StringUtil.isNullOrEmpty(nombreClase)) {
							nombreAtributoFinal.append(nombreClase);
							nombreAtributoFinal.append(".");
							nombreAtributoFinal.append(objAtr.getNombreAtributo());
						} else {
							nombreAtributoFinal.append(objAtr.getNombreAtributo());
						}
						if (value != null) {
							if (value instanceof Number) {
								BigDecimal numberTemp = new BigDecimal(value.toString());
								if (numberTemp.compareTo(BigDecimal.ZERO) <= 0) {
									value = null; // nulear valores por defecto 0
								}
							}
							if (!listaObjeto.containsKey(objAtr.getNombreAtributo())) {
								if (value != null) {
									if (!objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
										resultadoValue.put(nombreAtributoFinal.toString(), value);
									} else {
										resultadoValue.put(nombreAtributoFinal.toString(),
												FechaUtil.obtenerFechaFormatoPersonalizado((Date) value,
														fechaFormatoMap.get(nombreAtributoFinal.toString())));
									}
								} else {
									resultadoValue.put(nombreAtributoFinal.toString(), null);
								}
							} else {
								resultado.putAll(toFiltroMap(value, null, listaObjeto.get(objAtr.getNombreAtributo()),
										false, objAtr.getNombreAtributo(), fechaFormatoMap));
							}
						} else {
							if (!listaObjeto.containsKey(objAtr.getNombreAtributo())) {
								resultadoValue.put(nombreAtributoFinal.toString(), null);
							}
						}
					} catch (Exception e) {
						//
					}
				}
			}
			if (resultadoValue.size() > 0) {
				resultado.put(ressul.getClass().getName(), resultadoValue);
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toFiltroMap() al parsear " + ressul.getClass().getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyMap();
	}

	/**
	 * Transfer objeto vo atribute map.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @return the map
	 */
	public static Map<String, Object> toVOMap(Object ressul) {
		if (ressul == null) {
			return Collections.emptyMap();
		}
		try {
			Map<String, Object> resultado = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			for (AtributoEntityVO objAtr : listaAtributos) {
				try {
					Field fValue = getField(ressul, objAtr.getNombreAtributo());
					Object value = fValue.get(ressul);
					if (value != null) {
						if (!StringUtil.isNullOrEmpty(value)) {
							if (!objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
								resultado.put(objAtr.getNombreAtributo(), value);
							} else {
								resultado.put(objAtr.getNombreAtributo(),
										FechaUtil.obtenerFechaFormatoCompleto((Date) value));
							}
						}
					}
				} catch (Exception e) {
					//
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toVOMap() al parsear " + ressul.getClass().getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyMap();
	}

	/**
	 * Transfer objeto entity vo.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param scriptSqlResulJDBCVO
	 *            el script sql resul jdbcvo
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the t
	 */
	public static <T> T toEntityVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity) {
		try {
			List<Map<String, Object>> ressul = scriptSqlResulJDBCVO.getListaData();
			if (ressul == null || scriptSqlResulJDBCVO.isTieneError()) {
				return null;
			}
			Map<String, Object> valueMap = scriptSqlResulJDBCVO.getListaData().get(0);
			T resultado = entityClassEntity.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity);
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (scriptSqlResulJDBCVO.getListaHeader().contains(objAtr.getNombreAtributo())) {
					Field f = getField(resultado, objAtr.getNombreAtributo());
					Object value = obtenerValor(valueMap.get(objAtr.getNombreAtributo()) + "", objAtr, false);
					setField(f, resultado, value);
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntityVO() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	public static <T> T toRestVO(Map<String, Object> valueMap, Class<T> entityClassEntity) {
		return toRestVO(valueMap, null, entityClassEntity);
	}

	public static <T> T toRestVO(Map<String, Object> valueMap, Map<String, String> equivalenciaAtributeMap,
			Class<T> entityClassEntity) {
		try {
			T resultado = entityClassEntity.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity);
			for (AtributoEntityVO objAtr : listaAtributos) {
				String keyAtributo = objAtr.getNombreAtributo();
				if (equivalenciaAtributeMap != null && equivalenciaAtributeMap.containsKey(keyAtributo)) {
					keyAtributo = equivalenciaAtributeMap.get(keyAtributo);
				}
				if (valueMap.containsKey(keyAtributo)) {
					Field f = getField(resultado, objAtr.getNombreAtributo());
					Object value = obtenerValor(valueMap.get(keyAtributo), objAtr, false);
					setField(f, resultado, value);
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toRestVO() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	public static <T> List<T> toRestVOList(List<Map<String, Object>> listaValueMap, Class<T> entityClassEntity) {
		return toRestVOList(listaValueMap, null, entityClassEntity);
	}

	public static <T> List<T> toRestVOList(List<Map<String, Object>> listaValueMap,
			Map<String, String> equivalenciaAtributeMap, Class<T> entityClassEntity) {
		List<T> resultado = new ArrayList<>();
		if (listaValueMap == null) {
			return resultado;
		}
		for (Map<String, Object> ressul : listaValueMap) {
			T resultadoTemp = toRestVO(ressul, equivalenciaAtributeMap, entityClassEntity);
			resultado.add(resultadoTemp);
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity list vo.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param scriptSqlResulJDBCVO
	 *            el script sql resul jdbcvo
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the list
	 */
	public static <T> List<T> toEntityListVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity) {
		return toEntityListVO(scriptSqlResulJDBCVO, entityClassEntity, new HashMap<String, String>());
	}

	/**
	 * Transfer objeto entity list vo.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param scriptSqlResulJDBCVO
	 *            el script sql resul jdbcvo
	 * @param entityClassEntity
	 *            el entity class entity
	 * @param formatoFechaMap
	 *            el formato fecha map
	 * @return the list
	 */
	public static <T> List<T> toEntityListVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity,
			Map<String, String> formatoFechaMap) {
		try {
			List<Map<String, Object>> ressul = scriptSqlResulJDBCVO.getListaData();
			if (ressul == null || scriptSqlResulJDBCVO.isTieneError()) {
				return Collections.emptyList();
			}
			List<T> resultado = new ArrayList<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity);
			for (Map<String, Object> valueMap : scriptSqlResulJDBCVO.getListaData()) {
				T resultadoTemp = entityClassEntity.getDeclaredConstructor().newInstance();
				for (AtributoEntityVO objAtr : listaAtributos) {
					if (scriptSqlResulJDBCVO.getListaHeader().contains(objAtr.getNombreAtributo())) {
						Object value = null;
						try {
							Field f = getField(resultadoTemp, objAtr.getNombreAtributo());
							value = obtenerValor(valueMap.get(objAtr.getNombreAtributo()) + "", objAtr, false,
									formatoFechaMap);
							setField(f, resultadoTemp, value);
						} catch (Exception e) {
							//
						}
					}
				}
				resultado.add(resultadoTemp);
			}

			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntityListVO() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyList();
	}

	public static <T> List<T> toEntityListVO(Map<String, String> campoMapping,
			ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity,
			Map<String, String> formatoFechaMap) {
		try {
			List<Map<String, Object>> ressul = scriptSqlResulJDBCVO.getListaData();
			if (ressul == null || scriptSqlResulJDBCVO.isTieneError()) {
				return Collections.emptyList();
			}
			List<T> resultado = new ArrayList<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity);
			for (Map<String, Object> valueMap : scriptSqlResulJDBCVO.getListaData()) {
				T resultadoTemp = entityClassEntity.getDeclaredConstructor().newInstance();
				for (AtributoEntityVO objAtr : listaAtributos) {
					String campoKeyBD = campoMapping.get(objAtr.getNombreAtributo()) + "";
					if (scriptSqlResulJDBCVO.getListaHeader().contains(campoKeyBD)) {
						Object value = null;
						try {
							Field f = getField(resultadoTemp, objAtr.getNombreAtributo());
							value = obtenerValor(valueMap.get(campoKeyBD) + "", objAtr, false, formatoFechaMap);
							setField(f, resultadoTemp, value);
						} catch (Exception e) {
							//
						}

					}
				}
				resultado.add(resultadoTemp);
			}

			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntityListVO() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyList();
	}

	/**
	 * Transfer objeto entity list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param <E>
	 *            el tipo de elemento
	 * @param ressulList
	 *            el ressul list
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the list
	 */
	public static <T, E> List<T> toListEntity(List<E> ressulList, Class<T> entityClassEntity, boolean isFiltro,
			String... entityClasess) {
		List<T> resultado = new ArrayList<>();
		if (ressulList == null) {
			return resultado;
		}
		for (Object ressul : ressulList) {
			T resultadoTemp = to(ressul, entityClassEntity, isFiltro, entityClasess);
			resultado.add(resultadoTemp);
		}
		return resultado;
	}

	public static <T> T transferObjetoVOTrama(Map<String, Map<String, Object>> listaObjectValueMap,
			Class<T> entityClass, String grupoList) {
		try {
			Map<String, Object> ressul = listaObjectValueMap.get(entityClass.getName() + "" + grupoList);
			if (ressul == null) {
				return entityClass.newInstance();
			}
			T resultado = entityClass.newInstance();
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(entityClass);
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (ressul.containsKey(objAtr.getNombreAtributo())
						|| ressul.containsKey(objAtr.getNombreAtributo() + "${List}1")) {

					Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
					f.setAccessible(true);
					Object value = null;
					value = obtenerValor(ressul.get(objAtr.getNombreAtributo()) + "", objAtr, true);
					if (value == null) {
						value = obtenerValor(ressul.get(objAtr.getNombreAtributo() + "${List}1") + "", objAtr, true);
					}
					if (value != null) {
						if (listaObjectValueMap.containsKey(value.toString())
								|| listaObjectValueMap.containsKey(value.toString() + "1")) {
							boolean isLista = objAtr.getClasssAtributoType().isAssignableFrom(ArrayList.class);
							if (isLista) {
								int cantidad = 1;
								String key = value + "" + cantidad;
								List<Object> arrayLis = new ArrayList<>();
								while (listaObjectValueMap.containsKey(key)) {
									Object valueList = transferObjetoVOTrama(listaObjectValueMap,
											AtributosEntityCacheUtil.getInstance().obtenerClass(value.toString()),
											cantidad + "");
									arrayLis.add(valueList);
									cantidad++;
									key = value.toString() + "" + cantidad;
								}
								try {
									if (value != null) {
										f.set(resultado, arrayLis);
									}
								} catch (Exception e) {
									log.error(
											"Error OBJETO TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
													+ entityClass.getName() + " campo " + objAtr.getNombreAtributo()
													+ "  " + e.getMessage());
								}
							} else {
								if (listaObjectValueMap.containsKey(objAtr.getClasssAtributoType().getName())) {
									value = transferObjetoVOTrama(listaObjectValueMap, objAtr.getClasssAtributoType(),
											"");
									try {
										if (value != null) {
											f.set(resultado, value);
										}
									} catch (Exception e) {
										log.error(
												"Error OBJETO TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
														+ entityClass.getName() + " campo " + objAtr.getNombreAtributo()
														+ "  " + e.getMessage());
									}
								}
							}

						} else {
							boolean isLista = objAtr.getClasssAtributoType().isAssignableFrom(ArrayList.class);
							if (isLista) {
								int cantidad = 1;
								String key = objAtr.getNombreAtributo() + "${List}" + "" + cantidad;
								List<Object> arrayLis = new ArrayList<>();
								while (ressul.containsKey(key)) {
									Object valueList = ressul.get(objAtr.getNombreAtributo() + "${List}" + cantidad);
									arrayLis.add(valueList);
									cantidad++;
									key = objAtr.getNombreAtributo() + "${List}" + "" + cantidad;
								}
								try {
									if (value != null) {
										f.set(resultado, arrayLis);
									}
								} catch (Exception e) {
									log.error(
											"Error OBJETO TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
													+ entityClass.getName() + " campo " + objAtr.getNombreAtributo()
													+ "  " + e.getMessage());
								}
							} else {
								try {
									if (value != null) {
										f.set(resultado, value);
									}
								} catch (Exception e) {
									log.error(
											"Error TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
													+ entityClass.getName() + " campo " + objAtr.getNombreAtributo()
													+ "  " + e.getMessage());
								}
							}

						}
					}
				}
			}
			listaObjectValueMap = null;
			return resultado;
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
							+ entityClass.getName() + "  " + e.getMessage());
		}
		return null;
	}

}
