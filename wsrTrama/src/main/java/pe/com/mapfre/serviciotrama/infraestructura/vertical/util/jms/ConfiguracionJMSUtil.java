package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms;

/**
 * La Class ConfiguracionJMSUtil.
 * <ul>
 * <li>Copyright 2014 MAPFRE -
 * mapfre. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author Tecnocom.
 * @version 1.0, Fri Apr 25 18:56:23 COT 2014
 * @since Rep v1.0
 */
public final class ConfiguracionJMSUtil {
	/** La Constante QCF_TRAMA_CONTROL_NAME. */
	public static final String QCF_TRAMA_CONTROL_NAME = "bsTramaConnectionFactory";
	
	//Fin BuildSoft Mejora Orquestador flujo 11/09/2019
	
	/** La Constante QUEUE_TRAMA_CONTROL_NAME. */
	public static final String QUEUE_TRAMA_CONTROL_NAME = "queue/bsTramaQueue";

	/**
	 * Instancia un nuevo configuracion jms util.
	 */
	private ConfiguracionJMSUtil() {
		
	}
}
