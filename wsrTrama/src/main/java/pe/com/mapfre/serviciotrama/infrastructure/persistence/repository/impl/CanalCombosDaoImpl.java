package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.CanalCombosDaoLocal;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class CanalCombosDaoImpl extends GenericRepository<String, ListaItemsVO> implements CanalCombosDaoLocal {

	@Override
	public ListaItemsVO listar(ListaItemsVO filtro) {
		return getListaListaItemsVO(filtro.getCodigo(), generarQuery(filtro, false), filtro, true);
	}

	private Query generarQuery(ListaItemsVO filtro, boolean esContador) {
		Map<String, Object> parametros = new HashMap<>();
		StringBuilder jpaql = new StringBuilder();
		if (esContador) {
			jpaql.append(" SELECT COUNT(1) FROM tron2000.TARGEN49 where 1=1 ");
		} else {
			jpaql.append(
					" SELECT A.COD_ITEM AS COD_CANAL,A.DESC_ITEM AS NOM_CANAL FROM tron2000.TARGEN49 a where 1=1 ");
		}
		jpaql.append(" AND a.cod_rubro = 130 ");
		jpaql.append(" AND a.cod_tratamiento IN ('Z') ");
		jpaql.append(" AND a.tip_coaseguro IN (9) ");
		jpaql.append(" AND a.cod_ejecutivo IN (99999) ");

		jpaql.append(" AND a.cod_agt IN (99999) ");
		jpaql.append(" AND a.cod_modalidad IN (99999) ");
		jpaql.append(" AND a.num_poliza_grupo IN ('ZZZZZZZZZZZZZ') ");

		jpaql.append(" AND a.cod_mon IN (99) ");
		jpaql.append(" AND a.tip_emision IN ('Z') ");
		jpaql.append(" AND a.cod_spto IN (0) ");

		jpaql.append(" AND a.sub_cod_spto IN (0) ");
		jpaql.append(" AND a.mca_inh = 'N' ");

		jpaql.append(" AND a.fec_validez =\n" + "								(SELECT MAX (b.fec_validez)\n"
				+ "								   FROM targen49 b\n"
				+ "								  WHERE     b.cod_rubro = a.cod_rubro\n"
				+ "										AND b.cod_tratamiento = a.cod_tratamiento\n"
				+ "										AND b.tip_coaseguro = a.tip_coaseguro\n"
				+ "										AND b.cod_ejecutivo = a.cod_ejecutivo\n"
				+ "										AND b.cod_agt = a.cod_agt\n"
				+ "										AND b.cod_ramo = a.cod_ramo\n"
				+ "										AND b.cod_modalidad = a.cod_modalidad\n"
				+ "										AND b.num_poliza_grupo = a.num_poliza_grupo\n"
				+ "										AND b.cod_mon = a.cod_mon\n"
				+ "										AND b.tip_emision = a.tip_emision\n"
				+ "										AND b.cod_spto = a.cod_spto\n"
				+ "										AND b.sub_cod_spto = a.sub_cod_spto\n"
				+ "										AND b.fec_validez <= TRUNC (SYSDATE)) ");

		if (!esContador) {
			jpaql.append(" ORDER BY a.cod_tratamiento,\n" + "								 a.tip_coaseguro,\n"
					+ "								 a.cod_ejecutivo,\n"
					+ "								 a.cod_agt,\n" + "								 a.cod_ramo,\n"
					+ "								 a.cod_modalidad,\n"
					+ "								 a.num_poliza_grupo,\n"
					+ "								 a.cod_mon,\n" + "								 a.tip_emision,\n"
					+ "								 a.cod_spto DESC,\n"
					+ "								 A.SUB_COD_SPTO DESC,\n"
					+ "								 a.num_secu");
		} else {
			jpaql.append(" )");
		}
		return createNativeQuery(jpaql.toString(), parametros);
	}

	@Override
	public int contar(ListaItemsVO filtro) {
		return getContador(generarQuery(filtro, true));
	}

}