package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.AtributosEntityCacheUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.AtributoEntityVO;

/**
 * La Class GenericDAOImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 * 
 * @param <K> el tipo de clave
 * @param <T> el tipo generico
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@SuppressWarnings("hiding")
public class GenericRepository<K, T> extends GenericOperacionRepository {
	// http://www.baeldung.com/simplifying-the-data-access-layer-with-spring-and-java-generics
	// http://www.baeldung.com/persistence-layer-with-spring-and-hibernate

	// https://coderanch.com/t/621607/frameworks/Autowired-find-repository-bean-identified
	private final Logger log = Logger.getLogger(GenericRepository.class);

	@PersistenceContext(unitName = ConfiguracionEntityManagerUtil.PWR_MODEL_INTEGRATION_TRON2000)
	
	private EntityManager entityManager;


	public GenericRepository() {
		super();
	}
	
	@Override
	public Connection getConexionDS() throws SQLException, NamingException {
		Context ctx;
		ctx = new InitialContext();
		DataSource dataSource = (DataSource) ctx.lookup("java:jboss/datasources/pwrIntegrationTron2000DS");
		return dataSource.getConnection();
	}

	public T saveNative(T entity){
		try {
			Map<String, Object> parametros = TransferDataUtil.toMap(entity,true,true);
			List<String> parametrosHeader = new ArrayList<>();
			Map<String,Object> parametrosValue = new LinkedHashMap<>();
			int contador = 0;
			for (String key : parametros.keySet()) {
				contador++;
				parametrosHeader.add(key);
				parametrosValue.put(contador + "", parametros.get(key));
			}
			StringBuilder sql = saveInsert(entity.getClass(),parametrosHeader);
			log.debug(" saveNative sql : " + sql);
			log.debug(" saveNative parametros : " + parametrosValue);
			executeUpdatePreparedStatement(sql, parametrosValue);
		} catch (Exception e) {
			log.error("Error ", e);
		}
		return entity;
	}
	
	public T updateNative(T entity){
		try {
			Map<String, Object> parametros = TransferDataUtil.toMap(entity,true,true);
			List<String> parametrosHeader = new ArrayList<>();
			Map<String,Object> parametrosValue = new LinkedHashMap<>();
			int contador = 0;
			Object param = null;
			String campoPk= getId(entity);
			for (String key : parametros.keySet()) {
				if(!key.equals(campoPk)) {
					contador++;
					if(parametros.get(key).equals("")) {
						param = null;
					}else {
						param = parametros.get(key);
					}
					parametrosHeader.add(key);
					parametrosValue.put(contador + "", param);
				}
			}
			parametrosHeader.add(campoPk);
			parametrosValue.put((contador+1) + "", parametros.get(campoPk));
			StringBuilder sql = updateNative(entity.getClass(),parametrosHeader);
			log.debug(" updateNative sql : " + sql);
			log.debug(" updateNative parametros : " + parametrosValue);
			Map<String, Object> parametros2 = new HashMap<>();
			parametros2.put("1", FechaUtil.obtenerFechaActual());
			executeUpdatePreparedStatement(sql,parametrosValue);

		} catch (Exception e) {
			log.error("Error ", e);
		}
		return entity;
	}
	
	private <T> String getId(T entity) {
		List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance().obtenerListaAtributos(entity.getClass());
		String campoPk = "";
		for (AtributoEntityVO atributoEntityVO : listaAtributos) {
			if (atributoEntityVO.isEsPK()) {
				campoPk = atributoEntityVO.getNombreColumna();
			}		
		}
       return campoPk;
	}
	

	/**
	 * Save.
	 *
	 * @param entity
	 *            el entity
	 * @return the t
	 */
	public T save(T entity) {
		entityManager.persist(entity);
		return entity;
	}

	/**
	 * Update.
	 *
	 * @param entity
	 *            el entity
	 * @return the t
	 */
	public T update(T entity) {
		entityManager.merge(entity);
		return entity;
	}

	/**
	 * Delete.
	 *
	 * @param entity
	 *            el entity
	 * @return the t
	 */
	public T delete(T entity) {
		entityManager.remove(entity);
		return entity;
	}

	/**
	 * Find.
	 *
	 * @param classs
	 *            el classs
	 * @param id
	 *            el id
	 * @return the t
	 */
	public T find(Class<T> classs, K id) {
		return entityManager.find(classs, id);
	}

	/**
	 * Creates the named query.
	 *
	 * @param arg0
	 *            el arg0
	 * @param parametraMap
	 *            el parametra map
	 * @return the query
	 */
	public Query createNamedQuery(String arg0, Map<String, Object> parametraMap) {
		Query query = entityManager.createNamedQuery(arg0);
		if (parametraMap != null) {
			for (Map.Entry<String, Object> entry : parametraMap.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}
		return query;
	}

	/**
	 * Creates the query.
	 *
	 * @param arg0
	 *            el arg0
	 * @param parametraMap
	 *            el parametra map
	 * @return the query
	 */
	public Query createQuery(String arg0, Map<String, Object> parametraMap) {
		Query query = entityManager.createQuery(arg0);
		if (parametraMap != null) {
			for (Map.Entry<String, Object> entry : parametraMap.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}
		return query;
	}

	/**
	 * Creates the native query.
	 *
	 * @param arg0
	 *            el arg0
	 * @param parametraMap
	 *            el parametra map
	 * @return the query
	 */
	public Query createNativeQuery(String arg0, Map<String, Object> parametraMap) {
		Query query = entityManager.createNativeQuery(arg0);
		if (parametraMap != null) {
			for (Map.Entry<String, Object> entry : parametraMap.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}
		return query;
	}

	/**
	 * Creates the native query.
	 *
	 * @param arg0
	 *            el arg0
	 * @param arg1
	 *            el arg1
	 * @param parametraMap
	 *            el parametra map
	 * @return the query
	 */
	public Query createNativeQuery(String arg0, Class<?> arg1, Map<String, Object> parametraMap) {
		Query query = entityManager.createNativeQuery(arg0, arg1);
		if (parametraMap != null) {
			for (Map.Entry<String, Object> entry : parametraMap.entrySet()) {
				query.setParameter(entry.getKey(), entry.getValue());
			}
		}
		return query;
	}
}
