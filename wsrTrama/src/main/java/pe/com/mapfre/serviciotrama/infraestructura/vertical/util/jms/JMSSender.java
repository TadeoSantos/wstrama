package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms;

import java.io.Serializable;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.InitialContext;

/**
 * La Class JMSSender.
 * <ul>
 * <li>Copyright 2021 MAPFRE- mapfre. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft.
 * @version 1.0, 23/05/2021
 * @since Rep v1..0
 */
public class JMSSender {

	// NOTA: Las dos lineas siguientes establecen el nombre de la fabrica de
	// conexiones de cola
	// Y la cola que queremos utilizar.
	public JMSSender() {
	}

	public static String sendMessage(Object obj, String qcfName, String queueName) throws Exception {
		String resultado = "";
		InitialContext ctx = new InitialContext();
		ConnectionFactory connectionFactory = null;
		connectionFactory = (ConnectionFactory) ctx.lookup(qcfName);
		Connection connection = connectionFactory.createConnection();
		Queue queue = null;
		queue = (Queue) ctx.lookup(queueName);
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		MessageProducer messageProducer = session.createProducer(queue);
		ObjectMessage message = session.createObjectMessage();
		message.setObject((Serializable) obj);
		messageProducer.send(message);
		messageProducer.close();
		session.close();
		connection.close();
		connectionFactory = null;
		return resultado;
	}	
}