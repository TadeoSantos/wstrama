package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.JuegoCombosDaoLocal;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class JuegoCombosDaoImpl extends GenericRepository<String, ListaItemsVO> implements JuegoCombosDaoLocal {
	
	@Override
	public ListaItemsVO listar(ListaItemsVO filtro) {
		return getListaListaItemsVO(filtro.getCodigo(), generarQuery(filtro, false), filtro, true);
	}

	private Query generarQuery(ListaItemsVO filtro, boolean esContador) {
		Map<String, Object> parametros = new HashMap<>();
		StringBuilder jpaql = new StringBuilder();
		if (esContador) {
			jpaql.append(" SELECT COUNT(1) FROM PermisoEjecucionManualDetalle where 1=1 ");
		} else {
			jpaql.append(" Select o.juegoTrama.idJuegoTrama, o.juegoTrama.nombre from PermisoEjecucionManualDetalle o where 1 = 1 ");
		}
		if (!StringUtil.isNullOrEmptyNumeric(filtro.getIdNivel1())) {
			jpaql.append(" and o.permisoEjecucionManual.usuarioPermiso.idUsuario = :idUsuario ");
    		parametros.put("idUsuario", filtro.getIdNivel1());
		}
		if (!StringUtil.isNullOrEmptyNumeric(filtro.getIdNivel2())) {
    		jpaql.append(" and o.juegoTrama.procesoFlujo.idProcesoFlujo = :idProcesoFlujo ");
    		parametros.put("idProcesoFlujo", filtro.getIdNivel2());
		}
		
		if (!StringUtil.isNullOrEmptyNumeric(filtro.getIdNivel3())) {
    		jpaql.append(" and o.juegoTrama.producto = :codigoProducto ");
    		parametros.put("codigoProducto",  filtro.getIdNivel3());
		}
		
		if (!StringUtil.isNullOrEmptyNumeric(filtro.getIdNivel4())) {
    		jpaql.append(" and o.juegoTrama.canal = :codigoCanal");
    		parametros.put("codigoCanal", filtro.getIdNivel4());
		}
	
//		jpaql.append(" )");
	
		return createQuery(jpaql.toString(), parametros);
	}

	@Override
	public int contar(ListaItemsVO filtro) {
		return getContador(generarQuery(filtro, true));
	}
	
}