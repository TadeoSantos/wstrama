package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.TramaCombosDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.state.EstadoState;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class TramaCombosDaoImpl extends GenericRepository<String, ListaItemsVO> implements TramaCombosDaoLocal {
	
	@Override
	public ListaItemsVO listar(ListaItemsVO filtro) {
		return getListaListaItemsVO(filtro.getCodigo(), generarQuery(filtro, false), filtro, true);
	}

	private Query generarQuery(ListaItemsVO filtro, boolean esContador) {
		StringBuilder jpaql = new StringBuilder();
		Map<String, Object> parametros = new HashMap<String, Object>();
		jpaql.append(" select o.idConfiguradorTrama,o.tramaNomenclaturaArchivo.nombre,o.tramaNomenclaturaArchivo.tipoArchivo from ConfiguracionTrama o INNER JOIN o.tramaNomenclaturaArchivo nt INNER JOIN o.juegoTrama jt where 1=1 ");
		if (!StringUtil.isNullOrEmpty(filtro.getIdNivel1())) {
			jpaql.append(" and o.juegoTrama.estado =:estado  and o.juegoTrama.estadoProceso =:estado ");
			parametros.put("estado", filtro.getIdNivel1());
		}	
		jpaql.append(" and o.estado =:estadoTrama ");
		parametros.put("estadoTrama", EstadoState.ACTIVO.getKey());
		Long idProcesoFlujo = 0L;
		if (!StringUtil.isNullOrEmpty(filtro.getIdNivel2())) {
			jpaql.append(" and o.juegoTrama.procesoFlujo.idProcesoFlujo =:idProcesoFlujo ");
			parametros.put("idProcesoFlujo", filtro.getIdNivel2());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getIdNivel3())) {
			jpaql.append(" and o.juegoTrama.idJuegoTrama =:idJuegoTrama ");
			parametros.put("idJuegoTrama", filtro.getIdNivel3());
		}
		
		jpaql.append(" order by o.juegoTrama.idJuegoTrama, o.numeroOrden ");
		return createQuery(jpaql.toString(), parametros);
		//return query.getResultList();
	}

	@Override
	public int contar(ListaItemsVO filtro) {
		return getContador(generarQuery(filtro, true));
	}
	
}