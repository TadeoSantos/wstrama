package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ConfiguracionTrama.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_CONFIG_TRAMA", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ConfiguracionTrama implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id configurador trama. */
    @Id
    @Column(name = "N_ID_CONFIG_TRAMA" , length = 18)
    private Long idConfiguradorTrama;
   
    /** El juego trama. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CONFIG_JUEGO_TRAMA", referencedColumnName = "N_ID_CONFIG_JUEGO_TRAMA")
    private JuegoTrama juegoTrama;
   
    /** El trama nomenclatura archivo. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_TRA_NOMEN_ARCHI", referencedColumnName = "N_ID_TRA_NOMEN_ARCHI")
    private TramaNomenclaturaArchivo tramaNomenclaturaArchivo;
   
    /** El fila data. */
    @Column(name = "N_FILA_DATA" , length = 18)
    private Long filaData;
   
    /** El tiene separador. */
    @Column(name = "C_TIENE_SEPARADOR" , length = 1)
    private Character tieneSeparador;
   
    /** El numero orden. */
    @Column(name = "N_ORDEN" , length = 10)
    private Long numeroOrden;
   
    /** El numero hoja. */
    @Column(name = "N_HOJA" , length = 18)
    private Long numeroHoja;
   
    /** El separador. */
    @Column(name = "C_SEPARADOR" , length = 50)
    private String separador;
   
    /** El obligatorio. */
    @Column(name = "C_OBLIGATORIO" , length = 1)
    private Character obligatorio;
   
    /** El estado. */
    @Column(name = "C_ESTADO" , length = 1)
    private String estado;
   
    /** El tipo proceso. */
    @Column(name = "C_TIPO_PROCESO" , length = 2)
    private String tipoProceso;
   
    /** El nombre tabla. */
    @Column(name = "C_NOMBRE_TABLA" , length = 150)
    private String nombreTabla;
   
   
    /** El delimitador data. */
    @Column(name = "C_DELIMITADOR_DATA" , length = 10)
    private String delimitadorData;
    
    /** El delimitador data. */
    @Column(name = "C_COORDENADA" , length = 1)
    private Character esCoordenada;
   
    /** El configuracion trama configuracion trama detalle list. */
    @OneToMany(mappedBy = "configuracionTrama", fetch = FetchType.LAZY)
    private List<ConfiguracionTramaDetalle> configuracionTramaConfiguracionTramaDetalleList = new ArrayList<ConfiguracionTramaDetalle>();
    
    /**
     * Instancia un nuevo configuracion trama.
     */
    public ConfiguracionTrama() {
    }
   
   
    /**
     * Instancia un nuevo configuracion trama.
     *
     * @param idConfiguradorTrama el id configurador trama
     * @param juegoTrama el juego trama
     * @param tramaNomenclaturaArchivo el trama nomenclatura archivo
     * @param filaData el fila data
     * @param tieneSeparador el tiene separador
     * @param numeroOrden el numero orden
     * @param numeroHoja el numero hoja
     * @param separador el separador
     * @param obligatorio el obligatorio
     * @param estado el estado
     * @param tipoProceso el tipo proceso
     * @param nombreTabla el nombre tabla
     * @param cantidadCampo el cantidad campo
     * @param delimitadorData el delimitador data
     */
    public ConfiguracionTrama(Long idConfiguradorTrama, JuegoTrama juegoTrama,TramaNomenclaturaArchivo tramaNomenclaturaArchivo,Long filaData, Character tieneSeparador, Long numeroOrden, Long numeroHoja, String separador, Character obligatorio, String estado, String tipoProceso, String nombreTabla, Long cantidadCampo, String delimitadorData ) {
        super();
        this.idConfiguradorTrama = idConfiguradorTrama;
        this.juegoTrama = juegoTrama;
        this.tramaNomenclaturaArchivo = tramaNomenclaturaArchivo;
        this.filaData = filaData;
        this.tieneSeparador = tieneSeparador;
        this.numeroOrden = numeroOrden;
        this.numeroHoja = numeroHoja;
        this.separador = separador;
        this.obligatorio = obligatorio;
        this.estado = estado;
        this.tipoProceso = tipoProceso;
        this.nombreTabla = nombreTabla;
        this.delimitadorData = delimitadorData;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idConfiguradorTrama == null) ? 0 : idConfiguradorTrama.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConfiguracionTrama other = (ConfiguracionTrama) obj;
        if (idConfiguradorTrama == null) {
            if (other.idConfiguradorTrama != null) {
                return false;
            }
        } else if (!idConfiguradorTrama.equals(other.idConfiguradorTrama)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConfiguracionTrama [idConfiguradorTrama=" + idConfiguradorTrama + "]";
    }
   
}