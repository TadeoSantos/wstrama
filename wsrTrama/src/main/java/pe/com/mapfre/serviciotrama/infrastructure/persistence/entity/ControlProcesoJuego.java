package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ControlProcesoJuego.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_PROC_JUEGO", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ControlProcesoJuego implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id control proceso juego. */
    @Id
    @Column(name = "N_ID_PROC_JUE" , length = 32)
    private String idControlProcesoJuego;
   
    /** El control proceso. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUM_PROC", referencedColumnName = "NUM_PROC")
    private ControlProceso controlProceso;
   
    /** El juego trama. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CONFIG_JUEGO_TRAMA", referencedColumnName = "N_ID_CONFIG_JUEGO_TRAMA")
    private JuegoTrama juegoTrama;
   
    /** El codigo usuario. */
    @Column(name = "COD_USR" , length = 50)
    private String codigoUsuario;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_ACTU")
    private Date fechaActualizacion;
   
    /**
     * Instancia un nuevo control proceso juego.
     */
    public ControlProcesoJuego() {
    }
   
   
    /**
     * Instancia un nuevo control proceso juego.
     *
     * @param idControlProcesoJuego el id control proceso juego
     * @param controlProceso el control proceso
     * @param juegoTrama el juego trama
     * @param codigoUsuario el codigo usuario
     * @param fechaActualizacion el fecha actualizacion
     */
    public ControlProcesoJuego(String idControlProcesoJuego, ControlProceso controlProceso,JuegoTrama juegoTrama,String codigoUsuario, Date fechaActualizacion ) {
        super();
        this.idControlProcesoJuego = idControlProcesoJuego;
        this.controlProceso = controlProceso;
        this.juegoTrama = juegoTrama;
        this.codigoUsuario = codigoUsuario;
        this.fechaActualizacion = fechaActualizacion;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idControlProcesoJuego == null) ? 0 : idControlProcesoJuego.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ControlProcesoJuego other = (ControlProcesoJuego) obj;
        if (idControlProcesoJuego == null) {
            if (other.idControlProcesoJuego != null) {
                return false;
            }
        } else if (!idControlProcesoJuego.equals(other.idControlProcesoJuego)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ControlProcesoJuego [idControlProcesoJuego=" + idControlProcesoJuego + "]";
    }
   
}