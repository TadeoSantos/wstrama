package pe.com.mapfre.serviciotrama.domain.service.configuracion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
import pe.com.mapfre.serviciotrama.domain.service.IConfiguracionService;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ResourceUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.CanalCombosDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.JuegoCombosDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ProductoCombosDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.TramaCombosDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.SelectItemVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoArchivoProcesarType;

/**
 * La Class CamoteMantenedorServiceImpl.
 * <ul>
 * <li>Copyright 2021 MAPFRE - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft
 * @version 1.0, Mon Sep 06 16:48:51 COT 2021
 * @since MYTRON 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ConfiguracionTramaServiceImpl implements IConfiguracionService {

	@EJB
	private CanalCombosDaoLocal canalCombosDaoLocal;

	@EJB
	private ProductoCombosDaoLocal productoDaoLocal;

	@EJB
	private JuegoCombosDaoLocal juegoDaoLocal;

	@EJB
	private TramaCombosDaoLocal tramaDaoLocal;
	
	 private Map<Object,String> tipoArchivoProcesarMap = new HashMap<>();

	@Override
	public List<ListaItemsVO> listarItems(ListaItemsVO filtro) {
		List<ListaItemsVO> resultado = new ArrayList<>();
		for (String codigo : filtro.getListaCodigo()) {
			if ("canal".equalsIgnoreCase(codigo)) {
				filtro.setCodigo(codigo);
				resultado.add(canalCombosDaoLocal.listar(filtro));
			} else if ("producto".equalsIgnoreCase(codigo)) {
				filtro.setCodigo(codigo);
				resultado.add(productoDaoLocal.listar(filtro));
			} else if ("juego".equalsIgnoreCase(codigo)) {
				filtro.setCodigo(codigo);
				resultado.add(juegoDaoLocal.listar(filtro));
			} else if ("trama".equalsIgnoreCase(codigo)) {
				filtro.setCodigo(codigo);
				resultado.add(completarTipoCampoTrama(tramaDaoLocal.listar(filtro)));
			}
		}
		return resultado;
	}

	@Override
	public int contarItems(ListaItemsVO filtro) {
		int resultado = 0;
		String codigo = filtro.getCodigo();

		if ("canal".equalsIgnoreCase(codigo)) {
			filtro.setCodigo(codigo);
			resultado = canalCombosDaoLocal.contar(filtro);
		} else if ("producto".equalsIgnoreCase(codigo)) {
			filtro.setCodigo(codigo);
			resultado = productoDaoLocal.contar(filtro);
		} else if ("juego".equalsIgnoreCase(codigo)) {
			filtro.setCodigo(codigo);
			resultado = juegoDaoLocal.contar(filtro);
		} else if ("trama".equalsIgnoreCase(codigo)) {
			filtro.setCodigo(codigo);
			resultado = tramaDaoLocal.contar(filtro);
		}
		return resultado;
	}
	
	private ListaItemsVO completarTipoCampoTrama (ListaItemsVO filtro){
		 for (TipoArchivoProcesarType objType : TipoArchivoProcesarType.values()) {
			 tipoArchivoProcesarMap.put(objType.getKey(), getDescription(null,objType.getValue()));
		}
		if (filtro.getItems() != null) {
			for (SelectItemVO Items : filtro.getItems()) {
				Items.setDescripcion(tipoArchivoProcesarMap.get(Items.getDescripcion()));
			}
		}
		return filtro;
	}
	public String getDescription(Locale locale,String value) {
		return ResourceUtil.getString(locale, ConstanteTramaUtil.BUNDLE_NAME_CONFIGURADOR_TRAMA_TYPE, value);
}
	

}