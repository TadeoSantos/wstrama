package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ProcesoVariableInstancia.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_VAR_ORQ", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ProcesoVariableInstancia extends BasePaginator implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El codigo variable instancia. */
    @Id
    @Column(name = "COD_VAR_INSTANCE" , length = 32)
    private String codigoVariableInstancia;
   
    /** El control proceso. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "NUM_PROC", referencedColumnName = "NUM_PROC")
    private ControlProceso controlProceso;
   
    /** El codigo actividad. */
    @Column(name = "COD_ACT_INTER" , length = 18)
    private Long codigoActividadInterno;
   
    /** El agrupador instancia. */
    @Column(name = "C_GRUPO_INSTANCE" , length = 50)
    private String agrupadorInstancia;
   
    /** El key. */
    @Column(name = "C_KEY" , length = 100)
    private String key;
   
    /** El valor. */
    @Column(name = "C_VALOR" , length = 100)
    private String valor;
   
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
   
    /**
     * Instancia un nuevo proceso variable instancia.
     */
    public ProcesoVariableInstancia() {
    }
   
   
    /**
     * Instancia un nuevo proceso variable instancia.
     *
     * @param codigoVariableInstancia el codigo variable instancia
     * @param ControlProceso el control proceso
     * @param codigoActividad el codigo actividad
     * @param agrupadorInstancia el agrupador instancia
     * @param key el key
     * @param valor el valor
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public ProcesoVariableInstancia(String codigoVariableInstancia, ControlProceso controlProceso ,Long codigoActividadInterno, String agrupadorInstancia, String key, String valor, String usuarioCrea, Date fechaActualizacion ) {
        super();
        this.codigoVariableInstancia = codigoVariableInstancia;
        this.controlProceso = controlProceso;
        this.codigoActividadInterno = codigoActividadInterno;
        this.agrupadorInstancia = agrupadorInstancia;
        this.key = key;
        this.valor = valor;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((codigoVariableInstancia == null) ? 0 : codigoVariableInstancia.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProcesoVariableInstancia other = (ProcesoVariableInstancia) obj;
        if (codigoVariableInstancia == null) {
            if (other.codigoVariableInstancia != null) {
                return false;
            }
        } else if (!codigoVariableInstancia.equals(other.codigoVariableInstancia)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ProcesoVariableInstancia [codigoVariableInstancia=" + codigoVariableInstancia + "]";
    }
   
}