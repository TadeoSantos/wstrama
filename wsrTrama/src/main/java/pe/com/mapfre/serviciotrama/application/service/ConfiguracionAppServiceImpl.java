package pe.com.mapfre.serviciotrama.application.service;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
import pe.com.mapfre.serviciotrama.domain.service.IConfiguracionService;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.remote.Referencia;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.AtributosEntityCacheUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CollectionUtil;

/**
 * La Class ConfiguracionAppServiceImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ConfiguracionAppServiceImpl implements IConfiguracionAppService {

	private String nombreModulo;

	private final Logger logger = Logger.getLogger(ConfiguracionAppServiceImpl.class);
	
	public ConfiguracionAppServiceImpl() {
		AtributosEntityCacheUtil.getInstance()
				.sincronizarAtributos("pe.com.mapfre.serviciotrama.domain.entity.vo");
	}

	protected IConfiguracionService obtenerRemoto(String grupo) {
		if (nombreModulo == null) {
			nombreModulo = this.getClass().getClassLoader().toString().replace("ModuleClassLoader for Module \"deployment.", "").replace(".war:main\" from Service Module Loader", "");
		}
		return (IConfiguracionService) Referencia
				.getReference(nombreModulo + "/Configuracion" + grupo + "ServiceImpl");

	}

	@Override
	public List<ListaItemsVO> listarItems(ListaItemsVO filtro) {
		filtro.getListaCodigo().add("juego");
		filtro.getListaCodigo().add("trama");
		List<ListaItemsVO> resultado = new ArrayList<>();
		filtro.getListaGrupo().add("Trama");
		for (String grupo : filtro.getListaGrupo()) {
			IConfiguracionService servicio = obtenerRemoto(grupo);
			if (servicio != null) {
				resultado.addAll(servicio.listarItems(filtro));
			}
		}
		return resultado;
	}

	@Override
	public int contarItems(ListaItemsVO filtro) {
		if (!CollectionUtil.isEmpty(filtro.getListaGrupo())) {
			IConfiguracionService servicio = obtenerRemoto(filtro.getListaGrupo().get(0));
			return servicio.contarItems(filtro);
		}
		return 0;
	}
}