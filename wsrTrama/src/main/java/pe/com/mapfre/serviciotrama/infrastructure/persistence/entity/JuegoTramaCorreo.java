package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class JuegoTramaCorreo.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_CORREO_JUEGO_TRA", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class JuegoTramaCorreo implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id correo juego trama. */
    @Id
    @Column(name = "N_ID_CORREO_JUEGO_TRA" , length = 18)
    private Long idCorreoJuegoTrama;
   
    /** El correo. */
    @Column(name = "C_CORREO" , length = 150)
    private String correo;
   
    /** El juego trama. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CONFIG_JUEGO_TRAMA", referencedColumnName = "N_ID_CONFIG_JUEGO_TRAMA")
    private JuegoTrama juegoTrama;
   
    /** El codigo ramo. */
    @Column(name = "C_COD_RAMO" , length = 18)
    private Long codigoRamo;
   
    /** El codigo poliza grupo. */
    @Column(name = "C_POLIZ_GRUPO" , length = 12)
    private String codigoPolizaGrupo;
   
    /** El destinatario. */
    @Column(name = "C_DESTINATARIO" , length = 150)
    private String destinatario;
   
    /** El cargo. */
    @Column(name = "C_CARGO" , length = 150)
    private String cargo;
   
    /** El estado. */
    @Column(name = "C_ESTADO" , length = 1)
    private String estado;
   
    /** El codigoUsuario. */
    @Column(name = "C_CODUSU" , length = 50)
    private String codigoUsuario;
   
    /** El fecha actualizacion. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "D_FECACT")
	private Date fechaActualizacion;
    
    /** El mantenimiento destinatario. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_DESTI", referencedColumnName = "N_ID_DESTI")
    private MantenimientoDestinatario mantenimientoDestinatario;
   
    /**
     * Instancia un nuevo juego trama correo.
     */
    public JuegoTramaCorreo() {
    }
   
   
    /**
     * Instancia un nuevo juego trama correo.
     *
     * @param idCorreoJuegoTrama el id correo juego trama
     * @param correo el correo
     * @param juegoTrama el juego trama
     * @param codigoRamo el codigo ramo
     * @param codigoPolizaGrupo el codigo poliza grupo
     * @param destinatario el destinatario
     * @param cargo el cargo
     * @param estado el estado
     * @param codigoUsuario el codigo usuario
     * @param fechaActualizacion el fecha actualizacion
     * @param mantenimientoDestinatario el mantenimiento destinatario
     */
    public JuegoTramaCorreo(Long idCorreoJuegoTrama, String correo, JuegoTrama juegoTrama,Long codigoRamo, String codigoPolizaGrupo, String destinatario, String cargo, String estado, String codigoUsuario, Date fechaActualizacion, MantenimientoDestinatario mantenimientoDestinatario) {
        super();
        this.idCorreoJuegoTrama = idCorreoJuegoTrama;
        this.correo = correo;
        this.juegoTrama = juegoTrama;
        this.codigoRamo = codigoRamo;
        this.codigoPolizaGrupo = codigoPolizaGrupo;
        this.destinatario = destinatario;
        this.cargo = cargo;
        this.estado = estado;
        this.codigoUsuario = codigoUsuario;
        this.fechaActualizacion = fechaActualizacion;
        this.mantenimientoDestinatario = mantenimientoDestinatario;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idCorreoJuegoTrama == null) ? 0 : idCorreoJuegoTrama.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        JuegoTramaCorreo other = (JuegoTramaCorreo) obj;
        if (idCorreoJuegoTrama == null) {
            if (other.idCorreoJuegoTrama != null) {
                return false;
            }
        } else if (!idCorreoJuegoTrama.equals(other.idCorreoJuegoTrama)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "JuegoTramaCorreo [idCorreoJuegoTrama=" + idCorreoJuegoTrama + "]";
    }
   
}
