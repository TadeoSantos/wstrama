package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class JuegoTrama.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_CONFIG_JUEGO_TRA", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class JuegoTrama implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id juego trama. */
    @Id
    @Column(name = "N_ID_CONFIG_JUEGO_TRAMA" , length = 18)
    private Long idJuegoTrama;
   
    /** El nombre. */
    @Column(name = "C_NOMBRE" , length = 50)
    private String nombre;
   
    /** El canal. */
    @Column(name = "C_CANAL" , length = 50)
    private String canal;
   
    /** El producto. */
    @Column(name = "C_PRODUCTO" , length = 50)
    private String producto;
   
    /** El nombre funcion. */
    @Column(name = "C_NOM_FUNCION")
    private String nombreFuncion;
   
    /** El sql reporte. */
    @Column(name = "C_SQL_REPORTE")
    private String sqlReporte;
    
    /** El sql reporte interno. */
    @Column(name = "C_SQL_REPORTE_ERROR_INT")
    private String sqlReporteErrorInterno;
   
    /** El sql reporte externo. */
    @Column(name = "C_SQL_REPORTE_ERROR_EXT")
    private String sqlReporteErrorExterno;
    
    /** El estado. */
    @Column(name = "C_ESTADO" , length = 1)
    private String estado;
    
    /** El estado. */
    @Column(name = "C_ESTADO_DEMONIO" , length = 1)
    private String estadoProceso;
   
    /** El copiar destino. */
    @Column(name = "C_COPIAR_DESTINO" , length = 1)
    private Character copiarDestino;
   
    /** El resultado ruta origen. */
    @Column(name = "C_RESULT_RUT_ORIG" , length = 1)
    private Character resultadoRutaOrigen;
    
    /** El tipo proceso. */
    @Column(name = "C_TIPO_PROCESO" , length = 2)
    private String tipoProceso;
    
    /** El proceso flujo. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_PROC_FLU", referencedColumnName = "N_ID_PROC_FLU")
    private ProcesoFlujo procesoFlujo;
 
    /** El juego trama juego trama correo list. */
    @OneToMany(mappedBy = "juegoTrama", fetch = FetchType.LAZY)
    private List<JuegoTramaCorreo> juegoTramaJuegoTramaCorreoList = new ArrayList<JuegoTramaCorreo>();
    
    /** El juego trama configuracion trama list. */
    @OneToMany(mappedBy = "juegoTrama", fetch = FetchType.LAZY)
    private List<ConfiguracionTrama> juegoTramaConfiguracionTramaList = new ArrayList<ConfiguracionTrama>();
    
    /** El juego trama configuracion ftp trama list. */
    @OneToMany(mappedBy = "juegoTrama", fetch = FetchType.LAZY)
    private List<ConfiguracionFtpTrama> juegoTramaConfiguracionFtpTramaList = new ArrayList<ConfiguracionFtpTrama>();
    //requerimiento cobranza 19/10/2017
    /** El sql reporte externo. */
    @Column(name = "C_SQL_REPORTE_RESUMEN")
    private String sqlReporteResumen;
    //requerimiento cobranza 19/10/2017
    
    //requerimiento admistrar ejecuciones automaticas
    @Column(name = "C_EXCLUSION", length = 1)
    private String exclusion;
    
    //requerimiento nuevo exportar excel
    @Column(name = "N_ID_NOMER_PADRE", length = 20)
    private Long idTramaNomenclaturaArchivoPadre;
    //requerimiento nuevo exportar excel
    
    //Inicio Buildsoft Agregar nuevo campo 01/04/19
    @Column(name = "C_SQL_PROCESO")
    private String sqlProceso;
    //Fin Buildsoft Agregar nuevo campo 01/04/19
    
    //Inicio BuildSoft particionar trama
    @Column(name = "C_FLAG_PARTICIONA_TRAMA", length = 1)
    private String flagPartionaTrama;
    
    @Column(name = "N_CANTIDAD_PARTICIONAR" , length = 18)
    private Long cantidadParticionar;
    //Fin BuidSoft particionar trama
    
    /**
     * Instancia un nuevo juego trama.
     */
    public JuegoTrama() {
    }
   
   
    /**
     * Instancia un nuevo juego trama.
     *
     * @param idJuegoTrama el id juego trama
     * @param nombre el nombre
     * @param canal el canal
     * @param producto el producto
     * @param nombreFuncion el nombre funcion
     * @param sqlReporte el sql reporte
     * @param estado el estado
     * @param estadoProceso the estado proceso
     * @param copiarDestino el copiar destino
     * @param resultadoRutaOrigen el resultado ruta origen
     */
    public JuegoTrama(Long idJuegoTrama, String nombre, String canal, String producto, String nombreFuncion, String sqlReporte, String estado,String estadoProceso, Character copiarDestino, Character resultadoRutaOrigen ) {
        super();
        this.idJuegoTrama = idJuegoTrama;
        this.nombre = nombre;
        this.canal = canal;
        this.producto = producto;
        this.nombreFuncion = nombreFuncion;
        this.sqlReporte = sqlReporte;
        this.estado = estado;
        this.estadoProceso = estadoProceso;        
        this.copiarDestino = copiarDestino;
        this.resultadoRutaOrigen = resultadoRutaOrigen;
    }
   
	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idJuegoTrama == null) ? 0 : idJuegoTrama.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        JuegoTrama other = (JuegoTrama) obj;
        if (idJuegoTrama == null) {
            if (other.idJuegoTrama != null) {
                return false;
            }
        } else if (!idJuegoTrama.equals(other.idJuegoTrama)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "JuegoTrama [idJuegoTrama=" + idJuegoTrama + "]";
    }
   
}