package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import pe.com.mapfre.serviciotrama.infrastructure.vertical.interfaces.IGenericJDBC;

/**
 * La Class IGenericRepository.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public interface IGenericRepository<K,T> extends IGenericJDBC {

	T saveNative(T entity);
	
	T updateNative(T entity);
    /**
     * Save.
     *
     * @param entity el entity
     * @return the t
     */
    T save(T entity);

    /**
     * Update.
     *
     * @param entity el entity
     * @return the t
     */
    T update(T entity);

    /**
     * Delete.
     *
     * @param entity el entity
     * @return the t
     */
    T delete(T entity);

    
    /**
     * Find.
     *
     * @param classs el classs
     * @param id el id
     * @return the t
     */
    T find(Class<T> classs, K id);
}
