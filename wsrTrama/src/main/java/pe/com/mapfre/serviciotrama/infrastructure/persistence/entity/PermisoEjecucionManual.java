package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class PermisoEjecucionManual.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */	
@Getter
@Setter
@Entity
@Table(name = "SGSM_PERMISO_EJEC_MANUAL", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class PermisoEjecucionManual implements Serializable {
 
    /** La Constant serialVersionUID. */	
    private static final long serialVersionUID = 1L;
   
    /** El id permiso. */
    @Id
    @Column(name = "N_ID_PERMISO" , length = 18)
    private Long idPermiso;
   
    /** El usuario. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_USUARIO", referencedColumnName = "N_ID_USUARIO")
    private UsuarioPermiso usuarioPermiso;
   
   
    /** El estado. */
    @Column(name = "C_ESTADO" , length = 50)
    private String estado;
   

    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
    
    @Column(name = "VISUALIZAR_EJEC_AUT" , length = 1)
    private String visualizarEjecucionesAutomaticas;
    
    /** El permiso detalle permiso ejecucion manual detalle list. */
    @OneToMany(mappedBy = "permisoEjecucionManual", fetch = FetchType.LAZY)
    private List<PermisoEjecucionManualDetalle> permisoDetallePermisoEjecucionManualDetalleList = new ArrayList<PermisoEjecucionManualDetalle>();
    
    /**
     * Instancia un nuevo permiso ejecucion manual.
     */
    public PermisoEjecucionManual() {
    }
   
    /**
     * Instancia un nuevo permiso ejecucion manual.
     *
     * @param idPermiso el id permiso
     * @param usuarioPermiso el usuario permiso
     * @param estado el estado
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public PermisoEjecucionManual(Long idPermiso, UsuarioPermiso usuarioPermiso, String estado, String usuarioCrea, Date fechaActualizacion) 
    {
        super();
        this.idPermiso = idPermiso;
        this.usuarioPermiso = usuarioPermiso;
        this.estado = estado;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
    }
 
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idPermiso == null) ? 0 : idPermiso.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PermisoEjecucionManual other = (PermisoEjecucionManual) obj;
        if (idPermiso == null) {
            if (other.idPermiso != null) {
                return false;
            }
        } else if (!idPermiso.equals(other.idPermiso)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PermisoEjecucionManual [idPermiso=" + idPermiso + "]";
    }
   
}