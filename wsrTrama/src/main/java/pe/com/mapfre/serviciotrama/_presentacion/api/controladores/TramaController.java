package pe.com.mapfre.serviciotrama._presentacion.api.controladores;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import io.swagger.annotations.Api;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import pe.com.mapfre.serviciotrama._presentacion.api.entity.vo.RespuestaWSVO;
import pe.com.mapfre.serviciotrama.application.service.ITramaAppService;
import pe.com.mapfre.serviciotrama.domain.entity.vo.TramaVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.UUIDUtil;


@Stateless
@Api(value = "/trama", tags = { "Proceso Tramas" })
@SwaggerDefinition(tags = { @Tag(name = "Proceso Tramas", description = "Proceso Tramas") })
@Path("/trama")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TramaController extends GenericServiceRestImpl {

	private final Logger logger = Logger.getLogger(this.getClass());
	@EJB
	private ITramaAppService servicioApp;
	
    @POST
	public Response procesar(TramaVO tramavo) {
		RespuestaWSVO<String> resultado = new RespuestaWSVO<>();
		resultado.setCodigoError("");
		resultado.setError(false);
		resultado.setMensajeError("");
		if(tramavo.getUserName()==null) {
			return respuesta(resultado,null);
		}
		try {
//			parametroMap.put(ConstanteConfiguracionTramaUtil.ES_SIMULACION, false);
			resultado.setObjetoResultado(servicioApp.procesarConfiguracionTrama(tramavo));
		} catch (Exception e) {
			logger.error("procesarConfiguracionTrama ", e);
			resultado.setError(true);
			resultado.setCodigoError("ws_001");
			resultado.setMensajeError("Error al procesarConfiguracionTrama --> " + e.getMessage());
		}
		return respuesta(resultado,null);
	}
	
    @GET
    @Path("/ping")
    public Response ping(@Context HttpHeaders httpHeaders) {
    	RespuestaWSVO<String> resultado = new RespuestaWSVO<>();
    	String uuid = UUIDUtil.generarElementUUID();
		logger.error("inicio.apiSctrController.ping." + uuid + " " + FechaUtil.obtenerFechaActual() );
		resultado.setCodigoError("0");
		resultado.setMensajeError("Proceso Ejecuto ok");
    	try {
    		resultado.setObjetoResultado(FechaUtil.obtenerFechaActual() + "");
    	} catch (Exception e) {
    		logger.error("error.rest.ping." + uuid, e);
    		resultado.setCodigoError("99");
    		resultado.setMensajeError("Error general ping");
    	}
		logger.error("fin.apiSctrController.ping." + uuid + " " + FechaUtil.obtenerFechaActual() );
		return respuesta(resultado,null);
    }

}