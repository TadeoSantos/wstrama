package pe.com.mapfre.serviciotrama.domain.service;

import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.apache.log4j.Logger;

import pe.com.mapfre.serviciotrama.domain.entity.vo.TramaVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.SerializationUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms.ConfiguracionJMSUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms.JMSSender;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProceso;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.EstadoProcesoType;

/**
 * La Class TramaServiceImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class TramaServiceImpl implements ITramaService {
	
	@EJB
	private ControlProcesoAutomaticoDaoLocal controlProcesoDao;

	/** La log. */
	private final Logger logger = Logger.getLogger(this.getClass());
	
	private String crearControlProceso(TramaVO trama, boolean isCaledarizacion) throws Exception {
		String resultado = controlProcesoDao.generarUUID();
		
		ControlProceso controlProceso = new ControlProceso();
		controlProceso.setIdControlProceso(resultado);
//		controlProceso.setIdControlProceso("202001100001");
		controlProceso.setIdTipoProceso((Long) trama.getIdProcesoFlujo());// procesoFlujo
		// ConstanteTramaUtil.MODO_EJECUCION)
		controlProceso.setIdModoProceso(trama.getIndicadorInternoActividad());// ver variable indicadorInternoActividad
		controlProceso.setIdOrigenProceso(trama.getOrigen());// ver
		
		controlProceso.setUsuarioProceso(trama.getUserName());
		controlProceso.setFechaInicio(FechaUtil.obtenerFechaActual());
		controlProceso.setFechaFin(null);
		controlProceso.setEstado(EstadoProcesoType.EN_SOLICITUD.getKey()); // TODO SE CAMBIO EL NOMBRE A En Solicitud A
																			// UN NO PASA A LA COLA
		controlProceso.setCodigoUsuario(trama.getUserName());
		controlProceso.setFechaActualizacion(new Date());
		
		if (isCaledarizacion) {
			trama.setIdControlProceso(resultado);
			controlProceso.setDataTrama(SerializationUtil.toString(trama));
		}
		
		controlProcesoDao.saveNative(controlProceso);
		return resultado;
	}

	@Override
	public String procesarConfiguracionTrama(TramaVO trama) throws Exception {
		if ("S".equalsIgnoreCase(trama.getEjecucionCalendarizacion())) {
	       JMSSender.sendMessage(trama, ConfiguracionJMSUtil.QCF_TRAMA_CONTROL_NAME, ConfiguracionJMSUtil.QUEUE_TRAMA_CONTROL_NAME);
		} else{
//			boolean isCaledarizacion = caledarizacionDAo.existeCalendarizacion(trama.getidJuego());
			boolean isCaledarizacion = false;
			String idControlProceso = crearControlProceso(trama, isCaledarizacion);
			trama.setIdControlProceso(idControlProceso);
			if (!isCaledarizacion) {
				 JMSSender.sendMessage(trama, ConfiguracionJMSUtil.QCF_TRAMA_CONTROL_NAME, ConfiguracionJMSUtil.QUEUE_TRAMA_CONTROL_NAME);
			}
		}
		return "";
	}

}
