package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ProductoCombosDaoLocal;

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ProductoCombosDaoImpl extends GenericRepository<String, ListaItemsVO> implements ProductoCombosDaoLocal {
	
	@Override
	public ListaItemsVO listar(ListaItemsVO filtro) {
		return getListaListaItemsVO(filtro.getCodigo(), generarQuery(filtro, false), filtro, true);
	}

	private Query generarQuery(ListaItemsVO filtro, boolean esContador) {
		Map<String, Object> parametros = new HashMap<>();
		StringBuilder jpaql = new StringBuilder();
		if (esContador) {
			jpaql.append(" SELECT COUNT(1) FROM tron2000.targen49 where 1=1 ");
		} else {
			jpaql.append(" SELECT cod_item AS cod_producto, desc_item AS nom_producto  FROM tron2000.targen49 where 1=1 ");
		}
		jpaql.append("and cod_rubro = 515 ");
		if (!esContador) {
			jpaql.append(" ORDER BY cod_producto");
		}
		return createNativeQuery(jpaql.toString(), parametros);
	}

	@Override
	public int contar(ListaItemsVO filtro) {
		return getContador(generarQuery(filtro, true));
	}
	
}