package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;

import oracle.jdbc.internal.OracleTypes;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import oracle.sql.STRUCT;
import oracle.sql.StructDescriptor;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.ScriptSqlResulJDBCVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.interfaces.IGenericJDBC;

/**
 * La Class GenericJDBC.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */

public class GenericJDBC extends BaseTransfer implements IGenericJDBC {

	/** La log. */
	private static Logger log = Logger.getLogger(GenericJDBC.class);

	/**
	 * Execute query.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the t
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public <T> T executeQuery(StringBuilder sql, Map<String, Object> parametros, Class<T> entityClassEntity)
			throws Exception {
		return toEntityVO(ejecutarScriptSql(sql, parametros, null, true, false), entityClassEntity);
	}

	@Override
	public <T> T executeQueryPreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			Class<T> entityClassEntity) throws Exception {
		return toEntityVO(ejecutarScriptSqlPreparedStatement(sql, parametros, null, true, false), entityClassEntity);
	}

	protected StringBuilder definirParametroCall(String spEjecutar, int cantidad) throws Exception {
		StringBuilder resultado = new StringBuilder();
		resultado.append("{call " + spEjecutar + "(");
		int size = cantidad;
		for (int i = 1; i <= size; i++) {
			resultado.append("?");
			if (!(i == size)) {
				resultado.append(",");
			}
		}
		resultado.append(")} ");
		return resultado;
	}

	/**
	 * Execute query.
	 *
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param isProcedure
	 *            el is procedure
	 * @param parametroOutType
	 *            el parametro out type
	 * @param parametroInType
	 *            el parametro in type
	 * @return the script sql resul jdbcvo
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public ScriptSqlResulJDBCVO executeQuerySP(String sp, String jndiConexion, List<Integer> parametroOutType,
			List<Object> parametroInType, Map<String, Object> parametrosType) throws Exception {
		return ejecutarScriptSqlSP(definirParametroCall(sp, parametroOutType.size() + parametroInType.size()),
				jndiConexion, parametroOutType, parametroInType, parametrosType);
	}

	@Override
	public ScriptSqlResulJDBCVO executeQuerySP(StringBuilder sp) throws Exception {
		List<Integer> parametroType = new ArrayList<Integer>();
		parametroType.add(java.sql.Types.VARCHAR);
		return ejecutarScriptSqlSP(sp, null, parametroType, new ArrayList<Object>());
	}

	/**
	 * Execute query.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param jndiConexion
	 *            el JNDI conexion
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the t
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public <T> T executeQuery(StringBuilder sql, Map<String, Object> parametros, String jndiConexion,
			Class<T> entityClassEntity) throws Exception {
		return toEntityVO(ejecutarScriptSql(sql, parametros, jndiConexion, true, false), entityClassEntity);
	}

	@Override
	public <T> T executeQueryPreparedStatement(StringBuilder sql, Map<String, Object> parametros, String jndiConexion,
			Class<T> entityClassEntity) throws Exception {
		return toEntityVO(ejecutarScriptSqlPreparedStatement(sql, parametros, jndiConexion, true, false),
				entityClassEntity);
	}

	/**
	 * Execute query list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public <T> List<T> executeQueryList(StringBuilder sql, Map<String, Object> parametros, Class<T> entityClassEntity)
			throws Exception {
		return toEntityListVO(ejecutarScriptSql(sql, parametros, null, true, false), entityClassEntity);
	}

	@Override
	public <T> List<T> executeQueryListPreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			Class<T> entityClassEntity) throws Exception {
		return toEntityListVO(ejecutarScriptSqlPreparedStatement(sql, parametros, null, true, false),
				entityClassEntity);
	}

	/**
	 * Execute query list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param entityClassEntity
	 *            el entity class entity
	 * @param formatoMap
	 *            el formato map
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public <T> List<T> executeQueryList(StringBuilder sql, Map<String, Object> parametros, Class<T> entityClassEntity,
			Map<String, String> formatoMap) throws Exception {
		return toEntityListVO(ejecutarScriptSql(sql, parametros, null, true, false), entityClassEntity, formatoMap);
	}

	public <T> List<T> executeQueryListPreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			Class<T> entityClassEntity, Map<String, String> formatoMap) throws Exception {
		return toEntityListVO(ejecutarScriptSqlPreparedStatement(sql, parametros, null, true, false), entityClassEntity,
				formatoMap);
	}

	/**
	 * Execute query list.
	 *
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public List<Object[]> executeQueryList(StringBuilder sql, Map<String, Object> parametros) throws Exception {
		return ejecutarScriptSql(sql, parametros, null, true, true).getListaDataObject();
	}

	@Override
	public List<Object[]> executeQueryListPreparedStatement(StringBuilder sql, Map<String, Object> parametros)
			throws Exception {
		return ejecutarScriptSqlPreparedStatement(sql, parametros, null, true, true).getListaDataObject();
	}

	/**
	 * Execute query list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param jndiConexion
	 *            el JNDI conexion
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public <T> List<T> executeQueryList(StringBuilder sql, Map<String, Object> parametros, String jndiConexion,
			Class<T> entityClassEntity) throws Exception {
		return toEntityListVO(ejecutarScriptSql(sql, parametros, jndiConexion, true, false), entityClassEntity);
	}

	@Override
	public <T> List<T> executeQueryListPreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			String jndiConexion, Class<T> entityClassEntity) throws Exception {
		return toEntityListVO(ejecutarScriptSqlPreparedStatement(sql, parametros, jndiConexion, true, false),
				entityClassEntity);
	}

	/**
	 * Execute query.
	 *
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @return the script sql resul jdbcvo
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public ScriptSqlResulJDBCVO executeQuery(StringBuilder sql, Map<String, Object> parametros) throws Exception {
		return ejecutarScriptSql(sql, parametros, null, true, false);
	}

	@Override
	public ScriptSqlResulJDBCVO executeQueryPreparedStatement(StringBuilder sql, Map<String, Object> parametros)
			throws Exception {
		return ejecutarScriptSqlPreparedStatement(sql, parametros, null, true, false);
	}

	/**
	 * Execute query.
	 *
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param jndiConexion
	 *            el JNDI conexion
	 * @return the script sql resul jdbcvo
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public ScriptSqlResulJDBCVO executeQuery(StringBuilder sql, Map<String, Object> parametros, String jndiConexion)
			throws Exception {
		return ejecutarScriptSql(sql, parametros, jndiConexion, true, false);
	}

	@Override
	public ScriptSqlResulJDBCVO executeQueryPreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			String jndiConexion) throws Exception {
		return ejecutarScriptSqlPreparedStatement(sql, parametros, jndiConexion, true, false);
	}

	/**
	 * Execute update.
	 *
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @return the script sql resul jdbcvo
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public ScriptSqlResulJDBCVO executeUpdate(StringBuilder sql, Map<String, Object> parametros) throws Exception {
		return ejecutarScriptSql(sql, parametros, null, false, false);
	}

	@Override
	public ScriptSqlResulJDBCVO executeUpdatePreparedStatement(StringBuilder sql, Map<String, Object> parametros)
			throws Exception {
		return ejecutarScriptSqlPreparedStatement(sql, parametros, null, false, false);
	}

	/**
	 * Execute update.
	 *
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param jndiConexion
	 *            el JNDI conexion
	 * @return the script sql resul jdbcvo
	 * @throws Exception
	 *             the exception
	 */
	@Override
	public ScriptSqlResulJDBCVO executeUpdate(StringBuilder sql, Map<String, Object> parametros, String jndiConexion)
			throws Exception {
		return ejecutarScriptSql(sql, parametros, jndiConexion, false, false);
	}

	@Override
	public ScriptSqlResulJDBCVO executeUpdatePreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			String jndiConexion) throws Exception {
		return ejecutarScriptSqlPreparedStatement(sql, parametros, jndiConexion, false, false);
	}

	/**
	 * Ejecutar script sql.
	 *
	 * @param sql
	 *            el sql
	 * @param parametros
	 *            el parametros
	 * @param jndiConexion
	 *            el JNDI conexion
	 * @param isConsulta
	 *            el is consulta
	 * @param devolverOject
	 *            el devolver oject
	 * @param isProcedure
	 *            el is procedure
	 * @param parametroOutType
	 *            el parametro out type
	 * @return the script sql resul jdbcvo
	 * @throws Exception
	 *             the exception
	 */
	private ScriptSqlResulJDBCVO ejecutarScriptSql(StringBuilder sql, Map<String, Object> parametros,
			String jndiConexion, boolean isConsulta, boolean devolverOject) throws Exception {
		ScriptSqlResulJDBCVO resultado = new ScriptSqlResulJDBCVO();
		String jpaql = sql.toString();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		if (jndiConexion == null) {
			connection = getConexionDS();
		} else {
			connection = getConexionDS(jndiConexion);
		}
		if (parametros != null) {
			for (Map.Entry<String, Object> objParamMap : parametros.entrySet()) {
				if (objParamMap.getValue() != null) {
					if (objParamMap.getValue().getClass().isAssignableFrom(String.class)) {
						jpaql = jpaql.replaceAll(":" + objParamMap.getKey(),
								"'" + StringEscapeUtils.escapeSql((String) objParamMap.getValue()) + "'");
					} else if (objParamMap.getValue().getClass().isAssignableFrom(Character.class)) {
						jpaql = jpaql.replaceAll(":" + objParamMap.getKey(), "'" + objParamMap.getValue() + "'");
					} else if (objParamMap.getValue().getClass().isAssignableFrom(Date.class)) {
						String formato = "yyyyMMdd HH:mm:ss";
						String formatoBD = "yyyymmdd HH24:MI:SS";
						String dateValue = "to_date('"
								+ FechaUtil.obtenerFechaFormatoPersonalizado((Date) objParamMap.getValue(), formato)
								+ "','" + formatoBD + "')";
						jpaql = jpaql.replaceAll(":" + objParamMap.getKey(), dateValue);
					} else if (objParamMap.getValue().getClass().isAssignableFrom(ArrayList.class)) {
						jpaql = jpaql.replaceAll(":" + objParamMap.getKey(),
								objParamMap.getValue().toString().replace("[", "").replace("]", "").trim() + "");
					} else {
						jpaql = jpaql.replaceAll(":" + objParamMap.getKey(), objParamMap.getValue() + "");
					}

				} else {
					jpaql = jpaql.replaceAll(":" + objParamMap.getKey(), "''");
				}
			}
		}
		try {
			st = connection.prepareStatement(jpaql.toString());
			if (isConsulta) {
				log.info("jpaql " + jpaql);
				rs = st.executeQuery(jpaql.toString());
				resultado = generarListMap(rs, devolverOject);
			} else {
				log.info("jpaql " + jpaql);
				st.executeUpdate();
			}

		} catch (Exception e) {
			resultado.setTieneError(true);
			resultado.setMensajeError(e.getMessage() + " \n " + e.toString());
			throw e;
		} finally {
			if (st != null) {
				st.close();
			}
			if (rs != null) {
				rs.close();
			}
			if (connection != null) {
				connection.close();
			}

		}
		return resultado;
	}

	/*
	 * sql = "select * from mytabla where (fecha=? or fecha=?) and fechaFin<? "
	 * parametros.put("1","01/01/2010"); parametros.put("2","01/01/2011");
	 * parametros.put("3","01/01/2015");
	 */
	private ScriptSqlResulJDBCVO ejecutarScriptSqlPreparedStatement(StringBuilder sql, Map<String, Object> parametros,
			String jndiConexion, boolean isConsulta, boolean devolverOject) throws Exception {
		ScriptSqlResulJDBCVO resultado = new ScriptSqlResulJDBCVO();
		String jpaql = sql.toString();
		PreparedStatement pst = null;
		ResultSet rs = null;
		Connection connection = null;
		if (jndiConexion == null) {
			connection = getConexionDS();
		} else {
			connection = getConexionDS(jndiConexion);
		}
		try {
			pst = connection.prepareStatement(jpaql);
			if (parametros != null) {
				for (Map.Entry<String, Object> objParamMap : parametros.entrySet()) {

					if (objParamMap.getValue() != null) {
						if (objParamMap.getValue().getClass().isAssignableFrom(String.class)) {
							pst.setString(Integer.parseInt(objParamMap.getKey()),
									StringEscapeUtils.escapeSql((String) objParamMap.getValue()));
						} else if (objParamMap.getValue().getClass().isAssignableFrom(Character.class)) {
							pst.setString(Integer.parseInt(objParamMap.getKey()),
									StringEscapeUtils.escapeSql(objParamMap.getValue().toString()));
						} else if (objParamMap.getValue().getClass().isAssignableFrom(Date.class)) {
							java.sql.Timestamp dateTime = new Timestamp(((Date) objParamMap.getValue()).getTime());
							pst.setTimestamp(Integer.parseInt(objParamMap.getKey()), dateTime);
						} else if (objParamMap.getValue().getClass().isAssignableFrom(java.sql.Timestamp.class)) {
							java.sql.Timestamp dateTime = (java.sql.Timestamp) objParamMap.getValue();
							Calendar start = Calendar.getInstance();
							start.setTimeInMillis(dateTime.getTime());
							pst.setTimestamp(Integer.parseInt(objParamMap.getKey()), dateTime);
						} else if (objParamMap.getValue().getClass().isAssignableFrom(ArrayList.class)) {
							pst.setObject(Integer.parseInt(objParamMap.getKey()),
									objParamMap.getValue().toString().replace("[", "").replace("]", "").trim() + "");
						} else if (objParamMap.getValue().getClass().isAssignableFrom(Integer.class)) {
							pst.setInt(Integer.parseInt(objParamMap.getKey()), (Integer) objParamMap.getValue());
						} else if (objParamMap.getValue().getClass().isAssignableFrom(Long.class)) {
							pst.setLong(Integer.parseInt(objParamMap.getKey()), (Long) objParamMap.getValue());
						} else if (objParamMap.getValue().getClass().isAssignableFrom(Double.class)) {
							pst.setDouble(Integer.parseInt(objParamMap.getKey()), (Double) objParamMap.getValue());
						} else if (objParamMap.getValue().getClass().isAssignableFrom(Float.class)) {
							pst.setFloat(Integer.parseInt(objParamMap.getKey()), (Float) objParamMap.getValue());
						} else if (objParamMap.getValue().getClass().isAssignableFrom(BigDecimal.class)) {
							pst.setBigDecimal(Integer.parseInt(objParamMap.getKey()),
									(BigDecimal) objParamMap.getValue());
						} else {
							pst.setObject(Integer.parseInt(objParamMap.getKey()), objParamMap.getValue());
						}
					} else {
						pst.setObject(Integer.parseInt(objParamMap.getKey()), objParamMap.getValue());
					}
				}
			}
			if (isConsulta) {
				rs = pst.executeQuery();
				resultado = generarListMap(rs, devolverOject);
			} else {
				int dataProcesado = pst.executeUpdate();
				resultado.setExecuteUpdate(dataProcesado);
			}

		} catch (Exception e) {
			log.error("jpaql " + jpaql);
			log.error("error", e);
			resultado.setTieneError(true);
			resultado.setMensajeError(e.getMessage() + " \n " + e.toString());
		} finally {
			try {
				if (pst != null) {
					pst.close();
				}
				if (rs != null) {
					rs.close();
				}
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e2) {
				log.error("error", e2);
			}

		}

		return resultado;
	}

	private ScriptSqlResulJDBCVO ejecutarScriptSqlSP(StringBuilder sql, String JNDIConexion,
			List<Integer> parametroOutType, List<Object> parametroInType) throws Exception {
		ScriptSqlResulJDBCVO resultado = new ScriptSqlResulJDBCVO();
		String jpaql = sql.toString();
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		CallableStatement cst = null;
		if (JNDIConexion == null) {
			connection = getConexionDS();
		} else {
			connection = getConexionDS(JNDIConexion);
		}
		try {
			log.info("jpaql " + jpaql);
			log.info("parametroInType " + parametroInType);
			System.out.println("jpaql " + jpaql);
			System.out.println("parametroInType " + parametroInType);
			cst = connection.prepareCall(sql.toString());
			int index = 1;
			List<Integer> listaIndexOutPut = new ArrayList<Integer>();
			Map<Integer, Integer> indexOutPutMap = new HashMap<Integer, Integer>();
			for (Object objParamIn : parametroInType) {
				// Inicio BuildSoft Reporte Siniestro 01
				if (objParamIn != null) {
					if (objParamIn instanceof Boolean) {
						cst.setBoolean(index, (Boolean) objParamIn);
					} else if (objParamIn instanceof BigDecimal) {
						cst.setBigDecimal(index, (BigDecimal) objParamIn);
					} else if (objParamIn instanceof Long) {
						cst.setLong(index, (Long) objParamIn);
					} else if (objParamIn instanceof Integer) {
						cst.setInt(index, (Integer) objParamIn);
					} else if (objParamIn instanceof Date) {
						Date value = (Date) objParamIn;
						java.sql.Date jdbcDate = java.sql.Date.class.isInstance(value) ? (java.sql.Date) value
								: new java.sql.Date(value.getTime());
						cst.setDate(index, jdbcDate);
					} else if (objParamIn instanceof String) {
						cst.setString(index, StringEscapeUtils.escapeSql((String) objParamIn));
					} else if (objParamIn instanceof Character) {
						cst.setString(index, objParamIn + "");
					} else {
						cst.setObject(index, objParamIn);
					}
				} else {
					cst.setObject(index, objParamIn);
				}
				// Fin BuildSoft Reporte Siniestro 01
				index++;
			}
			for (Integer paramMap : parametroOutType) {
				cst.registerOutParameter(index, paramMap);
				listaIndexOutPut.add(index);
				indexOutPutMap.put(index, paramMap);
				index++;
			}
			cst.execute();
			if (parametroOutType.size() > 0) {
				int indexResul = 1;
				for (Integer indexOutPut : listaIndexOutPut) {
					if (OracleTypes.CURSOR == indexOutPutMap.get(indexOutPut)) {
						rs = (ResultSet) cst.getObject(indexOutPut);
						ScriptSqlResulJDBCVO resultadoCursor = generarListMap(rs, false);
						resultado.getResulCursorList().add(resultadoCursor);
					} else {
						resultado = generarResulSPMap(resultado, indexOutPut, indexResul, cst, listaIndexOutPut);
					}
					indexResul++;
				}
				log.info("parametroInType out" + resultado.getListaData());
				System.out.println("parametroInType out" + resultado.getListaData());
			}

		} catch (Exception e) {
			resultado.setTieneError(true);
			resultado.setMensajeError(e.getMessage() + " \n " + e.toString());
			log.error("error al call sp " + jpaql + " " + e.getMessage() + " \n " + e.toString());
			throw e;
		} finally {
			if (st != null) {
				st.close();
			}
			if (cst != null) {
				cst.close();
			}
			if (rs != null) {
				rs.close();
			}
			if (connection != null) {
				connection.close();
			}

		}
		return resultado;
	}

	private ScriptSqlResulJDBCVO generarListMap(ResultSet rs, boolean devolverOject) throws Exception {
		ScriptSqlResulJDBCVO resultado = new ScriptSqlResulJDBCVO();
		List<String> listaHeader = new ArrayList<>();
		List<Map<String, Object>> listaData = new ArrayList<>();
		List<Object[]> listaDataOject = new ArrayList<>();
		int i = 1;
		ResultSetMetaData metadata = rs.getMetaData();
		for (i = 0; i < metadata.getColumnCount(); i++) {
			listaHeader.add(metadata.getColumnLabel(i + 1));
		}
		while (rs.next()) {
			Map<String, Object> dataMap = new HashMap<>();
			for (i = 0; i < metadata.getColumnCount(); i++) {
				String nombreCampo = metadata.getColumnLabel(i + 1);
				Object value = null;
				if ("rownum".equalsIgnoreCase(nombreCampo)) {
					continue;
				}
				if ("IDU".equals(nombreCampo) || "ROWID".equals(nombreCampo)) {
					value = rs.getString(i + 1);
				} else {
					value = rs.getObject(i + 1);
				}
				value = value == null ? "" : value;
				dataMap.put(metadata.getColumnLabel(i + 1), value);
			}
			listaData.add(dataMap);
			if (devolverOject) {
				Object[] data = new Object[metadata.getColumnCount()];
				for (i = 0; i < metadata.getColumnCount(); i++) {
					Object value = rs.getObject(i + 1);
					data[i] = value == null ? "" : value;
				}
				listaDataOject.add(data);
			}
		}
		resultado.setListaDataObject(listaDataOject);
		resultado.setListaData(listaData);
		resultado.setListaHeader(listaHeader);
		return resultado;
	}

	private ScriptSqlResulJDBCVO generarResulSPMap(ScriptSqlResulJDBCVO resultado, Integer indexOutPut, int indexResul,
			CallableStatement cst, List<Integer> listaIndexOutPut) throws Exception {
		List<Map<String, Object>> listaData = resultado.getListaData();
		Map<String, Object> dataMap = new HashMap<>();
		Object resultadoSp = cst.getObject(indexOutPut);// pasear salidas
		dataMap.put("resultado" + indexResul, resultadoSp);
		listaData.add(dataMap);
		return resultado;
	}

	private boolean isTypeArray(List<Object> parametroInType, Map<String, Object> parametrosType) {
		boolean resultado = false;
		int index = 1;
		for (Object objParamIn : parametroInType) {
			if (objParamIn != null) {
				if (parametrosType.containsKey(index + "TBL")) {
					resultado = true;
					return resultado;

				}
			}
			index++;
		}
		return resultado;
	}

	private ScriptSqlResulJDBCVO ejecutarScriptSqlSP(StringBuilder sql, String jndiConexion,
			List<Integer> parametroOutType, List<Object> parametroInType, Map<String, Object> parametrosType)
			throws Exception {
		ScriptSqlResulJDBCVO resultado = new ScriptSqlResulJDBCVO();
		String jpaql = sql.toString();
		Connection connectionOracle = null;
		Connection connectionOracleArray = null;
		Connection connection = null;
		PreparedStatement st = null;
		CallableStatement cst = null;
		ResultSet rs = null;
		boolean isTypeArray = false;
		if (jndiConexion == null) {
			connection = getConexionDS();
		} else {
			connection = getConexionDS(jndiConexion);
		}
		connectionOracle = (Connection) connection;

		try {
			log.info("jpaql " + jpaql);
			cst = connectionOracle.prepareCall(sql.toString());
			int index = 1;
			List<Integer> listaIndexOutPut = new ArrayList<>();
			Map<Integer, Integer> indexOutPutMap = new HashMap<>();
			for (Object objParamIn : parametroInType) {
				if (objParamIn != null) {
					if (objParamIn instanceof Boolean) {
						cst.setBoolean(index, (Boolean) objParamIn);
					} else if (objParamIn instanceof BigDecimal) {
						cst.setBigDecimal(index, (BigDecimal) objParamIn);
					} else if (objParamIn instanceof Long) {
						cst.setLong(index, (Long) objParamIn);
					} else if (objParamIn instanceof Integer) {
						cst.setInt(index, (Integer) objParamIn);
					} else if (objParamIn instanceof Date) {
						Date value = (Date) objParamIn;
						java.sql.Date jdbcDate = java.sql.Date.class.isInstance(value) ? (java.sql.Date) value
								: new java.sql.Date(value.getTime());
						cst.setDate(index, jdbcDate);
					} else if (objParamIn instanceof String) {
						cst.setString(index, StringEscapeUtils.escapeSql((String) objParamIn));
					} else if (objParamIn instanceof Character) {
						cst.setString(index, StringEscapeUtils.escapeSql(objParamIn + ""));
					} else if (objParamIn.getClass().isAssignableFrom(ArrayList.class)) {
						StructDescriptor itemDescriptor = StructDescriptor
								.createDescriptor(parametrosType.get(index + "Type").toString(), connectionOracle);
						List<Object[]> lista = (List<Object[]>) objParamIn;
						STRUCT[] idsArray = new STRUCT[lista.size()];
						int indexArray = 0;
						for (Object[] itemAtributes : lista) {
							STRUCT itemObject1 = new STRUCT(itemDescriptor, connectionOracle, itemAtributes);
							idsArray[indexArray] = itemObject1;
							indexArray++;
						}
						ArrayDescriptor descriptor = ArrayDescriptor
								.createDescriptor(parametrosType.get(index + "TBL").toString(), connectionOracle);
						ARRAY array_to_pass = new ARRAY(descriptor, connectionOracle, idsArray);
						cst.setArray(index, array_to_pass);
					} else {
						cst.setObject(index, objParamIn);
					}
				} else {
					cst.setObject(index, objParamIn);
				}
				index++;
			}
			for (Integer paramMap : parametroOutType) {
				cst.registerOutParameter(index, paramMap);
				listaIndexOutPut.add(index);
				indexOutPutMap.put(index, paramMap);
				index++;
			}
			cst.execute();
			if (!parametroOutType.isEmpty()) {
				int indexResul = 1;
				for (Integer indexOutPut : listaIndexOutPut) {
					if (OracleTypes.CURSOR == indexOutPutMap.get(indexOutPut)) {
						rs = (ResultSet) cst.getObject(indexOutPut);
						ScriptSqlResulJDBCVO resultadoCursor = new ScriptSqlResulJDBCVO();
						if (rs != null) {
							resultadoCursor = generarListMap(rs, false);
						}
						resultado.getResulCursorList().add(resultadoCursor);
					} else {
						resultado = generarResulSPMap(resultado, indexOutPut, indexResul, cst, listaIndexOutPut);
					}
					indexResul++;
				}
			}

		} catch (Exception e) {
			log.error("error", e);
			resultado.setTieneError(true);
			resultado.setMensajeError(e.getMessage() + " \n " + e.toString());
			throw e;
		} finally {
			log.error("ejecutando  finally ");
			if (st != null) {
				st.close();
			}
			if (cst != null) {
				cst.close();
			}
			if (rs != null) {
				rs.close();
			}
			if (isTypeArray) {
				log.error("ejecutando  finally connectionOracle ");
				if (connectionOracleArray != null) {
					connectionOracleArray.close();
				}
				// connectionOracle.close();
			}
			try {
				log.error("ejecutando  finally wconn ");
				if (connection != null) {
					connection.close();
				}
			} catch (Exception e2) {
				log.error("errore2", e2);
				log.error("genero.ejecutando  finally connection error en finally " + e2.getMessage());
			}
		}
		return resultado;
	}

	/**
	 * Gets the conexion ds.
	 *
	 * @return the conexion ds
	 * @throws NamingException 
	 * @throws Exception the exception
	 */
	private Connection getConexionDS() throws SQLException, NamingException {
		InitialContext ctx;
		ctx = new InitialContext();
		DataSource dataSource = (DataSource) ctx.lookup("java:jboss/datasources/pwrIntegrationTron2000DS");
		return dataSource.getConnection();
	}

	/**
	 * Gets the conexion ds.
	 *
	 * @param jndiConexion
	 *            the JNDI conexion
	 * @return the conexion ds
	 * @throws SQLException
	 * @throws NamingException 
	 * @throws Exception
	 *             the exception
	 */
	private Connection getConexionDS(String jndiConexion) throws SQLException, NamingException {
		InitialContext ctx;
		ctx = new InitialContext();
		DataSource dataSource = (DataSource) ctx.lookup("java:jboss/datasources/pwrIntegrationTron2000DS");
		return dataSource.getConnection();
	}
}
