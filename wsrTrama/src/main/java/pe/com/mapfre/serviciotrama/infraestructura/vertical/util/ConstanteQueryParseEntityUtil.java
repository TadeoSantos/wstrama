package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;
/**
 * La Class ConstanteQueryParseEntityUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public final class ConstanteQueryParseEntityUtil {

	
	/** La Constante DISCRIMINAR_TILDE_MAYUSCULA_CONVERT. */
	public static final String DISCRIMINAR_TILDE_MAYUSCULA_CONVERT = "�?É�?ÓÚÀÈÌÒÙÄË�?ÖÜÂÊÎÔÛ";
	
	/** La Constante DISCRIMINAR_TILDE_MAYUSCULA_TRASLATE. */
	public static final String DISCRIMINAR_TILDE_MAYUSCULA_TRASLATE = "AEIOUAEIOUAEIOUAEIOU";	
	
	/** La Constante DISCRIMINAR_TILDE_MINUSCULA_CONVERT. */
	public static final String DISCRIMINAR_TILDE_MINUSCULA_CONVERT = "áéíóú�?É�?ÓÚ";		
	
	/** La Constante DISCRIMINAR_TILDE_MINUSCULA_TRASLATE. */
	public static final String DISCRIMINAR_TILDE_MINUSCULA_TRASLATE = "aeiouAEIOU";	
	
	/** La Constante IS_ORACLE_RECURSIVE. */
	public static final boolean IS_ORACLE_RECURSIVE = true;
	
	/**
	 * Instancia un nuevo constante query parse entity util.
	 */
	private ConstanteQueryParseEntityUtil() {
		
	}
}
