package pe.com.mapfre.serviciotrama.application.service;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.domain.entity.vo.TramaVO;

/**
 * La Class ITramaAppService.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface ITramaAppService {
	
	String procesarConfiguracionTrama(TramaVO tramavo) throws Exception;
	
}
