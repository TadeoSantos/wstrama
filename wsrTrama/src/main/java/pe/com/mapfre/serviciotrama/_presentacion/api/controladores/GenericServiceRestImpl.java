package pe.com.mapfre.serviciotrama._presentacion.api.controladores;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import pe.com.mapfre.serviciotrama._presentacion.api.entity.vo.RespuestaWSVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CollectionUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.AccionType;


public class GenericServiceRestImpl {

	public String getNodos() {
		return testNodos();
	}

	public static String testNodos() {
		String resultado = "";
		try {
			String nodeName = System.getProperty("jboss.node.name");
			if (nodeName == null) {
				nodeName = InetAddress.getLocalHost().getHostName() + " >>";
			}
			resultado = nodeName;
		} catch (Exception e) {
			resultado = e.getMessage();
		}
		return resultado;
	}

	public Response ping(String recurso) {
		RespuestaWSVO<String> resultado = inicializar(new RespuestaWSVO<String>());
		resultado.setMensajeError(
				"OK...(" + getNodos() + ")..." + recurso + "...ping " + FechaUtil.obtenerFechaActual());
		return ok(resultado);
	}

	public <T> RespuestaWSVO<T> parsearResultadoError(Exception e, RespuestaWSVO<T> resultado) {
		resultado.setError(true);
		resultado.setCodigoError("999");
		resultado.setMensajeError(e.getMessage() + " --> " + e.toString());
		return resultado;
	}

	public <T> RespuestaWSVO<T> inicializar(RespuestaWSVO<T> resultado) {
		resultado.setCodigoError("");
		resultado.setError(false);
		resultado.setMensajeError("");
		resultado.setListaResultado(new ArrayList<T>());
		return resultado;
	}

	public <T> RespuestaWSVO<T> parsearResultadoError(RespuestaWSVO<T> resultado, String codigoError,
			String mensajeError) {
		resultado.setError(true);
		resultado.setCodigoError(codigoError);
		resultado.setMensajeError(mensajeError);
		return resultado;
	}

	public Response noContent(Object object) {
		return Response.status(Response.Status.NO_CONTENT).type(MediaType.APPLICATION_JSON).entity(object).build();
	}

	public Response ok(Object object) {
		return Response.status(Response.Status.OK).entity(object).build();
	}

	public Response aceptado(Object object) {
		return Response.status(Response.Status.ACCEPTED).entity(object).build();
	}

	public Response created(Object object) {
		return Response.status(Response.Status.CREATED).entity(object).build();
	}

	public Response internalServerError(Object object) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(object).build();
	}

	public <T> Response respuesta(RespuestaWSVO<T> resultado, AccionType accionType) {
		if (!resultado.isError()) {
			if (resultado.getObjetoResultado() != null || !CollectionUtil.isEmpty(resultado.getListaResultado())) {
				if (accionType != null && AccionType.CREAR.equals(accionType))
					return created(resultado);

				return ok(resultado);
			} else {
				if (accionType == null) {
					return ok(resultado);
				}
				return noContent(resultado);
			}
		} else {
			return internalServerError(resultado);
		}
	}

	public <T> Response respuestaCola(RespuestaWSVO<T> resultado) {
		if (!resultado.isError()) {
			return aceptado(resultado);
		} else {
			return internalServerError(resultado);
		}
	}

	public static <T> T toRest(@Context UriInfo info, Class<T> entityClassDTO) {
		return TransferDataUtil.toRestDTO(info, entityClassDTO);
	}

	protected Map<String, Object> toMap(@Context UriInfo info) {
		return TransferDataUtil.toGetRestMap(info);
	}

}