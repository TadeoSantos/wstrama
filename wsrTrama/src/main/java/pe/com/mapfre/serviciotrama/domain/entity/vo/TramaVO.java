package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class TramaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class TramaVO implements Serializable {
 
	/** La Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private String userName;
	
	private String password;
	
	private String idControlProceso;
	
	private Long idProcesoFlujo;
	
	private Long juegoTrama;
	
	private String numeroPoliza;
	
	private String numeroSolicitud;
	
	private String origen;
	
	private Boolean validarExclusion;
	
	private Long indicadorInternoActividad;
	
	private String ejecucionCalendarizacion;
	
	
	
	/**
	 * Instancia un nuevo archivo adjunto.
	 */
	public TramaVO() {
	}


	public TramaVO(String userName, String password, String idControlProceso, Long idProcesoFlujo, Long juegoTrama,
			String numeroPoliza, String numeroSolicitud, String origen, Boolean validarExclusion,
			Long indicadorInternoActividad, String ejecucionCalendarizacion) {
		super();
		this.userName = userName;
		this.password = password;
		this.idControlProceso = idControlProceso;
		this.idProcesoFlujo = idProcesoFlujo;
		this.juegoTrama = juegoTrama;
		this.numeroPoliza = numeroPoliza;
		this.numeroSolicitud = numeroSolicitud;
		this.origen = origen;
		this.validarExclusion = validarExclusion;
		this.indicadorInternoActividad = indicadorInternoActividad;
		this.ejecucionCalendarizacion = ejecucionCalendarizacion;
	}
	
}