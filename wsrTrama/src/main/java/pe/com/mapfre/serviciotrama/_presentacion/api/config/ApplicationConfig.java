package pe.com.mapfre.serviciotrama._presentacion.api.config;

import javax.servlet.ServletConfig;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;

import io.swagger.jaxrs.config.BeanConfig;

/**
 * @author Antonio Goncalves
 *         http://www.antoniogoncalves.org
 *         --
 */

//@ApplicationPath("/rs")
@ApplicationPath("/api")
public class ApplicationConfig extends Application {

    // ======================================
    // =          Business methods          =
    // ======================================

   /* @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(BibliaServiceRestImpl.class);
        return classes;
    }*/
	
	public ApplicationConfig(@Context ServletConfig servletConfig) {
		try {
			BeanConfig beanConfig = new BeanConfig();
			beanConfig.setVersion("1.0.0");
			beanConfig.setTitle("Todo API");
			beanConfig.setBasePath("/wsrTrama/api");
			beanConfig.setResourcePackage("pe.com.mapfre.serviciotrama._presentacion.api.controladores");
			beanConfig.setScan(true);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
}
