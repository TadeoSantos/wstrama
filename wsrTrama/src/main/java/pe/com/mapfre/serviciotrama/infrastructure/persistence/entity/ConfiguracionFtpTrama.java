package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ConfiguracionFtpTrama.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_CNF_FTP_TRA", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ConfiguracionFtpTrama implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id configuracion ftp trama. */
    @Id
    @Column(name = "ID_CONF_FTP_TRAMA" , length = 18)
    private Long idConfiguracionFtpTrama;
   
    /** El juego trama. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CONFIG_JUEGO_TRAMA", referencedColumnName = "N_ID_CONFIG_JUEGO_TRAMA")
    private JuegoTrama juegoTrama;
   
    /** El servidor. */
    @Column(name = "C_SERVIDOR" , length = 100)
    private String servidor;
   
    /** El puerto. */
    @Column(name = "N_PUERTO" , length = 10)
    private Long puerto;
   
    /** El usuario. */
    @Column(name = "C_USUARIO" , length = 100)
    private String usuario;
   
    /** El password. */
    @Column(name = "C_PASS" , length = 100)
    private String password;
   
    /** El ruta. */
    @Column(name = "C_RUTA" , length = 150)
    private String ruta;
   
    /** El tipo. */
    @Column(name = "C_TIPO" , length = 1)
    private String tipo;
    
    /** El protocolo. */
    @Column(name = "C_PROTOCOLO" , length = 1)
    private String protocolo;
   
    /**
     * Instancia un nuevo configuracion ftp trama.
     */
    public ConfiguracionFtpTrama() {
    }
   
   
    /**
     * Instancia un nuevo configuracion ftp trama.
     *
     * @param idConfiguracionFtpTrama el id configuracion ftp trama
     * @param juegoTrama el juego trama
     * @param servidor el servidor
     * @param puerto el puerto
     * @param usuario el usuario
     * @param password el password
     * @param ruta el ruta
     * @param tipo el tipo
     */
    public ConfiguracionFtpTrama(Long idConfiguracionFtpTrama, JuegoTrama juegoTrama,String servidor, Long puerto, String usuario, String password, String ruta, String tipo, String protocolo ) {
        super();
        this.idConfiguracionFtpTrama = idConfiguracionFtpTrama;
        this.juegoTrama = juegoTrama;
        this.servidor = servidor;
        this.puerto = puerto;
        this.usuario = usuario;
        this.password = password;
        this.ruta = ruta;
        this.tipo = tipo;
        this.protocolo = protocolo;
    }

	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idConfiguracionFtpTrama == null) ? 0 : idConfiguracionFtpTrama.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConfiguracionFtpTrama other = (ConfiguracionFtpTrama) obj;
        if (idConfiguracionFtpTrama == null) {
            if (other.idConfiguracionFtpTrama != null) {
                return false;
            }
        } else if (!idConfiguracionFtpTrama.equals(other.idConfiguracionFtpTrama)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConfiguracionFtpTrama [idConfiguracionFtpTrama=" + idConfiguracionFtpTrama + "]";
    }
   
}