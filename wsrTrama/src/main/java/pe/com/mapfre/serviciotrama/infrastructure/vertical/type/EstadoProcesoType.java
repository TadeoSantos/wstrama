package pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class EstadoProcesoType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum EstadoProcesoType {

 	/** La en proceso. */
	EN_SOLICITUD("0", "estadoProcesoType.pendiente"),
	
 	/** La proceso ok. */
	PROCESO_OK("1", "estadoProcesoType.terminado"),
 	
 	/** La proceso error. */
	PROCESO_ERROR("2", "estadoProcesoType.error"),
	
	/** La no procesado. */
	SIN_TRAMAS("3", "estadoProcesoType.noProcesado"),
	
	/** La error ejecucion. */
	EN_PROCESO("4", "estadoProcesoType.proceso");
	
 
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, EstadoProcesoType> LOO_KUP_MAP = new HashMap<String, EstadoProcesoType>();
	
	static {
		for (EstadoProcesoType s : EnumSet.allOf(EstadoProcesoType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}
	
	/** La key. */
	private String key;
	
	
	/** La value. */
	private String value;

	/**
	 * Instancia un nuevo estado proceso type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private EstadoProcesoType(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Get.
	 *
	 * @param key el key
	 * @return the estado proceso type
	 */
	public static EstadoProcesoType get(Long key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
