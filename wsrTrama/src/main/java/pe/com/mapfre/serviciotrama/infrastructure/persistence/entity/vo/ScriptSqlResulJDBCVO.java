package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * La Class ScriptSqlResulJDBCVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class ScriptSqlResulJDBCVO implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
    /** La lista header. */
    private List<String> listaHeader = new ArrayList<String>();
    
    /** La lista data. */
    private List<Map<String,Object>> listaData = new ArrayList<Map<String,Object>>();
    
    /** La lista listaDataObject */
    private List<Object[]> listaDataObject = new ArrayList<Object[]>();
    
    /** La tiene error. */
    private boolean tieneError = false;
    
    /** La mensaje error. */
    private String mensajeError = "";
    
    private int executeUpdate = 0;
    
    private List<ScriptSqlResulJDBCVO> resulCursorList = new ArrayList<ScriptSqlResulJDBCVO>();
    
	/**
	 * Instancia un nuevo script sql resul vo.
	 */
	public ScriptSqlResulJDBCVO() {
		super();
	}

	public boolean isTieneData() {
		boolean resultado = false;
		if (!tieneError) {
			if (listaData != null && listaData.size() > 0) {
				resultado = true;
			}
		}
		return resultado;
	}
	
	public boolean isTieneDataCursor() {
		boolean resultado = false;
		if (!tieneError) {
			if (resulCursorList != null && resulCursorList.size() > 0) {
				resultado = true;
			}
		}
		return resultado;
	}

	
	/**
	 * Instancia un nuevo script sql resul vo.
	 *
	 * @param listaHeader el lista header
	 * @param listaData el lista data
	 */
	public ScriptSqlResulJDBCVO(List<String> listaHeader, List<Map<String,Object>> listaData) {
		super();
		this.listaHeader = listaHeader;
		this.listaData = listaData;
	}

	/**
     * Obtiene lista header.
     *
     * @return lista header
     */
    public List<String> getListaHeader() {
		return listaHeader;
	}

	/**
	 * Establece el lista header.
	 *
	 * @param listaHeader el new lista header
	 */
	public void setListaHeader(List<String> listaHeader) {
		this.listaHeader = listaHeader;
	}

	/**
	 * Obtiene lista data.
	 *
	 * @return lista data
	 */
	public List<Map<String,Object>> getListaData() {
		return listaData;
	}

	/**
	 * Establece el lista data.
	 *
	 * @param listaData el new lista data
	 */
	public void setListaData(List<Map<String,Object>> listaData) {
		this.listaData = listaData;
	}

	/**
	 * Comprueba si es tiene error.
	 *
	 * @return true, si es tiene error
	 */
	public boolean isTieneError() {
		return tieneError;
	}

	/**
	 * Establece el tiene error.
	 *
	 * @param tieneError el new tiene error
	 */
	public void setTieneError(boolean tieneError) {
		this.tieneError = tieneError;
	}

	/**
	 * Obtiene mensaje error.
	 *
	 * @return mensaje error
	 */
	public String getMensajeError() {
		return mensajeError;
	}

	/**
	 * Establece el mensaje error.
	 *
	 * @param mensajeError el new mensaje error
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}

	public List<Object[]> getListaDataObject() {
		return listaDataObject;
	}

	public void setListaDataObject(List<Object[]> listaDataObject) {
		this.listaDataObject = listaDataObject;
	}

	public int getExecuteUpdate() {
		return executeUpdate;
	}

	public void setExecuteUpdate(int executeUpdate) {
		this.executeUpdate = executeUpdate;
	}

	
	public List<ScriptSqlResulJDBCVO> getResulCursorList() {
		return resulCursorList;
	}

	public void setResulCursorList(List<ScriptSqlResulJDBCVO> resulCursorList) {
		this.resulCursorList = resulCursorList;
	}
	//INICIO Hitss 11/10/2021
	public ScriptSqlResulJDBCVO getResulCursor3() {
		if (resulCursorList.size() > 0) {
			return resulCursorList.get(2);	
		}
		return new ScriptSqlResulJDBCVO();
	}
	//FIN Hitss 11/10/2021
	public ScriptSqlResulJDBCVO getResulCursor() {
		if (resulCursorList.size() > 0) {
			return resulCursorList.get(0);	
		}
		return new ScriptSqlResulJDBCVO();
	}
	//INICIO INDRA 25/07/2019
	public ScriptSqlResulJDBCVO getResulCursor2() {
		if (resulCursorList.size() > 0) {
			return resulCursorList.get(1);	
		}
		return new ScriptSqlResulJDBCVO();
	}
	//FIN INDRA 25/07/2019
}
