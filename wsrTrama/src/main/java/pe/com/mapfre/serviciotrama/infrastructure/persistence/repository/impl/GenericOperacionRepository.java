package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Query;
import javax.sql.DataSource;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteQueryParseEntityUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.GenericJDBC;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ObjectUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.EntityMapperJPAQLUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.BasePaginator;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.SelectItemVO;

/**
 * La Class GenericOperacionDAOImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 * 
 * @param <K> el tipo de clave
 * @param <T> el tipo generico
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class GenericOperacionRepository  extends GenericJDBC{

	
	private static final String NOT_IN = " not in ";
	
	protected StringBuilder saveInsert(Class<?> entityClass,List<String> parametrosHeader) {
		return EntityMapperJPAQLUtil.generarInsertNative(entityClass,parametrosHeader);
	}
	
	protected StringBuilder updateNative(Class<?> entityClass,List<String> parametrosHeader) {
		return EntityMapperJPAQLUtil.generarUpdateNative(entityClass,parametrosHeader);
	}
	
	public Map<String,Object> obtenerParametroDiscriminarTilde() {
		Map<String,Object> parametraMap = new HashMap<>();
		parametraMap.put("discriminaTildeMAC",ConstanteQueryParseEntityUtil.DISCRIMINAR_TILDE_MAYUSCULA_CONVERT);			
		parametraMap.put("discriminaTildeMAT",ConstanteQueryParseEntityUtil.DISCRIMINAR_TILDE_MAYUSCULA_TRASLATE);			
		parametraMap.put("discriminaTildeMIC",ConstanteQueryParseEntityUtil.DISCRIMINAR_TILDE_MINUSCULA_CONVERT);			
		parametraMap.put("discriminaTildeMIT",ConstanteQueryParseEntityUtil.DISCRIMINAR_TILDE_MINUSCULA_TRASLATE);
		return parametraMap;
	}
	public Map<String,Object> obtenerParametroListaIn(String nombreParametro,List<?> listaParametroTemp) {
		Map<String,Object> parametraMap = new HashMap<>();
		 int indexDinamic = 0;
		 List<?> listaParametro = new ArrayList<>(listaParametroTemp);
		 if (!listaParametro.isEmpty()) {
			 if (listaParametro.size() > 1000) { 
			      while (listaParametro.size() > 1000) {
			        List<?> subList = new ArrayList<>(listaParametro.subList(0, 1000)) ;
			        parametraMap.put(nombreParametro + indexDinamic, subList);
			        listaParametro.subList(0, 1000).clear();
			        indexDinamic++;
			      }
			  }
			 parametraMap.put(nombreParametro, listaParametro);
		 }
		 
		return parametraMap;
	}
	
	/**
	 * Obtener parametro sql lista in.
	 *
	 * @param nombreParametro the nombre parametro
	 * @param campo the campo
	 * @param listaKeysTemp the lista parametro temp
	 * @param isIn the is in
	 * @return the string builder
	 */
	public StringBuilder obtenerParametroSqlListaIn(String nombreParametro,String campo,List<?> listaKeysTemp, boolean isIn) {
		StringBuilder jpaql = new StringBuilder();
		 int indexDinamic = 0;
		 List<Object> listaParametro = new ArrayList<>(listaKeysTemp);
		 if (!listaParametro.isEmpty()) {
			 jpaql.append(" and ( ");
			 if (listaParametro.size() > 1000) { 
			      while (listaParametro.size() > 1000) {
			        jpaql.append(" " + campo + " " + ( isIn ? "in" : NOT_IN ) + " (:" + nombreParametro + "" + indexDinamic + ") OR ");
			        listaParametro.subList(0, 1000).clear();
			        indexDinamic++;
			      }
			  }
			 jpaql.append("  " + campo + " " + ( isIn ? "in" : NOT_IN ) + "  (:" + nombreParametro + ") ) ");
		 }
		 return jpaql;
	}
	
	public StringBuilder obtenerParametroSqlListaInJdbc(String campo, List<Object> listaParametroTemp, boolean isIn) {
		StringBuilder jpaql = new StringBuilder();
		StringBuilder  cadena = new StringBuilder();
		List<?> listaParametro = new ArrayList<>(listaParametroTemp);
		if (!listaParametro.isEmpty()) {
			jpaql.append(" and ( ");
			while (!listaParametro.isEmpty()) {
				List<?> subList = null;
				if (listaParametro.size() > 1000) {
					subList = new ArrayList<>(listaParametro.subList(0, 1000));
				} else {
					subList = new ArrayList<>(listaParametro.subList(0, listaParametro.size()));
				}
				obtenerConcatenacion(cadena, subList);
				jpaql.append(" " + campo + " " + (isIn ? "in" : NOT_IN) + " (" + cadena + ") ");
				listaParametro.subList(0, subList.size()).clear();
				if (listaParametroTemp.size() > 1000 && !listaParametro.isEmpty()) {
					jpaql.append(" OR ");
				}
			}
			jpaql.append(" )");
		}
		return jpaql;
	}
	
	private StringBuilder obtenerConcatenacion(StringBuilder  cadena ,List<?> subList ) {
		for (Object object : subList) {
			if (!cadena.toString().isEmpty()) {
				cadena.append(", " + object.toString());
			} else {
				cadena = new StringBuilder(object.toString());
			}
		}
		return cadena;
	}
	
	public Map<String,String> obtenerResultadoMap(List<Object[]> listaObjetos, int cantidadKey,int posicionValue) {
		Map<String, String> resultado = new HashMap<>();
		if (!isEmpty(listaObjetos)) {
			for (Object[] objects : listaObjetos) {
				String key = StringUtil.generarKey(objects, cantidadKey);
				if (!resultado.containsKey(key)) {
					resultado.put(key, ObjectUtil.objectToString(objects[posicionValue]));
				}
			}
		}
		return resultado;
	}
	
	public static boolean isEmpty(List<?> list) {
		boolean respuesta = false;
		if (list == null || list.isEmpty()) {
			respuesta = true;
		}
		return respuesta;
	}
	
	public ListaItemsVO getListaListaItemsVO(String codigo,Query query, BasePaginator t, boolean esIdString) {
		ListaItemsVO resultado = new ListaItemsVO();
		if (t.getOffset() > 0) {
			query.setFirstResult(t.getStartRow());
			query.setMaxResults(t.getOffset());
		}
		List<Object[]> resultadoTemp =  query.getResultList();
		List<SelectItemVO> lista = new ArrayList<>(resultadoTemp.size());
		for(Object[] arr : resultadoTemp) {
			if(arr == null)
				continue;
			if(arr.length == 2) {
				if(esIdString) {
					lista.add(new SelectItemVO(ObjectUtil.objectToString(arr[0]), ObjectUtil.objectToString(arr[1])));
				}else {
					lista.add(new SelectItemVO(arr[0], ObjectUtil.objectToString(arr[1])));
				}
			}else if(arr.length == 3) {
				if(esIdString) {
					lista.add(new SelectItemVO(ObjectUtil.objectToString(arr[0]), ObjectUtil.objectToString(arr[1]), ObjectUtil.objectToString(arr[2])));
				}else {
					lista.add(new SelectItemVO(arr[0], ObjectUtil.objectToString(arr[1]), ObjectUtil.objectToString(arr[2])));
				}
			}
		
		}
		resultado.setCodigo(codigo);
		resultado.setItems(lista);
		return resultado;
	}
	
	public int getContador(Query query) {
		Object obj = query.getSingleResult();
		if(obj instanceof Long)
			return ((Long) obj).intValue();
		else if(obj instanceof BigDecimal)
			return ((BigDecimal) obj).intValue();
		else if(obj instanceof Integer)
			return ((Integer) obj).intValue();
		return ObjectUtil.objectToInteger(obj);
	}
	
	public Connection getConexionDS(String nombreJNDI) throws NamingException, SQLException {
		Context ctx;
		ctx = new InitialContext();
		DataSource dataSource = (DataSource) ctx.lookup("java:jboss/datasources/" + nombreJNDI);
		return dataSource.getConnection();
	}
	
	public Connection getConexionDS() throws SQLException, NamingException {
		Context ctx;
		ctx = new InitialContext();
		DataSource dataSource = (DataSource) ctx.lookup("java:jboss/datasources/pwrIntegrationTron2000DS");
		return dataSource.getConnection();
	}
}
