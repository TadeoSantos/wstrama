package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;
/**
 * La Class ConstanteTramaUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public final class ConstanteTramaUtil { 
	
	public static final String CODIGO_ACTIVIDAD_INTERNO = "CODIGO_ACTIVIDAD_INTERNO";
	public static final String MODO_EJECUCION = "MODO_EJECUCION";
	public static final String ES_SINCRONO = "ES_SINCRONO";
	public static final String VALIDAR_EXCLUSION = "VALIDAR_EXCLUSION";
	public static final String EMPEZAR_CONCATENACION = "0001";
	public static final Character PARAMETRO_COMPLETAR = '0';
	public static final Integer VECES_COMPLETAR = 4;
	public static final Integer TAMANIO_MAXIMO_MENSAJE_TECNICO = 499;
	public static final String USUARIO_EJECUCION = "USUARIO_EJECUCION";
	public static final String NOMBRE_FLUJO = "NOMBRE_FLUJO";
	public static final String PARAMETRO_LIST = "PARAMETRO_LIST";
	public static final String ID_CAB_CARGA = "ID_CAB_CARGA";
	public static final String SPLIT_PUNTO = "\\.";
	public static final String PLANTILLA_REPORTE_GERENCIA = "reporte-gerencia-template.xlsx";
	public static final String PLANTILLA_EQUIVALENCIA_DATOS = "PlantillaEquivalenciaDatos.xls";
	public static final String PLANTILLA_PROVEEDORES_RED = "Plantilla_Proveedores.xls";
	public static final String PLANTILLA_COBERTURA_DATOS = "PlantillaCobertura.xls";
	
	public static final String CODIGO_ACTIVIDAD_INTERNO_CONTINUAR = "CODIGO_ACTIVIDAD_INTERNO_CONTINUAR";
	
	public static final String BUNDLE_NAME_CONFIGURADOR_TRAMA_TYPE = "pe.com.mapfre.serviciotrama.infrastructure.vertical.type.properties-enum";

	private ConstanteTramaUtil() {}
}
