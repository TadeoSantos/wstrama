package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class MantenimientoDestinatario.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_DESTINATARIO", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class MantenimientoDestinatario implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id destinatario. */
    @Id
    @Column(name = "N_ID_DESTI" , precision = 18 , scale = 0)
    private Long idDestinatario;
   
    /** El nombre destinatario. */
    @Column(name = "C_NOM_DES" , length = 300)
    private String nombreDestinatario;
   
    /** El cargo destinatario. */
    @Column(name = "C_CARGO_DES" , length = 200)
    private String cargoDestinatario;
   
    /** El correo destinatario. */
    @Column(name = "C_CORREO_DES" , length = 250)
    private String correoDestinatario;
   
    /** El tipo destinatario. */
    @Column(name = "C_TIPO" , length = 1)
    private Long tipoDestinatario;
    
    /** El estado destinatario. */
    @Column(name = "C_ESTADO" , length = 20)
    private String estadoDestinatario;
   
    /** El codigo usuario. */
    @Column(name = "C_COD_USU" , length = 50)
    private String codigoUsuario;
   
    /** El fecha activo. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActivo;
   
    /**
     * Instancia un nuevo mantenimiento destinatario.
     */
    public MantenimientoDestinatario() {
    }
   
    /**
     * Instancia un nuevo mantenimiento destinatario.
     *
     * @param idDestinatario el id destinatario
     * @param nombreDestinatario el nombre destinatario
     * @param cargoDestinatario el cargo destinatario
     * @param correoDestinatario el correo destinatario
     * @param tipoDestinatario the tipo destinatario
     * @param estadoDestinatario el estado destinatario
     * @param codigoUsuario el codigo usuario
     * @param fechaActivo el fecha activo
     */
    public MantenimientoDestinatario(Long idDestinatario, String nombreDestinatario, String cargoDestinatario, String correoDestinatario, Long tipoDestinatario, String estadoDestinatario, String codigoUsuario, Date fechaActivo ) {
        super();
        this.idDestinatario = idDestinatario;
        this.nombreDestinatario = nombreDestinatario;
        this.cargoDestinatario = cargoDestinatario;
        this.correoDestinatario = correoDestinatario;
        this.tipoDestinatario = tipoDestinatario;        
        this.estadoDestinatario = estadoDestinatario;
        this.codigoUsuario = codigoUsuario;
        this.fechaActivo = fechaActivo;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idDestinatario == null) ? 0 : idDestinatario.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MantenimientoDestinatario other = (MantenimientoDestinatario) obj;
        if (idDestinatario == null) {
            if (other.idDestinatario != null) {
                return false;
            }
        } else if (!idDestinatario.equals(other.idDestinatario)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MantenimientoDestinatario [idDestinatario=" + idDestinatario + "]";
    }
   
}