package pe.com.mapfre.serviciotrama.domain.service;


import java.util.List;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;

/**
 * La Class CamoteMantenedorServiceLocal.
 * <ul>
 * <li>Copyright 2021 MAPFRE -
 * MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft
 * @version 1.0, Mon Sep 06 16:48:51 COT 2021
 * @since MYTRON 1.0
 */
@Local
public interface IConfiguracionService{
	List<ListaItemsVO> listarItems(ListaItemsVO filtro);

	int contarItems(ListaItemsVO filtro);
	
}
