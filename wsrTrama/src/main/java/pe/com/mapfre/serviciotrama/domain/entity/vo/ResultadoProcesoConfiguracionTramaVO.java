package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ResultadoProcesoConfiguracionTramaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ResultadoProcesoConfiguracionTramaVO implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La fecha lote. */
	private Date fechaLote;
	
	/** La numero lote. */
	private String numeroLote;
	
	/** La numero poliza. */
	private String numeroPoliza;
	
	/** La numero referencia. */
	private String numeroReferencia;
	
	/** La id config juego trama. */
	private Long idConfigJuegoTrama;
	
	/** La id nomenclatura archivo. */
	private Long idNomenclaturaArchivo;
	
	/** La id configuracion trama. */
	private Long idConfiguracionTrama;
	    
	/** La codigo error. */
	private String codigoError;
	
	/** La mensaje error. */
	private String mensajeError;
	
	/** La es error. */
	private boolean esError;
	
	/** La es tasme error. */
	private boolean esTasmeError;
	
	/** La fila. */
	private String fila;
	
	/** La nombre campo. */
	private String nombreCampo;
	
	/** La tipo error. */
	private String tipoError;
	
	//Inicio mejora fecha calculada
	/** La fecha calculada formato. */
	private Date fechaCalculadaParametro;
	//Fin mejora fecha calculada
	
	//aqui aumenta
	/** La lista tasme error. */
//	private BigMemoryManager<String,TasmeErrorMpe> listaTasmeError = new BigMemoryManager<>();
//	
//	private BigMemoryManager<String,LogConfiguracionTrama> listaLogConfiguracionTramaAyuda = new BigMemoryManager<>();
	/**
	 * Instancia un nuevo resultado proceso configuracion trama vo.
	 */
	public ResultadoProcesoConfiguracionTramaVO() {
		super();
	}

	/**
	 * Instancia un nuevo resultado proceso configuracion trama vo.
	 *
	 * @param fechaLote el fecha lote
	 * @param numeroLote el numero lote
	 * @param numeroPoliza el numero poliza
	 * @param numeroReferencia el numero referencia
	 * @param idConfigJuegoTrama el id config juego trama
	 * @param codigoError el codigo error
	 * @param mensajeError el mensaje error
	 * @param esError el es error
	 */
	public ResultadoProcesoConfiguracionTramaVO(Date fechaLote,
			String numeroLote,String numeroPoliza, String numeroReferencia,Long idConfigJuegoTrama, String codigoError, String mensajeError,
			boolean esError) {
		super();
		this.fechaLote = fechaLote;
		this.numeroLote = numeroLote;
		this.numeroPoliza = numeroPoliza;
		this.numeroReferencia = numeroReferencia;
		this.idConfigJuegoTrama = idConfigJuegoTrama;
		this.codigoError = codigoError;
		this.mensajeError = mensajeError;
		this.esError = esError;
	}

	/**
	 * Comprueba si es es error.
	 *
	 * @return true, si es es error
	 */
	public boolean isEsError() {
		return esError;
	}
	
	/**
	 * Comprueba si es es tasme error.
	 *
	 * @return true, si es es tasme error
	 */
	public boolean isEsTasmeError() {
		return esTasmeError;
	}

}
