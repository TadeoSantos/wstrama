package pe.com.mapfre.serviciotrama.application.service;

import java.util.List;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ListaItemsVO;

/**
 * La Class IConfiguracionAppService.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface IConfiguracionAppService {

	List<ListaItemsVO> listarItems(ListaItemsVO filtro);

	int contarItems(ListaItemsVO filtro);

}