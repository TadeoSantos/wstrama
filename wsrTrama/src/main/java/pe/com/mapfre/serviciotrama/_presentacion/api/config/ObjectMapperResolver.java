package pe.com.mapfre.serviciotrama._presentacion.api.config;
import java.text.SimpleDateFormat;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;


@Provider
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)  
public class ObjectMapperResolver implements ContextResolver<ObjectMapper> {
	
	protected final static String DATE_FORMAT_STR_ISO8601_Z = "dd/MM/yyyy HH:mm:ss";
    private ObjectMapper mapper;

    public ObjectMapperResolver() {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //mapper.configure(DeserializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.getDeserializationConfig().setDateFormat(new SimpleDateFormat(DATE_FORMAT_STR_ISO8601_Z));
    }

    @Override
    public ObjectMapper getContext(Class<?> cls) {
        return mapper;
    }
}