package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ProcesoVariable.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_ORQ_VAR_PROC", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ProcesoVariable implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id variable proceso. */
    @Id
    @Column(name = "N_ID_VAR_PROC" , length = 18)
    private Long idVariableProceso;
   
    /** El proceso flujo. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_PROC_FLU", referencedColumnName = "N_ID_PROC_FLU")
    private ProcesoFlujo procesoFlujo;
   
    /** El llave variable proceso. */
    @Column(name = "C_KEY_VAR_PROC" , length = 50)
    private String llaveVariableProceso;
   
    /** El valor variable proceso. */
    @Column(name = "C_VAL_VAR_PROC" , length = 100)
    private String valorVariableProceso;
   
    /** El tipo dato proceso. */
    @Column(name = "N_TIPO_DATO_PROC" , length = 1)
    private Long tipoDatoProceso;
   
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
   
    /**
     * Instancia un nuevo proceso variable.
     */
    public ProcesoVariable() {
    }
   
   
    /**
     * Instancia un nuevo proceso variable.
     *
     * @param idVariableProceso el id variable proceso
     * @param procesoFlujo el proceso flujo
     * @param llaveVariableProceso el llave variable proceso
     * @param valorVariableProceso el valor variable proceso
     * @param tipoDatoProceso el tipo dato proceso
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public ProcesoVariable(Long idVariableProceso, ProcesoFlujo procesoFlujo,String llaveVariableProceso, String valorVariableProceso, Long tipoDatoProceso, String usuarioCrea, Date fechaActualizacion ) {
        super();
        this.idVariableProceso = idVariableProceso;
        this.procesoFlujo = procesoFlujo;
        this.llaveVariableProceso = llaveVariableProceso;
        this.valorVariableProceso = valorVariableProceso;
        this.tipoDatoProceso = tipoDatoProceso;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idVariableProceso == null) ? 0 : idVariableProceso.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProcesoVariable other = (ProcesoVariable) obj;
        if (idVariableProceso == null) {
            if (other.idVariableProceso != null) {
                return false;
            }
        } else if (!idVariableProceso.equals(other.idVariableProceso)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ProcesoVariable [idVariableProceso=" + idVariableProceso + "]";
    }
   
}