package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * La Class CampoTablaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class CampoTablaVO implements Serializable  {

	private static final long serialVersionUID = 1L;
	private String nombreCampo;
	private String type;
	private String length;
	private String isNull;
	private String campoAsociado;
	
	
	public CampoTablaVO() {
		super();
	}

	public CampoTablaVO(String nombreCampo, String type, String length,
			String isNull, String campoAsociado) {
		super();
		this.nombreCampo = nombreCampo;
		this.type = type;
		this.length = length;
		this.isNull = isNull;
		this.campoAsociado = campoAsociado;
	}
	
}
