package pe.com.mapfre.serviciotrama.domain.entity.vo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.BasePaginator;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.SelectItemVO;


/**
 * <ul>
 * <li>Copyright 2014 MAPFRE. Todos los derechos reservados.</li>
 * </ul> 
 * 
 * La Class ListaItemsVO.
 *
 * @author BuildSoft
 * @version 1.0 , 06/04/2015
 * @since MYTRON-MAPFRE 1.0
 */
@Getter
@Setter
public class ListaItemsVO extends BasePaginator implements Serializable {
 
	/** La Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private List<SelectItemVO> items;
	private SelectItemVO filtro = new SelectItemVO();
	private List<String> listaCodigo = new ArrayList<>();
	private String codigo;
	private String id;
	private String idNivel1;
	private String idNivel2;
	private String idNivel3;
	private String idNivel4;
	private List<String> listaGrupo = new ArrayList<>();
	/**
	 * Instancia un nuevo compania vo.
	 */
	public ListaItemsVO() {
		super();
	}
	
	
}