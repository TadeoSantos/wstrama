package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ParametrosEntradaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ParametrosEntradaVO implements Serializable {

	private static final long serialVersionUID = 1L;

	/** La lista numero lote. */
	private List<String> listaNumeroLote = new ArrayList<>();
	
	/** La lista idconfiguracion juego trama. */
	private List<Long> listaIdconfiguracionJuegoTrama = new ArrayList<>();
	
	/** La lista fecha lote. */
	private List<String> listaFechaLote = new ArrayList<>();
	
	
	/**
	 * Instancia un nuevo parametros entrada vo.
	 */
	public ParametrosEntradaVO() {
		super();
	}
	
	/**
	 * Instancia un nuevo parametros entrada vo.
	 *
	 * @param listaNumeroLote el lista numero lote
	 * @param listaIdconfiguracionJuegoTrama el lista idconfiguracion juego trama
	 * @param listaFechaLote el lista fecha lote
	 */
	public ParametrosEntradaVO(List<String> listaNumeroLote, List<Long> listaIdconfiguracionJuegoTrama, List<String> listaFechaLote) {
		super();
		this.listaNumeroLote = listaNumeroLote;
		this.listaIdconfiguracionJuegoTrama = listaIdconfiguracionJuegoTrama;
		this.listaFechaLote = listaFechaLote;
	}

}
