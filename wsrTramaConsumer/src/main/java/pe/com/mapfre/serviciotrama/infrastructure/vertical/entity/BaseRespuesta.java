package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;
import lombok.Getter;
import lombok.Setter;

/**
 * La Class BaseRespuesta.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class BaseRespuesta {
	
	private String CodError;
	private String DescError;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BaseRespuesta [CodError=" + CodError + ", DescError=" + DescError + "]";
	}
}
