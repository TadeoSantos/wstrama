package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.domain.entity.vo.CargaArchivoTramaVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.RespuestaNaturalType;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CollectionUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionFtpTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaError;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.EquivalenciaDato;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.JuegoTramaCorreo;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TramaNomenclaturaArchivo;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TramaNomenclaturaArchivoDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.ScriptSqlResulJDBCVO;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ConfiguracionTramaAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.state.EstadoState;

/**
 * La Class ConfiguracionTramaAutomaticoDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ConfiguracionTramaAutomaticoDaoImpl extends GenericRepository<Object, TramaNomenclaturaArchivo>
		implements ConfiguracionTramaAutomaticoDaoLocal {
	private final Logger log = Logger.getLogger(this.getClass());
	@Override
	public TramaNomenclaturaArchivo obtenerTramaNomenclaturaArchivoById(Long idTramaNomenclaturaArchivo) {
		TramaNomenclaturaArchivo resultado = null;
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append("from TramaNomenclaturaArchivo o where ");
		jpaql.append(" o.idTramaNomenclaturaArchivo =:idTramaNomenclaturaArchivo ");
		parametros.put("idTramaNomenclaturaArchivo", idTramaNomenclaturaArchivo);
		Query query = createQuery(jpaql.toString(), parametros);
		List<TramaNomenclaturaArchivo> resulTemp = query.getResultList();
		if (!CollectionUtil.isEmpty(resulTemp)) {
			resultado = resulTemp.get(0);
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#listarConfiguracionTrama(pe.gob.mapfre.
	 * pwr.rep.model.configurador.trama.jpa.ConfiguracionTrama,
	 * javax.persistence.EntityManager)
	 */
	@Override
	public List<ConfiguracionTrama> listar(ConfiguracionTrama filtro) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(
				"from ConfiguracionTrama o  left join fetch o.tramaNomenclaturaArchivo nt  left join fetch o.juegoTrama jt where  1=1");
		if (!StringUtil.isNullOrEmpty(filtro.getEstado())) {
			jpaql.append(" and o.juegoTrama.estado =:estado  and o.juegoTrama.estadoProceso =:estado ");
			parametros.put("estado", filtro.getEstado());
		}
		jpaql.append(" and o.estado =:estadoTrama ");
		parametros.put("estadoTrama", EstadoState.ACTIVO.getKey());
		Long idProcesoFlujo = 0L;
		if (!StringUtil.isNullOrEmpty(filtro.getJuegoTrama())) {
			if (!StringUtil.isNullOrEmpty(filtro.getJuegoTrama().getProcesoFlujo())) {
				if (!StringUtil.isNullOrEmptyNumeric(filtro.getJuegoTrama().getProcesoFlujo().getIdProcesoFlujo())) {
					idProcesoFlujo = filtro.getJuegoTrama().getProcesoFlujo().getIdProcesoFlujo();
				}
			}
		}
		jpaql.append(" and o.juegoTrama.procesoFlujo.idProcesoFlujo =:idProcesoFlujo ");
		parametros.put("idProcesoFlujo", idProcesoFlujo);

		if (!StringUtil.isNullOrEmpty(filtro.getJuegoTrama())) {
			if (!StringUtil.isNullOrEmptyNumeric(filtro.getJuegoTrama().getIdJuegoTrama())) {
				jpaql.append(" and o.juegoTrama.idJuegoTrama =:idJuegoTrama ");
				parametros.put("idJuegoTrama", filtro.getJuegoTrama().getIdJuegoTrama());
			}
		}
		jpaql.append(" order by o.juegoTrama.idJuegoTrama, o.numeroOrden ");
		Query query = createQuery(jpaql.toString(), parametros);
		return query.getResultList();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#listarConfiguracionTramaDetalleMap(java.
	 * util.List, javax.persistence.EntityManager)
	 */
	@Override
	public Map<Long, List<ConfiguracionTramaDetalle>> listarMap(List<Long> listaIdConfiguracionTrama) {
		Map<Long, List<ConfiguracionTramaDetalle>> resultado = new HashMap<Long, List<ConfiguracionTramaDetalle>>();
		if (listaIdConfiguracionTrama == null || listaIdConfiguracionTrama.size() == 0) {
			return resultado;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(
				"from ConfiguracionTramaDetalle  o left join fetch o.configuracionTrama ct left join fetch o.campoAsociado ca left join fetch ca.configuracionTrama cta left join fetch o.campoAsociadoMatchInicio cai left join fetch o.campoAsociadoMatchInicio caf where  1=1");
		jpaql.append(" and o.configuracionTrama.idConfiguradorTrama in (:listaIdConfiguracionTrama) ");
		parametros.put("listaIdConfiguracionTrama", listaIdConfiguracionTrama);
		Query query = createQuery(jpaql.toString(), parametros);
		List<ConfiguracionTramaDetalle> resulTemp = query.getResultList();
		for (ConfiguracionTramaDetalle obj : resulTemp) {
			Long key = obj.getConfiguracionTrama().getIdConfiguradorTrama();
			if (!resultado.containsKey(key)) {
				List<ConfiguracionTramaDetalle> value = new ArrayList<>();
				value.add(obj);
				resultado.put(key, value);
			} else {
				List<ConfiguracionTramaDetalle> value = resultado.get(key);
				value.add(obj);
				resultado.put(key, value);
			}
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#listarTramaNomenclaturaArchivoDetalleMap
	 * (java.util.List, javax.persistence.EntityManager)
	 */
	@Override
	public Map<Long, List<TramaNomenclaturaArchivoDetalle>> listarNomenclaturaDetMap(
			List<Long> listaIdTramaNomenclaturaArchivo) {
		Map<Long, List<TramaNomenclaturaArchivoDetalle>> resultado = new HashMap<Long, List<TramaNomenclaturaArchivoDetalle>>();
		if (listaIdTramaNomenclaturaArchivo == null || listaIdTramaNomenclaturaArchivo.size() == 0) {
			return resultado;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(
				" from TramaNomenclaturaArchivoDetalle  o left join fetch o.tramaNomenclaturaArchivo  where  1=1 ");
		jpaql.append(
				" and o.tramaNomenclaturaArchivo.idTramaNomenclaturaArchivo in (:listaIdTramaNomenclaturaArchivo) ");
		parametros.put("listaIdTramaNomenclaturaArchivo", listaIdTramaNomenclaturaArchivo);
		jpaql.append(" order by o.numero");
		Query query = createQuery(jpaql.toString(), parametros);
		List<TramaNomenclaturaArchivoDetalle> resulTemp = query.getResultList();

		for (TramaNomenclaturaArchivoDetalle obj : resulTemp) {
			Long key = obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo();
			if (!resultado.containsKey(key)) {
				List<TramaNomenclaturaArchivoDetalle> value = new ArrayList<>();
				value.add(obj);
				resultado.put(key, value);
			} else {
				List<TramaNomenclaturaArchivoDetalle> value = resultado.get(key);
				value.add(obj);
				resultado.put(key, value);
			}
		}

		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#registrarTramaData(java.lang.
	 * StringBuilder)
	 */
	@Override
	public void registrarTramaData(StringBuilder sql, Map<String, Object> parametros) throws Exception {
		executeUpdatePreparedStatement(sql, parametros);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#obtenerNumeroLote(java.lang.String,
	 * java.lang.String, javax.persistence.EntityManager)
	 */
	@Override
	public String obtenerNumeroLote(String funncionLote, String codigoTratamiento) {
		String resultado = "";
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder();
		if (StringUtil.isNullOrEmpty(funncionLote)) {
			sql.append(" select em_k_sm_trama.f_num_lote(:codigoTratamiento) from dual ");
		} else {
			sql.append(" select " + funncionLote + "(:codigoTratamiento) from dual ");
		}
		parametros.put("codigoTratamiento", codigoTratamiento);
		Query query = createNativeQuery(sql.toString(), parametros);
		List<Object> listaData = query.getResultList();
		if (listaData.size() > 0) {
			resultado = (listaData.get(0) + "");
		}
		return resultado;
		/*
		 * Map<String, Object> parametros = TransferDataUtil.toEntityMap(obj);
		 * StringBuilder sql = saveInsert(obj.getClass());
		 * log.debug(" saveNative sql : " + sql); log.debug(" saveNative parametros : "
		 * + parametros); executeQuery(sql, parametros);
		 */
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#listarJuegoTramaCorreoMap(java.util.
	 * List, javax.persistence.EntityManager)
	 */
	@Override
	public Map<Long, List<JuegoTramaCorreo>> listarJuegoTramaCorreoMap(List<Long> listaIdJuegoTrama) {
		Map<Long, List<JuegoTramaCorreo>> resultado = new HashMap<Long, List<JuegoTramaCorreo>>();
		if (listaIdJuegoTrama == null || listaIdJuegoTrama.size() == 0) {
			return resultado;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(" from JuegoTramaCorreo  o left join fetch o.juegoTrama  where  1=1 ");
		jpaql.append(" and o.juegoTrama.idJuegoTrama in (:listaIdJuegoTrama) ");
		jpaql.append(" and o.estado =:estado ");
		parametros.put("estado", EstadoState.ACTIVO.getKey());
		parametros.put("listaIdJuegoTrama", listaIdJuegoTrama);
		Query query = createQuery(jpaql.toString(), parametros);
		List<JuegoTramaCorreo> resulTemp = query.getResultList();

		for (JuegoTramaCorreo obj : resulTemp) {
			Long key = obj.getJuegoTrama().getIdJuegoTrama();
			if (!resultado.containsKey(key)) {
				List<JuegoTramaCorreo> value = new ArrayList<JuegoTramaCorreo>();
				value.add(obj);
				resultado.put(key, value);
			} else {
				List<JuegoTramaCorreo> value = resultado.get(key);
				value.add(obj);
				resultado.put(key, value);
			}
		}

		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#listarConfiguracionFTPTramaMap(java.util
	 * .List, javax.persistence.EntityManager)
	 */
	@Override
	public Map<Long, List<ConfiguracionFtpTrama>> listarConfiguracionFTPTramaMap(List<Long> listaIdJuegoTrama) {
		Map<Long, List<ConfiguracionFtpTrama>> resultado = new HashMap<Long, List<ConfiguracionFtpTrama>>();
		if (listaIdJuegoTrama == null || listaIdJuegoTrama.size() == 0) {
			return resultado;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(" from ConfiguracionFtpTrama  o left join fetch o.juegoTrama  where  1=1 ");
		jpaql.append(" and o.juegoTrama.idJuegoTrama in (:listaIdJuegoTrama) ");
		parametros.put("listaIdJuegoTrama", listaIdJuegoTrama);
		Query query = createQuery(jpaql.toString(), parametros);
		List<ConfiguracionFtpTrama> resulTemp = query.getResultList();

		for (ConfiguracionFtpTrama obj : resulTemp) {
			Long key = obj.getJuegoTrama().getIdJuegoTrama();
			if (!resultado.containsKey(key)) {
				List<ConfiguracionFtpTrama> value = new ArrayList<ConfiguracionFtpTrama>();
				value.add(obj);
				resultado.put(key, value);
			} else {
				List<ConfiguracionFtpTrama> value = resultado.get(key);
				value.add(obj);
				resultado.put(key, value);
			}
		}

		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#ejecutarProgramaHomologacion(java.lang.
	 * String, java.lang.Object, javax.persistence.EntityManager)
	 */
	@Override
	public String ejecutarProgramaHomologacion(String programaHomologacion, Object valor) {
		String resultado = "";
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" select " + programaHomologacion + "(:parametro) from dual ");
		parametros.put("parametro", valor);
		Query query = createNativeQuery(sql.toString(), parametros);
		List<Object> listaData = query.getResultList();
		if (listaData.size() > 0) {
			resultado = (listaData.get(0) + "");
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#listarConfiguracionTramaErrorMap(java.
	 * util.List, javax.persistence.EntityManager)
	 */
	@Override
	public Map<Long, Map<String, String>> listarConfiguracionTramaErrorMap(List<Long> listaIdConfiguracionTrama) {
		Map<Long, Map<String, String>> resultado = new HashMap<Long, Map<String, String>>();
		if (listaIdConfiguracionTrama == null || listaIdConfiguracionTrama.size() == 0) {
			return resultado;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(
				"from ConfiguracionTramaError  o left join fetch o.configuracionTramaDetalle ct left join fetch ct.configuracionTrama  where  1=1");
		jpaql.append(
				" and o.configuracionTramaDetalle.configuracionTrama.idConfiguradorTrama in (:listaIdConfiguracionTrama) ");
		parametros.put("listaIdConfiguracionTrama", listaIdConfiguracionTrama);
		Query query = createQuery(jpaql.toString(), parametros);
		List<ConfiguracionTramaError> resulTemp = query.getResultList();
		for (ConfiguracionTramaError configuracionTramaErrorTemp : resulTemp) {
			Long key = configuracionTramaErrorTemp.getConfiguracionTramaDetalle().getConfiguracionTrama()
					.getIdConfiguradorTrama();
			String nombreCampo = configuracionTramaErrorTemp.getConfiguracionTramaDetalle().getNombeCampoTabla();
			boolean isCampoPersistente = !RespuestaNaturalType.NO.getKey()
					.equals(configuracionTramaErrorTemp.getConfiguracionTramaDetalle().getEsPersistente());
			if (!isCampoPersistente) {
				nombreCampo = configuracionTramaErrorTemp.getConfiguracionTramaDetalle().getNombreCampo();
			}
			if (!resultado.containsKey(key)) {
				Map<String, String> value = new HashMap<String, String>();
				value.put(configuracionTramaErrorTemp.getNombreCampo(), nombreCampo);
				resultado.put(key, value);
			} else {
				Map<String, String> value = resultado.get(key);
				value.put(configuracionTramaErrorTemp.getNombreCampo(), nombreCampo);
				resultado.put(key, value);
			}
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#obtenerCargaArchivoTrama(java.lang.Long,
	 * javax.persistence.EntityManager)
	 */
	@Override
	public CargaArchivoTramaVO obtenerCargaArchivoTrama(Long idCargaArchivo) throws Exception {
		CargaArchivoTramaVO resultado = new CargaArchivoTramaVO();
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(
				"select n_id_cab_carga_archivo as idCargaArchivo,c_nro_poliza as numeroPoliza,nro_solicitud as numeroSolicitud,");
		sql.append(" n_id_config_juego_trama as idJuegoTrama from sgsm_cab_carga_archivo  where  1=1 ");
		sql.append(" and n_id_cab_carga_archivo =:idCargaArchivo");
		parametros.put("idCargaArchivo", idCargaArchivo);
		if (!StringUtil.isNullOrEmptyNumeric(idCargaArchivo)) {
			resultado = executeQuery(sql, parametros, CargaArchivoTramaVO.class);
		} else {
			log.error("obtenerCargaArchivoTrama error no tiene idCargaArchivo " + idCargaArchivo);
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#ejecutarProcesoTramaEmision(javax.
	 * persistence.EntityManager)
	 */
	@Override
	public String ejecutarProcesoTramaEmision() throws Exception {
		String resultado = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("{call tron2000.em_k_sm_sgsm_generador_tramas.generar_trama_procesar(?)}");
			ScriptSqlResulJDBCVO data = executeQuerySP(sql);
			if (!data.isTieneError() && data.getListaData().size() > 0) {
				resultado = data.getListaData().get(0).get("resultado1") + "";
			}
		} catch (Exception e) {
			log.error("Error ", e);
			resultado = " ERROR ejecutarProcesoTramaEmision:" + e.getMessage();
		}
		log.error("resultado  ejecutarProcesoTramaEmision " + resultado);
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#ejecutarProcesoTramaEmisionMarcado(javax
	 * .persistence.EntityManager)
	 */
	@Override
	public String ejecutarProcesoTramaEmisionMarcado() throws Exception {
		String resultado = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("{call tron2000.p_marcar_para_aplicar(?)}");
			ScriptSqlResulJDBCVO data = executeQuerySP(sql);
			if (!data.isTieneError() && data.getListaData().size() > 0) {
				resultado = data.getListaData().get(0).get("resultado1") + "";
			}
		} catch (Exception e) {
			log.error("Error ", e);
			resultado = " ERROR ejecutarProcesoTramaEmision:" + e.getMessage();
		}
		log.error("resultado  ejecutarProcesoTramaEmision " + resultado);
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#ejecutarProcesoTramaEmisionCobros(javax.
	 * persistence.EntityManager)
	 */
	@Override
	public String ejecutarProcesoTramaEmisionCobros() throws Exception {
		String resultado = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("{call tron2000.p_cobrar_recibos_aplicados(?)}");
			ScriptSqlResulJDBCVO data = executeQuerySP(sql);
			if (!data.isTieneError() && data.getListaData().size() > 0) {
				resultado = data.getListaData().get(0).get("resultado1") + "";
			}
		} catch (Exception e) {
			log.error("Error ", e);
			resultado = " ERROR ejecutarProcesoTramaEmision:" + e.getMessage();
		}
		log.error("resultado  ejecutarProcesoTramaEmision " + resultado);
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#registrarFlagTasmeEDGP(java.lang.String,
	 * java.util.Date, java.lang.String)
	 */
	@Override
	public void registrarFlagTasmeEDGP(String numeroLote, Date fechaLote, String flagTipoResultadoEmis)
			throws Exception {
		if (!StringUtil.isNullOrEmpty(fechaLote) && !StringUtil.isNullOrEmpty(numeroLote)) {
			Map<String, Object> parametros = new HashMap<>();
			StringBuilder sql = new StringBuilder();
			sql.append("update TASMEDGP_MPE set tip_resultado_emis='" + flagTipoResultadoEmis + "'");
			sql.append(" where NUM_LOTE = '" + numeroLote + "'");
			sql.append(" and to_char(FEC_LOTE,'dd/MM/yyyy') = '"
					+ FechaUtil.obtenerFechaFormatoPersonalizado(fechaLote, "dd/MM/yyyy") + "'");
			executeUpdate(sql, parametros);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#registrarFlagTasmeEDGP(java.util.Map,
	 * java.lang.String)
	 */
	@Override
	public void registrarFlagTasmeEDGP(Map<String, Object> parametrosMap, String flagTipoResultadoEmis)
			throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("update TASMEDGP_MPE set tip_resultado_emis='" + flagTipoResultadoEmis + "'");
		sql.append(" where 1 = 1  ");
		boolean ejecutarQuery = true;
		for (Map.Entry<String, Object> dataMap : parametrosMap.entrySet()) {
			if (StringUtil.isNullOrEmpty(dataMap.getValue())) {
				ejecutarQuery = false;
				break;
			}
			if (dataMap.getValue().getClass().isAssignableFrom(Date.class)) {
				sql.append(" and to_char(" + dataMap.getKey() + ",'dd/MM/yyyy') = '"
						+ FechaUtil.obtenerFechaFormatoPersonalizado((Date) dataMap.getValue(), "dd/MM/yyyy") + "'");
			} else if (dataMap.getValue().getClass().isAssignableFrom(String.class)) {
				sql.append(" and  " + dataMap.getKey() + " = '" + dataMap.getValue() + "'");
			} else if (dataMap.getValue().getClass().isAssignableFrom(Character.class)) {
				sql.append(" and  " + dataMap.getKey() + " = '" + dataMap.getValue() + "'");
			} else {
				sql.append(" and  " + dataMap.getKey() + " = " + dataMap.getValue());
			}

		}
		if (ejecutarQuery && parametrosMap != null && parametrosMap.size() > 0) {
			executeUpdate(sql, parametrosMap);
		}
	}

	// jpaql.append(" select n_id_proc_trama,c_num_ref,n_id_juego, d_fec_reg
	// c_origen,c_nom_archivo,c_usuario from SGSM_PROCESA_TRAMA o where 1=1");
	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#obtenerNomenclaturaExcluirMap(java.util.
	 * Map)
	 */
	@Override
	public Map<String, String> obtenerNomenclaturaExcluirMap(Map<String, String> nomenclaturaExcluirMap)
			throws Exception {
		List<String> nomenclaturaExcluirList = new ArrayList<String>(nomenclaturaExcluirMap.values());
		Map<String, String> resultado = new HashMap<String, String>();
		if (nomenclaturaExcluirList == null || nomenclaturaExcluirList.size() == 0) {
			return resultado;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(" select c_nom_archivo ,n_id_juego from SGSM_PROCESA_TRAMA o  where  1=1");
		jpaql.append(" and o.c_nom_archivo in (:nomenclaturaExcluirList) ");
		jpaql.append(" and to_char(o.d_fec_reg,'dd/MM/yyyy') =:d_fec_reg ");
		parametros.put("nomenclaturaExcluirList", nomenclaturaExcluirList);
		parametros.put("d_fec_reg",
				FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "dd/MM/yyyy"));
		Query query = createNativeQuery(jpaql.toString(), parametros);
		List<Object[]> resulTemp = query.getResultList();
		for (Object[] obj : resulTemp) {
			if (obj[0] != null) {
				String key = obj[0] + "";
				if (!resultado.containsKey(key)) {
					String value = obj[1] + "";
					resultado.put(key, value);
				}
			}
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#registrarNomenclaturaExcluirMap(java.
	 * util.Map)
	 */
	@Override
	public void registrarNomenclaturaExcluirMap(Map<String, Object> nomenclaturaExcluirDataMap) throws Exception {

		try {
			Map<String, Object> parametros = new HashMap<String, Object>();
			StringBuilder jpaql = new StringBuilder();
			Long id = generarIdNomenclaturaExcluir();
			nomenclaturaExcluirDataMap.put("n_id_proc_trama", id);
			nomenclaturaExcluirDataMap.put("d_fec_reg",
					FechaUtil.obtenerFechaFormatoCompleto(FechaUtil.obtenerFechaActual()));
			jpaql.append(
					" insert into SGSM_PROCESA_TRAMA (n_id_proc_trama,c_num_ref,n_id_juego, d_fec_reg, c_origen,c_nom_archivo,c_usuario) ");
			jpaql.append(
					" values (:n_id_proc_trama,:c_num_ref,:n_id_juego, to_date(:d_fec_reg,'dd/MM/yyyy HH24:MI:SS') , :c_origen,:c_nom_archivo,:c_usuario) ");
			parametros.putAll(nomenclaturaExcluirDataMap);
			executeUpdate(jpaql, parametros);
		} catch (Exception e) {
			log.error("Error ", e);
		}
	}

	/**
	 * Generar id nomenclatura excluir.
	 *
	 * @return the long
	 * @throws Exception
	 *             the exception
	 */
	public Long generarIdNomenclaturaExcluir() throws Exception {
		Long resultado = 1L;
		Query query = createNativeQuery("select max(o.n_id_proc_trama) from SGSM_PROCESA_TRAMA o", null);
		List<Object> listLong = query.getResultList();
		if (listLong != null && listLong.size() > 0 && listLong.get(0) != null) {
			Long ultimoIdGenerado = Long.valueOf(listLong.get(0).toString());
			if (!StringUtil.isNullOrEmpty(ultimoIdGenerado)) {
				resultado = resultado + ultimoIdGenerado;
			}
		}
		return resultado;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.automatico.local.
	 * ConfiguracionTramaAutomaticoDaoLocal#listarEquivalenciaListCanalProducto(java
	 * .util.List, java.util.List)
	 */
	@Override
	public List<EquivalenciaDato> listarEquivalenciaListCanalProducto(List<String> listaProducto,
			List<String> listaCanal) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(" select o from EquivalenciaDato o left join fetch o.equivalenciaDato where 1=1 ");

		if (!StringUtil.isNullOrEmpty(listaProducto)) {
			jpaql.append(obtenerParametroSqlListaIn("PRODUCTO", "o.producto", listaProducto, true));
			parametros.putAll(obtenerParametroListaIn("PRODUCTO", listaProducto));
		}

		if (!StringUtil.isNullOrEmpty(listaCanal)) {
			jpaql.append(obtenerParametroSqlListaIn("CANAL", "o.canal", listaCanal, true));
			parametros.putAll(obtenerParametroListaIn("CANAL", listaCanal));
		}
		jpaql.append(" and o.estado =:estado ");
		parametros.put("estado", EstadoState.ACTIVO.getKey());

		Query query = createQuery(jpaql.toString(), parametros);
		return query.getResultList();
	}

}