package pe.com.mapfre.serviciotrama.domain.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;
import javax.persistence.EntityManager;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ConfiguracionTramaDataVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.GrupoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.RespuestaLecturaArchivoVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.ResultadoProcesoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.TramaVO;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionFtpTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

/**
 * La Class ITramaService.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface ITramaService {

	Map<Long, List<ConfiguracionFtpTrama>> listarConfiguracionFTPTramaMap(List<Long> listaIdJuegoTrama)
			throws Exception;

	String obtenerNomenclaturaPer(ConfiguracionTrama configuracionTrama, Map<String, Object> parametrosMap,
			boolean isManual, boolean esSimulacion) throws Exception;

	/**
	 * Generar tupa persistente.
	 *
	 * @param listaDataResulGrupoMap
	 *            the lista data resul grupo map
	 * @param errorTuplaMap
	 *            the error tupla map
	 * @param schemaTableName
	 *            the schema table name
	 * @param configuracionTramaDetalleMap
	 *            the configuracion trama detalle map
	 * @param campoNoPersistente
	 *            the campo no persistente
	 * @param parametroMap
	 *            the parametro map
	 * @return the big memory manager
	 * @throws Exception
	 *             the exception
	 */
	BigMemoryManager<String, ConfiguracionTramaDataVO> generarTupaPersistente(
			List<Map<String, ValueDataVO>> listaDataResulGrupoMap, Map<Integer, Boolean> errorTuplaMap,
			String[] schemaTableName, Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap,
			Map<String, String> campoNoPersistente, Map<String, Object> parametroMap) throws Exception;

	/**
	 * Registrar trama data.
	 *
	 * @param parametroMap
	 *            the parametro map
	 * @param grupoConfiguracionTramaVO
	 *            the grupo configuracion trama vo
	 * @param configuracionTramaDetalleMap
	 *            the configuracion trama detalle map
	 * @param listaConfiguracionTramaErrorMap
	 *            the lista configuracion trama error map
	 * @param fechaLote
	 *            the fecha lote
	 * @param numeroLote
	 *            the numero lote
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the respuesta lectura archivo vo
	 */
	RespuestaLecturaArchivoVO registrarTramaData(Map<String, Object> parametroMap,
			GrupoConfiguracionTramaVO grupoConfiguracionTramaVO,
			Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap,
			Map<Long, Map<String, String>> listaConfiguracionTramaErrorMap, Date fechaLote, String numeroLote,
			EntityManager sessionHibernate);

	/**
	 * Procesar configuracion trama.
	 *
	 * @param parametroMap
	 *            el parametro map
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	List<ResultadoProcesoConfiguracionTramaVO> procesarConfiguracionTrama(TramaVO parametroMap) throws Exception;

	/**
	 * Procesar configuracion trama.
	 *
	 * @param parametroMap
	 *            el parametro map
	 * @param listaConfiguracionTrama
	 *            el lista configuracion trama
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	List<ResultadoProcesoConfiguracionTramaVO> procesarConfiguracionTrama(Map<String, Object> parametroMap,
			List<ConfiguracionTrama> listaConfiguracionTrama) throws Exception;

	/**
	 * Listar configuracion trama.
	 *
	 * @param configuracionTrama
	 *            el configuracion trama
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	List<ConfiguracionTrama> listarConfiguracionTrama(ConfiguracionTrama configuracionTrama);

	Map<String, Object> obtenerVariableRespuestaLecturaArchivo(List<ConfiguracionTrama> listaConfiguracionTrama,
			Map<String, Object> parametroMap);
}
