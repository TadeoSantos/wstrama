package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class TasmctrLoteMpePK.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Embeddable
public class TasmctrLoteMpePK implements Serializable {
 
	/** La Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** El fecha lote. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "FEC_LOTE")
	private Date fechaLote;
	
	/** El numero lote. */
	@Column(name = "NUM_LOTE" , length = 10)
	private String numeroLote;
	
	
	/**
	 * Instancia un nuevo tasmctrl mpePK.
	 */
	public TasmctrLoteMpePK() {
	}
	
	
	/**
	 * Instancia un nuevo tasmctrl mpePK.
	 *
	 * @param fechaLote el fecha lote
	 * @param numeroLote el numero lote
	 */
	public TasmctrLoteMpePK(Date fechaLote, String numeroLote ) {
		super();
		this.fechaLote = fechaLote;
		this.numeroLote = numeroLote;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fechaLote == null) ? 0 : fechaLote.hashCode());
		result = prime * result
				+ ((numeroLote == null) ? 0 : numeroLote.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TasmctrLoteMpePK other = (TasmctrLoteMpePK) obj;
		if (fechaLote == null) {
			if (other.fechaLote != null) {
				return false;
			}
		} else if (!fechaLote.equals(other.fechaLote)) {
			return false;
		}
		if (numeroLote == null) {
			if (other.numeroLote != null) {
				return false;
			}
		} else if (!numeroLote.equals(other.numeroLote)) {
			return false;
		}
		return true;
	}
     
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TasmctrlMpe [fechaLote= " + fechaLote + " ,numeroLote= " + numeroLote + " ]";
	}
	
}