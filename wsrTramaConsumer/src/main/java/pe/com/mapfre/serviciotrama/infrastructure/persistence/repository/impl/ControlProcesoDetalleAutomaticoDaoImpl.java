package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.Date;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ConfiguracionTramaDataVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.GrupoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfiguracionTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms.UUIDUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProceso;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProcesoDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoDetalleAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.ConfiguracionCacheUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.EstadoDetalleProcesoType;

/**
 * La Class ControlProcesoDetalleAutomaticoDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ControlProcesoDetalleAutomaticoDaoImpl extends  GenericRepository<Long, ControlProcesoDetalle> implements ControlProcesoDetalleAutomaticoDaoLocal  {

	private Logger log = LoggerFactory.getLogger(ControlProcesoDetalleAutomaticoDaoImpl.class);
	
    @Override
    public boolean registrar(TasmctrLoteMpe tasmctrlMpe,GrupoConfiguracionTramaVO configuracionTramaCnfVO,Map<String,Boolean> juegoConfiguracionTramaErrorMap,Map<String, Object> parametroMap) {
    	boolean resultado = true;
    	try {
    		 for (GrupoConfiguracionTramaVO configuracionTramaCnf: configuracionTramaCnfVO.getListaConfiguracionTrama()) {
    	    	 for (String configuracionTramaDataKey: configuracionTramaCnf.getListaConfiguracionTramaData().getListaKey()) {
    	    		 ConfiguracionTramaDataVO configuracionTramaData = configuracionTramaCnf.getListaConfiguracionTramaData().get(configuracionTramaDataKey);
    	    		 for (ConfiguracionTramaDataVO objValue : configuracionTramaData.getListaCampoValue()) {
    	    			  if (objValue.isEsCampoNegocio()) {
    	    				  ControlProcesoDetalle controlProcesoDetalle = new ControlProcesoDetalle();
    	    				  String estadoControlProcesoDetalle = "";
    	    				  controlProcesoDetalle.setIdDetalleProcesoFlujo(UUIDUtil.generarElementUUID());//UUID
    	    				  controlProcesoDetalle.setControlProceso(new ControlProceso());
    	    				  controlProcesoDetalle.getControlProceso().setIdControlProceso(tasmctrlMpe.getIdControlProceso());
    	    				  boolean juegoConfiguracionTramaError = juegoConfiguracionTramaErrorMap.containsKey(objValue.getAtributeValue() + "");
    	    				  if (!juegoConfiguracionTramaError) {
    	    					  estadoControlProcesoDetalle = EstadoDetalleProcesoType.SIN_ERROR.getKey();
    	    				   } else {
    	    					   estadoControlProcesoDetalle = EstadoDetalleProcesoType.CON_ERROR.getKey();
    	    				   }
    	    				  controlProcesoDetalle.setEstado(estadoControlProcesoDetalle);
    	    				  controlProcesoDetalle.setCodigoUsuario(parametroMap.get(ConstanteConfiguracionTramaUtil.USUARIO) + "");
    	    				  controlProcesoDetalle.setFechaActualizacion(new Date());
    	    				  controlProcesoDetalle.setFechaLote(tasmctrlMpe.getTasmctrLoteMpePK().getFechaLote());
    	    				  controlProcesoDetalle.setNumeroLote(tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote());
    	    				  controlProcesoDetalle.setIdJuego(Long.valueOf(configuracionTramaCnfVO.getNombreConfiguracion()));
    	    				  controlProcesoDetalle.setCampoIdentificadorNegocio(configuracionTramaCnf.getNombreTabla() + "." + objValue.getAtributeName());
    	    				  controlProcesoDetalle.setCampoIdentificadorNegocioValor(objValue.getAtributeValue() + "");
    	    				  //imprimir log de produccion
    	    				  if (ConfiguracionCacheUtil.getInstance().isGenerarLogTramaJuego(configuracionTramaCnfVO.getNombreConfiguracion())) {
    	    					 Map<String,Object> parametros = TransferDataUtil.toVOMap(controlProcesoDetalle);
    							 log.error("proceso.registrarTramaData.parametros.save(" + tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote() + ") --> " + parametros.toString());
    						  }
    	    				  saveNative(controlProcesoDetalle);
    	    			  }
    	    		 }
    	    	 }
    	    	 configuracionTramaCnf.getListaConfiguracionTramaData().reiniciar();
    	    }
		} catch (Exception e) {
			log.error("Error ",e);
			resultado = false;
		}
    	return resultado;
    }
}