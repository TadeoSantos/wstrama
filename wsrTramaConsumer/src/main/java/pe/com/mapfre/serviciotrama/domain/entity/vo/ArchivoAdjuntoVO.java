package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ArchivoAdjuntoVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ArchivoAdjuntoVO implements Serializable {
 
	/** La Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** El tamano adjunto. */
	private Long tamanoAdjunto;
	
	/** El nombre adjunto. */
	private String nombreAdjunto;
	
	/** El extension. */
	private String extension;
	
	/** El codigo archivo. */
	private String codigoArchivo;
	
	/**
	 * Instancia un nuevo archivo adjunto.
	 */
	public ArchivoAdjuntoVO() {
	}
	
	
	/**
	 * Instancia un nuevo archivo adjunto.
	 *
	 * @param tamanoAdjunto el tamano adjunto
	 * @param nombreAdjunto el nombre adjunto
	 * @param extension el extension
	 * @param codigoArchivo el codigo archivo
	 */
	public ArchivoAdjuntoVO(Long tamanoAdjunto, String nombreAdjunto, String extension, String codigoArchivo ) {
		super();
		this.tamanoAdjunto = tamanoAdjunto;
		this.nombreAdjunto = nombreAdjunto;
		this.extension = extension;
		this.codigoArchivo = codigoArchivo;
	}
	
}