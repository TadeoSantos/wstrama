package  pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class ComponenteValidadoLogTramaType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum ComponenteValidadoLogTramaType {

    /** El CANTIDAD. */
 	CANTIDAD("1" , "componenteValidadoLogTrama.cantidad"),
	
    /** El LONGITUD. */
 	LONGITUD("2" , "componenteValidadoLogTrama.longitud"),
	
    /** El TIPO. */
 	TIPO("3" , "componenteValidadoLogTrama.tipo"),
	
    /** El OBLIGATORIDAD. */
 	OBLIGATORIDAD("4" , "componenteValidadoLogTrama.obligatoridad"),
	
    /** El CONF_TRAMA. */
 	CONF_TRAMA("5" , "componenteValidadoLogTrama.conf_trama"),
	
    /** El CONF_TRAMA_OBLIGATORIDAD. */
 	CONF_TRAMA_OBLIGATORIDAD("6" , "componenteValidadoLogTrama.conf_trama_obligatoridad"),
 	
 	CONF_TRAMA_NO_EXISTE_ARCHIVO("7" , "componenteValidadoLogTrama.conf_trama_no_existe");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, ComponenteValidadoLogTramaType> LOO_KUP_MAP = new HashMap<String, ComponenteValidadoLogTramaType>();
	
	static {
		for (ComponenteValidadoLogTramaType s : EnumSet.allOf(ComponenteValidadoLogTramaType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo componente validado log trama type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private ComponenteValidadoLogTramaType(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the componente validado log trama type
	 */
	public static ComponenteValidadoLogTramaType get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
