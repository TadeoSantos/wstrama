package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;
/**
 * La Class ConstanteTypeUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public final class ConstanteTypeUtil {
	
	
	/** La Constante BUNDLE_NAME_TYPE. */
	public static final String BUNDLE_NAME_TYPE = "pe.com.mapfre.serviciosctr.infraestructura.vertical.type.properties-enum";

	//public static final String BUNDLE_NAME_TYPE = "pe.gob.mapfre.pwr.rep.model.type.properties-enum";

	/** La Constante BUNDLE_NAME_ASISTENCIA_TELECOBRANZA_TYPE. */
	public static final String BUNDLE_NAME_ASISTENCIA_TELECOBRANZA_TYPE = "pe.gob.mapfre.pwr.rep.model.asistencia.telecobranza.type.properties-enum";
		
	/** La Constante BUNDLE_NAME_TYPE. */
	public static final String BUNDLE_NAME_CONFIGURADOR_TRAMA_TYPE = "pe.gob.mapfre.pwr.rep.model.configurador.trama.type.properties-enum";
	
	/** La Constante BUNDLE_NAME_TYPE. */
	public static final String BUNDLE_NAME_MULTI_PRODUCTO_TYPE = "pe.gob.mapfre.pwr.rep.model.multi.producto.type.properties-enum";
	
	/** La Constante BUNDLE_NAME_ERROR_TYPE. */
	public static final String BUNDLE_NAME_ERROR_TYPE = "pe.gob.mapfre.pwr.rep.model.type.pwr-rep-error-type";
	
	/** La Constante BUNDLE_NAME_TIPO_ACCION_PLAN_TYPE. */
	public static final String BUNDLE_NAME_TIPO_PRODUCTO_SALUD_TYPE = "pe.gob.mapfre.pwr.rep.model.producto.salud.type.properties-enum";
	
	/**  La Constante BUNDLE_NAME_HISTORIAL_REPORTE_PRODUCCION_CUBOS. */
	public static final String BUNDLE_NAME_HISTORIAL_REPORTE_PRODUCCION_CUBOS = "pe.gob.mapfre.pwr.rep.model.historial.reporte.dtprod.type.properties-enum";
	
	/** LA Constante BUNDLE_NAME_ORQUESTADOR_FLUJO. */
	public static final String BUNDLE_NAME_ORQUESTADOR_FLUJO = "pe.gob.mapfre.pwr.rep.model.orquestador.flujo.type.properties-enum";
	
	/** The Constant BUNDLE_NAME_SINIESTRALIDAD_TYPE. */
	public static final String BUNDLE_NAME_SINIESTRALIDAD_TYPE = "pe.gob.mapfre.pwr.rep.model.reporte.siniestralidad.type.properties-enum";
																	 		
	public static final String BUNDLE_NAME_ORQUESTADOR_ERROR_TYPE = "pe.gob.mapfre.pwr.rep.model.orquestador.flujo.type.pwr-rep-error-type";
	
	public static final String BUNDLE_NAME_WEB_METHOD = "pe.gob.mapfre.pwr.rep.model.webmethod.rolregla.type.properties-enum";
	
	public static final String BUNDLE_NAME_COMUNICACION_CERTIFICADA_TYPE = "pe.com.mapfre.pwr.rep.model.comunicacion.certificada.type.properties-enum";
	
	public static final String BUNDLE_NAME_CARTERA_TYPE = "pe.gob.mapfre.pwr.rep.model.cartera.type.properties-enum";
	
	public static final String BUNDLE_NAME_RECAUDACION_MASIVO = "pe.gob.mapfre.pwr.rep.model.configurador.trama.recaudacion.type.properties-enum";
	
	public static final String BUNDLE_NAME_REPORTE_CALENDARIO = "pe.gob.mapfre.pwr.rep.model.reporte.calendario.type.properties-enum";
	
	public static final String BUNDLE_NAME_COBRO_RECURRENTE = "pe.gob.mapfre.pwr.rep.model.cobro.recurrente.type.properties-enum";
	
	public static final String BUNDLE_NAME_CUBOS_CALIDAD = "pe.gob.mapfre.pwr.rep.model.cubos.calidad.type.properties-enum";
	/**
	 * Instancia un nuevo constante type util.
	 */
	private ConstanteTypeUtil() {
		
	}
}
