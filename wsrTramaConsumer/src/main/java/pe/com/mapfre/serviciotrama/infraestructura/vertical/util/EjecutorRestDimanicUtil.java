package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.CommonsClientHttpRequestFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.mapfre.serviciotrama.domain.entity.vo.RespuestaWSVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.BasePaginator;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.AccionType;

/**
 * La Class EjecutorRestDimanicUtil.
 * <ul>
 * <li>Copyright 2014 MAPFRE - mapfre. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft.
 * @version 1.0, 23/05/2021
 * @since PWR v1.0
 */
public class EjecutorRestDimanicUtil extends ProxyWSUtil {

	public static final String TRANSACCTION_TIMEOUT = "86400";

	private Logger log = Logger.getLogger(EjecutorRestDimanicUtil.class);
	private String urlBaseKey;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Instancia un nuevo proceso seleccion ficha service impl.
	 */
	public EjecutorRestDimanicUtil() {
		// Constructor
	}

	public String toString(List<String> listaParametro) {
		return (listaParametro.toString().replace("[", "").replace("]", "")).trim();
	}

	public TypeReference<RespuestaWSVO<String>> getValueTypeRef() {
		return new TypeReference<RespuestaWSVO<String>>() {
		};
	}

	public  ObjectMapper objectMapper() {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return objectMapper;
	}

	private  String getUrl(String key) {
		return ConfiguracionActiveDirectoryWSUtil.getWebService(key);
	}

	public  Map<String, String> generarHeaderMap(boolean isOnline, String archivoName) {
		Map<String, String> resultado = new HashMap<String, String>();
		resultado.put("isBatch", (!isOnline) + "");
		resultado.put("ARCHIVO_NAME", archivoName);
		return resultado;
	}

	private  HttpHeaders obtenerHttpHeaders(String usuario, String password, String token,
			Map<String, String> paramHeader) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("usuario", usuario);
		headers.add("password", password);

		if (token != null && !token.equalsIgnoreCase("")) {
			headers.add("Authorization", "Bearer " + token);
		}
		if (paramHeader != null) {
			for (Map.Entry<String, String> objMap : paramHeader.entrySet()) {
				headers.add(objMap.getKey(), objMap.getValue());
			}
		}
		headers.add("Content-Type", "application/json;charset=UTF-8");

		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		headers.setAcceptCharset(Arrays.asList(Charset.forName("UTF-8")));

		return headers;
	}

	private  HttpHeaders obtenerHttpHeaders(Map<String, String> paramHeader) {
		return obtenerHttpHeaders(paramHeader.get("usuario"), paramHeader.get("password"), paramHeader.get("token"),
				paramHeader);
	}

	public  CustomRestTemplate restTemplate() {
		int readTimeout = Integer.parseInt(TRANSACCTION_TIMEOUT) * 100;// 10 min
		CommonsClientHttpRequestFactory factory = new CommonsClientHttpRequestFactory();
		factory.setReadTimeout(readTimeout);
		CustomRestTemplate restTemplate = new CustomRestTemplate(factory);
		restTemplate.setDefaultResponseContentType(MediaType.APPLICATION_JSON);
		StringHttpMessageConverterOverride stringHttpMessageConverter = new StringHttpMessageConverterOverride();
		restTemplate.getMessageConverters().set(1, stringHttpMessageConverter);
		restTemplate.setErrorHandler(new ErrorHandlerClientRest());
		return restTemplate;
	}

	public  <T> T post(String url, Object objData, Map<String, String> paramHeader, TypeReference<T> valueTypeRef)
			throws IOException {
		ObjectMapper objectMapper = objectMapper();
		ObjectMapper mapper = new ObjectMapper();
		String parametros = mapper.writeValueAsString(objData);
		CustomRestTemplate restTemplate = restTemplate();
		HttpHeaders headers = obtenerHttpHeaders(paramHeader);
		HttpEntity<String> entity = new HttpEntity<String>(parametros, headers);
		String urlFinal = getUrl(getUrlBaseKey()) + url;
		ResponseEntity<String> res = restTemplate.exchange(urlFinal, HttpMethod.POST, entity, String.class);
		String respJson = res.getBody();
		generarExepcion(res,urlFinal,valueTypeRef);
		return objectMapper.readValue(respJson, valueTypeRef);
	}

	public  <T,K> T put(String url, Object objData,Map<String, String> paramHeader, TypeReference<T> valueTypeRef)
			throws IOException {
		return put(url,objData,null,paramHeader,valueTypeRef);
	}
	public  <T,K> T put(String url, Object objData,  K pk,Map<String, String> paramHeader, TypeReference<T> valueTypeRef)
			throws IOException {
		ObjectMapper objectMapper = objectMapper();
		ObjectMapper mapper = new ObjectMapper();
		String parametros = mapper.writeValueAsString(objData);
		log.error(" INICIO  PUT - url= "+url+"  Parametros: "+ parametros );
		CustomRestTemplate restTemplate = restTemplate();
		HttpHeaders headers = obtenerHttpHeaders(paramHeader);
		HttpEntity<String> entity = new HttpEntity<String>(parametros, headers);
		String urlFinal = url ;
		if (pk != null) {
			urlFinal = url + "/" + pk;
		}
		String urlFinalCompleto = getUrl(getUrlBaseKey()) + urlFinal;
		ResponseEntity<String> res = restTemplate.exchange(urlFinalCompleto, HttpMethod.PUT, entity, String.class);
		generarExepcion(res,urlFinalCompleto,valueTypeRef);
		String respJson = res.getBody();
		log.error("FIN PUT - url= "+urlFinalCompleto+" Respuesta= "+res+" respuesta Json = "+respJson);
		
		return objectMapper.readValue(respJson, valueTypeRef);
		
	}

	private  String generarUrl(String url, String recurso, Map<String, Object> parametros) {
		StringBuilder uriBuilder = new StringBuilder(url + recurso);
		if (parametros != null && parametros.size() > 0) {
			uriBuilder.append("?");
			int contador = 0;
			for (Map.Entry<String, Object> param : parametros.entrySet()) {
				contador++;
				uriBuilder.append(param.getKey());
				uriBuilder.append("=");
				uriBuilder.append(param.getValue());
				if (contador != parametros.size())
					uriBuilder.append("&");
			}
		}
		return uriBuilder.toString();
	}

	public  <T> T get(String url, Map<String, Object> parametros, Map<String, String> paramHeader,
			TypeReference<T> valueTypeRef) throws IOException {
		ObjectMapper objectMapper = objectMapper();
		CustomRestTemplate restTemplate = restTemplate();
		HttpHeaders headers = obtenerHttpHeaders(paramHeader);
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		String urlFinal = getUrl(getUrlBaseKey()) + url;
		ResponseEntity<String> res = restTemplate.exchange(generarUrl(urlFinal, "", parametros), HttpMethod.GET, entity, String.class);
		generarExepcion(res,urlFinal,valueTypeRef);
		String respJson = res.getBody();
		return objectMapper.readValue(respJson, valueTypeRef);
	}
	

	public  <T, K> T delete(String url, K pk, Map<String, String> paramHeader, TypeReference<T> valueTypeRef,
			boolean isUrriInfo) throws IOException {
		ObjectMapper objectMapper = objectMapper();
		CustomRestTemplate restTemplate = restTemplate();
		HttpHeaders headers = obtenerHttpHeaders(paramHeader);
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		String urlFinal = url + "/" + pk;
		if (isUrriInfo) {
			urlFinal = generarUrl(url, "", putParametros(pk));
		}
		String urlFinalCompleta = getUrl(getUrlBaseKey()) + urlFinal;
		ResponseEntity<String> res = restTemplate.exchange(urlFinalCompleta, HttpMethod.DELETE, entity, String.class);
		generarExepcion(res,urlFinalCompleta,valueTypeRef);
		String respJson = res.getBody();
		return objectMapper.readValue(respJson, valueTypeRef);
	}
	private <T> void generarExepcion(ResponseEntity<String> res,String url,TypeReference<T> valueTypeRef) throws IOException {
		if (HttpStatus.NO_CONTENT.equals(res.getStatusCode())) {
			throw new IOException("Error " +  url +" statusCode = " + res.getStatusCode().value() + "  NOT_FOUND");
		} if (HttpStatus.BAD_REQUEST.equals(res.getStatusCode()) || HttpStatus.NOT_FOUND.equals(res.getStatusCode())) {
			throw new IOException("Error " +  url +" statusCode = " + res.getStatusCode().value() + "");
		} else if (HttpStatus.INTERNAL_SERVER_ERROR.equals(res.getStatusCode())) {
			ObjectMapper objectMapper = objectMapper();
			String respJson = res.getBody();
			RespuestaWSVO<T> dataRes = objectMapper.readValue(respJson, valueTypeRef);
			throw new IOException("Error " +  url +" statusCode = " + res.getStatusCode().value() + " codigoError = " + dataRes.getCodigoError() + " mensajeError = " + dataRes.getMensajeError());
		}
		
	}

	public  <T,E, K> T controladorAccionRest(String url, E obj, K pk, AccionType accionType)
			throws IOException {
		return (T) controladorAccionRest(url, obj, pk, accionType, true,getValueTypeRef());
	}
	
	public  <T,E, K> T controladorAccionRest(String url, E obj, K pk, AccionType accionType,TypeReference<T> valueTypeRef)
			throws IOException {
		return controladorAccionRest(url, obj, pk, accionType, true,valueTypeRef);
	}

	public  <T,E, K> T controladorAccionRest(String url, E obj, K pk, AccionType accionType,
			boolean isUrriInfo) throws IOException {
		return (T) controladorAccionRest(url,obj,pk,accionType,isUrriInfo,getValueTypeRef());
	}
	public  <T,E, K> T controladorAccionRest(String url, E obj, K pk, AccionType accionType,
			boolean isUrriInfo,TypeReference<T> valueTypeRef) throws IOException {
		switch (accionType) {
		case CREAR:
			return post(url, obj, putParametrosHeader(), valueTypeRef);
		case MODIFICAR:
			return put(url, obj, pk,putParametrosHeader(), valueTypeRef);
		case ELIMINAR:
			return delete(url, pk, putParametrosHeader(),valueTypeRef, isUrriInfo);

		default:
			return null;
		}
	}

	public  Map<String, Object> putParametro(String key, Object value) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(key, value);
		return map;
	}

	public  Map<String, String> putParametrosHeader() {
		return new HashMap<String, String>();
	}

	public  <T> Map<String, Object> putParametros(T obj) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		if (obj != null) {
			parametros = TransferDataUtil.toVOMap(obj);
			if (obj instanceof BasePaginator) { // Algunos no reconocen BasePaginator
				parametros = TransferDataUtil.toVOMap(obj);
				parametros.put("startRow", ((BasePaginator) obj).getStartRow());
				parametros.put("offset", ((BasePaginator) obj).getOffset());
				parametros.put("offSet", ((BasePaginator) obj).getOffset());
			}
		}
		return parametros;
	}

	public String getUrlBaseKey() {
		return urlBaseKey;
	}

	public void setUrlBaseKey(String urlBaseKey) {
		this.urlBaseKey = urlBaseKey;
	}
	
}