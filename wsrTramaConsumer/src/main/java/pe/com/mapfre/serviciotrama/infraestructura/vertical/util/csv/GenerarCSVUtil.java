package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.csv;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opencsv.CSVWriter;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfigUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;

/**
 * La Class GenerarCSVUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class GenerarCSVUtil {

	/** 
	 * Logger para el registro de errores.
	 */
	private static final Logger log = Logger.getLogger(TransferDataUtil.class);

	/** La Constante RUTA_RECURSOS. */
	public static final String RUTA_RECURSOS_BYTE_BUFFER = ConstanteConfigUtil.RUTA_RECURSOS_BYTE_BUFFER;
	

	public static void generarCSV(List<Map<String,Object>> listaHeader, List<Map<String,Object>> listaData,String carpeta, String archivoName) {
		CSVWriter writer = null;
		try {
			// Inicio BuildSoft Reporte Fon Bienes
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER +(StringUtil.isNullOrEmpty(carpeta)  ? "" : ConstanteConfigUtil.generarRuta(carpeta)));
			// Fin BuildSoft Reporte Fon Bienes
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			// Inicio BuildSoft Reporte Fon Bienes
			writer = new CSVWriter(new FileWriter(RUTA_RECURSOS_BYTE_BUFFER + (StringUtil.isNullOrEmpty(carpeta)  ? "" : ConstanteConfigUtil.generarRuta(carpeta)) + "" + archivoName + ".csv"),';');
			synchronized(writer) {
			// Fin BuildSoft Reporte Fon Bienes
			//List<String[]> dataPersist = new ArrayList<String[]>();
			String[] header = new String[listaHeader.size()];
			int i = 0;
			for (Map<String,Object> objHeader : listaHeader) {
					header[i]  = objHeader.get("VALUE_HEADER") + "";
				i++;
			}
			//dataPersist.add(header);
			writer.writeNext(header);
			writer.flush();	
			int contador = 0;
			for (Map<String,Object> objDataTemp : listaData) {
				i = 0;
				String[] data = new String[listaHeader.size()];
				for (Map<String,Object> objHeader : listaHeader) {
					data[i]  = objDataTemp.get(objHeader.get("KEY_HEADER")) + "";
					i++;
				}
				//dataPersist.add(data);
				contador++;
				writer.writeNext(data);
				if (contador == 20000) {
					contador = 0;
					writer.flush();	
				}
			}
			writer.close();
		  }
		} catch (Exception e) {
			log.error("Error",e);
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e1) {
					log.error("Error",e);
				}	
			}
		}		
	}
}