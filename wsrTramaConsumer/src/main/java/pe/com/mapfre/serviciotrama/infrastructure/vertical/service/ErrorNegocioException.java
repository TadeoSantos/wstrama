package pe.com.mapfre.serviciotrama.infrastructure.vertical.service;

public class ErrorNegocioException  extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
	private String codigoOperacion;

	public ErrorNegocioException(String exception, String codigoOperacion){
		super(exception);
		this.codigoOperacion=codigoOperacion;
	}

	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	
}
