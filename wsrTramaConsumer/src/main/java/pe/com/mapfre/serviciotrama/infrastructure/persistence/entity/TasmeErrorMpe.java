/*
 * 
 */
package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class TasmeErrorMpe.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "TASMEERR_MPE", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class TasmeErrorMpe extends BasePaginator implements Serializable {
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El tasme error mpePK. */
    @Id
    @EmbeddedId
    private  TasmeErrorMpePK tasmeErrorMpePK;
    
    /** El numero poliza tron. */
    @Column(name = "NUM_POLIZA_TRON" , length = 13)
    private String numeroPolizaTron;
   
    /** El nivel error. */
    @Column(name = "COD_NIVEL_PROCESO" , length = 1)
    private String nivelError;
   
    /** El texto error. */
    @Column(name = "TXT_ERROR" , length = 4000)
    private String textoError;
    
    /** La fila. */
    @Column(name = "C_LINEA_ERROR" , length = 20)
    private Long fila;
    
    /** La codigo error. */
    @Column(name = "NUM_COD_ERROR" , precision = 20, scale = 0)
    private Long codigoError;
    
    @Column(name = "C_TIP_ERROR" , length = 1)
    private String tipoError;
    
    @Column(name = "NUM_HOJA" , precision = 20, scale = 0)
    private Long numeroHoja;
   
    /** La id configurador trama. */
    @Transient
    private Long idConfiguradorTrama;
    
    /** La nombre campo. */
    @Transient
    private String nombreCampo;
    
    /**
     * Instancia un nuevo tasme error mpe.
     */
    public TasmeErrorMpe() {
    }
   
    /**
     * Instancia un nuevo tasme error mpe.
     *
     * @param tasmeErrorMpePK el tasme error mpe pk
     * @param numeroPolizaTron el numero poliza tron
     * @param nivelError el nivel error
     * @param textoError el texto error
     * @param fila el fila
     * @param codigoError el codigo error
     */
    public TasmeErrorMpe(TasmeErrorMpePK tasmeErrorMpePK, String numeroPolizaTron, String nivelError, String textoError, Long fila, Long codigoError, String tipoError) {
		super();
		this.tasmeErrorMpePK = tasmeErrorMpePK;
		this.numeroPolizaTron = numeroPolizaTron;
		this.nivelError = nivelError;
		this.textoError = textoError;
		this.fila = fila;
		this.codigoError = codigoError;
		this.tipoError = tipoError;
	}

    
	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((tasmeErrorMpePK == null) ? 0 : tasmeErrorMpePK.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TasmeErrorMpe other = (TasmeErrorMpe) obj;
        if (tasmeErrorMpePK== null) {
            if (other.tasmeErrorMpePK != null) {
                return false;
            }
        } else if (!tasmeErrorMpePK.equals(other.tasmeErrorMpePK)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TasmeErrorMpe [tasmeErrorMpePK=" + tasmeErrorMpePK + "]";
    }
   
}