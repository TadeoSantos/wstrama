package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class BasePaginator.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class BasePaginator implements Serializable {
	
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La offset. */
	private int offset;
	
	/** La start row. */
	private int startRow;
	
	private boolean check;
	
	private String descripcionView;

	public boolean isCheck() {
		return check;
	}

}
