package pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;


// TODO: Auto-generated Javadoc
/**
 * La Class VariableProcesoType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum VariableProcesoType {
	

 	/** La username. */
	 USERNAME("userName", TipoDatoFlujoType.TEXTO.getKey()),
	
 	/** La password. */
	 PASSWORD("password", TipoDatoFlujoType.TEXTO.getKey()),
 	
 	/** La id proceso flujo. */
	 ID_PROCESO_FLUJO("idProcesoFlujo", TipoDatoFlujoType.NUMERICO.getKey()),
 	
 	/** La id juego trama. */
	 ID_JUEGO_TRAMA("idJuegoTrama", TipoDatoFlujoType.NUMERICO.getKey()),
	 
	 /** La id control proceso. */
	 ID_CONTROL_PROCESO("idControlProceso", TipoDatoFlujoType.TEXTO.getKey()),
 	
 	/** La numero poliza. */
	 NUMERO_POLIZA("numeroPoliza", TipoDatoFlujoType.TEXTO.getKey()),
 	
 	/** La numero solicitud. */
	 NUMERO_SOLICITUD("numeroSolicitud", TipoDatoFlujoType.TEXTO.getKey()),
 	
 	/** La origen. */
	 ORIGEN("origen", TipoDatoFlujoType.TEXTO.getKey()),
	 
	 /**  La exclusion. */
	 EXCLUSION("validarExclusion", TipoDatoFlujoType.BOOLEANO.getKey()),
	 
	 /** La indicador interno actividad. */
 	INDICADOR_INTERNO_ACTIVIDAD("indicadorInternoActividad", TipoDatoFlujoType.NUMERICO.getKey()),
	
	 /*Nuevo Proceso SP Inicio*/	 	
 	/** La codigo de cia. */
	 CODIGO_CIA("CODIGO_CIA", TipoDatoFlujoType.NUMERICO.getKey()),
	 
	 /**  La numero poliza SP. */
	 NUMERO_POLIZA_SP("NUMERO_POLIZA_SP", TipoDatoFlujoType.TEXTO.getKey()),
	 
	 /** La numero de suplemento. */
	 NUMERO_SUPLEMENTO("NUMERO_SUPLEMENTO", TipoDatoFlujoType.NUMERICO.getKey()),
	 
	 /** La numero de aplicativo. */
	 NUMERO_APLI("NUMERO_APLI", TipoDatoFlujoType.NUMERICO.getKey()),
	 
	 /** La numero de suplemento aplicativo. */
	 NUMERO_SUPLEMENTO_APLI("NUMERO_SUPLEMENTO_APLI", TipoDatoFlujoType.NUMERICO.getKey());
	 /*Nuevo Proceso SP Fin*/
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, VariableProcesoType> LOO_KUP_MAP = new HashMap<String, VariableProcesoType>();
	
	static {
		for (VariableProcesoType s : EnumSet.allOf(VariableProcesoType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private Long tipo;

	/**
	 * Instancia un nuevo tipo ejecucion flujo type.
	 *
	 * @param key el key
	 * @param tipo La tipo
	 */
	private VariableProcesoType(String key, Long tipo) {
		this.key = key;
		this.tipo = tipo;
	}
	
	/**
	 * Gets La.
	 *
	 * @param key el key
	 * @return La tipo ejecucion flujo type
	 */
	public static VariableProcesoType get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public Long getTipo() {
		return tipo;
	}
	
}
