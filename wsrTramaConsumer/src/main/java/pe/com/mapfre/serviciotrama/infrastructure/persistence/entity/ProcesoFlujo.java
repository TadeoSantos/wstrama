package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ProcesoFlujo.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_ORQ_PROC_FLU", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ProcesoFlujo implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id proceso flujo. */
    @Id
    @Column(name = "N_ID_PROC_FLU" , length = 18)
    private Long idProcesoFlujo;
   
    /** El nombre proceso flujo. */
    @Column(name = "C_NOM_FLU" , length = 100)
    private String nombreProcesoFlujo;
   
    /** El estado flujo. */
    @Column(name = "C_ESTADO_FLU" , length = 1)
    private String estadoFlujo;
   
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
   
    /** El proceso flujo proceso variable list. */
    @OneToMany(mappedBy = "procesoFlujo", fetch = FetchType.LAZY)
    private List<ProcesoVariable> procesoFlujoProcesoVariableList = new ArrayList<ProcesoVariable>();
    
    /** El proceso flujo actividad flujo list. */
    @OneToMany(mappedBy = "procesoFlujo", fetch = FetchType.LAZY)
    private List<ActividadFlujo> procesoFlujoActividadFlujoList = new ArrayList<ActividadFlujo>();
    
    /**
     * Instancia un nuevo proceso flujo.
     */
    public ProcesoFlujo() {
    }
   
   
    /**
     * Instancia un nuevo proceso flujo.
     *
     * @param idProcesoFlujo el id proceso flujo
     * @param nombreProcesoFlujo el nombre proceso flujo
     * @param estadoFlujo el estado flujo
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public ProcesoFlujo(Long idProcesoFlujo, String nombreProcesoFlujo, String estadoFlujo, String usuarioCrea, Date fechaActualizacion ) {
        super();
        this.idProcesoFlujo = idProcesoFlujo;
        this.nombreProcesoFlujo = nombreProcesoFlujo;
        this.estadoFlujo = estadoFlujo;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idProcesoFlujo == null) ? 0 : idProcesoFlujo.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProcesoFlujo other = (ProcesoFlujo) obj;
        if (idProcesoFlujo == null) {
            if (other.idProcesoFlujo != null) {
                return false;
            }
        } else if (!idProcesoFlujo.equals(other.idProcesoFlujo)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ProcesoFlujo [idProcesoFlujo=" + idProcesoFlujo + "]";
    }
   
}