package pe.com.mapfre.serviciotrama.application.entity;

import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.BaseRespuesta;

/**
*
* @author BuildSoft
*/
public class RespuestaClienteDTO <T> extends BaseRespuesta{
	
	private T objtoResultado;

	/**
	 * @return the objtoResultado
	 */
	public T getObjtoResultado() {
		return objtoResultado;
	}

	/**
	 * @param objtoResultado the objtoResultado to set
	 */
	public void setObjtoResultado(T objtoResultado) {
		this.objtoResultado = objtoResultado;
	}
	
	public boolean isError() {
		return !"0".equals(super.getCodError());
	}
	
}