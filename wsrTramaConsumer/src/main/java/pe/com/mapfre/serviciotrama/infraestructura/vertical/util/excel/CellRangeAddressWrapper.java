package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.excel;

import org.apache.poi.ss.util.CellRangeAddress;

/**
 * La Class CellRangeAddressWrapper.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class CellRangeAddressWrapper implements Comparable<CellRangeAddressWrapper> {

	/** La range. */
	public CellRangeAddress range;

	/**
	 * Instancia un nuevo cell range address wrapper.
	 *
	 * @param theRange el the range
	 */
	public CellRangeAddressWrapper(CellRangeAddress theRange) {
		this.range = theRange;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(CellRangeAddressWrapper o) {
		if (range.getFirstColumn() < o.range.getFirstColumn() && range.getFirstRow() < o.range.getFirstRow()) {
			return -1;
		} else if (range.getFirstColumn() == o.range.getFirstColumn() && range.getFirstRow() == o.range.getFirstRow()) {
			return 0;
		} else {
			return 1;
		}
	}
}