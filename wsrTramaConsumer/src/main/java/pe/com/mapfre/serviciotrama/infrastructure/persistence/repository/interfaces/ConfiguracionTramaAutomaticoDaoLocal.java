package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.domain.entity.vo.CargaArchivoTramaVO;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionFtpTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.EquivalenciaDato;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.JuegoTramaCorreo;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TramaNomenclaturaArchivo;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TramaNomenclaturaArchivoDetalle;

/**
 * La Class ConfiguracionTramaAutomaticoDaoLocal.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface ConfiguracionTramaAutomaticoDaoLocal extends IGenericRepository<Object, TramaNomenclaturaArchivo> {

	TramaNomenclaturaArchivo obtenerTramaNomenclaturaArchivoById(Long idTramaNomenclaturaArchivo);

	/**
	 * Listar configuracion trama.
	 *
	 * @param filtro
	 *            el configuracion trama
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	List<ConfiguracionTrama> listar(ConfiguracionTrama filtro);

	/**
	 * Listar configuracion trama detalle map.
	 *
	 * @param listaIdConfiguracionTrama
	 *            the lista id configuracion trama
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the map
	 * @throws Exception
	 *             the exception
	 */
	Map<Long, List<ConfiguracionTramaDetalle>> listarMap(List<Long> listaIdConfiguracionTrama);

	/**
	 * Listar trama nomenclatura archivo detalle map.
	 *
	 * @param listaIdTramaNomenclaturaArchivo
	 *            the lista id trama nomenclatura archivo
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the map
	 * @throws Exception
	 *             the exception
	 */
	Map<Long, List<TramaNomenclaturaArchivoDetalle>> listarNomenclaturaDetMap(
			List<Long> listaIdTramaNomenclaturaArchivo);

	/**
	 * Registrar trama data.
	 *
	 * @param sql
	 *            the sql
	 * @param parametros
	 *            el parametros
	 * @throws Exception
	 *             the exception
	 */
	void registrarTramaData(StringBuilder sql, Map<String, Object> parametros) throws Exception;

	/**
	 * Obtener numero lote.
	 *
	 * @param funncionLote
	 *            the funncion lote
	 * @param codigoTratamiento
	 *            the codigo tratamiento
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	String obtenerNumeroLote(String funncionLote, String codigoTratamiento);

	/**
	 * Listar juego trama correo map.
	 *
	 * @param listaIdJuegoTrama
	 *            the lista id juego trama
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the map
	 * @throws Exception
	 *             the exception
	 */
	Map<Long, List<JuegoTramaCorreo>> listarJuegoTramaCorreoMap(List<Long> listaIdJuegoTrama);

	/**
	 * Listar configuracion ftp trama map.
	 *
	 * @param listaIdJuegoTrama
	 *            the lista id juego trama
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the map
	 * @throws Exception
	 *             the exception
	 */
	Map<Long, List<ConfiguracionFtpTrama>> listarConfiguracionFTPTramaMap(List<Long> listaIdJuegoTrama);

	/**
	 * Ejecutar programa homologacion.
	 *
	 * @param programaHomologacion
	 *            the programa homologacion
	 * @param valor
	 *            the valor
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	String ejecutarProgramaHomologacion(String programaHomologacion, Object valor);

	/**
	 * Listar configuracion trama error map.
	 *
	 * @param listaIdConfiguracionTrama
	 *            the lista id configuracion trama
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the map
	 * @throws Exception
	 *             the exception
	 */
	Map<Long, Map<String, String>> listarConfiguracionTramaErrorMap(List<Long> listaIdConfiguracionTrama);

	/**
	 * Obtener carga archivo trama.
	 *
	 * @param idCargaArchivo
	 *            the id carga archivo
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the carga archivo trama vo
	 * @throws Exception
	 *             the exception
	 */
	CargaArchivoTramaVO obtenerCargaArchivoTrama(Long idCargaArchivo) throws Exception;

	/**
	 * Ejecutar proceso trama emision.
	 *
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	String ejecutarProcesoTramaEmision() throws Exception;

	/**
	 * Ejecutar proceso trama emision marcado.
	 *
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	String ejecutarProcesoTramaEmisionMarcado() throws Exception;

	/**
	 * Ejecutar proceso trama emision cobros.
	 *
	 * @param sessionHibernate
	 *            the session hibernate
	 * @return the string
	 * @throws Exception
	 *             the exception
	 */
	String ejecutarProcesoTramaEmisionCobros() throws Exception;

	/**
	 * Registrar flag tasme edgp.
	 *
	 * @param numeroLote
	 *            the numero lote
	 * @param fechaLote
	 *            the fecha lote
	 * @param flagTipoResultadoEmis
	 *            the flag tipo resultado emis
	 * @throws Exception
	 *             the exception
	 */
	void registrarFlagTasmeEDGP(String numeroLote, Date fechaLote, String flagTipoResultadoEmis) throws Exception;

	/**
	 * Registrar flag tasme edgp.
	 *
	 * @param parametrosMap
	 *            the parametros map
	 * @param flagTipoResultadoEmis
	 *            the flag tipo resultado emis
	 * @throws Exception
	 *             the exception
	 */
	void registrarFlagTasmeEDGP(Map<String, Object> parametrosMap, String flagTipoResultadoEmis) throws Exception;

	/**
	 * Obtener nomenclatura excluir map.
	 *
	 * @param nomenclaturaExcluirMap
	 *            the nomenclatura excluir map
	 * @return the map
	 * @throws Exception
	 *             the exception
	 */
	Map<String, String> obtenerNomenclaturaExcluirMap(Map<String, String> nomenclaturaExcluirMap) throws Exception;

	/**
	 * Registrar nomenclatura excluir map.
	 *
	 * @param nomenclaturaExcluirDataMap
	 *            the nomenclatura excluir data map
	 * @throws Exception
	 *             the exception
	 */
	void registrarNomenclaturaExcluirMap(Map<String, Object> nomenclaturaExcluirDataMap) throws Exception;

	/**
	 * Listar equivalencia list canal producto.
	 *
	 * @param listaProducto
	 *            the lista producto
	 * @param listaCanal
	 *            the lista canal
	 * @return the list
	 * @throws Exception
	 *             the exception
	 */
	List<EquivalenciaDato> listarEquivalenciaListCanalProducto(List<String> listaProducto, List<String> listaCanal);

}