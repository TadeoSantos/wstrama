package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpePK;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.TasmctrLoteMpeAutomaticoDaoLocal;

/**
 * La Class TasmctrlMpeDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author 2022 BuildSoft - MAPFRE.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class TasmctrlMpeAutomaticoDaoImpl extends GenericRepository<TasmctrLoteMpePK, TasmctrLoteMpe> implements TasmctrLoteMpeAutomaticoDaoLocal {

	private Logger log = LoggerFactory.getLogger(TasmctrlMpeAutomaticoDaoImpl.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.local.TasmctrlMpeDaoLocal#
	 * listarTasmctrlMpe(pe.gob.mapfre.pwr.rep.model.configurador.trama.jpa.
	 * TasmctrlMpe)
	 */
	@Override
	public List<TasmctrLoteMpe> listar(TasmctrLoteMpe filtro){
		Query query = generarQuery(filtro, false);
		query.setFirstResult(filtro.getStartRow());
		query.setMaxResults(filtro.getOffset());
		return query.getResultList();
	}

	/**
	 * Generar query lista TasmctrlMpe.
	 *
	 * @param TasmctrLoteMpeDTO
	 *            el tasmctrlMpe
	 * @param esContador
	 *            el es contador
	 * @return the query
	 */
	private Query generarQuery(TasmctrLoteMpe filtro, boolean esContador) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		if (esContador) {
			jpaql.append(" select count(o.tasmctrlMpePK.fechaLote) from TasmctrlMpe o where 1=1 ");
		} else {
			jpaql.append(" select o from TasmctrlMpe o where 1=1 ");
		}
		if (!StringUtil.isNullOrEmpty(filtro.getTasmctrLoteMpePK().getFechaLote())) {
			jpaql.append(" and o.tasmctrlMpePK.fechaLote = :fechaLote ");
			parametros.put("fechaLote", filtro.getTasmctrLoteMpePK().getFechaLote());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getTasmctrLoteMpePK().getNumeroLote())) {
			jpaql.append(" and upper(o.tasmctrlMpePK.numeroLote) like :numeroLote ");
			parametros.put("numeroLote", "%" + filtro.getTasmctrLoteMpePK().getNumeroLote().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(filtro.getMcaEmisDirectaMapfre())) {
			jpaql.append(" and upper(o.mcaEmisDirectaMapfre) like :mcaEmisDirectaMapfre ");
			parametros.put("mcaEmisDirectaMapfre", "%" + filtro.getMcaEmisDirectaMapfre().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(filtro.getCodigoTratamiento())) {
			jpaql.append(" and upper(o.codigoTratamiento) like :codigoTratamiento ");
			parametros.put("codigoTratamiento", "%" + filtro.getCodigoTratamiento().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(filtro.getFechaHoraInicioCargaTrama())) {
			jpaql.append(" and o.fechaHoraInicioCargaTrama = :fechaHoraInicioCargaTrama ");
			parametros.put("fechaHoraInicioCargaTrama", filtro.getFechaHoraInicioCargaTrama());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getFechaHoraFinCargaTrama())) {
			jpaql.append(" and o.fechaHoraFinCargaTrama = :fechaHoraFinCargaTrama ");
			parametros.put("fechaHoraFinCargaTrama", filtro.getFechaHoraFinCargaTrama());
		}
		if (!StringUtil.isNullOrEmptyNumeric(filtro.getNroRegistroLeidos())) {
			jpaql.append(" and o.nroRegistroLeidos = :nroRegistroLeidos ");
			parametros.put("nroRegistroLeidos", filtro.getNroRegistroLeidos());
		}
		if (!StringUtil.isNullOrEmptyNumeric(filtro.getNroRegistroErrorDeNivel2())) {
			jpaql.append(" and o.nroRegistroErrorDeNivel2 = :nroRegistroErrorDeNivel2 ");
			parametros.put("nroRegistroErrorDeNivel2", filtro.getNroRegistroErrorDeNivel2());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getMcaEmitidoExito())) {
			jpaql.append(" and upper(o.mcaEmitidoExito) like :mcaEmitidoExito ");
			parametros.put("mcaEmitidoExito", "%" + filtro.getMcaEmitidoExito().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(filtro.getFechaHoraInicioProcesoEmision())) {
			jpaql.append(" and o.fechaHoraInicioProcesoEmision = :fechaHoraInicioProcesoEmision ");
			parametros.put("fechaHoraInicioProcesoEmision", filtro.getFechaHoraInicioProcesoEmision());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getFechaHoraFinProcesoEmision())) {
			jpaql.append(" and o.fechaHoraFinProcesoEmision = :fechaHoraFinProcesoEmision ");
			parametros.put("fechaHoraFinProcesoEmision", filtro.getFechaHoraFinProcesoEmision());
		}
		if (!StringUtil.isNullOrEmptyNumeric(filtro.getNroRegistroProcesadoProcesoEmision())) {
			jpaql.append(" and o.nroRegistroProcesadoProcesoEmision = :nroRegistroProcesadoProcesoEmision ");
			parametros.put("nroRegistroProcesadoProcesoEmision", filtro.getNroRegistroProcesadoProcesoEmision());
		}
		if (!StringUtil.isNullOrEmptyNumeric(filtro.getNroRegistroErrorProcesoEmision())) {
			jpaql.append(" and o.nroRegistroErrorProcesoEmision = :nroRegistroErrorProcesoEmision ");
			parametros.put("nroRegistroErrorProcesoEmision", filtro.getNroRegistroErrorProcesoEmision());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getMcaPendienteGenerarTramaRetorno())) {
			jpaql.append(" and upper(o.mcaPendienteGenerarTramaRetorno) like :mcaPendienteGenerarTramaRetorno ");
			parametros.put("mcaPendienteGenerarTramaRetorno",
					"%" + filtro.getMcaPendienteGenerarTramaRetorno().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(filtro.getFechaHoraInicioGenerarTramaRetorno())) {
			jpaql.append(" and o.fechaHoraInicioGenerarTramaRetorno = :fechaHoraInicioGenerarTramaRetorno ");
			parametros.put("fechaHoraInicioGenerarTramaRetorno", filtro.getFechaHoraInicioGenerarTramaRetorno());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getFechaHoraFinGenerarTramaRetorno())) {
			jpaql.append(" and o.fechaHoraFinGenerarTramaRetorno = :fechaHoraFinGenerarTramaRetorno ");
			parametros.put("fechaHoraFinGenerarTramaRetorno", filtro.getFechaHoraFinGenerarTramaRetorno());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getMcaGeneroTramaRetroalimentacion())) {
			jpaql.append(" and upper(o.mcaGeneroTramaRetroalimentacion) like :mcaGeneroTramaRetroalimentacion ");
			parametros.put("mcaGeneroTramaRetroalimentacion",
					"%" + filtro.getMcaGeneroTramaRetroalimentacion().toUpperCase() + "%");
		}
		if (!StringUtil.isNullOrEmpty(filtro.getFechaHoraInicioTramaRetroalimentacion())) {
			jpaql.append(" and o.fechaHoraInicioTramaRetroalimentacion = :fechaHoraInicioTramaRetroalimentacion ");
			parametros.put("fechaHoraInicioTramaRetroalimentacion", filtro.getFechaHoraInicioTramaRetroalimentacion());
		}
		if (!StringUtil.isNullOrEmpty(filtro.getFechaHoraFinTramaRetroalimentacion())) {
			jpaql.append(" and o.fechaHoraFinTramaRetroalimentacion = :fechaHoraFinTramaRetroalimentacion ");
			parametros.put("fechaHoraFinTramaRetroalimentacion", filtro.getFechaHoraFinTramaRetroalimentacion());
		}
		if (!esContador) {
			// jpaql.append(" ORDER BY 1 ");
		}
		Query query = createQuery(jpaql.toString(), parametros);
		return query;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * pe.gob.mapfre.pwr.rep.ejb.configurador.trama.dao.local.TasmctrlMpeDaoLocal#
	 * contarListar{entity.getClassName()}(pe.gob.mapfre.pwr.rep.model.configurador.
	 * trama.jpa.TasmctrlMpeDTO)
	 */
	@Override
	public int contar(TasmctrLoteMpe filtro) {
		int resultado = 0;
		try {
			// StringBuilder jpaql = new StringBuilder();
			Query query = generarQuery(filtro, true);
			resultado = ((Long) query.getSingleResult()).intValue();
		} catch (Exception e) {
			resultado = 0;
		}
		return resultado;
	}


}