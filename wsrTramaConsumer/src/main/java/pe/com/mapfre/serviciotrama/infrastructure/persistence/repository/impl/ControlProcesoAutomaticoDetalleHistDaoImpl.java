package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.Date;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ConfiguracionTramaDataVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.GrupoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfiguracionTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms.UUIDUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProcesoDetalleHist;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoAutomaticoDetalleHistDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.ConfiguracionCacheUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.EstadoDetalleProcesoType;

/**
 * La Class ControlProcesoDetalleHistDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ControlProcesoAutomaticoDetalleHistDaoImpl extends  GenericRepository<String, ControlProcesoDetalleHist> implements ControlProcesoAutomaticoDetalleHistDaoLocal {

	private Logger log = LoggerFactory.getLogger(ControlProcesoAutomaticoDetalleHistDaoImpl.class);
	
    @Override
    public  boolean registrar(TasmctrLoteMpe tasmctrlMpe,GrupoConfiguracionTramaVO configuracionTramaCnfVO,Map<String,Boolean> juegoConfiguracionTramaErrorMap,Map<String, Object> parametroMap) {
    	boolean resultado = true;
    	try {
    		for (GrupoConfiguracionTramaVO configuracionTramaCnf: configuracionTramaCnfVO.getListaConfiguracionTrama()) {
    			for (String configuracionTramaDataKey: configuracionTramaCnf.getListaConfiguracionTramaData().getListaKey()) {
   	    		 ConfiguracionTramaDataVO configuracionTramaData = configuracionTramaCnf.getListaConfiguracionTramaData().get(configuracionTramaDataKey);
   	    		 for (ConfiguracionTramaDataVO objValue: configuracionTramaData.getListaCampoValue()) {
   	    			  if (objValue.isEsCampoNegocio()) {
   	    				  ControlProcesoDetalleHist controlProcesoDetalleHist = new ControlProcesoDetalleHist();
   	    				  String estadoControlProcesoDetalle = "";
   	    				  controlProcesoDetalleHist.setIdDetalleProcesoFlujo(UUIDUtil.generarElementUUID());//UUID
   	    				  controlProcesoDetalleHist.setIdControlProceso(tasmctrlMpe.getIdControlProceso());
   	    				  boolean juegoConfiguracionTramaError = juegoConfiguracionTramaErrorMap.containsKey(objValue.getAtributeValue() + "");
   	    				  if (!juegoConfiguracionTramaError) {
   	    					  estadoControlProcesoDetalle = EstadoDetalleProcesoType.SIN_ERROR.getKey();
   	    				   } else {
   	    					   estadoControlProcesoDetalle = EstadoDetalleProcesoType.CON_ERROR.getKey();
   	    				   }
   	    				  controlProcesoDetalleHist.setEstado(estadoControlProcesoDetalle);
   	    				  controlProcesoDetalleHist.setCodigoUsuario(parametroMap.get(ConstanteConfiguracionTramaUtil.USUARIO) + "");
   	    				  controlProcesoDetalleHist.setFechaActualizacion(new Date());
   	    				  controlProcesoDetalleHist.setFechaLote(tasmctrlMpe.getTasmctrLoteMpePK().getFechaLote());
   	    				  controlProcesoDetalleHist.setNumeroLote(tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote());
   	    				  controlProcesoDetalleHist.setIdJuego(Long.valueOf(configuracionTramaCnfVO.getNombreConfiguracion()));
   	    				  controlProcesoDetalleHist.setCampoIdentificadorNegocio(configuracionTramaCnf.getNombreTabla() + "." + objValue.getAtributeName());
   	    				  controlProcesoDetalleHist.setCampoIdentificadorNegocioValor(objValue.getAtributeValue() + "");
   	    				 
   	    				  //TODO MARTIN - GUARDANDO EN TABLA HISTORICA
   	    				  if (parametroMap.get(ConstanteConfiguracionTramaUtil.INDICADOR_INTERNO_ACTIVIDAD) != null) {
   	    					  controlProcesoDetalleHist.setIdentificadorInternoActividad((Long)parametroMap.get(ConstanteConfiguracionTramaUtil.INDICADOR_INTERNO_ACTIVIDAD)); //TODO MARTIN AGREGADO EL CODIGO IDENTIFICADOR UNICO DE ACTIVIDAD
   	    				  }
   	    				 //imprimir log de produccion
	    				  if (ConfiguracionCacheUtil.getInstance().isGenerarLogTramaJuego(configuracionTramaCnfVO.getNombreConfiguracion())) {
	    					 Map<String,Object> parametros = TransferDataUtil.toVOMap(controlProcesoDetalleHist);
							 log.error("proceso.registrarTramaData.parametros.save(" + tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote() + ") --> " + parametros.toString());
						  }
   	    				  saveNative(controlProcesoDetalleHist);
   	    			  }
   	    		 }
   	    	 }
    			configuracionTramaCnf.getListaConfiguracionTramaData().reiniciar();
   	    }
		} catch (Exception e) {
			log.error("Error ",e);
			resultado = false;
		}
    	 
    	return resultado;
    }
}