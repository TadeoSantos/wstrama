package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProceso;

/**
 * La Class ControlProcesoAutomaticoDaoLocal.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface ControlProcesoAutomaticoDaoLocal  extends IGenericRepository<String,ControlProceso> {
	
	/**
	 * Generar uuid control proceso.
	 *
	 * @return the string
	 * @throws Exception the exception
	 */
	String generarUUID() throws Exception;
}