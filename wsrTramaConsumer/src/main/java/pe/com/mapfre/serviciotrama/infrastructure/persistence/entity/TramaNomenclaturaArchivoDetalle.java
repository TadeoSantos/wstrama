package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class TramaNomenclaturaArchivoDetalle.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */	
@Getter
@Setter
@Entity
@Table(name = "SGSM_TRA_NOMEAR_DET", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class TramaNomenclaturaArchivoDetalle implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id trama nomenclatura archivo detalle. */
    @Id
    @Column(name = "N_ID_TRA_NOMEN_ARCHI_DET" , length = 18)
    private Long idTramaNomenclaturaArchivoDetalle;
   
    /** El trama nomenclatura archivo. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_TRA_NOMEN_ARCHI", referencedColumnName = "N_ID_TRA_NOMEN_ARCHI")
    private TramaNomenclaturaArchivo tramaNomenclaturaArchivo;
   
    /** El tipo. */
    @Column(name = "C_TIPO" , length = 1)
    private String tipo;
   
    /** El numero. */
    @Column(name = "N_NRO" , length = 10)
    private Long numero;
   
    /** El item. */
    @Column(name = "C_ITEM" , length = 100)
    private String item;
   
    /** El item valor. */
    @Column(name = "C_ITEM_VALOR" , length = 100)
    private String itemValor;
   
    /** El numero digito. */
    @Column(name = "N_NUMERO_DIGITO" , length = 10)
    private Long numeroDigito;
   
    /**
     * Instancia un nuevo trama nomenclatura archivo detalle.
     */
    public TramaNomenclaturaArchivoDetalle() {
    }
   
   
    /**
     * Instancia un nuevo trama nomenclatura archivo detalle.
     *
     * @param idTramaNomenclaturaArchivoDetalle el id trama nomenclatura archivo detalle
     * @param tramaNomenclaturaArchivo el trama nomenclatura archivo
     * @param tipo el tipo
     * @param numero el numero
     * @param item el item
     * @param itemValor el item valor
     * @param numeroDigito el numero digito
     */
    public TramaNomenclaturaArchivoDetalle(Long idTramaNomenclaturaArchivoDetalle, TramaNomenclaturaArchivo tramaNomenclaturaArchivo,String tipo, Long numero, String item, String itemValor, Long numeroDigito ) {
        super();
        this.idTramaNomenclaturaArchivoDetalle = idTramaNomenclaturaArchivoDetalle;
        this.tramaNomenclaturaArchivo = tramaNomenclaturaArchivo;
        this.tipo = tipo;
        this.numero = numero;
        this.item = item;
        this.itemValor = itemValor;
        this.numeroDigito = numeroDigito;
    }
  
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idTramaNomenclaturaArchivoDetalle == null) ? 0 : idTramaNomenclaturaArchivoDetalle.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TramaNomenclaturaArchivoDetalle other = (TramaNomenclaturaArchivoDetalle) obj;
        if (idTramaNomenclaturaArchivoDetalle == null) {
            if (other.idTramaNomenclaturaArchivoDetalle != null) {
                return false;
            }
        } else if (!idTramaNomenclaturaArchivoDetalle.equals(other.idTramaNomenclaturaArchivoDetalle)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TramaNomenclaturaArchivoDetalle [idTramaNomenclaturaArchivoDetalle=" + idTramaNomenclaturaArchivoDetalle + "]";
    }
   
}