package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfiguracionTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms.UUIDUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProceso;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProcesoJuego;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.JuegoTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoJuegoDaoLocal;

/**
 * La Class ControlProcesoJuegoDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ControlProcesoJuegoDaoImpl extends  GenericRepository<String, ControlProcesoJuego> implements ControlProcesoJuegoDaoLocal  {

	private Logger log = LoggerFactory.getLogger(ControlProcesoJuegoDaoImpl.class);
	
    @Override
    public boolean registrar(List<String> listaIdJuegoTrama,Map<String,Object> parametroMap) throws Exception {
    	boolean resultado = true;
    	try {
    		for (String keyGrupoOrden : listaIdJuegoTrama) {
    			registrarControlDetalleJuego(keyGrupoOrden, parametroMap);
    		}
		} catch (Exception e) {
			log.error("Error ", e);
		}
    	
    	return resultado;
    }
    
    protected void registrarControlDetalleJuego(String keyGrupoOrden, Map<String,Object> parametroMap) {
    	ControlProcesoJuego controlProcesoJuego = new ControlProcesoJuego();
    	controlProcesoJuego.setIdControlProcesoJuego(UUIDUtil.generarElementUUID());
    	controlProcesoJuego.setControlProceso(new ControlProceso());
    	controlProcesoJuego.getControlProceso().setIdControlProceso(parametroMap.get(ConstanteConfiguracionTramaUtil.ID_CONTROL_PROCESO) + "");
    	controlProcesoJuego.setJuegoTrama(new JuegoTrama());
    	controlProcesoJuego.getJuegoTrama().setIdJuegoTrama(Long.valueOf(keyGrupoOrden));
    	controlProcesoJuego.setFechaActualizacion(new Date());
    	controlProcesoJuego.setCodigoUsuario(parametroMap.get(ConstanteConfiguracionTramaUtil.USUARIO) + "");
    	saveNative(controlProcesoJuego );
      }
}