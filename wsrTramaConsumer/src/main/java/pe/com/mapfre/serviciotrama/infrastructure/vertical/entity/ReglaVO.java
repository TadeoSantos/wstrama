package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ReglaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ReglaVO implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** La data tupa map. */
	private Map<String, ValueDataVO> dataTupaMap = new HashMap<String, ValueDataVO>();

	/** La resultado. */
	private ValueDataVO resultado = new ValueDataVO();

	/** La fila. */
	private String fila;

	/** La es simulacion. */
	private boolean esSimulacion;

	/**
	 * Instancia un nuevo hecho regla configurador trama vo.
	 */
	public ReglaVO() {
		super();
	}

	/**
	 * Instancia un nuevo hecho regla configurador trama vo.
	 *
	 * @param dataTupaMap
	 *            el data tupa map
	 * @param resultado
	 *            el resultado
	 */
	public ReglaVO(Map<String, ValueDataVO> dataTupaMap, ValueDataVO resultado) {
		super();
		this.dataTupaMap = dataTupaMap;
		this.resultado = resultado;
	}

	/**
	 * Comprueba si es es simulacion.
	 *
	 * @return true, si es es simulacion
	 */
	public boolean isEsSimulacion() {
		return esSimulacion;
	}

}
