package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.RespuestaNaturalType;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.LogConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmeErrorMpe;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;

/**
 * La Class GrupoConfiguracionTramaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class GrupoConfiguracionTramaVO implements Serializable  {

	private static final long serialVersionUID = 1L;
	private String nombreConfiguracion;
	private RespuestaNaturalType ejecutarConfiguracion;
	private RespuestaNaturalType esObligatorio;
	private BigMemoryManager<String,ConfiguracionTramaDataVO> listaConfiguracionTramaData = new BigMemoryManager<>();
	private BigMemoryManager<String,TasmeErrorMpe> listaLongConfiguracionTramaData = new BigMemoryManager<>();
	private List<GrupoConfiguracionTramaVO> listaConfiguracionTrama = new ArrayList<GrupoConfiguracionTramaVO>();	
	private Map<String,String> campoMappingFormatoMap = new HashMap<String, String>();
	private BigMemoryManager<String,LogConfiguracionTrama> listaLogConfiguracionTramaAyuda = new BigMemoryManager<>();
	private String nombreTabla = "";
	  
	/** El fecha lote. */
	private Date fechaLote;
	
	/** El numero lote. */
	private String numeroLote;
	
	public GrupoConfiguracionTramaVO() {
		super();
	}

   
 public GrupoConfiguracionTramaVO(String nombreConfiguracion,
		    BigMemoryManager<String,ConfiguracionTramaDataVO> listaConfiguracionTramaData,
			List<GrupoConfiguracionTramaVO> listaConfiguracionTrama) {
		super();
		this.nombreConfiguracion = nombreConfiguracion;
		this.listaConfiguracionTramaData = listaConfiguracionTramaData;
		this.listaConfiguracionTrama = listaConfiguracionTrama;
	}

}
