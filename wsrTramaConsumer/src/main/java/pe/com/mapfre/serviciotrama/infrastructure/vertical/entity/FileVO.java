package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;
import java.util.Map;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TipoReporteGenerarType;

/**
 * La Class FileVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class FileVO implements Serializable {

    /** La Constante serialVersionUID. */
	private static final long serialVersionUID = -3854043104169839788L;

	/** El name. */
    private String name;
    
    /** El mime. */
    private String mime;
    
    /** El length. */
    private long length;
    
    /** La data. */
    private byte[] data;
    
   /** La reporte generado map. */
   private Map<String,Object> reporteGeneradoMap;
    
   private String DataBig;
   
   private String userName;
   
   /** La codigo. */
   private String codigo;
   
   private TipoReporteGenerarType tipoReporteGenerarType;
   
   /** El mime. */
   private String mimePresentar;
   
   private long lastModified = 0;
   //Inicio BUILDSOFT MantenimientoCCC
   private boolean eliminarArchivoTemp = true;
   
   private boolean nombreOriginal = true;
   //Fin BUILDSOFT MantenimientoCCC
    /**
     * Obtiene data.
     *
     * @return data
     */
    public byte[] getData() {
        return data;
    }
    
    /**
     * Establece el data.
     *
     * @param data el new data
     */
    public void setData(byte[] data) {
        this.data = data;
    }
    
    /**
     * Obtiene name.
     *
     * @return name
     */
    public String getName() {
        return name;
    }
    
    /**
     * Establece el name.
     *
     * @param name el new name
     */
    public void setName(String name) {
        this.name = name;
        int extDot = name.lastIndexOf('.');
        if (extDot > 0) {
            String extension = name.substring(extDot + 1);
            if ("bmp".equals(extension)) {
                mime = "image/bmp";
            } else if ("jpg".equals(extension)) {
                mime = "image/jpeg";
            } else if ("gif".equals(extension)) {
                mime = "image/gif";
            } else if ("png".equals(extension)) {
                mime = "image/png";
            } else if ("xls".equals(extension)) {
                mime = "application/vnd.ms-excel";
            } else if ("xlsx".equals(extension)) {
                mime = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            } else {
                mime = "image/unknown";
            }
        }
    }

    /**
     * Obtiene length.
     *
     * @return length
     */
    public long getLength() {
        return length;
    }
    
    /**
     * Establece el length.
     *
     * @param length el new length
     */
    public void setLength(long length) {
        this.length = length;
    }
    
    /**
     * Obtiene mime.
     *
     * @return mime
     */
    public String getMime() {
        return mime;
    }

	/**
	 * Establece el mime.
	 *
	 * @param mime el new mime
	 */
	public void setMime(String mime) {
		this.mime = mime;
	}

	/**
	 * Obtiene reporte generado map.
	 *
	 * @return reporte generado map
	 */
	public Map<String, Object> getReporteGeneradoMap() {
		return reporteGeneradoMap;
	}

	/**
	 * Sets the reporte generado map.
	 *
	 * @param reporteGeneradoMap el reporte generado map
	 */
	public void setReporteGeneradoMap(Map<String, Object> reporteGeneradoMap) {
		this.reporteGeneradoMap = reporteGeneradoMap;
	}

	public String getDataBig() {
		return DataBig;
	}

	public void setDataBig(String dataBig) {
		DataBig = dataBig;
	}

	public TipoReporteGenerarType getTipoReporteGenerarType() {
		return tipoReporteGenerarType;
	}

	public void setTipoReporteGenerarType(
			TipoReporteGenerarType tipoReporteGenerarType) {
		this.tipoReporteGenerarType = tipoReporteGenerarType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getLastModified() {
		return lastModified;
	}

	public void setLastModified(long lastModified) {
		this.lastModified = lastModified;
	}

	public String getMimePresentar() {
		return mimePresentar;
	}

	public void setMimePresentar(String mimePresentar) {
		this.mimePresentar = mimePresentar;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
   //Inicio BUILDSOFT MantenimientoCCC
	/**
	 * @return the eliminarArchivoTemp
	 */
	public boolean isEliminarArchivoTemp() {
		return eliminarArchivoTemp;
	}

	/**
	 * @param eliminarArchivoTemp the eliminarArchivoTemp to set
	 */
	public void setEliminarArchivoTemp(boolean eliminarArchivoTemp) {
		this.eliminarArchivoTemp = eliminarArchivoTemp;
	}

	/**
	 * @return the nombreOriginal
	 */
	public boolean isNombreOriginal() {
		return nombreOriginal;
	}

	/**
	 * @param nombreOriginal the nombreOriginal to set
	 */
	public void setNombreOriginal(boolean nombreOriginal) {
		this.nombreOriginal = nombreOriginal;
	}
	//Fin BUILDSOFT MantenimientoCCC	
}