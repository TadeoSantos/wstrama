package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class TasmctrLoteMpe.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "TASMCTRL_MPE", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class TasmctrLoteMpe extends BasePaginator implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El tasmctrl mpePK. */
    @Id
    @EmbeddedId
    private  TasmctrLoteMpePK tasmctrLoteMpePK;
	
    /** El mca emis directa mapfre. */
    @Column(name = "MCA_EMIS_MAPFRE" , length = 1)
    private String mcaEmisDirectaMapfre;
   
    /** El codigo tratamiento. */
    @Column(name = "COD_TRATAMIENTO" , length = 1)
    private String codigoTratamiento;
    
    /** El fecha hora inicio carga trama. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_INIC_CARGA" )
    private Date fechaHoraInicioCargaTrama;
   
    /** El fecha hora fin carga trama. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_FIN_CARGA")
    private Date fechaHoraFinCargaTrama;
   
    /** El nro registro leidos. */
    @Column(name = "NRO_REG_LEIDOS" , precision = 18 , scale = 0)
    private BigDecimal nroRegistroLeidos;
   
    /** El nro registro error de nivel2. */
    @Column(name = "NRO_REG_ERROR_VAL" , precision = 18 , scale = 0)
    private BigDecimal nroRegistroErrorDeNivel2;
   
    /** El mca emitido exito. */
    @Column(name = "MCA_EMITIDO" , length = 1)
    private String mcaEmitidoExito;
   
    /** El fecha hora inicio proceso emision. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_INIC_EMISION")
    private Date fechaHoraInicioProcesoEmision;
   
    /** El fecha hora fin proceso emision. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_FIN_EMISION" )
    private Date fechaHoraFinProcesoEmision;
   
    /** El nro registro procesado proceso emision. */
    @Column(name = "NRO_REG_PROCESADOS" , precision = 18 , scale = 0)
    private BigDecimal nroRegistroProcesadoProcesoEmision;
   
    /** El nro registro error proceso emision. */
    @Column(name = "NRO_REG_ERROR_EMIS" , precision = 18 , scale = 0)
    private BigDecimal nroRegistroErrorProcesoEmision;
   
    /** El mca pendiente generar trama retorno. */
    @Column(name = "MCA_FEEDBACK" , length = 1)
    private String mcaPendienteGenerarTramaRetorno;
   
    /** El fecha hora inicio generar trama retorno. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_INIC_FEEDBACK")
    private Date fechaHoraInicioGenerarTramaRetorno;
   
    /** El fecha hora fin generar trama retorno. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_FIN_FEEDBACK")
    private Date fechaHoraFinGenerarTramaRetorno;
   
    /** El mca genero trama retroalimentacion. */
    @Column(name = "MCA_TRANSFERIDO" , length = 1)
    private String mcaGeneroTramaRetroalimentacion;
   
    /** El fecha hora inicio trama retroalimentacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_INIC_TRANSF")
    private Date fechaHoraInicioTramaRetroalimentacion;
    
    /** El fecha hora fin trama retroalimentacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_FIN_TRANSF")
    private Date fechaHoraFinTramaRetroalimentacion;
    
    /** El mca emitido exito. */
    @Column(name = "NUM_PROC" , length = 32)
    private String idControlProceso;
    
    /** El id juego trama. */
    @Column(name = "N_ID_CONFIG_JUEGO_TRAMA" , length = 18)
    private Long idJuegoTrama;
    
    /** El numero lote. */
	@Column(name = "NUM_LOTE_ORIGINAL" , length = 10)
	private String numeroLoteOriginal;
   
    /**
     * Instancia un nuevo tasmctrl mpe.
     */
    public TasmctrLoteMpe() {
    }
   
    /**
     * Instancia un nuevo tasmctrl mpe.
     *
     * @param mcaEmisDirectaMapfre el mca emis directa mapfre
     * @param codigoTratamiento el codigo tratamiento
     * @param fechaHoraInicioCargaTrama el fecha hora inicio carga trama
     * @param fechaHoraFinCargaTrama el fecha hora fin carga trama
     * @param nroRegistroLeidos el nro registro leidos
     * @param nroRegistroErrorDeNivel2 el nro registro error de nivel2
     * @param mcaEmitidoExito el mca emitido exito
     * @param fechaHoraInicioProcesoEmision el fecha hora inicio proceso emision
     * @param fechaHoraFinProcesoEmision el fecha hora fin proceso emision
     * @param nroRegistroProcesadoProcesoEmision el nro registro procesado proceso emision
     * @param nroRegistroErrorProcesoEmision el nro registro error proceso emision
     * @param mcaPendienteGenerarTramaRetorno el mca pendiente generar trama retorno
     * @param fechaHoraInicioGenerarTramaRetorno el fecha hora inicio generar trama retorno
     * @param fechaHoraFinGenerarTramaRetorno el fecha hora fin generar trama retorno
     * @param mcaGeneroTramaRetroalimentacion el mca genero trama retroalimentacion
     * @param fechaHoraInicioTramaRetroalimentacion el fecha hora inicio trama retroalimentacion
     * @param fechaHoraFinTramaRetroalimentacion el fecha hora fin trama retroalimentacion
     */
    public TasmctrLoteMpe(String mcaEmisDirectaMapfre, String codigoTratamiento, Date fechaHoraInicioCargaTrama, Date fechaHoraFinCargaTrama, BigDecimal nroRegistroLeidos, BigDecimal nroRegistroErrorDeNivel2, String mcaEmitidoExito, Date fechaHoraInicioProcesoEmision, Date fechaHoraFinProcesoEmision, BigDecimal nroRegistroProcesadoProcesoEmision, BigDecimal nroRegistroErrorProcesoEmision, String mcaPendienteGenerarTramaRetorno, Date fechaHoraInicioGenerarTramaRetorno, Date fechaHoraFinGenerarTramaRetorno, String mcaGeneroTramaRetroalimentacion, Date fechaHoraInicioTramaRetroalimentacion, Date fechaHoraFinTramaRetroalimentacion ) {
        super();
        this.mcaEmisDirectaMapfre = mcaEmisDirectaMapfre;
        this.codigoTratamiento = codigoTratamiento;
        this.fechaHoraInicioCargaTrama = fechaHoraInicioCargaTrama;
        this.fechaHoraFinCargaTrama = fechaHoraFinCargaTrama;
        this.nroRegistroLeidos = nroRegistroLeidos;
        this.nroRegistroErrorDeNivel2 = nroRegistroErrorDeNivel2;
        this.mcaEmitidoExito = mcaEmitidoExito;
        this.fechaHoraInicioProcesoEmision = fechaHoraInicioProcesoEmision;
        this.fechaHoraFinProcesoEmision = fechaHoraFinProcesoEmision;
        this.nroRegistroProcesadoProcesoEmision = nroRegistroProcesadoProcesoEmision;
        this.nroRegistroErrorProcesoEmision = nroRegistroErrorProcesoEmision;
        this.mcaPendienteGenerarTramaRetorno = mcaPendienteGenerarTramaRetorno;
        this.fechaHoraInicioGenerarTramaRetorno = fechaHoraInicioGenerarTramaRetorno;
        this.fechaHoraFinGenerarTramaRetorno = fechaHoraFinGenerarTramaRetorno;
        this.mcaGeneroTramaRetroalimentacion = mcaGeneroTramaRetroalimentacion;
        this.fechaHoraInicioTramaRetroalimentacion = fechaHoraInicioTramaRetroalimentacion;
        this.fechaHoraFinTramaRetroalimentacion = fechaHoraFinTramaRetroalimentacion;
    }
   
	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((tasmctrLoteMpePK == null) ? 0 : tasmctrLoteMpePK.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TasmctrLoteMpe other = (TasmctrLoteMpe) obj;
        if (tasmctrLoteMpePK== null) {
            if (other.tasmctrLoteMpePK != null) {
                return false;
            }
        } else if (!tasmctrLoteMpePK.equals(other.tasmctrLoteMpePK)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TasmctrlMpe [tasmctrlMpePK=" + tasmctrLoteMpePK + "]";
    }
   
}