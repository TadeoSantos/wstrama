package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class PermisoEjecucionManualDetalle.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */	
@Getter
@Setter
@Entity
@Table(name = "SGSM_PERMISO_EJEC_MANUAL_DET", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class PermisoEjecucionManualDetalle implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id permiso detalle. */
    @Id
    @Column(name = "N_ID_PERMISO_DET" , length = 32)
    private String idPermisoDetalle;
   
    /** El permiso detalle. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_PERMISO", referencedColumnName = "N_ID_PERMISO")
    private PermisoEjecucionManual permisoEjecucionManual;
   
    /** El id juego trama. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CONFIG_JUEGO_TRAMA", referencedColumnName = "N_ID_CONFIG_JUEGO_TRAMA")
    private JuegoTrama juegoTrama;
   
    public PermisoEjecucionManualDetalle() {
    }
   
    /**
     * Instancia un nuevo permiso ejecucion manual detalle.
     *
     * @param idPermisoDetalle el id permiso detalle
     * @param permisoDetalle el permiso detalle
     * @param idJuego el id juego
     * @param juego el juego
     */
    public PermisoEjecucionManualDetalle(String idPermisoDetalle, PermisoEjecucionManual permisoEjecucionManual,JuegoTrama juegoTrama, String juego ) {
        super();
        this.idPermisoDetalle = idPermisoDetalle;
        this.permisoEjecucionManual = permisoEjecucionManual;
        this.juegoTrama = juegoTrama;
        
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idPermisoDetalle == null) ? 0 : idPermisoDetalle.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        PermisoEjecucionManualDetalle other = (PermisoEjecucionManualDetalle) obj;
        if (idPermisoDetalle == null) {
            if (other.idPermisoDetalle != null) {
                return false;
            }
        } else if (!idPermisoDetalle.equals(other.idPermisoDetalle)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PermisoEjecucionManualDetalle [idPermisoDetalle=" + idPermisoDetalle + "]";
    }
   
}