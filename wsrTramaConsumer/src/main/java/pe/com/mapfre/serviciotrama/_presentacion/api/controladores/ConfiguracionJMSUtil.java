package pe.com.mapfre.serviciotrama._presentacion.api.controladores;

/**
 * La Class ConfiguracionJMSUtil.
 * <ul>
 * <li>Copyright 2014 MAPFRE -
 * mapfre. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft.
 * @version 1.0, 28/07/2021
 * @since Rep v1.0
 */
public final class ConfiguracionJMSUtil {

	/** La Constante QCF_NAME. */
	public static final String QCF_NAME = "bsConnectionFactory";
	   
   	/** La Constante QUEUE_NAME. */
	public static final String QUEUE_NAME = "queue/bsQueue";
	
	/** La Constante QCF_TRAMA_CONTROL_NAME. */
	public static final String QCF_TRAMA_CONTROL_NAME = "bsTramaConnectionFactory";
	
	/** La Constante QUEUE_TRAMA_CONTROL_NAME. */
	public static final String QUEUE_TRAMA_CONTROL_NAME = "queue/bsTramaQueue";
	
	/** La Constante QUEUE_NAME. */
	public static final String TRANSACCTION_TIMEOUT = "86400";
	

	public static final String MAXIMA_INSTANCIA_COLA = "30";//5 = para pruebas 15 = normal
	
	//Fin Gestor de colas

	/**
	 * Instancia un nuevo configuracion jms util.
	 */
	private ConfiguracionJMSUtil() {
		
	}
}
