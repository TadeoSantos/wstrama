package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.LogConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmeErrorMpe;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

// TODO: Auto-generated Javadoc
/**
 * La Class RespuestaLecturaAgrupadoVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class RespuestaLecturaAgrupadoVO implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** La lista agrupado data resul map. */
	private List<Map<String, ValueDataVO>> listaAgrupadoDataResulMap = new ArrayList<>();

	/** La lista data resul map. */
	private List<Map<String, ValueDataVO>> listaDataResulMap = new ArrayList<>();

	/** La lista log configuracion trama . */
	private BigMemoryManager<String, TasmeErrorMpe> listaLogConfiguracionTrama = new BigMemoryManager<>();

	/** La lista log configuracion trama ayuda . */
	private BigMemoryManager<String, LogConfiguracionTrama> listaLogConfiguracionTramaAyuda = new BigMemoryManager<>();

	/** La error numero lote. */
	private boolean errorNumeroLote;

	/**
	 * Instancia un nuevo respuesta lectura agrupado vo.
	 */
	public RespuestaLecturaAgrupadoVO() {
		super();
	}

	/**
	 * Comprueba si es error numero lote.
	 *
	 * @return true, si es error numero lote
	 */
	public boolean isErrorNumeroLote() {
		return errorNumeroLote;
	}
	
}
