package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ResultadoOrquestadorTramaWSVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ResultadoOrquestadorTramaWSVO implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 239808362722058446L;

	/** La archivo. */
	private byte[] archivo;

    /** La tipo. */
    private String tipo;

    /** La nombre. */
    private String nombre;
    
    /** La codigo error. */
	private String codigoError;
	
	/** La mensaje error. */
	private String mensajeError;
	
	/** La is error. */
	private boolean isError;
	
	/**
	 * Instancia un nuevo resultado wsvo.
	 */
	public ResultadoOrquestadorTramaWSVO() {
		super();
	}
	
	
	/**
	 * Instancia un nuevo resultado orquestador trama wsvo.
	 *
	 * @param archivo el archivo
	 * @param tipo el tipo
	 * @param nombre el nombre
	 * @param codigoError el codigo error
	 * @param mensajeError el mensaje error
	 * @param isError el is error
	 */
	public ResultadoOrquestadorTramaWSVO(byte[] archivo, String tipo,
			String nombre, String codigoError, String mensajeError,
			boolean isError) {
		super();
		this.archivo = archivo;
		this.tipo = tipo;
		this.nombre = nombre;
		this.codigoError = codigoError;
		this.mensajeError = mensajeError;
		this.isError = isError;
	}

}