package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class EMail.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class EMail {
	
	private String mailFrom;

	private String mailTo;

	private String mailCc;

	private String mailBcc;

	private String mailSubject;

	private String mailContent;

	private String contentType;
	
	private boolean html;

	private List<Object> attachments;

	public EMail() {
	    contentType = "text/plain";
	    this.html = false;
	}

	public boolean isHtml() {
		return html;
	}
	
}
