package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ErrorValidacionVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ErrorValidacionVO extends BasePaginator implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 9166621199365741020L;
	
	/** La id correlativo error. */
	private Long idCorrelativoError;
	
	/** La linea error. */
	private Long lineaError;
	
	/** La tipo error. */
	private String tipoError;
	
	/** La descripcion error. */
	private String descripcionError;
	
	/** La error tecnico. */
	private String errorTecnico;
	
	/** La clase error. */
	private String claseError;
	
	/**
	 * Instancia un nuevo error regla vo.
	 */
	public ErrorValidacionVO() {
		super();
	}
	
	/**
	 * Instancia un nuevo error drools vo.
	 *
	 * @param idCorrelativoError el id correlativo error
	 * @param lineaError el linea error
	 * @param tipoError el tipo error
	 * @param descripcionError el descripcion error
	 */
	public ErrorValidacionVO(Long idCorrelativoError, Long lineaError, String tipoError, String descripcionError) {
		super();
		this.idCorrelativoError = idCorrelativoError;
		this.lineaError = lineaError;
		this.tipoError = tipoError;
		this.descripcionError = descripcionError;
	}

}
