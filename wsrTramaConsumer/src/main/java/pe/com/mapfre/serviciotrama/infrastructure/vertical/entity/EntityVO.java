package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class EntityVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class EntityVO {

	/** El alias. */
	private String alias;
	
	/** El classs. */
	private String classs;
	
	/** La posicion. */
	private int posicion;
	
	
	/**
	 * Instancia un nuevo entity vo.
	 */
	public EntityVO() {
		super();
	}

	

	/**
	 * Instancia un nuevo entity vo.
	 *
	 * @param alias el alias
	 * @param classs el classs
	 */
	public EntityVO(String alias, String classs) {
		super();
		this.alias = alias;
		this.classs = classs;
	}



	/**
	 * Instancia un nuevo entity vo.
	 *
	 * @param alias el alias
	 * @param classs el classs
	 * @param posicion el posicion
	 */
	public EntityVO(String alias, String classs, int posicion) {
		super();
		this.alias = alias;
		this.classs = classs;
		this.posicion = posicion;
	}

}
