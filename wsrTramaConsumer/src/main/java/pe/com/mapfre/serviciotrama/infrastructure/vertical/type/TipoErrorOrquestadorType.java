package pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class TipoErrorOrquestadorType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum TipoErrorOrquestadorType {

	/** El NUMERO_LOTE. */
	TECNICO("T", "tipoErrorOrquestadorType.tecnico"),
	
	/** El NUMERO_LOTE. */
	NEGOCIO("N", "tipoErrorOrquestadorType.negocio"),

	/** El FECHA_LOTE. */
	SIN_TRAMA("S", "tipoErrorOrquestadorType.sintrama");

	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, TipoErrorOrquestadorType> LOO_KUP_MAP = new HashMap<String, TipoErrorOrquestadorType>();

	static {
		for (TipoErrorOrquestadorType s : EnumSet.allOf(TipoErrorOrquestadorType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	private String key;
	private String value;

	private TipoErrorOrquestadorType(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}

	/**
	 * Gets the.
	 *
	 * @param key
	 *            el key
	 * @return the campo fijo type
	 */
	public static TipoErrorOrquestadorType get(String key) {
		return LOO_KUP_MAP.get(key);
	}
}

