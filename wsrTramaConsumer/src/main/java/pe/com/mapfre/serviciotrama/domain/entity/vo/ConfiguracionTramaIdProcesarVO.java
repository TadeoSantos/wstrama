package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.RespuestaNaturalType;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionFtpTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.EquivalenciaDato;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.LogConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TramaNomenclaturaArchivoDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.CampoTablaVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;

/**
 * La Class ConfiguracionTramaIdProcesarVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class ConfiguracionTramaIdProcesarVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ConfiguracionTrama> listaConfiguracionTrama = new ArrayList<ConfiguracionTrama>();
	private Map<Long, List<ConfiguracionTramaDetalle>> listaConfiguracionTramaDetalleMap = new HashMap<Long, List<ConfiguracionTramaDetalle>>();
	private Map<Long, Map<String, String>> listaConfiguracionTramaErrorMap = new HashMap<Long, Map<String, String>>();
	private Map<Long, Map<String, String>> listaConfiguracionTramaCampoNoPersistenteMap = new HashMap<Long, Map<String, String>>();
	private Map<String, Map<Long, Map<String, String>>> listaConfiguracionTramaErrorGrupoMap = new HashMap<String, Map<Long, Map<String, String>>>();
	private Map<String, Map<Long, Map<String, String>>> listaConfiguracionTramaCampoNoPersistenteGrupoMap = new HashMap<String, Map<Long, Map<String, String>>>();
	private Map<Long, List<TramaNomenclaturaArchivoDetalle>> listaTramaNomenclaturaArchivoDetalleMap = new HashMap<Long, List<TramaNomenclaturaArchivoDetalle>>();
	// filtro by id
	private List<Long> listaIdConfiguracionTrama = new ArrayList<Long>();
	private List<Long> listaIdTramaNomenclaturaArchivo = new ArrayList<Long>();
	private List<Long> listaIdJuegoTrama = new ArrayList<Long>();
	private List<String> listaNombreEsquemaTabla = new ArrayList<String>();
	private Map<Long, List<ConfiguracionFtpTrama>> listaConfiguracionFtpTramaMap = new HashMap<Long, List<ConfiguracionFtpTrama>>();

	private Map<String, Map<String, RespuestaNaturalType>> listaGrupoConfiguracionTramaValidarObigatoridadMap = new HashMap<String, Map<String, RespuestaNaturalType>>();
	private Map<String, String> listaConfiguracionTramaTablaMap = new HashMap<String, String>();

	private Map<String, Map<String, String>> listaDepenciaDataMap = new HashMap<String, Map<String, String>>();
	private Map<String, String> listaDepenciaTablaMixtaDataMap = new HashMap<String, String>();
	private Map<String, String> listaTablaMixtaMap = new HashMap<String, String>();
	private Map<String, Map<String, CampoTablaVO>> camposDiponibleTablaMap = new HashMap<String, Map<String, CampoTablaVO>>();

	private Map<String, List<ConfiguracionTrama>> juegoConfiguracionTramaMap = new HashMap<>();
	private Map<String, Map<String, ConfiguracionTrama>> nomenclaturaJuegoConfiguracionTramaMap = new HashMap<>();
	private Map<String, Map<String, Map<String, Map<String, Object>>>> listaRespuestaLecturaArchivoTemporalGrupoMap = new HashMap<String, Map<String, Map<String, Map<String, Object>>>>();
	private Map<String, String> nomenclaturaExcluirMap = new HashMap<String, String>();
	private ValidarNomenclaturaVO nomenclaturaExcluirVO = new ValidarNomenclaturaVO();

	private List<String> listaKeyGrupo = new ArrayList<String>();

	private Map<String, String> nomenclaturaExcluirByFechaMap = new HashMap<String, String>();

	// para obtener equivalencia simple o compuesta
	private Map<String, EquivalenciaDato> equivalenciaHomologacionSimpleSinDependenciaMap = new HashMap<>();
	private Map<String, EquivalenciaDato> equivalenciaHomologacionSimpleConDependenciaMap = new HashMap<>();
	private Map<String, EquivalenciaDato> equivalenciaHomologacionCanculadoMap = new HashMap<>();

	// filtro by id
	private List<String> listaCanal = new ArrayList<>();
	private List<String> listaProducto = new ArrayList<>();

	// Log para apoyo a trama
	private BigMemoryManager<String, LogConfiguracionTrama> listaLogConfiguracionTramaAyuda = new BigMemoryManager<String, LogConfiguracionTrama>();

	public ConfiguracionTramaIdProcesarVO() {
		super();
	}

	public ConfiguracionTramaIdProcesarVO(List<ConfiguracionTrama> listaConfiguracionTrama) {
		super();
		this.listaConfiguracionTrama = listaConfiguracionTrama;
	}

	// get y set

	public Map<Long, Map<String, String>> getListaConfiguracionTramaErrorMap() {
		return listaConfiguracionTramaErrorMap;
	}

	public Map<Long, List<ConfiguracionTramaDetalle>> getListaConfiguracionTramaDetalleMap() {
		return listaConfiguracionTramaDetalleMap;
	}

	public void setListaConfiguracionTramaDetalleMap(
			Map<Long, List<ConfiguracionTramaDetalle>> listaConfiguracionTramaDetalleMap) {
		this.listaConfiguracionTramaDetalleMap = listaConfiguracionTramaDetalleMap;
	}

	public void setListaConfiguracionTramaErrorMap(Map<Long, Map<String, String>> listaConfiguracionTramaErrorMap) {
		this.listaConfiguracionTramaErrorMap = listaConfiguracionTramaErrorMap;
	}

	public Map<Long, Map<String, String>> getListaConfiguracionTramaCampoNoPersistenteMap() {
		return listaConfiguracionTramaCampoNoPersistenteMap;
	}

	public void setListaConfiguracionTramaCampoNoPersistenteMap(
			Map<Long, Map<String, String>> listaConfiguracionTramaCampoNoPersistenteMap) {
		this.listaConfiguracionTramaCampoNoPersistenteMap = listaConfiguracionTramaCampoNoPersistenteMap;
	}

	public Map<String, Map<Long, Map<String, String>>> getListaConfiguracionTramaErrorGrupoMap() {
		return listaConfiguracionTramaErrorGrupoMap;
	}

	public void setListaConfiguracionTramaErrorGrupoMap(
			Map<String, Map<Long, Map<String, String>>> listaConfiguracionTramaErrorGrupoMap) {
		this.listaConfiguracionTramaErrorGrupoMap = listaConfiguracionTramaErrorGrupoMap;
	}

	public Map<String, Map<Long, Map<String, String>>> getListaConfiguracionTramaCampoNoPersistenteGrupoMap() {
		return listaConfiguracionTramaCampoNoPersistenteGrupoMap;
	}

	public void setListaConfiguracionTramaCampoNoPersistenteGrupoMap(
			Map<String, Map<Long, Map<String, String>>> listaConfiguracionTramaCampoNoPersistenteGrupoMap) {
		this.listaConfiguracionTramaCampoNoPersistenteGrupoMap = listaConfiguracionTramaCampoNoPersistenteGrupoMap;
	}

	public Map<Long, List<TramaNomenclaturaArchivoDetalle>> getListaTramaNomenclaturaArchivoDetalleMap() {
		return listaTramaNomenclaturaArchivoDetalleMap;
	}

	public void setListaTramaNomenclaturaArchivoDetalleMap(
			Map<Long, List<TramaNomenclaturaArchivoDetalle>> listaTramaNomenclaturaArchivoDetalleMap) {
		this.listaTramaNomenclaturaArchivoDetalleMap = listaTramaNomenclaturaArchivoDetalleMap;
	}

	public List<Long> getListaIdConfiguracionTrama() {
		return listaIdConfiguracionTrama;
	}

	public void setListaIdConfiguracionTrama(List<Long> listaIdConfiguracionTrama) {
		this.listaIdConfiguracionTrama = listaIdConfiguracionTrama;
	}

	public List<Long> getListaIdTramaNomenclaturaArchivo() {
		return listaIdTramaNomenclaturaArchivo;
	}

	public void setListaIdTramaNomenclaturaArchivo(List<Long> listaIdTramaNomenclaturaArchivo) {
		this.listaIdTramaNomenclaturaArchivo = listaIdTramaNomenclaturaArchivo;
	}

	public List<Long> getListaIdJuegoTrama() {
		return listaIdJuegoTrama;
	}

	public void setListaIdJuegoTrama(List<Long> listaIdJuegoTrama) {
		this.listaIdJuegoTrama = listaIdJuegoTrama;
	}

	public List<String> getListaNombreEsquemaTabla() {
		return listaNombreEsquemaTabla;
	}

	public void setListaNombreEsquemaTabla(List<String> listaNombreEsquemaTabla) {
		this.listaNombreEsquemaTabla = listaNombreEsquemaTabla;
	}

	public Map<Long, List<ConfiguracionFtpTrama>> getListaConfiguracionFtpTramaMap() {
		return listaConfiguracionFtpTramaMap;
	}

	public void setListaConfiguracionFtpTramaMap(Map<Long, List<ConfiguracionFtpTrama>> listaConfiguracionFtpTramaMap) {
		this.listaConfiguracionFtpTramaMap = listaConfiguracionFtpTramaMap;
	}

	public Map<String, Map<String, RespuestaNaturalType>> getListaGrupoConfiguracionTramaValidarObigatoridadMap() {
		return listaGrupoConfiguracionTramaValidarObigatoridadMap;
	}

	public void setListaGrupoConfiguracionTramaValidarObigatoridadMap(
			Map<String, Map<String, RespuestaNaturalType>> listaGrupoConfiguracionTramaValidarObigatoridadMap) {
		this.listaGrupoConfiguracionTramaValidarObigatoridadMap = listaGrupoConfiguracionTramaValidarObigatoridadMap;
	}

	public Map<String, String> getListaConfiguracionTramaTablaMap() {
		return listaConfiguracionTramaTablaMap;
	}

	public void setListaConfiguracionTramaTablaMap(Map<String, String> listaConfiguracionTramaTablaMap) {
		this.listaConfiguracionTramaTablaMap = listaConfiguracionTramaTablaMap;
	}

	public Map<String, Map<String, String>> getListaDepenciaDataMap() {
		return listaDepenciaDataMap;
	}

	public void setListaDepenciaDataMap(Map<String, Map<String, String>> listaDepenciaDataMap) {
		this.listaDepenciaDataMap = listaDepenciaDataMap;
	}

	public Map<String, String> getListaDepenciaTablaMixtaDataMap() {
		return listaDepenciaTablaMixtaDataMap;
	}

	public void setListaDepenciaTablaMixtaDataMap(Map<String, String> listaDepenciaTablaMixtaDataMap) {
		this.listaDepenciaTablaMixtaDataMap = listaDepenciaTablaMixtaDataMap;
	}

	public Map<String, String> getListaTablaMixtaMap() {
		return listaTablaMixtaMap;
	}

	public void setListaTablaMixtaMap(Map<String, String> listaTablaMixtaMap) {
		this.listaTablaMixtaMap = listaTablaMixtaMap;
	}

	public List<ConfiguracionTrama> getListaConfiguracionTrama() {
		return listaConfiguracionTrama;
	}

	public void setListaConfiguracionTrama(List<ConfiguracionTrama> listaConfiguracionTrama) {
		this.listaConfiguracionTrama = listaConfiguracionTrama;
	}

	public Map<String, Map<String, CampoTablaVO>> getCamposDiponibleTablaMap() {
		return camposDiponibleTablaMap;
	}

	public void setCamposDiponibleTablaMap(Map<String, Map<String, CampoTablaVO>> camposDiponibleTablaMap) {
		this.camposDiponibleTablaMap = camposDiponibleTablaMap;
	}

	public Map<String, List<ConfiguracionTrama>> getJuegoConfiguracionTramaMap() {
		return juegoConfiguracionTramaMap;
	}

	public void setJuegoConfiguracionTramaMap(Map<String, List<ConfiguracionTrama>> juegoConfiguracionTramaMap) {
		this.juegoConfiguracionTramaMap = juegoConfiguracionTramaMap;
	}

	public Map<String, Map<String, ConfiguracionTrama>> getNomenclaturaJuegoConfiguracionTramaMap() {
		return nomenclaturaJuegoConfiguracionTramaMap;
	}

	public void setNomenclaturaJuegoConfiguracionTramaMap(
			Map<String, Map<String, ConfiguracionTrama>> nomenclaturaJuegoConfiguracionTramaMap) {
		this.nomenclaturaJuegoConfiguracionTramaMap = nomenclaturaJuegoConfiguracionTramaMap;
	}

	public Map<String, Map<String, Map<String, Map<String, Object>>>> getListaRespuestaLecturaArchivoTemporalGrupoMap() {
		return listaRespuestaLecturaArchivoTemporalGrupoMap;
	}

	public void setListaRespuestaLecturaArchivoTemporalGrupoMap(
			Map<String, Map<String, Map<String, Map<String, Object>>>> listaRespuestaLecturaArchivoTemporalGrupoMap) {
		this.listaRespuestaLecturaArchivoTemporalGrupoMap = listaRespuestaLecturaArchivoTemporalGrupoMap;
	}

	public Map<String, String> getNomenclaturaExcluirMap() {
		return nomenclaturaExcluirMap;
	}

	public void setNomenclaturaExcluirMap(Map<String, String> nomenclaturaExcluirMap) {
		this.nomenclaturaExcluirMap = nomenclaturaExcluirMap;
	}

	public ValidarNomenclaturaVO getNomenclaturaExcluirVO() {
		return nomenclaturaExcluirVO;
	}

	public void setNomenclaturaExcluirVO(ValidarNomenclaturaVO nomenclaturaExcluirVO) {
		this.nomenclaturaExcluirVO = nomenclaturaExcluirVO;
	}

	public List<String> getListaKeyGrupo() {
		return listaKeyGrupo;
	}

	public void setListaKeyGrupo(List<String> listaKeyGrupo) {
		this.listaKeyGrupo = listaKeyGrupo;
	}

	public Map<String, String> getNomenclaturaExcluirByFechaMap() {
		return nomenclaturaExcluirByFechaMap;
	}

	public void setNomenclaturaExcluirByFechaMap(Map<String, String> nomenclaturaExcluirByFechaMap) {
		this.nomenclaturaExcluirByFechaMap = nomenclaturaExcluirByFechaMap;
	}

	public Map<String, EquivalenciaDato> getEquivalenciaHomologacionSimpleSinDependenciaMap() {
		return equivalenciaHomologacionSimpleSinDependenciaMap;
	}

	public void setEquivalenciaHomologacionSimpleSinDependenciaMap(
			Map<String, EquivalenciaDato> equivalenciaHomologacionSimpleSinDependenciaMap) {
		this.equivalenciaHomologacionSimpleSinDependenciaMap = equivalenciaHomologacionSimpleSinDependenciaMap;
	}

	public Map<String, EquivalenciaDato> getEquivalenciaHomologacionSimpleConDependenciaMap() {
		return equivalenciaHomologacionSimpleConDependenciaMap;
	}

	public void setEquivalenciaHomologacionSimpleConDependenciaMap(
			Map<String, EquivalenciaDato> equivalenciaHomologacionSimpleConDependenciaMap) {
		this.equivalenciaHomologacionSimpleConDependenciaMap = equivalenciaHomologacionSimpleConDependenciaMap;
	}

	public Map<String, EquivalenciaDato> getEquivalenciaHomologacionCanculadoMap() {
		return equivalenciaHomologacionCanculadoMap;
	}

	public void setEquivalenciaHomologacionCanculadoMap(
			Map<String, EquivalenciaDato> equivalenciaHomologacionCanculadoMap) {
		this.equivalenciaHomologacionCanculadoMap = equivalenciaHomologacionCanculadoMap;
	}

	public List<String> getListaCanal() {
		return listaCanal;
	}

	public void setListaCanal(List<String> listaCanal) {
		this.listaCanal = listaCanal;
	}

	public List<String> getListaProducto() {
		return listaProducto;
	}

	public void setListaProducto(List<String> listaProducto) {
		this.listaProducto = listaProducto;
	}

	public BigMemoryManager<String, LogConfiguracionTrama> getListaLogConfiguracionTramaAyuda() {
		return listaLogConfiguracionTramaAyuda;
	}

	public void setListaLogConfiguracionTramaAyuda(
			BigMemoryManager<String, LogConfiguracionTrama> listaLogConfiguracionTramaAyuda) {
		this.listaLogConfiguracionTramaAyuda = listaLogConfiguracionTramaAyuda;
	}

}
