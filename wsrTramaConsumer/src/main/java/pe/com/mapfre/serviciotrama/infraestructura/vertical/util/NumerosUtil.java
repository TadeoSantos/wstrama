package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * La Class NumerosUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class NumerosUtil {
	
	/**
	 * Redondear.
	 *
	 * @param numero el numero
	 * @param numeroRedondear numero de digitos a redondear
	 * @param roundingMode el rounding mode
	 * @return the big decimal
	 */
	public static BigDecimal redondear(BigDecimal numero, Integer numeroRedondear, RoundingMode roundingMode) {
		BigDecimal resultado = BigDecimal.ZERO;
		try {
			resultado = numero.setScale(numeroRedondear, roundingMode);
		} catch (Exception e) {
			resultado = BigDecimal.ZERO;
		}
		return resultado;
	}
	public static boolean estaDentroRango(BigDecimal numeroComparar, BigDecimal numeroIntervalo1, BigDecimal numeroIntervalo2) {
		boolean respuesta = false;
		respuesta = numeroIntervalo1.compareTo(numeroComparar) >= 0 &&  numeroIntervalo2.compareTo(numeroComparar) <= 0 ;
		return respuesta;
	}
	public static boolean estaDentroRango(int numeroComparar, int numeroIntervalo1, int numeroIntervalo2) {
		boolean respuesta = false;
		respuesta = numeroComparar  >= ( numeroIntervalo1)  &&  numeroComparar  <= (numeroIntervalo2) ;
		return respuesta;
	}
}
