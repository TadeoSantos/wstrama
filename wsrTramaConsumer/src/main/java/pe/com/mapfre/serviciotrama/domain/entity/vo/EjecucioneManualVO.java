/*
 * 
 */
package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.BasePaginator;

/**
 * La Class EjecucioneManualVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class EjecucioneManualVO extends BasePaginator implements Serializable  {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La id proceso control. */
	private String idProcesoControl;
	
	/** La id proceso flujo. */
	private Long idProcesoFlujo;
	
	/** La id tipo proceso. */
	private String tipoProceso;
	
	/** La codigo producto. */
	private String codigoProducto;
	
	/** La codigo canal. */
	private String codigoCanal;
	
	/** La nombre juego. */
	private Long idJuegoTrama;
	
	/** La nombre Juego. */
	private String nombreJuego;
	
	/** La fecha. */
	private Date fecha;
	
	/** La fecha incio ejecucion. */
	private Date fechaIncioEjecucion;
	
	/** La fecha fin ejecucion. */
	private Date fechaFinEjecucion;
	
	/** La nombre usuario. */
	private String nombreUsuario;
	
	/** Long idCodigoUsuario. */
	private Long idCodigoUsuario;
	
	/** La estado proceso. */
	private String estadoProceso;
	
	/** La origen ejecucion. */
	private String origenEjecucion;
	
	/** La nombre trama. */
	private String nombreTrama;
	
	/** La mostrar boton accion. */
	private boolean deshabilitarBotonAccion;
	
	/** La mostrar opcion reprocesar. */
	private boolean mostrarOpcionReprocesar;
	
	/** La exclusion. */
	private boolean exclusion;
	
	/** La numero lote. */
	private String numeroLote;
	
	/** La list tramas procesadas. */
	private List<String> listTramasProcesadas = new ArrayList<>();
	
	/**
	 * Instancia un nuevo ejecucione manual vo.
	 */
	public EjecucioneManualVO() {
		super();
	}

	/**
	 * Instancia un nuevo ejecucione manual vo.
	 *
	 * @param idProcesoControl el id proceso control
	 * @param idProcesoFlujo el id proceso flujo
	 * @param tipoProceso el tipo proceso
	 * @param codigoProducto el codigo producto
	 * @param codigoCanal el codigo canal
	 * @param idJuegoTrama el id juego trama
	 * @param nombreJuego el nombre juego
	 * @param fecha el fecha
	 * @param fechaIncioEjecucion el fecha incio ejecucion
	 * @param fechaFinEjecucion el fecha fin ejecucion
	 * @param nombreUsuario el nombre usuario
	 * @param idCodigoUsuario el id codigo usuario
	 * @param estadoProceso el estado proceso
	 */
	public EjecucioneManualVO(String idProcesoControl, Long idProcesoFlujo,
			String tipoProceso, String codigoProducto, String codigoCanal,
			Long idJuegoTrama, String nombreJuego, Date fecha,
			Date fechaIncioEjecucion, Date fechaFinEjecucion,
			String nombreUsuario, Long idCodigoUsuario, String estadoProceso) {
		super();
		this.idProcesoControl = idProcesoControl;
		this.idProcesoFlujo = idProcesoFlujo;
		this.tipoProceso = tipoProceso;
		this.codigoProducto = codigoProducto;
		this.codigoCanal = codigoCanal;
		this.idJuegoTrama = idJuegoTrama;
		this.nombreJuego = nombreJuego;
		this.fecha = fecha;
		this.fechaIncioEjecucion = fechaIncioEjecucion;
		this.fechaFinEjecucion = fechaFinEjecucion;
		this.nombreUsuario = nombreUsuario;
		this.idCodigoUsuario = idCodigoUsuario;
		this.estadoProceso = estadoProceso;
	}

	/**
	 * Comprueba si es deshabilitar boton accion.
	 *
	 * @return true, si es deshabilitar boton accion
	 */
	public boolean isDeshabilitarBotonAccion() {
		return deshabilitarBotonAccion;
	}

	/**
	 * Comprueba si es mostrar opcion reprocesar.
	 *
	 * @return true, si es mostrar opcion reprocesar
	 */
	public boolean isMostrarOpcionReprocesar() {
		return mostrarOpcionReprocesar;
	}

	/**
	 * Comprueba si es exclusion.
	 *
	 * @return true, si es exclusion
	 */
	public boolean isExclusion() {
		return exclusion;
	}
	
}	
