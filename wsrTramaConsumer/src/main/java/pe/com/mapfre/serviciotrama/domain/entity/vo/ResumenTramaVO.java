package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.BasePaginator;

/**
 * La Class ResumenTramaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ResumenTramaVO extends BasePaginator implements Serializable  {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1845452002743296520L;

	/** La nombre trama. */
	private String nombreTrama;
	
	/** La numero lote. */
	private String numeroLote;
	
	/** La numero registros. */
	private Long numeroRegistros;
	
	/** La numero registros ok. */
	private Long numeroRegistrosOk;
	
	/** La numero registros error. */
	private Long numeroRegistrosError;
	
	/** La fecha lote. */
	private Date fechaLote;
	
	/** La id juego trama. */
	private Long idJuegoTrama;
	
	/** La tipo archivo. */
	private String tipoArchivo;
	
	/** La estado proceso. */
	private String estadoProceso;
	
	/**
	 * Instancia un nuevo resumen trama vo.
	 */
	public ResumenTramaVO(){
		super();
	}
	
	/**
	 * Instancia un nuevo resumen trama vo.
	 *
	 * @param nombreTrama el nombre trama
	 * @param numeroLote el numero lote
	 * @param numeroRegistros el numero registros
	 * @param numeroRegistrosOk el numero registros ok
	 * @param numeroRegistrosError el numero registros error
	 * @param fechaLote el fecha lote
	 * @param idJuegoTrama el id juego trama
	 */
	public ResumenTramaVO(String nombreTrama, String numeroLote,
			Long numeroRegistros, Long numeroRegistrosOk,
			Long numeroRegistrosError, Date fechaLote, Long idJuegoTrama) {
		super();
		this.nombreTrama = nombreTrama;
		this.numeroLote = numeroLote;
		this.numeroRegistros = numeroRegistros;
		this.numeroRegistrosOk = numeroRegistrosOk;
		this.numeroRegistrosError = numeroRegistrosError;
		this.fechaLote = fechaLote;
		this.idJuegoTrama = idJuegoTrama;
	}

}	
