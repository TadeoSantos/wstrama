package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

/**
 * La Class ConstancyConstant.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class ConstancyConstant {

	private ConstancyConstant() {
		
	}
	
	public static final String ESTADO_CONSTANCIA_NO_ENCONTRADA = "NO_ENCONTRADA" ;
	public static final String ESTADO_CONSTANCIA_ENCONTRADA_NO_VIGENTE = "ENCONTRADA_NO_VIGENTE" ;
	public static final String ESTADO_CONSTANCIA_ENCONTRADA = "ENCONTRADA" ;
	
}
