package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.util.ArrayList;
import java.util.List;

import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;

public class test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<GrupoConfiguracionTramaVO> listaGrupoConfiguracionTramaVO = new ArrayList<GrupoConfiguracionTramaVO>();
		
		GrupoConfiguracionTramaVO grupoConfiguracionTramaVO = new GrupoConfiguracionTramaVO();
		grupoConfiguracionTramaVO.setNombreConfiguracion("Grupo 1");
		List<GrupoConfiguracionTramaVO> listaConfiguracionTrama = new ArrayList<GrupoConfiguracionTramaVO>();
		GrupoConfiguracionTramaVO configuracionTramaVO = new GrupoConfiguracionTramaVO();
		configuracionTramaVO.setNombreConfiguracion("Conf 01");
		BigMemoryManager<String,ConfiguracionTramaDataVO> listaConfiguracionTramaData = new BigMemoryManager<String,ConfiguracionTramaDataVO>();
		configuracionTramaVO.setListaConfiguracionTramaData(listaConfiguracionTramaData);
		listaConfiguracionTrama.add(configuracionTramaVO);
		grupoConfiguracionTramaVO.setListaConfiguracionTrama(listaConfiguracionTrama);
		listaGrupoConfiguracionTramaVO.add(grupoConfiguracionTramaVO);

	}

}
