package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class TasmctrTramaMpePK.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Embeddable
public class TasmctrTramaMpePK implements Serializable {
 
	/** La Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** El fecha lote. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "FEC_LOTE" , length = 9)
	private Date fechaLote;
	
	/** El numero lote. */
	@Column(name = "NUM_LOTE" , length = 10)
	private String numeroLote;
	
	/** El mca emis directa mapfre. */
	@Column(name = "MCA_EMIS_MAPFRE" , length = 1)
	private String mcaEmisDirectaMapfre;
	
	/** El tipo trama. */
	@Column(name = "TIP_TRAMA" , length = 4)
	private String tipoTrama;
	
	
	/**
	 * Instancia un nuevo tasmctr trama mpePK.
	 */
	public TasmctrTramaMpePK() {
	}
	
	
	/**
	 * Instancia un nuevo tasmctr trama mpePK.
	 *
	 * @param fechaLote el fecha lote
	 * @param numeroLote el numero lote
	 * @param mcaEmisDirectaMapfre el mca emis directa mapfre
	 * @param tipoTrama el tipo trama
	 */
	public TasmctrTramaMpePK(Date fechaLote, String numeroLote, String mcaEmisDirectaMapfre, String tipoTrama ) {
		super();
		this.fechaLote = fechaLote;
		this.numeroLote = numeroLote;
		this.mcaEmisDirectaMapfre = mcaEmisDirectaMapfre;
		this.tipoTrama = tipoTrama;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fechaLote == null) ? 0 : fechaLote.hashCode());
		result = prime * result
				+ ((numeroLote == null) ? 0 : numeroLote.hashCode());
		result = prime * result
				+ ((mcaEmisDirectaMapfre == null) ? 0 : mcaEmisDirectaMapfre.hashCode());
		result = prime * result
				+ ((tipoTrama == null) ? 0 : tipoTrama.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TasmctrTramaMpePK other = (TasmctrTramaMpePK) obj;
		if (fechaLote == null) {
			if (other.fechaLote != null) {
				return false;
			}
		} else if (!fechaLote.equals(other.fechaLote)) {
			return false;
		}
		if (numeroLote == null) {
			if (other.numeroLote != null) {
				return false;
			}
		} else if (!numeroLote.equals(other.numeroLote)) {
			return false;
		}
		if (mcaEmisDirectaMapfre == null) {
			if (other.mcaEmisDirectaMapfre != null) {
				return false;
			}
		} else if (!mcaEmisDirectaMapfre.equals(other.mcaEmisDirectaMapfre)) {
			return false;
		}
		if (tipoTrama == null) {
			if (other.tipoTrama != null) {
				return false;
			}
		} else if (!tipoTrama.equals(other.tipoTrama)) {
			return false;
		}
		return true;
	}
     
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TasmctrTramaMpe [fechaLote= " + fechaLote + " ,numeroLote= " + numeroLote + " ,mcaEmisDirectaMapfre= " + mcaEmisDirectaMapfre + " ,tipoTrama= " + tipoTrama + " ]";
	}
	
}