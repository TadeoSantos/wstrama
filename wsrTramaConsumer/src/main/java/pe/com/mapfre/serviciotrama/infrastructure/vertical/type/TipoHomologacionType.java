package  pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class TipoHomologacionType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum TipoHomologacionType {

 	/** The simple. */
	 SIMPLE(6L , "tipoHomologacionType.simple"),
	 
	 /** The COMPUESTO. */
	 COMPUESTO(7L , "tipoHomologacionType.compuesto"),
	
 	/** The calculado. */
	 CALCULADO(8L , "tipoHomologacionType.calculado");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<Long, TipoHomologacionType> LOO_KUP_MAP = new HashMap<Long, TipoHomologacionType>();
	
	static {
		for (TipoHomologacionType s : EnumSet.allOf(TipoHomologacionType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private Long key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo campo fijo type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private TipoHomologacionType(Long key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the campo fijo type
	 */
	public static TipoHomologacionType get(Long key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public Long getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
