package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * La Class ExcelGrupoDataVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ExcelGrupoDataVO implements Serializable{

	private static final long serialVersionUID = -5366374995964846317L;

	private String tipo;
	private int columnaInicio;
	private int columnaFin;
	private int filaInicio;
	private int filaFin;

	public ExcelGrupoDataVO(String tipo) {
		this.tipo = tipo;
	}

	public static long getSerialVersionUID() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ExcelGrupoDataVO{" +
				"ENTIDAD=" + tipo+
				" columnaInicio=" + columnaInicio +
				", columnaFin=" + columnaFin +
				", filaInicio=" + filaInicio +
				", filaFin=" + filaFin +
				'}';
	}
}
