package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class CargaArchivoTramaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class CargaArchivoTramaVO implements Serializable {

	private static final long serialVersionUID = -4029230410581109524L;
//	select n_id_cab_carga_archivo as idCargaArchivo,c_nro_poliza as numeroPoliza,nro_solicitud as numeroSolicitud,n_id_config_juego_trama as idJuegoTrama from sgsm_cab_carga_archivo
	private Long idCargaArchivo;
	private String numeroPoliza;
	private String numeroSolicitud;
	private  Long idJuegoTrama;
	
	public CargaArchivoTramaVO() {
		super();
	}

	public CargaArchivoTramaVO(Long idCargaArchivo, String numeroPoliza,
			String numeroSolicitud, Long idJuegoTrama) {
		super();
		this.idCargaArchivo = idCargaArchivo;
		this.numeroPoliza = numeroPoliza;
		this.numeroSolicitud = numeroSolicitud;
		this.idJuegoTrama = idJuegoTrama;
	}

}
