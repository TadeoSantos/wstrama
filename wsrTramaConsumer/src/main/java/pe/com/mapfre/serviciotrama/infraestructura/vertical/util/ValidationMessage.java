package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

/**
 * La Class ValidationMessage.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class ValidationMessage {
	
	private ValidationMessage() {
		
	}
	
    public static final String REQUiRIED = "es requerido";
    public static final String MIN_CHARACTER = "caracteres como mínimo";
    public static final String MAX_CHARACTER = "caracteres como máximo";
    public static final String NOT_VALID = "no es válido";

}
