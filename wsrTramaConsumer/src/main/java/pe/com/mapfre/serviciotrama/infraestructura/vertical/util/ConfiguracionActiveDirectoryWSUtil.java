package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.io.Serializable;

/**
 * <ul>
 * <li>Copyright 2014 MAPFRE. Todos los derechos reservados.</li>
 * </ul> 
 * 
 * La Class ConfiguracionActiveDirectoryWSUtil.
 *
 * @author Tecnocom Per&uacute; S.A.C.
 * @version 1.0 , 01/04/2015
 * @since MYTRON-MAPFRE 1.0
 */
public final class  ConfiguracionActiveDirectoryWSUtil implements Serializable {
	
	
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	//Sincronizado con el properties :webservice.properties
	/** El URI_SERVICIO_WEB. */
	
	public static final String URI_SERVICIO_DESCARGA_DOCUMENTO = "webservice.descargar.documento.webservice.url";
			
	public static final String URI_SERVICIO_WEB = "webservice.url";//properties
	
	/** El URI_SERVICIO_WEB. */
	public static final String URI_SERVICIO_WEB_NUMERO_LOTE = "numero.lote.webservice.url";//properties
	
	/** El URI_SERVICIO_WEB. */
	public static final String URI_SERVICIO_WEB_SGSM_GENERAR_TRAMA = "sgsm.generar.trama.webservice.url";//properties
	
	/** La Constante URI_SERVICIO_CONSULTA_AD_WEBMETHOD. */
	public static final String URI_SERVICIO_CONSULTA_AD_WEBMETHOD = "webservice.consulta.activeDirectory.webMethod.url";
	
	//Sincronicar con el propertis:proxy.properties
	/** La Constante PROXY_ENABLED. */
	public static final String PROXY_ENABLED = "proxy.enabled";
	
	/** La Constante PROXY_HOST_LOCAL. */
	public static final  String PROXY_HOST_LOCAL = "proxy.host.local";
	
	/** La Constante PROXY_HOST_PORT_LOCAL. */
	public static final  String PROXY_HOST_PORT_LOCAL = "proxy.host.port.local";
	
	/** La Constante PROXY_HOST_USER_LOCAL. */
	public static final  String PROXY_HOST_USER_LOCAL = "proxy.host.user.local";
	
	/** La Constante PROXY_PASSWORD_LOCAL. */
	public static final  String PROXY_PASSWORD_LOCAL = "proxy.host.password.local";
	
	/** La Constante PROXY_SET_LOCAL. */
	public static final  String PROXY_SET_LOCAL = "proxy.host.set.local";
	
	/** La Constante URI_SERVICIO_WEB_ESTADO_ENVIO. */
	public static final String URI_SERVICIO_WEB_ESTADO_ENVIO = "webservice.WSEstadoEnvioPE";//properties
	
	/** La Constante URI_SERVICIO_WEB_MONITOREO_ENVIO. */
	public static final String URI_SERVICIO_WEB_MONITOREO_ENVIO = "webservice.WSMonitoreoEnvioPE";//properties
	
	/** La Constante URI_SERVICIO_WEB_POLIZA_ELECTRONICA_OBTENER. */
	public static final String URI_SERVICIO_WEB_POLIZA_ELECTRONICA_OBTENER = "webservice.polizaElectronica.url.obtener";//properties
	
	/** La Constante URI_SERVICIO_WEB_POLIZA_ELECTRONICA_ENVIO. */
	public static final String URI_SERVICIO_WEB_POLIZA_ELECTRONICA_ENVIO = "webservice.polizaElectronica.url.envio";//properties
	
	/** La Constante URI_SERVICIO_WEB_POLIZA_ELECTRONICA_CONSULTA. */
	public static final String URI_SERVICIO_WEB_POLIZA_ELECTRONICA_CONSULTA = "webservice.polizaElectronica.url.consulta";//properties
	
	public static final String URI_SERVICIO_WEB_COVID = "covid.webservice.url";//properties
	
	public static final String URI_SERVICIO_WEB_HPEXTREAM_URL = "webservice.hpextream.ccu.url";
	public static final String URI_SERVICIO_WEB_HPEXTREAM_TOKEN = "webservice.hpextream.ccu.token";
	public static final String URI_SERVICIO_WEB_HPEXTREAM_USUARIO = "webservice.hpextream.ccu.usuario";
	public static final String URI_SERVICIO_WEB_HPEXTREAM_PASSWORD = "webservice.hpextream.ccu.password";
	public static final String URI_SERVICIO_WEB_HPEXTREAM_NONCE = "webservice.hpextream.ccu.nonce";
	
	public static final String URI_SERVICIO_WEB_BUSCAR_DOCUMENTO_MASIVO = "webservice.buscar.documento.masivo";//properties
	public static final String URI_SERVICIO_WEB_BUSCAR_DOCUMENTO_MASIVO_USUARIO = "webservice.buscar.documento.usuario";//properties
	public static final String URI_SERVICIO_WEB_BUSCAR_DOCUMENTO_MASIVO_PASSWORD = "webservice.buscar.documento.password";//propertiesç
	public static final String URI_SERVICIO_WEB_BUSCAR_DOCUMENTO_MASIVO_REPOSITORY = "webservice.buscar.documento.repository";//properties
	
	/**
	 * Instancia un nuevo configuracion active directory ws util.
	 */
	private  ConfiguracionActiveDirectoryWSUtil() {
	}

	/**
	 * Obtiene web service.
	 *
	 * @param key el key
	 * @return web service
	 */
	public static String getWebService(String key) {
		return ConfiguracionCacheActiveDirectoryWSUtil.getInstance().getWebService(key);
	}
	
	/**
	 * Obtiene proxy.
	 *
	 * @param key el key
	 * @return proxy
	 */
	public static String getProxy(String key) {
		return ConfiguracionCacheActiveDirectoryWSUtil.getInstance().getProxy(key);
	}
	
	/**
	 * Obtiene proxy boolean.
	 *
	 * @param key el key
	 * @return proxy boolean
	 */
	public static boolean getProxyBoolean(String key) {
		return ConfiguracionCacheActiveDirectoryWSUtil.getInstance().getProxy(key).equalsIgnoreCase("true");
	}
}
