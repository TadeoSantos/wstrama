package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.CampoTablaVO;

/**
 * La Class CampoTablaDisponibleAutomaticoDaoLocal.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface CampoTablaDisponibleAutomaticoDaoLocal  extends IGenericRepository<Object, CampoTablaVO>  {
	
	/**
	 * Obtener columna map.
	 *
	 * @return the map
	 * @throws Exception the exception
	 */
	Map<String,Map<String,CampoTablaVO>> obtenerCampoTablaDisponibleMap(List<String> listaNombreEsquemaTabla);

	
}