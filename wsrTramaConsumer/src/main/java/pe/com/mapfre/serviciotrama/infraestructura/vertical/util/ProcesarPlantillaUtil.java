package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.io.File;
import java.io.StringWriter;
import java.util.Map;

import org.apache.log4j.Logger;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * La Class ProcesarPlantillaUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class ProcesarPlantillaUtil {
	
	/** La Constante LOG. */
	private static final Logger LOG = Logger.getLogger(ProcesarPlantillaUtil.class);
	
	/**
	 * Procesar plantilla by freemarker do.
	 *
	 * @param dataModel el data model
	 * @param basePath el base path
	 * @param template el template
	 * @return the string
	 */
	public static String procesarPlantillaByFreemarkerDo(Map<String,Object> dataModel,String basePath,String template) {
		String resultado = "rr";
	    // Configuration
	    Configuration cfg = new Configuration();

	    try {
	      // Set Directory for templates
	      cfg.setDirectoryForTemplateLoading(new File(basePath));
	      // load template
	      Template tpl = cfg.getTemplate(template);
	      
	      // data-model = dataModel
	      
	     StringWriter sw = new StringWriter();

		 tpl.process(dataModel, sw);
		 
		 resultado = sw.toString();
         
	    } catch (Exception e) {
	    	LOG.error(e.getMessage());
	    } 
	    
	    return resultado;

	  }
	
	
}
