package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import java.util.List;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.domain.entity.vo.EjecucioneManualVO;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ProcesoFlujoError;

/**
 * La Class ProcesoFlujoErrorAutomaticoDaoLocal.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface ProcesoFlujoErrorAutomaticoDaoLocal  extends  IGenericRepository<String, ProcesoFlujoError> {
	List<ProcesoFlujoError> listar(EjecucioneManualVO filtro, List<String> codigoErrorList,List<String> numeroLoteList) throws Exception;
	int contar (String numeroProceso, boolean incluir) throws Exception;
	int contarList(EjecucioneManualVO filtro, List<String> codigoErrorList, List<String> numeroLoteList);
	Integer verificarTipoSinTramaError(String numeroProceso, String codigoErrorSinTrama) throws Exception;
}