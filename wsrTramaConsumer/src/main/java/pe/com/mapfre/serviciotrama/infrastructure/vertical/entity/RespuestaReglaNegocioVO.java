package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class RespuestaReglaNegocioVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class RespuestaReglaNegocioVO extends BasePaginator implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 9166621199365741020L;
	
	
	/** La respuesta validacion. */
	private boolean respuestaValidacion;
	
	/** La error regla negocio v os. */
	private List<ErrorValidacionVO> errorReglaNegocioList = new ArrayList<ErrorValidacionVO>();
	
	/**
	 * Instancia un nuevo error regla vo.
	 */
	public RespuestaReglaNegocioVO() {
		super();
	}

	/**
	 * Comprueba si es respuesta validacion.
	 *
	 * @return true, si es respuesta validacion
	 */
	public boolean isRespuestaValidacion() {
		return respuestaValidacion;
	}

}
