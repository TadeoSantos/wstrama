package  pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class OpcionMigradorJuegoTramaType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum OpcionMigradorJuegoTramaType {

    /** La CABECERA. */
 	NUEVO("1" , "opcionMigradorJuegoTrama.nuevo"),
	
    /** El EXISTENTE. */
 	EXISTENTE("2" , "opcionMigradorJuegoTrama.existente");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, OpcionMigradorJuegoTramaType> LOO_KUP_MAP = new HashMap<String, OpcionMigradorJuegoTramaType>();
	
	static {
		for (OpcionMigradorJuegoTramaType s : EnumSet.allOf(OpcionMigradorJuegoTramaType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo tipo proceso type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private OpcionMigradorJuegoTramaType(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the tipo proceso type
	 */
	public static OpcionMigradorJuegoTramaType get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
