package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * La Class SMTPAuthenticator.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class SMTPAuthenticator extends Authenticator {
	
	/** La usuario. */
	private String usuario;
	
	/** La password. */
	private String password;
	
	/**
	 * Instancia un nuevo sMTP authenticator.
	 *
	 * @param usuario el usuario
	 * @param password el password
	 */
	public SMTPAuthenticator(String usuario, String password) {
		this.usuario = usuario;
		this.password = password;
	}
	
	/* (non-Javadoc)
	 * @see javax.mail.Authenticator#getPasswordAuthentication()
	 */
	@Override
	public PasswordAuthentication getPasswordAuthentication() {
	    return new PasswordAuthentication(usuario, password);

	}
}
