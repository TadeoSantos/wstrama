package pe.com.mapfre.serviciotrama.infrastructure.vertical.state;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La enum EstadoState.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum EstadoState {

    /** El ACTIVO. */
 	ACTIVO("A" , "estado.activo"),
	
    /** El INACTIVO. */
 	INACTIVO("I" , "estado.inactivo");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, EstadoState> LOO_KUP_MAP = new HashMap<String, EstadoState>();
	
	static {
		for (EstadoState s : EnumSet.allOf(EstadoState.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo estado state.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private EstadoState(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the estado state
	 */
	public static EstadoState get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
