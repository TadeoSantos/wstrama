package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ProcesoFlujoError.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_LOG_ORQ", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ProcesoFlujoError implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id error. */
    @Id
    @Column(name = "C_ID_ERROR" , length = 32)
    private String idError;
   
    /** El id control proceso. */
    @Column(name = "NUM_PROC" , length = 12)
    private String idControlProceso;
   
    /** El id indicador interno. */
    @Column(name = "N_INDIC_INTERNO" , length = 18)
    private Long idIndicadorInterno;
   
    /** El codigo error. */
    @Column(name = "C_COD_ERROR" , length = 12)
    private String codigoError;
   
    /** El mensaje error. */
    @Column(name = "C_MEN_ERROR" , length = 500)
    private String mensajeError;
   
    /** El descripcion error. */
    @Column(name = "C_MEN_DESCRIP" , length = 500)
    private String descripcionError;
   
    /** El fecha ocurrio error. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ERROR")
    private Date fechaOcurrioError;
    
    /** La tipo error. */
    @Column(name = "C_TIP_ERROR" , length = 1)
    private String tipoError;

    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
    
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
    
    @Column(name = "C_ID_ACT_FLUJO" , length = 32)
    private String identificadorUnicoActividad;
    
    /** El tarea ejecucion flujo. */
    @Column(name = "C_TAREA_EJEC_FLUJO" , length = 150)
    private String tareaEjecucionFlujo;
   
    /**
     * Instancia un nuevo proceso flujo error.
     */
    public ProcesoFlujoError() {
    }
   
   
    /**
     * Instancia un nuevo proceso flujo error.
     *
     * @param idError el id error
     * @param idControlProceso el id control proceso
     * @param idIndicadorInterno el id indicador interno
     * @param codigoError el codigo error
     * @param mensajeError el mensaje error
     * @param descripcionError el descripcion error
     * @param fechaOcurrioError el fecha ocurrio error
     * @param tipoError el tipo error
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public ProcesoFlujoError(String idError, String idControlProceso, Long idIndicadorInterno, String codigoError, String mensajeError, String descripcionError, Date fechaOcurrioError, String tipoError,  String usuarioCrea, Date fechaActualizacion, String identificadorUnicoActividad,  String tareaEjecucionFlujo) {
        super();
        this.idError = idError;
        this.idControlProceso = idControlProceso;
        this.idIndicadorInterno = idIndicadorInterno;
        this.codigoError = codigoError;
        this.mensajeError = mensajeError;
        this.descripcionError = descripcionError;
        this.fechaOcurrioError = fechaOcurrioError;
        this.tipoError = tipoError;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
        this.identificadorUnicoActividad = identificadorUnicoActividad;
        this.tareaEjecucionFlujo = tareaEjecucionFlujo;
    }
   
	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idError == null) ? 0 : idError.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ProcesoFlujoError other = (ProcesoFlujoError) obj;
        if (idError == null) {
            if (other.idError != null) {
                return false;
            }
        } else if (!idError.equals(other.idError)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ProcesoFlujoError [idError=" + idError + "]";
    }
   
}