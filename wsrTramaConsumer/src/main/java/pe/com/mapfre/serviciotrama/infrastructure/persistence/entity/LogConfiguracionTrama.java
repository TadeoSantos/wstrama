package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class LogConfiguracionTrama.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_LOG_CNF_TRA", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class LogConfiguracionTrama implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id log configuracion trama. */
    @Id
    @Column(name = "N_ID_LOG_CNF_TRA" , length = 36)
    private String idLogConfiguracionTrama;
   
    /** El configuracion trama. */
    @Column(name = "N_ID_CONFIG_TRAMA" , precision = 18 , scale = 0)
    private Long configuracionTrama;
   
    /** El nombre error. */
    @Column(name = "C_NOM_ERROR" , length = 100)
    private String nombreError;
   
    /** El tipo error. */
    @Column(name = "C_TIPO_ERROR" , length = 1)
    private String tipoError;
   
    /** El conponente validado. */
    @Column(name = "C_COMPONENTE" , length = 1)
    private String conponenteValidado;
   
    /** El nombre conponente validado. */
    @Column(name = "C_COMPONENTE_NAME" , length = 150)
    private String nombreConponenteValidado;
   
    /** El descripcion error. */
    @Column(name = "C_DESC_ERROR", length = 200)
    private String descripcionError;
   
    /** El fecha error. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FECHA_ERROR")
    private Date fechaError;
    
    
    /** La fila. */
    @Transient
    private String fila;
    
    /** La nombre campo. */
    @Transient
    private String nombreCampo;
   
    /**
     * Instancia un nuevo log configuracion trama.
     */
    public LogConfiguracionTrama() {
    }
   
   
    /**
     * Instancia un nuevo log configuracion trama.
     *
     * @param idLogConfiguracionTrama el id log configuracion trama
     * @param configuracionTrama el configuracion trama
     * @param nombreError el nombre error
     * @param tipoError el tipo error
     * @param conponenteValidado el conponente validado
     * @param nombreConponenteValidado el nombre conponente validado
     * @param descripcionError el descripcion error
     * @param fechaError el fecha error
     */
    public LogConfiguracionTrama(String idLogConfiguracionTrama, Long configuracionTrama,String nombreError, String tipoError, String conponenteValidado, String nombreConponenteValidado, String descripcionError, Date fechaError ) {
        super();
        this.idLogConfiguracionTrama = idLogConfiguracionTrama;
        this.configuracionTrama = configuracionTrama;
        this.nombreError = nombreError;
        this.tipoError = tipoError;
        this.conponenteValidado = conponenteValidado;
        this.nombreConponenteValidado = nombreConponenteValidado;
        this.descripcionError = descripcionError;
        this.fechaError = fechaError;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idLogConfiguracionTrama == null) ? 0 : idLogConfiguracionTrama.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        LogConfiguracionTrama other = (LogConfiguracionTrama) obj;
        if (idLogConfiguracionTrama == null) {
            if (other.idLogConfiguracionTrama != null) {
                return false;
            }
        } else if (!idLogConfiguracionTrama.equals(other.idLogConfiguracionTrama)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "LogConfiguracionTrama [idLogConfiguracionTrama=" + idLogConfiguracionTrama + "]";
    }
   
}