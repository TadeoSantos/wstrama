package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanMap;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.DataValidationConstraint;
import org.apache.poi.ss.usermodel.DataValidationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.util.TempFile;
import org.apache.poi.util.TempFile.DefaultTempFileCreationStrategy;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFName;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfigUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ObjectUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CollectionUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jasper.ArchivoUtilidades;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms.UUIDUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ExcelComboDataVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ExcelGrupoDataVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ExcelHederDataVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ExcelHederTitleVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

/**
 * La Class DataExportExcelPersonalizadoUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class DataExportExcelPersonalizadoUtil extends DataExportExcel implements Serializable {

	/** La Constante DD_MM_YYY_HH_MM_SS. */
	private static final String DD_MM_YYY_HH_MM_SS = "dd/mm/yyy hh:mm:ss";
	
	/** La Constante DD_MM_YYY. */
	private static final String DD_MM_YYY = "dd/mm/yyy";

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 6359062834392294265L;

	/** La Constante NOMBRE_LETRA. */
	public static final String NOMBRE_LETRA = "Arial";
	
	/** La Constante ROW_INFO_INDEX. */
	private static final String ROW_INFO_INDEX = "rowInfo.index";
	private static final String CELL_TYPE_STRING = "S";
	private static final String CELL_TYPE_DECIMAL = "D";
	private static final String CELL_TYPE_INTEGER = "I";
	
	/** La Constante MAXIMO_RANGE_EXCEL. */
	private static final Integer MAXIMO_RANGE_EXCEL = 65535;
	
	private static final Integer MAXIMO_RANGE_EXCEL_XLSX = 1000000;
	
	/** La Constante CANTIDAD_FILAS_USADO_CABECERA. */
	private static final Integer CANTIDAD_FILAS_USADO_CABECERA = 1;
	
		/** El log. */
	private static final Logger log = Logger.getLogger(DataExportExcelPersonalizadoUtil.class);
	/* NFIRE 08/06/15 inicio */
	/** La constante NOMBRE_LETRA_TABLERO_SINIESTROS. */
	public static final String NOMBRE_LETRA_TABLERO_SINIESTROS = "Calibri";
	/* NFIRE 08/06/15 fin */
	
	/** La Constante RUTA_RECURSOS. */
	public static final String RUTA_RECURSOS_BYTE_BUFFER = ConstanteConfigUtil.RUTA_RECURSOS_BYTE_BUFFER;

	/** The Constant IS_FORMULA. */
	private static final String IS_FORMULA = "${FORMULA}";
	
	/**
	 * Instancia un nuevo data export excel.
	 */
	
	static	{
		generarRutaTemp();
	}
	
	public DataExportExcelPersonalizadoUtil() {
		
	}
	
	private static void generarRutaTemp() {
		File dir = new File(ConstanteConfigUtil.RUTA_RECURSOS_DATA_POI_BUFFER);
		if (dir.exists()) {
			  ArchivoUtilidades.limpiarArchivoAllDirectory(ConstanteConfigUtil.RUTA_RECURSOS_DATA_POI_BUFFER);
		}
		dir.mkdirs();
		TempFile.setTempFileCreationStrategy(new DefaultTempFileCreationStrategy(dir));
	}
	
	/**
	 * Generar excel.
	 *
	 * @param listaHeaderData el lista header data
	 * @param listaData the lista data
	 * @param archivoName el archivo name
	 * @param titulo el titulo
	 * @param propiedadesMap el propiedades map
	 * @return true, en caso de exito
	 */
	public static byte[] generarExcel(List<ExcelHederDataVO> listaHeaderData, List<?> listaData,String archivoName,String titulo,Map<String,Object> propiedadesMap) {
		byte[] resultado = null;
		try {
			//Inicio Agregar coombo
			int hojaActiva = 0;
			Map<String,Integer> campoPosicionMap = new HashMap<String, Integer>();
			boolean isCombo = propiedadesMap.containsKey("comboData");
			boolean anexarHojaExistente = propiedadesMap.containsKey("anexarHojaExistente");
			int posicionCellCabecera = 0;
			if (isCombo) {
				 for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						String nombreColumna = cellHeaderVO.getNameAtribute();
						if (!campoPosicionMap.containsKey(nombreColumna)) {
							campoPosicionMap.put(nombreColumna, posicionCellCabecera);
						}
						posicionCellCabecera++;
				 }
			}
			//Fin Agregar coombo
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER) ;
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			//ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			HSSFWorkbook workbook = new HSSFWorkbook();
			if (isCombo) {
				generarComboHoja(workbook, propiedadesMap);
			}
			int cantidadData = listaData.size();
			int cantidadHojas = 1;
			int contador = 0;
			if (cantidadData > MAXIMO_RANGE_EXCEL ) {
				BigDecimal  bCantidadData = new BigDecimal(cantidadData);
				BigDecimal  maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL);
				BigDecimal  bCantidadHojas = bCantidadData.divide(maxRange,2,BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue(); 
			}
			DataFormat format = workbook.createDataFormat();
			CellStyle cellDateStyle = generarStyleDate(workbook);

			HSSFCellStyle style =  workbook.createCellStyle();
			style.setDataFormat(workbook.createDataFormat().getFormat("############"));

            //indicando un patron de formato
			CellStyle titleStyle = generarStyleTitle(workbook);
			// titleStyle.setLocked(false);		
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap != null && propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName") + "";
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				HSSFSheet sheet = workbook.createSheet(tituloFinal);
				hojaActiva++;
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap != null &&  propiedadesMap.containsKey("printTitleView")) {
					Row filaTitle = sheet.createRow(posicionRow);
					Cell heraderTitleCell = filaTitle.createCell((listaHeaderData.size() / 2));
					heraderTitleCell.setCellValue(titulo);
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap != null && propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio") + "") - 1;
				}
				Row fila = sheet.createRow(posicionRow);
				posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					String cellHeader = cellHeaderVO.getNameHeader();
					Cell heraderCell = fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
				}
				posicionRow = posicionRow + incrementroRow;
				//llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndex(cantidadDataPaginadorHoja);
		        int toIndex = toIndex(cantidadData,cantidadDataPaginadorHoja);
		       for (Object cellData : listaData.subList(fromIndex, toIndex)) {
					Row filaDet = sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						  String nombreColumna = cellHeaderVO.getNameAtribute();
							Object value = null;
							if (!nombreColumna.equals(ROW_INFO_INDEX)) {
								value = atributoValueComplejo(cellData,nombreColumna);
							} else {
								value = (contador + 1);
							}
							if (esFecha(nombreColumna)) {
								Object valueDate = verificarFornatoFecha(nombreColumna, value);
								if (esFechaData(valueDate)) {
									Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
									cellDetalle.setCellValue((Date)valueDate);
									if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Format")) {
										CellStyle cellDateStyleFormat = generarStyleDate(workbook);
										cellDateStyleFormat.setDataFormat(format.getFormat(propiedadesMap.get(nombreColumna + "Format") + ""));
										cellDetalle.setCellStyle(cellDateStyleFormat);
									} else {
										cellDetalle.setCellStyle(cellDateStyle);
									}
									
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								} else {
									Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
									cellDetalle.setCellValue(value == null ? "" : value.toString());
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								}
							} else if (StringUtils.isNotEmpty(cellHeaderVO.getTypeCell())) {
								Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
								if (CELL_TYPE_STRING.equals(cellHeaderVO.getTypeCell())) {
									cellDetalle.setCellType(Cell.CELL_TYPE_STRING);
							} else {
									cellDetalle.setCellType(Cell.CELL_TYPE_NUMERIC);
								}
	
								if (Cell.CELL_TYPE_NUMERIC == cellDetalle.getCellType()) {
									if (value == null) {
										cellDetalle.setCellValue("");
									} else {
										if (CELL_TYPE_INTEGER.equals(cellHeaderVO.getTypeCell())) {
											cellDetalle.setCellStyle(style);
											cellDetalle.setCellValue(Long.parseLong(value.toString()));
										} else {
											String valor = value.toString().replace(',','.');
											cellDetalle.setCellValue(Double.parseDouble(valor));
										}
									}
								} else {
									cellDetalle.setCellValue(value == null ? "" : value.toString());
								}
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue(value == null ? "" : value.toString());
								posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
							}
					}
					i++;
					contador++;
				}
		       //Inicio agregar combo
		        if (isCombo) {
		    	   int hoja = 0;
		    	   int cantidadRegistros = 100;
		    	   if (listaData != null && listaData.size() > 0) {
		    		   cantidadRegistros = listaData.size();
		    	   }
		    	   List<ExcelComboDataVO> listaDataCombo = (List<ExcelComboDataVO>)propiedadesMap.get("comboData");
		    	   if (listaDataCombo == null) {
		    		   listaDataCombo= new ArrayList<ExcelComboDataVO>(); 
		    	   }
		    	   for (ExcelComboDataVO excelComboDataVO : listaDataCombo) {
		    		   hojaActiva++;
						String nombreColumna = excelComboDataVO.getNombreCampo();
						Name namedCell = workbook.createName();
						namedCell.setNameName("hidden" + hoja);
						namedCell.setRefersToFormula("hidden" + hoja + "!$A$1:$A$" + excelComboDataVO.getListaExcelComboData().size());
						
						DVConstraint constraint = DVConstraint.createFormulaListConstraint("hidden" + hoja);
						CellRangeAddressList addressList = new CellRangeAddressList(posicionRow,cantidadRegistros , campoPosicionMap.get(nombreColumna),campoPosicionMap.get(nombreColumna));
						HSSFDataValidation validation = new HSSFDataValidation(addressList, constraint);
						
						validation.setSuppressDropDownArrow(false);	
						validation.setEmptyCellAllowed(false);
						validation.setShowPromptBox(false);
						validation.createErrorBox("Mensaje","Elemento no válido");
						
						sheet.addValidationData(validation);
						hoja++;
		    	   }
		    	   propiedadesMap.remove("comboData");//limpiando data
		       }
		        //fin agregar combo
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
	
				for (int ih = 0;ih < listaHeaderData.size(); ih++) {
					sheet.autoSizeColumn(autoSizeColunm,true);
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
				
			}
			boolean anexarHojaProcesar = false;
			 if (isCombo) {
				 if (anexarHojaExistente) {
					 anexarHojaProcesar = true;
				 } else {
					 HSSFSheet sheet = workbook.createSheet("Instruccion");
					 Row row = sheet.createRow(0);
					 Cell cell = row.createCell(0);
					 cell.setCellStyle(titleStyle);
					 cell.setCellValue("Debe Seleccionar lista Existente");
					 sheet.autoSizeColumn(0, true);
				 }
				 hojaActiva++;
			 } else {
				 if (anexarHojaExistente) {
					 anexarHojaProcesar = true;
				 }
			 }
			 if (anexarHojaProcesar) {
				 String nombreArchivo = (String) propiedadesMap.get("nombreArchivo");
				 int anexarHojaPosition = (Integer) propiedadesMap.get("anexarHojaPosition");
				 File rutaArchivo = new File(ConstanteConfigUtil.RUTA_GENERAL_TEMPLANTE + nombreArchivo);
				 HSSFWorkbook HSSFWorkbookAnexar =  ExcelUtil.leerExcel(rutaArchivo );
				 HSSFSheet sheetAnexar = HSSFWorkbookAnexar.getSheetAt(anexarHojaPosition - 1);
				 if (sheetAnexar != null) {
					 HSSFSheet sheet = workbook.createSheet(sheetAnexar.getSheetName());
					 TransferUtilExcel.copySheets(sheet, sheetAnexar);
				 }
			 }
			workbook.setActiveSheet(hojaActiva - 1);
		
			//workbook.write(outputStream);
			//resultado = outputStream.toByteArray();

			FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xls");
			 workbook.write(out);
			 workbook.close();
			 out.close();
			 cellDateStyle = null;
			 workbook = null;
			 out = null;
			 ExcelUtil.defaultLocaleProcess();
			//Runtime garbage = Runtime.getRuntime();
	   	 	//garbage.gc();
		} catch (Exception ex) {
			 ex.printStackTrace();
			 log.error(ex.getMessage(), ex);
			resultado = null;
		}
		return resultado;
	}

	public static byte[] generarExcelXLSX(List<ExcelHederDataVO> listaHeaderData, List<?> listaData, String archivoName, String titulo, Map<String, Object> propiedadesMap) {
		byte[] resultado = null;
		try {
			Map<Integer,Integer> columnWidtMaxMap = new HashMap<Integer, Integer>();
			boolean calcularWitchDemanda = propiedadesMap.containsKey("calcularWitchDemanda");
			// Inicio Agregar coombo
			int hojaActiva = 0;
			Map<String, Integer> campoPosicionMap = new HashMap<String, Integer>();
			boolean isCombo = propiedadesMap.containsKey("comboData");
			boolean anexarHojaExistente = propiedadesMap.containsKey("anexarHojaExistente");
			int posicionCellCabecera = 0;
			if (isCombo) {
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					String nombreColumna = cellHeaderVO.getNameAtribute();
					if (!campoPosicionMap.containsKey(nombreColumna)) {
						campoPosicionMap.put(nombreColumna, posicionCellCabecera);
					}
					posicionCellCabecera++;
				}
			}
			// Fin Agregar coombo
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER);
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			SXSSFWorkbook workbook = new SXSSFWorkbook(100);
			workbook.setCompressTempFiles(true); // temp files will be gzipped
			if (isCombo) {
				generarComboHojaXLSX(workbook, propiedadesMap);
			}
			int cantidadData = listaData.size();
			int cantidadHojas = 1;
			int contador = 0;
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue();
			}
			DataFormat format = workbook.createDataFormat();
			CellStyle cellDateStyle = generarStyleDate(workbook);
			// indicando un patron de formato
			CellStyle titleStyle = generarStyleTitle(workbook);
			// titleStyle.setLocked(false);
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap != null && propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName") + "";
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				if(tituloFinal.length()>30){
					tituloFinal="A"+cantidadDataPaginadorHoja;
				}

				if(StringUtils.isBlank(tituloFinal)){
					tituloFinal="A"+cantidadDataPaginadorHoja;
				}
				log.info("titulo final "+tituloFinal);
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal); //CREA UNA HOJA
				sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
				hojaActiva++;
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap != null && propiedadesMap.containsKey("printTitleView")) {
					SXSSFRow filaTitle = (SXSSFRow) sheet.createRow(posicionRow);
					SXSSFCell heraderTitleCell = (SXSSFCell) filaTitle.createCell((listaHeaderData.size() / 2));
					heraderTitleCell.setCellValue(titulo);
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap != null && propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio") + "") - 1;
				}
				SXSSFRow fila = (SXSSFRow) sheet.createRow(posicionRow);
				posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				int columnIndex = 0;
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					String cellHeader = cellHeaderVO.getNameHeader();
					SXSSFCell heraderCell = (SXSSFCell) fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
					
					if (calcularWitchDemanda) {
						int widtMaxActual =  ObjectUtil.objectToString(cellHeaderVO.getNameHeader()).length();
						double porcentaje = 0.20;
						if (!columnWidtMaxMap.containsKey(columnIndex)) {
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							columnWidtMaxMap.put(columnIndex, widtMaxActual);
						}
					}
					columnIndex++;
				}
				posicionRow = posicionRow + incrementroRow;
				// llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
				int toIndex = toIndexXlsx(cantidadData, cantidadDataPaginadorHoja);
				for (Object cellData : listaData.subList(fromIndex, toIndex)) {
					SXSSFRow filaDet = (SXSSFRow) sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					columnIndex = 0;
					for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						String nombreColumna = cellHeaderVO.getNameAtribute();
						Object value = null;
						if (!nombreColumna.equals(ROW_INFO_INDEX)) {
							value = atributoValueComplejo(cellData, nombreColumna);
						} else {
							value = (contador + 1);
						}
						if (esFecha(nombreColumna)) {
							Object valueDate = verificarFornatoFecha(nombreColumna, value);
							if (esFechaData(valueDate)) {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue((Date) valueDate);
								if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Format")) {
									CellStyle cellDateStyleFormat = generarStyleDate(workbook);
									cellDateStyleFormat.setDataFormat(format.getFormat(propiedadesMap.get(nombreColumna + "Format") + ""));
									value = FechaUtil.obtenerFechaFormatoPersonalizado((Date)valueDate, propiedadesMap.get(nombreColumna + "Format") + "");
									cellDetalle.setCellStyle(cellDateStyleFormat);
								} else {
									cellDetalle.setCellStyle(cellDateStyle);
								}
								
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue(value == null ? "" : value.toString());
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							}
						} else {
							SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue(value == null ? "" : value.toString());
							posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
						}
						
						if (calcularWitchDemanda) {
							int widtMaxActual =  ObjectUtil.objectToString(cellHeaderVO.getNameHeader()).length();
							double porcentaje = 0.20;
							if (!columnWidtMaxMap.containsKey(columnIndex)) {
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							} 
							int widtMax = columnWidtMaxMap.get(columnIndex);
							widtMaxActual =  ObjectUtil.objectToString(value).length();
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							if (widtMax < widtMaxActual) {
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
						}
						columnIndex++;
					}					
					i++;
					contador++;
				}
				// Inicio agregar combo
				if (isCombo) {
					int hoja = 0;
					int cantidadRegistros = 100;
					if (listaData != null && listaData.size() > 0) {
						cantidadRegistros = listaData.size();
					}
					List<ExcelComboDataVO> listaDataCombo = (List<ExcelComboDataVO>) propiedadesMap.get("comboData");
					if (listaDataCombo == null) {
						listaDataCombo = new ArrayList<ExcelComboDataVO>();
					}
					for (ExcelComboDataVO excelComboDataVO : listaDataCombo) {
						hojaActiva++;
						String nombreColumna = excelComboDataVO.getNombreCampo();
						XSSFName namedCell = (XSSFName) workbook.createName();
						namedCell.setNameName("hidden" + hoja);
						namedCell.setRefersToFormula("hidden" + hoja + "!$A$1:$A$" + excelComboDataVO.getListaExcelComboData().size());

						DataValidationHelper dvHelper = sheet.getDataValidationHelper();
						DataValidationConstraint dataValidation = dvHelper.createFormulaListConstraint("hidden" + hoja);
						CellRangeAddressList addressList = new CellRangeAddressList(posicionRow, cantidadRegistros, campoPosicionMap.get(nombreColumna), campoPosicionMap.get(nombreColumna));
						DataValidation validation = dvHelper.createValidation(dataValidation,addressList );

						validation.setSuppressDropDownArrow(true);
						validation.setEmptyCellAllowed(true);
						validation.setShowPromptBox(true);
						validation.createErrorBox("Mensaje", "Elemento no válido");

						sheet.addValidationData(validation);
						hoja++;
					}
					propiedadesMap.remove("comboData");// limpiando data
				}
				// fin agregar combo
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;

				for (int ih = 0; ih < listaHeaderData.size(); ih++) {
					if (calcularWitchDemanda) {
						try {
							int  width = columnWidtMaxMap.get(autoSizeColunm);
							 width *= 256;
					            int maxColumnWidth = 255 * 256; // The maximum column width for an individual cell is 255 characters
					            if (width > maxColumnWidth) {
					                width = maxColumnWidth;
					            }
							sheet.setColumnWidth(autoSizeColunm, width);
						} catch (Exception e) {
							//log.error("ERROR autoSizeColunm -->" + autoSizeColunm);
						}
					} else {
						sheet.autoSizeColumn(autoSizeColunm, true);
					}
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}

			}
			boolean anexarHojaProcesar = false;
			if (isCombo) {
				if (anexarHojaExistente) {
					anexarHojaProcesar = true;
				} else {
					/*SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Instruccion");
					sheet.setRandomAccessWindowSize(10);// keep 100 rows in memory, exceeding rows will be flushed to disk
					SXSSFRow row = (SXSSFRow) sheet.createRow(0);
					SXSSFCell cell = (SXSSFCell) row.createCell(0);
					cell.setCellStyle(titleStyle);
					cell.setCellValue("Debe Seleccionar lista Existente");
					sheet.autoSizeColumn(0, true);*/
				}
				//hojaActiva++;
			} else {
				if (anexarHojaExistente) {
					anexarHojaProcesar = true;
				}
			}
			if (anexarHojaProcesar) {
				String nombreArchivo = (String) propiedadesMap.get("nombreArchivo");
				int anexarHojaPosition = (Integer) propiedadesMap.get("anexarHojaPosition");
				File rutaArchivo = new File(ConstanteConfigUtil.RUTA_GENERAL_TEMPLANTE + nombreArchivo);
				SXSSFWorkbook sXSSFWorkbookAnexar = ExcelUtil.leerExcelsXlsx(rutaArchivo);
				SXSSFSheet sheetAnexar = (SXSSFSheet) sXSSFWorkbookAnexar.getSheetAt(anexarHojaPosition - 1);
				if (sheetAnexar != null) {
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(sheetAnexar.getSheetName());
					sheet.setRandomAccessWindowSize(10);// keep 100 rows in memory, exceeding rows will be flushed to disk
					TransferUtilExcel.copySheetsXLSX(sheet, sheetAnexar);
				}
			}
			//workbook.setActiveSheet(hojaActiva - 1);
			FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
			workbook.write(out);
			workbook.dispose();
			out.close();
			cellDateStyle = null;
			workbook = null;
			out = null;
			ExcelUtil.defaultLocaleProcess();
			//Runtime garbage = Runtime.getRuntime();
			//garbage.gc();
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			resultado = null;
		}
		return resultado;
	}
	
	/**
	  * Generar excel xlsx per.
	  *
	  * @param listaHeaderData the lista header data
	  * @param listaData the lista data
	  * @param archivoName the archivo name
	  * @param titulo the titulo
	  * @param propiedadesMap the propiedades map
	  * @return the string
	  */
	 public static String generarExcelXLSXPer(List<ExcelHederDataVO> listaHeaderData, List<?> listaData, String archivoName, String titulo, Map<String, Object> propiedadesMap) {
		String resultado = null;
		int hojaActiva = 0;
		boolean isFormula = propiedadesMap.containsKey("isFormula");
		boolean isBloqueo = propiedadesMap.containsKey("isBloqueo");
		boolean isFreezePane = propiedadesMap.containsKey("isFreezePane");
		Map<String,Map<String,String>> propiedadCeldaMap = new HashMap<String, Map<String,String>>();
		if (propiedadesMap.containsKey("propiedadCeldaMap"))  {
			propiedadCeldaMap = (Map<String,Map<String,String>> ) propiedadesMap.get("propiedadCeldaMap");
		}
		try {
			boolean exluirCabecera = propiedadesMap.containsKey("exluirCabecera");
			List<ExcelHederTitleVO> listaTituloFinal = new ArrayList<ExcelHederTitleVO>();
			if (propiedadesMap.containsKey("listaTituloFinal")) {
				listaTituloFinal = (List<ExcelHederTitleVO>)propiedadesMap.get("listaTituloFinal");
			}
			Map<Integer,Integer> columnWidtMaxMap = new HashMap<Integer, Integer>();
			boolean calcularWitchDemanda = propiedadesMap.containsKey("calcularWitchDemanda");
			// Inicio Agregar coombo
			Map<String, Integer> campoPosicionMap = new HashMap<String, Integer>();
			boolean isCombo = propiedadesMap.containsKey("comboData");
			boolean anexarHojaExistente = propiedadesMap.containsKey("anexarHojaExistente");
			int posicionCellCabecera = 0;
			if (isCombo) {
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					String nombreColumna = cellHeaderVO.getNameAtribute();
					if (!campoPosicionMap.containsKey(nombreColumna)) {
						campoPosicionMap.put(nombreColumna, posicionCellCabecera);
					}
					posicionCellCabecera++;
				}
			}
			// Fin Agregar coombo
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER);
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			SXSSFWorkbook workbook = new SXSSFWorkbook(100);
			workbook.setCompressTempFiles(true); // temp files will be gzipped
			if (isCombo) {
				generarComboHojaXLSX(workbook, propiedadesMap);
			}
			int cantidadData = listaData.size();
			int cantidadHojas = 1;
			int contador = 0;
			if (cantidadData > MAXIMO_RANGE_EXCEL) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue();
			}
			DataFormat format = workbook.createDataFormat();
			CellStyle cellDateStyle = generarStyleDate(workbook);
			// indicando un patron de formato
			CellStyle titleStyle = generarStyleTitle(workbook,(short)9);
			// titleStyle.setLocked(false);
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap != null && propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName") + "";
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal); //CREA UNA HOJA
				sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
				if (isBloqueo) {
					sheet.protectSheet(UUIDUtil.generarElementUUID());
				}
				int posicionRow = 0;
				int incrementroRow = 1;
				int maxPosicionRow = 0;
				if (!CollectionUtil.isEmpty(listaTituloFinal)) {
					for (ExcelHederTitleVO excelHederTitleVO : listaTituloFinal) {
						if (!excelHederTitleVO.isEsPiePagina()) {
							//excelHederTitleVO.setPosicionRow(posicionRow);
							int posicionRowVar =  excelHederTitleVO.getPosicionRow();
							int posicionCeldaVar =  excelHederTitleVO.getPosicionCelda();
							if (posicionRowVar > 0) {
								posicionRowVar = posicionRowVar - 1;
							}
							if (posicionCeldaVar > 0) {
								posicionCeldaVar = posicionCeldaVar - 1;
							}
							if (posicionRowVar > maxPosicionRow) {
								maxPosicionRow = posicionRowVar;
							}
							SXSSFRow  filaTitle = (SXSSFRow)sheet.getRow(posicionRowVar);
					    	if (filaTitle == null) {
					    	  	filaTitle = (SXSSFRow)sheet.createRow(posicionRowVar);
					    	}
							String tituloFinalPer = excelHederTitleVO.getNameHeader();
							SXSSFCell  heraderTitleCell = null;
							if (posicionCeldaVar > 0) {
								heraderTitleCell = (SXSSFCell)filaTitle.createCell(posicionCeldaVar);
							} else {
								heraderTitleCell = (SXSSFCell)filaTitle.createCell(0);
							}
							heraderTitleCell.setCellValue(tituloFinalPer);
							
							CellStyle titleStyleVar = generarStyleTitle(workbook,excelHederTitleVO.getFontHeightInPoints());
							heraderTitleCell.setCellStyle(titleStyleVar);
							heraderTitleCell.getCellStyle().setAlignment((short)excelHederTitleVO.getAling());//TODO
							
							if (excelHederTitleVO.getVerticalAlignment() > -1) {
								heraderTitleCell.getCellStyle().setVerticalAlignment((excelHederTitleVO.getVerticalAlignment()));//TODO
							}
							heraderTitleCell.getCellStyle().setWrapText(excelHederTitleVO.isWrapText());
					
							excelHederTitleVO.setPosicionRow(posicionRowVar);
							excelHederTitleVO.setPosicionCelda(posicionCeldaVar);
							if (excelHederTitleVO.getRotacion() != 0) {
								heraderTitleCell.getCellStyle().setRotation((short)excelHederTitleVO.getRotacion() );
							}
							//posicionRow = posicionRow + incrementroRow;
							if (excelHederTitleVO.getColumnIndex() > -1 && excelHederTitleVO.getWidth() > -1) {
								columnWidtMaxMap.put(excelHederTitleVO.getColumnIndex(), excelHederTitleVO.getWidth());
							}
						}
					}
					posicionRow = maxPosicionRow + posicionRow + incrementroRow;
				} else {
					if (propiedadesMap != null && propiedadesMap.containsKey("printTitleView")) {
						SXSSFRow filaTitle = (SXSSFRow) sheet.createRow(posicionRow);
						SXSSFCell heraderTitleCell = (SXSSFCell) filaTitle.createCell(0);
						heraderTitleCell.setCellValue(titulo);
						posicionRow = posicionRow + incrementroRow;
					}
				}
				// creando cabecera del datos
				if (propiedadesMap != null && propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio") + "") - 1;
				}
				SXSSFRow fila = (SXSSFRow) sheet.createRow(posicionRow);
				if (isFreezePane) {
					sheet.createFreezePane(0, posicionRow + 1);
				}
				posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				int columnIndex = 0;
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					if (!exluirCabecera) {
						String cellHeader = cellHeaderVO.getNameHeader();
						SXSSFCell heraderCell = (SXSSFCell) fila.createCell(posicionCellCabecera);
						heraderCell.setCellValue(cellHeader);
						heraderCell.setCellStyle(titleStyle);
						posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
					}
					if (calcularWitchDemanda) {
						int widtMaxActual =  ObjectUtil.objectToString(cellHeaderVO.getNameHeader()).length();
						double porcentaje = 0.20;
						if (!columnWidtMaxMap.containsKey(columnIndex)) {
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							columnWidtMaxMap.put(columnIndex, widtMaxActual);
						}
					}
					columnIndex++;
				}
				//posicionRow = posicionRow + incrementroRow;
				
				for (ExcelHederTitleVO excelHederTitleVO : listaTituloFinal) {
					if (!excelHederTitleVO.isEsPiePagina()) {
						try {
							//int firstRow, int lastRow, int firstCol, int lastCol
							CellRangeAddress cellRangeAddress = null;
							if (excelHederTitleVO.getCantidadAgrupar() > 0 && excelHederTitleVO.getCantidadAgruparHorizontal() == 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorder(cellRangeAddress, sheet,null);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  == 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorder(cellRangeAddress, sheet,null);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  > 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorder(cellRangeAddress, sheet,null);
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
					}					
				}
				// llenando la data
				int primeraFila = posicionRow+1;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
				int toIndex = toIndexXlsx(cantidadData, cantidadDataPaginadorHoja);
				for (Object cellData : listaData.subList(fromIndex, toIndex)) {
					SXSSFRow filaDet = (SXSSFRow) sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					columnIndex = 0;
					for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						SXSSFCell cellDetalle = null;
						String nombreColumna = cellHeaderVO.getNameAtribute();
						Object value = null;
						if (!nombreColumna.equals(ROW_INFO_INDEX) && !nombreColumna.contains(IS_FORMULA)) {
							value = atributoValueComplejo(cellData, nombreColumna);
						} else {
							if (nombreColumna.equals(ROW_INFO_INDEX)) {
								value = (contador + 1);
							} 
						}
						if (esFecha(nombreColumna)) {
							Object valueDate = verificarFornatoFecha(nombreColumna, value);
							if (esFechaData(valueDate)) {
								cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue((Date) valueDate);
								if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Format")) {
									cellDateStyle.setDataFormat(format.getFormat(propiedadesMap.get(nombreColumna + "Format") + ""));
									value = FechaUtil.obtenerFechaFormatoPersonalizado((Date)valueDate, propiedadesMap.get(nombreColumna + "Format") + "");
								}
								cellDetalle.setCellStyle(cellDateStyle);
								cellDetalle.getCellStyle().setLocked(false);
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue(value == null ? "" : value.toString());		
								cellDetalle.getCellStyle().setLocked(false);
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							}
						} else {
							cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);			
							if (nombreColumna.contains(IS_FORMULA)) {		
								cellDetalle.setCellType(XSSFCell.CELL_TYPE_FORMULA);
								cellDetalle.getCellStyle().setLocked(true);
								String[] nombreColumnaCalc  = nombreColumna.split("=>",-1);
								String formula = nombreColumnaCalc[1];
								formula = formula.replace("${N}", "" + (filaDet.getRowNum() + 1));
								cellDetalle.setCellFormula(formula);///TODO
								 //evaluator.evaluateInCell(cellDetalle);
							} else {
								if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Numeric")) {
									if (!StringUtil.isNullOrEmptyNumeriCero(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}
								} else {
									if (value != null && value instanceof Number) {
										cellDetalle.setCellValue(Double.parseDouble(value.toString()));							
									} else { 
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}
									cellDetalle.getCellStyle().setLocked(false);
								}
								
							}
							posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
						}
						if (calcularWitchDemanda) {
							int widtMaxActual =  ObjectUtil.objectToString(cellHeaderVO.getNameHeader()).length();
							double porcentaje = 0.20;
							if (!columnWidtMaxMap.containsKey(columnIndex)) {
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							} 
							int widtMax = columnWidtMaxMap.get(columnIndex);
							widtMaxActual =  ObjectUtil.objectToString(value).length();
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							if (widtMax < widtMaxActual) {
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
						}
						String keyPropiedad = i + "";
						if (propiedadCeldaMap.containsKey(keyPropiedad) && cellDetalle != null) {
							int posicionCelda = -1;
							boolean aplicaCeldaAll = true;
							boolean styleMarco = true;
							boolean boldNone = propiedadCeldaMap.get(keyPropiedad).containsKey("boldNone");
							if (propiedadCeldaMap.get(keyPropiedad).containsKey("posicionCelda") ) {
								posicionCelda = Integer.parseInt(propiedadCeldaMap.get(keyPropiedad).get("posicionCelda") + "");
								aplicaCeldaAll = false;
							}
							if (propiedadCeldaMap.get(keyPropiedad).containsKey("styleMarcoNone") ) {
								styleMarco = false;
							}
							if (propiedadCeldaMap.get(keyPropiedad).containsKey("cellStyle") && (aplicaCeldaAll || posicionCelda == columnIndex ) ) {
								short fontHeightInPoints = 9;
								if (propiedadCeldaMap.get(keyPropiedad).containsKey("fontHeightInPoints")) {
									fontHeightInPoints = (short) Integer.parseInt(propiedadCeldaMap.get(keyPropiedad).get("fontHeightInPoints") + "");
								}
								short aling = -1;
								if (propiedadCeldaMap.get(keyPropiedad).containsKey("aling")) {
									aling = (short) Integer.parseInt(propiedadCeldaMap.get(keyPropiedad).get("aling") + "");
								}
								CellStyle titleStyleVar = generarStyleTitleData(workbook,fontHeightInPoints,styleMarco,!boldNone);
								cellDetalle.setCellStyle(titleStyleVar);
								if (aling > 0) {
									cellDetalle.getCellStyle().setAlignment(aling);//TODO
								}
							}
							
						}
						columnIndex++;
					}					
					
					contador++;
					int posicionCeldaData = 0; 
					for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						try {
							if (cellHeaderVO.getCantidadAgrupar()  > 0) {
								CellRangeAddress cellRangeAddress = new CellRangeAddress(i + primeraFila, i + primeraFila, posicionCeldaData,((posicionCeldaData) - 1) + cellHeaderVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								//generarMergeRegionBorder(cellRangeAddress, sheet);
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
						posicionCeldaData++;
					}				
					i++;//TODO
				}			
				//Inicio escribir pie de pagina
				if (!CollectionUtil.isEmpty(listaTituloFinal)) {
					for (ExcelHederTitleVO excelHederTitleVO : listaTituloFinal) {
						if (excelHederTitleVO.isEsPiePagina()) {
							//excelHederTitleVO.setPosicionRow(posicionRow);
							int posicionRowVar =  excelHederTitleVO.getPosicionRow();
							int posicionCeldaVar =  excelHederTitleVO.getPosicionCelda();
							if (posicionRowVar > 0) {
								posicionRowVar = posicionRowVar - 1;
							}
							if (posicionCeldaVar > 0) {
								posicionCeldaVar = posicionCeldaVar - 1;
							}
							if (posicionRowVar > maxPosicionRow) {
								maxPosicionRow = posicionRowVar;
							}
							SXSSFRow  filaTitle = (SXSSFRow)sheet.getRow(posicionRowVar);
					    	if (filaTitle == null) {
					    	  	filaTitle = (SXSSFRow)sheet.createRow(posicionRowVar);
					    	}
							String tituloFinalPer = excelHederTitleVO.getNameHeader();
							SXSSFCell  heraderTitleCell = null;
							if (posicionCeldaVar > 0) {
								heraderTitleCell = (SXSSFCell)filaTitle.createCell(posicionCeldaVar);
							} else {
								heraderTitleCell = (SXSSFCell)filaTitle.createCell(0);
							}
							heraderTitleCell.setCellValue(tituloFinalPer);
							
							CellStyle titleStyleVar = generarStyleTitlePie(workbook,excelHederTitleVO.getFontHeightInPoints());
							heraderTitleCell.setCellStyle(titleStyleVar);
							heraderTitleCell.getCellStyle().setAlignment(excelHederTitleVO.getAling());
							
							if (excelHederTitleVO.getVerticalAlignment() > -1) {
								heraderTitleCell.getCellStyle().setVerticalAlignment(excelHederTitleVO.getVerticalAlignment());
							}
							heraderTitleCell.getCellStyle().setWrapText(excelHederTitleVO.isWrapText());
					
							excelHederTitleVO.setPosicionRow(posicionRowVar);
							excelHederTitleVO.setPosicionCelda(posicionCeldaVar);
							if (excelHederTitleVO.getRotacion() != 0) {
								heraderTitleCell.getCellStyle().setRotation((short)excelHederTitleVO.getRotacion() );
							}
							//posicionRow = posicionRow + incrementroRow;
							if (excelHederTitleVO.getColumnIndex() > -1 && excelHederTitleVO.getWidth() > -1) {
							//	columnWidtMaxMap.put(excelHederTitleVO.getColumnIndex(), excelHederTitleVO.getWidth());
							}
						}
					}
					//posicionRow = maxPosicionRow + posicionRow + incrementroRow;
				}
				for (ExcelHederTitleVO excelHederTitleVO : listaTituloFinal) {
					if (excelHederTitleVO.isEsPiePagina()) {
						try {
							//int firstRow, int lastRow, int firstCol, int lastCol
							CellRangeAddress cellRangeAddress = null;
							if (excelHederTitleVO.getCantidadAgrupar() > 0 && excelHederTitleVO.getCantidadAgruparHorizontal() == 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorderPie(cellRangeAddress, sheet);//TODO
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  == 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorderPie(cellRangeAddress, sheet);//TODO
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  > 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorderPie(cellRangeAddress, sheet);//TODO
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
					}					
				}
				//Fin escrubir pie de pagina
				// Inicio agregar combo
				if (isCombo) {
					int hoja = 0;
					int cantidadRegistros = 100;
					if (listaData != null && listaData.size() > 0) {
						cantidadRegistros = listaData.size();
					}
					List<ExcelComboDataVO> listaDataCombo = (List<ExcelComboDataVO>) propiedadesMap.get("comboData");
					if (listaDataCombo == null) {
						listaDataCombo = new ArrayList<ExcelComboDataVO>();
					}
					for (ExcelComboDataVO excelComboDataVO : listaDataCombo) {
						String nombreColumna = excelComboDataVO.getNombreCampo();
						XSSFName namedCell = (XSSFName) workbook.createName();
						namedCell.setNameName("hidden" + hoja);
						namedCell.setRefersToFormula("hidden" + hoja + "!$A$1:$A$" + excelComboDataVO.getListaExcelComboData().size());

						DataValidationHelper dvHelper = sheet.getDataValidationHelper();
						DataValidationConstraint dataValidation = dvHelper.createFormulaListConstraint("hidden" + hoja);
						CellRangeAddressList addressList = new CellRangeAddressList(posicionRow, cantidadRegistros, campoPosicionMap.get(nombreColumna), campoPosicionMap.get(nombreColumna));
						DataValidation validation = dvHelper.createValidation(dataValidation,addressList );
						validation.setSuppressDropDownArrow(true);
						validation.setEmptyCellAllowed(true);
						validation.setShowPromptBox(true);
						validation.createErrorBox("Mensaje", "Elemento no válido");
						sheet.addValidationData(validation);
						hoja++;
					}
					propiedadesMap.remove("comboData");// limpiando data
				}
				// fin agregar combo
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
				for (int ih = 0; ih < listaHeaderData.size(); ih++) {
					if (calcularWitchDemanda) {
						try {
							int  width = columnWidtMaxMap.get(autoSizeColunm);
							 width *= 256;
					         int maxColumnWidth = 255 * 256; // The maximum column width for an individual cell is 255 characters
					         if (width > maxColumnWidth) {
					             width = maxColumnWidth;
					         }
							sheet.setColumnWidth(autoSizeColunm, width);
						} catch (Exception e) {
							//log.error("ERROR autoSizeColunm -->" + autoSizeColunm);
						}
					} else {
						sheet.autoSizeColumn(autoSizeColunm, true);
					}
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
			}
			boolean anexarHojaProcesar = false;
			 if (isCombo) {
				 if (anexarHojaExistente) {
					 anexarHojaProcesar = true;
				 } 
				 hojaActiva++;
			 } else {
				 if (anexarHojaExistente) {
					 anexarHojaProcesar = true;
				 }
			 }
			 if (anexarHojaProcesar) {
				 String nombreArchivo = (String) propiedadesMap.get("nombreArchivo");
				 int anexarHojaPosition = (Integer) propiedadesMap.get("anexarHojaPosition");
				 File rutaArchivo = new File(ConstanteConfigUtil.RUTA_GENERAL_TEMPLANTE + nombreArchivo);
//				 XSSFWorkbook HSSFWorkbookAnexar =  ExcelUtil.leerExcelXlsx(rutaArchivo );
//				 XSSFSheet sheetAnexar = HSSFWorkbookAnexar.getSheetAt(anexarHojaPosition - 1);
//				 if (sheetAnexar != null) {
//					 SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(sheetAnexar.getSheetName());
//					 sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
//					 TransferUtilExcel.copySheetsXLSX(sheet, sheetAnexar);
//				 }
				 
				 SXSSFWorkbook sXSSFWorkbookAnexar =  ExcelUtil.leerExcelsXlsx(rutaArchivo );
				 SXSSFSheet sheetAnexar = (SXSSFSheet)sXSSFWorkbookAnexar.getSheetAt(anexarHojaPosition - 1);
				 if (sheetAnexar != null) {
					 SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(sheetAnexar.getSheetName());
					 sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
					 TransferUtilExcel.copySheetsXLSX(sheet, sheetAnexar);
				 }
				 
			 }
			//workbook.setActiveSheet(hojaActiva - 1);			
			if (isFormula) {
				try {//SXSSFWorkbook
					XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook.getXSSFWorkbook());
					//workbook.setForceFormulaRecalculation(true);
				} catch (Exception e) {
					log.error("Error ", e);
				}
			} //TODO
			FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
			workbook.write(out);
			workbook.dispose();
			out.close();
			cellDateStyle = null;
			workbook = null;
			out = null;
			ExcelUtil.defaultLocaleProcess();
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			resultado = null;
		}
		return resultado;
	}


	 public static String generarExcelXLSXPerWithGroup(List<ExcelHederDataVO> listaHeaderData, List<?> listaData, String archivoName, String titulo, Map<String, Object> propiedadesMap) {
		String resultado = null;
		int hojaActiva = 0;
		boolean isFormula = propiedadesMap.containsKey("isFormula");
		boolean isBloqueo = propiedadesMap.containsKey("isBloqueo");
		boolean isFreezePane = propiedadesMap.containsKey("isFreezePane");
		Map<String,Map<String,String>> propiedadCeldaMap = new HashMap<String, Map<String,String>>();
		if (propiedadesMap.containsKey("propiedadCeldaMap"))  {
			propiedadCeldaMap = (Map<String,Map<String,String>> ) propiedadesMap.get("propiedadCeldaMap");
		}
		try {
			log.error(" generarExcelXLSXPerWithGroup "+archivoName);
			boolean exluirCabecera = propiedadesMap.containsKey("exluirCabecera");
			List<ExcelHederTitleVO> listaTituloFinal = new ArrayList<ExcelHederTitleVO>();
			if (propiedadesMap.containsKey("listaTituloFinal")) {
				listaTituloFinal = (List<ExcelHederTitleVO>)propiedadesMap.get("listaTituloFinal");
			}
			Map<Integer,Integer> columnWidtMaxMap = new HashMap<Integer, Integer>();
			boolean calcularWitchDemanda = propiedadesMap.containsKey("calcularWitchDemanda");
			// Inicio Agregar coombo
			Map<String, Integer> campoPosicionMap = new HashMap<String, Integer>();
			boolean isCombo = propiedadesMap.containsKey("comboData");
			boolean anexarHojaExistente = propiedadesMap.containsKey("anexarHojaExistente");
			int posicionCellCabecera = 0;
			if (isCombo) {
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					String nombreColumna = cellHeaderVO.getNameAtribute();
					if (!campoPosicionMap.containsKey(nombreColumna)) {
						campoPosicionMap.put(nombreColumna, posicionCellCabecera);
					}
					posicionCellCabecera++;
				}
			}
			// Fin Agregar coombo
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER);
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			SXSSFWorkbook workbook = new SXSSFWorkbook(100);
			workbook.setCompressTempFiles(true); // temp files will be gzipped
			if (isCombo) {
				generarComboHojaXLSX(workbook, propiedadesMap);
			}
			int cantidadData = listaData.size();
			int cantidadHojas = 1;
			int contador = 0;
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue();
			}
			DataFormat format = workbook.createDataFormat();
			CellStyle cellStyle = generarStyleData(workbook);
			boolean usarBorder= propiedadesMap.containsKey("usarBorder");
			CellStyle cellDateStyle = generarStyleDate(workbook);
			if(usarBorder){
				cellDateStyle=generarStyleDateBorder(workbook);
			}
			// indicando un patron de formato
			CellStyle titleStyle = generarStyleTitle(workbook,(short)9);
			titleStyle.setWrapText(true);
			// titleStyle.setLocked(false);
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap != null && propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName") + "";
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal); //CREA UNA HOJA
				sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
				if (isBloqueo) {
					sheet.protectSheet(UUIDUtil.generarElementUUID());
				}
				int posicionRow = 0;
				int incrementroRow = 1;
				int maxPosicionRow = 0;
				if (!CollectionUtil.isEmpty(listaTituloFinal)) {
					for (ExcelHederTitleVO excelHederTitleVO : listaTituloFinal) {
						if (!excelHederTitleVO.isEsPiePagina()) {
							//excelHederTitleVO.setPosicionRow(posicionRow);
							int posicionRowVar =  excelHederTitleVO.getPosicionRow();
							int posicionCeldaVar =  excelHederTitleVO.getPosicionCelda();
							if (posicionRowVar > 0) {
								posicionRowVar = posicionRowVar - 1;
							}
							if (posicionCeldaVar > 0) {
								posicionCeldaVar = posicionCeldaVar - 1;
							}
							if (posicionRowVar > maxPosicionRow) {
								maxPosicionRow = posicionRowVar;
							}
							SXSSFRow  filaTitle = (SXSSFRow)sheet.getRow(posicionRowVar);
					    	if (filaTitle == null) {
					    	  	filaTitle = (SXSSFRow)sheet.createRow(posicionRowVar);
					    	}
							String tituloFinalPer = excelHederTitleVO.getNameHeader();
							SXSSFCell  heraderTitleCell = null;
							if (posicionCeldaVar > 0) {
								heraderTitleCell = (SXSSFCell)filaTitle.createCell(posicionCeldaVar);
							} else {
								heraderTitleCell = (SXSSFCell)filaTitle.createCell(0);
							}
							heraderTitleCell.setCellValue(tituloFinalPer);

							CellStyle titleStyleVar = generarStyleTitle(workbook,excelHederTitleVO.getFontHeightInPoints());
							heraderTitleCell.setCellStyle(titleStyleVar);
							heraderTitleCell.getCellStyle().setAlignment((short)excelHederTitleVO.getAling());//TODO

							if (excelHederTitleVO.getVerticalAlignment() > -1) {
								heraderTitleCell.getCellStyle().setVerticalAlignment((excelHederTitleVO.getVerticalAlignment()));//TODO
							}
							heraderTitleCell.getCellStyle().setWrapText(excelHederTitleVO.isWrapText());

							excelHederTitleVO.setPosicionRow(posicionRowVar);
							excelHederTitleVO.setPosicionCelda(posicionCeldaVar);
							if (excelHederTitleVO.getRotacion() != 0) {
								heraderTitleCell.getCellStyle().setRotation((short)excelHederTitleVO.getRotacion() );
							}
							//posicionRow = posicionRow + incrementroRow;
							if (excelHederTitleVO.getColumnIndex() > -1 && excelHederTitleVO.getWidth() > -1) {
								columnWidtMaxMap.put(excelHederTitleVO.getColumnIndex(), excelHederTitleVO.getWidth());
							}
						}
					}
					posicionRow = maxPosicionRow + posicionRow + incrementroRow;
				} else {
					if (propiedadesMap != null && propiedadesMap.containsKey("printTitleView")) {
						SXSSFRow filaTitle = (SXSSFRow) sheet.createRow(posicionRow);
						SXSSFCell heraderTitleCell = (SXSSFCell) filaTitle.createCell(0);
						heraderTitleCell.setCellValue(titulo);
						posicionRow = posicionRow + incrementroRow;
					}
				}
				// creando cabecera del datos
				if (propiedadesMap != null && propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio") + "") - 1;
				}

				SXSSFRow fila = (SXSSFRow) sheet.createRow(posicionRow);
				if (isFreezePane) {
					sheet.createFreezePane(0, posicionRow + 1);
				}
				posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				int columnIndex = 0;
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					if (!exluirCabecera) {
						String cellHeader = cellHeaderVO.getNameHeader();
						SXSSFCell heraderCell = (SXSSFCell) fila.createCell(posicionCellCabecera);
						heraderCell.setCellValue(cellHeader);
						heraderCell.setCellStyle(titleStyle);
						posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
					}
					if (calcularWitchDemanda) {
						int widtMaxActual =  ObjectUtil.objectToString(cellHeaderVO.getNameHeader()).length();
						double porcentaje = 0.20;
						if (!columnWidtMaxMap.containsKey(columnIndex)) {
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							columnWidtMaxMap.put(columnIndex, widtMaxActual);
						}
					}
					columnIndex++;
				}
				//posicionRow = posicionRow + incrementroRow;

				for (ExcelHederTitleVO excelHederTitleVO : listaTituloFinal) {
					if (!excelHederTitleVO.isEsPiePagina()) {
						try {
							//int firstRow, int lastRow, int firstCol, int lastCol
							CellRangeAddress cellRangeAddress = null;
							if (excelHederTitleVO.getCantidadAgrupar() > 0 && excelHederTitleVO.getCantidadAgruparHorizontal() == 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorder(cellRangeAddress, sheet,null);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  == 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorder(cellRangeAddress, sheet,null);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  > 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorder(cellRangeAddress, sheet,null);
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
					}
				}
				// llenando la data
				int primeraFila = posicionRow+1;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
				int toIndex = toIndexXlsx(cantidadData, cantidadDataPaginadorHoja);

				List<ExcelGrupoDataVO> columnGroupList = new ArrayList<ExcelGrupoDataVO>();
				Map<String,Object> mapColumnValue = new HashMap<String, Object>();
				Map<Integer,ExcelGrupoDataVO> mapColumnGroup = new HashMap<Integer, ExcelGrupoDataVO>();
				boolean lastFileFin=false;
				for (Object cellData : listaData.subList(fromIndex, toIndex)) {
					SXSSFRow filaDet = (SXSSFRow) sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					columnIndex = 0;
					for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						if(columnIndex==0) lastFileFin=false;
						SXSSFCell cellDetalle = null;
						String nombreColumna = cellHeaderVO.getNameAtribute();
						Object value = null;
						if (!nombreColumna.equals(ROW_INFO_INDEX) && !nombreColumna.contains(IS_FORMULA)) {
							value = atributoValueComplejo(cellData, nombreColumna);
						} else {
							if (nombreColumna.equals(ROW_INFO_INDEX)) {
								value = (contador + 1);
							}
						}
						if(propiedadesMap.containsKey(nombreColumna+"Agrupar") && (lastFileFin || (mapColumnValue.get(nombreColumna)==null || !mapColumnValue.get(nombreColumna).equals(value)))) {

							if(mapColumnGroup.get(columnIndex)==null){
								ExcelGrupoDataVO columnGroup = new ExcelGrupoDataVO(String.valueOf(columnIndex));
								columnGroup.setFilaInicio(1);
								columnGroup.setColumnaFin(columnIndex);
								columnGroup.setColumnaInicio(columnIndex);
								mapColumnGroup.put(columnIndex,columnGroup);
							}

							mapColumnGroup.get(columnIndex).setFilaFin(i+primeraFila-1);

							columnGroupList.add(mapColumnGroup.get(columnIndex));
							mapColumnValue.put(nombreColumna,value) ;
							ExcelGrupoDataVO columnGroup= new ExcelGrupoDataVO(String.valueOf(columnIndex));
							columnGroup.setFilaInicio(i+primeraFila);
							columnGroup.setColumnaInicio(columnIndex);
							columnGroup.setColumnaFin(columnIndex);
							mapColumnGroup.put(columnIndex,columnGroup);
							lastFileFin=true;

						}



						if (esFecha(nombreColumna)) {
							Object valueDate = verificarFornatoFecha(nombreColumna, value);
							if (esFechaData(valueDate)) {
								cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue((Date) valueDate);
								if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Format")) {
									cellDateStyle.setDataFormat(format.getFormat(propiedadesMap.get(nombreColumna + "Format") + ""));
									value = FechaUtil.obtenerFechaFormatoPersonalizado((Date)valueDate, propiedadesMap.get(nombreColumna + "Format") + "");
								}
								cellDetalle.setCellStyle(cellDateStyle);
								cellDetalle.getCellStyle().setLocked(false);
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue(value == null ? "" : value.toString());

								if(usarBorder)
									cellDetalle.setCellStyle(cellStyle);
								else
									cellDetalle.getCellStyle().setLocked(false);

								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							}
						} else {
							cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
							if (nombreColumna.contains(IS_FORMULA)) {
								cellDetalle.setCellType(XSSFCell.CELL_TYPE_FORMULA);

								if(usarBorder)
									cellDetalle.setCellStyle(cellStyle);
								else
									cellDetalle.getCellStyle().setLocked(false);

								String[] nombreColumnaCalc  = nombreColumna.split("=>",-1);
								String formula = nombreColumnaCalc[1];
								formula = formula.replace("${N}", "" + (filaDet.getRowNum() + 1));
								cellDetalle.setCellFormula(formula);///TODO
								 //evaluator.evaluateInCell(cellDetalle);
							} else {
								if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Numeric")) {
									if (!StringUtil.isNullOrEmptyNumeriCero(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}
								} else {
									if (value != null && value instanceof Number) {
										cellDetalle.setCellValue(Double.parseDouble(value.toString()));
									} else {
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}

									if(usarBorder)
										cellDetalle.setCellStyle(cellStyle);
									else
										cellDetalle.getCellStyle().setLocked(false);

								}

							}
							posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
						}
						if (calcularWitchDemanda) {
							int widtMaxActual =  ObjectUtil.objectToString(cellHeaderVO.getNameHeader()).length();
							double porcentaje = 0.20;
							if (!columnWidtMaxMap.containsKey(columnIndex)) {
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
							int widtMax = columnWidtMaxMap.get(columnIndex);
							widtMaxActual =  ObjectUtil.objectToString(value).length();
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							if (widtMax < widtMaxActual) {
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
						}
						String keyPropiedad = i + "";
						if (propiedadCeldaMap.containsKey(keyPropiedad) && cellDetalle != null) {
							int posicionCelda = -1;
							boolean aplicaCeldaAll = true;
							boolean styleMarco = true;
							boolean boldNone = propiedadCeldaMap.get(keyPropiedad).containsKey("boldNone");
							if (propiedadCeldaMap.get(keyPropiedad).containsKey("posicionCelda") ) {
								posicionCelda = Integer.parseInt(propiedadCeldaMap.get(keyPropiedad).get("posicionCelda") + "");
								aplicaCeldaAll = false;
							}
							if (propiedadCeldaMap.get(keyPropiedad).containsKey("styleMarcoNone") ) {
								styleMarco = false;
							}
							if (propiedadCeldaMap.get(keyPropiedad).containsKey("cellStyle") && (aplicaCeldaAll || posicionCelda == columnIndex ) ) {
								short fontHeightInPoints = 9;
								if (propiedadCeldaMap.get(keyPropiedad).containsKey("fontHeightInPoints")) {
									fontHeightInPoints = (short) Integer.parseInt(propiedadCeldaMap.get(keyPropiedad).get("fontHeightInPoints") + "");
								}
								short aling = -1;
								if (propiedadCeldaMap.get(keyPropiedad).containsKey("aling")) {
									aling = (short) Integer.parseInt(propiedadCeldaMap.get(keyPropiedad).get("aling") + "");
								}
								CellStyle titleStyleVar = generarStyleTitleData(workbook,fontHeightInPoints,styleMarco,!boldNone);
								cellDetalle.setCellStyle(titleStyleVar);
								if (aling > 0) {
									cellDetalle.getCellStyle().setAlignment(aling);//TODO
								}
							}

						}
						columnIndex++;
					}

					contador++;
					int posicionCeldaData = 0;
					for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						try {
							if (cellHeaderVO.getCantidadAgrupar()  > 0) {
								CellRangeAddress cellRangeAddress = new CellRangeAddress(i + primeraFila, i + primeraFila, posicionCeldaData,((posicionCeldaData) - 1) + cellHeaderVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								//generarMergeRegionBorder(cellRangeAddress, sheet);
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
						posicionCeldaData++;
					}
					i++;//TODO
				}

				Set<Integer> keySet=mapColumnGroup.keySet();

				for (Integer next : keySet) {
					mapColumnGroup.get(next).setFilaFin(i+primeraFila-1);
					columnGroupList.add(mapColumnGroup.get(next));
				}

				for (ExcelGrupoDataVO next : columnGroupList) {
					if(next.getFilaInicio()> next.getFilaFin()) continue;
					CellRangeAddress range=new CellRangeAddress(next.getFilaInicio(), next.getFilaFin(), next.getColumnaInicio(), next.getColumnaFin());
					sheet.addMergedRegion(range);
//					generarMergeRegionBorder(range,sheet);
				}

				//Inicio escribir pie de pagina
				if (!CollectionUtil.isEmpty(listaTituloFinal)) {
					for (ExcelHederTitleVO excelHederTitleVO : listaTituloFinal) {
						if (excelHederTitleVO.isEsPiePagina()) {
							//excelHederTitleVO.setPosicionRow(posicionRow);
							int posicionRowVar =  excelHederTitleVO.getPosicionRow();
							int posicionCeldaVar =  excelHederTitleVO.getPosicionCelda();
							if (posicionRowVar > 0) {
								posicionRowVar = posicionRowVar - 1;
							}
							if (posicionCeldaVar > 0) {
								posicionCeldaVar = posicionCeldaVar - 1;
							}
							if (posicionRowVar > maxPosicionRow) {
								maxPosicionRow = posicionRowVar;
							}
							SXSSFRow  filaTitle = (SXSSFRow)sheet.getRow(posicionRowVar);
					    	if (filaTitle == null) {
					    	  	filaTitle = (SXSSFRow)sheet.createRow(posicionRowVar);
					    	}
							String tituloFinalPer = excelHederTitleVO.getNameHeader();
							SXSSFCell  heraderTitleCell = null;
							if (posicionCeldaVar > 0) {
								heraderTitleCell = (SXSSFCell)filaTitle.createCell(posicionCeldaVar);
							} else {
								heraderTitleCell = (SXSSFCell)filaTitle.createCell(0);
							}
							heraderTitleCell.setCellValue(tituloFinalPer);

							CellStyle titleStyleVar = generarStyleTitlePie(workbook,excelHederTitleVO.getFontHeightInPoints());
							heraderTitleCell.setCellStyle(titleStyleVar);
							heraderTitleCell.getCellStyle().setAlignment(excelHederTitleVO.getAling());

							if (excelHederTitleVO.getVerticalAlignment() > -1) {
								heraderTitleCell.getCellStyle().setVerticalAlignment(excelHederTitleVO.getVerticalAlignment());
							}
							heraderTitleCell.getCellStyle().setWrapText(excelHederTitleVO.isWrapText());

							excelHederTitleVO.setPosicionRow(posicionRowVar);
							excelHederTitleVO.setPosicionCelda(posicionCeldaVar);
							if (excelHederTitleVO.getRotacion() != 0) {
								heraderTitleCell.getCellStyle().setRotation((short)excelHederTitleVO.getRotacion() );
							}
							//posicionRow = posicionRow + incrementroRow;
							if (excelHederTitleVO.getColumnIndex() > -1 && excelHederTitleVO.getWidth() > -1) {
							//	columnWidtMaxMap.put(excelHederTitleVO.getColumnIndex(), excelHederTitleVO.getWidth());
							}
						}
					}
					//posicionRow = maxPosicionRow + posicionRow + incrementroRow;
				}
				for (ExcelHederTitleVO excelHederTitleVO : listaTituloFinal) {
					if (excelHederTitleVO.isEsPiePagina()) {
						try {
							//int firstRow, int lastRow, int firstCol, int lastCol
							CellRangeAddress cellRangeAddress = null;
							if (excelHederTitleVO.getCantidadAgrupar() > 0 && excelHederTitleVO.getCantidadAgruparHorizontal() == 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorderPie(cellRangeAddress, sheet);//TODO
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  == 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorderPie(cellRangeAddress, sheet);//TODO
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  > 0) {
								cellRangeAddress = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(cellRangeAddress);
								generarMergeRegionBorderPie(cellRangeAddress, sheet);//TODO
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
					}
				}
				//Fin escrubir pie de pagina
				// Inicio agregar combo
				if (isCombo) {
					int hoja = 0;
					int cantidadRegistros = 100;
					if (listaData != null && listaData.size() > 0) {
						cantidadRegistros = listaData.size();
					}
					List<ExcelComboDataVO> listaDataCombo = (List<ExcelComboDataVO>) propiedadesMap.get("comboData");
					if (listaDataCombo == null) {
						listaDataCombo = new ArrayList<ExcelComboDataVO>();
					}
					for (ExcelComboDataVO excelComboDataVO : listaDataCombo) {
						String nombreColumna = excelComboDataVO.getNombreCampo();
						XSSFName namedCell = (XSSFName) workbook.createName();
						namedCell.setNameName("hidden" + hoja);
						namedCell.setRefersToFormula("hidden" + hoja + "!$A$1:$A$" + excelComboDataVO.getListaExcelComboData().size());

						DataValidationHelper dvHelper = sheet.getDataValidationHelper();
						DataValidationConstraint dataValidation = dvHelper.createFormulaListConstraint("hidden" + hoja);
						CellRangeAddressList addressList = new CellRangeAddressList(posicionRow, cantidadRegistros, campoPosicionMap.get(nombreColumna), campoPosicionMap.get(nombreColumna));
						DataValidation validation = dvHelper.createValidation(dataValidation,addressList );
						validation.setSuppressDropDownArrow(true);
						validation.setEmptyCellAllowed(true);
						validation.setShowPromptBox(true);
						validation.createErrorBox("Mensaje", "Elemento no válido");
						sheet.addValidationData(validation);
						hoja++;
					}
					propiedadesMap.remove("comboData");// limpiando data
				}
				// fin agregar combo
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
				for (int x = 0; x < listaHeaderData.size(); x++) {
					if (calcularWitchDemanda) {
						try {
							int  width = columnWidtMaxMap.get(autoSizeColunm);
							 width *= 256;
					         int maxColumnWidth = 255 * 256; // The maximum column width for an individual cell is 255 characters
					         if (width > maxColumnWidth) {
					             width = maxColumnWidth;
					         }
							sheet.setColumnWidth(autoSizeColunm, width);
						} catch (Exception e) {
							log.error("ERROR autoSizeColunm -->" + autoSizeColunm,e);
						}
					} else {
						sheet.autoSizeColumn(autoSizeColunm);
					}
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
				if(i!=0){
					CellRangeAddress  border = new CellRangeAddress(primeraFila,i + primeraFila-1,1,columnIndex-1);
//					generarMergeRegionBorder(border,sheet);
				}
			}
			boolean anexarHojaProcesar = false;
			 if (isCombo) {
				 if (anexarHojaExistente) {
					 anexarHojaProcesar = true;
				 }
				 hojaActiva++;
			 } else {
				 if (anexarHojaExistente) {
					 anexarHojaProcesar = true;
				 }
			 }
			 if (anexarHojaProcesar) {
				 String nombreArchivo = (String) propiedadesMap.get("nombreArchivo");
				 int anexarHojaPosition = (Integer) propiedadesMap.get("anexarHojaPosition");
				 File rutaArchivo = new File(ConstanteConfigUtil.RUTA_GENERAL_TEMPLANTE + nombreArchivo);
//				 XSSFWorkbook HSSFWorkbookAnexar =  ExcelUtil.leerExcelXlsx(rutaArchivo );
//				 XSSFSheet sheetAnexar = HSSFWorkbookAnexar.getSheetAt(anexarHojaPosition - 1);
//				 if (sheetAnexar != null) {
//					 SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(sheetAnexar.getSheetName());
//					 sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
//					 TransferUtilExcel.copySheetsXLSX(sheet, sheetAnexar);
//				 }

				 SXSSFWorkbook sXSSFWorkbookAnexar =  ExcelUtil.leerExcelsXlsx(rutaArchivo );
				 SXSSFSheet sheetAnexar = (SXSSFSheet)sXSSFWorkbookAnexar.getSheetAt(anexarHojaPosition - 1);
				 if (sheetAnexar != null) {
					 SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(sheetAnexar.getSheetName());
					 sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
					 TransferUtilExcel.copySheetsXLSX(sheet, sheetAnexar);
				 }

			 }
			//workbook.setActiveSheet(hojaActiva - 1);
			if (isFormula) {
				try {//SXSSFWorkbook
					XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook.getXSSFWorkbook());
					//workbook.setForceFormulaRecalculation(true);
				} catch (Exception e) {
					log.error("Error ", e);
				}
			} //TODO
			log.error(" generarExcelXLSXPerWithGroup generando "+archivoName);
			FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
			workbook.write(out);
			workbook.dispose();
			out.close();
			cellDateStyle = null;
			workbook = null;
			out = null;
			ExcelUtil.defaultLocaleProcess();
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage(), ex);
			resultado = null;
		}
		return resultado;
	}

	 private static void generarMergeRegionBorderPie(CellRangeAddress range, Sheet sheet) {
			//RegionUtil.setBorderBottom(BorderStyle.THIN, range, sheet);
			//RegionUtil.setBorderTop(BorderStyle.THIN, range, sheet);
			//RegionUtil.setBorderLeft(BorderStyle.THIN, range, sheet);
			//RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
			//RegionUtil.setBorderRight(BorderStyle.THIN, range, sheet);
		}
	 private static void generarMergeRegionBorder(CellRangeAddress range, Sheet sheet) {
			Workbook wb = sheet.getWorkbook();
			RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, range, sheet, wb);
			RegionUtil.setBorderTop(CellStyle.BORDER_THIN, range, sheet, wb);
			RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, range, sheet, wb);
			RegionUtil.setBorderRight(CellStyle.BORDER_THIN, range, sheet, wb);
		}

	public static byte[] generarExcelXLSX(List<ExcelHederDataVO> listaHeaderData, BigMemoryManager<String,?> listaData,String archivoName,String titulo,Map<String,Object> propiedadesMap) {
		byte[] resultado = null;
		try {
			//Inicio Agregar coombo
			int hojaActiva = 0;
			Map<String,Integer> campoPosicionMap = new HashMap<String, Integer>();
			boolean isCombo = propiedadesMap.containsKey("comboData");
			boolean anexarHojaExistente = propiedadesMap.containsKey("anexarHojaExistente");
			int posicionCellCabecera = 0;
			if (isCombo) {
				 for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						String nombreColumna = cellHeaderVO.getNameAtribute();
						if (!campoPosicionMap.containsKey(nombreColumna)) {
							campoPosicionMap.put(nombreColumna, posicionCellCabecera);
						}
						posicionCellCabecera++;
				 }
			}
			//Fin Agregar coombo
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER) ;
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			SXSSFWorkbook workbook = new SXSSFWorkbook(100);
			workbook.setCompressTempFiles(true); // temp files will be gzipped
			if (isCombo) {
				generarComboHojaXLSX(workbook, propiedadesMap);
			}
			int cantidadData = listaData.getListaKey().size();
			int cantidadHojas = 1;
			int contador = 0;
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX ) {
				BigDecimal  bCantidadData = new BigDecimal(cantidadData);
				BigDecimal  maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal  bCantidadHojas = bCantidadData.divide(maxRange,2,BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue(); 
			}
			DataFormat format = workbook.createDataFormat();
			CellStyle cellDateStyle = generarStyleDate(workbook);
            //indicando un patron de formato
			CellStyle titleStyle = generarStyleTitle(workbook);
			// titleStyle.setLocked(false);		
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap != null && propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName") + "";
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal);
				sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
				hojaActiva++;
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap != null &&  propiedadesMap.containsKey("printTitleView")) {
					SXSSFRow filaTitle = (SXSSFRow)sheet.createRow(posicionRow);
					SXSSFCell heraderTitleCell = (SXSSFCell)filaTitle.createCell((listaHeaderData.size() / 2));
					heraderTitleCell.setCellValue(titulo);
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap != null && propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio") + "") - 1;
				}
				SXSSFRow fila = (SXSSFRow)sheet.createRow(posicionRow);
				posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					String cellHeader = cellHeaderVO.getNameHeader();
					SXSSFCell heraderCell = (SXSSFCell)fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
				}
				posicionRow = posicionRow + incrementroRow;
				//llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
		        int toIndex = toIndexXlsx(cantidadData,cantidadDataPaginadorHoja);
		       for (String cellDataKey : listaData.getListaKey().subList(fromIndex, toIndex)) {
		    	   Object cellData = listaData.get(cellDataKey);
		    	    SXSSFRow filaDet = (SXSSFRow)sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						  String nombreColumna = cellHeaderVO.getNameAtribute();
							Object value = null;
							if (!nombreColumna.equals(ROW_INFO_INDEX)) {
								value = atributoValueComplejo(cellData,nombreColumna);
							} else {
								value = (contador + 1);
							}
							if (esFecha(nombreColumna)) {
								Object valueDate = verificarFornatoFecha(nombreColumna, value);
								if (esFechaData(valueDate)) {
									if (propiedadesMap.containsKey("converFechaShort") ) {
										valueDate = FechaUtil.obtenerFechaShort((Date)valueDate);
									}
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									cellDetalle.setCellValue((Date)valueDate);
									if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Format")) {
										CellStyle cellDateStyleFormat = generarStyleDate(workbook);
										cellDateStyleFormat.setDataFormat(format.getFormat(propiedadesMap.get(nombreColumna + "Format") + ""));
										cellDetalle.setCellStyle(cellDateStyleFormat);
									} else {
										cellDetalle.setCellStyle(cellDateStyle);
									}
								
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								} else {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									cellDetalle.setCellValue(value == null ? "" : value.toString());
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								}
							} else {
								if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Numeric")) {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (!StringUtil.isNullOrEmptyNumeric(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								} else {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (esNumericoData(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								}
								
							}
					}
					i++;
					contador++;
				}
		       //Inicio agregar combo
		        if (isCombo) {
		    	   int hoja = 0;
		    	   int cantidadRegistros = 100;
		    	   if (listaData != null && listaData.getListaKey().size() > 0) {
		    		   cantidadRegistros = listaData.getListaKey().size();
		    	   }
		    	   List<ExcelComboDataVO> listaDataCombo = (List<ExcelComboDataVO>)propiedadesMap.get("comboData");
		    	   if (listaDataCombo == null) {
		    		   listaDataCombo= new ArrayList<ExcelComboDataVO>(); 
		    	   }
		    	   for (ExcelComboDataVO excelComboDataVO : listaDataCombo) {
		    		   hojaActiva++;
						String nombreColumna = excelComboDataVO.getNombreCampo();
						XSSFName namedCell = (XSSFName) workbook.createName();
						namedCell.setNameName("hidden" + hoja);
						namedCell.setRefersToFormula("hidden" + hoja + "!$A$1:$A$" + excelComboDataVO.getListaExcelComboData().size());
						
						DVConstraint constraint = DVConstraint.createFormulaListConstraint("hidden" + hoja);
						CellRangeAddressList addressList = new CellRangeAddressList(posicionRow,cantidadRegistros , campoPosicionMap.get(nombreColumna),campoPosicionMap.get(nombreColumna));
						HSSFDataValidation validation = new HSSFDataValidation(addressList, constraint);
						
						validation.setSuppressDropDownArrow(false);	
						validation.setEmptyCellAllowed(false);
						validation.setShowPromptBox(false);
						validation.createErrorBox("Mensaje","Elemento no válido");
						
						sheet.addValidationData(validation);
						hoja++;
		    	   }
		    	   propiedadesMap.remove("comboData");//limpiando data
		       }
		        //fin agregar combo
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
	
				for (int ih = 0;ih < listaHeaderData.size(); ih++) {
					sheet.autoSizeColumn(autoSizeColunm,true);
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
				
			}
			boolean anexarHojaProcesar = false;
			 if (isCombo) {
				 if (anexarHojaExistente) {
					 anexarHojaProcesar = true;
				 } else {
					 SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Instruccion");
					 sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
					 SXSSFRow row = (SXSSFRow) sheet.createRow(0);
					 SXSSFCell cell = (SXSSFCell)row.createCell(0);
					 cell.setCellStyle(titleStyle);
					 cell.setCellValue("Debe Seleccionar lista Existente");
					 sheet.autoSizeColumn(0, true);
				 }
				 hojaActiva++;
			 } else {
				 if (anexarHojaExistente) {
					 anexarHojaProcesar = true;
				 }
			 }
			 if (anexarHojaProcesar) {
				 String nombreArchivo = (String) propiedadesMap.get("nombreArchivo");
				 int anexarHojaPosition = (Integer) propiedadesMap.get("anexarHojaPosition");
				 File rutaArchivo = new File(ConstanteConfigUtil.RUTA_GENERAL_TEMPLANTE + nombreArchivo);
				 SXSSFWorkbook sXSSFWorkbookAnexar =  ExcelUtil.leerExcelsXlsx(rutaArchivo );
				 SXSSFSheet sheetAnexar = (SXSSFSheet)sXSSFWorkbookAnexar.getSheetAt(anexarHojaPosition - 1);
				 if (sheetAnexar != null) {
					 SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(sheetAnexar.getSheetName());
					 sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
					 TransferUtilExcel.copySheetsXLSX(sheet, sheetAnexar);
				 }
			 }
			workbook.setActiveSheet(hojaActiva - 1);
			FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
			workbook.write(out);
			workbook.dispose();
			out.close();
			cellDateStyle = null;
			workbook = null;
			out = null;
			if (!propiedadesMap.containsKey("notClean")) {
				listaData.clean();	
			}			
			 ExcelUtil.defaultLocaleProcess();
		} catch (Exception ex) {
			 log.error(ex.getMessage(), ex);
			resultado = null;
		}
		return resultado;
	}

	private static CellStyle generarStyleTitlePie(CellStyle titleStyle,Font titleFont) {
		titleStyle.setFont(titleFont);
		/*titleStyle.setBorderTop(BorderStyle.THIN);
		titleStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		titleStyle.setBorderRight(BorderStyle.THIN);
		titleStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		titleStyle.setBorderBottom(BorderStyle.THIN);
		titleStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		titleStyle.setBorderLeft(BorderStyle.THIN);
		titleStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());*/
		return titleStyle;
	}	
	
	private static CellStyle generarStyleTitlePie(SXSSFWorkbook workbook, short fontHeightInPoints) {
		Font titleFont = generarTitleFontPie(workbook,fontHeightInPoints);
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle = generarStyleTitlePie(titleStyle,titleFont);
		return titleStyle;
	}
	
	private static Font generarTitleFontPie(SXSSFWorkbook workbook, short fontHeightInPoints) {
		Font titleFont = workbook.createFont();
		titleFont = generarTitleFontPie(titleFont,fontHeightInPoints);
		return titleFont;
	}	

	private static Font generarTitleFontPie(Font titleFont,short fontHeightInPoints) {
		titleFont.setFontName(NOMBRE_LETRA);
		titleFont.setFontHeightInPoints((short) fontHeightInPoints);
		titleFont.setBold(true);
		//titleFont.setColor(HSSFColorPredefined.BLACK.getIndex());
		return titleFont;
	}
	
	private static CellStyle generarStyleDate(HSSFWorkbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle = generarStyleDate(cellDateStyle, format);
		return cellDateStyle;
	}
	private static CellStyle generarStyleDateBorder(SXSSFWorkbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle = generarStyleDate(cellDateStyle, format);
		cellDateStyle.setBorderTop(CellStyle.BORDER_THIN);
		cellDateStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cellDateStyle.setBorderRight(CellStyle.BORDER_THIN);
		cellDateStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cellDateStyle.setBorderBottom(CellStyle.BORDER_THIN);
		cellDateStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cellDateStyle.setBorderLeft(CellStyle.BORDER_THIN);
		cellDateStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		return cellDateStyle;
	}
	private static CellStyle generarStyleDate(SXSSFWorkbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle = generarStyleDate(cellDateStyle, format);
		return cellDateStyle;
	}
	
	private static CellStyle generarStyleTitle(HSSFWorkbook workbook) {
		Font titleFont = generarTitleFont(workbook);
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle = generarStyleTitle(titleStyle,titleFont,true);
		return titleStyle;
	}
	
	private static CellStyle generarStyleTitle2(SXSSFWorkbook workbook, short fontHeightInPoints) {
		Font titleFont = generarTitleFont(workbook,fontHeightInPoints,true);
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle = generarStyleTitle(titleStyle,titleFont,true);
		return titleStyle;
	}
	
	
	private static CellStyle generarStyleTitle(SXSSFWorkbook workbook) {
		Font titleFont = generarTitleFont(workbook);
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle = generarStyleTitle(titleStyle,titleFont,true);
		return titleStyle;
	}
	private static CellStyle generarStyleTitle(CellStyle titleStyle,Font titleFont, boolean styleMarco) {
		titleStyle.setFont(titleFont);
		if (styleMarco) {
			titleStyle.setBorderTop(CellStyle.BORDER_THIN);
			titleStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
			titleStyle.setBorderRight(CellStyle.BORDER_THIN);
			titleStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
			titleStyle.setBorderBottom(CellStyle.BORDER_THIN);
			titleStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
			titleStyle.setBorderLeft(CellStyle.BORDER_THIN);
			titleStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		}
		return titleStyle;
	}
	
	private static Font generarTitleFont(HSSFWorkbook workbook) {
		Font titleFont = workbook.createFont();
		titleFont = generarTitleFont(titleFont);
		return titleFont;
	}
	private static Font generarTitleFont(Font titleFont) {
		titleFont.setFontName(NOMBRE_LETRA);
		titleFont.setFontHeightInPoints((short) 9);
		titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
		titleFont.setColor(HSSFColor.BLACK.index);
		return titleFont;
	}
	private static Font generarTitleFont(SXSSFWorkbook workbook) {
		Font titleFont = workbook.createFont();
		titleFont = generarTitleFont(titleFont);
		return titleFont;
	}
	
	private static HSSFWorkbook generarComboHoja(HSSFWorkbook workbook , Map<String,Object> propiedadesMap) {
		List<ExcelComboDataVO> listaData = (List<ExcelComboDataVO>)propiedadesMap.get("comboData");
		int hoja = 0;
		for (ExcelComboDataVO excelComboDataVO : listaData) {
			HSSFSheet hidden = workbook.createSheet("hidden" + hoja);
			int i  = 0;
			for (String dataCombo : excelComboDataVO.getListaExcelComboData()) {
				Row row = hidden.createRow(i);
				Cell cell = row.createCell(0);
			    cell.setCellValue(dataCombo);
			    i++;
			 }
			for (int ih = 0;ih < listaData.size(); ih++) {
				hidden.autoSizeColumn(ih,true);
			}
			workbook.setSheetHidden(hoja, Workbook.SHEET_STATE_VERY_HIDDEN);
			 hoja++;
		}
		return workbook;
	} 
	private static SXSSFWorkbook generarComboHojaXLSX(SXSSFWorkbook workbook , Map<String,Object> propiedadesMap) {
		List<ExcelComboDataVO> listaData = (List<ExcelComboDataVO>)propiedadesMap.get("comboData");
		int hoja = 0;
		for (ExcelComboDataVO excelComboDataVO : listaData) {
			 SXSSFSheet hidden = (SXSSFSheet) workbook.createSheet("hidden" + hoja);
			 hidden.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
			int i  = 0;
			for (String dataCombo : excelComboDataVO.getListaExcelComboData()) {
				Row row = hidden.createRow(i);
				Cell cell = row.createCell(0);
			    cell.setCellValue(dataCombo);
			    i++;
			 }
			for (int ih = 0;ih < listaData.size(); ih++) {
				hidden.autoSizeColumn(ih,true);
			}
			workbook.setSheetHidden(hoja, Workbook.SHEET_STATE_VERY_HIDDEN);
			 hoja++;
		}
		return workbook;
	} 
	/**
	 * Generar excel object.
	 *
	 * @param listaHeader el lista header
	 * @param listaData el lista data
	 * @param archivoName el archivo name
	 * @param titulo el titulo
	 * @param propiedadesMap el propiedades map
	 * @return the byte[]
	 */
	public static byte[] generarExcelObject(List<String> listaHeader, List<Object[]> listaData,String archivoName,String titulo,Map<String,String> propiedadesMap) {
		byte[] resultado = null;
		try {
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER) ;
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			HSSFWorkbook workbook = new HSSFWorkbook();
			int cantidadData = listaData.size();
			int cantidadHojas = 1;
			if (cantidadData > MAXIMO_RANGE_EXCEL ) {
				BigDecimal  bCantidadData = new BigDecimal(cantidadData);
				BigDecimal  maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL);
				BigDecimal  bCantidadHojas = bCantidadData.divide(maxRange,2,BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue(); 
			}
			CellStyle cellDateStyle = generarStyleDate(workbook);
			CellStyle titleStyle = generarStyleTitle(workbook);
			// titleStyle.setLocked(false);			
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName");
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				HSSFSheet sheet = workbook.createSheet(tituloFinal);
				
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap.containsKey("printTitleView")) {
					Row filaTitle = sheet.createRow(posicionRow);
					Cell heraderTitleCell = filaTitle.createCell((listaHeader.size() / 2));
					heraderTitleCell.setCellValue(titulo);
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio")) - 1;
				}
				Row fila = sheet.createRow(posicionRow);
				int posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				for (String cellHeader : listaHeader) {
					Cell heraderCell = fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
				}
				posicionRow = posicionRow + incrementroRow;
				//llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndex(cantidadDataPaginadorHoja);
		        int toIndex = toIndex(cantidadData,cantidadDataPaginadorHoja);
		       for (Object[] cellData : listaData.subList(fromIndex, toIndex)) {
					Row filaDet = sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					
					for (int k = 0; k < listaHeader.size(); k++) {
						Object value = null;
						value = cellData[k];
						if (esFechaData(value)) {
							Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue((Date)value);
							cellDetalle.setCellStyle(cellDateStyle);
							posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
						} else {
							Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue(value == null ? "" : value.toString());
							posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
						}	
							
					}
					i++;
				}
	
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
	
				for (int ih = 0;ih < listaHeader.size(); ih++) {
					sheet.autoSizeColumn(autoSizeColunm);
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
			}
			
			 FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xls");
			 workbook.write(out);
			 workbook.close();
			 out.close();
			 cellDateStyle = null;
			 workbook = null;
			 out = null;
			 ExcelUtil.defaultLocaleProcess();
			//Runtime garbage = Runtime.getRuntime();
	   	 	//garbage.gc();
		} catch (Exception ex) {
			 log.error(ex.getMessage(), ex);
			resultado = null;
		}
		return resultado;
	}
	
	/**
	 * Generar excel con cabecera ovveridee.
	 * 
	 * @param listaHeader lista de cabeceras.
	 * @param listaHeaderOverrideMap lista de cabeceras que se requieren sobreescribir.
	 * @param listaDataMap lista de data
	 * @param archivoName 
	 * @param titulo
	 * @param propiedadesMap
	 * @return bytes
	 */
	public static byte[] generarExcelObjectMap(List<String> listaHeader, Map<String,String> listaHeaderOverrideMap, List<Map<String,ValueDataVO>> listaDataMap,String archivoName,String titulo,Map<String,String> propiedadesMap) {
		byte[] resultado = null;
		try {
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER) ;
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			//ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			HSSFWorkbook workbook = new HSSFWorkbook();
			int cantidadData = listaDataMap.size();
			int cantidadHojas = 1;
			if (cantidadData > MAXIMO_RANGE_EXCEL ) {
				BigDecimal  bCantidadData = new BigDecimal(cantidadData);
				BigDecimal  maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL);
				BigDecimal  bCantidadHojas = bCantidadData.divide(maxRange,2,BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue(); 
			}
			
			CellStyle cellDateStyle = generarStyleDate(workbook);
			CellStyle titleStyle = generarStyleTitle(workbook);
			
			// titleStyle.setLocked(false);		
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName");
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				HSSFSheet sheet = workbook.createSheet(tituloFinal);
	
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap.containsKey("printTitleView")) {
					Row filaTitle = sheet.createRow(posicionRow);
					Cell heraderTitleCell = filaTitle.createCell((listaHeader.size() / 2));
					heraderTitleCell.setCellValue(titulo);
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio")) - 1;
				}
				Row fila = sheet.createRow(posicionRow);
				sheet.createFreezePane(0, posicionRow + 1 );
				int posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				for (String cellHeaderKey : listaHeader) {
					String cellHeader  = cellHeaderKey;
					if (listaHeaderOverrideMap.containsKey(cellHeaderKey)) {
						cellHeader = listaHeaderOverrideMap.get(cellHeaderKey);
					}
					Cell heraderCell = fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
				}
				posicionRow = posicionRow + incrementroRow;
				//llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndex(cantidadDataPaginadorHoja);
		        int toIndex = toIndex(cantidadData,cantidadDataPaginadorHoja);
		       for (Map<String,ValueDataVO> dataMap : listaDataMap.subList(fromIndex, toIndex)) {
					Row filaDet = sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					
					for (String headerKey : listaHeader) {
						Object value = null;
						if (propiedadesMap.containsKey(headerKey)) {
							value = dataMap.get(headerKey);
							if (StringUtil.isNullOrEmpty(value)) {
								value = 0;
							}
						} else {
							value = dataMap.get(headerKey);
						}  
						
						if (esFechaData(value)) {
							Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue((Date)value);
							cellDetalle.setCellStyle(cellDateStyle);
							posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
						} else {
							if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Numeric")) {
								Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
								if (!StringUtil.isNullOrEmptyNumeric(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(value == null ? "" : value.toString());
								}
								posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
							} else {
								Cell cellDetalle = (Cell)filaDet.createCell(posicionCellCabecera);
								if (esNumericoData(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(value == null ? "" : value.toString());
								}
								posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
							}
						}	
							
					}
					i++;
				}
	
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
	
				for (int ih = 0;ih < listaHeader.size(); ih++) {
					sheet.autoSizeColumn(autoSizeColunm);
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
			}
			//workbook.write(outputStream);
			//resultado = outputStream.toByteArray();
			 
			FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xls");
			 workbook.write(out);
			 workbook.close();
			 out.close();
			 cellDateStyle = null;
			 workbook = null;
			 out = null;
			 ExcelUtil.defaultLocaleProcess();
		} catch (Exception ex) {
			 log.error(ex.getMessage(), ex);
			resultado = null;
		}
		return resultado;
	}
	public static void  generarExcelObjectXLSXMap(List<ExcelHederDataVO> listaHeader, List<Map<String,Object>> listaDataMap,String archivoName,String titulo,Map<String,String> propiedadesMap) {
		try {
//			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER) ;
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
	        SXSSFWorkbook workbook = new SXSSFWorkbook(100);
			 workbook.setCompressTempFiles(true); // temp files will be gzipped
			int cantidadData = listaDataMap.size();
			int cantidadHojas = 1;
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX ) {
				BigDecimal  bCantidadData = new BigDecimal(cantidadData);
				BigDecimal  maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal  bCantidadHojas = bCantidadData.divide(maxRange,2,BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue(); 
			}
			CellStyle cellDateStyle = generarStyleDate(workbook);
			CellStyle titleStyle = generarStyleTitle(workbook);
			// titleStyle.setLocked(false);			
			log.info("generarExcelObjectMapBigMemory.cantidadHojas --> " + cantidadHojas);
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName");
				}
				log.info("generarExcelObjectMapBigMemory.cantidadDataPaginadorHoja --> " + cantidadDataPaginadorHoja);
				log.info("generarExcelObjectMapBigMemory.tituloFinal antes --> " + tituloFinal);
				if (cantidadDataPaginadorHoja > 1) {
					tituloFinal = cantidadDataPaginadorHoja + tituloFinal ;
				}
				log.info("generarExcelObjectMapBigMemory.tituloFinal despues --> " + tituloFinal);
				//Sheet  sheet = workbook.createSheet(tituloFinal);
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal);
				sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap.containsKey("printTitleView")) {
					SXSSFRow filaTitle = (SXSSFRow)sheet.createRow(posicionRow);
					SXSSFCell heraderTitleCell = (SXSSFCell)filaTitle.createCell(0);
					heraderTitleCell.setCellValue(titulo);
					heraderTitleCell.setCellStyle(titleStyle);
					//heraderTitleCell.getCellStyle().setAlignment(CellStyle.ALIGN_CENTER);
					if (listaHeader.size() > 1) {
						sheet.addMergedRegion(new CellRangeAddress(posicionRow, posicionRow, 0,listaHeader.size() - 1));
					}
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio")) - 1;
				}
				SXSSFRow fila = (SXSSFRow)sheet.createRow(posicionRow);
				int posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				for (ExcelHederDataVO cellHeaderKey : listaHeader) {
					String cellHeader = cellHeaderKey.getNameHeader();
					SXSSFCell heraderCell = (SXSSFCell)fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
				}
				posicionRow = posicionRow + incrementroRow;
				//llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
		        int toIndex = toIndexXlsx(cantidadData,cantidadDataPaginadorHoja);
		       for (Map<String,Object> dataMap : listaDataMap.subList(fromIndex, toIndex)) {
		    	    SXSSFRow filaDet = (SXSSFRow)sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					
					for (ExcelHederDataVO headerKey : listaHeader) {
						Object value = null;
						if (propiedadesMap.containsKey(headerKey.getNameAtribute())) {
							value = dataMap.get(headerKey);
							if (StringUtil.isNullOrEmpty(value)) {
								value = 0;
							}
						} else {
							value = dataMap.get(headerKey.getNameAtribute());
						}  
						
						if (esFechaData(value)) {
							SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue((Date)value);
							cellDetalle.setCellStyle(cellDateStyle);
							posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
						} else {
							if (propiedadesMap != null && propiedadesMap.containsKey(headerKey.getNameAtribute() + "Numeric")) {
								SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
								if (!StringUtil.isNullOrEmptyNumeric(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(value == null ? "" : value.toString());
								}
								posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
							} else {
								SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
								if (esNumericoData(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(value == null ? "" : value.toString());
								}
								posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
							}
						}	
							
					}
					i++;
					dataMap = null;
				}
	
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
	
				for (int ih = 0;ih < listaHeader.size(); ih++) {
					sheet.autoSizeColumn(autoSizeColunm);
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
			}
			listaDataMap = null;
			 FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
			 workbook.write(out);
			 workbook.dispose();
			 out.close();
			 cellDateStyle = null;
			 workbook = null;
			 out = null;
			 ExcelUtil.defaultLocaleProcess();
		} catch (Exception ex) {
			 log.error(ex.getMessage(), ex);
		}
	}
	public static void  generarExcelObjectMapBigMemory(List<String> listaHeader, Map<String,String> listaHeaderOverrideMap, BigMemoryManager<String,Map<String,Object>> listaDataMap,String archivoName,String titulo,Map<String,String> propiedadesMap) {
		try {
//			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER) ;
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
	        SXSSFWorkbook workbook = new SXSSFWorkbook(100);
			 workbook.setCompressTempFiles(true); // temp files will be gzipped
			int cantidadData = listaDataMap.getListaKey().size();
			int cantidadHojas = 1;
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX ) {
				BigDecimal  bCantidadData = new BigDecimal(cantidadData);
				BigDecimal  maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal  bCantidadHojas = bCantidadData.divide(maxRange,2,BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue(); 
			}
			CellStyle cellDateStyle = generarStyleDate(workbook);
			CellStyle titleStyle = generarStyleTitle(workbook);
			// titleStyle.setLocked(false);			
			log.info("generarExcelObjectMapBigMemory.cantidadHojas --> " + cantidadHojas);
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName");
				}
				log.info("generarExcelObjectMapBigMemory.cantidadDataPaginadorHoja --> " + cantidadDataPaginadorHoja);
				log.info("generarExcelObjectMapBigMemory.tituloFinal antes --> " + tituloFinal);
				if (cantidadDataPaginadorHoja > 1) {
					tituloFinal = cantidadDataPaginadorHoja + tituloFinal ;
				}
				log.info("generarExcelObjectMapBigMemory.tituloFinal despues --> " + tituloFinal);
				//Sheet  sheet = workbook.createSheet(tituloFinal);
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal);
				sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap.containsKey("printTitleView")) {
					Row filaTitle = sheet.createRow(posicionRow);
					Cell heraderTitleCell = filaTitle.createCell((listaHeader.size() / 2));
					heraderTitleCell.setCellValue(titulo);
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio")) - 1;
				}
				Row fila = sheet.createRow(posicionRow);
				int posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				for (String cellHeaderKey : listaHeader) {
					String cellHeader = listaHeaderOverrideMap.get(cellHeaderKey);
					Cell heraderCell = fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
				}
				posicionRow = posicionRow + incrementroRow;
				//llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
		        int toIndex = toIndexXlsx(cantidadData,cantidadDataPaginadorHoja);
		       for (String dataMapKey : listaDataMap.getListaKey().subList(fromIndex, toIndex)) {
		    	   Map<String,Object> dataMap = listaDataMap.get(dataMapKey);
					Row filaDet = sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					
					for (String headerKey : listaHeader) {
						Object value = null;
						if (propiedadesMap.containsKey(headerKey)) {
							value = dataMap.get(headerKey);
							if (StringUtil.isNullOrEmpty(value)) {
								value = 0;
							}
						} else {
							value = dataMap.get(headerKey);
						}  
						
						if (esFechaData(value)) {
							Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue((Date)value);
							cellDetalle.setCellStyle(cellDateStyle);
							posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
						} else {
							Cell cellDetalle = filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue(value == null ? "" : value.toString());
							posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
						}	
							
					}
					i++;
					dataMap = null;
					listaDataMap.removeKey(dataMapKey);
				}
	
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
	
				for (int ih = 0;ih < listaHeader.size(); ih++) {
					sheet.autoSizeColumn(autoSizeColunm);
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
			}
			listaDataMap.setListaKey(null);
			listaDataMap.clean();
//			workbook.write(outputStream);
//			resultado.put(archivoName, outputStream.toByteArray());
//			outputStream = null;
			 FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
			 workbook.write(out);
			 workbook.dispose();
			 out.close();
			 cellDateStyle = null;
			 workbook = null;
			 out = null;
			 ExcelUtil.defaultLocaleProcess();
		} catch (Exception ex) {
			 log.error(ex.getMessage(), ex);
		}
	}
	/**
	 * From index.
	 *
	 * @param dataPaginator el data paginator
	 * @return the int
	 */
    private static int fromIndex(Integer dataPaginator) {
 	   int pagina = 0; 
        if (dataPaginator == null) {
            pagina = 1;
        } else {
            pagina = dataPaginator;                
        }    
 	   int fromIndex = ((pagina - 1) * (MAXIMO_RANGE_EXCEL - CANTIDAD_FILAS_USADO_CABECERA));//
 	   return fromIndex;
    }
    private static int fromIndexXlsx(Integer dataPaginator) {
  	   int pagina = 0; 
         if (dataPaginator == null) {
             pagina = 1;
         } else {
             pagina = dataPaginator;                
         }    
  	   int fromIndex = ((pagina - 1) * (MAXIMO_RANGE_EXCEL_XLSX - CANTIDAD_FILAS_USADO_CABECERA));//
  	   return fromIndex;
     }
    
    /**
     * To index.
     *
     * @param cantidadTotalData el cantidad total data
     * @param dataPaginator el data paginator
     * @return the int
     */
    private static int toIndex(int cantidadTotalData,Integer dataPaginator) {
 	   int pagina = 0; 
        if (dataPaginator == null) {
            pagina = 1;
        } else {
            pagina = dataPaginator;                
        }  
 	   int toIndex = ((pagina - 1) * (MAXIMO_RANGE_EXCEL - CANTIDAD_FILAS_USADO_CABECERA)) + (MAXIMO_RANGE_EXCEL - CANTIDAD_FILAS_USADO_CABECERA);
        if (toIndex > cantidadTotalData) {
            toIndex = cantidadTotalData;
        }
        return toIndex;
    }
    private static int toIndexXlsx(int cantidadTotalData,Integer dataPaginator) {
  	   int pagina = 0; 
         if (dataPaginator == null) {
             pagina = 1;
         } else {
             pagina = dataPaginator;                
         }  
  	   int toIndex = ((pagina - 1) * (MAXIMO_RANGE_EXCEL_XLSX - CANTIDAD_FILAS_USADO_CABECERA)) + (MAXIMO_RANGE_EXCEL_XLSX - CANTIDAD_FILAS_USADO_CABECERA);
         if (toIndex > cantidadTotalData) {
             toIndex = cantidadTotalData;
         }
         return toIndex;
     }
	/**
	 * Es fecha.
	 *
	 * @param columnaName el columna name
	 * @return true, en caso de exito
	 */
	private static boolean esFecha(String columnaName) {
		boolean resultado = false;
		if (columnaName.toUpperCase().contains("fecha".toUpperCase())) {
			resultado = true;
		}
		return resultado;
	}
	
	/**
	 * Es fecha data.
	 *
	 * @param valueDate el value date
	 * @return true, en caso de exito
	 */
	private static boolean esFechaData(Object valueDate) {
		boolean resultado = false;
		if (valueDate != null && (valueDate.getClass().isAssignableFrom(Date.class) || valueDate.getClass().isAssignableFrom(java.sql.Timestamp.class) )) {
			resultado = true;
		}
		return resultado;
	}
	/**
	 * Verificar fornato fecha.
	 *
	 * @param columnaName el columna name
	 * @param value el value
	 * @return the object
	 */
	private static Object verificarFornatoFecha(String columnaName,Object value) {
		Object resultado = value;
		if (esFecha(columnaName)) {
			try {
				Date date = FechaUtil.obtenerFechaFormatoCompleto(value.toString());
				resultado = date;
			} catch (Exception e) {
				 resultado = value;
			}
		} else {
			resultado = value;
		}
		return resultado;
	}
	/**
	 * Atributo value complejo.
	 *
	 * @param object el object
	 * @param nombreColumna el nombre columna
	 * @return the object
	 */
	private  static Object atributoValueComplejo(Object object, String nombreColumna) {
		Object resultado = null;
		String nombreColumnaReplace = nombreColumna.replace(".", ":");
	 	String[] objeto = nombreColumnaReplace.split(":");
	 	int cantidadPropiedad = objeto.length;
		if (cantidadPropiedad == 1) {				
			resultado = getValue(object, nombreColumna);
		}
		if (cantidadPropiedad > 1) {
				String propertyName = objeto[cantidadPropiedad - 1];
				Object object2 = object;
				for (String string : objeto) {
					if (!string.equals(propertyName)) {
						object2 = getValue(object2, string);
					}
				}
				resultado = atributoValueComplejo(object2, propertyName);
		}
		
		return resultado;
	}
	
	/**
	 * Obtiene value.
	 *
	 * @param object el object
	 * @param nombreColumna el nombre columna
	 * @return value
	 */
	public static Object getValue(Object object,String nombreColumna) {
		Object resultado = null;
		try {
			BeanMap beanMap = new BeanMap(object);
			resultado = beanMap.get(nombreColumna);
		} catch (Exception e) {
			resultado = null;
		}
		
		return resultado;
	}
	/**
	 * Obtener nombre.
	 *
	 * @param nombreArchivo el nombre archivo
	 * @return the string
	 */
	public static String obtenerNombrePath(String nombreArchivo) {
		SimpleDateFormat	sdf = new SimpleDateFormat("_yyyyMMdd_HHmmss_SSS");
		StringBuilder		sbNombreArchivo = new StringBuilder();
		//sbNombreArchivo.append(ConstantesUtil.DIRECTORIO_UPLOADS);
		String				extension = obtenerExtension( nombreArchivo );
		
		sbNombreArchivo.append( obtenerNombre( nombreArchivo ) );
		sbNombreArchivo.append( sdf.format(new Date()) );
		if (null != extension && extension.length() > 0 ) {
			sbNombreArchivo.append( "." + extension );
		}
		
		return 	sbNombreArchivo.toString();
	}
	  
  	/**
  	 * Obtener nombre.
  	 *
  	 * @param fileName el file name
  	 * @return the string
  	 */
  	public static  String	obtenerNombre(String fileName) {
	    	int 		pos = fileName.lastIndexOf(".");
			String 		nombre = "";
			if (pos >= 0) {
				nombre = fileName.substring(0, pos);
			} else {
				nombre = fileName;
			}
	    	return nombre;
	    }
	  
	  /**
  	 * Obtener extension.
  	 *
  	 * @param fileName el file name
  	 * @return the string
  	 */
  	public static  String obtenerExtension(String fileName) {
	    	int 	pos = fileName.lastIndexOf(".");
			String 	extension = "";
			if (pos >= 0) {
				extension = fileName.substring(pos + 1);
			}
	    	return extension;
	}
	
	/**
	 * Obtener widt.
	 *
	 * @param widtMaxActual el widt max actual
	 * @param porcentaje el porcentaje
	 * @return the integer
	 */
	private static Integer obtenerWidt(int widtMaxActual, double porcentaje) {
		return (new BigDecimal(widtMaxActual + (widtMaxActual * porcentaje)).setScale(0, RoundingMode.HALF_UP).intValue());
	}

	/**
	 * Es Numerico.
	 *
	 * @param columnaName  el nombre columna
	 * @return true, en caso de exito
	 */
	private static boolean esNumeric(String columnaName) {
		boolean resultado = false;
		String regex = "[0-9]*";
		if (columnaName.matches(regex)) {
			resultado = true;
		}	
		return resultado;
	}
	/* NFIRE 04/06/15 fin */
	
	private static boolean esNumericoData(Object value) {
		boolean	resultado =  false;
		if (value == null ) {
			return resultado;
		}
		if (value instanceof Number ) {
		 resultado =  true;
		} 
		 return resultado;
	}
	
	public static byte[] generarExcelXLSXMap(List<String> listaHeaderData, List<?> listaData, String archivoName, String titulo, Map<String, Object> propiedadesMap) {
		byte[] resultado = null;
		try {
			Map<Integer, Integer> columnWidtMaxMap = new HashMap<Integer, Integer>();
			boolean calcularWitchDemanda = propiedadesMap.containsKey("calcularWitchDemanda");
			// Inicio Agregar coombo
			int hojaActiva = 0;
			Map<String, Integer> campoPosicionMap = new HashMap<String, Integer>();
			boolean isCombo = propiedadesMap.containsKey("comboData");
			boolean anexarHojaExistente = propiedadesMap.containsKey("anexarHojaExistente");
			int posicionCellCabecera = 0;
			if (isCombo) {
				for (String nombreColumna : listaHeaderData) {
					if (!campoPosicionMap.containsKey(nombreColumna)) {
						campoPosicionMap.put(nombreColumna, posicionCellCabecera);
					}
					posicionCellCabecera++;
				}
			}
			// Fin Agregar coombo
			File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER);
			if (!archivoXLS.isFile()) {
				archivoXLS.mkdirs();
			}
			SXSSFWorkbook workbook = new SXSSFWorkbook(100);
			workbook.setCompressTempFiles(true); // temp files will be gzipped
			if (isCombo) {
				generarComboHojaXLSX(workbook, propiedadesMap);
			}
			int cantidadData = listaData.size();
			int cantidadHojas = 1;
			int contador = 0;
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue();
			}
			DataFormat format = workbook.createDataFormat();
			CellStyle cellDateStyle = generarStyleDate(workbook);
			// indicando un patron de formato
			CellStyle titleStyle = generarStyleTitle(workbook);
			// titleStyle.setLocked(false);
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap != null && propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName") + "";
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal); // CREA UNA HOJA
				sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
				hojaActiva++;
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap != null && propiedadesMap.containsKey("printTitleView")) {
					SXSSFRow filaTitle = (SXSSFRow) sheet.createRow(posicionRow);
					SXSSFCell heraderTitleCell = (SXSSFCell) filaTitle.createCell((listaHeaderData.size() / 2));
					heraderTitleCell.setCellValue(titulo);
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap != null && propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio") + "") - 1;
				}
				SXSSFRow fila = (SXSSFRow) sheet.createRow(posicionRow);
				posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				for (String cellHeader : listaHeaderData) {
					SXSSFCell heraderCell = (SXSSFCell) fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
				}
				posicionRow = posicionRow + incrementroRow;
				// llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
				int toIndex = toIndexXlsx(cantidadData, cantidadDataPaginadorHoja);
				for (Object cellData : listaData.subList(fromIndex, toIndex)) {
					SXSSFRow filaDet = (SXSSFRow) sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					int columnIndex = 0;
					for (String nombreColumna : listaHeaderData) {
						Object value = null;
						if (!nombreColumna.equals(ROW_INFO_INDEX)) {
							value = atributoValueComplejo(cellData, nombreColumna);
						} else {
							value = (contador + 1);
						}
						if (esFecha(nombreColumna)) {
							Object valueDate = verificarFornatoFecha(nombreColumna, value);
							if (esFechaData(valueDate)) {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue((Date) valueDate);
								if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Format")) {
									CellStyle cellDateStyleFormat = generarStyleDate(workbook);
									cellDateStyleFormat.setDataFormat(format.getFormat(propiedadesMap.get(nombreColumna + "Format") + ""));
									value = FechaUtil.obtenerFechaFormatoPersonalizado((Date) valueDate, propiedadesMap.get(nombreColumna + "Format") + "");
									cellDetalle.setCellStyle(cellDateStyleFormat);
								} else {
									cellDetalle.setCellStyle(cellDateStyle);
								}
								
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue(value == null ? "" : value.toString());
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							}
						} else {
							SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue(value == null ? "" : value.toString());
							posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
						}

						if (calcularWitchDemanda) {
							int widtMaxActual = ObjectUtil.objectToString(nombreColumna).length();
							double porcentaje = 0.20;
							if (!columnWidtMaxMap.containsKey(columnIndex)) {
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
							int widtMax = columnWidtMaxMap.get(columnIndex);
							widtMaxActual = ObjectUtil.objectToString(value).length();
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							if (widtMax < widtMaxActual) {
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
						}
						columnIndex++;
					}
					i++;
					contador++;
				}
				// Inicio agregar combo
				if (isCombo) {
					int hoja = 0;
					int cantidadRegistros = 100;
					if (listaData != null && listaData.size() > 0) {
						cantidadRegistros = listaData.size();
					}
					List<ExcelComboDataVO> listaDataCombo = (List<ExcelComboDataVO>) propiedadesMap.get("comboData");
					if (listaDataCombo == null) {
						listaDataCombo = new ArrayList<ExcelComboDataVO>();
					}
					for (ExcelComboDataVO excelComboDataVO : listaDataCombo) {
						hojaActiva++;
						String nombreColumna = excelComboDataVO.getNombreCampo();
						XSSFName namedCell = (XSSFName) workbook.createName();
						namedCell.setNameName("hidden" + hoja);
						namedCell.setRefersToFormula("hidden" + hoja + "!$A$1:$A$" + excelComboDataVO.getListaExcelComboData().size());

						DVConstraint constraint = DVConstraint.createFormulaListConstraint("hidden" + hoja);
						CellRangeAddressList addressList = new CellRangeAddressList(posicionRow, cantidadRegistros, campoPosicionMap.get(nombreColumna), campoPosicionMap.get(nombreColumna));
						HSSFDataValidation validation = new HSSFDataValidation(addressList, constraint);

						validation.setSuppressDropDownArrow(false);
						validation.setEmptyCellAllowed(false);
						validation.setShowPromptBox(false);
						validation.createErrorBox("Mensaje", "Elemento no válido");

						sheet.addValidationData(validation);
						hoja++;
					}
					propiedadesMap.remove("comboData");// limpiando data
				}
				// fin agregar combo
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;

				for (int ih = 0; ih < listaHeaderData.size(); ih++) {
					if (calcularWitchDemanda) {
						try {
							int width = columnWidtMaxMap.get(autoSizeColunm);
							width *= 256;
							int maxColumnWidth = 255 * 256; // The maximum column width for an individual cell is 255 characters
							if (width > maxColumnWidth) {
								width = maxColumnWidth;
							}
							sheet.setColumnWidth(autoSizeColunm, width);
						} catch (Exception e) {
							// log.error("ERROR autoSizeColunm -->" +
							// autoSizeColunm);
						}
					} else {
						sheet.autoSizeColumn(autoSizeColunm, true);
					}
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}

			}
			boolean anexarHojaProcesar = false;
			if (isCombo) {
				if (anexarHojaExistente) {
					anexarHojaProcesar = true;
				} else {
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet("Instruccion");
					sheet.setRandomAccessWindowSize(10);// keep 100 rows in memory, exceeding rows will be flushed to disk
					SXSSFRow row = (SXSSFRow) sheet.createRow(0);
					SXSSFCell cell = (SXSSFCell) row.createCell(0);
					cell.setCellStyle(titleStyle);
					cell.setCellValue("Debe Seleccionar lista Existente");
					sheet.autoSizeColumn(0, true);
				}
				hojaActiva++;
			} else {
				if (anexarHojaExistente) {
					anexarHojaProcesar = true;
				}
			}
			if (anexarHojaProcesar) {
				String nombreArchivo = (String) propiedadesMap.get("nombreArchivo");
				int anexarHojaPosition = (Integer) propiedadesMap.get("anexarHojaPosition");
				File rutaArchivo = new File(ConstanteConfigUtil.RUTA_GENERAL_TEMPLANTE + nombreArchivo);
				SXSSFWorkbook sXSSFWorkbookAnexar = ExcelUtil.leerExcelsXlsx(rutaArchivo);
				SXSSFSheet sheetAnexar = (SXSSFSheet) sXSSFWorkbookAnexar.getSheetAt(anexarHojaPosition - 1);
				if (sheetAnexar != null) {
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(sheetAnexar.getSheetName());
					sheet.setRandomAccessWindowSize(10);// keep 100 rows in memory, exceeding rows will be flushed to disk
					TransferUtilExcel.copySheetsXLSX(sheet, sheetAnexar);
				}
			}
			workbook.setActiveSheet(hojaActiva - 1);
			FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
			workbook.write(out);
			workbook.dispose();
			out.close();
			cellDateStyle = null;
			workbook = null;
			out = null;
			ExcelUtil.defaultLocaleProcess();
			//Runtime garbage = Runtime.getRuntime();
			//garbage.gc();
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			resultado = null;
		}
		return resultado;
	}
	//Inicio BUIDSOFT 02 Requerimiento reporte detalle produccion excel
	public  static void generarExcelXLSXMap(List<String> listaHeader, List<ExcelHederTitleVO> listaHeaderCabecera, List<Map<String, Object>> listaDataMap, String archivoName, String titulo, Map<String, String> propiedadesMap, SXSSFWorkbook workbook, List<ExcelHederTitleVO> paginaParametroList) {
		Map<String, Object> propiedadesFinalMap = new HashMap<String, Object>();
		propiedadesFinalMap.putAll(propiedadesMap);
		generarExcelXLSXPerMap(listaHeader, listaHeaderCabecera, listaDataMap, archivoName, titulo, propiedadesFinalMap, workbook, paginaParametroList);
	}
	//Fin BUIDSOFT 02 Requerimiento reporte detalle produccion excel
	//Inicio BUIDSOFT 02 Requerimiento reporte detalle produccion excel
	public  static void generarExcelXLSXPerMap(List<String> listaHeader, List<ExcelHederTitleVO> listaHeaderCabecera, List<Map<String, Object>> listaDataMap, String archivoName, String titulo, Map<String, Object> propiedadesMap, SXSSFWorkbook workbookx, List<ExcelHederTitleVO> paginaParametroList) {
	//Fin BUIDSOFT 02 Requerimiento reporte detalle produccion excel
		boolean isFreezePane = propiedadesMap.containsKey("isFreezePane");
		//Inicio BuildSoft 01/10/2019 Reporte Siniestro Taller
		boolean isHeaderCabeceraUnionTitle = propiedadesMap.containsKey("isHeaderCabeceraUnionTitle");
		//Fin BuildSoft 01/10/2019 Reporte Siniestro Taller
		try {
			if (propiedadesMap.containsKey("writeExcel")) {
				File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER);
				boolean isExisteArchivo = archivoXLS.exists();
				if (!isExisteArchivo || !archivoXLS.isFile()) 
					archivoXLS.mkdirs();
			}
			SXSSFWorkbook workbook = workbookx;
			synchronized (workbook) {
			workbook.setCompressTempFiles(true); // temp files will be gzipped
			boolean calcularWitchDemanda = Boolean.valueOf(propiedadesMap.containsKey("calcularWitchDemanda"));
			Map<Integer, Integer> columnWidtMaxMap = new HashMap<Integer, Integer>();
			int cantidadData = listaDataMap.size();
			int cantidadHojas = 1;
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue();
			}			
			CellStyle cellDateStyle = generarStyleDateDMY(workbook);
			Map<String,CellStyle> cellDateStyleMap = new HashMap<String, CellStyle>();
			CellStyle titleStyle = generarStyleTitle(workbook);
			CellStyle numberDecimalStyle = generarStyleNumberDecimal(workbook);
			//Inicio BuildSoft 02/10/2019 reporte siniestro taller
			CellStyle numberDecimalStylePer = generarStyleNumberDecimalSytle(workbook);
			CellStyle numberStyle = generarStyleNumberStyle(workbook);
			//Fin BuildSoft 02/10/2019 reporte siniestro taller
			//Inicio BUIDSOFT 02 Requerimiento reporte detalle produccion excel
			Map<String,String> overrideHeaderMap = new HashMap<String, String>();
			if (propiedadesMap.containsKey("overrideHeaderMap")) {
				overrideHeaderMap = (Map<String, String>) propiedadesMap.get("overrideHeaderMap");
			}
			//Fin BUIDSOFT 02 Requerimiento reporte detalle produccion excel
			if (propiedadesMap.containsKey("escribirHojaParametro")) {
				String escribirHojaParametro = (String) propiedadesMap.get("escribirHojaParametro");
				String hojaParametroTitulo = (String) propiedadesMap.get("hojaParametroTitulo"); 
				if ("true".equals(escribirHojaParametro)) {	
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(hojaParametroTitulo); //CREA UNA HOJA
					for (ExcelHederTitleVO excelHeaderTitle : paginaParametroList) {
						int posicionRowVar =  excelHeaderTitle.getPosicionRow();
						int posicionCeldaVar =  excelHeaderTitle.getPosicionCelda();
						if (posicionRowVar > 0) {
							posicionRowVar = posicionRowVar - 1;
						}
						if (posicionCeldaVar > 0) {
							posicionCeldaVar = posicionCeldaVar - 1;
						}
						SXSSFRow  filaTitle = (SXSSFRow) sheet.getRow(posicionRowVar); 
				    	if (filaTitle == null) {
				    	  	filaTitle = (SXSSFRow) sheet.createRow(posicionRowVar); 
				    	}
						String tituloFinalPer = excelHeaderTitle.getNameHeader();
						SXSSFCell  heraderTitleCell = null;
						SXSSFCell  valorCell = null;
						if (posicionCeldaVar > 0) {
							heraderTitleCell = (SXSSFCell)filaTitle.createCell(posicionCeldaVar);
							int posicionValue = posicionCeldaVar + 1;
							valorCell = (SXSSFCell)filaTitle.createCell(posicionValue);
						} else {
							heraderTitleCell = (SXSSFCell)filaTitle.createCell(0);
							valorCell = (SXSSFCell)filaTitle.createCell(1);
						}
						heraderTitleCell.setCellValue(tituloFinalPer);
						if (valorCell != null) {
							valorCell.setCellValue(excelHeaderTitle.getValor());
						}
						CellStyle titleStyleVar = generarStyleTitle(workbook, excelHeaderTitle.getFontHeightInPoints());
						heraderTitleCell.setCellStyle(titleStyleVar);
						heraderTitleCell.getCellStyle().setAlignment(excelHeaderTitle.getAling()); //por defecto se alinea a la izquierda
						if (valorCell != null) {
							CellStyle titleStyleData = generarStyleData(workbook);
							valorCell.setCellStyle(titleStyleData);
							valorCell.getCellStyle().setAlignment(excelHeaderTitle.getAling());
						}
						if (excelHeaderTitle.getVerticalAlignment() > -1) {
							heraderTitleCell.getCellStyle().setVerticalAlignment(excelHeaderTitle.getVerticalAlignment());  //si se define otra alienacion
							if (valorCell != null) {
								valorCell.getCellStyle().setVerticalAlignment(excelHeaderTitle.getVerticalAlignment());
							}
						}
						//autosizecolumna
						sheet.autoSizeColumn(heraderTitleCell.getColumnIndex());
						if (valorCell != null) {
							sheet.autoSizeColumn(valorCell.getColumnIndex());
						}
						excelHeaderTitle.setPosicionRow(posicionRowVar);
						excelHeaderTitle.setPosicionCelda(posicionCeldaVar);
					}
					
					for (ExcelHederTitleVO excelHederTitleVO : paginaParametroList) {
						try {
							CellRangeAddress range = null;
							if (excelHederTitleVO.getCantidadAgrupar() > 0 && excelHederTitleVO.getCantidadAgruparHorizontal() == 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(),excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(range);
								generarMergeRegionBorder(range, sheet, workbook);//generarMergeRegionBorder(range, sheet, workbook);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar() == 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(),((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue());
								sheet.addMergedRegion(new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(),((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue()));
								generarMergeRegionBorder(range, sheet, workbook);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar() > 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(),((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1)+ excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(range);
								generarMergeRegionBorder(range, sheet, workbook);
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
					}
				}
			}
			
			log.info("generarExcelObjectMapBigMemory.cantidadHojas --> " + cantidadHojas);
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap.containsKey("hojaName")) {
					//Inicio BUIDSOFT 02 Requerimiento reporte detalle produccion excel
					tituloFinal = (String )propiedadesMap.get("hojaName");
					//Fin BUIDSOFT 02 Requerimiento reporte detalle produccion excel
				}
				if (cantidadDataPaginadorHoja > 1) {
					tituloFinal = cantidadDataPaginadorHoja + tituloFinal;
				}
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal);
				int posicionRow = 0; //aca coloco el numero de row donde me he quedado
				int incrementroRow = 1;
				
				if (propiedadesMap.containsKey("printTitleView")) {
					SXSSFRow filaTitle = (SXSSFRow) sheet.createRow(posicionRow);
					SXSSFCell heraderTitleCell = (SXSSFCell) filaTitle.createCell(0);
					heraderTitleCell.setCellValue(titulo);
					heraderTitleCell.setCellStyle(titleStyle);
					if (listaHeader.size() > 1) {
						sheet.addMergedRegion(new CellRangeAddress(posicionRow, posicionRow, 0, listaHeader.size() - 1));
					}
					posicionRow = posicionRow + incrementroRow;
				}
				
				// creando cabecera del datos
				//Inicio BUIDSOFT 02 Requerimiento reporte detalle produccion excel
				if (propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio").toString()) - 1;
				}
				//Fin BUIDSOFT 02 Requerimiento reporte detalle produccion excel
				SXSSFRow fila = (SXSSFRow) sheet.createRow(posicionRow);
				int posicionCellCabecera = 0;
				int incremetoCellCabecera = 1; 
				int maxPosicionRow = 0;
				if (listaHeaderCabecera != null && listaHeaderCabecera.size() > 0) {
					for (ExcelHederTitleVO excelHeaderTitle : listaHeaderCabecera) {
						int posicionRowVar =  excelHeaderTitle.getPosicionRow();
						int posicionCeldaVar =  excelHeaderTitle.getPosicionCelda();
						if (posicionRowVar > 0) {
							posicionRowVar = posicionRowVar - 1;
						}
						if (posicionCeldaVar > 0) {
							posicionCeldaVar = posicionCeldaVar - 1;
						}
						if (posicionRowVar > maxPosicionRow) {
							maxPosicionRow = posicionRowVar;
						}
						SXSSFRow  filaTitle = (SXSSFRow)sheet.getRow(posicionRowVar);
				    	if (filaTitle == null) {
				    	  	filaTitle = (SXSSFRow)sheet.createRow(posicionRowVar);
				    	}
						String tituloFinalPer = excelHeaderTitle.getNameHeader();
						SXSSFCell  heraderTitleCell = null;
						if (posicionCeldaVar > 0) {
							heraderTitleCell = (SXSSFCell)filaTitle.createCell(posicionCeldaVar);
						} else {
							heraderTitleCell = (SXSSFCell)filaTitle.createCell(0);
						}
						heraderTitleCell.setCellValue(tituloFinalPer);
						
						CellStyle titleStyleVar = generarStyleTitle(workbook, excelHeaderTitle.getFontHeightInPoints());
						heraderTitleCell.setCellStyle(titleStyleVar);
						heraderTitleCell.getCellStyle().setAlignment(excelHeaderTitle.getAling()); //por defecto se alinea a la izquierda
						
						if (excelHeaderTitle.getVerticalAlignment() > -1) {
							heraderTitleCell.getCellStyle().setVerticalAlignment(excelHeaderTitle.getVerticalAlignment());  //si se define otra alienacion
						}
						excelHeaderTitle.setPosicionRow(posicionRowVar);
						excelHeaderTitle.setPosicionCelda(posicionCeldaVar);
						if (excelHeaderTitle.getColumnIndex() > -1 && excelHeaderTitle.getWidth() > -1) {
							columnWidtMaxMap.put(excelHeaderTitle.getColumnIndex(), excelHeaderTitle.getWidth());
						}
						//Inicio BuildSoft Reporte Comision
						if (propiedadesMap.containsKey("wrapText")) {
							heraderTitleCell.getCellStyle().setWrapText(true);
						}
						//Fin BuildSoft Reporte Comision
					}
				} else {
				//Inicio BuildSoft 01/10/2019 Reporte Siniestro Taller
					isHeaderCabeceraUnionTitle = true;					
				}
				if (isHeaderCabeceraUnionTitle) {
					if ( propiedadesMap.containsKey("isHeaderCabeceraUnionTitle")) {
						if (propiedadesMap.containsKey("rowInicio")) {
							maxPosicionRow = 0;
						}
					}
					int columnIndex = 0;
					for (String cellHeader : listaHeader) {
						SXSSFCell heraderCell = (SXSSFCell) fila.createCell(posicionCellCabecera);
						heraderCell.setCellValue(cellHeader);
						//Inicio BUIDSOFT 02 Requerimiento reporte detalle produccion excel
						if (overrideHeaderMap.containsKey(cellHeader)) {
							heraderCell.setCellValue(overrideHeaderMap.get(cellHeader));
						}
						//Fin BUIDSOFT 02 Requerimiento reporte detalle produccion excel
						heraderCell.setCellStyle(titleStyle);
                        //Inicio BuildSoft Reporte Comision
						if (propiedadesMap.containsKey("wrapText")) {
							heraderCell.getCellStyle().setWrapText(true);
						}
                        //Fin BuildSoft Reporte Comision
						posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
						//Inicio BuildSoft Mant OA
						if (calcularWitchDemanda) {
							int widtMaxActual = ObjectUtil.objectToString(heraderCell.getStringCellValue()).length();
							double porcentaje = 0.20;
							if (!columnWidtMaxMap.containsKey(columnIndex)) {
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
						}
						columnIndex++;
						//Fin BuildSoft Mant OA
					}
				}
				//Fin BuildSoft 01/10/2019 Reporte Siniestro Taller
				posicionRow = maxPosicionRow + posicionRow + incrementroRow;
				
				if (listaHeaderCabecera != null && listaHeaderCabecera.size() > 0) {
					for (ExcelHederTitleVO excelHederTitleVO : listaHeaderCabecera) {
						try {
							//int firstRow, int lastRow, int firstCol, int lastCol
							CellRangeAddress range = null;
							if (excelHederTitleVO.getCantidadAgrupar() > 0 && excelHederTitleVO.getCantidadAgruparHorizontal() == 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionRow().intValue(),excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1)+ excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(range);
								generarMergeRegionBorder(range, sheet, workbook);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  == 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue());
								sheet.addMergedRegion(new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue()));
								generarMergeRegionBorder(range, sheet, workbook);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar()  > 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(), ((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(range);
								generarMergeRegionBorder(range, sheet, workbook);
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
					}
				}
				
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
				int toIndex = toIndexXlsx(cantidadData, cantidadDataPaginadorHoja);
				//Inicio Build Soft Mant OA
				if (isFreezePane) {
					int colSplit= 0;
					int rowSplit = posicionRow;
					if (!StringUtil.isNullOrEmptyNumeriCero(propiedadesMap.get("isFreezePaneColSplit"))) {
						colSplit = ObjectUtil.objectToInteger(propiedadesMap.get("isFreezePaneColSplit"));
					}
					if (!StringUtil.isNullOrEmptyNumeriCero(propiedadesMap.get("isFreezePaneRowSplit"))) {
						rowSplit = ObjectUtil.objectToInteger(propiedadesMap.get("isFreezePaneRowSplit"));
					}
					
					sheet.createFreezePane(colSplit, rowSplit);
				}
				CellStyle cswrapText = workbook.createCellStyle();
				cswrapText.setWrapText(true);
				//Fin Build Soft Mant OA
				for (Map<String, Object> dataMap : listaDataMap.subList(fromIndex, toIndex)) {
					SXSSFRow filaDet = (SXSSFRow) sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					int columnIndex = 0;
					for (String headerKey : listaHeader) {
						Object value = null;
						if (propiedadesMap.containsKey(headerKey)) {
							value = dataMap.get(headerKey);
							if (StringUtil.isNullOrEmpty(value)) {
								value = 0;
							}
						} else {
							value = dataMap.get(headerKey);
						}
						if (esFechaData(value)) {
							SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue((Date) value);
							if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Format")) {
								if (!cellDateStyleMap.containsKey(headerKey)) {
									DataFormat format = workbook.createDataFormat();
									CellStyle cellDateStyleFormat =  workbook.createCellStyle();
									cellDateStyleFormat.setDataFormat(format.getFormat(propiedadesMap.get(headerKey + "Format") + ""));
									cellDetalle.setCellStyle(cellDateStyleFormat);
									cellDateStyleMap.put(headerKey, cellDateStyleFormat);
								} else {
									cellDetalle.setCellStyle(cellDateStyleMap.get(headerKey));
								}
							} else {
								cellDetalle.setCellStyle(cellDateStyle);
							}
							posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
						} else {
							//Inicio BuildSoft 01/10/2019 reporte taller
							if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Numeric")) {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								if (!StringUtil.isNullOrEmptyNumeriCero(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(value == null ? "" : value.toString());
								}
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Decimal")) {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (!StringUtil.isNullOrEmptyNumeriCero(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(Double.valueOf("0.00"));  //se rellena con cero
									}
									cellDetalle.setCellStyle(numberDecimalStyle);
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								} else if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "DecimalSytle")) {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (!StringUtil.isNullOrEmptyNumeriCero(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(Double.valueOf("0.00"));  //se rellena con cero
									}
									cellDetalle.setCellStyle(numberDecimalStylePer);
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								} else if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "NumericStyle")) {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (!StringUtil.isNullOrEmptyNumeriCero(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(Double.valueOf("0"));  //se rellena con cero
									}
									cellDetalle.setCellStyle(numberStyle);
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;	
								} else {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (esNumericoData(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}
									//Inicio BuildSoft 
									if (propiedadesMap.containsKey("wrapText" + headerKey)) {
										cellDetalle.setCellStyle(cswrapText);
									}
									//Fin BuildSoft
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								}
							}
						}
						//Fin BuildSoft 01/10/2019 reporte taller
						if (calcularWitchDemanda && !propiedadesMap.containsKey("witch" + headerKey) ) {
							int widtMaxActual = ObjectUtil.objectToString(headerKey).length();
							double porcentaje = 0.20;
							if (!columnWidtMaxMap.containsKey(columnIndex)) {
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
							int widtMax = columnWidtMaxMap.get(columnIndex);
							widtMaxActual = ObjectUtil.objectToString(value).length();
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							if (widtMax < widtMaxActual) {
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
						} else {
							int widtMaxActual = ObjectUtil.objectToInteger(propiedadesMap.get("witch" + headerKey));
							double porcentaje = 0.20;
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							columnWidtMaxMap.put(columnIndex, widtMaxActual);
						}
						columnIndex++;
					}
					i++;
					dataMap = null;
				}

				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
				for (int ih = 0; ih < listaHeader.size(); ih++) {
					if (calcularWitchDemanda) {
						try {
							int width = columnWidtMaxMap.get(autoSizeColunm);
							width *= 256;
							int maxColumnWidth = 255 * 256; // The maximum column width for an individual cell is 255 characters
							if (width > maxColumnWidth) {
								width = maxColumnWidth;
							}
							sheet.setColumnWidth(autoSizeColunm, width);
						} catch (Exception e) {
							// log.error("ERROR autoSizeColunm -->" +
							// autoSizeColunm);
						}
					} else {
						sheet.autoSizeColumn(autoSizeColunm, true);
					}
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
			}
			listaDataMap = null;
			if (propiedadesMap.containsKey("writeExcel")) {
				FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
				workbook.write(out);
				workbook.dispose();
				out.close();
				cellDateStyle = null;
				workbook = null;
				out = null;
				ExcelUtil.defaultLocaleProcess();
			}
		 }
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("Error ", ex);
		}
	}
	
	public synchronized static void generarExcelXLSX(List<ExcelHederDataVO> listaHeaderData, List<?> listaData, String archivoName, String titulo, Map<String, Object> propiedadesMap, SXSSFWorkbook workbook, List<ExcelHederTitleVO> paginaParametroList) {
		try {
			if (propiedadesMap.containsKey("writeExcel")) {
				File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER);
				if (!archivoXLS.isFile()) {
					archivoXLS.mkdirs();
				}
			}
			workbook.setCompressTempFiles(true); // temp files will be gzipped
			boolean calcularWitchDemanda = Boolean.valueOf(propiedadesMap.containsKey("calcularWitchDemanda"));
			Map<Integer, Integer> columnWidtMaxMap = new HashMap<Integer, Integer>();
			int cantidadData = listaData.size();
			int cantidadHojas = 1;
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue();
			}
			DataFormat format = workbook.createDataFormat();
			CellStyle cellDateStyle = generarStyleDateDMY(workbook);
			CellStyle titleStyle = generarStyleTitle(workbook);
			CellStyle numberDecimalStyle = generarStyleNumberDecimal(workbook);
			CellStyle numberStyle = generarStyleNumber(workbook);
			
			if (propiedadesMap.containsKey("escribirHojaParametro")) {
				String escribirHojaParametro = (String) propiedadesMap.get("escribirHojaParametro");
				String hojaParametroTitulo = (String) propiedadesMap.get("hojaParametroTitulo"); 
				if ("true".equals(escribirHojaParametro)) {	
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(hojaParametroTitulo); //CREA UNA HOJA
					for (ExcelHederTitleVO excelHeaderTitle : paginaParametroList) {
						int posicionRowVar =  excelHeaderTitle.getPosicionRow();
						int posicionCeldaVar =  excelHeaderTitle.getPosicionCelda();
						if (posicionRowVar > 0) {
							posicionRowVar = posicionRowVar - 1;
						}
						if (posicionCeldaVar > 0) {
							posicionCeldaVar = posicionCeldaVar - 1;
						}
						SXSSFRow  filaTitle = (SXSSFRow) sheet.getRow(posicionRowVar); 
				    	if (filaTitle == null) {
				    	  	filaTitle = (SXSSFRow) sheet.createRow(posicionRowVar); 
				    	}
						String tituloFinalPer = excelHeaderTitle.getNameHeader();
						SXSSFCell  heraderTitleCell = null;
						SXSSFCell  valorCell = null;
						if (posicionCeldaVar > 0) {
							heraderTitleCell = (SXSSFCell)filaTitle.createCell(posicionCeldaVar);
							int posicionValue = posicionCeldaVar + 1;
							valorCell = (SXSSFCell)filaTitle.createCell(posicionValue);
						} else {
							heraderTitleCell = (SXSSFCell)filaTitle.createCell(0);
							valorCell = (SXSSFCell)filaTitle.createCell(1);
						}
						heraderTitleCell.setCellValue(tituloFinalPer);
						if (valorCell != null) {
							valorCell.setCellValue(excelHeaderTitle.getValor());
						}
						CellStyle titleStyleVar = generarStyleTitle(workbook, excelHeaderTitle.getFontHeightInPoints());
						heraderTitleCell.setCellStyle(titleStyleVar);
						heraderTitleCell.getCellStyle().setAlignment(excelHeaderTitle.getAling()); //por defecto se alinea a la izquierda
						if (valorCell != null) {
							CellStyle titleStyleData = generarStyleData(workbook);
							valorCell.setCellStyle(titleStyleData);
							valorCell.getCellStyle().setAlignment(excelHeaderTitle.getAling());
						}
						if (excelHeaderTitle.getVerticalAlignment() > -1) {
							heraderTitleCell.getCellStyle().setVerticalAlignment(excelHeaderTitle.getVerticalAlignment());  //si se define otra alienacion
							if (valorCell != null) {
								valorCell.getCellStyle().setVerticalAlignment(excelHeaderTitle.getVerticalAlignment());
							}
						}
						//autosizecolumna
						sheet.autoSizeColumn(heraderTitleCell.getColumnIndex());
						if (valorCell != null) {
							sheet.autoSizeColumn(valorCell.getColumnIndex());
						}
						excelHeaderTitle.setPosicionRow(posicionRowVar);
						excelHeaderTitle.setPosicionCelda(posicionCeldaVar);
					}
					
					for (ExcelHederTitleVO excelHederTitleVO : paginaParametroList) {
						try {
							CellRangeAddress range = null;
							if (excelHederTitleVO.getCantidadAgrupar() > 0 && excelHederTitleVO.getCantidadAgruparHorizontal() == 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(),excelHederTitleVO.getPosicionRow().intValue(), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1) + excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(range);
								generarMergeRegionBorder(range, sheet, workbook);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar() == 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(),((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue());
								sheet.addMergedRegion(new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(),((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),excelHederTitleVO.getPosicionCelda().intValue()));
								generarMergeRegionBorder(range, sheet, workbook);
							}
							if (excelHederTitleVO.getCantidadAgruparHorizontal() > 0 && excelHederTitleVO.getCantidadAgrupar() > 0) {
								range = new CellRangeAddress(excelHederTitleVO.getPosicionRow().intValue(),((excelHederTitleVO.getPosicionRow().intValue() - 1) + excelHederTitleVO.getCantidadAgruparHorizontal().intValue()), excelHederTitleVO.getPosicionCelda().intValue(),((excelHederTitleVO.getPosicionCelda().intValue()) - 1)+ excelHederTitleVO.getCantidadAgrupar().intValue());
								sheet.addMergedRegion(range);
								generarMergeRegionBorder(range, sheet, workbook);
							}
						} catch (Exception e) {
							log.error("Error ", e);
						}
					}
				}
			}
			
			for (int cantidadDataPaginadorHoja = 1; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap != null && propiedadesMap.containsKey("hojaName")) {
					tituloFinal = propiedadesMap.get("hojaName") + "";
				}
				if (cantidadHojas > 1) {
					tituloFinal = tituloFinal + cantidadDataPaginadorHoja;
				}
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal); //CREA UNA HOJA
				sheet.setRandomAccessWindowSize(100);// keep 100 rows in memory, exceeding rows will be flushed to disk
				int posicionRow = 0;
				int incrementroRow = 1;
				if (propiedadesMap != null && propiedadesMap.containsKey("printTitleView")) {
					SXSSFRow filaTitle = (SXSSFRow) sheet.createRow(posicionRow);
					SXSSFCell heraderTitleCell = (SXSSFCell) filaTitle.createCell((listaHeaderData.size() / 2));
					heraderTitleCell.setCellValue(titulo);
					posicionRow = posicionRow + incrementroRow;
				}
				// creando cabecera del datos
				if (propiedadesMap != null && propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio") + "") - 1;
				}
				SXSSFRow fila = (SXSSFRow) sheet.createRow(posicionRow);
				int posicionCellCabecera = 0;
				int incremetoCellCabecera = 1;
				for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
					String cellHeader = cellHeaderVO.getNameHeader();
					SXSSFCell heraderCell = (SXSSFCell) fila.createCell(posicionCellCabecera);
					heraderCell.setCellValue(cellHeader);
					heraderCell.setCellStyle(titleStyle);
					posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;	
				}
				posicionRow = posicionRow + incrementroRow;
				// llenando la data
				int primeraFila = posicionRow;
				int i = 0;
				int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
				int toIndex = toIndexXlsx(cantidadData, cantidadDataPaginadorHoja);
				for (Object cellData : listaData.subList(fromIndex, toIndex)) {
					SXSSFRow filaDet = (SXSSFRow) sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					int columnIndex = 0;
					for (ExcelHederDataVO cellHeaderVO : listaHeaderData) {
						String nombreColumna = cellHeaderVO.getNameAtribute();
						Object value = null;
						value = atributoValueComplejo(cellData, nombreColumna);
						if (esFecha(nombreColumna)) {
							Object valueDate = verificarFornatoFecha(nombreColumna, value);
							if (esFechaData(valueDate)) {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue((Date) valueDate);
								if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Format")) {
									CellStyle cellDateStyleFormat = generarStyleDate(workbook);
									cellDateStyleFormat.setDataFormat(format.getFormat(propiedadesMap.get(nombreColumna + "Format") + ""));
									value = FechaUtil.obtenerFechaFormatoPersonalizado((Date)valueDate, propiedadesMap.get(nombreColumna + "Format") + "");
									cellDetalle.setCellStyle(cellDateStyleFormat);
								} else {
									cellDetalle.setCellStyle(cellDateStyle);
								}
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue(value == null ? "" : value.toString());
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							}
						} else {
							if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Decimal")) {
								SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
								if (!StringUtil.isNullOrEmptyNumeriCero(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(Double.valueOf("0.00"));  //se rellena con cero
								}
								cellDetalle.setCellStyle(numberDecimalStyle);
								posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
							} else if (propiedadesMap != null && propiedadesMap.containsKey(nombreColumna + "Numeric")) {
								SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
								if (!StringUtil.isNullOrEmptyNumeriCero(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(Double.valueOf("0"));  //se rellena con cero
								}
								cellDetalle.setCellStyle(numberStyle);
								posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;	
							} else {
								SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
								if (esNumericoData(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(value == null ? "" : value.toString());
								}
								posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
							}
						}
						
						if (calcularWitchDemanda) {
							int widtMaxActual =  ObjectUtil.objectToString(cellHeaderVO.getNameHeader()).length();
							double porcentaje = 0.20;
							if (!columnWidtMaxMap.containsKey(columnIndex)) {
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							} 
							int widtMax = columnWidtMaxMap.get(columnIndex);
							widtMaxActual =  ObjectUtil.objectToString(value).length();
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							if (widtMax < widtMaxActual) {
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
						}
						columnIndex++;
					}					
					i++;
					cellData = null;
				}
				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
				for (int ih = 0; ih < listaHeaderData.size(); ih++) {
					if (calcularWitchDemanda) {
						try {
							int  width = columnWidtMaxMap.get(autoSizeColunm);
							 width *= 256;
					            int maxColumnWidth = 255 * 256; // The maximum column width for an individual cell is 255 characters
					            if (width > maxColumnWidth) {
					                width = maxColumnWidth;
					            }
							sheet.setColumnWidth(autoSizeColunm, width);
						} catch (Exception e) {
							//log.error("ERROR autoSizeColunm -->" + autoSizeColunm);
						}
					} else {
						sheet.autoSizeColumn(autoSizeColunm, true);
					}
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
			}
			listaData = null; //liberar memoria
			if (propiedadesMap.containsKey("writeExcel")) {
				FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
				workbook.write(out);
				workbook.dispose();
				out.close();
				cellDateStyle = null;
				workbook = null;
				out = null;
				ExcelUtil.defaultLocaleProcess();
			}
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
	}
	
	//INICIO:HITSS 
	public static void crea_libro(List<String> listaHeader, List<ExcelHederTitleVO> listaHeaderCabecera, List<Map<String, Object>> listaDataMap, String archivoName, String titulo, Map<String, Object> propiedadesMap, SXSSFWorkbook workbook, List<ExcelHederTitleVO> paginaParametroList, int cantidadHojasIni, int cantidadHojas) {
		
			try {
				int cont_pag = 1;
				int cantidadData = listaDataMap.size();
				boolean calcularWitchDemanda = Boolean.valueOf(propiedadesMap.containsKey("calcularWitchDemanda"));
				Map<Integer, Integer> columnWidtMaxMap = new HashMap<Integer, Integer>();
								
				DataFormat format = workbook.createDataFormat();
				CellStyle cellDateStyle = generarStyleDateDMY(workbook);
				CellStyle titleStyle = generarStyleTitle(workbook);
				CellStyle numberDecimalStyle = generarStyleNumberDecimal(workbook);
				CellStyle numberStyle = generarStyleNumber(workbook);
				
				Map<String,String> overrideHeaderMap = new HashMap<String, String>();
				if (propiedadesMap.containsKey("overrideHeaderMap")) {
					overrideHeaderMap = (Map<String, String>) propiedadesMap.get("overrideHeaderMap");
				}
				
				log.info("generarExcelObjectMapBigMemory.cantidadHojas --> " + cantidadHojas);
				for (int cantidadDataPaginadorHoja = cantidadHojasIni; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
					String tituloFinal = titulo;
					if (propiedadesMap.containsKey("hojaName")) {
						tituloFinal = (String )propiedadesMap.get("hojaName");
					}
					if (cont_pag > 1) {
						tituloFinal = cont_pag + tituloFinal;
					}
					SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal);
					int posicionRow = 0; //aca coloco el numero de row donde me he quedado
					int incrementroRow = 1;
					
					if (propiedadesMap.containsKey("printTitleView")) {
						SXSSFRow filaTitle = (SXSSFRow) sheet.createRow(posicionRow);
						SXSSFCell heraderTitleCell = (SXSSFCell) filaTitle.createCell(0);
						heraderTitleCell.setCellValue(titulo);
						heraderTitleCell.setCellStyle(titleStyle);
						if (listaHeader.size() > 1) {
							sheet.addMergedRegion(new CellRangeAddress(posicionRow, posicionRow, 0, listaHeader.size() - 1));
						}
						posicionRow = posicionRow + incrementroRow;
					}
					
					// creando cabecera del datos
					if (propiedadesMap.containsKey("rowInicio")) {
						posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio").toString()) - 1;
					}
					SXSSFRow fila = (SXSSFRow) sheet.createRow(posicionRow);
					int posicionCellCabecera = 0;
					int incremetoCellCabecera = 1; 
					int maxPosicionRow = 0;
					if (!(listaHeaderCabecera != null && listaHeaderCabecera.size() > 0)){
						for (String cellHeader : listaHeader) {
							SXSSFCell heraderCell = (SXSSFCell) fila.createCell(posicionCellCabecera);
							heraderCell.setCellValue(cellHeader);
							if (overrideHeaderMap.containsKey(cellHeader)) {
								heraderCell.setCellValue(overrideHeaderMap.get(cellHeader));
							}
							heraderCell.setCellStyle(titleStyle);
							if (propiedadesMap.containsKey("wrapText")) {
								heraderCell.getCellStyle().setWrapText(true);
							}
							posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
						}
					}
					posicionRow = maxPosicionRow + posicionRow + incrementroRow;
					
					int primeraFila = posicionRow;
					int i = 0;
					/*int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
					int toIndex = toIndexXlsx(cantidadData, cantidadDataPaginadorHoja);*/
					int fromIndex = fromIndexXlsx(cont_pag);
					int toIndex = toIndexXlsx(cantidadData, cont_pag);
					for (Map<String, Object> dataMap : listaDataMap.subList(fromIndex, toIndex)) {
						SXSSFRow filaDet = (SXSSFRow) sheet.createRow(i + primeraFila);
						posicionCellCabecera = 0;
						incremetoCellCabecera = 1;
						int columnIndex = 0;
						for (String headerKey : listaHeader) {
							Object value = null;
							if (propiedadesMap.containsKey(headerKey)) {
								value = dataMap.get(headerKey);
								if (StringUtil.isNullOrEmpty(value)) {
									value = 0;
								}
							} else {
								value = dataMap.get(headerKey);
							}
							if (esFechaData(value)) {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								cellDetalle.setCellValue((Date) value);
								if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Format")) {
									CellStyle cellDateStyleFormat = generarStyleDate(workbook);
									cellDateStyleFormat.setDataFormat(format.getFormat(propiedadesMap.get(headerKey + "Format") + ""));
									value = FechaUtil.obtenerFechaFormatoPersonalizado((Date)value, propiedadesMap.get(headerKey + "Format") + "");
									cellDetalle.setCellStyle(cellDateStyleFormat);
								} else {
									cellDetalle.setCellStyle(cellDateStyle);
								}
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Numeric")) {
									SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
									if (!StringUtil.isNullOrEmptyNumeric(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}
									posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
								} else {
									if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Decimal")) {
										SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
										if (!StringUtil.isNullOrEmptyNumeric(value)) {
											cellDetalle.setCellValue(Double.valueOf(value.toString()));
										} else {
											cellDetalle.setCellValue(Double.valueOf("0.00"));  //se rellena con cero
										}
										cellDetalle.setCellStyle(numberDecimalStyle);
										posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
									} else if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Numeric")) {
										SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
										if (!StringUtil.isNullOrEmptyNumeric(value)) {
											cellDetalle.setCellValue(Double.valueOf(value.toString()));
										} else {
											cellDetalle.setCellValue(Double.valueOf("0"));  //se rellena con cero
										}
										cellDetalle.setCellStyle(numberStyle);
										posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;	
									} else {
										SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
										if (esNumericoData(value)) {
											cellDetalle.setCellValue(Double.valueOf(value.toString()));
										} else {
											cellDetalle.setCellValue(value == null ? "" : value.toString());
										}
										posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
									}
								}
							}
							if (calcularWitchDemanda) {
								int widtMaxActual = ObjectUtil.objectToString(headerKey).length();
								double porcentaje = 0.20;
								if (!columnWidtMaxMap.containsKey(columnIndex)) {
									widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
									columnWidtMaxMap.put(columnIndex, widtMaxActual);
								}
								int widtMax = columnWidtMaxMap.get(columnIndex);
								widtMaxActual = ObjectUtil.objectToString(value).length();
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								if (widtMax < widtMaxActual) {
									columnWidtMaxMap.put(columnIndex, widtMaxActual);
								}
							}
							columnIndex++;
						}
						i++;
						dataMap = null;
					}

					int autoSizeColunm = 0;// 2
					int incrementoSize = 1;
					for (int ih = 0; ih < listaHeader.size(); ih++) {
						if (calcularWitchDemanda) {
							try {
								int width = columnWidtMaxMap.get(autoSizeColunm);
								width *= 256;
								int maxColumnWidth = 255 * 256; // The maximum column width for an individual cell is 255 characters
								if (width > maxColumnWidth) {
									width = maxColumnWidth;
								}
								sheet.setColumnWidth(autoSizeColunm, width);
							} catch (Exception e) {
								// log.error("ERROR autoSizeColunm -->" +
								// autoSizeColunm);
							}
						} else {
							sheet.autoSizeColumn(autoSizeColunm, true);
						}
						autoSizeColunm = autoSizeColunm + incrementoSize;
					}
					cont_pag = cont_pag + 1;
				}
				listaDataMap = null;
			} catch (Exception ex) {
				ex.printStackTrace();
				log.error("Error ", ex);
			}
		}
	
	public synchronized static void generarExcelXLSXPerMap2(List<String> listaHeader, List<ExcelHederTitleVO> listaHeaderCabecera, List<Map<String, Object>> listaDataMap, String archivoName, String titulo, Map<String, Object> propiedadesMap, SXSSFWorkbook workbook, List<ExcelHederTitleVO> paginaParametroList, List<String> listaHeader2,List<Map<String, Object>> listaDataMap2, Map<String, Object> propiedadesMap2) {		
			try {
				if (propiedadesMap.containsKey("writeExcel")) {
					File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER);
					if (!archivoXLS.isFile()) {
						archivoXLS.mkdirs();
					}
				}				
				workbook.setCompressTempFiles(true);
				CellStyle cellDateStyle = generarStyleDateDMY(workbook);
				
				int cantidadHojas = 1;
				int cantidadHojas2 = 1;
				
				int cantidadData = listaDataMap.size();
				if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX) {
					BigDecimal bCantidadData = new BigDecimal(cantidadData);
					BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
					BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
					bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
					cantidadHojas = bCantidadHojas.intValue();
				}
				System.out.println("Primer Libro");
				crea_libro(listaHeader, listaHeaderCabecera, listaDataMap, archivoName, titulo, propiedadesMap, workbook, paginaParametroList, cantidadHojas2, cantidadHojas);
				listaDataMap = null;
				
				int cantidadData2 = listaDataMap2.size();				
				if (cantidadData2 > MAXIMO_RANGE_EXCEL_XLSX) {
					BigDecimal bCantidadData = new BigDecimal(cantidadData2);
					BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
					BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
					bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
					cantidadHojas2 = bCantidadHojas.intValue();
				}
				System.out.println("Segundo Libro");
				crea_libro(listaHeader2, listaHeaderCabecera, listaDataMap2, archivoName, titulo, propiedadesMap2, workbook, paginaParametroList, (cantidadHojas+1), (cantidadHojas2+cantidadHojas));				
				listaDataMap2 = null;
				
				if (propiedadesMap.containsKey("writeExcel")) {
					FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
					workbook.write(out);
					workbook.dispose();
					out.close();
					cellDateStyle = null;
					workbook = null;
					out = null;
					ExcelUtil.defaultLocaleProcess();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				log.error("Error ", ex);
			}
		}
	//FIN:HITSS

	//INICIO:HITSS 13102021-00419223
	public static void crea_libro2(List<String> listaHeader, List<ExcelHederTitleVO> listaHeaderCabecera, List<Map<String, Object>> listaDataMap, String archivoName, String titulo, Map<String, Object> propiedadesMap, SXSSFWorkbook workbook, List<ExcelHederTitleVO> paginaParametroList, int cantidadHojasIni, int cantidadHojas) {
		
		try {
			int cont_pag = 1;
			int cantidadData = listaDataMap.size();
			boolean calcularWitchDemanda = Boolean.valueOf(propiedadesMap.containsKey("calcularWitchDemanda"));
			Map<Integer, Integer> columnWidtMaxMap = new HashMap<Integer, Integer>();
							
			DataFormat format = workbook.createDataFormat();
			CellStyle cellDateStyle = generarStyleDateDMY(workbook);
			
			////titulo color de letra blanco
			//CellStyle titleStyle = generarStyleTitle(workbook);
			Font titleFont = workbook.createFont();
			titleFont.setFontName(NOMBRE_LETRA);
			titleFont.setFontHeightInPoints((short) 9);
			titleFont.setBoldweight(Font.BOLDWEIGHT_BOLD);
			titleFont.setColor(HSSFColor.WHITE.index);
			CellStyle titleStyle = workbook.createCellStyle();
			titleStyle = generarStyleTitle(titleStyle,titleFont,true);
			////
			
			CellStyle numberDecimalStyle = generarStyleNumberDecimal(workbook);
			CellStyle numberStyle = generarStyleNumber(workbook);
			
			CellStyle cellDateStyle2 = generarStyleDateDMY(workbook);			
			CellStyle numberDecimalStyle2 = generarStyleNumberDecimal(workbook);
			CellStyle numberStyle2 = generarStyleNumber(workbook);
			CellStyle otherStyle2 = workbook.createCellStyle();
			
			short indexedColorsElegido = 0;		
			boolean cabeceraConRelleno = Boolean.valueOf(propiedadesMap.containsKey("cabeceraConRelleno"));
			boolean primeraColumnaConRelleno = Boolean.valueOf(propiedadesMap.containsKey("primeraColumnaConRelleno"));
			boolean celdaConBordes = Boolean.valueOf(propiedadesMap.containsKey("celdaConBordes"));
			if (propiedadesMap.containsKey("indexedColorsElegido")) {
				//org.apache.poi.ss.usermodel.IndexedColors.LIGHT_BLUE.getIndex() --color elegido
				log.info("Paso2 indexedColorsElegido:"+propiedadesMap.get("indexedColorsElegido"));
				indexedColorsElegido=Short.valueOf( propiedadesMap.get("indexedColorsElegido")+"" );
				log.info("Paso3 indexedColorsElegido:"+indexedColorsElegido);
			}							
			CellStyle otherStyle = workbook.createCellStyle();	
			if (cabeceraConRelleno && propiedadesMap.containsKey("indexedColorsElegido")) {
				titleStyle.setFillForegroundColor(indexedColorsElegido);
				titleStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);				
			}

			if(primeraColumnaConRelleno) {
				Font font1 = workbook.createFont();
				font1.setColor(HSSFColor.WHITE.index);
				Font font2 = workbook.createFont();
				font2.setColor(HSSFColor.WHITE.index);
				Font font3 = workbook.createFont();
				font3.setColor(HSSFColor.WHITE.index);
				Font font4 = workbook.createFont();
				font4.setColor(HSSFColor.WHITE.index);
				
				cellDateStyle2=workbook.createCellStyle();
				cellDateStyle2.setDataFormat(format.getFormat(FechaUtil.DATE_DMY));
				cellDateStyle2.setFont(font1);
				
				numberDecimalStyle2 = workbook.createCellStyle();
				numberDecimalStyle2.setDataFormat(format.getFormat("#,##0.00"));
				numberDecimalStyle2.setFont(font2);

				numberStyle2 = workbook.createCellStyle();
				numberStyle2.setDataFormat(format.getFormat("#,##0"));
				numberStyle2.setFont(font3);
				
				otherStyle2 = workbook.createCellStyle();
				otherStyle2.setFont(font4);
				
				numberStyle2.setFillForegroundColor(indexedColorsElegido);
				numberStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);				
				numberDecimalStyle2.setFillForegroundColor(indexedColorsElegido);
				numberDecimalStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);				
				cellDateStyle2.setFillForegroundColor(indexedColorsElegido);
				cellDateStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);				
				otherStyle2.setFillForegroundColor(indexedColorsElegido);
				otherStyle2.setFillPattern(CellStyle.SOLID_FOREGROUND);
			}
			
			log.info("IndexedColors.LIGHT_BLUE.getIndex()");
			log.info("CellStyle.SOLID_FOREGROUND");
			if(celdaConBordes){//nueva condicional			
				numberStyle.setBorderTop(CellStyle.BORDER_THIN);
				numberStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
				numberStyle.setBorderRight(CellStyle.BORDER_THIN);
				numberStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
				numberStyle.setBorderBottom(CellStyle.BORDER_THIN);
				numberStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				numberStyle.setBorderLeft(CellStyle.BORDER_THIN);
				numberStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());				
				numberDecimalStyle.setBorderTop(CellStyle.BORDER_THIN);
				numberDecimalStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
				numberDecimalStyle.setBorderRight(CellStyle.BORDER_THIN);
				numberDecimalStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
				numberDecimalStyle.setBorderBottom(CellStyle.BORDER_THIN);
				numberDecimalStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				numberDecimalStyle.setBorderLeft(CellStyle.BORDER_THIN);
				numberDecimalStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());				
				cellDateStyle.setBorderTop(CellStyle.BORDER_THIN);
				cellDateStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
				cellDateStyle.setBorderRight(CellStyle.BORDER_THIN);
				cellDateStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
				cellDateStyle.setBorderBottom(CellStyle.BORDER_THIN);
				cellDateStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				cellDateStyle.setBorderLeft(CellStyle.BORDER_THIN);
				cellDateStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());				
				otherStyle.setBorderTop(CellStyle.BORDER_THIN);
				otherStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
				otherStyle.setBorderRight(CellStyle.BORDER_THIN);
				otherStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
				otherStyle.setBorderBottom(CellStyle.BORDER_THIN);
				otherStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				otherStyle.setBorderLeft(CellStyle.BORDER_THIN);
				otherStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
				
				numberStyle2.setBorderTop(CellStyle.BORDER_THIN);
				numberStyle2.setTopBorderColor(IndexedColors.BLACK.getIndex());
				numberStyle2.setBorderRight(CellStyle.BORDER_THIN);
				numberStyle2.setRightBorderColor(IndexedColors.BLACK.getIndex());
				numberStyle2.setBorderBottom(CellStyle.BORDER_THIN);
				numberStyle2.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				numberStyle2.setBorderLeft(CellStyle.BORDER_THIN);
				numberStyle2.setLeftBorderColor(IndexedColors.BLACK.getIndex());				
				numberDecimalStyle2.setBorderTop(CellStyle.BORDER_THIN);
				numberDecimalStyle2.setTopBorderColor(IndexedColors.BLACK.getIndex());
				numberDecimalStyle2.setBorderRight(CellStyle.BORDER_THIN);
				numberDecimalStyle2.setRightBorderColor(IndexedColors.BLACK.getIndex());
				numberDecimalStyle2.setBorderBottom(CellStyle.BORDER_THIN);
				numberDecimalStyle2.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				numberDecimalStyle2.setBorderLeft(CellStyle.BORDER_THIN);
				numberDecimalStyle2.setLeftBorderColor(IndexedColors.BLACK.getIndex());				
				cellDateStyle2.setBorderTop(CellStyle.BORDER_THIN);
				cellDateStyle2.setTopBorderColor(IndexedColors.BLACK.getIndex());
				cellDateStyle2.setBorderRight(CellStyle.BORDER_THIN);
				cellDateStyle2.setRightBorderColor(IndexedColors.BLACK.getIndex());
				cellDateStyle2.setBorderBottom(CellStyle.BORDER_THIN);
				cellDateStyle2.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				cellDateStyle2.setBorderLeft(CellStyle.BORDER_THIN);
				cellDateStyle2.setLeftBorderColor(IndexedColors.BLACK.getIndex());				
				otherStyle2.setBorderTop(CellStyle.BORDER_THIN);
				otherStyle2.setTopBorderColor(IndexedColors.BLACK.getIndex());
				otherStyle2.setBorderRight(CellStyle.BORDER_THIN);
				otherStyle2.setRightBorderColor(IndexedColors.BLACK.getIndex());
				otherStyle2.setBorderBottom(CellStyle.BORDER_THIN);
				otherStyle2.setBottomBorderColor(IndexedColors.BLACK.getIndex());
				otherStyle2.setBorderLeft(CellStyle.BORDER_THIN);
				otherStyle2.setLeftBorderColor(IndexedColors.BLACK.getIndex());
			}

			
						
			Map<String,String> overrideHeaderMap = new HashMap<String, String>();
			if (propiedadesMap.containsKey("overrideHeaderMap")) {
				overrideHeaderMap = (Map<String, String>) propiedadesMap.get("overrideHeaderMap");
			}
			
			log.info("generarExcelObjectMapBigMemory.cantidadHojas --> " + cantidadHojas);
			for (int cantidadDataPaginadorHoja = cantidadHojasIni; cantidadDataPaginadorHoja <= cantidadHojas; cantidadDataPaginadorHoja++) {
				String tituloFinal = titulo;
				if (propiedadesMap.containsKey("hojaName")) {
					tituloFinal = (String )propiedadesMap.get("hojaName");
				}
				if (cont_pag > 1) {
					tituloFinal = cont_pag + tituloFinal;
				}
				SXSSFSheet sheet = (SXSSFSheet) workbook.createSheet(tituloFinal);
				int posicionRow = 0; //aca coloco el numero de row donde me he quedado
				int incrementroRow = 1;
				
				if (propiedadesMap.containsKey("printTitleView")) {
					SXSSFRow filaTitle = (SXSSFRow) sheet.createRow(posicionRow);
					SXSSFCell heraderTitleCell = (SXSSFCell) filaTitle.createCell(0);
					heraderTitleCell.setCellValue(titulo);
					heraderTitleCell.setCellStyle(titleStyle);
					if (listaHeader.size() > 1) {
						sheet.addMergedRegion(new CellRangeAddress(posicionRow, posicionRow, 0, listaHeader.size() - 1));
					}
					posicionRow = posicionRow + incrementroRow;
				}
				
				// creando cabecera del datos
				if (propiedadesMap.containsKey("rowInicio")) {
					posicionRow = Integer.parseInt(propiedadesMap.get("rowInicio").toString()) - 1;
				}
				SXSSFRow fila = (SXSSFRow) sheet.createRow(posicionRow);
				int posicionCellCabecera = 0;
				int incremetoCellCabecera = 1; 
				int maxPosicionRow = 0;
				if (!(listaHeaderCabecera != null && listaHeaderCabecera.size() > 0)){
					for (String cellHeader : listaHeader) {
						SXSSFCell heraderCell = (SXSSFCell) fila.createCell(posicionCellCabecera);
						heraderCell.setCellValue(cellHeader);
						if (overrideHeaderMap.containsKey(cellHeader)) {
							heraderCell.setCellValue(overrideHeaderMap.get(cellHeader));
						}
						heraderCell.setCellStyle(titleStyle);
						if (propiedadesMap.containsKey("wrapText")) {
							heraderCell.getCellStyle().setWrapText(true);
						}
						posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
					}
				}
				posicionRow = maxPosicionRow + posicionRow + incrementroRow;
				
				int primeraFila = posicionRow;
				int i = 0;
				/*int fromIndex = fromIndexXlsx(cantidadDataPaginadorHoja);
				int toIndex = toIndexXlsx(cantidadData, cantidadDataPaginadorHoja);*/
				int fromIndex = fromIndexXlsx(cont_pag);
				int toIndex = toIndexXlsx(cantidadData, cont_pag);
				for (Map<String, Object> dataMap : listaDataMap.subList(fromIndex, toIndex)) {
					SXSSFRow filaDet = (SXSSFRow) sheet.createRow(i + primeraFila);
					posicionCellCabecera = 0;
					incremetoCellCabecera = 1;
					int columnIndex = 0;
					for (String headerKey : listaHeader) {
						Object value = null;
						if (propiedadesMap.containsKey(headerKey)) {
							value = dataMap.get(headerKey);
							if (StringUtil.isNullOrEmpty(value)) {
								value = 0;
							}
						} else {
							value = dataMap.get(headerKey);
						}
						if (esFechaData(value)) {
							SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
							cellDetalle.setCellValue((Date) value);
							if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Format")) {
								CellStyle cellDateStyleFormat = generarStyleDate(workbook);
								cellDateStyleFormat.setDataFormat(format.getFormat(propiedadesMap.get(headerKey + "Format") + ""));
								value = FechaUtil.obtenerFechaFormatoPersonalizado((Date)value, propiedadesMap.get(headerKey + "Format") + "");
								cellDetalle.setCellStyle(cellDateStyleFormat);
							} else {
								cellDetalle.setCellStyle(cellDateStyle);
								if(columnIndex==0 && primeraColumnaConRelleno){//nueva condicional
									cellDetalle.setCellStyle(cellDateStyle2);
								}
							}
							posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
						} else {
							if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Numeric")) {
								SXSSFCell cellDetalle = (SXSSFCell) filaDet.createCell(posicionCellCabecera);
								if (!StringUtil.isNullOrEmptyNumeric(value)) {
									cellDetalle.setCellValue(Double.valueOf(value.toString()));
								} else {
									cellDetalle.setCellValue(value == null ? "" : value.toString());
								}
								if(celdaConBordes){//nueva condicional
									cellDetalle.setCellStyle(otherStyle);
								}
								if(columnIndex==0 && primeraColumnaConRelleno){//nueva condicional
									cellDetalle.setCellStyle(otherStyle2);
								}
								posicionCellCabecera = posicionCellCabecera + incremetoCellCabecera;
							} else {
								if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Decimal")) {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (!StringUtil.isNullOrEmptyNumeric(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(Double.valueOf("0.00"));  //se rellena con cero
									}
									cellDetalle.setCellStyle(numberDecimalStyle);
									if(columnIndex==0 && primeraColumnaConRelleno){//nueva condicional
										cellDetalle.setCellStyle(numberDecimalStyle2);
									}
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								} else if (propiedadesMap != null && propiedadesMap.containsKey(headerKey + "Numeric")) {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (!StringUtil.isNullOrEmptyNumeric(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(Double.valueOf("0"));  //se rellena con cero
									}
									cellDetalle.setCellStyle(numberStyle);
									if(columnIndex==0 && primeraColumnaConRelleno){//nueva condicional
										cellDetalle.setCellStyle(numberStyle2);
									}
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;	
								} else {
									SXSSFCell cellDetalle = (SXSSFCell)filaDet.createCell(posicionCellCabecera);
									if (esNumericoData(value)) {
										cellDetalle.setCellValue(Double.valueOf(value.toString()));
									} else {
										cellDetalle.setCellValue(value == null ? "" : value.toString());
									}
									if(celdaConBordes){//nueva condicional
										cellDetalle.setCellStyle(otherStyle);
									}
									if(columnIndex==0 && primeraColumnaConRelleno){//nueva condicional
										cellDetalle.setCellStyle(otherStyle2);
									}
									
									
									posicionCellCabecera = posicionCellCabecera	+ incremetoCellCabecera;
								}
							}
						}
						if (calcularWitchDemanda) {
							int widtMaxActual = ObjectUtil.objectToString(headerKey).length();
							double porcentaje = 0.20;
							if (!columnWidtMaxMap.containsKey(columnIndex)) {
								widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
							int widtMax = columnWidtMaxMap.get(columnIndex);
							widtMaxActual = ObjectUtil.objectToString(value).length();
							widtMaxActual = obtenerWidt(widtMaxActual, porcentaje);
							if (widtMax < widtMaxActual) {
								columnWidtMaxMap.put(columnIndex, widtMaxActual);
							}
						}
						columnIndex++;
					}
					i++;
					dataMap = null;
				}

				int autoSizeColunm = 0;// 2
				int incrementoSize = 1;
				for (int ih = 0; ih < listaHeader.size(); ih++) {
					if (calcularWitchDemanda) {
						try {
							int width = columnWidtMaxMap.get(autoSizeColunm);
							width *= 256;
							int maxColumnWidth = 255 * 256; // The maximum column width for an individual cell is 255 characters
							if (width > maxColumnWidth) {
								width = maxColumnWidth;
							}
							sheet.setColumnWidth(autoSizeColunm, width);
						} catch (Exception e) {
							// log.error("ERROR autoSizeColunm -->" +
							// autoSizeColunm);
						}
					} else {
						sheet.autoSizeColumn(autoSizeColunm, true);
					}
					autoSizeColunm = autoSizeColunm + incrementoSize;
				}
				cont_pag = cont_pag + 1;
			}
			listaDataMap = null;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("Error ", ex);
		}
	}

	public synchronized static void generarExcelXLSXPerMap3(List<String> listaHeader, List<ExcelHederTitleVO> listaHeaderCabecera, List<Map<String, Object>> listaDataMap, String archivoName, String titulo, Map<String, Object> propiedadesMap, SXSSFWorkbook workbook, List<ExcelHederTitleVO> paginaParametroList, List<String> listaHeader2,List<Map<String, Object>> listaDataMap2, Map<String, Object> propiedadesMap2, List<String> listaHeader3,List<Map<String, Object>> listaDataMap3, Map<String, Object> propiedadesMap3) {		
		try {
			if (propiedadesMap.containsKey("writeExcel")) {
				File archivoXLS = new File(RUTA_RECURSOS_BYTE_BUFFER);
				if (!archivoXLS.isFile()) {
					archivoXLS.mkdirs();
				}
			}				
			workbook.setCompressTempFiles(true);
			CellStyle cellDateStyle = generarStyleDateDMY(workbook);
			
			int cantidadHojas = 1;
			int cantidadHojas2 = 1;
			int cantidadHojas3 = 1;
			
			int cantidadData = listaDataMap.size();
			if (cantidadData > MAXIMO_RANGE_EXCEL_XLSX) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas = bCantidadHojas.intValue();
			}
			System.out.println("Primer Libro");
			crea_libro2(listaHeader, listaHeaderCabecera, listaDataMap, archivoName, titulo, propiedadesMap, workbook, paginaParametroList, cantidadHojas2, cantidadHojas);
			listaDataMap = null;
			
			int cantidadData2 = listaDataMap2.size();				
			if (cantidadData2 > MAXIMO_RANGE_EXCEL_XLSX) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData2);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas2 = bCantidadHojas.intValue();
			}
			System.out.println("Segundo Libro");
			crea_libro2(listaHeader2, listaHeaderCabecera, listaDataMap2, archivoName, titulo, propiedadesMap2, workbook, paginaParametroList, (cantidadHojas+1), (cantidadHojas2+cantidadHojas));				
			listaDataMap2 = null;			

			int cantidadData3 = listaDataMap3.size();				
			if (cantidadData3 > MAXIMO_RANGE_EXCEL_XLSX) {
				BigDecimal bCantidadData = new BigDecimal(cantidadData3);
				BigDecimal maxRange = new BigDecimal(MAXIMO_RANGE_EXCEL_XLSX);
				BigDecimal bCantidadHojas = bCantidadData.divide(maxRange, 2, BigDecimal.ROUND_UP);
				bCantidadHojas = bCantidadHojas.setScale(0, BigDecimal.ROUND_UP);
				cantidadHojas3 = bCantidadHojas.intValue();
			}
			System.out.println("Tercer Libro");
			log.info("Tercer Libro");
			crea_libro2(listaHeader3, listaHeaderCabecera, listaDataMap3, archivoName, titulo, propiedadesMap3, workbook, paginaParametroList, (cantidadHojas+1+1), (cantidadHojas3+cantidadHojas2+cantidadHojas));				
			listaDataMap3 = null;			
			
			if (propiedadesMap.containsKey("writeExcel")) {
				FileOutputStream out = new FileOutputStream(RUTA_RECURSOS_BYTE_BUFFER + "" + archivoName + ".xlsx");
				workbook.write(out);
				workbook.dispose();
				out.close();
				cellDateStyle = null;
				workbook = null;
				out = null;
				ExcelUtil.defaultLocaleProcess();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error("Error ", ex);
		}
	}
	//FIN:HITSS 13102021-00419223
	
	
	private static CellStyle generarStyleDate(CellStyle cellDateStyle,DataFormat format) {
		cellDateStyle.setDataFormat(format.getFormat(DD_MM_YYY_HH_MM_SS));
		return cellDateStyle;
	}
	
	private static CellStyle generarStyleTitle(SXSSFWorkbook workbook,short fontHeightInPoints) {
		Font titleFont = generarTitleFont(workbook, fontHeightInPoints,true);
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle = generarStyleTitle(titleStyle, titleFont,true);
		return titleStyle;
	}
	private static CellStyle generarStyleTitleData(SXSSFWorkbook workbook,short fontHeightInPoints, boolean styleMarco, boolean bold) {
		Font titleFont = generarTitleFont(workbook, fontHeightInPoints,bold);
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle = generarStyleTitle(titleStyle, titleFont,styleMarco);
		return titleStyle;
	}
	
	private static Font generarTitleFont(Font titleFont,
			short fontHeightInPoints,boolean bold) {
		titleFont.setFontName(NOMBRE_LETRA);
		titleFont.setFontHeightInPoints((short) fontHeightInPoints);
		titleFont.setBold(bold);
		titleFont.setColor(HSSFColor.BLACK.index);
		return titleFont;
	}

	private static Font generarTitleFont(SXSSFWorkbook workbook,
			short fontHeightInPoints,boolean bold) {
		Font titleFont = workbook.createFont();
		titleFont = generarTitleFont(titleFont, fontHeightInPoints,bold);
		return titleFont;
	}
}
