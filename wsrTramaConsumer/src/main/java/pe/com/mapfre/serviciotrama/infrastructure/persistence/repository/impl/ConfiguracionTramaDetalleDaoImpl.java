package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ConfiguracionTramaDetalleDaoLocal;

/**
 * La Class ConfiguracionTramaDetalleDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Repository
public class ConfiguracionTramaDetalleDaoImpl extends  GenericRepository<Long, ConfiguracionTramaDetalle> implements ConfiguracionTramaDetalleDaoLocal  {

    @Override
    public  List<ConfiguracionTramaDetalle> obtenerReglasConfDetalle(){
    	Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		jpaql.append("from ConfiguracionTramaDetalle  o where  1=1");
		jpaql.append(" and (o.reglaNegocio is not null) ");
		Query query = createQuery(jpaql.toString(),parametros);
		return query.getResultList();
    }
}