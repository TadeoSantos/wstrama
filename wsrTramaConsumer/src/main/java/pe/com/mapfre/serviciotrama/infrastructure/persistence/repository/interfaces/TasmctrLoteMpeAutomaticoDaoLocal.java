package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import java.util.List;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpePK;

/**
 * La Class TasmctrLoteMpeAutomaticoDaoLocal.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface TasmctrLoteMpeAutomaticoDaoLocal  extends IGenericRepository<TasmctrLoteMpePK,TasmctrLoteMpe> {
	/**
	 * Listar tasmctrl mpe.
	 *
	 * @param tasmctrlMpe el tasmctrl mpeDTO
	 * @return the list
	 * @throws Exception the exception
	 */
	List<TasmctrLoteMpe> listar(TasmctrLoteMpe filtro);
	
	/**
	 * contar lista tasmctrl mpe.
	 *
	 * @param tasmctrlMpe el tasmctrl mpe
	 * @return the list
	 * @throws Exception the exception
	 */
	int contar(TasmctrLoteMpe filtro);
}