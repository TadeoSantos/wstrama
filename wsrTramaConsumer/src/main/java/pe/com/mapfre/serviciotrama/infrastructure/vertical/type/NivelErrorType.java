package  pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class NivelErrorType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum NivelErrorType {

    /** El LESTURA_TRAMA. */
 	LECTURA_TRAMA("1" , "nivelError.lectura_trama"),
	
    /** El VALIDACION_PRE_EMISION. */
 	VALIDACION_PRE_EMISION("2" , "nivelError.validacion_pre_emision"),
	
    /** El VALIDACION_POST_EMISION. */
 	VALIDACION_POST_EMISION("3" , "nivelError.validacion_post_emision"),
	
    /** El IMPRESION_DOCUMENTOS. */
 	IMPRESION_DOCUMENTOS("4" , "nivelError.impresion_documentos");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, NivelErrorType> LOO_KUP_MAP = new HashMap<String, NivelErrorType>();
	
	static {
		for (NivelErrorType s : EnumSet.allOf(NivelErrorType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo nivel error type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private NivelErrorType(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the nivel error type
	 */
	public static NivelErrorType get(Long key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
