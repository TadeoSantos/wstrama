package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ValidarNomenclaturaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ValidarNomenclaturaVO implements Serializable  {

	private static final long serialVersionUID = 1L;
	private Map<String, Map<String, Map<String,Object>>> valueGrupoMap = new HashMap<String, Map<String,Map<String,Object>>>();
	private  Map<String,String> nomenclaturaExcluirMap = new HashMap<>();
	
	public ValidarNomenclaturaVO() {
		super();
	}
	
	public ValidarNomenclaturaVO(
			Map<String, Map<String, Map<String, Object>>> valueGrupoMap,
			Map<String, String> nomenclaturaExcluirMap) {
		super();
		this.valueGrupoMap = valueGrupoMap;
		this.nomenclaturaExcluirMap = nomenclaturaExcluirMap;
	}
	
}
