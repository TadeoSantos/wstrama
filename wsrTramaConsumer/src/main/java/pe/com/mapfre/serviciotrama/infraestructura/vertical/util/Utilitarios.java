package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;

/**
 * La Class Utilitarios.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class Utilitarios {
	
	public static String NVL(Object obj){
		String retorno ="";
		if(obj!=null){
			retorno = obj.toString();
		}
		return retorno;
	}
	public static String NVL(Object obj,String opcional){
		String retorno ="";
		if(obj!=null){
			retorno = obj.toString();
		}
		else{
			retorno = opcional;
		}
		return retorno;
	}
	public static BigDecimal NVLBigDecimal(BigDecimal obj){
		BigDecimal retorno =new BigDecimal("0");
		if(obj!=null){
			retorno =obj;
		}
		return retorno;
	}
	public static BigDecimal NVLBigDecimal(String obj){
		BigDecimal retorno =new BigDecimal("0");
		if(obj!=null){
			retorno =new BigDecimal(obj);
		}
		return retorno;
	}
	public static BigDecimal NVLBigDecimal(Integer obj){
		BigDecimal retorno =new BigDecimal("0");
		if(obj!=null){
			retorno =new BigDecimal(obj);
		}
		return retorno;
	}
	public static BigDecimal NVLBigDecimal(double obj){
		BigDecimal retorno =new BigDecimal("0");
		if(obj!=0.0){
			String tmp = obj+"";
			retorno =new BigDecimal(tmp);
			//retorno.setScale(2, RoundingMode.CEILING);
			//retorno = retorno.round(new MathContext(2, RoundingMode.CEILING));
		}
		return retorno;
	}
	//15022018
	public static BigDecimal NVLBigDecimal(Double obj){
		BigDecimal retorno =new BigDecimal("0");
		if(obj!=null){
			retorno =new BigDecimal(obj.doubleValue()+"");
		}
		return retorno;
	}
	public static BigDecimal NVLBigDecimal(int obj){
		BigDecimal retorno =new BigDecimal("0");
		if(obj!=0){
			retorno =new BigDecimal(obj);
		}
		return retorno;
	}
	
	public static boolean MensajeExitoso(String codMensaje){
		boolean retorno =false;
		if(codMensaje!=null){
			if(codMensaje.equals("0") || codMensaje.equals("OK")  || codMensaje.equals("200")){
				retorno = true;
			}
		}
		return retorno;
	}
	
	public static Integer BigDecimalAInt(BigDecimal obj){
		Integer retorno;

		try{
			retorno = obj.intValueExact();
		}catch(Exception e){
			retorno = null;
		}
		return retorno;
	}
	
	public static Double BigDecimalADouble(BigDecimal obj){
		Double retorno = null;

		try{
			retorno = obj.doubleValue();
		}catch(Exception e){
			retorno = null;
		}
		return retorno;
	}
	
	public static Integer StringAInt(BigDecimal obj){
		Integer retorno = null;
		
		try{
			retorno = obj.intValueExact();
		}catch(Exception e){
			retorno = null;
		}
		return retorno;
	}
	
	public static Integer StringAInt(String obj){
		Integer retorno = null;
		
		try{
			retorno = Integer.parseInt(obj);
		}catch(Exception e){
			retorno = null;
		}
		return retorno;
	}
	/*
	public static int StringAIntPrimitivo(String obj){
		int retorno = 0;
		
		try{
			retorno = Integer.parseInt(obj);
		}catch(Exception e){
			retorno = 0;
		}
		return retorno;
	}*/
	
	public static Double StringADouble(Object obj){
		Double retorno = new Double("0");
		
		String datoS ="0";
		if(obj!=null){
			datoS = obj.toString().trim()==""?"0":obj.toString();
		}
		
		try{
			retorno = Double.parseDouble(datoS);
		}catch(NumberFormatException e){
			retorno = new Double("0");
		}
		catch(ArithmeticException e){
			retorno = new Double("0");
		}
		return retorno;
	}
	public static BigDecimal StringABigDecimal(String obj){
		BigDecimal retorno =new BigDecimal("0");
		try {
			if(obj!=null){
				retorno =new BigDecimal(obj);
			}
		} catch (Exception e) {
			retorno =new BigDecimal("0");
		}
		return retorno;
	}
	
	public synchronized static String encodeFileToBase64Binary(File archivo) throws IOException {
	    byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(archivo));
	    return new String(encoded, StandardCharsets.UTF_8);
	}
	public static String BigDecimalAString(BigDecimal obj) {
		
		String retorno = "";
		
		try {
			retorno = String.valueOf(obj);
		} catch (Exception e) {
			retorno = "";
		}
		return retorno;
	}
	
	public static boolean MensajeExitosoOInformativo(String codMensaje){
		boolean retorno =false;
		if(codMensaje!=null){
			if(codMensaje.equals("0") || codMensaje.equals("OK") || codMensaje.equals("IN")){
				retorno = true;
			}
		}
		return retorno;
	}
	
	
	public static String ConvertirAFormatoDeFecha(String fecha,String formatoAntiguo, String formatoNuevo) {
		SimpleDateFormat dt1 = new SimpleDateFormat(formatoAntiguo);
		Date dtFecha = null;
		
		try {
			dtFecha = dt1.parse(fecha);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return null;
		}
		
		SimpleDateFormat dt2 = new SimpleDateFormat(formatoNuevo);
		
		
		return dt2.format(dtFecha);
	}
	
	public static Integer ConvertirEstCivil(String obj){
		Integer retorno = null;
		String temp;
		if(obj!=null) {
			temp=obj;
		}else {
			temp="";
		}
		switch (temp) {
	        case "S": retorno = 1;
	                break;
	        case "C": retorno = 2;
	                break;
	        case "V": retorno = 3;
	                break;
	        case "D": retorno = 4;
	                break;
	        default: retorno = 0;
        			break;
	    }		
		return retorno;
	}
	
	//INI CCABANILLAS - HITSS - 02/04/2018
	public static Long BigDecimalALong(BigDecimal var) {
		try {
			if (var == null) {
		        return null;
		    }
		    return var.longValueExact();
		}catch(Exception e){
			return null;
		}	    
	}
	//FIN CCABANILLAS - HITSS - 02/04/2018
}
