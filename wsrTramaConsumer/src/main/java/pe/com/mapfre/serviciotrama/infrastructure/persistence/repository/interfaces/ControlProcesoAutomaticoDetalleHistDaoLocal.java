package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import java.util.Map;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.domain.entity.vo.GrupoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProcesoDetalleHist;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpe;

/**
 * La Class ControlProcesoAutomaticoDetalleHistDaoLocal.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface ControlProcesoAutomaticoDetalleHistDaoLocal  extends IGenericRepository<String, ControlProcesoDetalleHist> {
	
	boolean registrar(TasmctrLoteMpe tasmctrlMpe,GrupoConfiguracionTramaVO configuracionTramaCnfVO,Map<String,Boolean> juegoConfiguracionTramaErrorMap,Map<String, Object> parametroMap);
}