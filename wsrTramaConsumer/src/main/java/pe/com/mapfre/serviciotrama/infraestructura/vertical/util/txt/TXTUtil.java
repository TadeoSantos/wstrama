package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.txt;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;

import org.mozilla.universalchardet.UniversalDetector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

/**
 * <ul>
 * <li>Copyright 2014 MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 * 
 * La Class TXTUtil.
 *
 * @author Tecnocom Per&uacute; S.A.C.
 * @version 1.0 , 07/04/2015
 * @since MYTRON-MAPFRE 1.0
 */
public class TXTUtil {

	/**
	 * Logger para el registro de errores.
	 */
	private static Logger log = LoggerFactory.getLogger(TXTUtil.class);

	public static List<Map<String, ValueDataVO>> transferObjetoEntityTXTMapDTO(BufferedReader br, int filaData,
			Map<String, Object> campoMappingExcelMap, Map<String, String> campoMappingExcelTypeMap,
			Map<String, String> campoMappingFormatoMap, Integer cantidadData, Map<String, Object> parametroMap,
			Map<String, Character> configuracionTramaDetalleMap) throws Exception {
		parametroMap.put("filaData", filaData);
		parametroMap.put("cantidadData", cantidadData);
		return TransferDataUtil.transferObjetoEntityTXTMapDTO(campoMappingExcelMap, br, campoMappingExcelTypeMap,
				campoMappingFormatoMap, parametroMap, configuracionTramaDetalleMap);
	}

	public static List<Map<String, ValueDataVO>> transferObjetoEntityCoordenadaTXTMapDTO(BufferedReader br,
			int filaData, Map<String, Object> campoMappingExcelMap, Map<String, String> campoMappingExcelTypeMap,
			Map<String, String> campoMappingFormatoMap, Integer cantidadData, boolean isCabecera,
			String delimitadorData, Map<String, Object> parametroMap,
			Map<String, Character> configuracionTramaDetalleMap) throws Exception {
		parametroMap.put("filaData", filaData);
		parametroMap.put("cantidadData", cantidadData);
		parametroMap.put("delimitadorData", delimitadorData);
		return TransferDataUtil.transferObjetoEntityCoordenadaTXTMapDTO(campoMappingExcelMap, br,
				campoMappingExcelTypeMap, campoMappingFormatoMap, isCabecera, parametroMap,
				configuracionTramaDetalleMap);
	}

	public static List<Map<String, ValueDataVO>> transferObjetoEntityTXTMapDTO(BufferedReader br, int filaData,
			Map<String, Object> campoMappingExcelMap, Map<String, String> campoMappingExcelTypeMap,
			Map<String, String> campoMappingFormatoMap, String txtSplitBy, Integer cantidadData,
			Map<String, Object> parametroMap, Map<String, Character> configuracionTramaDetalleMap) throws Exception {
		parametroMap.put("filaData", filaData);
		parametroMap.put("txtSplitBy", txtSplitBy);
		parametroMap.put("cantidadData", cantidadData);
		return TransferDataUtil.transferObjetoEntityTXTSeparadorMapDTO(campoMappingExcelMap, br,
				campoMappingExcelTypeMap, campoMappingFormatoMap, parametroMap, configuracionTramaDetalleMap);
	}

	public static BufferedReader leerTXT(String pathFile, boolean imprimirLog) {
		BufferedReader resultado = null;
		String encoding = "";
		try {
			encoding = detectarCodificacion(pathFile);
			if (!StringUtil.isNullOrEmpty(encoding)) {
				if (imprimirLog) {
					log.error("leerTXT.linea encoding del  " + pathFile + " es " + encoding);
				}
				resultado = new BufferedReader(new InputStreamReader(new FileInputStream(pathFile), encoding));
			} else {
				log.error("leerTXT.linea no se encontro encoding para el archivo " + pathFile);
				log.error("leerTXT.linea se procedera a leer el archivo con el encoding por defecto ");
				resultado = new BufferedReader(new InputStreamReader(new FileInputStream(pathFile)));
			}
		} catch (Exception e) {
			log.error("Error ", e);
		}
		if (imprimirLog) {
			try {
				String temp = "";
				int linea = 1;
				BufferedReader resultadoTemp = null;
				resultadoTemp = new BufferedReader(new InputStreamReader(new FileInputStream(pathFile)));
				while ((temp = resultadoTemp.readLine()) != null) {
					Charset.forName(encoding).encode(temp);
					log.error("leerTXT.linea " + linea + temp);
					linea++;
				}
			} catch (Exception e) {
				log.error("leerTXT.Error en leer buffer del log " + e.getMessage());
			}
		}
		return resultado;
	}

	public static BufferedReader leerTXT(byte[] datosArchivo) {
		BufferedReader resultado = null;
		String encoding = "";
		try {
			encoding = null;// detectarCodificacion(pathFile);
			if (!StringUtil.isNullOrEmpty(encoding)) {
				resultado = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(datosArchivo), encoding));
			} else {
				resultado = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(datosArchivo)));
			}
		} catch (Exception e) {
			log.error("Error ", e);
		}
		return resultado;
	}

	public static String detectarCodificacion(String strRutaArchivoP) {
		byte[] buf;
		java.io.FileInputStream fis;
		UniversalDetector detector;
		int nread;
		String encoding = "";
		try {
			buf = new byte[4096];
			fis = new java.io.FileInputStream(strRutaArchivoP);
			detector = new UniversalDetector(null);
			while ((nread = fis.read(buf)) > 0 && !detector.isDone()) {
				detector.handleData(buf, 0, nread);
			}
			detector.dataEnd();
			encoding = detector.getDetectedCharset();
			detector.reset();
			fis = null;
			buf = null;
			return encoding;
		} catch (IOException e) {
			log.error("Error en - Clase(" + new Exception().getStackTrace()[0].getClassName() + ") " + "- Método("
					+ new Exception().getStackTrace()[0].getMethodName() + ") \n" + e);
		}
		return encoding;
	}
}