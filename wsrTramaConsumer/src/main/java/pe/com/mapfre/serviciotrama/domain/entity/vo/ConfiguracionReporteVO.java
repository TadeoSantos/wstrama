package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.BasePaginator;

/**
 * La Class ConfiguracionReporteVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ConfiguracionReporteVO extends BasePaginator implements Serializable{

	private static final long serialVersionUID = 1L;
	   
    /** El id destinatario. */
    private Long idReporte;
   
    /** El nombre destinatario. */
    private String descripcionReporte;
    
    public  ConfiguracionReporteVO(){
    	
    }

	public ConfiguracionReporteVO(Long idReporte, String descripcionReporte){
		this.idReporte = idReporte;
		this.descripcionReporte = descripcionReporte;
	}
	
}
