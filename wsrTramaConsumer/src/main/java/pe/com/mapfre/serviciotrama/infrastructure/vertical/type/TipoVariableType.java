package  pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class TipoVariableType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum TipoVariableType {

    /** El TRAMA_SOLICITUD. */
 	CONSTANTE("C" , "tipoVariable.constante"),
	
    /** El TRAMA_RESPUESTA_ERROR. */
 	PARAMETRO("P" , "tipoVariable.parametro"),
	
    /** El TRAMA_RESPUESTA_SOLICITUD. */
 	FECHA("F" , "tipoVariable.fecha"),
 	
 	/** El FECHA_CALCULADA */
 	FECHA_CALCULADA("A" , "tipoVariable.fechaCalculada");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, TipoVariableType> LOO_KUP_MAP = new HashMap<String, TipoVariableType>();
	
	static {
		for (TipoVariableType s : EnumSet.allOf(TipoVariableType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo tipo trama type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private TipoVariableType(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the tipo trama type
	 */
	public static TipoVariableType get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
