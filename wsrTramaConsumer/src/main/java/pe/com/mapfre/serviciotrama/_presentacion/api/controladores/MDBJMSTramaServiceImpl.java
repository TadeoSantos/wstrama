package pe.com.mapfre.serviciotrama._presentacion.api.controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.log4j.Logger;

import pe.com.mapfre.serviciotrama.application.service.ITramaAppService;
import pe.com.mapfre.serviciotrama.domain.entity.vo.ResultadoProcesoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.TramaVO;

/**
 * La Class MDBJMSTramaServiceImpl.
 * <ul>
 * <li>Copyright 2014 MAPFRE - mapfre. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author Tecnocom.
 * @version 1.0, Tue Apr 29 19:20:13 COT 2014
 * @since PWR v1.0
 */
@MessageDriven(activationConfig = {
		@ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
		@ActivationConfigProperty(propertyName = "destination", propertyValue = ConfiguracionJMSUtil.QUEUE_TRAMA_CONTROL_NAME),
		@ActivationConfigProperty(propertyName = "transactionTimeout", propertyValue = ConfiguracionJMSUtil.TRANSACCTION_TIMEOUT),
		@ActivationConfigProperty(propertyName = "maxSession", propertyValue = ConfiguracionJMSUtil.MAXIMA_INSTANCIA_COLA) })
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class MDBJMSTramaServiceImpl implements MessageListener {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	@EJB
	private ITramaAppService servicioApp;

	/**
	 * Instancia un nuevo proceso seleccion ficha service impl.
	 */
	public MDBJMSTramaServiceImpl() {
	}

	public void onMessage(Message aMessage) {
		
		try {
			@SuppressWarnings("unused")
			List<ResultadoProcesoConfiguracionTramaVO> resultado = new ArrayList<>();
			if (aMessage instanceof ObjectMessage) {
				ObjectMessage objMessage = (ObjectMessage) aMessage;
				Serializable serialObj = objMessage.getObject();
				if (serialObj instanceof TramaVO) {
					servicioApp.procesarConfiguracionTrama((TramaVO) serialObj);
					System.out.print("Resultado");
				}
			}
		} catch (Exception e) {
			logger.error("Error ", e);
		}

	}

}