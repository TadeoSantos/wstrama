package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

/**
 * La Class BaseTransfer.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.ScriptSqlResulJDBCVO;

public class BaseTransfer {
	protected <T> T to(Object ressul, Class<T> entityClassEntity, boolean isFiltro, String... entityClasess) {
		return TransferDataUtil.to(ressul, entityClassEntity, isFiltro, entityClasess);
	}

	protected <T> T to(Object ressul, Class<T> entityClassEntity, String... entityClasess) {
		return TransferDataUtil.to(ressul, entityClassEntity, false, entityClasess);
	}

	protected <T> T toDTO(Object ressul, Class<T> entityClassEntity, String... entityClasess) {
		return TransferDataUtil.toDTO(ressul, entityClassEntity, entityClasess);
	}

	protected <T, E> List<T> toList(List<E> ressul, Class<T> entityClassEntity, String... entityClasess) {
		return TransferDataUtil.toList(ressul, entityClassEntity, entityClasess);
	}

	protected <T, E> List<T> toListEntity(List<E> ressulList, Class<T> entityClassEntity, boolean isFiltro,
			String... entityClasess) {
		return TransferDataUtil.toListEntity(ressulList, entityClassEntity, isFiltro, entityClasess);
	}

	protected <T, E> List<T> toListEntity(List<E> ressulList, Class<T> entityClassEntity, String... entityClasess) {
		return TransferDataUtil.toListEntity(ressulList, entityClassEntity, false, entityClasess);
	}

	protected Map<String, Object> toVOMap(Object ressul) {
		return TransferDataUtil.toVOMap(ressul);
	}

	public Map<String, Object> toMap(Object ressul, boolean isNative, boolean isCargarManyToOne) {
		return TransferDataUtil.toMap(ressul, isNative, isCargarManyToOne);
	}

	public Map<String, Object> toEntityMap(Object entity) {
		return TransferDataUtil.toEntityMap(entity);
	}

	public <T> T toEntityVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity) {
		return TransferDataUtil.toEntityVO(scriptSqlResulJDBCVO, entityClassEntity);
	}

	public static <T> List<T> toEntityListVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity) {
		return TransferDataUtil.toEntityListVO(scriptSqlResulJDBCVO, entityClassEntity);
	}

	public <T> List<T> toEntityListVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity,
			Map<String, String> formatoFechaMap) {
		return TransferDataUtil.toEntityListVO(scriptSqlResulJDBCVO, entityClassEntity, formatoFechaMap);
	}

	public Map<String, Map<String, Object>> toFiltroMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir, String nombreClase, Map<String, String> fechaFormatoMap) {
		return TransferDataUtil.toFiltroMap(ressul, listaObjeto, listaAtributo, isExcluir, nombreClase,
				fechaFormatoMap);
	}

	public Map<String, Map<String, Object>> toVOMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir) {
		return TransferDataUtil.toVOMap(ressul, listaObjeto, listaAtributo, isExcluir);
	}

	public <T> Map<String, Map<String, Object>> toVOMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir, String claseNamePadre, String grupoList) {
		return TransferDataUtil.toVOMap(ressul, listaObjeto, listaAtributo, isExcluir, claseNamePadre, grupoList);
	}

	public static <T> T toVOTrama(Map<String, Map<String, Object>> listaObjectValueMap, Class<T> entityClass,
			String grupoList) {
		return TransferDataUtil.transferObjetoVOTrama(listaObjectValueMap, entityClass, grupoList);
	}
	
	public static <T> T toRest(@Context UriInfo info, Class<T> entityClassDTO) {
		return TransferDataUtil.toRestDTO(info, entityClassDTO);
	}

}
