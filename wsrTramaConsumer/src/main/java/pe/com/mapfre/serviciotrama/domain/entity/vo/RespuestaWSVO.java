package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.List;

/**
 * La Class RespuestaWSVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class RespuestaWSVO<T> implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La lista resultado. */
	private List<T> listaResultado;
	
	/** La objeto resultado. */
	private T objetoResultado;
	
	/** La codigo error. */
	private String codigoError;
	
	/** La mensaje error. */
	private String mensajeError;
	
	/** La is error. */
	private boolean isError;
	private int contador;
	
	
	
	/**
	 * Instancia un nuevo resultado wsvo.
	 */
	public RespuestaWSVO() {
		super();
	}

   public boolean isData() {
	   return contador > 0;
   }
	/**
	 * Instancia un nuevo resultado wsvo.
	 *
	 * @param listaResultado el lista resultado
	 * @param objetoResultado el objeto resultado
	 * @param codigoError el codigo error
	 * @param mensajeError el mensaje error
	 * @param isError el is error
	 */
	public RespuestaWSVO(List<T> listaResultado, T objetoResultado,
			String codigoError, String mensajeError, boolean isError) {
		super();
		this.listaResultado = listaResultado;
		this.objetoResultado = objetoResultado;
		this.codigoError = codigoError;
		this.mensajeError = mensajeError;
		this.isError = isError;
	}


	/**
	 * Obtiene lista resultado.
	 *
	 * @return lista resultado
	 */
	public List<T> getListaResultado() {
		return listaResultado;
	}


	/**
	 * Establece el lista resultado.
	 *
	 * @param listaResultado el new lista resultado
	 */
	public void setListaResultado(List<T> listaResultado) {
		this.listaResultado = listaResultado;
	}

	public T getObjetoResultado() {
		if (listaResultado != null && listaResultado.size() > 0) {
			objetoResultado = listaResultado.get(0);
		} 
		return objetoResultado;
	}


	public void setObjetoResultado(T objetoResultado) {
		this.objetoResultado = objetoResultado;
	}


	/**
	 * Obtiene codigo error.
	 *
	 * @return codigo error
	 */
	public String getCodigoError() {
		return codigoError;
	}


	/**
	 * Establece el codigo error.
	 *
	 * @param codigoError el new codigo error
	 */
	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}


	/**
	 * Obtiene mensaje error.
	 *
	 * @return mensaje error
	 */
	public String getMensajeError() {
		return mensajeError;
	}


	/**
	 * Establece el mensaje error.
	 *
	 * @param mensajeError el new mensaje error
	 */
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}


	/**
	 * Comprueba si es error.
	 *
	 * @return true, si es error
	 */
	public boolean isError() {
		return isError;
	}


	/**
	 * Establece el error.
	 *
	 * @param isError el new error
	 */
	public void setError(boolean isError) {
		this.isError = isError;
	}


	public int getContador()
	{
		return contador;
	}


	public void setContador(int contador)
	{
		this.contador = contador;
	}
	
}