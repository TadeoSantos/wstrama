package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.excel;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

/**
 * La Class ExcelUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class ExcelUtil {

	/**
	 * Logger para el registro de errores.
	 */
	
	private static final Logger log = Logger.getLogger(ExcelUtil.class);

	public static <T> List<T> transferObjetoEntityExcelDTO(byte[] datosArchivo, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Class<T> entityClassDTO) throws Exception {
		return TransferDataUtil.transferObjetoEntityExcelDTO(campoMappingExcelMap, leerExcel(datosArchivo),
				entityClassDTO, hoja, filaData);
	}

	public static <T> List<T> transferObjetoEntityExcelDTO(byte[] datosArchivo, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Map<String, String> campoMappingFormatoMap,
			Map<String, Object> parametroMap, Class<T> entityClassDTO) throws Exception {
		return TransferDataUtil.transferObjetoEntityExcelDTO(campoMappingExcelMap, leerExcel(datosArchivo),
				campoMappingFormatoMap, parametroMap, entityClassDTO, hoja, filaData);
	}

	public static <T> List<T> transferObjetoEntityExcelAllDTO(byte[] datosArchivo, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Map<String, String> campoMappingFormatoMap,
			Map<String, Object> parametroMap, Class<T> entityClassDTO) throws Exception {
		try {
			return transferObjetoEntityExcelXlsxDTO(datosArchivo, hoja, filaData, campoMappingExcelMap,
					campoMappingFormatoMap, parametroMap, entityClassDTO);
		} catch (Exception e) {
			return transferObjetoEntityExcelDTO(datosArchivo, hoja, filaData, campoMappingExcelMap,
					campoMappingFormatoMap, parametroMap, entityClassDTO);
		}

	}

	public static <T> List<T> transferObjetoEntityExcelAllDTO(byte[] datosArchivo, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Class<T> entityClassDTO) throws Exception {
		try {
			return transferObjetoEntityExcelDTO(datosArchivo, hoja, filaData, campoMappingExcelMap, entityClassDTO);
		} catch (Exception e) {
			return transferObjetoEntityExcelXlsxDTO(datosArchivo, hoja, filaData, campoMappingExcelMap, entityClassDTO);
		}
	}

	public static <T> List<T> transferObjetoEntityExcelDTO(HSSFWorkbook hSSFWorkbook, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Class<T> entityClassDTO) throws Exception {
		return TransferDataUtil.transferObjetoEntityExcelDTO(campoMappingExcelMap, hSSFWorkbook, entityClassDTO, hoja,
				filaData);
	}

	public static <T> List<T> transferObjetoEntityExcelXlsxDTO(byte[] datosArchivo, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Class<T> entityClassDTO) throws Exception {
		return TransferDataUtil.transferObjetoEntityExcelXlsxDTO(campoMappingExcelMap, leerExcelXLSX(datosArchivo),
				entityClassDTO, hoja, filaData);
	}

	public static <T> List<T> transferObjetoEntityExcelXlsxDTO(byte[] datosArchivo, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Map<String, String> campoMappingFormatoMap,
			Map<String, Object> parametroMap, Class<T> entityClassDTO) throws Exception {
		return TransferDataUtil.transferObjetoEntityExcelXlsxDTO(campoMappingExcelMap, campoMappingFormatoMap,
				parametroMap, leerExcelXLSX(datosArchivo), entityClassDTO, hoja, filaData);
	}

	public static <T> List<T> transferObjetoEntityExcelXlsxDTO(XSSFWorkbook xSSFWorkbook, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Class<T> entityClassDTO) throws Exception {
		return TransferDataUtil.transferObjetoEntityExcelXlsxDTO(campoMappingExcelMap, xSSFWorkbook, entityClassDTO,
				hoja, filaData);
	}

	public static <T> List<T> transferObjetoEntityExcelXlsxDTO(XSSFWorkbook xSSFWorkbook, int hoja, int filaData,
			Map<String, Integer> campoMappingExcelMap, Map<String, String> campoMappingFormatoMap,
			Map<String, Object> parametroMap, Class<T> entityClassDTO) throws Exception {
		return TransferDataUtil.transferObjetoEntityExcelXlsxDTO(campoMappingExcelMap, campoMappingFormatoMap,
				parametroMap, xSSFWorkbook, entityClassDTO, hoja, filaData);
	}

	public static List<Map<String, ValueDataVO>> transferObjetoEntityExcelMapDTO(HSSFWorkbook hSSFWorkbook, int hoja,
			int filaData, Map<String, Object> campoMappingExcelMap, Map<String, String> campoMappingExcelTypeMap,
			Map<String, String> campoMappingFormatoMap, Integer cantidadData, Map<String, Object> parametroMap,
			Map<String, Character> configuracionTramaDetalleMap) throws Exception {
		parametroMap.put("hoja", hoja);
		parametroMap.put("filaData", filaData);
		parametroMap.put("cantidadData", cantidadData);
		return TransferDataUtil.transferObjetoEntityExcelMapDTO(campoMappingExcelMap, hSSFWorkbook,
				campoMappingExcelTypeMap, campoMappingFormatoMap, parametroMap, configuracionTramaDetalleMap);
	}

	public static List<Map<String, ValueDataVO>> transferObjetoEntityExcelXlsxMapDTO(XSSFWorkbook xSSFWorkbook,
			int hoja, int filaData, Map<String, Object> campoMappingExcelMap,
			Map<String, String> campoMappingExcelTypeMap, Map<String, String> campoMappingFormatoMap,
			Integer cantidadData, Map<String, Object> parametroMap,
			Map<String, Character> configuracionTramaDetalleMap) throws Exception {
		parametroMap.put("hoja", hoja);
		parametroMap.put("filaData", filaData);
		parametroMap.put("cantidadData", cantidadData);
		return TransferDataUtil.transferObjetoEntityExcelXlsxMapDTO(campoMappingExcelMap, xSSFWorkbook,
				campoMappingExcelTypeMap, campoMappingFormatoMap, parametroMap, configuracionTramaDetalleMap);
	}

	public static void defaultLocale() {
		Locale ES = new Locale("es", "PE");
		Locale.setDefault(ES);
	}

	public static void defaultLocaleProcess() {
		TransferDataUtil.defaultLocaleProcess();
	}

	public static HSSFWorkbook leerExcel(File rutaArchivo) throws Exception {
		defaultLocale();
		FileInputStream fileInputStream = new FileInputStream(rutaArchivo);
		HSSFWorkbook workBook = new HSSFWorkbook(fileInputStream);
		return workBook;
	}

	public static HSSFWorkbook leerExcel(byte[] datosArchivo) throws Exception {
		defaultLocale();
		InputStream fileExcel = new ByteArrayInputStream(datosArchivo);
		/** Create a workbook desde InputStream **/
		HSSFWorkbook workBook = new HSSFWorkbook(fileExcel);
		return workBook;
	}

	public static XSSFWorkbook leerExcelXLSX(byte[] datosArchivo) throws Exception {
		defaultLocale();
		InputStream fileExcel = new ByteArrayInputStream(datosArchivo);
		/** Create a workbook desde InputStream **/
		XSSFWorkbook workBook = new XSSFWorkbook(fileExcel);
		return workBook;
	}

	public static XSSFWorkbook leerExcelXlsx(File rutaArchivo) throws Exception {
		defaultLocale();
		FileInputStream fileInputStream = new FileInputStream(rutaArchivo);
		XSSFWorkbook workBook = new XSSFWorkbook(fileInputStream);
		return workBook;
	}

	public static SXSSFWorkbook leerExcelsXlsx(File rutaArchivo) throws Exception {
		XSSFWorkbook xSSFWorkbook = leerExcelXlsx(rutaArchivo);
		SXSSFWorkbook workBook = new SXSSFWorkbook(xSSFWorkbook);
		return workBook;
	}
}