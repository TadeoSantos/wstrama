package pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class TipoDatoFlujoType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum TipoDatoFlujoType {

    /** El FECHA. */
 	FECHA(1L , "tipoDatoFlujo.fecha"),
	
    /** El NUMERICO. */
 	NUMERICO(2L , "tipoDatoFlujo.numerico"),
	
    /** El TEXTO. */
 	TEXTO(3L , "tipoDatoFlujo.texto"),
	
    /** El BOOLEANO. */
 	BOOLEANO(4L , "tipoDatoFlujo.booleano");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<Long, TipoDatoFlujoType> LOO_KUP_MAP = new HashMap<Long, TipoDatoFlujoType>();
	
	static {
		for (TipoDatoFlujoType s : EnumSet.allOf(TipoDatoFlujoType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private Long key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo tipo dato flujo type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private TipoDatoFlujoType(Long key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the tipo dato flujo type
	 */
	public static TipoDatoFlujoType get(Long key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public Long getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
