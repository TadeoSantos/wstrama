package pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteTypeUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ResourceUtil;

/**
 * La Class ClaseErrorOrquestadorType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum ClaseErrorOrquestadorType {
	
	
	//Errores Tecnicos
	BASE_DATOS("BD", "Base de Datos"),
	
	SERVICIO("SV", "Servicio"),
	
	FTP("FTP", "FTP"),
	
	OTROS("OT", "Otros"),
	//Errores Negocio
	LECTURA("1","Lectura"),
	
	VALIDACION_PRE_EMISION("2","Validación Pre Emisión"),
	
	POST_EMISION("3","Post Emisión"),
	
	IMPRESION("4","Impresión"),
	//Errores sin trama
	SIN_TRAMAS("S","-"),
	
	ERROR_NO_GENERO_NUMERO_LOTE("TEC001", "Base de Datos"),
	
	ERROR_GENERAL_NO_CONTROLADO_ORQE001("ORQE001", "mensaje no controlado"),
	
	ERROR_GENERAL_NO_CONTROLADO_WEBSERVICE_ENVIO_CORREO_ORQE002("ORQE002", "mensaje no controlado Webservice envioCorreo"),
	
	ERROR_GENERAL_NO_CONTROLADO_WEBSERVICE_ENVIO_CORREO_CONSOLIDADO_ORQE003("ORQE003", "mensaje no controlado Webservice envioCorreoConsolidado"),
	
	TRAMA_PROCESADA_ANTERIORMENTE("LEC_E001", "El archivo ya fue procesado"),
	
	NO_CONEXION_FTP_ARCHIVO("LEC_E002", "No se pudo conectar al FTP"),
	
	ARCHIVO_OBLIGATORIO_PROCESADO_ERROR("LEC_E003", "Error : Archivos obligatorio procesado con error idConfiguradorTrama"),
	
	MENSAJE_EJECUCION_PROCESO("ORMG001", "controlProceso.grilla.accion.mensajeEjecucion"),
	
	MENSAJE_NO_SE_ENCONTRO_JUEGO("ORMG999", "No se encontro ningun juego para procesar");   //CODIGO RESERVADO PARA LA LOGICA DEL NEGOCIO SIN TRAMA
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, ClaseErrorOrquestadorType> LOO_KUP_MAP = new HashMap<String, ClaseErrorOrquestadorType>();

	static {
		for (ClaseErrorOrquestadorType s : EnumSet.allOf(ClaseErrorOrquestadorType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;

	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo accion type.
	 * 
	 * @param key
	 *            el key
	 * @param value
	 *            el value
	 */
	private ClaseErrorOrquestadorType(String key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * Gets the.
	 * 
	 * @param key
	 *            el key
	 * @return the accion type
	 */
	public static ClaseErrorOrquestadorType get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 * 
	 * @return key
	 */
	public String getKey() {
		return key;
	}
	
	public String getValor()  {
		return value;
	}
	
	/**
	 * Obtiene value.
	 * 
	 * @return value
	 */
	public String getValue() {
		return  ResourceUtil.getString(ConstanteTypeUtil.BUNDLE_NAME_ORQUESTADOR_ERROR_TYPE, key); 
	}
	
	/**
	 * Obtiene value.
	 *
	 * @param parametros el parametros
	 * @return value
	 */
	public String getValue(String...parametros ) {
		return  ResourceUtil.getString(ConstanteTypeUtil.BUNDLE_NAME_ORQUESTADOR_ERROR_TYPE, key, parametros); 
	}

}
