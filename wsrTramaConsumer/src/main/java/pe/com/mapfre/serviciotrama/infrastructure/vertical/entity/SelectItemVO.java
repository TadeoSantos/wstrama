package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class SelectItemVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class SelectItemVO implements Serializable {
 
	/** La Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** El value. */
	private Object id;
	
	/** El nombre. */
	private String nombre;
	
	/** La descripcion. */
	private String descripcion;

	/**
	 * Instancia un nuevo select item vo.
	 */
	public SelectItemVO() {
		super();
	}

	/**
	 * Instancia un nuevo select item vo.
	 *
	 * @param id el id
	 * @param nombre el nombre
	 * @param descripcion el descripcion
	 */
	public SelectItemVO(Object id, String nombre,String descripcion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	
	
	/**
	 * Instancia un nuevo select item vo.
	 *
	 * @param id el id
	 * @param nombre el nombre
	 * @param descripcion el descripcion
	 */
	public SelectItemVO(Object id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = nombre;
	}
	
}