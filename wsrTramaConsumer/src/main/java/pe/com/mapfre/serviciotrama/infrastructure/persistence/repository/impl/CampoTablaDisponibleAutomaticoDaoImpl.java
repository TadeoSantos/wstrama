package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.ConstanteQueryTron2000Util;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.SqlMapingUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.CampoTablaVO;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.CampoTablaDisponibleAutomaticoDaoLocal;

/**
 * La Class CampoTablaDisponibleDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class CampoTablaDisponibleAutomaticoDaoImpl extends GenericRepository<Object, CampoTablaVO>
		implements CampoTablaDisponibleAutomaticoDaoLocal {

	@Override
	public Map<String, Map<String, CampoTablaVO>> obtenerCampoTablaDisponibleMap(List<String> listaNombreEsquemaTabla) {
		if (listaNombreEsquemaTabla == null || listaNombreEsquemaTabla.size() == 0) {
			return new HashMap<String, Map<String, CampoTablaVO>>();
		}
		List<String> listaNombreTabla = new ArrayList<String>();
		List<String> listaEsquema = new ArrayList<String>();
		for (String string : listaNombreEsquemaTabla) {
			string = string.replace(".", ";");
			String dataTable[] = string.split(";");
			if (!listaNombreTabla.contains(dataTable[1])) {
				listaNombreTabla.add(dataTable[1]);
			}
			if (!listaEsquema.contains(dataTable[0])) {
				listaEsquema.add(dataTable[0]);
			}
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder(obtenerColumnaSQL());
		jpaql.append(" where owner       in (:listaOwner) ");
		jpaql.append(" and  table_name  in (:listaNombreTabla)");
		parametros.put("listaNombreTabla", listaNombreTabla);
		parametros.put("listaOwner", listaEsquema);
		Query query = createNativeQuery(jpaql.toString(), parametros);
		List<Object[]> resultadoTemp = query.getResultList();
		return parsearColumnaMap(resultadoTemp);
	}

	/**
	 * Obtener columna sql.
	 *
	 * @return the string
	 */
	private String obtenerColumnaSQL() {
		StringBuilder jpaql = new StringBuilder();
		jpaql.append(SqlMapingUtil
				.obtenerSqlSentenciaSystem(ConstanteQueryTron2000Util.SQL_SYSTEM_DATABASE_OBTENER_COLUMNA_DISPONIBLE));
		return jpaql.toString();
	}

	/**
	 * Parsear columna map.
	 *
	 * @param resultadoTemp
	 *            el resultado temp
	 * @return the map
	 */
	private Map<String, Map<String, CampoTablaVO>> parsearColumnaMap(List<Object[]> resultadoTemp) {
		Map<String, Map<String, CampoTablaVO>> resultado = new HashMap<String, Map<String, CampoTablaVO>>();
		for (Object[] objects : resultadoTemp) {
			String key = objects[5].toString() + "." + objects[0].toString();
			CampoTablaVO campoTablaVO = new CampoTablaVO();
			campoTablaVO.setNombreCampo(objects[1] + "");
			campoTablaVO.setType(objects[3] + "");
			campoTablaVO.setLength(objects[2] + "");
			campoTablaVO.setIsNull(objects[4] + "");

			if (!resultado.containsKey(key)) {
				Map<String, CampoTablaVO> value = new HashMap<String, CampoTablaVO>();
				value.put(campoTablaVO.getNombreCampo(), campoTablaVO);
				resultado.put(key, value);
			} else {
				Map<String, CampoTablaVO> value = resultado.get(key);
				value.put(campoTablaVO.getNombreCampo(), campoTablaVO);
				resultado.put(key, value);
			}

		}
		return resultado;
	}

}