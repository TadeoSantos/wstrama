package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ConfiguracionTramaError.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_CONFIG_TRA_ERROR", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ConfiguracionTramaError implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id configuracion trama error. */
    @Id
    @Column(name = "N_ID_CONF_TRAMA_ERROR" , length = 18)
    private Long idConfiguracionTramaError;
   
    /** El configuracion trama detalle. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_CONFIG_TRAMA_DET", referencedColumnName = "N_ID_CONFIG_TRAMA_DET")
    private ConfiguracionTramaDetalle configuracionTramaDetalle;
   
    /** El nombre campo. */
    @Column(name = "C_NOMBRE_CAMPO" , length = 150)
    private String nombreCampo;
   
    /**
     * Instancia un nuevo configuracion trama error.
     */
    public ConfiguracionTramaError() {
    }
   
   
    /**
     * Instancia un nuevo configuracion trama error.
     *
     * @param idConfiguracionTramaError el id configuracion trama error
     * @param configuracionTramaDetalle el configuracion trama detalle
     * @param nombreCampo el nombre campo
     */
    public ConfiguracionTramaError(Long idConfiguracionTramaError, ConfiguracionTramaDetalle configuracionTramaDetalle,String nombreCampo ) {
        super();
        this.idConfiguracionTramaError = idConfiguracionTramaError;
        this.configuracionTramaDetalle = configuracionTramaDetalle;
        this.nombreCampo = nombreCampo;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idConfiguracionTramaError == null) ? 0 : idConfiguracionTramaError.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ConfiguracionTramaError other = (ConfiguracionTramaError) obj;
        if (idConfiguracionTramaError == null) {
            if (other.idConfiguracionTramaError != null) {
                return false;
            }
        } else if (!idConfiguracionTramaError.equals(other.idConfiguracionTramaError)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ConfiguracionTramaError [idConfiguracionTramaError=" + idConfiguracionTramaError + "]";
    }
   
}