package pe.com.mapfre.serviciotrama.domain.service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfigUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfiguracionTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.excel.DataExportExcelPersonalizadoUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.excel.ExcelUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ftp.FTPClienteUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms.UUIDUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ExcelHederDataVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.FTPVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

/**
 * La Class ParticionarArchivoExcel.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */

public class ParticionarArchivoExcel {

	private static final String ARTIFICIO_CAMPO = "${CA}";
	private static final String ARTIFICIO_COLUMNA = "@@";

	public static void runParticionar() {
//	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Map<String,String> nomenclaturaMap = new HashMap<String, String>(); 
			nomenclaturaMap.put("TRON2000.TASMEDGP_MPE", "TASMEDGP_ENEL_1.xls");
			nomenclaturaMap.put("TRON2000.TASMEDRV", "TASMEDRV_ENEL_2.xls");
			nomenclaturaMap.put("TRON2000.TASMEDBV", "TASMEDBV_ENEL_3.xls");
			FTPVO fTPVO = new FTPVO();
			fTPVO.setPort(21);
			fTPVO.setServer("216.177.223.87");
			fTPVO.setUsuario("tecnocom");
			fTPVO.setClave("t3cn0.2016");
			fTPVO.setRutaFTP("/mytron_pase/tramas/");
			fTPVO.setRutaLocal("C:/svr/mapfre/pwr/ByteBuffer/");
			fTPVO.setNombreArchivoFTP("ENEL_Asistencias_Altas_20180627_114000.xls");
			fTPVO.setNombreArchivoLocal("ENEL_Asistencias_Altas_20180627_114000.xls");//boorar linea
			fTPVO.setIdJuegoTrama(200L);
			Map<String, Object> parametrosMap = new HashMap<String, Object>();
			parametrosMap.put(ConstanteConfiguracionTramaUtil.ES_IMPRIMIR, "false");
			//boolean respuestaFTPVO = FTPClienteUtil.descargarArchivo(fTPVO, parametrosMap );
			//if (respuestaFTPVO) {
			if (true) {
				ParticionarArchivoExcel.ejecutarParticionExcel(nomenclaturaMap,fTPVO);
			} else {
				System.out.println("No puedo descargar");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void ejecutarParticionExcel(Map<String,String> nomenclaturaMap,FTPVO fTPVO) {
		try {
			//Inicio
			//1.-Leer el exel
			 File rutaArchivo = new File(fTPVO.getRutaLocal() + fTPVO.getNombreArchivoLocal());
			 //Almacenar Excel en libro HSSFWorkbook
			 HSSFWorkbook hSSFWorkbook = ExcelUtil.leerExcel(rutaArchivo);
			 Map<String,Integer> posicionPerMap = new HashMap<String, Integer>();
			 //Obtener lista de encabezados
			 List<ExcelHederDataVO> listaHeader = obtenerHeaderDGP();
			 
			 Map<String,String> listaSubirFTP =  new HashMap<String,String>(); 
			 String archivoNameDGP = UUIDUtil.generarElementUUID();//generar nombre en base a la nomenclatura
			 listaSubirFTP.put(archivoNameDGP  + ".xls",nomenclaturaMap.get("TRON2000.TASMEDGP_MPE"));
			 //2.-Particionar segun funcionalidad
			 generarExelTipo(posicionPerMap,listaHeader,null, null,hSSFWorkbook , archivoNameDGP,0 ,0, false);
			 
			 
			 listaHeader = obtenerHeaderDRV();
			
			 String archivoNameDRV = UUIDUtil.generarElementUUID();
			 listaSubirFTP.put(archivoNameDRV  + ".xls", nomenclaturaMap.get("TRON2000.TASMEDRV_MPE"));

			 List<String> listaHeaderDinamico = obtenerHeaderDRVDinamico();//11 encontrar el bloque dinamico
			 Map<String,String> equivalenciaMap = new HashMap<String, String>();
			 
			 posicionPerMap = new HashMap<String, Integer>();
			 posicionPerMap.put("NUM_UNICO_ITEM", 0);
			 posicionPerMap.put("NUM_ID_ADICIONAL", 1);
			 posicionPerMap.put("NUM_RIESGO", -1);
			 
			 generarExelTipo(posicionPerMap,listaHeader,listaHeaderDinamico,equivalenciaMap, hSSFWorkbook , archivoNameDRV ,11,25,true);
			 
			 String archivoNameDBV = UUIDUtil.generarElementUUID();
			 listaSubirFTP.put(archivoNameDBV + ".xls", nomenclaturaMap.get("TRON2000.TASMEDBV_MPE"));
			 listaHeader = obtenerHeaderDBV();
			 equivalenciaMap = new HashMap<String, String>();
			 listaHeaderDinamico = obtenerHeaderDBVDinamico();//5 encontrar el bloque dinamico
			 
			 posicionPerMap = new HashMap<String, Integer>();
			 posicionPerMap.put("NUM_UNICO_ITEM", 0);
			 posicionPerMap.put("NUM_ID_ADICIONAL", 1);
			 
			 generarExelTipo(posicionPerMap,listaHeader,listaHeaderDinamico,equivalenciaMap, hSSFWorkbook , archivoNameDBV,5,140, false);
			 
			 //3.-Subir al FTP los archivos particionados	//FTPUTIL subirarchivos
			 subirArchivoFTP(fTPVO, listaSubirFTP);
			 
			// resultado = isTrasnferenciaError;
			//Fin
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static void subirArchivoFTP(FTPVO fTPVO,Map<String,String> listaSubirFTPMap ) {
		for (Map.Entry<String, String> archivoFTP : listaSubirFTPMap.entrySet()) {
			 fTPVO.setEliminarArchivoTemp(false);
			 fTPVO.setRutaLocal(ConstanteConfigUtil.RUTA_RECURSOS_BYTE_BUFFER);
			 fTPVO.setNombreArchivoFTP(archivoFTP.getValue());
			 fTPVO.setNombreArchivoLocal(archivoFTP.getKey());
			 String isTrasnferenciaError = FTPClienteUtil.subirArchivo(fTPVO);
			 if (!isTrasnferenciaError.equals("")) {
				//log.error("ConvertirWordPDFUtil.error.subirArchivo no se pudo subir el archvio de la ruta " + nombreArchivo + " ==> " + isTrasnferenciaError);
			 } 
		}
		for (Map.Entry<String, String> archivoFTP : listaSubirFTPMap.entrySet()) {
			 File file = new File(ConstanteConfigUtil.RUTA_RECURSOS_BYTE_BUFFER + archivoFTP.getKey() );
			 file.delete();
		}
	}
	private static List<ExcelHederDataVO> obtenerHeaderDGP() {
		List<ExcelHederDataVO> listaHeader = new ArrayList<ExcelHederDataVO>();
		 listaHeader.add(new ExcelHederDataVO("NUM_UNICO_ITEM",0));//listaHeader.add("IdVenta");
		 listaHeader.add(new ExcelHederDataVO("NUM_ID_ADICIONAL",1));//listaHeader.add("NIE");
		 listaHeader.add(new ExcelHederDataVO("MCA_SEXO",8));//listaHeader.add("Sexo");
		 listaHeader.add(new ExcelHederDataVO("FEC_NACIMIENTO",10));//listaHeader.add("cl_FechaNacimiento");
		 listaHeader.add(new ExcelHederDataVO("NOM_DIRECCION",11));//listaHeader.add("Direccion");
		 listaHeader.add(new ExcelHederDataVO("COD_PROV",13));//listaHeader.add("ci_ciudad");
		 listaHeader.add(new ExcelHederDataVO("COD_LOCALIDAD",14));//listaHeader.add("distrito");
		 listaHeader.add(new ExcelHederDataVO("NUM_TELF_FIJO",15));//listaHeader.add("Telefono1");
		 listaHeader.add(new ExcelHederDataVO("NUM_TELF_MOVIL",17));//listaHeader.add("Celular");
		 listaHeader.add(new ExcelHederDataVO("TXT_EMAIL",18));//listaHeader.add("Mail");
		 
		 listaHeader.add(new ExcelHederDataVO("IMP_PRIMA_TOTAL",178));
		 listaHeader.add(new ExcelHederDataVO("FEC_EFEC_POLIZA",179));
		 listaHeader.add(new ExcelHederDataVO("MCA_VENTA_TELEF",185));
		 listaHeader.add(new ExcelHederDataVO("COD_PROD_SALUD",176));
		 
		 return listaHeader;
	}
	private static List<ExcelHederDataVO> obtenerHeaderDRV() {
		List<ExcelHederDataVO> listaHeader = new ArrayList<ExcelHederDataVO>();
		listaHeader.add(new ExcelHederDataVO("COD_PARENTESCO",11));//11
		listaHeader.add(new ExcelHederDataVO("TIP_DOCUM_ASEGURADO",6));//6
		listaHeader.add(new ExcelHederDataVO("COD_DOCUM_ASEGURADO",7));//7
		listaHeader.add(new ExcelHederDataVO("NOM_ASEGURADO",3));//3
		listaHeader.add(new ExcelHederDataVO("APE1_ASEGURADO",4));//4
		listaHeader.add(new ExcelHederDataVO("APE2_ASEGURADO",5));//5
		listaHeader.add(new ExcelHederDataVO("FEC_NACIMIENTO",10));//10
		listaHeader.add(new ExcelHederDataVO("MCA_SEXO",8));//8
		listaHeader.add(new ExcelHederDataVO("COD_PROD_SALUD",176));//176
		listaHeader.add(new ExcelHederDataVO("COD_PLAN",177));//177
	
		//Orden de pintar campos
		listaHeader.add(new ExcelHederDataVO("NUM_UNICO_ITEM",0));//0
		listaHeader.add(new ExcelHederDataVO("NUM_ID_ADICIONAL",1));//1
		listaHeader.add(new ExcelHederDataVO("NUM_RIESGO",-1));//178
		
		return listaHeader;
	}
	
	private static List<ExcelHederDataVO> obtenerHeaderDBV() { 
		List<ExcelHederDataVO> listaHeader = new ArrayList<ExcelHederDataVO>();
		
		listaHeader.add(new ExcelHederDataVO("NOM_BENEFICIARIO",133));
		listaHeader.add(new ExcelHederDataVO("APE1_BENEFICIARIO",134));
		listaHeader.add(new ExcelHederDataVO("APE2_BENEFICIARIO",135));
		listaHeader.add(new ExcelHederDataVO("COD_PARENTESCO",136));
		listaHeader.add(new ExcelHederDataVO("PCT_PARTICIPACION",137));
		
		listaHeader.add(new ExcelHederDataVO("COD_DOCUM_BENEFICIARIO",138));
		listaHeader.add(new ExcelHederDataVO("TIP_DOCUM_BENEFICIARIO",139));
		
		//Orden de pintar campos
		listaHeader.add(new ExcelHederDataVO("NUM_UNICO_ITEM",0));//0
		listaHeader.add(new ExcelHederDataVO("NUM_ID_ADICIONAL",1));//1
		return listaHeader;
	}
	private static List<String> obtenerHeaderDBVDinamico() {
		List<String> listaHeader = new ArrayList<String>();
		listaHeader.add("NUM_UNICO_ITEM@@${CA}_${i}");
		listaHeader.add("NUM_ID_ADICIONAL@@${CA}_${i}");
		//El orden que esta en el exel
		listaHeader.add("NOM_BENEFICIARIO${CA}_${i}");
		listaHeader.add("APE1_BENEFICIARIO${CA}_${i}");
		listaHeader.add("APE2_BENEFICIARIO${CA}_${i}");
		listaHeader.add("COD_PARENTESCO${CA}_${i}");
		listaHeader.add("PCT_PARTICIPACION${CA}${i}");
		listaHeader.add("COD_DOCUM_BENEFICIARIO${CA}${i}");
		listaHeader.add("TIP_DOCUM_BENEFICIARIO${CA}${i}");
		return listaHeader;
	}
	private static List<String> obtenerHeaderDRVDinamico() {
		//Los campos del bloque se aumentan con CA y los otros campos que no estan con @@
		
		List<String> listaHeader = new ArrayList<String>();
			listaHeader.add("NUM_UNICO_ITEM@@${CA}_${i}");
			listaHeader.add("NUM_ID_ADICIONAL@@${CA}_${i}");
			//El orden que esta en el exel
			listaHeader.add("COD_PARENTESCO${CA}_${i}" );//25
			listaHeader.add("TIP_DOCUM_ASEGURADO${CA}_${i}");//26
			listaHeader.add("COD_DOCUM_ASEGURADO${CA}_${i}");//27
			listaHeader.add("NOM_ASEGURADO${CA}_${i}");//28
			listaHeader.add("APE1_ASEGURADO${CA}_${i}");//29
			listaHeader.add("APE2_ASEGURADO${CA}_${i}");//30
			listaHeader.add("FEC_NACIMIENTO${CA}_${i}");//31
			listaHeader.add("MCA_SEXO${CA}_${i}");//32
			listaHeader.add("NUM_RIESGO@@${CA}_${i}");
		return listaHeader;
	}
	private static List<String> obtenerHeaderDinamico(int cantidadRegistro,List<String> listaHeaderDinamicoTemp) {
		List<String> listaHeader = new ArrayList<String>();
		for (int i = 1; i <= cantidadRegistro; i++) {
			for (String colummna : listaHeaderDinamicoTemp) {
				listaHeader.add(colummna.replace("${i}",  "" + i + ""));
			}
		}
		return listaHeader;
	}
	//generarExelTipo(3,posicionPerMap,listaHeader,listaHeaderDinamico,equivalenciaMap, hSSFWorkbook , archivoNameDRV ,11,25);
	private static void generarExelTipo(Map<String,Integer> posicionPerMap,List<ExcelHederDataVO> listaHeaderTemp,List<String> listaHeaderDinamicoTemp, Map<String,String> equivalenciaMap,HSSFWorkbook hSSFWorkbook,String archivoName, int bloque, int posicionDinamico, boolean isDrv) throws Exception {
		Map<String,String> planByPolizaMap = new HashMap<String, String>();
		Map<String,String> productoByPolizaMap = new HashMap<String, String>();
		Map<String,Integer> riesgosByPolizaMap = new HashMap<String, Integer>();
		List<String> listaHeaderDinamico = obtenerHeaderDinamico(bloque,listaHeaderDinamicoTemp); 
		 Map<String, Object> campoMappingPosicionMap = new HashMap<String, Object>();
		 Map<String, String> campoMappingFormatoMap = new HashMap<String, String>();
		 Map<String, String> campoMappingTypeMap = new HashMap<String, String>();
		 Map<String, Character> configuracionTramaDetalleMap = new HashMap<>();
		 List<String>  listaHeader = new ArrayList<String>();
		for (ExcelHederDataVO objHeader : listaHeaderTemp) {
			String atributo = objHeader.getNameHeader();
			campoMappingPosicionMap.put(atributo,objHeader.getPosicionCelda());
			listaHeader.add(atributo);
		 }
		for (Map.Entry<String, Object> objCampos : campoMappingPosicionMap.entrySet()) {
			 campoMappingTypeMap.put(objCampos.getKey(), "java.lang.String");//tipo
			 configuracionTramaDetalleMap.put(objCampos.getKey(), null);
		}
		Map<String, Object> parametroMap = new HashMap<String, Object>();
		List<Map<String,ValueDataVO>>  listaDataResulMap = ExcelUtil.transferObjetoEntityExcelMapDTO(hSSFWorkbook, 1, 2, campoMappingPosicionMap , campoMappingTypeMap ,campoMappingFormatoMap ,null,parametroMap ,configuracionTramaDetalleMap );
		if (isDrv) {  //tipo drv
			for (Map<String,ValueDataVO> dataMap : listaDataResulMap) {
				ValueDataVO valueData  = new ValueDataVO();
				valueData.setData("10");
				dataMap.put("COD_PARENTESCO", valueData);
				String key = (String)dataMap.get("NUM_UNICO_ITEM").getData();
				if (!planByPolizaMap.containsKey(key)) {
					planByPolizaMap.put(key, (String)dataMap.get("COD_PLAN").getData());
				}
				
				if (!productoByPolizaMap.containsKey(key)) {
					productoByPolizaMap.put(key, (String)dataMap.get("COD_PROD_SALUD").getData());
				}
			}
		}
		//empieza a traer data de la los campos dinamicos
		if (listaHeaderDinamico != null) {
			 Map<String, Object> campoMappingPosicionMapDinamico = new HashMap<String, Object>();
			 Map<String, String> campoMappingFormatoMapDinamico = new HashMap<String, String>();
			 Map<String, String> campoMappingTypeMapDinamico = new HashMap<String, String>();
			 Map<String, Character> configuracionTramaDetalleMapDinamico = new HashMap<>();
			int i = posicionDinamico;
			 for (String atributo : listaHeaderDinamico) {
				 if (!atributo.contains(ARTIFICIO_COLUMNA)) {
					 campoMappingPosicionMapDinamico.put(atributo,i);
					 i++;	
				 } else {
					 int lastIndexOf = atributo.lastIndexOf(ARTIFICIO_COLUMNA);
					 String keyArtificio =  atributo.substring(0, lastIndexOf);
					 int iPer = posicionPerMap.get(keyArtificio);
					 campoMappingPosicionMapDinamico.put(atributo,iPer);
				 }
			  }
			 for (Map.Entry<String, Object> objCampos : campoMappingPosicionMapDinamico.entrySet()) {
				 campoMappingTypeMapDinamico.put(objCampos.getKey(), "java.lang.String");
				 configuracionTramaDetalleMapDinamico.put(objCampos.getKey(), null);
			 }
			 parametroMap.put("NoValidarExcel", ""); 
			 List<Map<String,ValueDataVO>>  listaDataResulMapDinamico = ExcelUtil.transferObjetoEntityExcelMapDTO(hSSFWorkbook, 1, 2, campoMappingPosicionMapDinamico , campoMappingTypeMapDinamico ,campoMappingFormatoMapDinamico ,null,parametroMap ,configuracionTramaDetalleMapDinamico );
			for (Map<String, ValueDataVO> objMap : listaDataResulMapDinamico) {
				
				for (i = 1; i <= bloque; i++) {
					boolean agregarData = false;
					List<String> listaHeaderPlano = new ArrayList<String>();
					for (String colummna : listaHeaderDinamicoTemp) {
						listaHeaderPlano.add(colummna.replace("${i}",  "" + i + ""));
					}
					Map<String,ValueDataVO> dataValueMap = new HashMap<String, ValueDataVO>();
					int j = 0;
					for (String objKeyDinamico : listaHeaderPlano) {
						int lastIndexOf = objKeyDinamico.lastIndexOf(ARTIFICIO_CAMPO);
						if (objKeyDinamico.contains(ARTIFICIO_COLUMNA)) {
							lastIndexOf = lastIndexOf -2;
						}
						//asociar a campos para escribir el exel
						String keyNew = objKeyDinamico.substring(0, lastIndexOf);
						dataValueMap.put(keyNew, objMap.get(objKeyDinamico));
						if (!StringUtil.isNullOrEmpty(objMap.get(objKeyDinamico)) &&  !objKeyDinamico.contains(ARTIFICIO_COLUMNA) ) {
							agregarData = true;
						}
						j++;
					}
					if (agregarData) {
						if (isDrv) {  //tipo drv
							String key = (String)dataValueMap.get("NUM_UNICO_ITEM").getData();
							if (planByPolizaMap.containsKey(key)) {
								ValueDataVO valueData = dataValueMap.get("COD_PLAN");
								if (valueData == null) {
									valueData = new ValueDataVO();
								}
								valueData.setData(planByPolizaMap.get(key));
								dataValueMap.put("COD_PLAN", valueData);
							}
							
							if (productoByPolizaMap.containsKey(key)) {
								ValueDataVO valueData = dataValueMap.get("COD_PROD_SALUD");
								if (valueData == null) {
									valueData = new ValueDataVO();
								}
								valueData.setData(productoByPolizaMap.get(key));
								dataValueMap.put("COD_PROD_SALUD", valueData);
							}
						}
						listaDataResulMap.add(dataValueMap);
					}
				}
			}
			listaDataResulMapDinamico = null;
		}
		String titulo = "";
		//String archivoName = "EDGP";
		Map<String, String> listaHeaderOverrideMap = new HashMap<String, String>();
		Map<String, String> propiedadesMap = new HashMap<String, String>();
		propiedadesMap.put("calcularWitchDemanda", "true");
		//escribir redes
		propiedadesMap.put("hojaName", "DATA");
		propiedadesMap.put("writeExcel", "true");
		if (isDrv) {  //tipo drv
			for (Map<String,ValueDataVO> dataMap : listaDataResulMap) {
				String key = (String)dataMap.get("NUM_UNICO_ITEM").getData();
				//Inicio contador de riesgos by poliza
				if (!riesgosByPolizaMap.containsKey(key)) {
					riesgosByPolizaMap.put(key, 1);//por lo menos un riesgo
					ValueDataVO valueDataVO = dataMap.get("NUM_RIESGO");
					valueDataVO.setData(1);
				} else {
					Integer valueContador = riesgosByPolizaMap.get(key);
					valueContador++;
					riesgosByPolizaMap.put(key, valueContador);
					ValueDataVO valueDataVO = dataMap.get("NUM_RIESGO");
					valueDataVO.setData(valueContador);
				}
				//Fin contador de riesgos by poliza
			}
		}
		DataExportExcelPersonalizadoUtil.generarExcelObjectMap(listaHeader, listaHeaderOverrideMap, listaDataResulMap, archivoName, titulo, propiedadesMap);
	}

}
