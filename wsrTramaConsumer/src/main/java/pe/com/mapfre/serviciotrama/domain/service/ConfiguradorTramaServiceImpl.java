package pe.com.mapfre.serviciotrama.domain.service;

import java.util.List;

import javax.ejb.EJB;

import org.apache.log4j.Logger;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ConfiguracionTramaDetalleDaoLocal;

/**
 * La Class ConfiguradorTramaServiceImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
 /*@Stateless
 @EJB(name = "java:app/ConfiguradorTramaService", beanInterface = ConfiguradorTramaServiceLocal.class)*/

public class ConfiguradorTramaServiceImpl implements IConfiguradorTramaService {
	
	/** El log. */
	private final Logger logger = Logger.getLogger(this.getClass());
		
	
	/** El servicio configuracion trama detalle dao impl. */
	@EJB
	private ConfiguracionTramaDetalleDaoLocal configuracionTramaDetalleDaoImpl; 
	
	
	/* (non-Javadoc)
	 * @see pe.gob.mapfre.pwr.rep.ejb.service.configurador.trama.local.ConfiguradorTramaServiceLocal#obtenerReglasConfDetalle()
	 */
	@Override
	public List<ConfiguracionTramaDetalle> obtenerReglasConfDetalle() throws Exception {
		return this.configuracionTramaDetalleDaoImpl.obtenerReglasConfDetalle();
	}

	/* (non-Javadoc)
	 * @see pe.gob.mapfre.pwr.rep.ejb.service.configurador.trama.local.ConfiguradorTramaServiceLocal#obtenerNomenclaturaDatos(java.lang.Long)
	 */
	
 }
