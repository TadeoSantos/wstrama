package  pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class TipoErrorLogTramaType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum TipoErrorLogTramaType {

    /** El GENERAL. */
 	GENERAL("G" , "tipoErrorLogTrama.general"),
	
    /** El CAMPO. */
 	CAMPO("C" , "tipoErrorLogTrama.campo");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, TipoErrorLogTramaType> LOO_KUP_MAP = new HashMap<String, TipoErrorLogTramaType>();
	
	static {
		for (TipoErrorLogTramaType s : EnumSet.allOf(TipoErrorLogTramaType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo tipo error log trama type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private TipoErrorLogTramaType(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the tipo error log trama type
	 */
	public static TipoErrorLogTramaType get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
