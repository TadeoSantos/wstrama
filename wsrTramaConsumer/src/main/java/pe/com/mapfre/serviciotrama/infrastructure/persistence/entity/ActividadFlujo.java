package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ActividadFlujo.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_ORQ_ACT_FLU", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ActividadFlujo implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El identificador unico actividad. */
    @Id
    @Column(name = "C_ID_ACT_FLUJO" , length = 32)
    private String identificadorUnicoActividad;
   
    /** El proceso flujo. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_PROC_FLU", referencedColumnName = "N_ID_PROC_FLU")
    private ProcesoFlujo procesoFlujo;
   
    /** El pull conexion. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_PULL_CONEX", referencedColumnName = "N_ID_PULL_CONEX")
    private PullConexion pullConexion;
   
    /** El nombre actividad flujo. */
    @Column(name = "C_NOM_ACT_FLUJO" , length = 100)
    private String nombreActividadFlujo;
   
    /** El tipo ejecucion flujo. */
    @Column(name = "N_TIP_EJEC_FLUJO" , length = 1)
    private Long tipoEjecucionFlujo;
   
    /** El tarea ejecucion flujo. */
    @Column(name = "C_TAREA_EJEC_FLUJO" , length = 150)
    private String tareaEjecucionFlujo;
   
    /** El orden ejecucion. */
    @Column(name = "N_ORD_EJEC" , length = 2)
    private Long ordenEjecucion;
   
    /** El coordenada x. */
    @Column(name = "C_COORD_ACT_X" , length = 9)
    private String coordenadaX;
   
    /** El coordenada y. */
    @Column(name = "C_COORD_ACT_Y" , length = 9)
    private String coordenadaY;
   
    /** El estereotipo actividad. */
    @Column(name = "N_STEREOTIPO_ACT" , length = 1)
    private Long estereotipoActividad;
   
    /** El id indicador interno. */
    @Column(name = "N_INDIC_INTERNO" , length = 18)
    private Long idIndicadorInterno;
   
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
   
    /** El actividad flujo actividad variable list. */
    @OneToMany(mappedBy = "actividadFlujo", fetch = FetchType.LAZY)
    private List<ActividadVariable> actividadFlujoActividadVariableList = new ArrayList<ActividadVariable>();
    
    /** El actividad flujo dependencia actividad list. */
    @OneToMany(mappedBy = "actividadFlujo", fetch = FetchType.LAZY)
    private List<DependenciaActividad> actividadFlujoDependenciaActividadList = new ArrayList<DependenciaActividad>();
    
    /** El actividad flujo depedencia dependencia actividad list. */
    @OneToMany(mappedBy = "actividadFlujoDepedencia", fetch = FetchType.LAZY)
    private List<DependenciaActividad> actividadFlujoDepedenciaDependenciaActividadList = new ArrayList<DependenciaActividad>();
    
    /**
     * Instancia un nuevo actividad flujo.
     */
    public ActividadFlujo() {
    }
   
   
    /**
     * Instancia un nuevo actividad flujo.
     *
     * @param identificadorUnicoActividad el identificador unico actividad
     * @param procesoFlujo el proceso flujo
     * @param pullConexion el pull conexion
     * @param nombreActividadFlujo el nombre actividad flujo
     * @param tipoEjecucionFlujo el tipo ejecucion flujo
     * @param tareaEjecucionFlujo el tarea ejecucion flujo
     * @param ordenEjecucion el orden ejecucion
     * @param coordenadaX el coordenada x
     * @param coordenadaY el coordenada y
     * @param estereotipoActividad el estereotipo actividad
     * @param idIndicadorInterno el id indicador interno
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public ActividadFlujo(String identificadorUnicoActividad, ProcesoFlujo procesoFlujo,PullConexion pullConexion,String nombreActividadFlujo, Long tipoEjecucionFlujo, String tareaEjecucionFlujo, Long ordenEjecucion, String coordenadaX, String coordenadaY, Long estereotipoActividad, Long idIndicadorInterno, String usuarioCrea, Date fechaActualizacion ) {
        super();
        this.identificadorUnicoActividad = identificadorUnicoActividad;
        this.procesoFlujo = procesoFlujo;
        this.pullConexion = pullConexion;
        this.nombreActividadFlujo = nombreActividadFlujo;
        this.tipoEjecucionFlujo = tipoEjecucionFlujo;
        this.tareaEjecucionFlujo = tareaEjecucionFlujo;
        this.ordenEjecucion = ordenEjecucion;
        this.coordenadaX = coordenadaX;
        this.coordenadaY = coordenadaY;
        this.estereotipoActividad = estereotipoActividad;
        this.idIndicadorInterno = idIndicadorInterno;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((identificadorUnicoActividad == null) ? 0 : identificadorUnicoActividad.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ActividadFlujo other = (ActividadFlujo) obj;
        if (identificadorUnicoActividad == null) {
            if (other.identificadorUnicoActividad != null) {
                return false;
            }
        } else if (!identificadorUnicoActividad.equals(other.identificadorUnicoActividad)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ActividadFlujo [identificadorUnicoActividad=" + identificadorUnicoActividad + "]";
    }
   
}