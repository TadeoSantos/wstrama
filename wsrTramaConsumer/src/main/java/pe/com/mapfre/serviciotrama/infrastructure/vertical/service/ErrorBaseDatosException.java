package pe.com.mapfre.serviciotrama.infrastructure.vertical.service;

public class ErrorBaseDatosException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;
	
	private String codigoOperacion;

	public ErrorBaseDatosException(String exception, String codigoOperacion){
		super(exception);
		this.codigoOperacion=codigoOperacion;
	}

	public String getCodigoOperacion() {
		return codigoOperacion;
	}

	public void setCodigoOperacion(String codigoOperacion) {
		this.codigoOperacion = codigoOperacion;
	}
	
}
