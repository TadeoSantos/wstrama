package pe.com.mapfre.serviciotrama.infrastructure.vertical.interfaces;

import java.util.Map;

public interface IRestClientUtil {
	
	public String operacionPost(String url, String recurso, String parametros, String usuario, String password, String token);
	public String operacionGet(String url, String recurso, Map<String, Object> parametros);
	
}
