package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ControlProcesoDetalleHist.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "TASMREGN_HIS", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ControlProcesoDetalleHist implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id detalle proceso flujo. */
    @Id
    @Column(name = "ID_REG_HIST" , length = 32)
    private String idDetalleProcesoFlujo;
   
    /** El id control proceso. */
    @Column(name = "NUM_PROC" , length = 12)
    private String idControlProceso;
   
    /** El fecha lote. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_LOTE")
    private Date fechaLote;
   
    /** El numero lote. */
    @Column(name = "NUM_LOTE" , length = 20)
    private String numeroLote;
   
    /** El campo identificador negocio. */
    @Column(name = "COD_NEGOCIO_NOM" , length = 100)
    private String campoIdentificadorNegocio;
   
    /** El campo identificador negocio valor. */
    @Column(name = "COD_REG" , length = 100)
    private String campoIdentificadorNegocioValor;
   
    /** El id juego. */
    @Column(name = "COD_JUEGO" , length = 18)
    private Long idJuego;
   
    /** El estado. */
    @Column(name = "COD_EST_REG" , length = 2)
    private String estado;
   
    /** El codigo usuario. */
    @Column(name = "COD_USR" , length = 50)
    private String codigoUsuario;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_ACTU")
    private Date fechaActualizacion;
   
    /** El identificador interno actividad. */
    @Column(name = "COD_ACT_FLUJO" , precision = 18 , scale = 0)
    private Long identificadorInternoActividad;
   
    /**
     * Instancia un nuevo control proceso detalle hist.
     */
    public ControlProcesoDetalleHist() {
    }
   
   
    /**
     * Instancia un nuevo control proceso detalle hist.
     *
     * @param idDetalleProcesoFlujo el id detalle proceso flujo
     * @param idControlProceso el id control proceso
     * @param fechaLote el fecha lote
     * @param numeroLote el numero lote
     * @param campoIdentificadorNegocio el campo identificador negocio
     * @param campoIdentificadorNegocioValor el campo identificador negocio valor
     * @param idJuego el id juego
     * @param estado el estado
     * @param codigoUsuario el codigo usuario
     * @param fechaActualizacion el fecha actualizacion
     * @param identificadorInternoActividad el identificador interno actividad
     */
    public ControlProcesoDetalleHist(String idDetalleProcesoFlujo, String idControlProceso, Date fechaLote, String numeroLote, String campoIdentificadorNegocio, String campoIdentificadorNegocioValor, Long idJuego, String estado, String codigoUsuario, Date fechaActualizacion, Long identificadorInternoActividad ) {
        super();
        this.idDetalleProcesoFlujo = idDetalleProcesoFlujo;
        this.idControlProceso = idControlProceso;
        this.fechaLote = fechaLote;
        this.numeroLote = numeroLote;
        this.campoIdentificadorNegocio = campoIdentificadorNegocio;
        this.campoIdentificadorNegocioValor = campoIdentificadorNegocioValor;
        this.idJuego = idJuego;
        this.estado = estado;
        this.codigoUsuario = codigoUsuario;
        this.fechaActualizacion = fechaActualizacion;
        this.identificadorInternoActividad = identificadorInternoActividad;
    }
   
	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idDetalleProcesoFlujo == null) ? 0 : idDetalleProcesoFlujo.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ControlProcesoDetalleHist other = (ControlProcesoDetalleHist) obj;
        if (idDetalleProcesoFlujo == null) {
            if (other.idDetalleProcesoFlujo != null) {
                return false;
            }
        } else if (!idDetalleProcesoFlujo.equals(other.idDetalleProcesoFlujo)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ControlProcesoDetalleHist [idDetalleProcesoFlujo=" + idDetalleProcesoFlujo + "]";
    }
   
}