package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class RespuestaFTPVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class RespuestaFTPVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Map<String,Map<String,String>>> listaGrupoArchivoMap = new ArrayList<Map<String,Map<String,String>>>();
	private Map<String,String> nombreArchivoFTPMap = new HashMap<String, String>();
	
	/** La codigo error. */
	private String codigoError;
	
	/** La mensaje error. */
	private String mensajeError;
	
	/** La is error. */
	private boolean isError;
	
	public RespuestaFTPVO() {
		super();
	}
	
	//get y set
	public RespuestaFTPVO(
			List<Map<String, Map<String, String>>> listaGrupoArchivoMap,
			Map<String, String> nombreArchivoFTPMap) {
		super();
		this.listaGrupoArchivoMap = listaGrupoArchivoMap;
		this.nombreArchivoFTPMap = nombreArchivoFTPMap;
	}

	public boolean isError() {
		return isError;
	}

}
