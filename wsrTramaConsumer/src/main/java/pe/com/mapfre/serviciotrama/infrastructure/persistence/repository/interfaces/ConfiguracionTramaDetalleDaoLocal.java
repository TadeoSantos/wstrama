package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import java.util.List;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;

/**
 * La Class ConfiguracionTramaDetalleDaoLocal.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public interface ConfiguracionTramaDetalleDaoLocal  extends IGenericRepository<Long,ConfiguracionTramaDetalle> {
	
	/**
	 * Obtener reglas conf detalle.
	 *
	 * @return the list
	 * @throws Exception the exception
	 */
	List<ConfiguracionTramaDetalle> obtenerReglasConfDetalle();
}