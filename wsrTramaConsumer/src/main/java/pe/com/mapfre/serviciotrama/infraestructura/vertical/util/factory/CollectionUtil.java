package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanComparator;
import org.apache.commons.collections.comparators.ComparatorChain;
import org.apache.log4j.Logger;


/**
 * La Class CollectionUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class CollectionUtil implements Serializable {
	

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

		/** La log. */
	private static final Logger log = Logger.getLogger(CatalogoCacheErrorUtil.class);
	

	/**
	 * Ordenador.
	 *
	 * @param descending el descending
	 * @param listaGeneral el lista general
	 * @param nombreColumna el nombre columna
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void ordenador(boolean descending, List listaGeneral, String nombreColumna) {
		try {
			BeanComparator actorComparator = null;
			if (descending) {
				actorComparator = new BeanComparator(nombreColumna, Collections.reverseOrder());
			} else {
				actorComparator = new BeanComparator(nombreColumna);
			}
			Collections.sort(listaGeneral,actorComparator);
		} catch (Exception e) {
			log.error("Error ",e);
		}
	}
	
	public static void ordenadorGroup(boolean descending, List listaGeneral, String... nombreColumna) {
		try {
			List<BeanComparator> listaFiled = new ArrayList<BeanComparator>();
			ComparatorChain chain = null;
			if (descending) {
				for (String columna : nombreColumna) {
					listaFiled.add(new BeanComparator(columna,Collections.reverseOrder()));
				}
			} else {
				for (String columna : nombreColumna) {
					listaFiled.add(new BeanComparator(columna));
				}
			}
			chain = new ComparatorChain(listaFiled);
			Collections.sort(listaGeneral, chain);
		} catch (Exception e) {
			log.error("Error ",e);
		}
	}
	
	public static boolean isEmpty(List<?> list) {
		boolean respuesta = false;
		if (list == null || list.size() == 0) {
			respuesta = true;
		}
		return respuesta;
	}
	public static boolean isEmpty(Map<?,?> listMap) {
		boolean respuesta = false;
		if (listMap == null || listMap.size() == 0) {
			respuesta = true;
		}
		return respuesta;
	}
}