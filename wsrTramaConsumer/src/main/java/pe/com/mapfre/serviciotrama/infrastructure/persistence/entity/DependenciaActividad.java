package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class DependenciaActividad.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_ORQ_DEP_ACT", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class DependenciaActividad implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El identificador unico dependecia actividad. */
    @Id
    @Column(name = "C_ID_DEP_ACT" , length = 32)
    private String identificadorUnicoDependeciaActividad;
   
    /** El actividad flujo. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "C_ID_ACT_FLUJO", referencedColumnName = "C_ID_ACT_FLUJO")
    private ActividadFlujo actividadFlujo;
   
    /** El actividad flujo depedencia. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "C_ID_ACT_FLUJO_DEP", referencedColumnName = "C_ID_ACT_FLUJO")
    private ActividadFlujo actividadFlujoDepedencia;
   
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
   
    /**
     * Instancia un nuevo dependencia actividad.
     */
    public DependenciaActividad() {
    }
   
   
    /**
     * Instancia un nuevo dependencia actividad.
     *
     * @param identificadorUnicoDependeciaActividad el identificador unico dependecia actividad
     * @param actividadFlujo el actividad flujo
     * @param actividadFlujoDepedencia el actividad flujo depedencia
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public DependenciaActividad(String identificadorUnicoDependeciaActividad, ActividadFlujo actividadFlujo,ActividadFlujo actividadFlujoDepedencia,String usuarioCrea, Date fechaActualizacion ) {
        super();
        this.identificadorUnicoDependeciaActividad = identificadorUnicoDependeciaActividad;
        this.actividadFlujo = actividadFlujo;
        this.actividadFlujoDepedencia = actividadFlujoDepedencia;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((identificadorUnicoDependeciaActividad == null) ? 0 : identificadorUnicoDependeciaActividad.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DependenciaActividad other = (DependenciaActividad) obj;
        if (identificadorUnicoDependeciaActividad == null) {
            if (other.identificadorUnicoDependeciaActividad != null) {
                return false;
            }
        } else if (!identificadorUnicoDependeciaActividad.equals(other.identificadorUnicoDependeciaActividad)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DependenciaActividad [identificadorUnicoDependeciaActividad=" + identificadorUnicoDependeciaActividad + "]";
    }
   
}