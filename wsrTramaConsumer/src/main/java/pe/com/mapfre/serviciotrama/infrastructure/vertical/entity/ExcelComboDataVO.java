package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ExcelComboDataVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ExcelComboDataVO implements Serializable {

    /** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	private String nombreCampo;
	private Integer posicionCampo;
    private List<String> listaExcelComboData = new ArrayList<String>();
    
	public ExcelComboDataVO() {
		super();
	}

}