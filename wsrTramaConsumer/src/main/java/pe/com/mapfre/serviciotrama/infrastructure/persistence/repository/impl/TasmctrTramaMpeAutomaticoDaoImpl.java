package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrTramaMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrTramaMpePK;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.TasmctrTramaMpeAutomaticoDaoLocal;

/**
 * La Class TasmctrTramaMpeAutomaticoDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class TasmctrTramaMpeAutomaticoDaoImpl extends GenericRepository<TasmctrTramaMpePK, TasmctrTramaMpe>
		implements TasmctrTramaMpeAutomaticoDaoLocal {

	private Logger log = LoggerFactory.getLogger(TasmctrTramaMpeAutomaticoDaoImpl.class);

	/**
	 * saveNative.
	 *
	 * @param obj
	 *            el entity
	 * @return the t
	 * 
	 * @Override public TasmctrTramaMpe saveNative(TasmctrTramaMpe obj) { try {
	 *           Map<String, Object> parametros = TransferDataUtil.toEntityMap(obj);
	 *           StringBuilder sql = saveInsert(obj.getClass()); log.debug("
	 *           saveNative sql : " + sql); log.debug(" saveNative parametros : " +
	 *           parametros); executeUpdate(sql, parametros); } catch (Exception e)
	 *           { log.error("Error ", e); } return obj; }
	 */

}