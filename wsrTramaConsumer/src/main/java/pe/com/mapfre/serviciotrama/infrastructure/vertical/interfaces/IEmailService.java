package pe.com.mapfre.serviciotrama.infrastructure.vertical.interfaces;

import java.util.Map;

import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.CorreoVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.EMail;

public interface IEmailService {
	public void enviarEmail(EMail email);
	String enviarCorreoElectronico(final CorreoVO correo, Map<String, String> paramsCorreo);
	
}
