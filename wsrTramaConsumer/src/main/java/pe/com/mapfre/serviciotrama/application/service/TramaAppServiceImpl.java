package pe.com.mapfre.serviciotrama.application.service;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ResultadoProcesoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.TramaVO;
import pe.com.mapfre.serviciotrama.domain.service.ITramaService;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.AtributosEntityCacheUtil;

/**
 * La Class TramaAppServiceImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class TramaAppServiceImpl implements ITramaAppService {

	@EJB
	private ITramaService servicio;

	public TramaAppServiceImpl() {
		AtributosEntityCacheUtil.getInstance().sincronizarAtributos("pe.com.mapfre.serviciotrama.infrastructure.persistence.entity");
	}

	@Override
	public List<ResultadoProcesoConfiguracionTramaVO> procesarConfiguracionTrama(TramaVO obj) throws Exception {
		return servicio.procesarConfiguracionTrama(obj);
	}
}
