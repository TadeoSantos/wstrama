package pe.com.mapfre.serviciotrama.domain.service;

import java.io.BufferedReader;
import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ConfiguracionTramaDataVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.ConfiguracionTramaIdProcesarVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.GrupoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.RespuestaLecturaAgrupadoVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.RespuestaLecturaArchivoVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.ResultadoProcesoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.TramaVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfigUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfiguracionTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ObjectUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ResourceUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.RespuestaNaturalType;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.UUIDUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.csv.CSVUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.excel.ExcelUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CollectionUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ftp.FTPClienteUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jasper.ArchivoUtilidades;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.motor.regla.ParametroReglaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.motor.regla.ProcesarReglaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.txt.TXTUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionFtpTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProceso;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.JuegoTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.LogConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ProcesoFlujo;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrTramaMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmeErrorMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TramaNomenclaturaArchivo;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TramaNomenclaturaArchivoDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.CampoTablaVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.ConfiguracionCacheUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.EjecutarActividadParaleloInstance;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.FTPVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ReglaVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.RespuestaFTPVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.state.EstadoConfiguracionTramaState;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.state.EstadoProcesamientoConfiguracionTramaState;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.ClaseErrorOrquestadorType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.ComponenteValidadoLogTramaType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.ErrorCatalogoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.ErrorTipoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.EstadoDetalleProcesoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.EstadoProcesoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoArchivoProcesarType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoCampoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoDestinatarioFTPType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoErrorLogTramaType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoErrorOrquestadorType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoOrigenType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoProcesoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.VariableProcesoType;

/**
 * La Class TramaServiceImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */

@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class TramaServiceImpl extends LectorTramaUtil implements ITramaService {

	/** La log. */
	private final Logger log = Logger.getLogger(this.getClass());
	
	private String updateProceso(Long idFlujoOrquestar, Map<String, Object> variableFlujo) throws Exception {
		obtenerServiceContext();

		ControlProceso controlProceso = new ControlProceso();
		
		controlProceso.setIdControlProceso(variableFlujo.get(VariableProcesoType.ID_CONTROL_PROCESO.getKey()) + "");
		controlProceso.setIdTipoProceso(idFlujoOrquestar);// procesoFlujo
		// ConstanteTramaUtil.MODO_EJECUCION)
		if (variableFlujo.containsKey("indicadorInternoActividad")) {
			controlProceso.setIdModoProceso((Long) variableFlujo.get("indicadorInternoActividad"));// ver variable
		}
		
		if (variableFlujo.containsKey(VariableProcesoType.ORIGEN.getKey())) {
			controlProceso.setIdOrigenProceso((String) variableFlujo.get(VariableProcesoType.ORIGEN.getKey()));// ver																								// variable
		}
		
		controlProceso.setUsuarioProceso(variableFlujo.get(VariableProcesoType.USERNAME.getKey()) + "");
		controlProceso.setFechaInicio(FechaUtil.obtenerFechaActual());
		controlProceso.setFechaFin(null);
		controlProceso.setEstado(EstadoProcesoType.EN_SOLICITUD.getKey()); // TODO SE CAMBIO EL NOMBRE A En Solicitud A
																			// UN NO PASA A LA COLA
		controlProceso.setCodigoUsuario(variableFlujo.get(VariableProcesoType.USERNAME.getKey()) + "");
		controlProceso.setFechaActualizacion(new Date());
		
		controlProcesoDao.updateNative(controlProceso);//cambiar de estad
		return "";	
	}
	private Map<String, Object> obtenerParametro(TramaVO obj){
		Map<String, Object> parametroMap = new HashMap<String, Object>();
		parametroMap.put("userName", obj.getUserName());
		parametroMap.put("password", obj.getPassword());
		parametroMap.put("idControlProceso", obj.getIdControlProceso());
		parametroMap.put("idProcesoFlujo", obj.getIdProcesoFlujo());
		parametroMap.put("idJuegoTrama", obj.getJuegoTrama());
		parametroMap.put("numeroPoliza", obj.getNumeroPoliza());
		parametroMap.put("numeroSolicitud", obj.getNumeroSolicitud());
		parametroMap.put("origen", obj.getOrigen());
		parametroMap.put("validarExclusion", obj.getValidarExclusion());
		parametroMap.put("indicadorInternoActividad", obj.getIndicadorInternoActividad());
		parametroMap.put(ConstanteConfiguracionTramaUtil.ES_SIMULACION, false);
		return parametroMap;
	}

	@Override
	public List<ResultadoProcesoConfiguracionTramaVO> procesarConfiguracionTrama(TramaVO obj) throws Exception {
		Map<String, Object> parametroMap = obtenerParametro(obj);
		Long idProcesoFlujo = (Long) parametroMap.get(VariableProcesoType.ID_PROCESO_FLUJO.getKey());
		String numProceso = updateProceso(idProcesoFlujo, parametroMap);
		parametroMap.put(VariableProcesoType.ID_CONTROL_PROCESO.getKey(), numProceso);
		// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
		if (EjecutarActividadParaleloInstance.getInstance().isBloquearProceso(parametroMap.get(VariableProcesoType.ID_CONTROL_PROCESO.getKey()) + "")) {
			EjecutarActividadParaleloInstance.getInstance()
					.removeBloquearProceso(parametroMap.get(VariableProcesoType.ID_CONTROL_PROCESO.getKey()) + "");
			return new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
		}
		// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
		// (userName, password, idControlProceso, idProcesoFlujo, idJuegoTrama,
		// numeroPoliza, numeroSolicitud, origen, validarExclusion,
		// indicadorInternoActividad));

		Long idJuegoTrama = (Long) parametroMap.get(VariableProcesoType.ID_JUEGO_TRAMA.getKey());
		String origen = (String) parametroMap.get(VariableProcesoType.ORIGEN.getKey());
		// obtenerSessionHibernate();
		List<ConfiguracionTrama> listaConfiguracionTrama = null;
		if (idProcesoFlujo != null && idProcesoFlujo > 0) {
			ConfiguracionTrama configuracionTramaFiltro = new ConfiguracionTrama();
			configuracionTramaFiltro.setJuegoTrama(new JuegoTrama());
			if (!StringUtil.isNullOrEmptyNumeric(idJuegoTrama)) {
				configuracionTramaFiltro.getJuegoTrama().setIdJuegoTrama(idJuegoTrama);
			}
			configuracionTramaFiltro.getJuegoTrama().setProcesoFlujo(new ProcesoFlujo());
			configuracionTramaFiltro.getJuegoTrama().getProcesoFlujo().setIdProcesoFlujo(idProcesoFlujo);
			// configuracionTramaFiltro.setEstado(EstadoConfiguracionTramaState.ACTIVO.getKey());
			if (TipoOrigenType.MASIVO_AUTOMATICO.getKey().toString().equals(origen)) {
				configuracionTramaFiltro.setEstado(EstadoConfiguracionTramaState.ACTIVO.getKey());
			}
			listaConfiguracionTrama = configuracionTramaDao.listar(configuracionTramaFiltro);
		}
		return procesarConfiguracionTrama(parametroMap, listaConfiguracionTrama);
	}

	@Override
	public List<ResultadoProcesoConfiguracionTramaVO> procesarConfiguracionTrama(Map<String, Object> parametroMap,
			List<ConfiguracionTrama> listaConfiguracionTrama) throws Exception {
		// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019 idControlProceso
		if (EjecutarActividadParaleloInstance.getInstance()
				.isBloquearProceso(parametroMap.get(VariableProcesoType.ID_CONTROL_PROCESO.getKey()) + "")) {
			EjecutarActividadParaleloInstance.getInstance()
					.removeBloquearProceso(parametroMap.get(VariableProcesoType.ID_CONTROL_PROCESO.getKey()) + "");
			return new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
		}
		// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
		String userName = (String) parametroMap.get(VariableProcesoType.USERNAME.getKey() + "");
		String password = (String) parametroMap.get(VariableProcesoType.PASSWORD.getKey());
		String idControlProceso = (String) parametroMap.get(VariableProcesoType.ID_CONTROL_PROCESO.getKey());
		Long idProcesoFlujo = (Long) parametroMap.get(VariableProcesoType.ID_PROCESO_FLUJO.getKey());
		Long idJuegoTrama = (Long) parametroMap.get(VariableProcesoType.ID_JUEGO_TRAMA.getKey());
		String numeroPoliza = (String) parametroMap.get(VariableProcesoType.NUMERO_POLIZA.getKey());
		String numeroSolicitud = (String) parametroMap.get(VariableProcesoType.NUMERO_SOLICITUD.getKey());
		boolean validarExcluision = (Boolean) parametroMap.get(VariableProcesoType.EXCLUSION.getKey());// validarExclusion
		String origen = (String) parametroMap.get(VariableProcesoType.ORIGEN.getKey());
		Long indicadorInternoActividad = (Long) parametroMap
				.get(VariableProcesoType.INDICADOR_INTERNO_ACTIVIDAD.getKey());
		parametroMap.put(ConstanteConfiguracionTramaUtil.ES_IMPRIMIR,
				ConfiguracionCacheUtil.getInstance().isGenerarLogTramaJuego(idJuegoTrama));

		log.error("Inicio.procesarConfiguracionTrama(userName = " + userName + ",password =..., idControlProceso = "
				+ idControlProceso + ",idProcesoFlujo = " + idProcesoFlujo + ",idJuegoTrama = " + idJuegoTrama
				+ ",numeroPoliza = " + numeroPoliza + ", numeroSolicitud = " + numeroSolicitud + ",validarExcluision = "
				+ validarExcluision + ",origen = " + origen + ",indicadorInternoActividad = "
				+ indicadorInternoActividad + ") ");
		List<ResultadoProcesoConfiguracionTramaVO> resultado = new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
		// obtenerSessionHibernate();
		parametroMap.put(ConstanteConfiguracionTramaUtil.USUARIO, userName);
		String rutaRelativaTemp = obtenerRutaRelativaTemp(parametroMap);
		parametroMap.put("rutaRelativaTemp", rutaRelativaTemp);
		EntityManager sessionHibernate = null;
		if (idProcesoFlujo != null && idProcesoFlujo > 0) {
			String usuario = ConstanteConfiguracionTramaUtil.CARPETA_AUTOMATIC;
			if (idJuegoTrama != null && idJuegoTrama > 0) {
				usuario = usuario + idJuegoTrama;
			}
			String rutaArchivo = ConstanteConfigUtil.generarRuta(ArchivoUtilidades.RUTA_RECURSOS,
					ArchivoUtilidades.RUTA_REPORTE_GENERADO) + usuario;
			try {
				ArchivoUtilidades.limpiarArchivoAll(rutaArchivo);
			} catch (Exception e) {
				log.error("Error ", e);
			}
			// TIPIFICACION ERROR SIN TRAMA CASO: CUANDO NO SE ENCONTRO JUEGO PARA PROCESAR
			if (CollectionUtil.isEmpty(listaConfiguracionTrama)) {
				LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "",
						ClaseErrorOrquestadorType.MENSAJE_NO_SE_ENCONTRO_JUEGO.getKey(),
						ClaseErrorOrquestadorType.MENSAJE_NO_SE_ENCONTRO_JUEGO.getValue(),
						"idProceso : " + idControlProceso, 0L, ComponenteValidadoLogTramaType.CONF_TRAMA,
						TipoErrorLogTramaType.GENERAL);
				BigMemoryManager<String, LogConfiguracionTrama> listaLogConfiguracionTramaAyuda = new BigMemoryManager<String, LogConfiguracionTrama>(
						rutaRelativaTemp, "data_log_ayuda");
				listaLogConfiguracionTramaAyuda.add(logConfiguracionTrama);
				registrarLogConfiguracionDataAyudaGrupo(parametroMap, listaLogConfiguracionTramaAyuda,
						sessionHibernate);
				// PARA MOSTRAR ERRORES AL USUARIO
				resultado.add(generarObjetoError("", "",
						ClaseErrorOrquestadorType.MENSAJE_NO_SE_ENCONTRO_JUEGO.getKey(),
						ClaseErrorOrquestadorType.MENSAJE_NO_SE_ENCONTRO_JUEGO.getValue(), idJuegoTrama, 0L, 0L, null));
			}

			if (!StringUtil.isNullOrEmpty(numeroPoliza)) {
				parametroMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM, numeroPoliza.trim());
			}
			if (!StringUtil.isNullOrEmpty(numeroSolicitud)) {
				parametroMap.put(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD, numeroSolicitud.trim());
			}
			parametroMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_ORIGEN, origen);
			parametroMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_USUARIO, userName);
			parametroMap.put(ConstanteConfiguracionTramaUtil.ID_CONTROL_PROCESO, idControlProceso);
			parametroMap.put(ConstanteConfiguracionTramaUtil.INDICADOR_INTERNO_ACTIVIDAD, indicadorInternoActividad);
			resultado = procesarConfiguracionTrama(listaConfiguracionTrama, parametroMap, true, validarExcluision,
					sessionHibernate);
			// TODO:VER_SIMULACION
			if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				registrarProcesoFlujoError(resultado, idControlProceso, indicadorInternoActividad, parametroMap); // GUARDAR
																													// LOS
																													// ERRORES
																													// DE
																													// TRAMA
				registrarVariablesProceso(resultado, idControlProceso, indicadorInternoActividad, userName); // GUARDA
																												// LAS
																												// VARIABLES
																												// DE LA
																												// ACTIVIDAD
			}
			listaConfiguracionTrama = null;// Limpia memoria
			if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				ArchivoUtilidades.limpiarArchivoAllDirectory(ConstanteConfigUtil.RUTA_RECURSOS_DATA_BUFFER
						+ ConstanteConfigUtil.generarRuta(userName) + rutaRelativaTemp);
			}
		}
		parametroMap = null;// Limpia memoria

		System.gc();
		log.error("Fin.procesarConfiguracionTrama(userName = " + userName + ",password =..., idControlProceso = "
				+ idControlProceso + ",idProcesoFlujo = " + idProcesoFlujo + ",idJuegoTrama = " + idJuegoTrama
				+ ",numeroPoliza = " + numeroPoliza + ", numeroSolicitud = " + numeroSolicitud + ",validarExcluision = "
				+ validarExcluision + ",origen = " + origen + ",indicadorInternoActividad = "
				+ indicadorInternoActividad + ") ");
		return resultado;
	}

	private String obtenerRutaRelativaTemp(Map<String, Object> parametroMap) {
		if (!parametroMap.containsKey("idJuegoTrama")) {
			parametroMap.put("idJuegoTrama", 0L);
		}
		String rutaRelativaTemp = ConstanteConfigUtil.generarRuta(
				parametroMap.get(ConstanteConfiguracionTramaUtil.USUARIO) + "", UUIDUtil.generarElementUUID())
				+ parametroMap.get("idJuegoTrama");
		if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
			rutaRelativaTemp = ConstanteConfigUtil.RUTA_SESSION_TEMP
					+ ConstanteConfigUtil.generarRuta(parametroMap.get(ConstanteConfiguracionTramaUtil.USUARIO) + "",
							parametroMap.get("uuidProcesar") + "", parametroMap.get("idJuegoTrama") + "")
					+ UUIDUtil.generarElementUUID();
		}
		return rutaRelativaTemp;
	}

	@Override
	public Map<String, Object> obtenerVariableRespuestaLecturaArchivo(List<ConfiguracionTrama> listaConfiguracionTrama,
			Map<String, Object> parametroMap) {
		Map<String, Object> resultado = new HashMap<String, Object>();
		List<RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoVOGrupo = new ArrayList<RespuestaLecturaArchivoVO>();

		JuegoTrama juegoTrama = listaConfiguracionTrama.get(0).getJuegoTrama();
		FTPVO fTPVO = new FTPVO();
		ConfiguracionTrama configuracionTramaTemp = new ConfiguracionTrama();
		configuracionTramaTemp.setJuegoTrama(juegoTrama);
		List<TramaNomenclaturaArchivoDetalle> listaDetalleNomenclatura = new ArrayList<TramaNomenclaturaArchivoDetalle>();
		String nomenclatura = "";
		String ruta = "";
		boolean isManual = true;
		// obtener lista de ids de configuracion de trama
		try {
			// 1.obtener el archivo del ftp
			// 1.1 obtener nomenclaruta por id de la configuracion de la tram
			// con el id padre buscar nomenclatura.
			TramaNomenclaturaArchivo tramaNomenclaturaArchivo = configuracionTramaDao
					.obtenerTramaNomenclaturaArchivoById(juegoTrama.getIdTramaNomenclaturaArchivoPadre());

			// traer los detalles de la nomenclatura en lista
			List<Long> listaIdTramaNomenclaturaArchivo = new ArrayList<Long>();
			listaIdTramaNomenclaturaArchivo.add(juegoTrama.getIdTramaNomenclaturaArchivoPadre());
			listaDetalleNomenclatura = configuracionTramaDao.listarNomenclaturaDetMap(listaIdTramaNomenclaturaArchivo)
					.get(juegoTrama.getIdTramaNomenclaturaArchivoPadre());
			configuracionTramaTemp.setTramaNomenclaturaArchivo(tramaNomenclaturaArchivo);
			configuracionTramaTemp.getTramaNomenclaturaArchivo()
					.getTramaNomenclaturaArchivoTramaNomenclaturaArchivoDetalleList().addAll(listaDetalleNomenclatura);

			// obtener el formato de la nomenclatura
			nomenclatura = obtenerNomenclatura(configuracionTramaTemp, parametroMap, isManual, false);
			// traer configuracion del FTP
			// bajar archivo
			fTPVO = obtenerFTP(fTPVO, configuracionTramaTemp, TipoDestinatarioFTPType.ORIGEN);
			List<Map<String, Map<String, String>>> resultadoFTPList = null;
			ruta = fTPVO.getRutaFTP();
			fTPVO.setIdTramaNomenclaturaArchivo(
					configuracionTramaTemp.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo());
			if (nomenclatura.contains("${PARAMETRO}")) {
				fTPVO.setRutaLocal(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoLocal(nomenclatura);
				fTPVO.setRutaFTP(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoFTP(nomenclatura);
				fTPVO.setIdJuegoTrama(juegoTrama.getIdJuegoTrama());
				RespuestaFTPVO respuestaFTPVO = FTPClienteUtil.descargarArchivoListFilterPersonalizadoGrupo(fTPVO,
						parametroMap);
				resultadoFTPList = respuestaFTPVO.getListaGrupoArchivoMap();
				for (Map<String, Map<String, String>> resultadoFTPMap : resultadoFTPList) {
					for (Map.Entry<String, Map<String, String>> resultadoFTPDataMap : resultadoFTPMap.entrySet()) {
						Map<String, String> resultadoFTPDataProcesarMap = resultadoFTPDataMap.getValue();
						if (parametroMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)) {// cuando se
																											// tiene
																											// numero de
																											// solicitud
							if (!resultadoFTPDataProcesarMap
									.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)) {
								resultadoFTPDataProcesarMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM,
										null);
								if (!StringUtil.isNullOrEmpty(
										parametroMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM))) {
									resultadoFTPDataProcesarMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM,
											parametroMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM) + "");
								}
							}
						}
						// Inicio requerimiento fecha calculada //
						if (!resultadoFTPDataProcesarMap
								.containsKey(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO)) {
							resultadoFTPDataProcesarMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO,
									null);
							if (!StringUtil.isNullOrEmpty(
									parametroMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO))) {
								resultadoFTPDataProcesarMap.put(
										ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO,
										parametroMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO)
												+ "");
							}
						}
						// Fin requerimiento fecha calculada //
						RespuestaLecturaArchivoVO resultadoTempGrupoData = new RespuestaLecturaArchivoVO();
						String resultadoFTP = resultadoFTPDataProcesarMap
								.get(ConstanteConfiguracionTramaUtil.NOMENCLATURA);
						if (!(resultadoFTPList == null || resultadoFTPList.contains("${ERROR}"))) {
							resultadoTempGrupoData.setNumeroReferencia(resultadoFTPDataMap.getKey());
							resultadoTempGrupoData.setNomenclatura(nomenclatura);
							resultadoTempGrupoData.setNomenclaturaLeer(resultadoFTP);
							resultadoTempGrupoData.setPropiedadMap(resultadoFTPDataProcesarMap);
							listaRespuestaLecturaArchivoVOGrupo.add(resultadoTempGrupoData);
						}
					}
				}
			} else {
				fTPVO.setRutaLocal(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoLocal(nomenclatura);
				fTPVO.setRutaFTP(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoFTP(nomenclatura);
				fTPVO.setIdJuegoTrama(juegoTrama.getIdJuegoTrama());
				// RespuestaFTPVO respuestaFTPVO =
				// FTPClienteUtil.descargarArchivoListFilterPersonalizadoGrupo(fTPVO,parametroMap);
				RespuestaLecturaArchivoVO resultadoTempGrupoData = new RespuestaLecturaArchivoVO();
				resultadoTempGrupoData.setNomenclaturaLeer(nomenclatura);
				resultadoTempGrupoData.setNomenclatura(nomenclatura);
				Map<String, String> dataValueMap = new HashMap<String, String>();
				dataValueMap.put(ConstanteConfiguracionTramaUtil.NOMENCLATURA, nomenclatura);
				dataValueMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM, null);
				dataValueMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM, null);
				if (!StringUtil.isNullOrEmpty(parametroMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM))) {
					dataValueMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM,
							parametroMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM) + "");
				}
				if (!StringUtil.isNullOrEmpty(
						parametroMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM))) {
					dataValueMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM,
							parametroMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM) + "");
				}
				// Inicio requerimiento fecha calculada //
				if (!StringUtil
						.isNullOrEmpty(parametroMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO))) {
					dataValueMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO,
							parametroMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO) + "");
				}
				// Fin requerimiento fecha calculada //
				resultadoTempGrupoData.getPropiedadMap().putAll(dataValueMap);
				if (!StringUtil.isNullOrEmpty(parametroMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD))) {
					resultadoTempGrupoData.setNumeroReferencia(
							parametroMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD) + "");
				}
				listaRespuestaLecturaArchivoVOGrupo.add(resultadoTempGrupoData);
			}
		} catch (Exception e) {
			log.error("Error ", e);
		}
		resultado.put("listaRespuestaLecturaArchivoVOGrupo", listaRespuestaLecturaArchivoVOGrupo);
		resultado.put("fTPVO", fTPVO);
		resultado.put("ruta", ruta);
		resultado.put("isManual", isManual);
		return resultado;
	}

	public void ejecutarParticionExcel(List<ConfiguracionTrama> listaConfiguracionTrama,
			Map<String, Object> parametroMap) {
		FTPVO fTPVO = new FTPVO();
		try {
			Map<String, Object> resulMap = obtenerVariableRespuestaLecturaArchivo(listaConfiguracionTrama,
					parametroMap);
			List<RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoVOGrupo = (List<RespuestaLecturaArchivoVO>) resulMap
					.get("listaRespuestaLecturaArchivoVOGrupo");
			fTPVO = (FTPVO) resulMap.get("fTPVO");
			String ruta = (String) resulMap.get("ruta");
			boolean isManual = (Boolean) resulMap.get("isManual");
			for (RespuestaLecturaArchivoVO respuestaLecturaArchivoVO : listaRespuestaLecturaArchivoVOGrupo) {
				fTPVO.setRutaLocal(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoFTP(respuestaLecturaArchivoVO.getNomenclaturaLeer());
				fTPVO.setNombreArchivoLocal(respuestaLecturaArchivoVO.getNomenclaturaLeer());
				fTPVO.setRutaFTP(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				boolean descargoArchivo = FTPClienteUtil.descargarArchivo(fTPVO, parametroMap);
				if (descargoArchivo) {
					Map<String, String> nomenclaturaMap = new HashMap<String, String>();
					for (ConfiguracionTrama objData : listaConfiguracionTrama) {
						Map<String, Object> parametroMapTemp = new HashMap<String, Object>();
						parametroMapTemp.putAll(parametroMap);
						parametroMapTemp.putAll(respuestaLecturaArchivoVO.getPropiedadMap());
						if (StringUtil.isNullOrEmpty(
								parametroMapTemp.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD))) {
							parametroMapTemp.put(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD,
									respuestaLecturaArchivoVO.getNumeroReferencia());
						}
						String nomenclaturaTemp = obtenerNomenclatura(objData, parametroMapTemp, isManual, false);
						nomenclaturaMap.put(objData.getNombreTabla(), nomenclaturaTemp);
					}
					ParticionarArchivoExcel.ejecutarParticionExcel(nomenclaturaMap, fTPVO);
				}
			}
		} catch (Exception e) {
			log.error("Error ", e);
		}
	}

	private boolean generarTramaParticionada(List<ConfiguracionTrama> listaConfiguracionTrama,
			Map<String, Object> parametroMap) {
		boolean resultado = true;
		Map<String, List<ConfiguracionTrama>> listaKeyGrupoMap = new HashMap<String, List<ConfiguracionTrama>>();
		for (ConfiguracionTrama configuracionTrama : listaConfiguracionTrama) {
			String keyGrupo = configuracionTrama.getJuegoTrama().getIdJuegoTrama() + "";
			Long idTramaNomenclaturaArchivoPadre = configuracionTrama.getJuegoTrama()
					.getIdTramaNomenclaturaArchivoPadre();
			if (!StringUtil.isNullOrEmptyNumeric(idTramaNomenclaturaArchivoPadre)) {
				if (!listaKeyGrupoMap.containsKey(keyGrupo)) {
					List<ConfiguracionTrama> value = new ArrayList<ConfiguracionTrama>();
					value.add(configuracionTrama);
					listaKeyGrupoMap.put(keyGrupo, value);
				} else {
					List<ConfiguracionTrama> value = listaKeyGrupoMap.get(keyGrupo);
					value.add(configuracionTrama);
					listaKeyGrupoMap.put(keyGrupo, value);
				}
			}
		}
		for (Map.Entry<String, List<ConfiguracionTrama>> keyGrupoOrdenParticionarMap : listaKeyGrupoMap.entrySet()) {
			List<ConfiguracionTrama> listaConfiguracionTramaTmp = keyGrupoOrdenParticionarMap.getValue();
			ejecutarParticionExcel(listaConfiguracionTramaTmp, parametroMap);
		}
		return resultado;
	}

	private List<ResultadoProcesoConfiguracionTramaVO> procesarConfiguracionTrama(
			List<ConfiguracionTrama> listaConfiguracionTramaTemp, Map<String, Object> parametroMap, boolean isManual,
			boolean validadExclusion, EntityManager sessionHibernate) throws Exception {
		// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
		if (EjecutarActividadParaleloInstance.getInstance()
				.isBloquearProceso(parametroMap.get("idControlProceso") + "")) {
			EjecutarActividadParaleloInstance.getInstance()
					.removeBloquearProceso(parametroMap.get("idControlProceso") + "");
			return new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
		}
		// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
		String rutaRelativaTemp = parametroMap.get("rutaRelativaTemp") + "";
		List<ResultadoProcesoConfiguracionTramaVO> resultado = new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
		Map<Long, Long> idNomenclaturaArchivoByConfigTrama = new HashMap<Long, Long>();
		if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
			for (ConfiguracionTrama objConf : listaConfiguracionTramaTemp) {
				idNomenclaturaArchivoByConfigTrama.put(objConf.getIdConfiguradorTrama(),
						objConf.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo());
			}
		}
		ConfiguracionTramaIdProcesarVO objConf = new ConfiguracionTramaIdProcesarVO(listaConfiguracionTramaTemp);
		objConf.setListaLogConfiguracionTramaAyuda(
				new BigMemoryManager<String, LogConfiguracionTrama>(rutaRelativaTemp, "data_log_ayuda"));
		// Inicio completar datos by id
		objConf = completarIdProcesamiento(objConf);
		// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
		if (EjecutarActividadParaleloInstance.getInstance()
				.isBloquearProceso(parametroMap.get("idControlProceso") + "")) {
			EjecutarActividadParaleloInstance.getInstance()
					.removeBloquearProceso(parametroMap.get("idControlProceso") + "");
			return new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
		}
		// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
		// Fin completar datos by id
		// Inicio particionar archivos si es que se tiene configurador
		generarTramaParticionada(objConf.getListaConfiguracionTrama(), parametroMap);
		// Fin particionar archivos si es que se tiene configurador
		// Inicio generado juego trama
		objConf = generarJuegoTramaProcesar(objConf, parametroMap, isManual, validadExclusion);
		// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
		if (EjecutarActividadParaleloInstance.getInstance()
				.isBloquearProceso(parametroMap.get("idControlProceso") + "")) {
			EjecutarActividadParaleloInstance.getInstance()
					.removeBloquearProceso(parametroMap.get("idControlProceso") + "");
			return new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
		}
		// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
		log.error("Procesando.procesarConfiguracionTrama(listaJuegos = " + objConf.getListaKeyGrupo().size()
				+ " parametroMap = " + parametroMap.toString() + ") ");
		// Fin generado juego trama
		objConf = procesarGrupoJuegoTramaExlusion(objConf, parametroMap, isManual, validadExclusion);
		Collections.sort(objConf.getListaKeyGrupo());
		if (objConf.getListaLogConfiguracionTramaAyuda().getListaKey().size() > 0) {
			registrarLogConfiguracionDataAyudaGrupo(parametroMap, objConf.getListaLogConfiguracionTramaAyuda(),
					sessionHibernate);
			// TIPIFICACION ERROR SIN TRAMA CASO: CUANDO NO SE PUDO CONECTAR AL FTP O NO SE
			// ENCONTRO ARCHIVO EN FTP
			for (String objLogKey : objConf.getListaLogConfiguracionTramaAyuda().getListaKey()) {
				LogConfiguracionTrama objLog = objConf.getListaLogConfiguracionTramaAyuda().get(objLogKey);
				if (objLog.getDescripcionError().contains(ConstanteConfiguracionTramaUtil.ARTIFICIO_NO_HAY_TRAMAS)) {
					objLog.setDescripcionError(objLog.getDescripcionError()
							.replace(ConstanteConfiguracionTramaUtil.ARTIFICIO_NO_HAY_TRAMAS, "").trim());
					resultado.add(generarObjetoError(objLog.getNombreCampo(), objLog.getFila(),
							ClaseErrorOrquestadorType.NO_CONEXION_FTP_ARCHIVO.getKey(), objLog.getDescripcionError(),
							(Long) parametroMap.get("idJuegoTrama"), objLog.getConfiguracionTrama(),
							idNomenclaturaArchivoByConfigTrama.get(objLog.getConfiguracionTrama()), "H"));
				} else if (objLog.getDescripcionError()
						.contains(ConstanteConfiguracionTramaUtil.ARTIFICIO_ERROR_NOMENCLATURA)
						|| objLog.getDescripcionError()
								.contains(ConstanteConfiguracionTramaUtil.ARTIFICIO_ERROR_TECNICO)) {
					objLog.setDescripcionError(objLog.getDescripcionError()
							.replace(ConstanteConfiguracionTramaUtil.ARTIFICIO_ERROR_NOMENCLATURA, "").trim());
					objLog.setDescripcionError(objLog.getDescripcionError()
							.replace(ConstanteConfiguracionTramaUtil.ARTIFICIO_ERROR_TECNICO, "").trim());
					resultado.add(generarObjetoError(objLog.getNombreCampo(), objLog.getFila(),
							ClaseErrorOrquestadorType.FTP.getKey(), objLog.getDescripcionError(),
							(Long) parametroMap.get("idJuegoTrama"), objLog.getConfiguracionTrama(),
							idNomenclaturaArchivoByConfigTrama.get(objLog.getConfiguracionTrama()),
							TipoErrorOrquestadorType.TECNICO.getKey()));
				} else {
					resultado.add(generarObjetoError(objLog.getNombreCampo(), objLog.getFila(),
							ClaseErrorOrquestadorType.NO_CONEXION_FTP_ARCHIVO.getKey(), objLog.getDescripcionError(),
							(Long) parametroMap.get("idJuegoTrama"), objLog.getConfiguracionTrama(),
							idNomenclaturaArchivoByConfigTrama.get(objLog.getConfiguracionTrama()), null));
				}
			}
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				return resultado;
			}
		}
		// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
		if (EjecutarActividadParaleloInstance.getInstance()
				.isBloquearProceso(parametroMap.get("idControlProceso") + "")) {
			EjecutarActividadParaleloInstance.getInstance()
					.removeBloquearProceso(parametroMap.get("idControlProceso") + "");
			return new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
		}
		// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
		registrarControlProcesoJuego(objConf.getListaKeyGrupo(), parametroMap);
		for (String keyGrupoOrden : objConf.getListaKeyGrupo()) {
			// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
			if (EjecutarActividadParaleloInstance.getInstance()
					.isBloquearProceso(parametroMap.get("idControlProceso") + "")) {
				EjecutarActividadParaleloInstance.getInstance()
						.removeBloquearProceso(parametroMap.get("idControlProceso") + "");
				return new ArrayList<ResultadoProcesoConfiguracionTramaVO>();
			}
			// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
			// Procesando juegos y grupos de archivos
			Map<String, Map<String, Map<String, Object>>> grupoArchivoProcesarMap = objConf
					.getListaRespuestaLecturaArchivoTemporalGrupoMap().get(keyGrupoOrden);
			List<String> listaKeyNumeroSolicitudOrden = new ArrayList<String>(grupoArchivoProcesarMap.keySet());
			Collections.sort(listaKeyNumeroSolicitudOrden);
			Map<String, ConfiguracionTrama> listaKeyNumeroSolicitudOrdenExcluirMap = new HashMap<>();
			listaKeyNumeroSolicitudOrdenExcluirMap = validarExlusionNumeroSolicitud(objConf,
					listaKeyNumeroSolicitudOrden, validadExclusion, keyGrupoOrden, grupoArchivoProcesarMap);
			// procesando by numero de referencia
			for (String keyOrden : listaKeyNumeroSolicitudOrden) {
				Map<String, ResultadoProcesoConfiguracionTramaVO> resultadoProcesoConfiguracionTramaVOMap = new HashMap<String, ResultadoProcesoConfiguracionTramaVO>();
				Map<String, TasmctrLoteMpe> tasmctrLoteMpeMap = new HashMap<String, TasmctrLoteMpe>();
				Map<String, Map<String, TasmctrTramaMpe>> tasmctrTramaGrupoMpeMap = new HashMap<String, Map<String, TasmctrTramaMpe>>();
				// leer en temporal
				Map<String, RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoTemporalMap = new HashMap<String, RespuestaLecturaArchivoVO>();
				Map<String, String> nombreNomenclaturaArchivoTramaMap = new HashMap<String, String>();
				Map<String, RespuestaLecturaArchivoVO> listaResultadoValidacionEstructuraMap = new HashMap<String, RespuestaLecturaArchivoVO>();
				Map<String, RespuestaLecturaArchivoVO> nomenclaturaArchivoProcesadoMap = new HashMap<String, RespuestaLecturaArchivoVO>();
				Map<String, Map<String, Object>> listaArchivoNomenclaturaProcesarTempMap = grupoArchivoProcesarMap
						.get(keyOrden);
				Map<String, Map<String, EstadoProcesamientoConfiguracionTramaState>> listaGrupoConfiguracionTramaValidarMap = obtenerListaGrupoConfiguracionTramaValidarMap(
						objConf.getListaConfiguracionTrama());

				Map<String, BigMemoryManager<String, TasmeErrorMpe>> listaLogConfiguracionTramaGrupo = new HashMap<String, BigMemoryManager<String, TasmeErrorMpe>>();
				Map<String, BigMemoryManager<String, LogConfiguracionTrama>> listaLogConfiguracionTramaAyudaGrupo = new HashMap<String, BigMemoryManager<String, LogConfiguracionTrama>>();

				Map<String, GrupoConfiguracionTramaVO> listaGrupoConfiguracionTramaMap = new HashMap<String, GrupoConfiguracionTramaVO>();

				// Inicio procesando por juego
				// for (Map.Entry<String,List<ConfiguracionTramaDTO>> juegoTramaMap :
				// juegoConfiguracionTramaMap.entrySet()) {
				// Inicio extraer archivo y leer temporal
				if (listaKeyNumeroSolicitudOrdenExcluirMap.containsKey(keyOrden)) {
					ConfiguracionTrama configuracionTrama = listaKeyNumeroSolicitudOrdenExcluirMap.get(keyOrden);
					String keyGrupo = configuracionTrama.getJuegoTrama().getIdJuegoTrama() + "";
					String keyNomenclatura = configuracionTrama.getTramaNomenclaturaArchivo()
							.getIdTramaNomenclaturaArchivo() + "";
					Map<String, Object> parametroProcesadoMap = new HashMap<String, Object>();
					if (listaArchivoNomenclaturaProcesarTempMap.get(keyNomenclatura) != null) {
						parametroProcesadoMap.putAll(listaArchivoNomenclaturaProcesarTempMap.get(keyNomenclatura));
					}
					ResultadoProcesoConfiguracionTramaVO resulTemp = new ResultadoProcesoConfiguracionTramaVO();
					if (!resultadoProcesoConfiguracionTramaVOMap.containsKey(keyGrupo)) {
						resultadoProcesoConfiguracionTramaVOMap.put(keyGrupo, resulTemp);
						resulTemp.setIdConfigJuegoTrama(configuracionTrama.getJuegoTrama().getIdJuegoTrama());
						if (parametroProcesadoMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)) {
							resulTemp.setNumeroPoliza(
									parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)
											+ "");
						}
						if (parametroProcesadoMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)) {
							resulTemp.setNumeroReferencia(
									parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD) + "");
						}
						// Inicio mejora fecha calculada
						if (parametroProcesadoMap
								.containsKey(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO)) {
							if (parametroProcesadoMap
									.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO) != null) {
								try {
									Date fechaCalculada = FechaUtil.obtenerFechaFormatoPersonalizado(
											parametroProcesadoMap.get(
													ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO) + "",
											FechaUtil.DATE_DMY);
									resulTemp.setFechaCalculadaParametro(fechaCalculada);
								} catch (Exception e) {
									log.error("Error ", e);
								}
							}
						}
						// Fin mejora fecha calculada
						String nomenclaturaExcluir = parametroProcesadoMap
								.get(ConstanteConfiguracionTramaUtil.NOMENCLATURA) + "";
						// TIPIFICACION ERROR SIN TRAMA CASO: CUANDO EL ARCHIVO DE TRAMA YA FUE
						// PROCESADO
						resultado.add(generarObjetoError("", "",
								ClaseErrorOrquestadorType.TRAMA_PROCESADA_ANTERIORMENTE.getKey(),
								"El archivo con la nomenclatura " + nomenclaturaExcluir + " ya fue procesada",
								(Long) parametroMap.get("idJuegoTrama"), configuracionTrama.getIdConfiguradorTrama(),
								configuracionTrama.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo(),
								null));
					}
					// limpiar memoria
					parametroProcesadoMap = null;
					continue;
				}
				for (ConfiguracionTrama obj : objConf.getJuegoConfiguracionTramaMap().get(keyGrupoOrden)) {
					String rutaRelativaTempByConf = ConstanteConfigUtil.generarRuta(rutaRelativaTemp)
							+ obj.getIdConfiguradorTrama() + "";
					parametroMap.put("rutaRelativaTemp", rutaRelativaTempByConf);
					String keyConfiguracionTrama = obj.getIdConfiguradorTrama() + "";
					String keyGrupo = obj.getJuegoTrama().getIdJuegoTrama() + "";
					String keyNomenclatura = obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo() + "";
					Map<String, Object> parametroProcesadoMap = new HashMap<String, Object>();
					if (listaArchivoNomenclaturaProcesarTempMap.get(keyNomenclatura) != null) {
						parametroProcesadoMap.putAll(listaArchivoNomenclaturaProcesarTempMap.get(keyNomenclatura));
					}
					ResultadoProcesoConfiguracionTramaVO resulTemp = new ResultadoProcesoConfiguracionTramaVO();
					try {
						if (!resultadoProcesoConfiguracionTramaVOMap.containsKey(keyGrupo)) {
							resultadoProcesoConfiguracionTramaVOMap.put(keyGrupo, resulTemp);
							resulTemp.setIdConfigJuegoTrama(obj.getJuegoTrama().getIdJuegoTrama());
							resulTemp.setIdConfiguracionTrama(obj.getIdConfiguradorTrama());
							resulTemp.setIdNomenclaturaArchivo(
									obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo());
							if (parametroProcesadoMap
									.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)) {
								resulTemp.setNumeroPoliza(
										parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)
												+ "");
							}
							if (parametroProcesadoMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)) {
								resulTemp.setNumeroReferencia(
										parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)
												+ "");
							}
							// Inicio mejora fecha calculada
							if (parametroProcesadoMap
									.containsKey(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO)) {
								if (parametroProcesadoMap
										.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO) != null) {
									try {
										Date fechaCalculada = FechaUtil.obtenerFechaFormatoPersonalizado(
												parametroProcesadoMap.get(
														ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO) + "",
												FechaUtil.DATE_DMY);
										resulTemp.setFechaCalculadaParametro(fechaCalculada);
									} catch (Exception e) {
										log.error("Error ", e);
									}
								}
							}
							// Fin mejora fecha calculada

							resultado.add(resulTemp);
						} else {
							resulTemp = resultadoProcesoConfiguracionTramaVOMap.get(keyGrupo);
						}
						// INICIO TASM CONTROL LOTE POR CAUSA DE OBTENER LA FECHAS DE PROCESAMIENTO DE
						// INICIO LO MAS EXACTO
						// para empezar
						if (!tasmctrLoteMpeMap.containsKey(keyGrupo)) {
							TasmctrLoteMpe resultadoTasmctrlMpeTemp = obtenerTasmeCtrLote(keyGrupo,
									objConf.getListaDepenciaTablaMixtaDataMap(),
									listaRespuestaLecturaArchivoTemporalMap, parametroMap, obj);
							resulTemp.setFechaLote(resultadoTasmctrlMpeTemp.getTasmctrLoteMpePK().getFechaLote());
							resulTemp.setNumeroLote(resultadoTasmctrlMpeTemp.getTasmctrLoteMpePK().getNumeroLote());
							tasmctrLoteMpeMap.put(keyGrupo, resultadoTasmctrlMpeTemp);
						}
						// FIN TASM CONTROL LOTE
						// para empezar tams control trama
						if (!tasmctrTramaGrupoMpeMap.containsKey(keyGrupo)) {
							String keyTablaMixtaTrama = keyGrupo
									+ ConstanteConfiguracionTramaUtil.TASMCTRTRAMA_MPE.toUpperCase() + keyNomenclatura;
							String keyTramaTrama = objConf.getListaDepenciaTablaMixtaDataMap().get(keyTablaMixtaTrama);
							Map<String, TasmctrTramaMpe> tasmctrTramaMpeMap = obtenerTasmctrTrama(keyGrupo,
									keyTramaTrama, keyNomenclatura, obj, listaRespuestaLecturaArchivoTemporalMap,
									tasmctrLoteMpeMap, nombreNomenclaturaArchivoTramaMap);
							tasmctrTramaGrupoMpeMap.put(keyGrupo, tasmctrTramaMpeMap);
						} else {
							// si es el trama que estoy procesando
							String keyTablaMixtaTrama = keyGrupo
									+ ConstanteConfiguracionTramaUtil.TASMCTRTRAMA_MPE.toUpperCase() + keyNomenclatura;
							String keyTramaTrama = objConf.getListaDepenciaTablaMixtaDataMap().get(keyTablaMixtaTrama);
							Map<String, TasmctrTramaMpe> tasmctrTramaMpeMap = tasmctrTramaGrupoMpeMap.get(keyGrupo);
							if (!tasmctrTramaMpeMap.containsKey(keyNomenclatura)) {
								tasmctrTramaMpeMap.putAll(obtenerTasmctrTrama(keyGrupo, keyTramaTrama, keyNomenclatura,
										obj, listaRespuestaLecturaArchivoTemporalMap, tasmctrLoteMpeMap,
										nombreNomenclaturaArchivoTramaMap));
								tasmctrTramaGrupoMpeMap.put(keyGrupo, tasmctrTramaMpeMap);
							} else {
								if (isCampoNegocio(obj)) {
									TasmctrTramaMpe resultadoTasmctrTramaMpeTemp = tasmctrTramaMpeMap
											.get(keyNomenclatura);
									resultadoTasmctrTramaMpeTemp.setIdConfiguradorTrama(obj.getIdConfiguradorTrama());
								}
							}
						}
						// FIN TASM CONTROL TRAMA

						objConf.getListaConfiguracionTramaTablaMap().put(keyConfiguracionTrama, obj.getNombreTabla());
						// Inicio optimizando lectura de archivo
						String keyOptimizar = keyNomenclatura + "" + keyGrupo;
						RespuestaLecturaArchivoVO resultadoValidacionEstructuraTemp = null;
						RespuestaLecturaArchivoVO resultadoValidacionEstructura = null;
						if (!nomenclaturaArchivoProcesadoMap.containsKey(keyOptimizar)) {
							resultadoValidacionEstructuraTemp = validarPrimerNivelEstructruraTramaNomenclatura(
									parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NOMENCLATURA) + "", obj,
									parametroMap, isManual);// TODO: NOMENCLATURA
							nomenclaturaArchivoProcesadoMap.put(keyOptimizar, resultadoValidacionEstructuraTemp);
							resultadoValidacionEstructura = (RespuestaLecturaArchivoVO) BeanUtils
									.cloneBean(resultadoValidacionEstructuraTemp);
							BigMemoryManager<String, LogConfiguracionTrama> listaLogConfiguracionTramaAyuda = new BigMemoryManager<String, LogConfiguracionTrama>(
									rutaRelativaTempByConf, "validar_nomenclatura_data_log_ayuda");
							listaLogConfiguracionTramaAyuda
									.addAll(resultadoValidacionEstructuraTemp.getListaLogConfiguracionTramaAyuda());
							resultadoValidacionEstructura
									.setListaLogConfiguracionTramaAyuda(listaLogConfiguracionTramaAyuda);
						} else {
							resultadoValidacionEstructura = (RespuestaLecturaArchivoVO) BeanUtils
									.cloneBean(nomenclaturaArchivoProcesadoMap.get(keyOptimizar));
							BigMemoryManager<String, LogConfiguracionTrama> listaLogConfiguracionTramaAyuda = new BigMemoryManager<String, LogConfiguracionTrama>(
									rutaRelativaTempByConf, "validar_nomenclatura_data_log_ayuda");
							listaLogConfiguracionTramaAyuda.addAll(nomenclaturaArchivoProcesadoMap.get(keyOptimizar)
									.getListaLogConfiguracionTramaAyuda());
							resultadoValidacionEstructura
									.setListaLogConfiguracionTramaAyuda(listaLogConfiguracionTramaAyuda);
						}
						// Fin optimizando lectura de archivo
						listaResultadoValidacionEstructuraMap.put(keyConfiguracionTrama, resultadoValidacionEstructura);
						if (!resultadoValidacionEstructura.isEsError()) {
							RespuestaLecturaArchivoVO respuestaLecturaArchivoVO = leerConfiguracionTramaDataTemporal(
									obj, objConf.getCamposDiponibleTablaMap().get(obj.getNombreTabla()),
									resultadoValidacionEstructura.getNomenclaturaLeer(), parametroMap);
							listaRespuestaLecturaArchivoTemporalMap.put(keyConfiguracionTrama,
									respuestaLecturaArchivoVO);
						} else {
							for (String logConfiguracionTramaDTOKey : resultadoValidacionEstructura
									.getListaLogConfiguracionTramaAyuda().getListaKey()) {
								LogConfiguracionTrama objLog = resultadoValidacionEstructura
										.getListaLogConfiguracionTramaAyuda().get(logConfiguracionTramaDTOKey);
								objLog.setIdLogConfiguracionTrama(UUIDUtil.generarUUID());
								objLog.setConfiguracionTrama(obj.getIdConfiguradorTrama());
								resultadoValidacionEstructura.getListaLogConfiguracionTramaAyuda()
										.update(logConfiguracionTramaDTOKey, objLog);
							}
						}
						keyNomenclatura = obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo() + "";
						if (!nombreNomenclaturaArchivoTramaMap.containsKey(keyNomenclatura)
								&& !StringUtil.isNullOrEmpty(resultadoValidacionEstructura.getNomenclaturaLeer())) {
							nombreNomenclaturaArchivoTramaMap.put(keyNomenclatura,
									resultadoValidacionEstructura.getNomenclaturaLeer());
						}
					} catch (Exception e) {
						log.error("Error ", e);
						resulTemp.setEsError(true);
						resulTemp.setCodigoError("LEC_E006");
						resulTemp.setMensajeError("Error : Excepcion no controlada idConfiguradorTrama: "
								+ obj.getIdConfiguradorTrama() + " " + e.getMessage());

					}
					// limpiando memoria
					parametroProcesadoMap = null;
				}
				// Fin extraer archivo y leer temporal
				// Inicio parseo y procesamiento de los archivos
				for (ConfiguracionTrama obj : objConf.getJuegoConfiguracionTramaMap().get(keyGrupoOrden)) {
					String rutaRelativaTempByConf = ConstanteConfigUtil.generarRuta(rutaRelativaTemp)
							+ obj.getIdConfiguradorTrama() + "";
					parametroMap.put("rutaRelativaTemp", rutaRelativaTempByConf);
					String keyGrupo = obj.getJuegoTrama().getIdJuegoTrama() + "";
					String keyConfiguracionTrama = obj.getIdConfiguradorTrama() + "";
					String keyNomenclatura = obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo() + "";
					ResultadoProcesoConfiguracionTramaVO resulTemp = new ResultadoProcesoConfiguracionTramaVO();
					Map<String, Object> parametroProcesadoMap = listaArchivoNomenclaturaProcesarTempMap
							.get(keyNomenclatura);
					try {
						String nombreTabla = obj.getNombreTabla().replace(".", ";");
						String[] schemaTableName = nombreTabla.split(";");
						if (!resultadoProcesoConfiguracionTramaVOMap.containsKey(keyGrupo)) {
							resultadoProcesoConfiguracionTramaVOMap.put(keyGrupo, resulTemp);
							resulTemp.setIdConfigJuegoTrama(obj.getJuegoTrama().getIdJuegoTrama());
							resulTemp.setIdConfiguracionTrama(obj.getIdConfiguradorTrama());
							resulTemp.setIdNomenclaturaArchivo(
									obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo());
							if (parametroProcesadoMap
									.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)) {
								resulTemp.setNumeroPoliza(
										parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)
												+ "");
							}
							if (parametroProcesadoMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)) {
								resulTemp.setNumeroReferencia(
										parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)
												+ "");
							}
							// Inicio mejora fecha calculada
							if (parametroProcesadoMap
									.containsKey(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO)) {
								if (parametroProcesadoMap
										.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO) != null) {
									try {
										Date fechaCalculada = FechaUtil.obtenerFechaFormatoPersonalizado(
												parametroProcesadoMap.get(
														ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO) + "",
												FechaUtil.DATE_DMY);
										resulTemp.setFechaCalculadaParametro(fechaCalculada);
									} catch (Exception e) {
										log.error("Error ", e);
									}
								}
							}
							// Fin mejora fecha calculada
							resultado.add(resulTemp);
						} else {
							resulTemp = resultadoProcesoConfiguracionTramaVOMap.get(keyGrupo);
						}

						// Inicio de configuracion de log de errores
						if (!objConf.getListaConfiguracionTramaErrorGrupoMap().containsKey(keyGrupo)) {
							if (objConf.getListaConfiguracionTramaErrorMap()
									.containsKey(obj.getIdConfiguradorTrama())) {
								Map<Long, Map<String, String>> value = new HashMap<Long, Map<String, String>>();
								value.put(obj.getIdConfiguradorTrama(),
										objConf.getListaConfiguracionTramaErrorMap().get(obj.getIdConfiguradorTrama()));
								objConf.getListaConfiguracionTramaErrorGrupoMap().put(keyGrupo, value);
							}
						} else {
							if (objConf.getListaConfiguracionTramaErrorMap()
									.containsKey(obj.getIdConfiguradorTrama())) {
								Map<Long, Map<String, String>> value = objConf.getListaConfiguracionTramaErrorGrupoMap()
										.get(keyGrupo);
								value.put(obj.getIdConfiguradorTrama(),
										objConf.getListaConfiguracionTramaErrorMap().get(obj.getIdConfiguradorTrama()));
								objConf.getListaConfiguracionTramaErrorGrupoMap().put(keyGrupo, value);
							}
						}
						// Fin de configuracion de log de errores
						// Inicio obteniendo campos no persistente
						if (!objConf.getListaConfiguracionTramaCampoNoPersistenteGrupoMap().containsKey(keyGrupo)) {
							if (objConf.getListaConfiguracionTramaCampoNoPersistenteMap()
									.containsKey(obj.getIdConfiguradorTrama())) {
								Map<Long, Map<String, String>> value = new HashMap<Long, Map<String, String>>();
								value.put(obj.getIdConfiguradorTrama(),
										objConf.getListaConfiguracionTramaCampoNoPersistenteMap()
												.get(obj.getIdConfiguradorTrama()));
								objConf.getListaConfiguracionTramaCampoNoPersistenteGrupoMap().put(keyGrupo, value);
							}
						} else {
							if (objConf.getListaConfiguracionTramaCampoNoPersistenteMap()
									.containsKey(obj.getIdConfiguradorTrama())) {
								Map<Long, Map<String, String>> value = objConf
										.getListaConfiguracionTramaCampoNoPersistenteGrupoMap().get(keyGrupo);
								value.put(obj.getIdConfiguradorTrama(),
										objConf.getListaConfiguracionTramaCampoNoPersistenteMap()
												.get(obj.getIdConfiguradorTrama()));
								objConf.getListaConfiguracionTramaCampoNoPersistenteGrupoMap().put(keyGrupo, value);
							}
						}
						// Fin obteniendo campos no persistente

						// datos para tascrtl..
						// datos para tascrtl
						Map<String, String> listaDependenicaDataMap = objConf.getListaDepenciaDataMap()
								.get(keyConfiguracionTrama);
						if (listaDependenicaDataMap == null) {
							listaDependenicaDataMap = new HashMap<String, String>();
						}
						Map<String, RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoTemporalProcesarMap = new HashMap<String, RespuestaLecturaArchivoVO>();
						for (Map.Entry<String, String> listaDependenicaData : listaDependenicaDataMap.entrySet()) {
							listaRespuestaLecturaArchivoTemporalProcesarMap.put(listaDependenicaData.getKey(),
									listaRespuestaLecturaArchivoTemporalMap.get(listaDependenicaData.getKey()));
						}
						listaRespuestaLecturaArchivoTemporalProcesarMap.put(keyConfiguracionTrama,
								listaRespuestaLecturaArchivoTemporalMap.get(keyConfiguracionTrama));
						// INICIO TASM CONTROL LOTE
						// para empezar
						if (tasmctrLoteMpeMap.containsKey(keyGrupo)) {
							TasmctrLoteMpe resultadoTasmctrlMpeTemp = tasmctrLoteMpeMap.get(keyGrupo);
							String keyTablaMixtaLote = keyGrupo
									+ ConstanteConfiguracionTramaUtil.TASMCTRLOTE_MPE.toUpperCase();
							String keyTramaLote = objConf.getListaDepenciaTablaMixtaDataMap().get(keyTablaMixtaLote);
							resultadoTasmctrlMpeTemp = obtenerTasmctrLoteMpe(
									listaRespuestaLecturaArchivoTemporalMap.get(keyTramaLote), resultadoTasmctrlMpeTemp,
									obj.getJuegoTrama().getProducto());
							tasmctrLoteMpeMap.put(keyGrupo, resultadoTasmctrlMpeTemp);
							resulTemp.setFechaLote(resultadoTasmctrlMpeTemp.getTasmctrLoteMpePK().getFechaLote());
							resulTemp.setNumeroLote(resultadoTasmctrlMpeTemp.getTasmctrLoteMpePK().getNumeroLote());
						}
						// FIN TASM CONTROL LOTE
						// para empezar
						if (tasmctrTramaGrupoMpeMap.containsKey(keyGrupo)) {
							// si es el trama que estoy procesando
							Map<String, TasmctrTramaMpe> tasmctrTramaMpeMap = tasmctrTramaGrupoMpeMap.get(keyGrupo);
							if (tasmctrTramaMpeMap.containsKey(keyNomenclatura)) {
								String keyTablaMixtaTrama = keyGrupo
										+ ConstanteConfiguracionTramaUtil.TASMCTRTRAMA_MPE.toUpperCase()
										+ keyNomenclatura;
								String keyTramaTrama = objConf.getListaDepenciaTablaMixtaDataMap()
										.get(keyTablaMixtaTrama);
								TasmctrTramaMpe resultadoTasmctrTramaMpeTemp = tasmctrTramaMpeMap.get(keyNomenclatura);
								resultadoTasmctrTramaMpeTemp = obtenerTasmctrTramaMpe(
										listaRespuestaLecturaArchivoTemporalMap.get(keyTramaTrama),
										tasmctrLoteMpeMap.get(keyGrupo), resultadoTasmctrTramaMpeTemp,
										nombreNomenclaturaArchivoTramaMap.get(keyNomenclatura),
										obj.getTramaNomenclaturaArchivo().getAcronimoTipoTrama());
								resultadoTasmctrTramaMpeTemp.getTasmctrTramaMpePK().setFechaLote(
										tasmctrLoteMpeMap.get(keyGrupo).getTasmctrLoteMpePK().getFechaLote());
								resultadoTasmctrTramaMpeTemp.getTasmctrTramaMpePK().setNumeroLote(
										tasmctrLoteMpeMap.get(keyGrupo).getTasmctrLoteMpePK().getNumeroLote());
								resultadoTasmctrTramaMpeTemp
										.setNombreTramaInput(nombreNomenclaturaArchivoTramaMap.get(keyNomenclatura));
								if (isCampoNegocio(obj)) {
									resultadoTasmctrTramaMpeTemp.setIdConfiguradorTrama(obj.getIdConfiguradorTrama());
								}
								tasmctrTramaMpeMap.put(keyNomenclatura, resultadoTasmctrTramaMpeTemp);
								tasmctrTramaGrupoMpeMap.put(keyGrupo, tasmctrTramaMpeMap);
							}
						}
						// FIN TASM CONTROL TRAMA
						RespuestaLecturaArchivoVO resultadoValidacionEstructura = listaResultadoValidacionEstructuraMap
								.get(keyConfiguracionTrama);
						if (!resultadoValidacionEstructura.isEsError()) {
							// proceder a leer
							boolean isConfiguradorMap = listaGrupoConfiguracionTramaMap.containsKey(keyGrupo);
							GrupoConfiguracionTramaVO grupoConfiguracionTramaVO = new GrupoConfiguracionTramaVO();
							if (isConfiguradorMap) {
								grupoConfiguracionTramaVO = listaGrupoConfiguracionTramaMap.get(keyGrupo);
							}
							grupoConfiguracionTramaVO.setNombreConfiguracion(keyGrupo);
							GrupoConfiguracionTramaVO configuracionTramaCnfVO = new GrupoConfiguracionTramaVO();
							configuracionTramaCnfVO.setNombreConfiguracion(keyConfiguracionTrama + "");
							configuracionTramaCnfVO.setNombreTabla(schemaTableName[1].toUpperCase());
							configuracionTramaCnfVO.setEsObligatorio(RespuestaNaturalType.get(obj.getObligatorio()));
							// aa
							TasmctrLoteMpe tasmctrlMpeTemp = tasmctrLoteMpeMap.get(keyGrupo);
							ConfiguracionTramaIdProcesarVO configuracionTramaIdProcesarEquivalencia = new ConfiguracionTramaIdProcesarVO();
							configuracionTramaIdProcesarEquivalencia.setEquivalenciaHomologacionSimpleSinDependenciaMap(
									objConf.getEquivalenciaHomologacionSimpleSinDependenciaMap());
							configuracionTramaIdProcesarEquivalencia.setEquivalenciaHomologacionSimpleConDependenciaMap(
									objConf.getEquivalenciaHomologacionSimpleConDependenciaMap());
							configuracionTramaIdProcesarEquivalencia.setEquivalenciaHomologacionCanculadoMap(
									objConf.getEquivalenciaHomologacionCanculadoMap());

							RespuestaLecturaArchivoVO respuestaLecturaArchivoVO = parsearConfiguracionTramaData(
									configuracionTramaIdProcesarEquivalencia,
									listaRespuestaLecturaArchivoTemporalProcesarMap, obj,
									objConf.getCamposDiponibleTablaMap().get(obj.getNombreTabla()),
									resultadoValidacionEstructura.getNomenclaturaLeer(),
									objConf.getListaConfiguracionTramaErrorMap().get(obj.getIdConfiguradorTrama()),
									objConf.getListaConfiguracionTramaCampoNoPersistenteGrupoMap().get(keyGrupo),
									tasmctrlMpeTemp.getTasmctrLoteMpePK().getFechaLote(),
									tasmctrlMpeTemp.getTasmctrLoteMpePK().getNumeroLote(), parametroMap);
							configuracionTramaIdProcesarEquivalencia = null;
							if (respuestaLecturaArchivoVO.isExisteErrorNumLote()) { // ERROR NUMERO DE LOTE AL NO
																					// PROCESAR NUMERO DE LOTE
								resultado.add(generarObjetoError("", "",
										ClaseErrorOrquestadorType.ERROR_NO_GENERO_NUMERO_LOTE.getKey(),
										"NO SE GENERO NUMERO DE LOTE, EN BASE DE DATOS", null,
										respuestaLecturaArchivoVO.getIdConfiguracionTrama(), null, null));
							}
							// Inicio actualizando datos calculados para procesar las dependencias
							RespuestaLecturaArchivoVO respuestaLecturaArchivoActualizarVO = listaRespuestaLecturaArchivoTemporalProcesarMap
									.get(keyConfiguracionTrama);
							// respuestaLecturaArchivoActualizarVO.setListaDataResulBigMap(respuestaLecturaArchivoVO.getListaDataResulBigMap());
							respuestaLecturaArchivoActualizarVO
									.setListaDataResulMap(respuestaLecturaArchivoActualizarVO.getListaDataResulMap());
							listaRespuestaLecturaArchivoTemporalProcesarMap.put(keyConfiguracionTrama,
									respuestaLecturaArchivoActualizarVO);
							// Fin actualizando datos calculados para procesar las dependencias

							// verificando grupos
							Map<String, EstadoProcesamientoConfiguracionTramaState> verificadorGrupo = listaGrupoConfiguracionTramaValidarMap
									.get(keyGrupo);
							verificadorGrupo.put(keyConfiguracionTrama,
									respuestaLecturaArchivoVO.getEstadoProcesamientoConfiguracionTrama());

							boolean isTablaMixta = objConf.getListaTablaMixtaMap()
									.containsKey(schemaTableName[1].toUpperCase());
							if (!isTablaMixta) {
								BigMemoryManager<String, ConfiguracionTramaDataVO> listaConfiguracionTramaData = respuestaLecturaArchivoVO
										.getListaConfiguracionTramaDataVO();
								configuracionTramaCnfVO.setListaConfiguracionTramaData(listaConfiguracionTramaData);
							}
							configuracionTramaCnfVO.setListaLongConfiguracionTramaData(
									respuestaLecturaArchivoVO.getListaLogConfiguracionTrama());
							configuracionTramaCnfVO
									.setCampoMappingFormatoMap(respuestaLecturaArchivoVO.getCampoMappingFormatoMap());

							grupoConfiguracionTramaVO.getListaConfiguracionTrama().add(configuracionTramaCnfVO);

							listaGrupoConfiguracionTramaMap.put(keyGrupo, grupoConfiguracionTramaVO);

							// Inicio Log para enviar por correo
							BigMemoryManager<String, TasmeErrorMpe> valueTasmeError = listaLogConfiguracionTramaGrupo
									.get(keyGrupo);
							BigMemoryManager<String, LogConfiguracionTrama> valueLogAyuda = listaLogConfiguracionTramaAyudaGrupo
									.get(keyGrupo);
							if (valueTasmeError == null) {
								valueTasmeError = new BigMemoryManager<String, TasmeErrorMpe>(rutaRelativaTempByConf,
										"data_log_error");
							}
							if (valueLogAyuda == null) {
								valueLogAyuda = new BigMemoryManager<String, LogConfiguracionTrama>(
										rutaRelativaTempByConf, "data_log_ayuda");
							}
							valueTasmeError.addAll(respuestaLecturaArchivoVO.getListaLogConfiguracionTrama());
							valueLogAyuda.addAll(respuestaLecturaArchivoVO.getListaLogConfiguracionTramaAyuda());
							/*
							 * for (String logConfiguracionTramaDTOKey :
							 * respuestaLecturaArchivoVO.getListaLogConfiguracionTramaAyuda().getListaKey
							 * ()) { LogConfiguracionTramaDTO logConfiguracionTramaDTO =
							 * respuestaLecturaArchivoVO.getListaLogConfiguracionTramaAyuda().get(
							 * logConfiguracionTramaDTOKey); if
							 * (!valueLogAyuda.getListaKeyHelp().contains(logConfiguracionTramaDTO.toString(
							 * ))) { valueLogAyuda.add(logConfiguracionTramaDTO);
							 * valueLogAyuda.getListaKeyHelp().add(logConfiguracionTramaDTO.toString());//
							 * TODO : VER HELP } }
							 */
							listaLogConfiguracionTramaAyudaGrupo.put(keyGrupo, valueLogAyuda);
							listaLogConfiguracionTramaGrupo.put(keyGrupo, valueTasmeError);
							// Inicio Log para enviar por correo

							// verificando errores de procesamiento de obtener numero lote
							TasmctrLoteMpe tasmctrlMpe = tasmctrLoteMpeMap.get(keyGrupo);
							if (!isTablaMixta) {
								Long cantidadRegistros = obtenerCantidadRegistroGrupo(grupoConfiguracionTramaVO);
								tasmctrlMpe.setNroRegistroLeidos(new BigDecimal(cantidadRegistros));
								tasmctrLoteMpeMap.put(keyGrupo, tasmctrlMpe);
							}
							// Inicio Completando datos de tramas calculado
							if (tasmctrTramaGrupoMpeMap.containsKey(keyGrupo)) {
								// si es el trama que estoy procesando
								Map<String, TasmctrTramaMpe> tasmctrTramaMpeMap = tasmctrTramaGrupoMpeMap.get(keyGrupo);
								if (tasmctrTramaMpeMap.containsKey(keyNomenclatura)) {
									if (!isTablaMixta) {
										TasmctrTramaMpe resultadoTasmctrTramaMpeTemp = tasmctrTramaMpeMap
												.get(keyNomenclatura);
										BigDecimal nroRegistrosLeidos = resultadoTasmctrTramaMpeTemp
												.getNroRegistrosLeidos();
										BigDecimal cantidadDataProcesada = new BigDecimal(respuestaLecturaArchivoVO
												.getListaConfiguracionTramaDataVO().getListaKey().size());
										if (nroRegistrosLeidos.compareTo(cantidadDataProcesada) < 0) {
											nroRegistrosLeidos = cantidadDataProcesada;// OBTENEMOS EL MAYOR YA QUE EL
																						// MISMO ARCHIVO SE USA PARA
																						// VARIAS TABLAS
										}
										resultadoTasmctrTramaMpeTemp.setNroRegistrosLeidos(nroRegistrosLeidos);
										if (isCampoNegocio(obj)) {
											resultadoTasmctrTramaMpeTemp
													.setIdConfiguradorTrama(obj.getIdConfiguradorTrama());
										}
										tasmctrTramaMpeMap.put(keyNomenclatura, resultadoTasmctrTramaMpeTemp);
										tasmctrTramaGrupoMpeMap.put(keyGrupo, tasmctrTramaMpeMap);
									}
								}
							}

							// Fin Completando datos de trama calculado
							if (verificarGrupoCompleto(verificadorGrupo)) {
								Map<String, RespuestaNaturalType> verificadorGrupoObligatoridad = objConf
										.getListaGrupoConfiguracionTramaValidarObigatoridadMap().get(keyGrupo);
								RespuestaLecturaArchivoVO respuestaLecturaArchivoObligatoridad = verificarGrupoCompletoObligatoridad(
										verificadorGrupoObligatoridad, verificadorGrupo,
										resultadoValidacionEstructura.getNomenclatura(), parametroMap);
								if (!respuestaLecturaArchivoObligatoridad.isEsError()) {
									Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap = new HashMap<String, ConfiguracionTramaDetalle>();
									for (ConfiguracionTramaDetalle objDet : obj
											.getConfiguracionTramaConfiguracionTramaDetalleList()) {
										String nombeCampoTabla = obtenerCampoPersistenteAsociado(objDet);
										configuracionTramaDetalleMap.put(nombeCampoTabla, objDet);
									}
									RespuestaLecturaArchivoVO respuestaProcesoRegistroVO = registrarTrama(
											grupoConfiguracionTramaVO, tasmctrlMpe,
											tasmctrTramaGrupoMpeMap.get(keyGrupo),
											objConf.getListaConfiguracionTramaErrorGrupoMap().get(keyGrupo),
											sessionHibernate, parametroMap, configuracionTramaDetalleMap);
									if (respuestaProcesoRegistroVO.isEsError()) {
										// Inicio Log para enviar por correo
										valueTasmeError = listaLogConfiguracionTramaGrupo.get(keyGrupo);
										valueLogAyuda = listaLogConfiguracionTramaAyudaGrupo.get(keyGrupo);
										valueTasmeError
												.addAll(respuestaProcesoRegistroVO.getListaLogConfiguracionTrama());

										valueLogAyuda.addAll(
												respuestaProcesoRegistroVO.getListaLogConfiguracionTramaAyuda());
										/*
										 * for (String logConfiguracionTramaDTOKey :
										 * respuestaProcesoRegistroVO.getListaLogConfiguracionTramaAyuda().
										 * getListaKey()) { LogConfiguracionTramaDTO logConfiguracionTramaDTO =
										 * respuestaProcesoRegistroVO.getListaLogConfiguracionTramaAyuda().get(
										 * logConfiguracionTramaDTOKey); if
										 * (!valueLogAyuda.getListaKeyHelp().contains(logConfiguracionTramaDTO.toString(
										 * ))) { valueLogAyuda.add(logConfiguracionTramaDTO);
										 * valueLogAyuda.getListaKeyHelp().add(logConfiguracionTramaDTO.toString());//
										 * TODO : VER HELP } }
										 */
										listaLogConfiguracionTramaGrupo.put(keyGrupo, valueTasmeError);
										listaLogConfiguracionTramaAyudaGrupo.put(keyGrupo, valueLogAyuda);
										// Inicio Log para enviar por correo
										// enviarCorreo(configuracionTrama,
										// listaLogConfiguracionTramaGrupo.get(keyGrupo));
									}
									// registrando log de negocio y de ejecucion
									registrarLogConfiguracionDataGrupo(grupoConfiguracionTramaVO, tasmctrlMpe,
											parametroMap, valueTasmeError, sessionHibernate);
									// DATA TASME ERROR
									agregarTasmeErrorSimulacion(resultado, valueTasmeError, parametroMap);
									Long cantidadRegistrosError = obtenerCantidadErrorRegistroGrupo(valueTasmeError);
									if (tasmctrlMpe != null) {
										tasmctrlMpe.setNroRegistroErrorDeNivel2(new BigDecimal(cantidadRegistrosError));
										tasmctrLoteMpeMap.put(keyGrupo, tasmctrlMpe);
									}
									registrarLogConfiguracionDataAyudaGrupo(parametroMap, valueLogAyuda,
											sessionHibernate);
									// LOG DE AYUDA
									agregarLogAyudaSimulacion(resultado, valueLogAyuda, parametroMap,
											idNomenclaturaArchivoByConfigTrama);
									// limpiando memoria
									listaLogConfiguracionTramaGrupo.remove(keyGrupo);
									listaLogConfiguracionTramaAyudaGrupo.remove(keyGrupo);
									if (!ResourceUtil.esSimulacion(
											parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
										valueTasmeError.clear();
										valueLogAyuda.clear();
									}
									valueTasmeError = null;
									valueLogAyuda = null;
									respuestaLecturaArchivoObligatoridad = null;
								} else {
									resulTemp.setEsError(true);
									resulTemp.setCodigoError("LEC_E003");
									resulTemp.setMensajeError(
											"Error : Archivos obligatorio procesado con error idConfiguradorTrama: "
													+ obj.getIdConfiguradorTrama());

									// TODO:VER_SIMULACION
									if (!ResourceUtil.esSimulacion(
											parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
										// no guardar
										RespuestaLecturaArchivoVO respuestaTasmeL = registrarTasmctrLoteMpe(
												parametroMap, tasmctrlMpe, obj.getIdConfiguradorTrama(),
												sessionHibernate);
										if (respuestaTasmeL.isEsError()) {
											if (!CollectionUtil.isEmpty(respuestaTasmeL
													.getListaLogConfiguracionTramaAyuda().getListaKey())) {
												configuracionTramaCnfVO.getListaLogConfiguracionTramaAyuda()
														.addAll(respuestaTasmeL.getListaLogConfiguracionTramaAyuda());
											}
										}
										RespuestaLecturaArchivoVO respuestaTasmeT = registrarTasmctrTramaMpe(
												parametroMap, tasmctrlMpe, tasmctrTramaGrupoMpeMap.get(keyGrupo),
												sessionHibernate);
										if (respuestaTasmeT.isEsError()) {
											if (!CollectionUtil.isEmpty(respuestaTasmeT
													.getListaLogConfiguracionTramaAyuda().getListaKey())) {
												configuracionTramaCnfVO.getListaLogConfiguracionTramaAyuda()
														.addAll(respuestaTasmeT.getListaLogConfiguracionTramaAyuda());
											}
										}
									}
									// guardando log de procesamiento de data
									valueTasmeError = listaLogConfiguracionTramaGrupo.get(keyGrupo);
									valueLogAyuda = listaLogConfiguracionTramaAyudaGrupo.get(keyGrupo);
									valueTasmeError
											.addAll(configuracionTramaCnfVO.getListaLongConfiguracionTramaData());
									valueLogAyuda.addAll(configuracionTramaCnfVO.getListaLogConfiguracionTramaAyuda());
									/*
									 * for (String logConfiguracionTramaDTOKey :
									 * configuracionTramaCnfVO.getListaLogConfiguracionTramaAyuda().getListaKey() )
									 * { LogConfiguracionTramaDTO logConfiguracionTramaDTO =
									 * configuracionTramaCnfVO.getListaLogConfiguracionTramaAyuda().get(
									 * logConfiguracionTramaDTOKey); if
									 * (!valueLogAyuda.getListaKeyHelp().contains(logConfiguracionTramaDTO.toString(
									 * ))) { valueLogAyuda.add(logConfiguracionTramaDTO);
									 * valueLogAyuda.getListaKeyHelp().add(logConfiguracionTramaDTO.toString());//
									 * TODO : VER HELP } }
									 */
									listaLogConfiguracionTramaGrupo.put(keyGrupo, valueTasmeError);
									listaLogConfiguracionTramaAyudaGrupo.put(keyGrupo, valueLogAyuda);
									// registrarLogConfiguracionDataGrupo(configuracionTramaCnfVO,
									// sessionHibernate);
									// DATA TASME ERROR
									agregarTasmeErrorSimulacion(resultado, valueTasmeError, parametroMap);
									// Inicio Log para enviar por correo
									valueTasmeError.addAll(
											respuestaLecturaArchivoObligatoridad.getListaLogConfiguracionTrama());

									valueLogAyuda.addAll(
											respuestaLecturaArchivoObligatoridad.getListaLogConfiguracionTramaAyuda());
									/*
									 * for (String logConfiguracionTramaDTOKey :
									 * respuestaLecturaArchivoObligatoridad.getListaLogConfiguracionTramaAyuda().
									 * getListaKey()) { LogConfiguracionTramaDTO logConfiguracionTramaDTO =
									 * respuestaLecturaArchivoObligatoridad.getListaLogConfiguracionTramaAyuda().
									 * get(logConfiguracionTramaDTOKey); if
									 * (!valueLogAyuda.getListaKeyHelp().contains(logConfiguracionTramaDTO.toString(
									 * ))) { valueLogAyuda.add(logConfiguracionTramaDTO);
									 * valueLogAyuda.getListaKeyHelp().add(logConfiguracionTramaDTO.toString());//
									 * TODO : VER HELP } }
									 */
									listaLogConfiguracionTramaGrupo.put(keyGrupo, valueTasmeError);
									listaLogConfiguracionTramaAyudaGrupo.put(keyGrupo, valueLogAyuda);

									// registrarLogConfiguracionDataGrupo(configuracionTramaCnfVO,
									// sessionHibernate);

									registrarLogConfiguracionDataAyudaGrupo(parametroMap, valueLogAyuda,
											sessionHibernate);
									// LOG DE AYUDA
									agregarLogAyudaSimulacion(resultado, valueLogAyuda, parametroMap,
											idNomenclaturaArchivoByConfigTrama);
									// Inicio Log para enviar por correo
									// enviarCorreo(configuracionTrama,
									// listaLogConfiguracionTramaGrupo.get(keyGrupo));
									// limpiando memoria
									listaLogConfiguracionTramaGrupo.remove(keyGrupo);
									listaLogConfiguracionTramaAyudaGrupo.remove(keyGrupo);
									valueTasmeError = null;
									valueLogAyuda = null;
									respuestaLecturaArchivoObligatoridad = null;
								}
							}
							respuestaLecturaArchivoVO = null;// liberando memoria
						} else {
							resulTemp.setEsError(true);
							resulTemp.setCodigoError("LEC_E003");
							resulTemp.setMensajeError("Error : Archivo procesado con error idConfiguradorTrama: "
									+ obj.getIdConfiguradorTrama());
							// guardar log
							// verificando grupos
							Map<String, EstadoProcesamientoConfiguracionTramaState> verificadorGrupo = listaGrupoConfiguracionTramaValidarMap
									.get(keyGrupo);
							verificadorGrupo.put(keyConfiguracionTrama,
									EstadoProcesamientoConfiguracionTramaState.PROCESADO_CON_ERROR);

							// Inicio Log para enviar por correo
							BigMemoryManager<String, TasmeErrorMpe> valueTasmeError = listaLogConfiguracionTramaGrupo
									.get(keyGrupo);
							BigMemoryManager<String, LogConfiguracionTrama> valueLogAyuda = listaLogConfiguracionTramaAyudaGrupo
									.get(keyGrupo);
							if (valueTasmeError == null) {
								valueTasmeError = new BigMemoryManager<String, TasmeErrorMpe>(rutaRelativaTempByConf,
										"data_log_error");
							}
							if (valueLogAyuda == null) {
								valueLogAyuda = new BigMemoryManager<String, LogConfiguracionTrama>(
										rutaRelativaTempByConf, "data_log_ayuda");
							}
							listaLogConfiguracionTramaGrupo.put(keyGrupo, valueTasmeError);
							listaLogConfiguracionTramaAyudaGrupo.put(keyGrupo, valueLogAyuda);
							// Inicio Log para enviar por correo

							TasmctrLoteMpe tasmctrlMpe = tasmctrLoteMpeMap.get(keyGrupo);
							tasmctrlMpe.setFechaHoraFinCargaTrama(FechaUtil.obtenerFechaActual());
							tasmctrLoteMpeMap.put(keyGrupo, tasmctrlMpe);
							if (verificarGrupoCompleto(verificadorGrupo)) {
								if (!StringUtil.isNullOrEmpty(tasmctrlMpe.getTasmctrLoteMpePK())) {
									if (!StringUtil.isNullOrEmpty(tasmctrlMpe.getTasmctrLoteMpePK().getFechaLote())
											&& !StringUtil
													.isNullOrEmpty(tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote())) {
										Long cantidadRegistros = 0L;
										tasmctrlMpe.setNroRegistroLeidos(new BigDecimal(cantidadRegistros));
										// TODO:VER_SIMULACION
										if (!ResourceUtil.esSimulacion(
												parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
											RespuestaLecturaArchivoVO respuestaTasmeL = registrarTasmctrLoteMpe(
													parametroMap, tasmctrlMpe, obj.getIdConfiguradorTrama(),
													sessionHibernate);
											if (respuestaTasmeL.isEsError()) {
												if (!CollectionUtil.isEmpty(respuestaTasmeL
														.getListaLogConfiguracionTramaAyuda().getListaKey())) {
													valueLogAyuda.addAll(
															respuestaTasmeL.getListaLogConfiguracionTramaAyuda());
												}
											}
											RespuestaLecturaArchivoVO respuestaTasmeT = registrarTasmctrTramaMpe(
													parametroMap, tasmctrlMpe, tasmctrTramaGrupoMpeMap.get(keyGrupo),
													sessionHibernate);
											if (respuestaTasmeT.isEsError()) {
												if (!CollectionUtil.isEmpty(respuestaTasmeT
														.getListaLogConfiguracionTramaAyuda().getListaKey())) {
													valueLogAyuda.addAll(
															respuestaTasmeT.getListaLogConfiguracionTramaAyuda());
												}
											}
										}
									}
								}
							}
							// Inicio Log para enviar por correo
							valueTasmeError = listaLogConfiguracionTramaGrupo.get(keyGrupo);
							valueLogAyuda = listaLogConfiguracionTramaAyudaGrupo.get(keyGrupo);
							valueTasmeError.addAll(resultadoValidacionEstructura.getListaLogConfiguracionTrama());
							// valueLogAyuda.addAll(resultadoValidacionEstructura.getListaLogConfiguracionTramaAyuda());
							for (String logConfiguracionTramaDTOKey : resultadoValidacionEstructura
									.getListaLogConfiguracionTramaAyuda().getListaKey()) {
								LogConfiguracionTrama logConfiguracionTrama = resultadoValidacionEstructura
										.getListaLogConfiguracionTramaAyuda().get(logConfiguracionTramaDTOKey);
								if (!valueLogAyuda.getListaKeyHelp().contains(logConfiguracionTrama.toString())) {
									valueLogAyuda.add(logConfiguracionTrama);
									valueLogAyuda.getListaKeyHelp().add(logConfiguracionTrama.toString());
								}
							}
							listaLogConfiguracionTramaGrupo.put(keyGrupo, valueTasmeError);
							listaLogConfiguracionTramaAyudaGrupo.put(keyGrupo, valueLogAyuda);

							if (verificarGrupoCompleto(verificadorGrupo)) {
								registrarLogConfiguracionDataGrupo(listaGrupoConfiguracionTramaMap.get(keyGrupo),
										tasmctrlMpe, parametroMap, valueTasmeError, sessionHibernate);
								// DATA TASME ERROR
								agregarTasmeErrorSimulacion(resultado, valueTasmeError, parametroMap);
								Long cantidadRegistrosError = obtenerCantidadErrorRegistroGrupo(valueTasmeError);
								if (tasmctrlMpe != null) {
									tasmctrlMpe.setNroRegistroErrorDeNivel2(new BigDecimal(cantidadRegistrosError));
									tasmctrLoteMpeMap.put(keyGrupo, tasmctrlMpe);
								}
								registrarLogConfiguracionDataAyudaGrupo(parametroMap, valueLogAyuda, sessionHibernate);
								// LOG DE AYUDA
								agregarLogAyudaSimulacion(resultado, valueLogAyuda, parametroMap,
										idNomenclaturaArchivoByConfigTrama);
								// liberando memoria
								listaLogConfiguracionTramaGrupo.remove(keyGrupo);
								listaLogConfiguracionTramaAyudaGrupo.remove(keyGrupo);
								valueTasmeError = null;
								valueLogAyuda = null;
							}
							// Inicio Log para enviar por correo
							// enviar correo de fallo
							// enviarCorreo(configuracionTrama,
							// listaLogConfiguracionTramaGrupo.get(keyGrupo));
							resultadoValidacionEstructura.setListaLogConfiguracionTramaAyuda(
									new BigMemoryManager<String, LogConfiguracionTrama>());
						}
						listaRespuestaLecturaArchivoTemporalProcesarMap = null;// liberando memoria
					} catch (Exception e) {
						log.error("Error ", e);
						resulTemp.setEsError(true);
						resulTemp.setCodigoError("LEC_E004");
						resulTemp.setMensajeError("Error : Excepcion no controlada idConfiguradorTrama: "
								+ obj.getIdConfiguradorTrama() + " " + e.getMessage());
					}
				}
				// Fin parseo y procesamiento de los archivos
				// limpiando memoria
				resultadoProcesoConfiguracionTramaVOMap = null;
				tasmctrLoteMpeMap = null;
				tasmctrTramaGrupoMpeMap = null;
				// leer en temporal
				listaRespuestaLecturaArchivoTemporalMap = null;
				nombreNomenclaturaArchivoTramaMap = null;
				listaResultadoValidacionEstructuraMap = null;
				nomenclaturaArchivoProcesadoMap = null;
				listaArchivoNomenclaturaProcesarTempMap = null;
				listaGrupoConfiguracionTramaValidarMap = null;
				listaLogConfiguracionTramaGrupo = null;
				listaLogConfiguracionTramaAyudaGrupo = null;

				listaGrupoConfiguracionTramaMap = null;
			} // procesando by numero de referencia
				// } //Fin procesando por juego
				// limpiando memoria
			grupoArchivoProcesarMap = null;
			listaKeyNumeroSolicitudOrden = null;
			listaKeyNumeroSolicitudOrdenExcluirMap = null;
		} // key grupo orden
			// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
		EjecutarActividadParaleloInstance.getInstance().putActividad(parametroMap.get("idControlProceso") + "",
				"TRAMAFTP", new ConfiguracionTramaIdProcesarVO());
		// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
		// limpiando memoria
		objConf = null;
		listaConfiguracionTramaTemp = null;
		idNomenclaturaArchivoByConfigTrama = null;
		parametroMap = null;
		return resultado;
	}

	private List<ResultadoProcesoConfiguracionTramaVO> agregarLogAyudaSimulacion(
			List<ResultadoProcesoConfiguracionTramaVO> resultado,
			BigMemoryManager<String, LogConfiguracionTrama> listaLogConfiguracionTramaAyuda,
			Map<String, Object> parametroMap, Map<Long, Long> idNomenclaturaArchivoByConfigTrama) {
		// LOG D AYUDA
		if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
			if (!CollectionUtil.isEmpty(listaLogConfiguracionTramaAyuda.getListaKey())) {
				ResultadoProcesoConfiguracionTramaVO resulTemp = new ResultadoProcesoConfiguracionTramaVO();
				resulTemp.setEsError(true);
				resulTemp.setEsTasmeError(true);
				resulTemp.setListaLogConfiguracionTramaAyuda(listaLogConfiguracionTramaAyuda);
				resultado.add(resulTemp);
			}
			/*
			 * for (String objLogKey : listaLogConfiguracionTramaAyuda.getListaKey()) {
			 * LogConfiguracionTramaDTO objLog =
			 * listaLogConfiguracionTramaAyuda.get(objLogKey);
			 * resultado.add(generarObjetoError(objLog.getNombreCampo(),objLog.getFila(),
			 * objLog.getConponenteValidado(), objLog.getDescripcionError(), (Long)
			 * parametroMap.get("idJuegoTrama"),objLog.getConfiguracionTrama(),
			 * idNomenclaturaArchivoByConfigTrama.get(objLog.getConfiguracionTrama()))); }
			 */
		}
		return resultado;
	}

	private List<ResultadoProcesoConfiguracionTramaVO> agregarTasmeErrorSimulacion(
			List<ResultadoProcesoConfiguracionTramaVO> resultado, BigMemoryManager<String, TasmeErrorMpe> value,
			Map<String, Object> parametroMap) {
		// LOG DE TASME ERROR
		if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
			if (!CollectionUtil.isEmpty(value.getListaKey())) {
				ResultadoProcesoConfiguracionTramaVO resulTemp = new ResultadoProcesoConfiguracionTramaVO();
				resulTemp.setEsError(true);
				resulTemp.setEsTasmeError(true);
				resulTemp.setListaTasmeError(value);
				resultado.add(resulTemp);
			}
		}
		return resultado;
	}

	private RespuestaLecturaArchivoVO registrarTrama(GrupoConfiguracionTramaVO configuracionTramaCnfVO,
			TasmctrLoteMpe tasmctrlMpe, Map<String, TasmctrTramaMpe> tasmctrTramaMpeMap,
			Map<Long, Map<String, String>> listaConfiguracionTramaErrorMap, EntityManager sessionHibernate,
			Map<String, Object> parametroMap, Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap) {
		log.error("inicio.registrarTrama " + parametroMap.toString());
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		RespuestaLecturaArchivoVO respuestaTasmeL = registrarTasmctrLoteMpe(parametroMap, tasmctrlMpe, 0L,
				sessionHibernate);
		if (respuestaTasmeL.isEsError()) {
			if (!CollectionUtil.isEmpty(respuestaTasmeL.getListaLogConfiguracionTramaAyuda().getListaKey())) {
				resultado.getListaLogConfiguracionTramaAyuda()
						.addAll(respuestaTasmeL.getListaLogConfiguracionTramaAyuda());
				resultado.setEsError(true);
			}
		}
		RespuestaLecturaArchivoVO respuestaTasmeT = registrarTasmctrTramaMpe(parametroMap, tasmctrlMpe,
				tasmctrTramaMpeMap, sessionHibernate);
		if (respuestaTasmeT.isEsError()) {
			if (!CollectionUtil.isEmpty(respuestaTasmeT.getListaLogConfiguracionTramaAyuda().getListaKey())) {
				resultado.getListaLogConfiguracionTramaAyuda()
						.addAll(respuestaTasmeT.getListaLogConfiguracionTramaAyuda());
				resultado.setEsError(true);
			}
		}
		RespuestaLecturaArchivoVO respuestaProcesoRegistroVO = registrarTramaData(parametroMap, configuracionTramaCnfVO,
				configuracionTramaDetalleMap, listaConfiguracionTramaErrorMap,
				tasmctrlMpe.getTasmctrLoteMpePK().getFechaLote(), tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote(),
				sessionHibernate);
		if (respuestaProcesoRegistroVO.isEsError()) {
			resultado.setListaLogConfiguracionTrama(respuestaProcesoRegistroVO.getListaLogConfiguracionTrama());
			if (resultado.getListaLogConfiguracionTrama().getListaKey().size() > 0) {
				resultado.setEsError(true);
			}
			if (respuestaProcesoRegistroVO.getListaLogConfiguracionTramaAyuda().getListaKey().size() > 0) {
				resultado.setEsError(true);
			}
			resultado.setListaLogConfiguracionTramaAyuda(
					respuestaProcesoRegistroVO.getListaLogConfiguracionTramaAyuda());
		}

		// Inicio
		try {
			// TODO:VER_SIMULACION
			if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				configuracionTramaDao.registrarFlagTasmeEDGP(tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote(),
						tasmctrlMpe.getTasmctrLoteMpePK().getFechaLote(), EstadoDetalleProcesoType.SIN_ERROR.getKey());
			}
		} catch (Exception e) {
			log.error("Error ", e);
		}
		log.error("fin.registrarTrama " + parametroMap.toString());
		// Fin
		return resultado;
	}

	@Override
	public RespuestaLecturaArchivoVO registrarTramaData(Map<String, Object> parametroMap,
			GrupoConfiguracionTramaVO grupoConfiguracionTramaVO,
			Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap,
			Map<Long, Map<String, String>> listaConfiguracionTramaErrorMap, Date fechaLote, String numeroLote,
			EntityManager sessionHibernate) {
		if (parametroMap.containsKey("activar.session.hibernate")) {
			// this.obtenerSessionHibernate();
		}
		if (ConfiguracionCacheUtil.getInstance()
				.isGenerarLogTramaJuego(grupoConfiguracionTramaVO.getNombreConfiguracion())) {
			log.error("inicio.registrarTramaData " + parametroMap.toString());
		}
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		String rutaRelativaTemp = parametroMap.get("rutaRelativaTemp") + "";
		resultado.setListaLogConfiguracionTramaAyuda(new BigMemoryManager<String, LogConfiguracionTrama>(
				rutaRelativaTemp, "registrarTramaData_data_log_ayuda"));
		resultado.setListaLogConfiguracionTrama(
				new BigMemoryManager<String, TasmeErrorMpe>(rutaRelativaTemp, "registrarTramaData_data_log_error"));
		for (GrupoConfiguracionTramaVO configuracionTramaCnf : grupoConfiguracionTramaVO.getListaConfiguracionTrama()) {
			Long idConfiguracionTrama = Long.valueOf(configuracionTramaCnf.getNombreConfiguracion());
			log.error("proceso.registrarTramaData " + idConfiguracionTrama + "  data --> "
					+ configuracionTramaCnf.getListaConfiguracionTramaData().getListaKey().size());
			int dataPosicion = 1;
			for (String configuracionTramaDataKey : configuracionTramaCnf.getListaConfiguracionTramaData()
					.getListaKey()) {
				ConfiguracionTramaDataVO configuracionTramaData = configuracionTramaCnf.getListaConfiguracionTramaData()
						.get(configuracionTramaDataKey);
				Map<String, ValueDataVO> dataResulMapTemp = new HashMap<String, ValueDataVO>();
				Map<String, Object> parametros = new LinkedHashMap<String, Object>();
				StringBuilder sql = new StringBuilder();
				String tableName = configuracionTramaData.getSchemaName() + "." + configuracionTramaData.getTableName();
				try {
					String shemaName = "";
					if (!StringUtil.isNullOrEmpty(configuracionTramaData.getSchemaName())) {
						shemaName = configuracionTramaData.getSchemaName() + ".";
					}
					StringBuilder sqlHeader = new StringBuilder();
					StringBuilder sqlValue = new StringBuilder();
					sql.append("insert into " + shemaName + "" + configuracionTramaData.getTableName() + " (");
					sqlValue.append(" values(");
					int cantidad = obtenerCantidad(configuracionTramaData.getListaCampoValue());
					int contador = 0;
					int indexPs = 0;
					for (ConfiguracionTramaDataVO objValue : configuracionTramaData.getListaCampoValue()) {
						dataResulMapTemp.put(objValue.getAtributeName(), objValue.getAtributeValue());
						if (StringUtil.isNullOrEmpty(objValue.getAtributeValue())) {
							continue;
						}
						String separador = ",";
						contador++;
						if (contador >= cantidad) {
							separador = "";
						}
						indexPs++;
						TipoCampoType tipoCampoType = TipoCampoType.get(objValue.getAtributeType());
						if (tipoCampoType != null) {
							switch (tipoCampoType) {
							case FECHA:
								if (!StringUtil.isNullOrEmpty(objValue.getAtributeValue())) {
									sqlHeader.append(objValue.getAtributeName() + separador);
									String formato = "dd/MM/yyyy";
									String keyFormat = (indexPs + 1) + "";// objValue.getAtributeName() + "Format";
									sqlValue.append("to_date(:" + indexPs + ",:" + keyFormat + ")" + separador);
									parametros.put(indexPs + "", FechaUtil.obtenerFechaFormatoPersonalizado(
											(Date) objValue.getAtributeValue().getData(), formato));
									parametros.put(keyFormat, formato);
									indexPs++;
								}
								break;
							case NUMERICO:
								if (!StringUtil.isNullOrEmpty(objValue.getAtributeValue())) {
									sqlHeader.append(objValue.getAtributeName() + separador);
									sqlValue.append(":" + indexPs + "" + separador);
									parametros.put(indexPs + "", objValue.getAtributeValue().getData());
								}
								break;

							case TEXTO:
								if (!StringUtil.isNullOrEmpty(objValue.getAtributeValue())) {
									sqlHeader.append(objValue.getAtributeName() + separador);
									sqlValue.append(":" + indexPs + "" + separador);
									parametros.put(indexPs + "", objValue.getAtributeValue().getData());
								}
								break;
							default:
								if (!StringUtil.isNullOrEmpty(objValue.getAtributeValue())) {
									sqlHeader.append(objValue.getAtributeName() + separador);
									sqlValue.append(":" + indexPs + "" + separador);
									parametros.put(indexPs + "", objValue.getAtributeValue().getData());
								}
								break;
							}
						} else {
							if (!StringUtil.isNullOrEmpty(objValue.getAtributeValue())) {
								sqlHeader.append(objValue.getAtributeName() + separador);
								sqlValue.append(":" + indexPs + "" + separador);
								parametros.put(indexPs + "", objValue.getAtributeValue().getData());
							}
						}

					}
					sqlHeader.append(")");
					sqlValue.append(")");

					sql.append(sqlHeader.toString());
					sql.append(sqlValue.toString());
					// log.info(sql + "");
					if (ConfiguracionCacheUtil.getInstance()
							.isGenerarLogTramaJuego(grupoConfiguracionTramaVO.getNombreConfiguracion())) {
						log.error("proceso.registrarTramaData.parametros " + parametros.toString());
						log.error("proceso.registrarTramaData.sql " + sql.toString());
					}
					// TODO:VER_SIMULACION
					// Inicio BuildSoft Configuracion de caja raiz
					if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))
							&& contador > 0) {
						configuracionTramaDao.registrarTramaData(sql, parametros);
					}
					// Fin BuildSoft Configuracion de caja raiz
					// Limpiando memoria
					sqlHeader = null;
					sqlValue = null;
				} catch (Exception e) {
					log.error("registrarTramaData.error -->" + e.getMessage());
					boolean isPrintError = false;
					Map<String, Object> varErrorMap = new HashMap<String, Object>();
					// Inicio BuildSoft Mejoras Interconexion
					String mensajeError = e.getMessage() + " --> No se pudo registrar en la tabla " + tableName
							+ ",registro nro " + dataPosicion + "";
					// Fin BuildSoft Mejoras Interconexion
					if (!StringUtil.isNullOrEmpty(fechaLote) && !StringUtil.isNullOrEmpty(numeroLote)
							&& listaConfiguracionTramaErrorMap != null) {
						TasmeErrorMpe tasmeErrorMpe = generarLogError("", mensajeError, dataResulMapTemp,
								configuracionTramaDetalleMap, listaConfiguracionTramaErrorMap.get(idConfiguracionTrama),
								fechaLote, numeroLote, idConfiguracionTrama, "ERROR", varErrorMap);
						if (tasmeErrorMpe != null) {
							resultado.getListaLogConfiguracionTrama().add(tasmeErrorMpe);
						} else {
							isPrintError = true;
						}
					} else {
						isPrintError = true;
					}
					if (isPrintError) {
						// log.error("registrarTramaData.error --> " + mensajeError);

						if (ResourceUtil
								.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
							// mensajeError = "El campo excede la longitud permitida";
							LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "",
									"Exception:No Registro", obtenerDescripcionError(mensajeError),
									"Exception:No Registro", idConfiguracionTrama,
									ComponenteValidadoLogTramaType.CONF_TRAMA, TipoErrorLogTramaType.GENERAL);
							resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
						} else {
							LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "",
									"Exception:No Registro",
									obtenerDescripcionError("Fecha Lote = " + fechaLote + " numero Lote = " + numeroLote
											+ " " + mensajeError),
									"Exception:No Registro", idConfiguracionTrama,
									ComponenteValidadoLogTramaType.CONF_TRAMA, TipoErrorLogTramaType.GENERAL);
							resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);

						}

					}
					resultado.setEsError(true);
					resultado.setEstadoProcesamientoConfiguracionTrama(
							EstadoProcesamientoConfiguracionTramaState.PROCESADO_CON_ERROR);
					// imprimir log
				}
				dataPosicion++;
			}
			configuracionTramaCnf.getListaConfiguracionTramaData().reiniciar();
		}
		// limpiando memoria
		if (ConfiguracionCacheUtil.getInstance()
				.isGenerarLogTramaJuego(grupoConfiguracionTramaVO.getNombreConfiguracion())) {
			log.error("fin.registrarTramaData " + parametroMap.toString());
		}
		grupoConfiguracionTramaVO = null;
		configuracionTramaDetalleMap = null;
		listaConfiguracionTramaErrorMap = null;
		fechaLote = null;
		numeroLote = null;
		return resultado;
	}

	// FILADATA
	private RespuestaLecturaArchivoVO leerConfiguracionTramaDataTemporal(ConfiguracionTrama configuracionTrama,
			Map<String, CampoTablaVO> campoDisponibleMap, String nomenclatura, Map<String, Object> parametroMap) {
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		String rutaRelativaTemp = parametroMap.get("rutaRelativaTemp") + "";
		if (ConfiguracionCacheUtil.getInstance()
				.isGenerarLogTramaJuego(configuracionTrama.getJuegoTrama().getIdJuegoTrama())) {
			log.error("inicio.leerConfiguracionTramaDataTemporal datos nomenclatura : " + nomenclatura
					+ " configuracionTrama (" + configuracionTrama.getIdConfiguradorTrama() + " , "
					+ configuracionTrama.getNombreTabla() + ") ");
		}
		BigMemoryManager<String, ConfiguracionTramaDataVO> resultadoListaConf = new BigMemoryManager<String, ConfiguracionTramaDataVO>();
		try {
			boolean isCabecera = TipoProcesoType.CABECERA.getKey().equals(configuracionTrama.getTipoProceso());
			Integer cantidadDataProcesar = isCabecera ? 1 : null;
			TipoArchivoProcesarType tipoArchivoProcesarType = TipoArchivoProcesarType
					.get(configuracionTrama.getTramaNomenclaturaArchivo().getTipoArchivo());
			Map<String, Object> campoMappingPosicionMap = obtenerCampoMappingPosicionMap(configuracionTrama);
			Map<String, String> campoMappingTypeMap = obtenerCampoMappingTypeMap(configuracionTrama);
			List<Map<String, ValueDataVO>> listaDataResulMap = new ArrayList<Map<String, ValueDataVO>>();
			// seteando valor por defaulft
			Map<String, ValueDataVO> valuePorDefectoMap = new HashMap<String, ValueDataVO>();
			Map<String, String> campoMappingFormatoMap = new HashMap<String, String>();
			Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap = new HashMap<>();
			Map<String, Character> configuracionTramaDetalleCampoMap = new HashMap<>();
			for (ConfiguracionTramaDetalle objDet : configuracionTrama
					.getConfiguracionTramaConfiguracionTramaDetalleList()) {
				String nombeCampoTabla = obtenerCampoPersistenteAsociado(objDet);
				if (RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
					if (!StringUtil.isNullOrEmpty(objDet.getValorDefectoCampo())) {
						valuePorDefectoMap.put(nombeCampoTabla,
								TransferDataUtil.obtenerValueParse(objDet.getValorDefectoCampo(),
										campoMappingTypeMap.get(nombeCampoTabla), objDet.getFormatoCampo(), 0,
										parametroMap));
					}
				}
				campoMappingFormatoMap.put(nombeCampoTabla, objDet.getFormatoCampo());
				configuracionTramaDetalleMap.put(nombeCampoTabla, objDet);
				configuracionTramaDetalleCampoMap.put(nombeCampoTabla, objDet.getFlagCampoAgrupador());
			}
			resultado.setCampoMappingFormatoMap(campoMappingFormatoMap);

			ConfiguracionFtpTrama configuracionFtpTramaDTO = obtenerConfiguracionFtpTrama(configuracionTrama,
					TipoDestinatarioFTPType.ORIGEN);
			String ruta = configuracionFtpTramaDTO.getRuta();
			ruta = FTPClienteUtil.obtenerVariableFechaFormateada(ruta);
			// limpiando memeoria
			configuracionFtpTramaDTO = null;
			String usuario = ConstanteConfiguracionTramaUtil.CARPETA_MANUAL
					+ configuracionTrama.getJuegoTrama().getIdJuegoTrama();
			String rutaArchivo = ConstanteConfigUtil.generarRuta(ArchivoUtilidades.RUTA_RECURSOS,
					ArchivoUtilidades.RUTA_REPORTE_GENERADO, usuario) + ruta;
			// rutaArchivo = rutaArchivo.replace("/", "\\");
			switch (tipoArchivoProcesarType) {
			case EXCEL_XLS:
				HSSFWorkbook hSSFWorkbook = ExcelUtil.leerExcel(new File(rutaArchivo + nomenclatura));
				listaDataResulMap = ExcelUtil.transferObjetoEntityExcelMapDTO(hSSFWorkbook,
						configuracionTrama.getNumeroHoja().intValue(), configuracionTrama.getFilaData().intValue(),
						campoMappingPosicionMap, campoMappingTypeMap, campoMappingFormatoMap, cantidadDataProcesar,
						parametroMap, configuracionTramaDetalleCampoMap);
				hSSFWorkbook = null;
				break;

			case EXCEL_XLSX:
				XSSFWorkbook xSSFWorkbook = ExcelUtil.leerExcelXlsx(new File(rutaArchivo + nomenclatura));
				listaDataResulMap = ExcelUtil.transferObjetoEntityExcelXlsxMapDTO(xSSFWorkbook,
						configuracionTrama.getNumeroHoja().intValue(), configuracionTrama.getFilaData().intValue(),
						campoMappingPosicionMap, campoMappingTypeMap, campoMappingFormatoMap, cantidadDataProcesar,
						parametroMap, configuracionTramaDetalleCampoMap);
				xSSFWorkbook = null;
				break;

			case CSV:
				BufferedReader br = CSVUtil.leerCVS(rutaArchivo + nomenclatura);
				listaDataResulMap = CSVUtil.transferObjetoEntityCSVMapDTO(br,
						configuracionTrama.getFilaData().intValue(), campoMappingPosicionMap, campoMappingTypeMap,
						campoMappingFormatoMap, configuracionTrama.getSeparador(), cantidadDataProcesar, parametroMap,
						configuracionTramaDetalleCampoMap);
				br = null;
				break;

			case TXT:
				boolean isByCoordenada = verificarProcesamientoByCoordenada(configuracionTrama);
				BufferedReader brText = TXTUtil.leerTXT(rutaArchivo + nomenclatura,
						Boolean.valueOf(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_IMPRIMIR).toString()));
				if (!isByCoordenada) {
					if (RespuestaNaturalType.SI.getKey().equals(configuracionTrama.getTieneSeparador())) {
						listaDataResulMap = TXTUtil.transferObjetoEntityTXTMapDTO(brText,
								configuracionTrama.getFilaData().intValue(), campoMappingPosicionMap,
								campoMappingTypeMap, campoMappingFormatoMap, configuracionTrama.getSeparador(),
								cantidadDataProcesar, parametroMap, configuracionTramaDetalleCampoMap);
					} else {
						listaDataResulMap = TXTUtil.transferObjetoEntityTXTMapDTO(brText,
								configuracionTrama.getFilaData().intValue(), campoMappingPosicionMap,
								campoMappingTypeMap, campoMappingFormatoMap, cantidadDataProcesar, parametroMap,
								configuracionTramaDetalleCampoMap);
					}
				} else {
					if (isCabecera) {
						cantidadDataProcesar = obtenerCantidadProcesamientoByCoordenada(configuracionTrama);
					}
					listaDataResulMap = TXTUtil.transferObjetoEntityCoordenadaTXTMapDTO(brText,
							configuracionTrama.getFilaData().intValue(), campoMappingPosicionMap, campoMappingTypeMap,
							campoMappingFormatoMap, cantidadDataProcesar, isCabecera,
							configuracionTrama.getDelimitadorData(), parametroMap, configuracionTramaDetalleCampoMap);
				}
				brText = null;
				break;

			default:
				break;
			}
			// parseando la data y validar :tramas no ledias con valor por defecto
			log.error("proceso.leerConfiguracionTramaDataTemporal.data.map.size + "
					+ parametroMap.get("idControlProceso") + " : " + listaDataResulMap.size());
			for (Map<String, ValueDataVO> map : listaDataResulMap) {
				for (Map.Entry<String, ValueDataVO> mapValue : valuePorDefectoMap.entrySet()) {
					if (map.containsKey(mapValue.getKey())) {
						map.put(mapValue.getKey(), valuePorDefectoMap.get(mapValue.getKey()));
					}
				}
				if (ConfiguracionCacheUtil.getInstance()
						.isGenerarLogTramaJuego(configuracionTrama.getJuegoTrama().getIdJuegoTrama())) {
					log.error("proceso.leerConfiguracionTramaDataTemporal.data.map : " + map.toString());
				}
			}
			//completardarta equifax
			resultado.setListaDataResulMap(listaDataResulMap);
			// limpiando memoria
			campoMappingPosicionMap = null;
			campoMappingTypeMap = null;
			valuePorDefectoMap = null;
		} catch (Exception e) {
			log.error("LEC_E005 leerConfiguracionTramaDataTemporal --> Error : INESPERADO VERIFICAR EL LOG ");
			log.error("Error ", e);
			resultado.setEsError(true);
		}
		if (resultado.isEsError()) {
			resultado.setEstadoProcesamientoConfiguracionTrama(
					EstadoProcesamientoConfiguracionTramaState.PROCESADO_CON_ERROR);
		} else {
			resultado.setEstadoProcesamientoConfiguracionTrama(
					EstadoProcesamientoConfiguracionTramaState.PROCESADO_CON_EXITO);
		}
		resultado.setIdConfiguracionTrama(configuracionTrama.getIdConfiguradorTrama());
		resultado.setListaConfiguracionTramaDataVO(resultadoListaConf);
		TransferDataUtil.defaultLocaleProcess();
		if (ConfiguracionCacheUtil.getInstance()
				.isGenerarLogTramaJuego(configuracionTrama.getJuegoTrama().getIdJuegoTrama())) {
			log.error("fin.leerConfiguracionTramaDataTemporal datos nomenclatura : " + nomenclatura
					+ " configuracionTrama (" + configuracionTrama.getIdConfiguradorTrama() + " , "
					+ configuracionTrama.getNombreTabla() + ") ");
		}
		return resultado;
	}

	private RespuestaLecturaArchivoVO parsearConfiguracionTramaData(
			ConfiguracionTramaIdProcesarVO configuracionTramaEquivalenciaMapVO,
			Map<String, RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoTemporalMap,
			ConfiguracionTrama configuracionTrama, Map<String, CampoTablaVO> campoDisponibleMap, String nomenclatura,
			Map<String, String> configuracionTramaErrorMap,
			Map<Long, Map<String, String>> listaConfiguracionTramaCampoNoPersistenteMap, Date fechaLote,
			String numeroLote, Map<String, Object> parametroMap) throws Exception {
		if (ConfiguracionCacheUtil.getInstance()
				.isGenerarLogTramaJuego(configuracionTrama.getJuegoTrama().getIdJuegoTrama())) {
			log.error("inicio.parsearConfiguracionTramaData datos nomenclatura : " + nomenclatura
					+ " configuracionTrama (" + configuracionTrama.getIdConfiguradorTrama() + " , "
					+ configuracionTrama.getNombreTabla() + ") ");
		}
		String rutaRelativaTemp = parametroMap.get("rutaRelativaTemp") + "";
		Long idConfiguracionTrama = configuracionTrama.getIdConfiguradorTrama();
		parametroMap.put(ConstanteConfiguracionTramaUtil.NUMERO_HOJA + "_" + idConfiguracionTrama,
				configuracionTrama.getNumeroHoja()); // setear numero de hoja
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		// resultado.setListaDataResulBigMap(new BigMemoryManager<String,
		// Map<String,ValueDataVO>>(rutaRelativaTemp,"procesar_trama_data_map"));
		resultado.setListaLogConfiguracionTrama(
				new BigMemoryManager<String, TasmeErrorMpe>(rutaRelativaTemp, "procesar_trama_data_log_error"));
		resultado.setListaLogConfiguracionTramaAyuda(
				new BigMemoryManager<String, LogConfiguracionTrama>(rutaRelativaTemp, "procesar_trama_data_log_ayuda"));
		BigMemoryManager<String, ConfiguracionTramaDataVO> resultadoListaConf = new BigMemoryManager<String, ConfiguracionTramaDataVO>();
		String nombreTabla = configuracionTrama.getNombreTabla().replace(".", ";");
		String[] schemaTableName = nombreTabla.split(";");
		Map<String, String> campoMappingTypeMap = obtenerCampoMappingTypeMap(configuracionTrama);
		List<Map<String, ValueDataVO>> listaDataResulMap = listaRespuestaLecturaArchivoTemporalMap
				.get(configuracionTrama.getIdConfiguradorTrama() + "").getListaDataResulMap();
		log.error("proceso.inicio.parsearConfiguracionTramaData.data.map.size + " + parametroMap.get("idControlProceso")
				+ " : " + listaDataResulMap.size());
		resultado.setCampoMappingFormatoMap(listaRespuestaLecturaArchivoTemporalMap
				.get(configuracionTrama.getIdConfiguradorTrama() + "").getCampoMappingFormatoMap());
		Map<String, String> campoNoPersistente = listaConfiguracionTramaCampoNoPersistenteMap
				.get(configuracionTrama.getIdConfiguradorTrama());
		if (campoNoPersistente == null) {
			campoNoPersistente = new HashMap<String, String>();
		}
		Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap = new HashMap<String, ConfiguracionTramaDetalle>();
		RespuestaLecturaAgrupadoVO respuestaLecturaAgrupadoVO = null;
		try {
			// seteando valor por defaulft
			Map<String, ValueDataVO> valuePorDefectoMap = new HashMap<String, ValueDataVO>();
			for (ConfiguracionTramaDetalle objDet : configuracionTrama
					.getConfiguracionTramaConfiguracionTramaDetalleList()) {
				String nombeCampoTabla = obtenerCampoPersistenteAsociado(objDet);
				if (!StringUtil.isNullOrEmpty(objDet.getValorDefectoCampo())) {
					valuePorDefectoMap.put(nombeCampoTabla,
							TransferDataUtil.obtenerValueParse(objDet.getValorDefectoCampo(),
									campoMappingTypeMap.get(nombeCampoTabla), objDet.getFormatoCampo(), 0,
									parametroMap));
				}
				configuracionTramaDetalleMap.put(nombeCampoTabla, objDet);
			}
			// agrupando
			respuestaLecturaAgrupadoVO = obtenerAgrupacionMap(configuracionTramaEquivalenciaMapVO, configuracionTrama,
					listaDataResulMap, configuracionTramaDetalleMap, campoMappingTypeMap, configuracionTramaErrorMap,
					listaRespuestaLecturaArchivoTemporalMap, nombreTabla, nomenclatura, fechaLote, numeroLote,
					parametroMap);
			if (respuestaLecturaAgrupadoVO.isErrorNumeroLote()) { /// ERROR NUMERO DE LOTE NO GENERO
				resultado.setExisteErrorNumLote(respuestaLecturaAgrupadoVO.isErrorNumeroLote());
			}
			resultado.getListaLogConfiguracionTramaAyuda()
					.addAll(respuestaLecturaAgrupadoVO.getListaLogConfiguracionTramaAyuda());
			List<Map<String, ValueDataVO>> listaDataResulGrupoMap = respuestaLecturaAgrupadoVO
					.getListaAgrupadoDataResulMap();
			listaDataResulMap = null;
			listaDataResulMap = listaDataResulGrupoMap;
			// parseando la data y validar
			Map<Integer, Boolean> errorTuplaMap = new HashMap<Integer, Boolean>();
			int errorTuplaIndice = 0;
			for (Map<String, ValueDataVO> map : listaDataResulGrupoMap) {
				boolean existeErrorTupla = false;
				for (Map.Entry<String, ValueDataVO> mapValue : map.entrySet()) {
					errorTuplaIndice++;
					ConfiguracionTramaDetalle configuracionTramaDetalle = configuracionTramaDetalleMap
							.get(mapValue.getKey());
					TipoCampoType tipoCampoType = obtenerTipoCampo(configuracionTramaDetalle);
					// validar
					boolean existeError = false;
					if (StringUtil.isNullOrEmpty(configuracionTramaDetalle.getReglaNegocio())) {
						validarCampoReglaNegocio(resultado, tipoCampoType, map, mapValue, nomenclatura, fechaLote,
								numeroLote, configuracionTramaErrorMap, configuracionTramaDetalleMap,
								idConfiguracionTrama, schemaTableName, campoDisponibleMap, configuracionTramaDetalle,
								parametroMap);
						existeError = resultado.isExisteError();
						map.put(mapValue.getKey(), resultado.getMap().get(mapValue.getKey())); // RESOLUCION DE
																								// INCIDENCIA
					}
					if (StringUtil.isNullOrEmpty(mapValue.getValue())) {
						if (valuePorDefectoMap.containsKey(mapValue.getKey())) {
							map.put(mapValue.getKey(), valuePorDefectoMap.get(mapValue.getKey()));
						}
					}
					if (existeError) {
						existeErrorTupla = true;
					}
				}
				errorTuplaMap.put(errorTuplaIndice, existeErrorTupla);
			}
			// INICIO CAMPOS CALCULADOS.
			listaDataResulGrupoMap = calcularCampoFijo(configuracionTramaEquivalenciaMapVO, configuracionTrama,
					listaDataResulGrupoMap, configuracionTramaDetalleMap, campoMappingTypeMap,
					configuracionTramaErrorMap, listaRespuestaLecturaArchivoTemporalMap, nombreTabla, nomenclatura,
					fechaLote, numeroLote);
			// Inicio Procesar Reglas cuando no cumplio con la regla en el for de arriba no
			// se considera el valor de esta para este for.
			errorTuplaIndice = 0;
			for (Map<String, ValueDataVO> map : listaDataResulGrupoMap) {
				boolean existeErrorTupla = false;
				for (Map.Entry<String, ValueDataVO> mapValue : map.entrySet()) {
					errorTuplaIndice++;
					ConfiguracionTramaDetalle configuracionTramaDetalle = configuracionTramaDetalleMap
							.get(mapValue.getKey());
					TipoCampoType tipoCampoType = obtenerTipoCampo(configuracionTramaDetalle);
					// validar
					boolean existeError = false;
					// INICIO CALCULO DE REGLA
					if (!StringUtil.isNullOrEmpty(configuracionTramaDetalle.getReglaNegocio())) {
						try {
							ValueDataVO memoriaRegla = ejecutarProcesamientoRegla(map, mapValue.getKey(),
									campoMappingTypeMap, tipoCampoType, configuracionTramaDetalle, parametroMap);
							if (memoriaRegla != null && memoriaRegla.getTipoCampoType() != null) { // MODIFICAR EL TIPO
																									// DE CAMPO EN
																									// TIEMPO DE
																									// EJECUCION
								tipoCampoType = memoriaRegla.getTipoCampoType();
								configuracionTramaDetalle.setTipoCampo(memoriaRegla.getTipoCampoType().getKey());
								configuracionTramaDetalleMap.put(mapValue.getKey(), configuracionTramaDetalle);
							}
							map.put(mapValue.getKey(), memoriaRegla);
							validarCampoReglaNegocio(resultado, tipoCampoType, map, mapValue, nomenclatura, fechaLote,
									numeroLote, configuracionTramaErrorMap, configuracionTramaDetalleMap,
									idConfiguracionTrama, schemaTableName, campoDisponibleMap,
									configuracionTramaDetalle, parametroMap);
							existeError = resultado.isExisteError();
							map.put(mapValue.getKey(), resultado.getMap().get(mapValue.getKey())); // RESOLUCION DE
																									// INCIDENCIA
						} catch (Exception e) {
							if (ConfiguracionCacheUtil.getInstance()
									.isGenerarLogTramaJuego(configuracionTrama.getJuegoTrama().getIdJuegoTrama())) {
								log.error(
										"proceso.parsearConfiguracionTramaData.validarCampoReglaNegocio.reglaProcesadoError.error : ",
										e);
							}
							reglaProcesadoError(resultado, map, mapValue, nomenclatura, fechaLote, numeroLote,
									configuracionTramaErrorMap, configuracionTramaDetalleMap, idConfiguracionTrama,
									configuracionTramaDetalle, parametroMap, e.getMessage());
						}

					}
					// FIN CALCULO DE REGLAS
					if (existeError) {
						existeErrorTupla = true;
					}
				}
				if (!errorTuplaMap.get(errorTuplaIndice) && existeErrorTupla) {
					errorTuplaMap.put(errorTuplaIndice, existeErrorTupla);
				}
				if (ConfiguracionCacheUtil.getInstance()
						.isGenerarLogTramaJuego(configuracionTrama.getJuegoTrama().getIdJuegoTrama())) {
					log.error("proceso.parsearConfiguracionTramaData.data.map : " + map.toString());
				}
			}
			// Fin Procesar Reglas
			// PASAR AL VO PARA PERSISTIR
			parametroMap.remove(
					ConstanteConfiguracionTramaUtil.NUMERO_HOJA + "_" + configuracionTrama.getIdConfiguradorTrama()); // remover
																														// numero
																														// de
																														// hoja
																														// dinamico
			resultadoListaConf = generarTupaPersistente(listaDataResulGrupoMap, errorTuplaMap, schemaTableName,
					configuracionTramaDetalleMap, campoNoPersistente, parametroMap);
			// listaDataResulGrupoMap = null;
		} catch (Exception e) {
			log.error("Error ", e);
			LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "", "Exception",
					obtenerDescripcionError(
							"Fecha Lote = " + fechaLote + " numero Lote = " + numeroLote + " " + e.getMessage()),
					nomenclatura, configuracionTrama.getIdConfiguradorTrama(),
					ComponenteValidadoLogTramaType.CONF_TRAMA, TipoErrorLogTramaType.CAMPO);
			resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
			resultado.setEsError(true);
		}
		if (resultado.isEsError()) {
			resultado.setEstadoProcesamientoConfiguracionTrama(
					EstadoProcesamientoConfiguracionTramaState.PROCESADO_CON_ERROR);
		} else {
			resultado.setEstadoProcesamientoConfiguracionTrama(
					EstadoProcesamientoConfiguracionTramaState.PROCESADO_CON_EXITO);
		}
		// persistiendo en big memory
		/*
		 * for (Map<String, ValueDataVO> map : listaDataResulMap) {
		 * resultado.getListaDataResulBigMap().put(map); } listaDataResulMap = null;
		 */
		resultado.setListaDataResulMap(listaDataResulMap);
		resultado.setListaConfiguracionTramaDataVO(resultadoListaConf);
		// limpiando memoria
		if (ConfiguracionCacheUtil.getInstance()
				.isGenerarLogTramaJuego(configuracionTrama.getJuegoTrama().getIdJuegoTrama())) {
			log.error("fin.parsearConfiguracionTramaData datos nomenclatura : " + nomenclatura + " configuracionTrama ("
					+ configuracionTrama.getIdConfiguradorTrama() + " , " + configuracionTrama.getNombreTabla() + ") ");
		}
		configuracionTramaDetalleMap = null;
		campoMappingTypeMap = null;
		campoNoPersistente = null;
		respuestaLecturaAgrupadoVO = null;
		log.error("proceso.fin.parsearConfiguracionTramaData.data.map.size + " + parametroMap.get("idControlProceso")
				+ " : " + listaDataResulMap.size());
		return resultado;
	}

	private ValueDataVO ejecutarProcesamientoRegla(Map<String, ValueDataVO> map, String nombreCampoTabla,
			Map<String, String> campoMappingTypeMap, TipoCampoType tipoCampoType,
			ConfiguracionTramaDetalle configuracionTramaDetalle, Map<String, Object> parametroMap) throws Exception {
		ReglaVO memoria = new ReglaVO(map, new ValueDataVO());
		String fila = "";
		memoria.setEsSimulacion(
				ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION)));
		for (Entry<String, ValueDataVO> elemento : map.entrySet()) { // TODO OBTIENE LA FILA
			if (!StringUtil.isNullOrEmpty(elemento.getValue().getFila())) {
				fila = elemento.getValue().getFila();
				break;
			}
		}
		ProcesarReglaUtil.procesarRegla(memoria, ParametroReglaUtil.ACRONIMO_REGLA_CONF_TRAMA_DETALLE
				+ configuracionTramaDetalle.getIdConfiguradorTramaDetalle());
		if (memoria.getResultado() != null && memoria.getResultado().getTipoCampoType() != null) {
			tipoCampoType = memoria.getResultado().getTipoCampoType();
		}
		parserarTipoCampoByRegla(tipoCampoType, memoria.getResultado(), nombreCampoTabla, campoMappingTypeMap,
				configuracionTramaDetalle, parametroMap);
		// memoria.setFila(fila);
		memoria.getResultado().setFila(fila);
		return memoria.getResultado();
	}

	private RespuestaLecturaArchivoVO reglaProcesadoError(RespuestaLecturaArchivoVO resultado,
			Map<String, ValueDataVO> map, Map.Entry<String, ValueDataVO> mapValue, String nomenclatura, Date fechaLote,
			String numeroLote, Map<String, String> configuracionTramaErrorMap,
			Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap, Long idConfiguracionTrama,
			ConfiguracionTramaDetalle configuracionTramaDetalle, Map<String, Object> parametroMap, String error)
			throws Exception {
		boolean isPrintError = true;
		Map<String, Object> varErrorMap = new HashMap<String, Object>();
		String mensajeError = "Nombre archivo : " + nomenclatura + " Nombre Campo : "
				+ configuracionTramaDetalle.getNombreCampo() + " Campo Tabla : " + mapValue.getKey()
				+ "  Error al procesar la regla " + error;
		if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
			mensajeError = "El campo tiene error de procesamiento de regla ";
		}
		if (!StringUtil.isNullOrEmpty(fechaLote) && !StringUtil.isNullOrEmpty(numeroLote)
				&& configuracionTramaErrorMap != null) {
			TasmeErrorMpe tasmeErrorMpe = generarLogError(mapValue.getValue().getFila(), mensajeError, map,
					configuracionTramaDetalleMap, configuracionTramaErrorMap, fechaLote, numeroLote,
					idConfiguracionTrama, configuracionTramaDetalle.getNombreCampo(), varErrorMap);
			if (tasmeErrorMpe != null) {
				resultado.getListaLogConfiguracionTrama().add(tasmeErrorMpe);
			} else {
				isPrintError = true;
			}
		} else {
			isPrintError = true;
		}
		if (isPrintError) {
			LogConfiguracionTrama logConfiguracionTrama = new LogConfiguracionTrama();
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				logConfiguracionTrama = generarLogConfiguracionTrama(configuracionTramaDetalle.getNombreCampo(),
						mapValue.getValue().getFila(), "Error procesamiento  tipo Campo",
						obtenerDescripcionError(mensajeError), mapValue.getKey(), idConfiguracionTrama,
						ComponenteValidadoLogTramaType.TIPO, TipoErrorLogTramaType.CAMPO);
			} else {
				logConfiguracionTrama = generarLogConfiguracionTrama(configuracionTramaDetalle.getNombreCampo(),
						mapValue.getValue().getFila(), "Error procesamiento  tipo Campo",
						obtenerDescripcionError(
								"Fecha Lote = " + fechaLote + " numero Lote = " + numeroLote + " " + mensajeError),
						mapValue.getKey(), idConfiguracionTrama, ComponenteValidadoLogTramaType.TIPO,
						TipoErrorLogTramaType.CAMPO);
			}
			resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
		}
		return resultado;
	}

	private RespuestaLecturaArchivoVO validarCampoReglaNegocio(RespuestaLecturaArchivoVO resultado,
			TipoCampoType tipoCampoType, Map<String, ValueDataVO> map, Map.Entry<String, ValueDataVO> mapValue,
			String nomenclatura, Date fechaLote, String numeroLote, Map<String, String> configuracionTramaErrorMap,
			Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap, Long idConfiguracionTrama,
			String[] schemaTableName, Map<String, CampoTablaVO> campoDisponibleMap,
			ConfiguracionTramaDetalle configuracionTramaDetalle, Map<String, Object> parametroMap) throws Exception {
		boolean existeErrorValor = mapValue.getValue() != null && mapValue.getValue().toString().contains("${ERROR}");
		boolean tieneValor = !StringUtil.isNullOrEmpty(mapValue.getValue());
		boolean esObligatorio = RespuestaNaturalType.SI.getKey().equals(configuracionTramaDetalle.getObligatorio());
		boolean isCampoPersistente = !RespuestaNaturalType.NO.getKey()
				.equals(configuracionTramaDetalle.getEsPersistente());
		boolean existeError = false;
		resultado.setExisteError(existeError);
		// Validar Tipo
		boolean typeValido = true;
		Map<String, Object> valErrorMap = new HashMap<String, Object>();
		valErrorMap.put(ConstanteConfiguracionTramaUtil.CAMPO_NAME, configuracionTramaDetalle.getNombreCampo());
		valErrorMap.put(ConstanteConfiguracionTramaUtil.TABLA_NAME, schemaTableName[1]);
		valErrorMap.put(ConstanteConfiguracionTramaUtil.TIPO_ERROR_TRAMA, ErrorTipoType.EXTERNO.getKey());
		valErrorMap.put(ConstanteConfiguracionTramaUtil.NUMERO_HOJA + "_" + idConfiguracionTrama,
				ObjectUtil.objectToLong(
						parametroMap.get(ConstanteConfiguracionTramaUtil.NUMERO_HOJA + "_" + idConfiguracionTrama)));
		if (tieneValor) {
			if (mapValue.getValue().toString().contains("${type}")) {
				typeValido = false;
			}
		}
		// Validad Obligatoridad
		if (esObligatorio) {
			if (existeErrorValor) {
				tieneValor = false;
				existeErrorValor = false;
			}
			String valor = mapValue.getValue() + "";
			if (!valor.contains("{PROCESADO}")) {
				valor = valor.replace("${ERROR}", "Error");
				valor = valor.replace("${type}", tipoCampoType.toString());
			}
			if (!tieneValor) {
				// TODO:PONER EN PROPERTIES
				existeError = true;
				boolean isPrintError = false;
				String mensajeError = "Nombre archivo : " + nomenclatura + " Nombre Campo : "
						+ configuracionTramaDetalle.getNombreCampo() + " Campo Tabla : " + mapValue.getKey()
						+ " Esta configurado como obligatorio " + valor;
				if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
					mensajeError = "El campo está configurado como obligatorio";
				}
				if (!StringUtil.isNullOrEmpty(fechaLote) && !StringUtil.isNullOrEmpty(numeroLote)
						&& configuracionTramaErrorMap != null) {
					valErrorMap.put(ConstanteConfiguracionTramaUtil.TIPO_ERROR, ErrorCatalogoType.OBLIGATORIO.getKey()); // Tipo
																															// de
																															// error
																															// en
																															// la
																															// tabla
																															// de
																															// catalogo
					TasmeErrorMpe tasmeErrorMpe = generarLogError(mapValue.getValue().getFila(), mensajeError, map,
							configuracionTramaDetalleMap, configuracionTramaErrorMap, fechaLote, numeroLote,
							idConfiguracionTrama, configuracionTramaDetalle.getNombreCampo(), valErrorMap);
					if (tasmeErrorMpe != null) {
						resultado.getListaLogConfiguracionTrama().add(tasmeErrorMpe);
					} else {
						isPrintError = true;
					}
				} else {
					isPrintError = true;
				}
				if (isPrintError) {
					LogConfiguracionTrama logConfiguracionTrama = new LogConfiguracionTrama();
					if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
						logConfiguracionTrama = generarLogConfiguracionTrama(configuracionTramaDetalle.getNombreCampo(),
								mapValue.getValue().getFila(), "Campo Obligatorio",
								obtenerDescripcionError(mensajeError), mapValue.getKey(), idConfiguracionTrama,
								ComponenteValidadoLogTramaType.OBLIGATORIDAD, TipoErrorLogTramaType.CAMPO);
					} else {
						logConfiguracionTrama = generarLogConfiguracionTrama(configuracionTramaDetalle.getNombreCampo(),
								mapValue.getValue().getFila(), "Campo Obligatorio",
								obtenerDescripcionError("Fecha Lote = " + fechaLote + " numero Lote = " + numeroLote
										+ " " + mensajeError),
								mapValue.getKey(), idConfiguracionTrama, ComponenteValidadoLogTramaType.OBLIGATORIDAD,
								TipoErrorLogTramaType.CAMPO);
					}
					resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
				}
				// resultado.setEsError(true);
			}
			// Inicio nuevo requerimiento
			if (existeError) {
				Object valueDefault = obtenerValorDefautProcesamientoError(configuracionTramaDetalle);
				mapValue.getValue().setData(valueDefault);
				map.put(mapValue.getKey(), mapValue.getValue());
				existeError = false;
				resultado.setErrorTuplaProcesamiento(true);
			}
			// Fin nuevo requerimiento
			// tieneValor = !StringUtil.isNullOrEmpty(mapValue.getValue());
		}
		// error tipo de dato
		if (!typeValido) {
			existeError = true;
			if (ConstanteConfiguracionTramaUtil.TABLA_INTERMEDIA_EDGP.equals(schemaTableName[1])) {
				existeError = false;
				resultado.setErrorTuplaProcesamiento(true);
			}
			String valor = mapValue.getValue().toString();
			valor = valor.replace("${ERROR}", "Error");
			valor = valor.replace("${type}", tipoCampoType.toString());
			boolean isPrintError = false;
			String mensajeError = "";
			if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				if (TipoCampoType.FECHA.equals(tipoCampoType)) {
					mensajeError = "El formato de fecha no es el correcto ";
				} else {
					mensajeError = "El tipo de dato del campo no corresponde a lo configurado ";
				}
			} else {
				mensajeError = "Nombre archivo : " + nomenclatura + " Nombre Campo : "
						+ configuracionTramaDetalle.getNombreCampo() + " Campo Tabla : " + mapValue.getKey()
						+ " Tipo de campo No Válido " + valor;
			}

			if (!StringUtil.isNullOrEmpty(fechaLote) && !StringUtil.isNullOrEmpty(numeroLote)
					&& configuracionTramaErrorMap != null) {
				valErrorMap.put(ConstanteConfiguracionTramaUtil.TIPO_ERROR, ErrorCatalogoType.DATOS.getKey()); // Tipo
																												// de
																												// error
																												// en la
																												// tabla
																												// de
																												// catalogo
				TasmeErrorMpe tasmeErrorMpe = generarLogError(mapValue.getValue().getFila(), mensajeError, map,
						configuracionTramaDetalleMap, configuracionTramaErrorMap, fechaLote, numeroLote,
						idConfiguracionTrama, configuracionTramaDetalle.getNombreCampo(), valErrorMap);
				if (tasmeErrorMpe != null) {
					resultado.getListaLogConfiguracionTrama().add(tasmeErrorMpe);
				} else {
					isPrintError = true;
				}
			} else {
				isPrintError = true;
			}
			if (isPrintError) {
				LogConfiguracionTrama logConfiguracionTrama = new LogConfiguracionTrama();
				if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
					logConfiguracionTrama = generarLogConfiguracionTrama(configuracionTramaDetalle.getNombreCampo(),
							mapValue.getValue().getFila(), "Tipo No Válido", obtenerDescripcionError(mensajeError),
							mapValue.getKey(), idConfiguracionTrama, ComponenteValidadoLogTramaType.TIPO,
							TipoErrorLogTramaType.CAMPO);
					resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
				} else {
					logConfiguracionTrama = generarLogConfiguracionTrama(configuracionTramaDetalle.getNombreCampo(),
							mapValue.getValue().getFila(), "Tipo No Válido",
							obtenerDescripcionError(
									"Fecha Lote = " + fechaLote + " numero Lote = " + numeroLote + " " + mensajeError),
							mapValue.getKey(), idConfiguracionTrama, ComponenteValidadoLogTramaType.TIPO,
							TipoErrorLogTramaType.CAMPO);
					resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
				}
			}
			// resultado.setEsError(true);
		}
		// Validar Longitud
		if ((TipoCampoType.NUMERICO.equals(tipoCampoType) || TipoCampoType.TEXTO.equals(tipoCampoType))
				&& isCampoPersistente) {
			String valor = mapValue.getValue() + "";
			if (tieneValor && !existeErrorValor) {
				CampoTablaVO campoTablaVO = campoDisponibleMap.get(mapValue.getKey());
				Integer longitudCampo = valor.length();
				if (!StringUtil.isNullOrEmpty(campoTablaVO)) {
					if (!StringUtil.isNullOrEmpty(campoTablaVO.getLength())) {
						longitudCampo = (new BigDecimal(campoTablaVO.getLength())).intValue();
					}
				}
				Long longitudCampoUsuario = obtenerLongitud(configuracionTramaDetalle);
				if (valor.length() > longitudCampo.intValue()) {
					existeError = true;
					String mensajeError = "";
					if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
						mensajeError = "El valor excede la longitud permitida del campo en base de datos ";
					} else {
						mensajeError = "Nombre archivo : " + nomenclatura + " Nombre Campo : "
								+ configuracionTramaDetalle.getNombreCampo() + " Campo Tabla : " + mapValue.getKey()
								+ " Campo Longitud Excedida permitidos en el campo de la tabla";
					}
					boolean isPrintError = false;
					if (!StringUtil.isNullOrEmpty(fechaLote) && !StringUtil.isNullOrEmpty(numeroLote)
							&& configuracionTramaErrorMap != null) {
						valErrorMap.put(ConstanteConfiguracionTramaUtil.TIPO_ERROR_TRAMA,
								ErrorTipoType.INTERNO.getKey());
						valErrorMap.put(ConstanteConfiguracionTramaUtil.TIPO_ERROR,
								ErrorCatalogoType.LONGITUD.getKey()); // Tipo de error en la tabla de catalogo - cuando
																		// el usuario configuro mal es error interno
						TasmeErrorMpe tasmeErrorMpe = generarLogError(mapValue.getValue().getFila(), mensajeError, map,
								configuracionTramaDetalleMap, configuracionTramaErrorMap, fechaLote, numeroLote,
								idConfiguracionTrama, configuracionTramaDetalle.getNombreCampo(), valErrorMap);
						if (tasmeErrorMpe != null) {
							resultado.getListaLogConfiguracionTrama().add(tasmeErrorMpe);
						} else {
							isPrintError = true;
						}
					} else {
						isPrintError = true;
					}
					if (isPrintError) {
						LogConfiguracionTrama logConfiguracionTrama = new LogConfiguracionTrama();
						if (ResourceUtil
								.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
							logConfiguracionTrama = generarLogConfiguracionTrama(
									configuracionTramaDetalle.getNombreCampo(), mapValue.getValue().getFila(),
									"Campo Longitud Excedida", obtenerDescripcionError(mensajeError), mapValue.getKey(),
									idConfiguracionTrama, ComponenteValidadoLogTramaType.LONGITUD,
									TipoErrorLogTramaType.CAMPO);
							resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
						} else {
							logConfiguracionTrama = generarLogConfiguracionTrama(
									configuracionTramaDetalle.getNombreCampo(), mapValue.getValue().getFila(),
									"Campo Longitud Excedida",
									obtenerDescripcionError("Fecha Lote = " + fechaLote + " numero Lote = " + numeroLote
											+ " " + mensajeError),
									mapValue.getKey(), idConfiguracionTrama, ComponenteValidadoLogTramaType.LONGITUD,
									TipoErrorLogTramaType.CAMPO);
							resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
						}

					}
					// resultado.setEsError(true);
					// Inicio nuevo requerimiento
					if (existeError) {
						Object valueDefault = obtenerValorDefautProcesamientoErrorTamanho(valor,
								longitudCampo.intValue());
						mapValue.getValue().setData(valueDefault);
						map.put(mapValue.getKey(), mapValue.getValue());
						existeError = false;
						resultado.setErrorTuplaProcesamiento(true);
					}
					// Fin nuevo requerimiento
				}
				if (longitudCampoUsuario != null && valor.length() > longitudCampoUsuario.intValue()) {
					existeError = true;
					boolean isPrintError = false;
					String mensajeError = "";
					if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
						mensajeError = "El campo excede la longitud permitida configurada por el usuario ";
					} else {
						mensajeError = "Nombre archivo : " + nomenclatura + " Nombre Campo : "
								+ configuracionTramaDetalle.getNombreCampo() + " Campo Tabla : " + mapValue.getKey()
								+ " Campo Longitud Excedida configurado por el usuario";
					}
					if (!StringUtil.isNullOrEmpty(fechaLote) && !StringUtil.isNullOrEmpty(numeroLote)
							&& configuracionTramaErrorMap != null) {
						valErrorMap.put(ConstanteConfiguracionTramaUtil.TIPO_ERROR_TRAMA,
								ErrorTipoType.EXTERNO.getKey());
						valErrorMap.put(ConstanteConfiguracionTramaUtil.TIPO_ERROR,
								ErrorCatalogoType.LONGITUD.getKey()); // Tipo de error en la tabla de catalogo
						TasmeErrorMpe tasmeErrorMpe = generarLogError(mapValue.getValue().getFila(), mensajeError, map,
								configuracionTramaDetalleMap, configuracionTramaErrorMap, fechaLote, numeroLote,
								idConfiguracionTrama, configuracionTramaDetalle.getNombreCampo(), valErrorMap);
						if (tasmeErrorMpe != null) {
							resultado.getListaLogConfiguracionTrama().add(tasmeErrorMpe);
						} else {
							isPrintError = true;
						}
					} else {
						isPrintError = true;
					}
					if (isPrintError) {
						LogConfiguracionTrama logConfiguracionTrama = new LogConfiguracionTrama();
						if (ResourceUtil
								.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
							logConfiguracionTrama = generarLogConfiguracionTrama(
									configuracionTramaDetalle.getNombreCampo(), mapValue.getValue().getFila(),
									"Error validación longitud usuario", obtenerDescripcionError(mensajeError),
									mapValue.getKey(), idConfiguracionTrama, ComponenteValidadoLogTramaType.TIPO,
									TipoErrorLogTramaType.CAMPO);
							resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
						} else {
							logConfiguracionTrama = generarLogConfiguracionTrama(
									configuracionTramaDetalle.getNombreCampo(), mapValue.getValue().getFila(),
									"Error validación longitud usuario",
									obtenerDescripcionError("Fecha Lote = " + fechaLote + " numero Lote = " + numeroLote
											+ " " + mensajeError),
									mapValue.getKey(), idConfiguracionTrama, ComponenteValidadoLogTramaType.TIPO,
									TipoErrorLogTramaType.CAMPO);
							resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
						}
					}
					// resultado.setEsError(true);
					// Inicio nuevo requerimiento
					if (existeError) {
						Object valueDefault = obtenerValorDefautProcesamientoErrorTamanho(valor,
								longitudCampoUsuario.intValue());
						mapValue.getValue().setData(valueDefault);
						map.put(mapValue.getKey(), mapValue.getValue());
						existeError = false;
						resultado.setErrorTuplaProcesamiento(true);
					}
					// Fin nuevo requerimiento
				}
			}
		}
		if (existeErrorValor && typeValido && !esObligatorio) {
			// existeError = true;
			String valor = mapValue.getValue() + "";
			if (!valor.contains("{PROCESADO}")) {
				valor = valor.replace("${ERROR}", "Error");
				valor = valor.replace("${type}", tipoCampoType.toString());
				boolean isPrintError = true;
				String mensajeError = "Nombre archivo : " + nomenclatura + " Nombre Campo : "
						+ configuracionTramaDetalle.getNombreCampo() + " Campo Tabla : " + mapValue.getKey()
						+ "  Error obtener al información de " + valor;
				if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
					mensajeError = valor;
				}

				if (isPrintError) {
					LogConfiguracionTrama logConfiguracionTrama = new LogConfiguracionTrama();
					if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
						logConfiguracionTrama = generarLogConfiguracionTrama(configuracionTramaDetalle.getNombreCampo(),
								mapValue.getValue().getFila(), "Error procesamiento  tipo Campo",
								obtenerDescripcionError(mensajeError), mapValue.getKey(), idConfiguracionTrama,
								ComponenteValidadoLogTramaType.TIPO, TipoErrorLogTramaType.CAMPO);
						resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
					} else {
						logConfiguracionTrama = generarLogConfiguracionTrama(configuracionTramaDetalle.getNombreCampo(),
								mapValue.getValue().getFila(), "Error procesamiento  tipo Campo",
								obtenerDescripcionError("Fecha Lote = " + fechaLote + " numero Lote = " + numeroLote
										+ " " + mensajeError),
								mapValue.getKey(), idConfiguracionTrama, ComponenteValidadoLogTramaType.TIPO,
								TipoErrorLogTramaType.CAMPO);
						resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
					}
				}
			}
			// resultado.setEsError(true);
			map.put(mapValue.getKey(), new ValueDataVO(""));
		}
		resultado.setMap(map);
		resultado.setExisteError(existeError);
		return resultado;
	}

	private ValueDataVO parserarTipoCampoByRegla(TipoCampoType tipoCampoType, ValueDataVO data, String nombeCampoTabla,
			Map<String, String> campoMappingTypeMap, ConfiguracionTramaDetalle configuracionTramaDetalle,
			Map<String, Object> parametroMap) throws Exception {
		if (data == null || data.getData() == null) {
			return data;
		}
		if (data.getData().toString().contains("${ERROR}")) {
			return data;
		}
		if (tipoCampoType != null) {
			switch (tipoCampoType) {
			case FECHA:
				if (data.getData().toString().contains(";")) {
					String[] dataTemp = data.getData().toString().split(";", -1);
					if (dataTemp.length > 1) {
						data.setData(TransferDataUtil.obtenerValueParse(dataTemp[0],
								campoMappingTypeMap.get(nombeCampoTabla), dataTemp[1], 0, parametroMap).getData());
					}
				} else {
					try {
						if (!(data.getData() instanceof Date)) {
							Date date = FechaUtil.obtenerFechaFormatoPersonalizado(data.getData().toString(),
									"dd/MM/yyyy");
							data.setData(date);
						}
					} catch (Exception e) {
						data.setData("${ERROR}El campo no tiene formato ejemplo 01/01/1900;dd/MM/yyyy");
					}
				}
				break;
			case TEXTO:
				if (data.getData().toString().contains(";")) {
					String[] dataTemp = data.getData().toString().split(";", -1);
					if (dataTemp.length > 0) {
						data.setData(dataTemp[0]);
					}
				}
				break;

			case NUMERICO:
				if (data.getData().toString().contains(";")) {
					String[] dataTemp = data.getData().toString().split(";", -1);
					if (dataTemp.length > 0) {
						data.setData(
								TransferDataUtil
										.obtenerValueParse(dataTemp[0], campoMappingTypeMap.get(nombeCampoTabla),
												configuracionTramaDetalle.getFormatoCampo(), 0, parametroMap)
										.getData());
					}
				} else {
					data.setData(TransferDataUtil
							.obtenerValueParse(data.getData().toString(), campoMappingTypeMap.get(nombeCampoTabla),
									configuracionTramaDetalle.getFormatoCampo(), 0, parametroMap)
							.getData());
				}
				break;
			default:
				break;
			}
		}
		return data;
	}

	private List<Map<String, Object>> validarTuplaDefault(List<Map<String, Object>> listaDataResulGrupoMap)
			throws Exception {
		return listaDataResulGrupoMap;

	}

	@Override
	public String obtenerNomenclaturaPer(ConfiguracionTrama configuracionTrama, Map<String, Object> parametrosMap,
			boolean isManual, boolean esSimulacion) throws Exception {
		return obtenerNomenclatura(configuracionTrama, parametrosMap, isManual, esSimulacion);
	}

	@Override
	public Map<Long, List<ConfiguracionFtpTrama>> listarConfiguracionFTPTramaMap(List<Long> listaIdJuegoTrama)
			throws Exception {
		// obtenerSessionHibernate();
		return configuracionTramaDao.listarConfiguracionFTPTramaMap(listaIdJuegoTrama);
	}

	@Override
	public BigMemoryManager<String, ConfiguracionTramaDataVO> generarTupaPersistente(
			List<Map<String, ValueDataVO>> listaDataResulGrupoMap, Map<Integer, Boolean> errorTuplaMap,
			String[] schemaTableName, Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap,
			Map<String, String> campoNoPersistente, Map<String, Object> parametroMap) throws Exception {
		String rutaRelativaTemp = parametroMap.get("rutaRelativaTemp") + "";
		String rutaRelativaTempTable = ConstanteConfigUtil.generarRuta(rutaRelativaTemp)
				+ schemaTableName[1].toLowerCase();
		BigMemoryManager<String, ConfiguracionTramaDataVO> resultadoListaConf = new BigMemoryManager<String, ConfiguracionTramaDataVO>(
				rutaRelativaTempTable, "data_persist_table");
		int errorTupaIndice = 0;
		for (Map<String, ValueDataVO> map : listaDataResulGrupoMap) {
			ConfiguracionTramaDataVO resultadoTemp = new ConfiguracionTramaDataVO();
			resultadoTemp.setSchemaName(schemaTableName[0]);
			resultadoTemp.setTableName(schemaTableName[1]);
			List<ConfiguracionTramaDataVO> listaCampoValue = new ArrayList<ConfiguracionTramaDataVO>();
			boolean existeErrorTupla = false;
			for (Map.Entry<String, ValueDataVO> mapValue : map.entrySet()) {
				errorTupaIndice++;
				ConfiguracionTramaDataVO resultadoCampoValue = new ConfiguracionTramaDataVO();
				resultadoCampoValue.setSchemaName(schemaTableName[0]);
				resultadoCampoValue.setTableName(schemaTableName[1]);
				resultadoCampoValue.setAtributeName(mapValue.getKey());
				// validar
				ConfiguracionTramaDetalle configuracionTramaDetalle = configuracionTramaDetalleMap
						.get(mapValue.getKey());
				boolean esCampoNegocio = false; // TODO:AUMENTAR CAMPO - MARTIN REALIZADO
				if (RespuestaNaturalType.SI.getKey().equals(configuracionTramaDetalle.getFlagCampoNegocio())) {
					esCampoNegocio = true;
				}
				resultadoCampoValue.setEsCampoNegocio(esCampoNegocio);
				TipoCampoType tipoCampoType = obtenerTipoCampo(configuracionTramaDetalle);
				resultadoCampoValue.setAtributeValue(mapValue.getValue());
				resultadoCampoValue.setAtributeType(tipoCampoType.getKey());
				if (!campoNoPersistente.containsKey(mapValue.getKey())) {
					listaCampoValue.add(resultadoCampoValue);
				}
				if (!existeErrorTupla) {
					if (errorTuplaMap.containsKey(errorTupaIndice)) {
						existeErrorTupla = errorTuplaMap.get(errorTupaIndice);
					}
				}

			}
			resultadoTemp.setListaCampoValue(listaCampoValue);
			if (!existeErrorTupla) {
				resultadoListaConf.add(resultadoTemp);
			}
		}
		return resultadoListaConf;
	}

	private ResultadoProcesoConfiguracionTramaVO generarObjetoError(String nombreCampo, String fila, String codigoError,
			String mensaje, Long idConfigJuegoTrama, Long idConfiguracionTrama, Long idNomenclaturaArchivo,
			String tipoError) {
		ResultadoProcesoConfiguracionTramaVO resulTemp = new ResultadoProcesoConfiguracionTramaVO();
		resulTemp.setIdConfigJuegoTrama(idConfigJuegoTrama);
		resulTemp.setIdNomenclaturaArchivo(idNomenclaturaArchivo);
		resulTemp.setIdConfiguracionTrama(idConfiguracionTrama);
		resulTemp.setFila(fila);
		resulTemp.setEsError(true);
		resulTemp.setCodigoError(codigoError);
		resulTemp.setMensajeError(mensaje);
		resulTemp.setNombreCampo(nombreCampo);
		resulTemp.setTipoError(tipoError);
		return resulTemp;
	}

	@Override
	public List<ConfiguracionTrama> listarConfiguracionTrama(ConfiguracionTrama configuracionTrama) {
		return this.configuracionTramaDao.listar(configuracionTrama);
	}
}
