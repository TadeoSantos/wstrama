package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.csv;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfiguracionTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

/**
 * La Class CSVUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class CSVUtil {

	/** 
	 * Logger para el registro de errores.
	 */
	private static final Logger log = Logger.getLogger(TransferDataUtil.class);
	
	public static List<Map<String, ValueDataVO>> transferObjetoEntityCSVMapDTO(BufferedReader br, int filaData, Map<String, Object> campoMappingCVSMap, Map<String, String> campoMappingExcelTypeMap, Map<String, String> campoMappingFormatoMap, String csvSplitBy, Integer cantidadData, Map<String, Object> parametroMap,Map<String,Character> configuracionTramaDetalleMap) throws Exception {
		parametroMap.put(ConstanteConfiguracionTramaUtil.FILA_DATA_ORIGINAL, filaData);
		parametroMap.put("filaData", filaData);
		parametroMap.put("cantidadData", cantidadData);
		parametroMap.put("csvSplitBy", csvSplitBy);
		return TransferDataUtil.transferObjetoEntityCSVMapDTO(campoMappingCVSMap, br, campoMappingExcelTypeMap, campoMappingFormatoMap, parametroMap,configuracionTramaDetalleMap);
	}

	public static BufferedReader leerCVS(String pathFile) {
		BufferedReader resultado = null;
		InputStream in = null;
		try {
			File f = new File(pathFile);
			in = new FileInputStream(f);		
		} catch (Exception e) {
			log.error("Error",e);
		} 
		
		try {
			 resultado = new BufferedReader(new InputStreamReader(in , "latin1"));
		} catch (Exception e) {
			log.error("Error",e);
		} 
		
		return resultado;
	}
	public static BufferedReader leerCVS(byte[] datosArchivo) {
		BufferedReader resultado = null;
		InputStream in = null;
		try {
			in = new ByteArrayInputStream(datosArchivo);	
		} catch (Exception e) {
			log.error("Error",e);
		} 
		try {
			 resultado = new BufferedReader(new InputStreamReader(in , "latin1"));
		} catch (Exception e) {
			log.error("Error",e);
		} 
		
		return resultado;
	}
}