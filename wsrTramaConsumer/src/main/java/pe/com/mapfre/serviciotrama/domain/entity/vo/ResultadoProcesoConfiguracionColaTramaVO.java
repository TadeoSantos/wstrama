package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ResultadoProcesoConfiguracionColaTramaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ResultadoProcesoConfiguracionColaTramaVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String codigoError;
	private String mensajeError;
	private boolean esError;
	private Integer cantidadTramasVob;
	private List<ResultadoProcesoConfiguracionTramaVO> listaResultadoProcesoConfiguracionTramaVO;
	
	public ResultadoProcesoConfiguracionColaTramaVO() {
		super();
	}

	public ResultadoProcesoConfiguracionColaTramaVO(String codigoError, String mensajeError,
			boolean esError) {
		super();
		this.codigoError = codigoError;
		this.mensajeError = mensajeError;
		this.esError = esError;
	}
	public boolean isEsError() {
		return esError;
	}

}
