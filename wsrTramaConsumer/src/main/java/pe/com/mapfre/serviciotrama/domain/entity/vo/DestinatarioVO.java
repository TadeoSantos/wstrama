package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.BasePaginator;

/**
 * La Class DestinatarioVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class DestinatarioVO extends BasePaginator implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id destinatario. */
    private Long idDestinatario;
   
    /** El nombre destinatario. */
    private String nombreDestinatario;
   
    /** El cargo destinatario. */
    private String cargoDestinatario;
   
    /** El correo destinatario. */
    private String correoDestinatario;
   
    /** El estado destinatario. */
    private String estadoDestinatario;
   
    /** El codigo usuario. */
    private String codigoUsuario;
   
    /** El fecha activo. */
    private Date fechaActivo;
    
    private boolean opcionAccionActivar;
   
    /**
     * Instancia un nuevo mantenimiento destinatarioDTO.
     */
    public DestinatarioVO() {
    }
   
   
    /**
     * Instancia un nuevo mantenimiento destinatarioDTO.
     *
     * @param idDestinatario el id destinatario
     * @param nombreDestinatario el nombre destinatario
     * @param cargoDestinatario el cargo destinatario
     * @param correoDestinatario el correo destinatario
     * @param estadoDestinatario el estado destinatario
     * @param codigoUsuario el codigo usuario
     * @param fechaActivo el fecha activo
     */
    public DestinatarioVO(Long idDestinatario, String nombreDestinatario, String cargoDestinatario, String correoDestinatario, String estadoDestinatario, String codigoUsuario, Date fechaActivo ) {
        super();
        this.idDestinatario = idDestinatario;
        this.nombreDestinatario = nombreDestinatario;
        this.cargoDestinatario = cargoDestinatario;
        this.correoDestinatario = correoDestinatario;
        this.estadoDestinatario = estadoDestinatario;
        this.codigoUsuario = codigoUsuario;
        this.fechaActivo = fechaActivo;
    }
    
    public boolean isOpcionAccionActivar() {
		return opcionAccionActivar;
	}
  
}