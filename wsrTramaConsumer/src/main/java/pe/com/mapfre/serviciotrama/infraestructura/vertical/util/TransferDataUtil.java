package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Hibernate;
import org.hibernate.proxy.HibernateProxy;

import org.springframework.web.context.request.WebRequest;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CollectionUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.ScriptSqlResulJDBCVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.ConfiguracionCacheUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.AtributoEntityVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

/**
 * La Class TransferDataUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class TransferDataUtil extends TransferDataObjectValidarUtil implements Serializable {

	private static final String SERIAL_VERSION_UID = "serialVersionUID";

	private static final String CANTIDAD_DATA = "cantidadData";

	private static final String FILA_DATA = "filaData";

	private static final String CAMPO_MAPPING_FORMATO_SUB_OBJECT_MAP = "campoMappingFormatoSubObjectMap";

	private static final String CAMPO_MAPPING_EXCEL_SUB_OBJECT_MAP = "campoMappingExcelSubObjectMap";

	private static final String GRUPO_SUB_OBJECTO_MAP = "grupoSubObjectoMap";

	private static final String DEFAULT_CHARSET = "UTF-8";

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** El log. */
	private static final Logger log = Logger.getLogger(TransferDataUtil.class);

	/**
	 * Instancia un nuevo data export excel.
	 */
	public TransferDataUtil() {
		//
	}

	public static <T> T toPojo(Object ressul, Class<T> entityClassVO) {
		String className = "";
		if (ressul == null) {
			return null;
		}
		try {
			T resultado = entityClassVO.getDeclaredConstructor().newInstance();
			className = ressul.getClass().getName();
			String handlerHibernate = obtenerHandlerHibernate(className);
			if (className.contains(handlerHibernate)) {
				int indexOf = className.indexOf(handlerHibernate);
				className = className.substring(0, indexOf);
				Hibernate.initialize(ressul);
				if (ressul instanceof HibernateProxy) {
					ressul = ((HibernateProxy) ressul).getHibernateLazyInitializer().getImplementation();
				}
			}
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			Map<String, Integer> fieldHerenciaMap = fieldHerenciaMap(ressul);
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (!SERIAL_VERSION_UID.equalsIgnoreCase(objAtr.getNombreAtributo())) {
					try {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						setField(f, resultado, fValue.get(ressul));
					} catch (Exception e) {
						//
					}

				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toPojo() al parsear class name = " + className + ", "
					+ entityClassVO.getName() + "  " + e.getMessage());
		}
		return null;
	}

	/**
	 * Transfer objeto entity get rest dto.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param info
	 *            el info
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the t
	 */
	public static <T> T toRestDTO(@Context UriInfo info, Class<T> entityClassDTO) {
		T resultado = null;
		try {
			if (info == null) {
				return null;
			}
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassDTO);
			resultado = entityClassDTO.getDeclaredConstructor().newInstance();
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);
			for (AtributoEntityVO objAtr : listaAtributos) {
				Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
				Object value = obtenerValor(info.getQueryParameters().getFirst(objAtr.getNombreAtributo()), objAtr,false);
				setField(f, resultado, value);
			}

		} catch (Exception e) {
			log.error("Error TransferDataUtil.toRestDTO() al parsear " + entityClassDTO.getName() + "  "
					+ e.getMessage());
		}
		return resultado;
	}

	public static Map<String, Object> toGetRestMap(WebRequest info) {
		Map<String, Object> resultado = new HashMap<>();
		try {
			if (info == null) {
				return resultado;
			}
			for (String key : info.getParameterMap().keySet()) {
				resultado.put(key.toUpperCase(), info.getParameter(key));
			}

		} catch (Exception e) {
			log.error("Error TransferDataObjectUtil.transferObjetoEntityGetRestMap " + e.getMessage());
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity historial.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the t
	 */
	public static <T> T toHistorial(Object ressul, Class<T> entityClassDTO) {
		try {
			if (ressul == null) {
				return null;
			}
			T resultado = entityClassDTO.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass().getName());
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (objAtr.isColumn()) {
					try {
						Field f = getField(resultado, objAtr.getNombreAtributo());
						Field fValue = getField(ressul, objAtr.getNombreAtributo());
						setField(f, resultado, fValue.get(ressul));
					} catch (Exception e) {
						//
					}
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toHistorial() al parsear " + entityClassDTO.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	/**
	 * Transfer objeto entity dto list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param <E>
	 *            el tipo de elemento
	 * @param ressulList
	 *            el ressul list
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the list
	 */
	public static <T, E> List<T> toList(List<E> ressulList, Class<T> entityClassDTO) {
		List<T> resultado = new ArrayList<>();
		if (ressulList == null) {
			return resultado;
		}
		for (Object ressul : ressulList) {
			T resultadoTemp = toDTO(ressul, entityClassDTO);
			resultado.add(resultadoTemp);
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity dto list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param <E>
	 *            el tipo de elemento
	 * @param ressulList
	 *            el ressul list
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the list
	 */
	public static <T, E> List<T> toList(List<E> ressulList, Class<T> entityClassDTO, String... entityClasess) {
		List<T> resultado = new ArrayList<>();
		if (ressulList == null) {
			return resultado;
		}
		for (Object ressul : ressulList) {
			T resultadoTemp = toDTO(ressul, entityClassDTO, entityClasess);
			resultado.add(resultadoTemp);
		}
		return resultado;
	}

	private static <T> T transferObjetoEntityDTOPK(Object ressul, Class<T> entityClassDTO, String... entityClasess) {
		return toEntityDTO(ressul, entityClassDTO, true, entityClasess);
	}

	public static <T> T toDTO(Object ressul, Class<T> entityClassDTO, String... entityClasess) {
		return toEntityDTO(ressul, entityClassDTO, false, entityClasess);
	}

	/**
	 * Transfer objeto entity dto.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the t
	 */
	private static <T> T toEntityDTO(Object ressul, Class<T> entityClassDTO, boolean esPK, String... entityClasess) {
		String className = "";
		if (ressul == null) {
			return null;
		}

		try {

			T resultado = entityClassDTO.getDeclaredConstructor().newInstance();
			className = ressul.getClass().getName();
			String handlerHibernate = obtenerHandlerHibernate(className);
			if (className.contains(handlerHibernate)) {
				int indexOf = className.indexOf(handlerHibernate);
				className = className.substring(0, indexOf);
				Hibernate.initialize(ressul);
				if (ressul instanceof HibernateProxy) {
					ressul = ((HibernateProxy) ressul).getHibernateLazyInitializer().getImplementation();
				}
			}
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(className);
			Map<String, Integer> fieldHerenciaMap = fieldHerenciaMap(ressul);
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (esPK) {
					if (objAtr.isColumn() && objAtr.isEsPK()) {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						setField(f, resultado, fValue.get(ressul));
						break;
					}
				} else {
					if (objAtr.isColumn() || objAtr.isTransient()) {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						setField(f, resultado, fValue.get(ressul));
					}
				}

			}
			if (entityClasess != null && entityClasess.length > 0) {
				for (String clasesPojoTemp : entityClasess) {
					boolean isSubClase = clasesPojoTemp.contains(":");
					String[] entitySubClasess = null;
					if (isSubClase) {
						String[] dataTempClase = clasesPojoTemp.split(":");
						clasesPojoTemp = dataTempClase[0];
						String dataTempArray = dataTempClase[1];
						if (dataTempArray.contains("{")) {
							int indexOf = dataTempArray.indexOf("{");
							int lastIndexOf = dataTempArray.lastIndexOf("}");
							dataTempArray = dataTempArray.substring(indexOf + 1, lastIndexOf);
						}

						String[] entitySubClasessTemp = dataTempArray.split(";", -1);
						if (entitySubClasessTemp != null && entitySubClasessTemp.length > 0) {
							entitySubClasess = new String[entitySubClasessTemp.length];
							int index = 0;
							for (String dataTemp : entitySubClasessTemp) {
								if (!dataTemp.contains("{") && !dataTemp.contains("}")) {
									entitySubClasess[index] = dataTemp;
									index++;
								}
							}
						} else {
							if (entitySubClasessTemp != null) {
								entitySubClasess = new String[1];
								String dataTemp = entitySubClasessTemp[0];
								int indexOf = dataTemp.indexOf("{");
								int lastIndexOf = dataTemp.lastIndexOf("}");
								dataTemp = dataTemp.substring(indexOf + 1, lastIndexOf);
								entitySubClasess[0] = dataTemp;
							}
						}

					}
					String clasesPojo = clasesPojoTemp;
					boolean esTansferSoloPK = false;
					if (clasesPojoTemp.contains("@PK@")) {
						esTansferSoloPK = true;
						clasesPojo = clasesPojoTemp.substring(0, clasesPojoTemp.indexOf("@PK@"));
					}

					try {
						Field f = getField(resultado, clasesPojo);
						if (f != null) {
							Field fValue = getField(ressul, clasesPojo);
							Object valueTransfer = fValue.get(ressul);
							if (valueTransfer == null) {
								valueTransfer = fValue.getType().getDeclaredConstructor().newInstance();
							}
							Object value = null;
							if (esTansferSoloPK) {
								value = transferObjetoEntityDTOPK(valueTransfer, f.getType());
							} else {
								if (entitySubClasess != null) {
									value = toDTO(valueTransfer, f.getType(), entitySubClasess);
								} else {
									value = toDTO(valueTransfer, f.getType());
								}

							}
							setField(f, resultado, value);
						}
					} catch (Exception e) {
						//
					}

				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntityDTO() al parsear class name = " + className + ", "
					+ entityClassDTO.getName() + "  " + e.getMessage());
		}
		return null;
	}

	public static <T> T to(Object ressul, Class<T> entityClassEntity, boolean isFiltro, String... entityClasess) {
		return toEntity(ressul, entityClassEntity, false, isFiltro, entityClasess);
	}

	private static <T> T transferObjetoEntityPK(Object ressul, Class<T> entityClassEntity, boolean isFiltro,
			String... entityClasess) {
		return toEntity(ressul, entityClassEntity, true, isFiltro, entityClasess);
	}

	/**
	 * Transfer objeto entity.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the t
	 */
	public static <T> T toEntity(Object ressul, Class<T> entityClassEntity, boolean esPK, boolean isFiltro,
			String... entityClasess) {
		if (ressul == null) {
			return null;
		}
		try {

			T resultado = entityClassEntity.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity.getName());
			if (!esPK) {
				entityClasess = obtenerListaAtributosPK(entityClassEntity, entityClasess);
			}
			Map<String, Integer> fieldHerenciaMap = fieldHerenciaMap(ressul);
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);

			for (AtributoEntityVO objAtr : listaAtributos) {
				if (esPK) {
					if ((objAtr.isColumn() || objAtr.isTransient()) && objAtr.isEsPK()) {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						Object value = fValue.get(ressul);
						if (!StringUtil.isNullOrEmpty(value)) {
							setField(f, resultado, value);
						} else {
							if (!isFiltro) {
								resultado = null;
							}
						}
						break;
					}

				} else {
					if (objAtr.isColumn() || objAtr.isTransient()) {
						Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						setField(f, resultado, fValue.get(ressul));
					}
				}
			}
			if (entityClasess != null && entityClasess.length > 0) {
				for (String clasesPojoTemp : entityClasess) {
					boolean isSubClase = clasesPojoTemp.contains(":");
					String[] entitySubClasess = null;
					if (isSubClase) {
						String[] dataTempClase = clasesPojoTemp.split(":");
						clasesPojoTemp = dataTempClase[0];
						String dataTempArray = dataTempClase[1];
						if (dataTempArray.contains("{")) {
							int indexOf = dataTempArray.indexOf("{");
							int lastIndexOf = dataTempArray.lastIndexOf("}");
							dataTempArray = dataTempArray.substring(indexOf + 1, lastIndexOf);
						}

						String[] entitySubClasessTemp = dataTempArray.split(";", -1);
						if (entitySubClasessTemp != null && entitySubClasessTemp.length > 0) {
							entitySubClasess = new String[entitySubClasessTemp.length];
							int index = 0;
							for (String dataTemp : entitySubClasessTemp) {
								if (!dataTemp.contains("{") && !dataTemp.contains("}")) {
									entitySubClasess[index] = dataTemp;
									index++;
								}
							}
						} else {
							if (entitySubClasessTemp != null) {
								entitySubClasess = new String[1];
								String dataTemp = entitySubClasessTemp[0];
								int indexOf = dataTemp.indexOf("{");
								int lastIndexOf = dataTemp.lastIndexOf("}");
								dataTemp = dataTemp.substring(indexOf + 1, lastIndexOf);
								entitySubClasess[0] = dataTemp;
							}
						}

					}
					String clasesPojo = clasesPojoTemp;
					boolean esTansferSoloPK = false;
					if (clasesPojoTemp.contains("@PK@")) {
						esTansferSoloPK = true;
						clasesPojo = clasesPojoTemp.substring(0, clasesPojoTemp.indexOf("@PK@"));
					}

					try {
						Field f = getField(resultado, clasesPojo);
						if (f != null) {
							Field fValue = getField(ressul, clasesPojo);
							Object valueTransfer = fValue.get(ressul);
							if (valueTransfer == null) {
								valueTransfer = fValue.getType().getDeclaredConstructor().newInstance();
							}
							Object value = null;
							if (esTansferSoloPK) {
								value = transferObjetoEntityPK(valueTransfer, f.getType(), isFiltro);
							} else {
								if (entitySubClasess != null) {
									value = to(valueTransfer, f.getType(), isFiltro, entitySubClasess);
								} else {
									value = to(valueTransfer, f.getType(), isFiltro);
								}

							}
							setField(f, resultado, value);
						}
					} catch (Exception e) {
						//
					}

				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntity() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	public static <T> T toEntityVO(Object ressul, Class<T> entityClassEntity) {
		if (ressul == null) {
			return null;
		}
		try {
			T resultado = entityClassEntity.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity.getName());
			for (AtributoEntityVO objAtr : listaAtributos) {
				try {
					if (objAtr.isColumn()) {
						Field f = getField(resultado, objAtr.getNombreAtributo());
						Field fValue = getField(ressul, objAtr.getNombreAtributo());
						setField(f, resultado, fValue.get(ressul));
					}
				} catch (Exception e) {
					//
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.transferObjetoEntity() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	public static Map<String, Object> toEntityAtributeMap(Object ressul) {
		return toMap(ressul, false, false);

	}

	/**
	 * Transfer objeto entity atribute map.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @return the map
	 */
	public static Map<String, Object> toMap(Object ressul, boolean isNative, boolean isCargarManyToOne) {
		if (ressul == null) {
			return Collections.emptyMap();
		}
		try {
			Map<String, Object> resultado = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			resultado.putAll(obtenerEntityAtributeMap(listaAtributos, ressul, isNative, false, false));
			if (isCargarManyToOne) {
				listaAtributos = obtenerListaAtributosManyToOne(ressul.getClass());
				resultado.putAll(obtenerEntityAtributeMap(listaAtributos, ressul, isNative, true, false));
			}
			return resultado;
		} catch (Exception e) {
			log.error(
					"Error TransferDataUtil.toMap() al parsear " + ressul.getClass().getName() + "  " + e.getMessage());
		}
		return Collections.emptyMap();
	}

	private static Map<String, Object> obtenerEntityAtributeMap(List<AtributoEntityVO> listaAtributos, Object ressul,
			boolean isNative, boolean isManyToOne, boolean isPK) throws Exception {
		Map<String, Object> resultado = new HashMap<>();
		for (AtributoEntityVO objAtr : listaAtributos) {
			if (!StringUtil.isNullOrEmpty(objAtr.getNombreColumna()) || objAtr.isPKCompuesta()) {
				if (!isManyToOne || isPK) {
					if (isPK) {
						if (objAtr.isEsPK() && !objAtr.isPKCompuesta()) {
							Field fValue = getField(ressul, objAtr.getNombreAtributo());
							Object value = fValue.get(ressul);
							String key = isNative ? objAtr.getNombreColumna() : objAtr.getNombreAtributo();
							if (value == null) {
								resultado.put(key, "");
							} else {
								resultado.put(key, value);
							}
							break;
						} else {
							if (objAtr.isEsPK() && objAtr.isPKCompuesta()) {
								Field fValue = getField(ressul, objAtr.getNombreAtributo());
								Object value = fValue.get(ressul);
								if (value == null) {
									value = fValue.getType().getDeclaredConstructor().newInstance();
								}
								resultado.putAll(toMap(value, isNative, false));
								break;
							}

						}
					} else {
						if (!objAtr.isPKCompuesta()) {
							Field fValue = getField(ressul, objAtr.getNombreAtributo());
							Object value = fValue.get(ressul);
							String key = isNative ? objAtr.getNombreColumna() : objAtr.getNombreAtributo();
							if (value == null) {
								resultado.put(key, "");
							} else {
								resultado.put(key, value);
							}
						} else {
							Field fValue = getField(ressul, objAtr.getNombreAtributo());
							Object value = fValue.get(ressul);
							if (value == null) {
								value = fValue.getType().getDeclaredConstructor().newInstance();
							}
							resultado.putAll(toMap(value, isNative, false));
						}
					}

				} else {
					Field fValue = getField(ressul, objAtr.getNombreAtributo());
					Object valueTransfer = fValue.get(ressul);
					List<AtributoEntityVO> listaAtributosManyToOne = obtenerListaAtributos(valueTransfer.getClass());
					Map<String, Object> valueMap = obtenerEntityAtributeMap(listaAtributosManyToOne, valueTransfer,
							isNative, false, true);
					Object value = valueMap.get(getAtributoPK(listaAtributosManyToOne, isNative));
					String key = isNative ? objAtr.getNombreColumna() : objAtr.getNombreAtributo();
					if (value == null) {
						resultado.put(key, "");
					} else {
						resultado.put(key, value);
					}
				}

			}
		}
		return resultado;
	}

	private static String getAtributoPK(List<AtributoEntityVO> listaAtributosManyToOne, boolean isNative) {
		String resultado = "";
		for (AtributoEntityVO objAtr : listaAtributosManyToOne) {
			if (!StringUtil.isNullOrEmpty(objAtr.getNombreColumna()) || objAtr.isEsPK()) {
				resultado = isNative ? objAtr.getNombreColumna() : objAtr.getNombreAtributo();
				break;
			}
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity campos map.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @return the map
	 */
	public static Map<String, Object> toEntityMap(Object ressul) {// OPTIMIZA$R
		return toMap(ressul, true, false);
		/*
		 * if (ressul == null) { return Collections.emptyMap(); } try { Map<String,
		 * Object> resultado = new HashMap<>(); List<AtributoEntityVO> listaAtributos =
		 * obtenerListaAtributos(ressul.getClass()); for (AtributoEntityVO objAtr :
		 * listaAtributos) { if (!StringUtil.isNullOrEmpty(objAtr.getNombreColumna()) ||
		 * objAtr.isPKCompuesta()) { if (!objAtr.isPKCompuesta()) { Field fValue =
		 * getField(ressul, objAtr.getNombreAtributo()); Object value =
		 * fValue.get(ressul); if (value == null) {
		 * resultado.put(objAtr.getNombreColumna(), ""); } else {
		 * resultado.put(objAtr.getNombreColumna(), value); } } else { Field fValue =
		 * getField(ressul, objAtr.getNombreAtributo()); Object value =
		 * fValue.get(ressul); if (value == null) { value =
		 * fValue.getType().getDeclaredConstructor().newInstance(); }
		 * resultado.putAll(toEntityMap(value));
		 * 
		 * } } } return resultado; } catch (Exception e) {
		 * log.error("Error TransferDataUtil.toEntityMap() al parsear " +
		 * ressul.getClass().getName() + "  " + e.getMessage()); } return
		 * Collections.emptyMap();
		 */
	}

	/**
	 * Transfer objeto vo atribute map.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param listaObjeto
	 *            el lista objeto
	 * @param listaAtributo
	 *            el lista atributo
	 * @param isExcluir
	 *            el is excluir
	 * @return the map
	 */
	public static Map<String, Map<String, Object>> toVOMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir) {
		if (ressul == null) {
			return Collections.emptyMap();
		}
		try {
			if (listaAtributo == null) {
				listaAtributo = new ArrayList<>();
			}
			if (isExcluir && !listaAtributo.contains(SERIAL_VERSION_UID)) {
				listaAtributo.add(SERIAL_VERSION_UID);
			}
			if (listaObjeto == null) {
				listaObjeto = new HashMap<>();
			}
			Map<String, Map<String, Object>> resultado = new HashMap<>();
			Map<String, Object> resultadoValue = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			for (AtributoEntityVO objAtr : listaAtributos) {
				boolean isObtenerAtributo = false;
				if (isExcluir) {
					isObtenerAtributo = !listaAtributo.contains(objAtr.getNombreAtributo());
				} else {
					isObtenerAtributo = listaAtributo.contains(objAtr.getNombreAtributo());
				}
				if (isObtenerAtributo) {
					try {
						Field fValue = getField(ressul, objAtr.getNombreAtributo());
						Object value = fValue.get(ressul);
						if (value != null) {
							if (!listaObjeto.containsKey(objAtr.getNombreAtributo())) {
								if (!StringUtil.isNullOrEmpty(value)) {
									if (!objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
										resultadoValue.put(objAtr.getNombreAtributo(), value);
									} else {
										resultadoValue.put(objAtr.getNombreAtributo(),
												FechaUtil.obtenerFechaFormatoCompleto((Date) value));
									}
								}
							} else {
								resultadoValue.put(objAtr.getNombreAtributo(), ARTIFICIO_CLASS);
								resultado.putAll(
										toVOMap(value, null, listaObjeto.get(objAtr.getNombreAtributo()), false));
							}
						}
					} catch (Exception e) {
						//
					}
				}
			}
			if (resultadoValue.size() > 0) {
				resultado.put(ressul.getClass().getName(), resultadoValue);
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toVOMap() al parsear " + ressul.getClass().getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyMap();
	}

	public static <T> Map<String, Map<String, Object>> toVOMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir, String claseNamePadre, String grupoList) {
		try {
			if (ressul == null) {
				return null;
			}
			if (listaAtributo == null) {
				listaAtributo = new ArrayList<>();
			}
			if (isExcluir) {
				if (!listaAtributo.contains("serialVersionUID")) {
					listaAtributo.add("serialVersionUID");
				}
			}
			if (listaObjeto == null) {
				listaObjeto = new HashMap<>();
			}
			Map<String, Map<String, Object>> resultado = new HashMap<>();
			Map<String, Object> resultadoValue = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(ressul.getClass());
			boolean soloInclurirAtributosEnviados = listaObjeto.containsKey(ressul.getClass().getName());
			List<String> listaAtributoEnviado = new ArrayList<>();
			if (soloInclurirAtributosEnviados) {
				listaAtributoEnviado = listaObjeto.get(ressul.getClass().getName());
			}
			Map<String, Integer> fieldHerenciaMap = fieldHerenciaMap(ressul);
			for (AtributoEntityVO objAtr : listaAtributos) {
				String nombreAtributo = objAtr.getNombreAtributo().toLowerCase();
				boolean isObtenerAtributo = false;
				if (isExcluir) {
					isObtenerAtributo = !listaAtributo.contains(nombreAtributo);
				} else {
					isObtenerAtributo = listaAtributo.contains(nombreAtributo);
				}
				if (isObtenerAtributo && soloInclurirAtributosEnviados) {
					isObtenerAtributo = listaAtributoEnviado.contains(nombreAtributo);
				}
				if (isObtenerAtributo) {
					try {
						// Field fValue =
						// ressul.getClass().getDeclaredField(objAtr.getNombreAtributo());
						/*
						 * if
						 * (objAtr.getNombreAtributo().toLowerCase().contains("riesgoSCTR".toLowerCase()
						 * )) { System.out.println("siiiiiiii"); }
						 */
						boolean isLista = false;
						Field fValue = fieldHerenciaSet(ressul, fieldHerenciaMap, objAtr);
						fValue.setAccessible(true);
						Object value = fValue.get(ressul);
						if (value != null) {
							isLista = value.getClass().isAssignableFrom(ArrayList.class);
							if (!listaObjeto.containsKey(nombreAtributo)) {
								String keyRecursive = claseNamePadre + "." + nombreAtributo;
								if (!listaObjeto.containsKey(keyRecursive)) {
									if (!StringUtil.isNullOrEmpty(value)) {
										if (!objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
											if (isLista) {
												int cantidad = 0;
												for (Object objList : (ArrayList) value) {
													cantidad++;
													resultadoValue.put(
															objAtr.getNombreAtributo() + "${List}" + cantidad, objList);
												}
											} else {
												resultadoValue.put(objAtr.getNombreAtributo(), value);
											}

										} else {
											resultadoValue.put(objAtr.getNombreAtributo(),
													FechaUtil.obtenerFechaFormatoCompleto((Date) value));
										}
									}
								} else {
									if (isLista) {
										int cantidad = 0;
										for (Object objList : (ArrayList) value) {
											cantidad++;
											resultadoValue.put(objAtr.getNombreAtributo() + "" + cantidad,
													objList.getClass().getName());
											resultado
													.putAll(toVOMap(objList, listaObjeto, listaObjeto.get(keyRecursive),
															false, nombreAtributo, "" + cantidad));
										}

									} else {
										resultadoValue.put(objAtr.getNombreAtributo(),
												objAtr.getClasssAtributoType().getName());
										resultado.putAll(toVOMap(value, listaObjeto, listaObjeto.get(keyRecursive),
												false, nombreAtributo, ""));
									}

								}

							} else {
								if (isLista) {
									int cantidad = 0;
									for (Object objList : (ArrayList) value) {
										cantidad++;
										resultadoValue.put(objAtr.getNombreAtributo(), objList.getClass().getName());
										resultado.putAll(toVOMap(objList, listaObjeto, listaObjeto.get(nombreAtributo),
												false, nombreAtributo, "" + cantidad));
									}
								} else {
									resultadoValue.put(objAtr.getNombreAtributo(),
											objAtr.getClasssAtributoType().getName());
									resultado.putAll(toVOMap(value, listaObjeto, listaObjeto.get(nombreAtributo), false,
											nombreAtributo, ""));
								}

							}
						}
					} catch (Exception e) {
						// log.error("error convertir " + objAtr.getNombreAtributo());
					}
				}
			}
			if (resultadoValue.size() > 0) {
				resultado.put(ressul.getClass().getName() + grupoList, resultadoValue);
			}
			return resultado;
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityAtributeMap(Object ressul,Class<T> entityClassEntity) al parsear "
							+ ressul.getClass().getName() + "  " + e.getMessage());
		}
		return null;
	}

	public static Map<String, Map<String, Object>> toFiltroMap(Object ressul, Map<String, List<String>> listaObjeto,
			List<String> listaAtributo, boolean isExcluir, String nombreClase, Map<String, String> fechaFormatoMap) {
		if (ressul == null) {
			return Collections.emptyMap();
		}
		try {

			if (listaAtributo == null) {
				listaAtributo = new ArrayList<>();
			}
			if (isExcluir && !listaAtributo.contains(SERIAL_VERSION_UID)) {
				listaAtributo.add(SERIAL_VERSION_UID);
			}
			if (listaObjeto == null) {
				listaObjeto = new HashMap<>();
			}
			Map<String, Map<String, Object>> resultado = new HashMap<>();
			Map<String, Object> resultadoValue = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			for (AtributoEntityVO objAtr : listaAtributos) {
				boolean isObtenerAtributo = false;
				if (isExcluir) {
					isObtenerAtributo = !listaAtributo.contains(objAtr.getNombreAtributo());
				} else {
					isObtenerAtributo = listaAtributo.contains(objAtr.getNombreAtributo());
				}
				if (isObtenerAtributo) {
					try {
						Field fValue = getField(ressul, objAtr.getNombreAtributo());
						Object value = fValue.get(ressul);
						StringBuilder nombreAtributoFinal = new StringBuilder();
						if (!StringUtil.isNullOrEmpty(nombreClase)) {
							nombreAtributoFinal.append(nombreClase);
							nombreAtributoFinal.append(".");
							nombreAtributoFinal.append(objAtr.getNombreAtributo());
						} else {
							nombreAtributoFinal.append(objAtr.getNombreAtributo());
						}
						if (value != null) {
							if (value instanceof Number) {
								BigDecimal numberTemp = new BigDecimal(value.toString());
								if (numberTemp.compareTo(BigDecimal.ZERO) <= 0) {
									value = null; // nulear valores por defecto 0
								}
							}
							if (!listaObjeto.containsKey(objAtr.getNombreAtributo())) {
								if (value != null) {
									if (!objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
										resultadoValue.put(nombreAtributoFinal.toString(), value);
									} else {
										resultadoValue.put(nombreAtributoFinal.toString(),
												FechaUtil.obtenerFechaFormatoPersonalizado((Date) value,
														fechaFormatoMap.get(nombreAtributoFinal.toString())));
									}
								} else {
									resultadoValue.put(nombreAtributoFinal.toString(), null);
								}
							} else {
								resultado.putAll(toFiltroMap(value, null, listaObjeto.get(objAtr.getNombreAtributo()),
										false, objAtr.getNombreAtributo(), fechaFormatoMap));
							}
						} else {
							if (!listaObjeto.containsKey(objAtr.getNombreAtributo())) {
								resultadoValue.put(nombreAtributoFinal.toString(), null);
							}
						}
					} catch (Exception e) {
						//
					}
				}
			}
			if (resultadoValue.size() > 0) {
				resultado.put(ressul.getClass().getName(), resultadoValue);
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toFiltroMap() al parsear " + ressul.getClass().getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyMap();
	}

	/**
	 * Transfer objeto vo atribute map.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @return the map
	 */
	public static Map<String, Object> toVOMap(Object ressul) {
		if (ressul == null) {
			return Collections.emptyMap();
		}
		try {
			Map<String, Object> resultado = new HashMap<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(ressul.getClass());
			for (AtributoEntityVO objAtr : listaAtributos) {
				try {
					Field fValue = getField(ressul, objAtr.getNombreAtributo());
					Object value = fValue.get(ressul);
					if (value != null) {
						if (!StringUtil.isNullOrEmpty(value)) {
							if (!objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
								resultado.put(objAtr.getNombreAtributo(), value);
							} else {
								resultado.put(objAtr.getNombreAtributo(),
										FechaUtil.obtenerFechaFormatoCompleto((Date) value));
							}
						}
					}
				} catch (Exception e) {
					//
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toVOMap() al parsear " + ressul.getClass().getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyMap();
	}

	/**
	 * Transfer objeto entity vo.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param scriptSqlResulJDBCVO
	 *            el script sql resul jdbcvo
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the t
	 */
	public static <T> T toEntityVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity) {
		try {
			List<Map<String, Object>> ressul = scriptSqlResulJDBCVO.getListaData();
			if (ressul == null || scriptSqlResulJDBCVO.isTieneError()) {
				return null;
			}
			Map<String, Object> valueMap = scriptSqlResulJDBCVO.getListaData().get(0);
			T resultado = entityClassEntity.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity);
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (scriptSqlResulJDBCVO.getListaHeader().contains(objAtr.getNombreAtributo())) {
					Field f = getField(resultado, objAtr.getNombreAtributo());
					Object value = obtenerValor(valueMap.get(objAtr.getNombreAtributo()) + "", objAtr, false);
					setField(f, resultado, value);
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntityVO() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	public static <T> T toRestVO(Map<String, Object> valueMap, Class<T> entityClassEntity) {
		return toRestVO(valueMap, null, entityClassEntity);
	}

	public static <T> T toRestVO(Map<String, Object> valueMap, Map<String, String> equivalenciaAtributeMap,
			Class<T> entityClassEntity) {
		try {
			T resultado = entityClassEntity.getDeclaredConstructor().newInstance();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity);
			for (AtributoEntityVO objAtr : listaAtributos) {
				String keyAtributo = objAtr.getNombreAtributo();
				if (equivalenciaAtributeMap != null && equivalenciaAtributeMap.containsKey(keyAtributo)) {
					keyAtributo = equivalenciaAtributeMap.get(keyAtributo);
				}
				if (valueMap.containsKey(keyAtributo)) {
					Field f = getField(resultado, objAtr.getNombreAtributo());
					Object value = obtenerValor(valueMap.get(keyAtributo), objAtr, false);
					setField(f, resultado, value);
				}
			}
			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toRestVO() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return null;
	}

	public static <T> List<T> toRestVOList(List<Map<String, Object>> listaValueMap, Class<T> entityClassEntity) {
		return toRestVOList(listaValueMap, null, entityClassEntity);
	}

	public static <T> List<T> toRestVOList(List<Map<String, Object>> listaValueMap,
			Map<String, String> equivalenciaAtributeMap, Class<T> entityClassEntity) {
		List<T> resultado = new ArrayList<>();
		if (listaValueMap == null) {
			return resultado;
		}
		for (Map<String, Object> ressul : listaValueMap) {
			T resultadoTemp = toRestVO(ressul, equivalenciaAtributeMap, entityClassEntity);
			resultado.add(resultadoTemp);
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity list vo.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param scriptSqlResulJDBCVO
	 *            el script sql resul jdbcvo
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the list
	 */
	public static <T> List<T> toEntityListVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity) {
		return toEntityListVO(scriptSqlResulJDBCVO, entityClassEntity, new HashMap<String, String>());
	}

	/**
	 * Transfer objeto entity list vo.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param scriptSqlResulJDBCVO
	 *            el script sql resul jdbcvo
	 * @param entityClassEntity
	 *            el entity class entity
	 * @param formatoFechaMap
	 *            el formato fecha map
	 * @return the list
	 */
	public static <T> List<T> toEntityListVO(ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity,
			Map<String, String> formatoFechaMap) {
		try {
			List<Map<String, Object>> ressul = scriptSqlResulJDBCVO.getListaData();
			if (ressul == null || scriptSqlResulJDBCVO.isTieneError()) {
				return Collections.emptyList();
			}
			List<T> resultado = new ArrayList<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity);
			for (Map<String, Object> valueMap : scriptSqlResulJDBCVO.getListaData()) {
				T resultadoTemp = entityClassEntity.getDeclaredConstructor().newInstance();
				for (AtributoEntityVO objAtr : listaAtributos) {
					if (scriptSqlResulJDBCVO.getListaHeader().contains(objAtr.getNombreAtributo())) {
						Object value = null;
						try {
							Field f = getField(resultadoTemp, objAtr.getNombreAtributo());
							value = obtenerValor(valueMap.get(objAtr.getNombreAtributo()) + "", objAtr, false,
									formatoFechaMap);
							setField(f, resultadoTemp, value);
						} catch (Exception e) {
							//
						}
					}
				}
				resultado.add(resultadoTemp);
			}

			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntityListVO() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyList();
	}

	public static <T> List<T> toEntityListVO(Map<String, String> campoMapping,
			ScriptSqlResulJDBCVO scriptSqlResulJDBCVO, Class<T> entityClassEntity,
			Map<String, String> formatoFechaMap) {
		try {
			List<Map<String, Object>> ressul = scriptSqlResulJDBCVO.getListaData();
			if (ressul == null || scriptSqlResulJDBCVO.isTieneError()) {
				return Collections.emptyList();
			}
			List<T> resultado = new ArrayList<>();
			List<AtributoEntityVO> listaAtributos = obtenerListaAtributos(entityClassEntity);
			for (Map<String, Object> valueMap : scriptSqlResulJDBCVO.getListaData()) {
				T resultadoTemp = entityClassEntity.getDeclaredConstructor().newInstance();
				for (AtributoEntityVO objAtr : listaAtributos) {
					String campoKeyBD = campoMapping.get(objAtr.getNombreAtributo()) + "";
					if (scriptSqlResulJDBCVO.getListaHeader().contains(campoKeyBD)) {
						Object value = null;
						try {
							Field f = getField(resultadoTemp, objAtr.getNombreAtributo());
							value = obtenerValor(valueMap.get(campoKeyBD) + "", objAtr, false, formatoFechaMap);
							setField(f, resultadoTemp, value);
						} catch (Exception e) {
							//
						}

					}
				}
				resultado.add(resultadoTemp);
			}

			return resultado;
		} catch (Exception e) {
			log.error("Error TransferDataUtil.toEntityListVO() al parsear " + entityClassEntity.getName() + "  "
					+ e.getMessage());
		}
		return Collections.emptyList();
	}

	/**
	 * Transfer objeto entity list.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param <E>
	 *            el tipo de elemento
	 * @param ressulList
	 *            el ressul list
	 * @param entityClassEntity
	 *            el entity class entity
	 * @return the list
	 */
	public static <T, E> List<T> toListEntity(List<E> ressulList, Class<T> entityClassEntity, boolean isFiltro,
			String... entityClasess) {
		List<T> resultado = new ArrayList<>();
		if (ressulList == null) {
			return resultado;
		}
		for (Object ressul : ressulList) {
			T resultadoTemp = to(ressul, entityClassEntity, isFiltro, entityClasess);
			resultado.add(resultadoTemp);
		}
		return resultado;
	}

	public static <T> T transferObjetoVOTrama(Map<String, Map<String, Object>> listaObjectValueMap,
			Class<T> entityClass, String grupoList) {
		try {
			Map<String, Object> ressul = listaObjectValueMap.get(entityClass.getName() + "" + grupoList);
			if (ressul == null) {
				return entityClass.newInstance();
			}
			T resultado = entityClass.newInstance();
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(entityClass);
			Map<String, Integer> fieldHerenciaResultadoMap = fieldHerenciaMap(resultado);
			for (AtributoEntityVO objAtr : listaAtributos) {
				if (ressul.containsKey(objAtr.getNombreAtributo())
						|| ressul.containsKey(objAtr.getNombreAtributo() + "${List}1")) {

					Field f = fieldHerenciaSet(resultado, fieldHerenciaResultadoMap, objAtr);
					f.setAccessible(true);
					Object value = null;
					value = obtenerValor(ressul.get(objAtr.getNombreAtributo()) + "", objAtr, true);
					if (value == null) {
						value = obtenerValor(ressul.get(objAtr.getNombreAtributo() + "${List}1") + "", objAtr, true);
					}
					if (value != null) {
						if (listaObjectValueMap.containsKey(value.toString())
								|| listaObjectValueMap.containsKey(value.toString() + "1")) {
							boolean isLista = objAtr.getClasssAtributoType().isAssignableFrom(ArrayList.class);
							if (isLista) {
								int cantidad = 1;
								String key = value + "" + cantidad;
								List<Object> arrayLis = new ArrayList<>();
								while (listaObjectValueMap.containsKey(key)) {
									Object valueList = transferObjetoVOTrama(listaObjectValueMap,
											AtributosEntityCacheUtil.getInstance().obtenerClass(value.toString()),
											cantidad + "");
									arrayLis.add(valueList);
									cantidad++;
									key = value.toString() + "" + cantidad;
								}
								try {
									if (value != null) {
										f.set(resultado, arrayLis);
									}
								} catch (Exception e) {
									log.error(
											"Error OBJETO TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
													+ entityClass.getName() + " campo " + objAtr.getNombreAtributo()
													+ "  " + e.getMessage());
								}
							} else {
								if (listaObjectValueMap.containsKey(objAtr.getClasssAtributoType().getName())) {
									value = transferObjetoVOTrama(listaObjectValueMap, objAtr.getClasssAtributoType(),
											"");
									try {
										if (value != null) {
											f.set(resultado, value);
										}
									} catch (Exception e) {
										log.error(
												"Error OBJETO TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
														+ entityClass.getName() + " campo " + objAtr.getNombreAtributo()
														+ "  " + e.getMessage());
									}
								}
							}

						} else {
							boolean isLista = objAtr.getClasssAtributoType().isAssignableFrom(ArrayList.class);
							if (isLista) {
								int cantidad = 1;
								String key = objAtr.getNombreAtributo() + "${List}" + "" + cantidad;
								List<Object> arrayLis = new ArrayList<>();
								while (ressul.containsKey(key)) {
									Object valueList = ressul.get(objAtr.getNombreAtributo() + "${List}" + cantidad);
									arrayLis.add(valueList);
									cantidad++;
									key = objAtr.getNombreAtributo() + "${List}" + "" + cantidad;
								}
								try {
									if (value != null) {
										f.set(resultado, arrayLis);
									}
								} catch (Exception e) {
									log.error(
											"Error OBJETO TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
													+ entityClass.getName() + " campo " + objAtr.getNombreAtributo()
													+ "  " + e.getMessage());
								}
							} else {
								try {
									if (value != null) {
										f.set(resultado, value);
									}
								} catch (Exception e) {
									log.error(
											"Error TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
													+ entityClass.getName() + " campo " + objAtr.getNombreAtributo()
													+ "  " + e.getMessage());
								}
							}

						}
					}
				}
			}
			listaObjectValueMap = null;
			return resultado;
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
							+ entityClass.getName() + "  " + e.getMessage());
		}
		return null;
	}

	/**
	 * Transfer objeto entity csv map dto.
	 *
	 * @param campoMappingCVSMap
	 *            el campo mapping cvs map
	 * @param dataList
	 *            el data list
	 * @param campoMappingCSVTypeMap
	 *            el campo mapping csv type map
	 * @param campoMappingFormatoMap
	 *            el campo mapping formato map
	 * @param parametroMap
	 *            el parametro map
	 * @return the list
	 */
	public static List<Map<String, ValueDataVO>> transferObjetoEntityCSVMapDTO(Map<String, Object> campoMappingCVSMap,
			BufferedReader br, Map<String, String> campoMappingCSVTypeMap, Map<String, String> campoMappingFormatoMap,
			Map<String, Object> parametroMap, Map<String, Character> configuracionTramaDetalleMap) {
		List<Map<String, ValueDataVO>> resultado = new ArrayList<Map<String, ValueDataVO>>();
		if (campoMappingFormatoMap == null) {
			campoMappingFormatoMap = new HashMap<String, String>();
		}
		Map<String, String> grupoMap = new HashMap<String, String>();
		try {
			String cvsSplitBy = (String) parametroMap.get("cvsSplitBy");
			int filaData = (Integer) parametroMap.get(FILA_DATA);
			Integer cantidadData = (Integer) parametroMap.get(CANTIDAD_DATA);
			int contador = 0;
			int contadorData = 0;
			String line = "";
			if (StringUtil.isNullOrEmpty(cvsSplitBy)) {
				cvsSplitBy = ",";
			}
			int filaDataProcesar = Integer
					.parseInt(parametroMap.get(ConstanteConfiguracionTramaUtil.FILA_DATA_ORIGINAL) + ""); // OBTIENE LA
																											// FILA DE
																											// LECTURA
																											// DEL
																											// ARCHIVO
																											// CONFIGURADO
			while ((line = br.readLine()) != null) {
				contador++;
				if (contador >= filaData) {
					contadorData++;
					String[] data = line.split(cvsSplitBy, -1);// -1 para leer
					boolean isValido = validarCSV(data, campoMappingCVSMap);
					if (isValido) {
						Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
						for (Map.Entry<String, Object> objAtr : campoMappingCVSMap.entrySet()) {
							ValueDataVO value = obtenerValueCSV(data, Integer.parseInt(objAtr.getValue() + ""),
									campoMappingCSVTypeMap.get(objAtr.getKey()),
									campoMappingFormatoMap.get(objAtr.getKey()), filaDataProcesar, parametroMap);
							resultadoTemp.put(objAtr.getKey(), value);
						}
						StringBuilder key = generarKeyAgrupador(resultadoTemp, configuracionTramaDetalleMap);
						// para agrupar
						if (StringUtil.isNullOrEmpty(key)) {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							} else {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						} else {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						}
					} else {
						break;
					}
				}
				if ((cantidadData != null) && contadorData == cantidadData.intValue()) {
					break;
				}
			}
			if (CollectionUtil.isEmpty(resultado)) {
				String[] data = new String[0];// campoMappingTXTMap.size() obtener maximo index
				Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
				for (Map.Entry<String, Object> objAtr : campoMappingCVSMap.entrySet()) {
					ValueDataVO value = obtenerValueCSV(data, Integer.parseInt(objAtr.getValue() + ""),
							campoMappingCSVTypeMap.get(objAtr.getKey()), campoMappingFormatoMap.get(objAtr.getKey()),
							filaDataProcesar, parametroMap);
					resultadoTemp.put(objAtr.getKey(), value);
				}
				resultado.add(resultadoTemp);
			}
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityCSVMapDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					log.error("Error ", e);
				}
			}
		}
		grupoMap = null;
		return resultado;
	}

	/**
	 * Transfer objeto entity trama.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param ressul
	 *            el ressul
	 * @param entityClass
	 *            el entity class
	 * @return the t
	 */
	public static <T> T transferObjetoEntityTrama(Map<String, ValueDataVO> ressul, Class<T> entityClass) {
		try {
			if (ressul == null) {
				return null;
			}
			T resultado = entityClass.newInstance();
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(entityClass.getName());

			for (AtributoEntityVO objAtr : listaAtributos) {
				if (ressul.containsKey(objAtr.getNombreColumna())) {
					Field f = resultado.getClass().getDeclaredField(objAtr.getNombreAtributo());
					f.setAccessible(true);
					Object value = null;
					if (objAtr.getClasssAtributoType().isAssignableFrom(Date.class)) {
						value = ((ValueDataVO) ressul.get(objAtr.getNombreColumna())).getData();
					} else {
						value = obtenerValor(ressul.get(objAtr.getNombreColumna()).getData() + "", objAtr, false);
					}
					try {
						if (value != null) {
							f.set(resultado, value);
						}
					} catch (Exception e) {
						//
					}
				}
			}
			ressul = null;
			return resultado;
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityTrama(Object ressul,Class<T> entityClass) al parsear "
							+ entityClass.getName() + "  " + e.getMessage());
		}
		return null;
	}

	/**
	 * Transfer objeto entity dto.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param campoMappingExcelMap
	 *            el campo mapping excel map
	 * @param cellDataList
	 *            el cell data list
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the t
	 */
	public static <T> List<T> transferObjetoEntityExcelDTO(Map<String, Integer> campoMappingExcelMap,
			HSSFWorkbook workBook, Class<T> entityClassDTO, int hoja, int filaData) {
		return transferObjetoEntityExcelDTO(campoMappingExcelMap, workBook, new HashMap<String, String>(),
				new HashMap<String, Object>(), entityClassDTO, hoja, filaData);
	}

	public static <T> List<T> transferObjetoEntityExcelDTO(Map<String, Integer> campoMappingExcelMap,
			HSSFWorkbook workBook, Map<String, String> campoMappingFormatoMap, Map<String, Object> parametroMap,
			Class<T> entityClassDTO, int hoja, int filaData) {
		List<T> resultado = new ArrayList<T>();
		try {
			if (workBook == null) {
				return resultado;
			}
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(entityClassDTO);
			Map<String, Object> resulObjectComplejo = obtenerAtributosComplejo(campoMappingExcelMap,
					campoMappingFormatoMap);
			Map<String, String> grupoSubObjectoMap = (Map<String, String>) resulObjectComplejo
					.get(GRUPO_SUB_OBJECTO_MAP);
			Map<String, Map<String, Integer>> campoMappingExcelSubObjectMap = (Map<String, Map<String, Integer>>) resulObjectComplejo
					.get(CAMPO_MAPPING_EXCEL_SUB_OBJECT_MAP);
			Map<String, Map<String, String>> campoMappingFormatoSubObjectMap = (Map<String, Map<String, String>>) resulObjectComplejo
					.get(CAMPO_MAPPING_FORMATO_SUB_OBJECT_MAP);
			HSSFSheet hssfSheet = workBook.getSheetAt(hoja - 1);
			Iterator rowIterator = (Iterator) hssfSheet.rowIterator();
			int contador = 0;
			while (rowIterator.hasNext()) {
				contador++;
				HSSFRow hssfRow = (HSSFRow) rowIterator.next();
				if (contador >= filaData) {
					T resultadoTemp = entityClassDTO.newInstance();
					for (AtributoEntityVO objAtr : listaAtributos) {
						if (campoMappingExcelMap.containsKey(objAtr.getNombreAtributo())) {
							Field f = resultadoTemp.getClass().getDeclaredField(objAtr.getNombreAtributo());
							f.setAccessible(true);
							Object value = obtenerValorXls(hssfRow,
									campoMappingExcelMap.get(objAtr.getNombreAtributo()), objAtr);
							if (value != null) {
								f.set(resultadoTemp, value);
							}
						}
					}
					for (Map.Entry<String, String> subObject : grupoSubObjectoMap.entrySet()) {
						Field f = resultadoTemp.getClass().getDeclaredField(subObject.getKey());
						if (f != null) {
							f.setAccessible(true);
							Object valueTransfer = f.get(resultadoTemp);
							if (valueTransfer == null) {
								valueTransfer = f.getType().newInstance();
							}
							Object value = transferObjetoEntityExcelObjectDTO(
									campoMappingExcelSubObjectMap.get(subObject.getKey()), hssfRow,
									campoMappingFormatoSubObjectMap.get(subObject.getKey()), parametroMap, f.getType(),
									hoja, filaData);
							if (value != null) {
								f.set(resultadoTemp, value);
							}
						}
					}
					resultado.add(resultadoTemp);
				}
			}
			if (workBook != null) {
				workBook.close();
			}
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityExelDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ entityClassDTO.getName() + "  " + e.getMessage());
		}
		return resultado;
	}

	private static Map<String, Object> obtenerAtributosComplejo(Map<String, Integer> campoMappingExcelMap,
			Map<String, String> campoMappingFormatoMap) {
		Map<String, Object> resultado = new HashMap<String, Object>();
		Map<String, String> grupoSubObjectoMap = new HashMap<String, String>();
		Map<String, Map<String, Integer>> campoMappingExcelSubObjectMap = new HashMap<String, Map<String, Integer>>();
		Map<String, Map<String, String>> campoMappingFormatoSubObjectMap = new HashMap<String, Map<String, String>>();
		for (String subKey : campoMappingExcelMap.keySet()) {
			if (subKey.contains(".")) {
				String subKeyProcesar = subKey.replace(".", ";");
				String[] subKeys = subKeyProcesar.split(";", -1);
				if (subKeys != null) {
					String subObjecto = subKeys[0];
					if (!grupoSubObjectoMap.containsKey(subObjecto)) {
						Map<String, Integer> campoMappingValue = new HashMap<String, Integer>();
						String abributoKey = subKey.substring(subObjecto.length() + 1);
						campoMappingValue.put(abributoKey, campoMappingExcelMap.get(subKey));// aqui se puede hacer
																								// recursivo
																								// subObject.obj.obj2.atributo
						Map<String, String> campoMappingFormatoValue = new HashMap<String, String>();
						campoMappingFormatoValue.put(abributoKey, campoMappingFormatoMap.get(subKey));
						grupoSubObjectoMap.put(subObjecto, subObjecto);
						campoMappingExcelSubObjectMap.put(subObjecto, campoMappingValue);
						campoMappingFormatoSubObjectMap.put(subObjecto, campoMappingFormatoValue);
					} else {
						Map<String, Integer> campoMappingValue = campoMappingExcelSubObjectMap.get(subObjecto);
						String abributoKey = subKey.substring(subObjecto.length() + 1);
						campoMappingValue.put(abributoKey, campoMappingExcelMap.get(subKey));
						Map<String, String> campoMappingFormatoValue = campoMappingFormatoSubObjectMap.get(subObjecto);
						campoMappingFormatoValue.put(abributoKey, campoMappingFormatoMap.get(subKey));
						grupoSubObjectoMap.put(subObjecto, subObjecto);
						campoMappingExcelSubObjectMap.put(subObjecto, campoMappingValue);
						campoMappingFormatoSubObjectMap.put(subObjecto, campoMappingFormatoValue);
					}
				}

			}
		}
		resultado.put(GRUPO_SUB_OBJECTO_MAP, grupoSubObjectoMap);
		resultado.put(CAMPO_MAPPING_EXCEL_SUB_OBJECT_MAP, campoMappingExcelSubObjectMap);
		resultado.put(CAMPO_MAPPING_FORMATO_SUB_OBJECT_MAP, campoMappingFormatoSubObjectMap);
		return resultado;
	}

	public static <T> T transferObjetoEntityExcelObjectDTO(Map<String, Integer> campoMappingExcelMap,
			HSSFRow hssfRowData, Map<String, String> campoMappingFormatoMap, Map<String, Object> parametroMap,
			Class<T> entityClassDTO, int hoja, int filaData) {
		T resultado = null;
		try {
			resultado = entityClassDTO.newInstance();
			if (hssfRowData == null) {
				return resultado;
			}
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(entityClassDTO);
			Map<String, String> grupoSubObjectoMap = new HashMap<String, String>();
			Map<String, Map<String, Integer>> campoMappingExcelSubObjectMap = new HashMap<String, Map<String, Integer>>();
			Map<String, Map<String, String>> campoMappingFormatoSubObjectMap = new HashMap<String, Map<String, String>>();
			Map<String, Object> resulObjectComplejo = obtenerAtributosComplejo(campoMappingExcelMap,
					campoMappingFormatoMap);
			grupoSubObjectoMap = (Map<String, String>) resulObjectComplejo.get(GRUPO_SUB_OBJECTO_MAP);
			campoMappingExcelSubObjectMap = (Map<String, Map<String, Integer>>) resulObjectComplejo
					.get(CAMPO_MAPPING_EXCEL_SUB_OBJECT_MAP);
			campoMappingFormatoSubObjectMap = (Map<String, Map<String, String>>) resulObjectComplejo
					.get(CAMPO_MAPPING_FORMATO_SUB_OBJECT_MAP);

			for (AtributoEntityVO objAtr : listaAtributos) {
				if (campoMappingExcelMap.containsKey(objAtr.getNombreAtributo())) {
					Field f = resultado.getClass().getDeclaredField(objAtr.getNombreAtributo());
					f.setAccessible(true);
					Object value = obtenerValorXls(hssfRowData, campoMappingExcelMap.get(objAtr.getNombreAtributo()),
							objAtr);
					if (value != null) {
						f.set(resultado, value);
					}
				}
			}
			for (Map.Entry<String, String> subObject : grupoSubObjectoMap.entrySet()) {
				Field f = resultado.getClass().getDeclaredField(subObject.getKey());
				if (f != null) {
					f.setAccessible(true);
					Object valueTransfer = f.get(resultado);
					if (valueTransfer == null) {
						valueTransfer = f.getType().newInstance();
					}
					Object value = transferObjetoEntityExcelObjectDTO(
							campoMappingExcelSubObjectMap.get(subObject.getKey()), hssfRowData,
							campoMappingFormatoSubObjectMap.get(subObject.getKey()), parametroMap, f.getType(), hoja,
							filaData);
					if (value != null) {
						f.set(resultado, value);
					}
				}
			}

		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityExelObjectDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ entityClassDTO.getName() + "  " + e.getMessage());
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity excel xlsx dto.
	 *
	 * @param <T>
	 *            el tipo generico
	 * @param campoMappingExcelMap
	 *            el campo mapping excel map
	 * @param cellDataList
	 *            el cell data list
	 * @param entityClassDTO
	 *            el entity class dto
	 * @return the list
	 */
	public static <T> List<T> transferObjetoEntityExcelXlsxDTO(Map<String, Integer> campoMappingExcelMap,
			XSSFWorkbook workBook, Class<T> entityClassDTO, int hoja, int filaData) {
		return transferObjetoEntityExcelXlsxDTO(campoMappingExcelMap, new HashMap<String, String>(),
				new HashMap<String, Object>(), workBook, entityClassDTO, hoja, filaData);
	}

	public static <T> List<T> transferObjetoEntityExcelXlsxDTO(Map<String, Integer> campoMappingExcelMap,
			Map<String, String> campoMappingFormatoMap, Map<String, Object> parametroMap, XSSFWorkbook workBook,
			Class<T> entityClassDTO, int hoja, int filaData) {
		List<T> resultado = new ArrayList<T>();
		try {
			if (workBook == null) {
				return resultado;
			}
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(entityClassDTO);
			Map<String, Object> resulObjectComplejo = obtenerAtributosComplejo(campoMappingExcelMap,
					campoMappingFormatoMap);
			Map<String, String> grupoSubObjectoMap = (Map<String, String>) resulObjectComplejo
					.get(GRUPO_SUB_OBJECTO_MAP);
			Map<String, Map<String, Integer>> campoMappingExcelSubObjectMap = (Map<String, Map<String, Integer>>) resulObjectComplejo
					.get(CAMPO_MAPPING_EXCEL_SUB_OBJECT_MAP);
			Map<String, Map<String, String>> campoMappingFormatoSubObjectMap = (Map<String, Map<String, String>>) resulObjectComplejo
					.get(CAMPO_MAPPING_FORMATO_SUB_OBJECT_MAP);
			XSSFSheet hssfSheet = workBook.getSheetAt(hoja - 1);
			Iterator<XSSFRow> rowIterator = (Iterator) hssfSheet.rowIterator();
			int contador = 0;
			while (rowIterator.hasNext()) {
				contador++;
				XSSFRow hssfRow = rowIterator.next();
				if (contador >= filaData) {
					T resultadoTemp = entityClassDTO.newInstance();
					for (AtributoEntityVO objAtr : listaAtributos) {
						if (campoMappingExcelMap.containsKey(objAtr.getNombreAtributo())) {
							Field f = resultadoTemp.getClass().getDeclaredField(objAtr.getNombreAtributo());
							f.setAccessible(true);
							Object value = obtenerValorXlsx(hssfRow,
									campoMappingFormatoMap.get(objAtr.getNombreAtributo()),
									campoMappingExcelMap.get(objAtr.getNombreAtributo()), objAtr);
							if (value != null) {
								f.set(resultadoTemp, value);
							}
						}
					}
					for (Map.Entry<String, String> subObject : grupoSubObjectoMap.entrySet()) {
						Field f = resultadoTemp.getClass().getDeclaredField(subObject.getKey());
						if (f != null) {
							f.setAccessible(true);
							Object valueTransfer = f.get(resultadoTemp);
							if (valueTransfer == null) {
								valueTransfer = f.getType().newInstance();
							}
							Object value = transferObjetoEntityExcelXlsxObjectDTO(
									campoMappingExcelSubObjectMap.get(subObject.getKey()),
									campoMappingFormatoSubObjectMap.get(subObject.getKey()), parametroMap, hssfRow,
									f.getType(), hoja, filaData);
							if (value != null) {
								f.set(resultadoTemp, value);
							}
						}
					}
					resultado.add(resultadoTemp);
				}

			}
			if (workBook != null) {
				workBook.close();
			}
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityExcelXlsxDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ entityClassDTO.getName() + "  " + e.getMessage());
		}
		return resultado;
	}

	public static <T> T transferObjetoEntityExcelXlsxObjectDTO(Map<String, Integer> campoMappingExcelMap,
			Map<String, String> campoMappingFormatoMap, Map<String, Object> parametroMap, XSSFRow hssfRow,
			Class<T> entityClassDTO, int hoja, int filaData) {
		T resultado = null;
		try {
			resultado = entityClassDTO.newInstance();
			if (hssfRow == null) {
				return resultado;
			}
			List<AtributoEntityVO> listaAtributos = AtributosEntityCacheUtil.getInstance()
					.obtenerListaAtributos(entityClassDTO);
			Map<String, String> grupoSubObjectoMap = new HashMap<String, String>();
			Map<String, Map<String, Integer>> campoMappingExcelSubObjectMap = new HashMap<String, Map<String, Integer>>();
			Map<String, Map<String, String>> campoMappingFormatoSubObjectMap = new HashMap<String, Map<String, String>>();
			Map<String, Object> resulObjectComplejo = obtenerAtributosComplejo(campoMappingExcelMap,
					campoMappingFormatoMap);
			grupoSubObjectoMap = (Map<String, String>) resulObjectComplejo.get(GRUPO_SUB_OBJECTO_MAP);
			campoMappingExcelSubObjectMap = (Map<String, Map<String, Integer>>) resulObjectComplejo
					.get(CAMPO_MAPPING_EXCEL_SUB_OBJECT_MAP);
			campoMappingFormatoSubObjectMap = (Map<String, Map<String, String>>) resulObjectComplejo
					.get(CAMPO_MAPPING_FORMATO_SUB_OBJECT_MAP);

			for (AtributoEntityVO objAtr : listaAtributos) {
				if (campoMappingExcelMap.containsKey(objAtr.getNombreAtributo())) {
					Field f = resultado.getClass().getDeclaredField(objAtr.getNombreAtributo());
					f.setAccessible(true);
					Object value = obtenerValorXlsx(hssfRow, campoMappingFormatoMap.get(objAtr.getNombreAtributo()),
							campoMappingExcelMap.get(objAtr.getNombreAtributo()), objAtr);
					if (value != null) {
						f.set(resultado, value);
					}
				}
			}
			for (Map.Entry<String, String> subObject : grupoSubObjectoMap.entrySet()) {
				Field f = resultado.getClass().getDeclaredField(subObject.getKey());
				if (f != null) {
					f.setAccessible(true);
					Object valueTransfer = f.get(resultado);
					if (valueTransfer == null) {
						valueTransfer = f.getType().newInstance();
					}
					Object value = transferObjetoEntityExcelXlsxObjectDTO(
							campoMappingExcelSubObjectMap.get(subObject.getKey()),
							campoMappingFormatoSubObjectMap.get(subObject.getKey()), parametroMap, hssfRow, f.getType(),
							hoja, filaData);
					if (value != null) {
						f.set(resultado, value);
					}
				}
			}
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityExcelXlsxObjectDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ entityClassDTO.getName() + "  " + e.getMessage());
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity map dto.
	 *
	 * @param campoMappingExcelMap
	 *            el campo mapping excel map
	 * @param dataList
	 *            el data list
	 * @param campoMappingExcelTypeMap
	 *            el campo mapping excel type map
	 * @param campoMappingFormatoMap
	 *            el campo mapping formato map
	 * @return the t
	 */
	public static List<Map<String, ValueDataVO>> transferObjetoEntityExcelMapDTO(
			Map<String, Object> campoMappingExcelMap, HSSFWorkbook workBook,
			Map<String, String> campoMappingExcelTypeMap, Map<String, String> campoMappingFormatoMap,
			Map<String, Object> parametroMap, Map<String, Character> configuracionTramaDetalleMap) {
		List<Map<String, ValueDataVO>> resultado = new ArrayList<Map<String, ValueDataVO>>();
		if (campoMappingFormatoMap == null) {
			campoMappingFormatoMap = new HashMap<String, String>();
		}
		Map<String, String> grupoMap = new HashMap<String, String>();
		try {
			int hoja = (Integer) parametroMap.get("hoja");
			int filaData = (Integer) parametroMap.get(FILA_DATA);
			Integer cantidadData = (Integer) parametroMap.get(CANTIDAD_DATA);
			HSSFSheet hssfSheet = workBook.getSheetAt(hoja - 1);
			Iterator rowIterator = (Iterator) hssfSheet.rowIterator();
			int contador = 0;
			int contadorData = 0;
			int filaDataProcesar = 0;
			while (rowIterator.hasNext()) {
				filaDataProcesar++;
				contador++;
				HSSFRow hssfRow = (HSSFRow) rowIterator.next();
				if (contador >= filaData) {
					contadorData++;
					boolean isValido = validarDataExel(hssfRow, campoMappingExcelMap);
					if (isValido) {
						Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
						for (Map.Entry<String, Object> objAtr : campoMappingExcelMap.entrySet()) {
							ValueDataVO value = obtenerValorXls(hssfRow, Integer.parseInt(objAtr.getValue() + ""),
									campoMappingExcelTypeMap.get(objAtr.getKey()),
									campoMappingFormatoMap.get(objAtr.getKey()), filaDataProcesar, parametroMap);
							resultadoTemp.put(objAtr.getKey(), value);
						}
						StringBuilder key = generarKeyAgrupador(resultadoTemp, configuracionTramaDetalleMap);
						// para agrupar
						if (StringUtil.isNullOrEmpty(key)) {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							} else {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						} else {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						}
					} else {
						if (parametroMap.containsKey("NoValidarExcel")) {
							isValido = true;
						} else {
							break;
						}

					}

				}
				if ((cantidadData != null) && contadorData == cantidadData.intValue()) {
					break;
				}
			}
			if (CollectionUtil.isEmpty(resultado)) {
				filaDataProcesar++;
				HSSFWorkbook workbook = new HSSFWorkbook();
				HSSFSheet sheet = workbook.createSheet("data");
				HSSFRow hssfRow = sheet.createRow(0);
				Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
				for (Map.Entry<String, Object> objAtr : campoMappingExcelMap.entrySet()) {
					ValueDataVO value = obtenerValorXls(hssfRow, Integer.parseInt(objAtr.getValue() + ""),
							campoMappingExcelTypeMap.get(objAtr.getKey()), campoMappingFormatoMap.get(objAtr.getKey()),
							filaDataProcesar, parametroMap);
					resultadoTemp.put(objAtr.getKey(), value);
				}
				resultado.add(resultadoTemp);
				workbook.close();
			}
			if (workBook != null) {
				workBook.close();
			}
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityExcelMapDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ e.getMessage());
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity excel xlsx map dto.
	 *
	 * @param campoMappingExcelMap
	 *            el campo mapping excel map
	 * @param dataList
	 *            el data list
	 * @param campoMappingExcelTypeMap
	 *            el campo mapping excel type map
	 * @param campoMappingFormatoMap
	 *            el campo mapping formato map
	 * @return the list
	 */
	public static List<Map<String, ValueDataVO>> transferObjetoEntityExcelXlsxMapDTO(
			Map<String, Object> campoMappingExcelMap, XSSFWorkbook workBook,
			Map<String, String> campoMappingExcelTypeMap, Map<String, String> campoMappingFormatoMap,
			Map<String, Object> parametroMap, Map<String, Character> configuracionTramaDetalleMap) {
		List<Map<String, ValueDataVO>> resultado = new ArrayList<Map<String, ValueDataVO>>();
		if (campoMappingFormatoMap == null) {
			campoMappingFormatoMap = new HashMap<String, String>();
		}
		Map<String, String> grupoMap = new HashMap<String, String>();
		try {
			int hoja = (Integer) parametroMap.get("hoja");
			int filaData = (Integer) parametroMap.get(FILA_DATA);
			Integer cantidadData = (Integer) parametroMap.get(CANTIDAD_DATA);
			XSSFSheet hssfSheet = workBook.getSheetAt(hoja - 1);
			Iterator<XSSFRow> rowIterator = (Iterator) hssfSheet.rowIterator();
			int contador = 0;
			int contadorData = 0;
			int filaDataProcesar = 0;
			while (rowIterator.hasNext()) {
				filaDataProcesar++;
				contador++;
				XSSFRow hssfRow = rowIterator.next();
				if (contador >= filaData) {
					contadorData++;
					boolean isValido = validarDataExel(hssfRow, campoMappingExcelMap);
					if (isValido) {
						Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
						for (Map.Entry<String, Object> objAtr : campoMappingExcelMap.entrySet()) {
							ValueDataVO value = obtenerValorXlsx(hssfRow, Integer.parseInt(objAtr.getValue() + ""),
									campoMappingExcelTypeMap.get(objAtr.getKey()),
									campoMappingFormatoMap.get(objAtr.getKey()), filaDataProcesar, parametroMap);
							resultadoTemp.put(objAtr.getKey(), value);
						}
						StringBuilder key = generarKeyAgrupador(resultadoTemp, configuracionTramaDetalleMap);
						// para agrupar
						if (StringUtil.isNullOrEmpty(key)) {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							} else {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						} else {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						}
					} else {
						break;
					}
				}
				if ((cantidadData != null) && contadorData == cantidadData.intValue()) {
					break;
				}
			}
			if (CollectionUtil.isEmpty(resultado)) {
				filaDataProcesar++;
				XSSFWorkbook workbook = new XSSFWorkbook();
				XSSFSheet sheet = (XSSFSheet) workbook.createSheet("data");
				XSSFRow hssfRow = (XSSFRow) sheet.createRow(0);
				Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
				for (Map.Entry<String, Object> objAtr : campoMappingExcelMap.entrySet()) {
					ValueDataVO value = obtenerValorXlsx(hssfRow, Integer.parseInt(objAtr.getValue() + ""),
							campoMappingExcelTypeMap.get(objAtr.getKey()), campoMappingFormatoMap.get(objAtr.getKey()),
							filaDataProcesar, parametroMap);
					resultadoTemp.put(objAtr.getKey(), value);
				}
				resultado.add(resultadoTemp);
				workbook.close();
			}
			if (workBook != null) {
				workBook.close();
			}
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityExcelXlsxMapDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ e.getMessage());
		}
		return resultado;
	}

	/**
	 * Transfer objeto entity txt map dto.
	 *
	 * @param campoMappingTXTMap
	 *            el campo mapping txt map
	 * @param dataList
	 *            el data list
	 * @param campoMappingTXTTypeMap
	 *            el campo mapping txt type map
	 * @param campoMappingFormatoMap
	 *            el campo mapping formato map
	 * @return the list
	 */
	public static List<Map<String, ValueDataVO>> transferObjetoEntityTXTMapDTO(Map<String, Object> campoMappingTXTMap,
			BufferedReader br, Map<String, String> campoMappingTXTTypeMap, Map<String, String> campoMappingFormatoMap,
			Map<String, Object> parametroMap, Map<String, Character> configuracionTramaDetalleMap) {
		List<Map<String, ValueDataVO>> resultado = new ArrayList<Map<String, ValueDataVO>>();
		if (campoMappingFormatoMap == null) {
			campoMappingFormatoMap = new HashMap<String, String>();
		}
		Map<String, List<Map<String, ValueDataVO>>> grupoMap = new HashMap<String, List<Map<String, ValueDataVO>>>();
		try {
			int filaData = (Integer) parametroMap.get(FILA_DATA);
			Integer cantidadData = (Integer) parametroMap.get(CANTIDAD_DATA);
			int contador = 0;
			int contadorData = 0;
			int filaDataProcesar = 0;
			String line = "";
			while ((line = br.readLine()) != null) {
				contador++;
				filaDataProcesar++;
				if (contador >= filaData) {
					contadorData++;
					String data = line;
					if (isConvertToCharsetUtf8()) {
						data = new String(line.getBytes(DEFAULT_CHARSET), DEFAULT_CHARSET);// incidencia encoding
					}
					boolean isValido = validarTXT(data, campoMappingTXTMap);
					if (isValido) {
						Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
						for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
							ValueDataVO value = obtenerValuePosicion(data, objAtr.getValue() + "",
									campoMappingTXTTypeMap.get(objAtr.getKey()),
									campoMappingFormatoMap.get(objAtr.getKey()), filaDataProcesar, parametroMap);
							resultadoTemp.put(objAtr.getKey(), value);
						}
						StringBuilder key = generarKeyAgrupador(resultadoTemp, configuracionTramaDetalleMap);
						// para agrupar
						if (StringUtil.isNullOrEmpty(key)) {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							} else {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						} else {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						}
					} else {
						break;
					}
				}
				if ((cantidadData != null) && contadorData == cantidadData.intValue()) {
					break;
				}
			}
			if (CollectionUtil.isEmpty(resultado)) {
				filaDataProcesar++;
				String data = "";
				Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
				for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
					ValueDataVO value = obtenerValuePosicion(data, objAtr.getValue() + "",
							campoMappingTXTTypeMap.get(objAtr.getKey()), campoMappingFormatoMap.get(objAtr.getKey()),
							filaDataProcesar, parametroMap);
					resultadoTemp.put(objAtr.getKey(), value);
				}
				resultado.add(resultadoTemp);
			}
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityTXTMapDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					log.error("Error ", e);
				}
			}
		}
		grupoMap = null;
		return resultado;
	}

	private static boolean isConvertToCharsetUtf8() {
		return ConfiguracionCacheUtil.getInstance().isElementoTrue("convert.to.charset.utf8");
	}

	/**
	 * Transfer objeto entity coordenada txt map dto.
	 *
	 * @param campoMappingTXTMap
	 *            el campo mapping txt map
	 * @param dataList
	 *            el data list
	 * @param campoMappingTXTTypeMap
	 *            el campo mapping txt type map
	 * @param campoMappingFormatoMap
	 *            el campo mapping formato map
	 * @param isCabecera
	 *            el is cabecera
	 * @return the list
	 */
	public static List<Map<String, ValueDataVO>> transferObjetoEntityCoordenadaTXTMapDTO(
			Map<String, Object> campoMappingTXTMap, BufferedReader br, Map<String, String> campoMappingTXTTypeMap,
			Map<String, String> campoMappingFormatoMap, boolean isCabecera, Map<String, Object> parametroMap,
			Map<String, Character> configuracionTramaDetalleMap) {
		List<Map<String, ValueDataVO>> resultado = new ArrayList<Map<String, ValueDataVO>>();
		if (campoMappingFormatoMap == null) {
			campoMappingFormatoMap = new HashMap<String, String>();
		}
		Map<String, String> grupoMap = new HashMap<String, String>();
		try {
			int filaData = (Integer) parametroMap.get(FILA_DATA);
			Integer cantidadData = (Integer) parametroMap.get(CANTIDAD_DATA);
			String delimitadorData = (String) parametroMap.get("delimitadorData");
			if (isCabecera) {
				int contador = 0;
				int contadorData = 0;
				String line = "";
				List<String> dataList = new ArrayList<String>();
				while ((line = br.readLine()) != null) {
					contador++;
					if (contador >= filaData) {
						contadorData++;
						String data = line;
						if (isConvertToCharsetUtf8()) {
							data = new String(line.getBytes(DEFAULT_CHARSET), DEFAULT_CHARSET);// incidencia encoding
						}
						dataList.add(data);
					}
					if ((isCabecera && cantidadData != null) && cantidadData.compareTo(contadorData) == 0) {
						break;
					}
				}
				if (CollectionUtil.isEmpty(dataList)) {
					dataList.add("");
				}
				Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
				for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
					ValueDataVO value = obtenerCoordenadaValor(dataList, null,
							campoMappingTXTMap.get(objAtr.getKey()) + "", campoMappingTXTTypeMap.get(objAtr.getKey()),
							campoMappingFormatoMap.get(objAtr.getKey()), isCabecera, parametroMap);
					resultadoTemp.put(objAtr.getKey(), value);
				}
				resultado.add(resultadoTemp);
			} else {
				int contador = 0;
				String line = "";
				while ((line = br.readLine()) != null) {
					contador++;
					if (contador >= filaData) {
						if (!isCabecera && line.contains(delimitadorData)) {
							break;
						}
						String data = line;
						if (isConvertToCharsetUtf8()) {
							data = new String(line.getBytes(DEFAULT_CHARSET), DEFAULT_CHARSET);// Incidencia Encoding
						}
						boolean isValido = validarTXTCoordenada(data, campoMappingTXTMap);
						if (isValido) {
							Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
							for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
								ValueDataVO value = obtenerCoordenadaValor(null, data, objAtr.getValue() + "",
										campoMappingTXTTypeMap.get(objAtr.getKey()),
										campoMappingFormatoMap.get(objAtr.getKey()), isCabecera, parametroMap);
								resultadoTemp.put(objAtr.getKey(), value);
							}
							StringBuilder key = generarKeyAgrupador(resultadoTemp, configuracionTramaDetalleMap);
							// para agrupar
							if (StringUtil.isNullOrEmpty(key)) {
								if (!grupoMap.containsKey(key.toString())) {
									resultado.add(resultadoTemp);
									grupoMap.put(key.toString(), null);
								} else {
									resultado.add(resultadoTemp);
									grupoMap.put(key.toString(), null);
								}
							} else {
								if (!grupoMap.containsKey(key.toString())) {
									resultado.add(resultadoTemp);
									grupoMap.put(key.toString(), null);
								}
							}
						} else {
							break;
						}
					}
				}
				if (CollectionUtil.isEmpty(resultado)) {
					Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
					for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
						ValueDataVO value = obtenerCoordenadaValor(null, "", objAtr.getValue() + "",
								campoMappingTXTTypeMap.get(objAtr.getKey()),
								campoMappingFormatoMap.get(objAtr.getKey()), isCabecera, parametroMap);
						resultadoTemp.put(objAtr.getKey(), value);
					}
					resultado.add(resultadoTemp);
				}
			}

		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityCoordenadaTXTMapDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					log.error("Error ", e);
				}
			}
		}
		grupoMap = null;
		return resultado;
	}

	/**
	 * Transfer objeto entity txt separador map dto.
	 *
	 * @param campoMappingTXTMap
	 *            el campo mapping txt map
	 * @param dataList
	 *            el data list
	 * @param campoMappingTxtTypeMap
	 *            el campo mapping txt type map
	 * @param campoMappingFormatoMap
	 *            el campo mapping formato map
	 * @param parametroMap
	 *            el parametro map
	 * @return the list
	 */
	public static List<Map<String, ValueDataVO>> transferObjetoEntityTXTSeparadorMapDTO(
			Map<String, Object> campoMappingTXTMap, BufferedReader br, Map<String, String> campoMappingTxtTypeMap,
			Map<String, String> campoMappingFormatoMap, Map<String, Object> parametroMap,
			Map<String, Character> configuracionTramaDetalleMap) {
		List<Map<String, ValueDataVO>> resultado = new ArrayList<Map<String, ValueDataVO>>();
		if (campoMappingFormatoMap == null) {
			campoMappingFormatoMap = new HashMap<String, String>();
		}
		Map<String, String> grupoMap = new HashMap<String, String>();
		try {
			String txtSplitBy = (String) parametroMap.get("txtSplitBy");
			int filaData = (Integer) parametroMap.get(FILA_DATA);
			Integer cantidadData = (Integer) parametroMap.get(CANTIDAD_DATA);
			int contador = 0;
			int contadorData = 0;
			String line = "";
			if (StringUtil.isNullOrEmpty(txtSplitBy)) {
				txtSplitBy = "\t";// tabuladores
			}
			int filaDataProcesar = 0;
			while ((line = br.readLine()) != null) {
				filaDataProcesar++;
				contador++;
				if (contador >= filaData) {
					contadorData++;
					if (isConvertToCharsetUtf8()) {
						line = new String(line.getBytes(DEFAULT_CHARSET), DEFAULT_CHARSET);// incidencia encoding
					}
					String[] data = line.split(txtSplitBy, -1);// -1 para leer
					boolean isValido = validarCSV(data, campoMappingTXTMap);
					if (isValido) {
						Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
						for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
							ValueDataVO value = obtenerValueCSV(data, Integer.parseInt(objAtr.getValue() + ""),
									campoMappingTxtTypeMap.get(objAtr.getKey()),
									campoMappingFormatoMap.get(objAtr.getKey()), filaDataProcesar, parametroMap);
							resultadoTemp.put(objAtr.getKey(), value);
						}
						StringBuilder key = generarKeyAgrupador(resultadoTemp, configuracionTramaDetalleMap);
						// para agrupar
						if (StringUtil.isNullOrEmpty(key)) {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							} else {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						} else {
							if (!grupoMap.containsKey(key.toString())) {
								resultado.add(resultadoTemp);
								grupoMap.put(key.toString(), null);
							}
						}
					} else {
						break;
					}
				}
				if ((cantidadData != null) && contadorData == cantidadData.intValue()) {
					break;
				}
			}
			if (CollectionUtil.isEmpty(resultado)) {
				filaDataProcesar++;
				String[] data = new String[0];// campoMappingTXTMap.size() obtener maximo index
				Map<String, ValueDataVO> resultadoTemp = new HashMap<String, ValueDataVO>();
				for (Map.Entry<String, Object> objAtr : campoMappingTXTMap.entrySet()) {
					ValueDataVO value = obtenerValueCSV(data, Integer.parseInt(objAtr.getValue() + ""),
							campoMappingTxtTypeMap.get(objAtr.getKey()), campoMappingFormatoMap.get(objAtr.getKey()),
							filaDataProcesar, parametroMap);
					resultadoTemp.put(objAtr.getKey(), value);
				}
				resultado.add(resultadoTemp);
			}
		} catch (Exception e) {
			log.error(
					"Error TransferDataObjectUtil.transferObjetoEntityTXTSeparadorMapDTO(Object ressul,Class<T> entityClassDTO) al parsear "
							+ e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					log.error("Error ", e);
				}
			}
		}
		grupoMap = null;
		return resultado;
	}

}
