package pe.com.mapfre.serviciotrama.domain.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ConfiguracionTramaDataVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.ConfiguracionTramaIdProcesarVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.GrupoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.RespuestaLecturaAgrupadoVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.RespuestaLecturaArchivoVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.ResultadoProcesoConfiguracionTramaVO;
import pe.com.mapfre.serviciotrama.domain.entity.vo.ValidarNomenclaturaVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfigUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfiguracionTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteTypeUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ObjectUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ResourceUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.RespuestaNaturalType;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.TransferDataUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CatalogoCacheErrorUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CollectionUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ftp.FTPClienteUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jasper.ArchivoUtilidades;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms.UUIDUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionFtpTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProceso;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.EquivalenciaDato;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.JuegoTramaCorreo;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.LogConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ProcesoFlujoError;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ProcesoVariableInstancia;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrLoteMpePK;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrTramaMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmctrTramaMpePK;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmeErrorMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmeErrorMpePK;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TramaNomenclaturaArchivoDetalle;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.CampoTablaDisponibleAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ConfiguracionTramaAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoAutomaticoDetalleHistDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoDetalleAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoJuegoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.LogConfiguracionTramaAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ProcesoFlujoErrorAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ProcesoVariableInstanciaDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.TasmctrLoteMpeAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.TasmctrTramaMpeAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.TasmeErrorMpeAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.ConfiguracionCacheUtil;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.EjecutarActividadParaleloInstance;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.FTPVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.RespuestaFTPVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.state.EstadoProcesamientoConfiguracionTramaState;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.CampoFijoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.ComponenteValidadoLogTramaType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.EstadoDetalleProcesoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.NivelErrorType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoArchivoProcesarType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoCampoType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoDestinatarioFTPType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoErrorLogTramaType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoErrorOrquestadorType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoHomologacionType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoParametroType;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.TipoVariableType;

/**
 * La Class LectorTramaUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */

public class LectorTramaUtil {

	protected boolean esPrueba = false;

	/** La log. */
	private final Logger log = Logger.getLogger(this.getClass());

	/** La solicitud reporte dao impl. */
	@EJB
	protected ConfiguracionTramaAutomaticoDaoLocal configuracionTramaDao;

	@EJB
	protected TasmctrLoteMpeAutomaticoDaoLocal tasmctrlMpeDao;

	@EJB
	protected CampoTablaDisponibleAutomaticoDaoLocal campoTablaDisponibleDao;

	@EJB
	protected TasmeErrorMpeAutomaticoDaoLocal tasmeErrorMpeDao;

	@EJB
	protected TasmctrTramaMpeAutomaticoDaoLocal tasmctrTramaMpeAutomaticoDaoLocal;

	@EJB
	protected LogConfiguracionTramaAutomaticoDaoLocal logConfiguracionTramaDao;
	
	@EJB
	protected transient ControlProcesoAutomaticoDaoLocal controlProcesoDao;
	
	@EJB
	protected ControlProcesoDetalleAutomaticoDaoLocal controlProcesoDetalleAutomaticoDaoLocal;
	
	@EJB
	protected ControlProcesoAutomaticoDetalleHistDaoLocal controlProcesoAutomaticoDetalleHistDaoLocal;
	
	@EJB
	protected ProcesoFlujoErrorAutomaticoDaoLocal procesoFlujoErrorAutomaticoDaoLocal;
	
	@EJB
	protected ProcesoVariableInstanciaDaoLocal procesoVariableInstanciaDaoLocal;
	
	@EJB
	protected ControlProcesoJuegoDaoLocal controlProcesoJuegoDaoLocal;

	private static final String ERROR_TECNICO = "TEC";

	/**
	 * Obtener session hibernate.
	 */
	protected void obtenerServiceContext() {
		/*
		 * if (configuracionTramaAutomaticoDaoImpl == null) {
		 * configuracionTramaAutomaticoDaoImpl =
		 * Referencia.getReference(ConfiguracionTramaAutomaticoDaoLocal.class); } if
		 * (tasmctrlMpeAutomaticoDaoLocal == null) { tasmctrlMpeAutomaticoDaoLocal =
		 * Referencia.getReference(TasmctrLoteMpeAutomaticoDaoLocal.class); } if
		 * (campoTablaDisponibleAutomaticoDaoLocal == null) {
		 * campoTablaDisponibleAutomaticoDaoLocal =
		 * Referencia.getReference(CampoTablaDisponibleAutomaticoDaoLocal.class); } if
		 * (tasmeErrorMpeAutomaticoDaoLocal == null) { tasmeErrorMpeAutomaticoDaoLocal =
		 * Referencia.getReference(TasmeErrorMpeAutomaticoDaoLocal.class); } if
		 * (tasmctrTramaMpeAutomaticoDaoLocal == null) {
		 * tasmctrTramaMpeAutomaticoDaoLocal =
		 * Referencia.getReference(TasmctrTramaMpeAutomaticoDaoLocal.class); } if
		 * (logConfiguracionTramaAutomaticoDaoLocal == null) {
		 * logConfiguracionTramaAutomaticoDaoLocal =
		 * Referencia.getReference(LogConfiguracionTramaAutomaticoDaoLocal.class); } if
		 * (controlProcesoAutomaticoDaoLocal == null) { controlProcesoAutomaticoDaoLocal
		 * = Referencia.getReference(ControlProcesoAutomaticoDaoLocal.class); } if
		 * (controlProcesoDetalleAutomaticoDaoLocal == null) {
		 * controlProcesoDetalleAutomaticoDaoLocal =
		 * Referencia.getReference(ControlProcesoDetalleAutomaticoDaoLocal.class); } if
		 * (controlProcesoAutomaticoDetalleHistDaoLocal == null) {
		 * controlProcesoAutomaticoDetalleHistDaoLocal =
		 * Referencia.getReference(ControlProcesoAutomaticoDetalleHistDaoLocal.class); }
		 * if (procesoFlujoErrorAutomaticoDaoLocal == null) {
		 * procesoFlujoErrorAutomaticoDaoLocal =
		 * Referencia.getReference(ProcesoFlujoErrorAutomaticoDaoLocal.class); } if
		 * (procesoVariableInstanciaDaoLocal == null) { procesoVariableInstanciaDaoLocal
		 * = Referencia.getReference(ProcesoVariableInstanciaDaoLocal.class); } if
		 * (controlProcesoJuegoDaoLocal == null) { controlProcesoJuegoDaoLocal =
		 * Referencia.getReference(ControlProcesoJuegoDaoLocal.class); }
		 */

		/*
		 * controlProcesoAutomaticoDaoLocal = (ControlProcesoAutomaticoDaoLocal) ctx
		 * .getBean(ControlProcesoAutomaticoDaoLocal.class);
		 */
		/*
		 * try { InitialContext context = new InitialContext(); Object vva =
		 * context.lookup(ControlProcesoAutomaticoDaoLocal.class.getName());
		 * System.out.println(vva); } catch (Exception e) { e.printStackTrace(); }
		 * 
		 * try { InitialContext context = new InitialContext(); Object vva =
		 * context.lookup("ControlProcesoAutomaticoDaoLocal"); System.out.println(vva);
		 * } catch (Exception e) { e.printStackTrace(); }
		 */

	}

	/**
	 * Obtener lista procesamiento mixto.
	 *
	 * @return the map
	 */
	protected Map<String, String> obtenerListaProcesamientoMixto() {
		Map<String, String> resultado = new HashMap<>();
		resultado.put(ConstanteConfiguracionTramaUtil.TASMEEROR_MPE, ConstanteConfiguracionTramaUtil.TASMEEROR_MPE);
		resultado.put(ConstanteConfiguracionTramaUtil.TASMCTRLOTE_MPE, ConstanteConfiguracionTramaUtil.TASMCTRLOTE_MPE);
		resultado.put(ConstanteConfiguracionTramaUtil.TASMCTRTRAMA_MPE,
				ConstanteConfiguracionTramaUtil.TASMCTRTRAMA_MPE);
		return resultado;
	}

	protected Map<String, String> obtenerListaCamposActualizarEDGP() {
		Map<String, String> resultado = new HashMap<>();
		resultado.put("FEC_LOTE", "FEC_LOTE");
		resultado.put("NUM_LOTE", "NUM_LOTE");
		resultado.put("COD_CIA", "COD_CIA");
		resultado.put("NUM_POLIZA_GRUPO", "NUM_POLIZA_GRUPO");
		resultado.put("NUM_UNICO_ITEM", "NUM_UNICO_ITEM");
		return resultado;
	}

	protected String getDescription(Locale locale, String value) {
		return ResourceUtil.getString(locale, ConstanteTypeUtil.BUNDLE_NAME_CONFIGURADOR_TRAMA_TYPE, value);
	}

	protected Long obtenerCantidadRegistroGrupo(GrupoConfiguracionTramaVO configuracionTramaCnfVO) throws Exception {
		Long resultado = 0L;
		for (GrupoConfiguracionTramaVO cnfTrama : configuracionTramaCnfVO.getListaConfiguracionTrama()) {
			if (ConstanteConfiguracionTramaUtil.TABLA_INTERMEDIA_EDGP.equalsIgnoreCase(cnfTrama.getNombreTabla())) {
				resultado = resultado + cnfTrama.getListaConfiguracionTramaData().getListaKey().size();
			}
		}
		return resultado;
	}

	protected Long obtenerCantidadErrorRegistroGrupo(
			BigMemoryManager<String, TasmeErrorMpe> listaLongConfiguracionTramaData) throws Exception {
		Long resultado = 0L;
		if (listaLongConfiguracionTramaData != null) {
			listaLongConfiguracionTramaData.reiniciar();
			Map<String, String> polizaMap = new HashMap<>();
			for (String errorKey : listaLongConfiguracionTramaData.getListaKey()) {
				TasmeErrorMpe error = listaLongConfiguracionTramaData.get(errorKey);
				if (!StringUtil.isNullOrEmpty(error.getNumeroPolizaTron())) {
					if (!polizaMap.containsKey(error.getNumeroPolizaTron())) {
						resultado = resultado + 1;
						polizaMap.put(error.getNumeroPolizaTron(), error.getNumeroPolizaTron());
					}
				}
			}
		}
		return resultado;
	}

	protected boolean verificarGrupoCompleto(Map<String, EstadoProcesamientoConfiguracionTramaState> verificadorGrupo)
			throws Exception {
		boolean resultado = true;
		for (Map.Entry<String, EstadoProcesamientoConfiguracionTramaState> objMap : verificadorGrupo.entrySet()) {
			if (EstadoProcesamientoConfiguracionTramaState.NO_PROCESADO.equals(objMap.getValue())) {
				resultado = false;
				break;
			}
		}
		return resultado;
	}

	protected RespuestaLecturaArchivoVO verificarGrupoCompletoObligatoridad(
			Map<String, RespuestaNaturalType> verificadorGrupoObligatoridad,
			Map<String, EstadoProcesamientoConfiguracionTramaState> verificadorGrupo, String nomenclatura,
			Map<String, Object> parametrosMap) throws Exception {
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		String rutaRelativaTemp = parametrosMap.get("rutaRelativaTemp") + "";
		resultado.setListaLogConfiguracionTramaAyuda(new BigMemoryManager<String, LogConfiguracionTrama>(
				rutaRelativaTemp, "verificarGrupoCompleto_data_log_ayuda"));
		for (Map.Entry<String, RespuestaNaturalType> objMap : verificadorGrupoObligatoridad.entrySet()) {
			if (RespuestaNaturalType.SI.equals(objMap.getValue())) {
				EstadoProcesamientoConfiguracionTramaState estadoProcesamientoConfiguracionTramaState = verificadorGrupo
						.get(objMap.getKey());
				if (!EstadoProcesamientoConfiguracionTramaState.PROCESADO_CON_EXITO
						.equals(estadoProcesamientoConfiguracionTramaState)) {
					// log.info("TRAM_LECT_0002 --> Error : Archivo Obligatorio");
					LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "",
							"Archivo Obligatorio", obtenerDescripcionError("Archivo obligatorio"), nomenclatura,
							Long.parseLong(objMap.getKey()), ComponenteValidadoLogTramaType.CONF_TRAMA,
							TipoErrorLogTramaType.GENERAL);
					resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
					resultado.setEsError(true);
				}
			}
		}
		return resultado;
	}

	protected String obtenerDescripcionError(String error) {
		String resultado = error;
		int cantidadMaxima = 198;
		if (error != null) {
			if (error.length() > cantidadMaxima) {
				resultado = error.substring(0, cantidadMaxima);
			}
		}
		if (resultado == null || resultado.equals("")) {
			resultado = "Error";
		}
		return resultado;
	}

	protected Map<String, Map<String, EstadoProcesamientoConfiguracionTramaState>> obtenerListaGrupoConfiguracionTramaValidarMap(
			List<ConfiguracionTrama> listaConfiguracionTrama) throws Exception {
		Map<String, Map<String, EstadoProcesamientoConfiguracionTramaState>> resultado = new HashMap<String, Map<String, EstadoProcesamientoConfiguracionTramaState>>();
		for (ConfiguracionTrama obj : listaConfiguracionTrama) {
			String keyGrupo = obj.getJuegoTrama().getIdJuegoTrama() + "";
			if (!resultado.containsKey(keyGrupo)) {
				Map<String, EstadoProcesamientoConfiguracionTramaState> value = new HashMap<String, EstadoProcesamientoConfiguracionTramaState>();
				value.put(obj.getIdConfiguradorTrama() + "", EstadoProcesamientoConfiguracionTramaState.NO_PROCESADO);
				resultado.put(keyGrupo, value);
			} else {
				Map<String, EstadoProcesamientoConfiguracionTramaState> value = resultado.get(keyGrupo);
				value.put(obj.getIdConfiguradorTrama() + "", EstadoProcesamientoConfiguracionTramaState.NO_PROCESADO);
				resultado.put(keyGrupo, value);
			}
		}
		return resultado;
	}

	protected Map<String, Map<String, String>> obtenerListaDependenciaDataMap(
			List<ConfiguracionTrama> listaConfiguracionTrama) throws Exception {
		Map<String, Map<String, String>> resultado = new HashMap<String, Map<String, String>>();
		for (ConfiguracionTrama obj : listaConfiguracionTrama) {
			String key = obj.getIdConfiguradorTrama() + "";
			for (ConfiguracionTramaDetalle objDet : obj.getConfiguracionTramaConfiguracionTramaDetalleList()) {
				ConfiguracionTrama configuracionTramaAsociada = getConfiguracionTramaAsociada(objDet);
				if (objDet.getCampoAsociado() != null && configuracionTramaAsociada != null) {
					if (!resultado.containsKey(key)) {
						Map<String, String> value = new HashMap<>();
						value.put(configuracionTramaAsociada.getIdConfiguradorTrama() + "",
								configuracionTramaAsociada.getIdConfiguradorTrama() + "");
						resultado.put(key, value);
					} else {
						Map<String, String> value = resultado.get(key);
						value.put(configuracionTramaAsociada.getIdConfiguradorTrama() + "",
								configuracionTramaAsociada.getIdConfiguradorTrama() + "");
						resultado.put(key, value);
					}
				}
			}

		}
		return resultado;
	}

	private ConfiguracionTrama getConfiguracionTramaAsociada(ConfiguracionTramaDetalle objDet) {
		if (objDet.getCampoAsociado() != null && objDet.getCampoAsociado().getConfiguracionTrama() != null) {
			return objDet.getCampoAsociado().getConfiguracionTrama();
		}
		return null;
	}

	protected Map<String, String> obtenerListaDependenciaTablaMixtaDataMap(
			List<ConfiguracionTrama> listaConfiguracionTrama) throws Exception {
		Map<String, String> resultado = new HashMap<>();
		for (ConfiguracionTrama obj : listaConfiguracionTrama) {
			String keyGrupo = obj.getJuegoTrama().getIdJuegoTrama() + "";
			String nombreTabla = obj.getNombreTabla();
			nombreTabla = nombreTabla.replace(".", ";");
			String[] schemaTableName = nombreTabla.split(";");
			if (obtenerListaProcesamientoMixto().containsKey(schemaTableName[1].toUpperCase())) {
				if (ConstanteConfiguracionTramaUtil.TASMCTRTRAMA_MPE.equals(schemaTableName[1].toUpperCase())) {
					String keyTablaMixta = keyGrupo + "" + schemaTableName[1].toUpperCase()
							+ obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo() + "";
					if (!resultado.containsKey(keyTablaMixta)) {
						resultado.put(keyTablaMixta, obj.getIdConfiguradorTrama() + "");
					}
				} else {
					String keyTablaMixta = keyGrupo + "" + schemaTableName[1].toUpperCase();
					if (!resultado.containsKey(keyTablaMixta)) {
						resultado.put(keyTablaMixta, obj.getIdConfiguradorTrama() + "");
					}
				}
			}
		}
		return resultado;
	}

	protected Map<String, Map<String, RespuestaNaturalType>> obtenerListaGrupoConfiguracionTramaValidarObligatoridadMap(
			List<ConfiguracionTrama> listaConfiguracionTrama) throws Exception {
		Map<String, Map<String, RespuestaNaturalType>> resultado = new HashMap<String, Map<String, RespuestaNaturalType>>();
		for (ConfiguracionTrama obj : listaConfiguracionTrama) {
			String keyGrupo = obj.getJuegoTrama().getIdJuegoTrama() + "";
			if (!resultado.containsKey(keyGrupo)) {
				Map<String, RespuestaNaturalType> value = new HashMap<String, RespuestaNaturalType>();
				if (RespuestaNaturalType.SI.getKey().equals(obj.getObligatorio())) {
					value.put(obj.getIdConfiguradorTrama() + "", RespuestaNaturalType.SI);
				} else {
					value.put(obj.getIdConfiguradorTrama() + "", RespuestaNaturalType.NO);
				}

				resultado.put(keyGrupo, value);
			} else {
				Map<String, RespuestaNaturalType> value = resultado.get(keyGrupo);
				if (RespuestaNaturalType.SI.getKey().equals(obj.getObligatorio())) {
					value.put(obj.getIdConfiguradorTrama() + "", RespuestaNaturalType.SI);
				} else {
					value.put(obj.getIdConfiguradorTrama() + "", RespuestaNaturalType.NO);
				}
				resultado.put(keyGrupo, value);
			}
		}
		return resultado;
	}

	protected TasmctrLoteMpe obtenerTasmeCtrLote(String keyGrupo, Map<String, String> listaDepenciaTablaMixtaDataMap,
			Map<String, RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoTemporalMap,
			Map<String, Object> parametroMap, ConfiguracionTrama configuracionTrama) throws Exception {
		TasmctrLoteMpe resultadoTasmctrlMpeTemp = new TasmctrLoteMpe();
		resultadoTasmctrlMpeTemp.setFechaHoraInicioCargaTrama(FechaUtil.obtenerFechaActual());
		resultadoTasmctrlMpeTemp.setNroRegistroLeidos(BigDecimal.ZERO);
		resultadoTasmctrlMpeTemp.setNroRegistroErrorDeNivel2(BigDecimal.ZERO);
		String keyTablaMixtaLote = keyGrupo + ConstanteConfiguracionTramaUtil.TASMCTRLOTE_MPE.toUpperCase();
		String keyTramaLote = listaDepenciaTablaMixtaDataMap.get(keyTablaMixtaLote);
		resultadoTasmctrlMpeTemp = obtenerTasmctrLoteMpe(listaRespuestaLecturaArchivoTemporalMap.get(keyTramaLote),
				resultadoTasmctrlMpeTemp, configuracionTrama.getJuegoTrama().getProducto());
		resultadoTasmctrlMpeTemp
				.setIdControlProceso(parametroMap.get(ConstanteConfiguracionTramaUtil.ID_CONTROL_PROCESO) + ""); // TODO:aumentar
																													// el
																													// campo
																													// -
																													// SE
																													// AGREGO
																													// EL
																													// NUEVO
																													// CAMPO
		resultadoTasmctrlMpeTemp.setIdJuegoTrama(configuracionTrama.getJuegoTrama().getIdJuegoTrama());
		return resultadoTasmctrlMpeTemp;
	}

	protected TasmctrLoteMpe obtenerTasmctrLoteMpe(RespuestaLecturaArchivoVO listaRespuestaLecturaArchivoTemporalMap,
			TasmctrLoteMpe resultadoTasmctrlMpeTemp, String producto) throws Exception {
		List<Map<String, ValueDataVO>> listaDataResulMap = new ArrayList<Map<String, ValueDataVO>>();
		if (listaRespuestaLecturaArchivoTemporalMap != null) {
			listaDataResulMap = listaRespuestaLecturaArchivoTemporalMap.getListaDataResulMap();
		}
		TasmctrLoteMpe resultadoTasmctrlMpe = new TasmctrLoteMpe();
		TasmctrLoteMpePK tasmctrlMpePK = new TasmctrLoteMpePK();
		if (listaDataResulMap != null && listaDataResulMap.size() > 0) {
			resultadoTasmctrlMpe = TransferDataUtil.transferObjetoEntityTrama(listaDataResulMap.get(0),
					TasmctrLoteMpe.class);
			tasmctrlMpePK = TransferDataUtil.transferObjetoEntityTrama(listaDataResulMap.get(0),
					TasmctrLoteMpePK.class);
		}
		resultadoTasmctrlMpe.setTasmctrLoteMpePK(tasmctrlMpePK);

		if (StringUtil.isNullOrEmpty(resultadoTasmctrlMpe.getMcaEmisDirectaMapfre())) {
			resultadoTasmctrlMpe.setMcaEmisDirectaMapfre(RespuestaNaturalType.NO.getKey() + "");// OK
		}
		if (StringUtil.isNullOrEmpty(resultadoTasmctrlMpe.getCodigoTratamiento())) {
			resultadoTasmctrlMpe.setCodigoTratamiento(producto);
		}
		if (StringUtil.isNullOrEmpty(resultadoTasmctrlMpe.getFechaHoraInicioCargaTrama())) {
			resultadoTasmctrlMpe.setFechaHoraInicioCargaTrama(resultadoTasmctrlMpeTemp.getFechaHoraInicioCargaTrama());
		}
		resultadoTasmctrlMpe.setIdControlProceso(resultadoTasmctrlMpeTemp.getIdControlProceso());
		resultadoTasmctrlMpe.setIdJuegoTrama(resultadoTasmctrlMpeTemp.getIdJuegoTrama());
		resultadoTasmctrlMpe.setNroRegistroLeidos(resultadoTasmctrlMpeTemp.getNroRegistroLeidos());

		return resultadoTasmctrlMpe;
	}

	protected Map<String, TasmctrTramaMpe> obtenerTasmctrTrama(String keyGrupo, String keyTramaTrama,
			String keyNomenclatura, ConfiguracionTrama configuracionTrama,
			Map<String, RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoTemporalMap,
			Map<String, TasmctrLoteMpe> tasmctrLoteMpeMap, Map<String, String> nombreNomenclaturaArchivoTramaMap)
			throws Exception {
		TasmctrTramaMpe resultadoTasmctrTramaMpeTemp = new TasmctrTramaMpe();
		resultadoTasmctrTramaMpeTemp.setFechaHoraInicioCarga(FechaUtil.obtenerFechaActual());
		resultadoTasmctrTramaMpeTemp.setNroRegistrosLeidos(BigDecimal.ZERO);
		if (isCampoNegocio(configuracionTrama)) {
			resultadoTasmctrTramaMpeTemp.setIdConfiguradorTrama(configuracionTrama.getIdConfiguradorTrama());
		}
		resultadoTasmctrTramaMpeTemp = obtenerTasmctrTramaMpe(
				listaRespuestaLecturaArchivoTemporalMap.get(keyTramaTrama), tasmctrLoteMpeMap.get(keyGrupo),
				resultadoTasmctrTramaMpeTemp, nombreNomenclaturaArchivoTramaMap.get(keyNomenclatura),
				configuracionTrama.getTramaNomenclaturaArchivo().getAcronimoTipoTrama());
		Map<String, TasmctrTramaMpe> tasmctrTramaMpeMap = new HashMap<String, TasmctrTramaMpe>();
		tasmctrTramaMpeMap.put(keyNomenclatura, resultadoTasmctrTramaMpeTemp);
		return tasmctrTramaMpeMap;
	}

	protected TasmctrTramaMpe obtenerTasmctrTramaMpe(RespuestaLecturaArchivoVO listaRespuestaLecturaArchivoTemporalMap,
			TasmctrLoteMpe tasmctrlMpeTemp, TasmctrTramaMpe resultadoTasmctrTramaMpeTemp,
			String nombreNomenclaturaArchivo, String acronimo) throws Exception {
		List<Map<String, ValueDataVO>> listaDataResulMap = new ArrayList<Map<String, ValueDataVO>>();
		if (listaRespuestaLecturaArchivoTemporalMap != null) {
			listaDataResulMap = listaRespuestaLecturaArchivoTemporalMap.getListaDataResulMap();
		}
		TasmctrTramaMpe resultadoTasmctrTramaMpe = new TasmctrTramaMpe();
		TasmctrTramaMpePK tasmctrlMpePK = new TasmctrTramaMpePK();

		if (listaDataResulMap != null && listaDataResulMap.size() > 0) {
			resultadoTasmctrTramaMpe = TransferDataUtil.transferObjetoEntityTrama(listaDataResulMap.get(0),
					TasmctrTramaMpe.class);
			tasmctrlMpePK = TransferDataUtil.transferObjetoEntityTrama(listaDataResulMap.get(0),
					TasmctrTramaMpePK.class);
		}

		resultadoTasmctrTramaMpe.setTasmctrTramaMpePK(tasmctrlMpePK);
		if (StringUtil.isNullOrEmpty(resultadoTasmctrTramaMpe.getTasmctrTramaMpePK().getFechaLote())) {
			resultadoTasmctrTramaMpe.getTasmctrTramaMpePK()
					.setFechaLote(tasmctrlMpeTemp.getTasmctrLoteMpePK().getFechaLote());
		}
		if (StringUtil.isNullOrEmpty(resultadoTasmctrTramaMpe.getTasmctrTramaMpePK().getNumeroLote())) {
			resultadoTasmctrTramaMpe.getTasmctrTramaMpePK()
					.setNumeroLote(tasmctrlMpeTemp.getTasmctrLoteMpePK().getNumeroLote());
		}
		if (StringUtil.isNullOrEmpty(resultadoTasmctrTramaMpe.getTasmctrTramaMpePK().getMcaEmisDirectaMapfre())) {
			resultadoTasmctrTramaMpe.getTasmctrTramaMpePK()
					.setMcaEmisDirectaMapfre(RespuestaNaturalType.NO.getKey() + "");// OK
		}
		resultadoTasmctrTramaMpe.getTasmctrTramaMpePK().setTipoTrama(acronimo);
		resultadoTasmctrTramaMpe.setNombreTramaInput(nombreNomenclaturaArchivo);
		if (StringUtil.isNullOrEmpty(resultadoTasmctrTramaMpe.getFechaHoraInicioCarga())) {
			resultadoTasmctrTramaMpe.setFechaHoraInicioCarga(resultadoTasmctrTramaMpeTemp.getFechaHoraInicioCarga());
		}
		resultadoTasmctrTramaMpe.setNroRegistrosLeidos(resultadoTasmctrTramaMpeTemp.getNroRegistrosLeidos());
		resultadoTasmctrTramaMpe.setIdConfiguradorTrama(resultadoTasmctrTramaMpeTemp.getIdConfiguradorTrama());
		return resultadoTasmctrTramaMpe;
	}

	protected Integer obtenerCantidadProcesamientoByCoordenada(ConfiguracionTrama configuracionTrama) throws Exception {
		Integer resultado = 0;
		int maxResultado = 0;
		if (!StringUtil.isNullOrEmptyNumeric(
				configuracionTrama.getConfiguracionTramaConfiguracionTramaDetalleList().get(0).getFilaData())) {
			maxResultado = configuracionTrama.getConfiguracionTramaConfiguracionTramaDetalleList().get(0).getFilaData()
					.intValue();
		}
		for (ConfiguracionTramaDetalle objDet : configuracionTrama
				.getConfiguracionTramaConfiguracionTramaDetalleList()) {
			if (!StringUtil.isNullOrEmptyNumeric(objDet.getFilaData())) {
				int valorMaximo = objDet.getFilaData().intValue();
				if (maxResultado < valorMaximo) {
					maxResultado = valorMaximo;
				}
			}
		}
		resultado = maxResultado;
		return resultado;
	}

	protected boolean verificarProcesamientoByCoordenada(ConfiguracionTrama configuracionTrama) {
		boolean resultado = RespuestaNaturalType.SI.getKey().equals(configuracionTrama.getEsCoordenada());
		return resultado;
	}

	protected Object obtenerValorDefautProcesamientoError(ConfiguracionTramaDetalle configuracionTramaDetalle) {
		Object resultado = null;
		try {
			TipoCampoType tipoCampoType = obtenerTipoCampo(configuracionTramaDetalle);
			switch (tipoCampoType) {
			case FECHA:
				Date fechaAntigua = FechaUtil.obtenerFechaFormatoPersonalizado("01/01/1900", "dd/MM/yyyy");
				resultado = fechaAntigua;
				break;
			case NUMERICO:
				BigDecimal numero = BigDecimal.ONE;
				resultado = numero;
				break;
			case TEXTO:
				StringBuilder cadena = new StringBuilder();
				int longitud = 0;
				if (StringUtil.isNullOrEmptyNumeric(configuracionTramaDetalle.getLongitud())) {
					longitud = 1;
				} else {
					longitud = configuracionTramaDetalle.getLongitud().intValue();
				}
				for (int i = 0; i < longitud; i++) {
					cadena.append("9");
				}
				resultado = cadena.toString();
				break;
			default:
				break;
			}
		} catch (Exception e) {
			resultado = null;
		}

		return resultado;
	}

	protected Object obtenerValorDefautProcesamientoErrorTamanho(Object data, Integer longitudUsuario) {
		Object resultado = null;
		try {
			String cadena = data + "";
			int longitud = 0;
			if (StringUtil.isNullOrEmptyNumeric(longitudUsuario)) {
				longitud = 1;
			} else {
				longitud = longitudUsuario;
			}
			cadena = cadena.substring(0, longitud);
			resultado = cadena;
		} catch (Exception e) {
			resultado = null;
		}

		return resultado;
	}

	protected ValueDataVO obtenerValorCampoAsociado(List<Map<String, ValueDataVO>> listaDataResulMap,
			String campoTablaAsociado) throws Exception {
		ValueDataVO resultado = null;
		for (Map<String, ValueDataVO> map : listaDataResulMap) {
			if (map.containsKey(campoTablaAsociado)) {
				resultado = map.get(campoTablaAsociado);
				break;
			}
		}
		return resultado;
	}

	/**
	 * Obtener valor campo sumatoria.
	 *
	 * @param index
	 *            el index
	 * @param listaDataResulMap
	 *            el lista data resul map
	 * @param campoTablaAsociado
	 *            el campo tabla asociado
	 * @return the object
	 * @throws Exception
	 *             the exception
	 */
	protected ValueDataVO obtenerValorCampoCalculado(List<Map<String, ValueDataVO>> listaDataResulMap,
			String campoTablaAsociado, CampoFijoType campoFijoType) throws Exception {
		ValueDataVO resultado = new ValueDataVO();
		BigDecimal sumatoria = BigDecimal.ZERO;
		BigDecimal contador = BigDecimal.ZERO;
		for (Map<String, ValueDataVO> map : listaDataResulMap) {
			if (map.containsKey(campoTablaAsociado)) {
				ValueDataVO resultadoTemp = map.get(campoTablaAsociado);
				// contador
				if (!StringUtil.isNullOrEmpty(resultadoTemp)) {
					if (!StringUtil.isNullOrEmpty(resultadoTemp.getData())) {
						contador = contador.add(BigDecimal.ONE);
					}
				}
				// sumatoria
				if (!StringUtil.isNullOrEmptyNumeric(resultadoTemp)) {
					if (!StringUtil.isNullOrEmptyNumeric(resultadoTemp.getData())) {
						sumatoria = sumatoria.add(new BigDecimal(resultadoTemp.getData().toString()));
					}
				}
			}
		}

		switch (campoFijoType) {
		case CAMPO_SUMA:
			resultado.setData(sumatoria);
			break;
		case CAMPO_CONTADOR:
			resultado.setData(contador);
			break;
		case CAMPO_PROMEDIO:
			if (!StringUtil.isNullOrEmptyNumeric(contador)) {
				resultado.setData(sumatoria.divide(contador, 2, RoundingMode.HALF_UP));
			} else {
				resultado.setData(BigDecimal.ZERO);
			}
			break;
		default:
			break;
		}
		return resultado;
	}

	protected ValueDataVO obtenerValorCampoAsociadoJoin(List<Map<String, ValueDataVO>> listaDataResulAsociadoMap,
			Map<String, ValueDataVO> actualAsociarMap, ConfiguracionTramaDetalle obj) throws Exception {
		ValueDataVO resultado = new ValueDataVO();
		String campoJoinInicio = obtenerCampoPersistenteAsociado(obj.getCampoAsociadoMatchInicio());
		String campoJoinFin = obtenerCampoPersistenteAsociado(obj.getCampoAsociadoMatchFin());
		String campoAsociado = obtenerCampoPersistenteAsociado(obj.getCampoAsociado());
		boolean encontro = false;
		if (actualAsociarMap.containsKey(campoJoinFin)) {
			if (!StringUtil.isNullOrEmpty(actualAsociarMap.get(campoJoinFin))) {
				String dataFin = actualAsociarMap.get(campoJoinFin) + "";
				for (Map<String, ValueDataVO> mapAsociarInicio : listaDataResulAsociadoMap) {
					if (mapAsociarInicio.containsKey(campoJoinInicio)) {
						String dataInicio = mapAsociarInicio.get(campoJoinInicio) + "";
						if (dataFin.equalsIgnoreCase(dataInicio)) {
							resultado = mapAsociarInicio.get(campoAsociado);
							encontro = true;
							break;
						}
						if (encontro) {
							break;
						}
					}
					if (encontro) {
						break;
					}
				}
			}
		}
		return resultado;

	}

	protected String obtenerCampoPersistenteAsociado(ConfiguracionTramaDetalle obj) throws Exception {
		String resultado = obj.getNombeCampoTabla();
		boolean isCampoPersistente = !RespuestaNaturalType.NO.getKey().equals(obj.getEsPersistente());
		if (!isCampoPersistente) {
			resultado = obj.getNombreCampo();
		}
		return resultado;

	}

	protected TasmeErrorMpe generarLogError(String fila, String desripcionError,
			Map<String, ValueDataVO> dataResulMapTemp,
			Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap,
			Map<String, String> configuracionTramaErrorMap, Date fechaLote, String numeroLote, Long idConfiguradorTrama,
			String nombreCampo, Map<String, Object> valErrorMap) {
		TasmeErrorMpe resultado = new TasmeErrorMpe();
		try {
			if (configuracionTramaErrorMap != null) {
				Map<String, ValueDataVO> dataResulMap = new HashMap<String, ValueDataVO>();
				for (Map.Entry<String, String> objConfMap : configuracionTramaErrorMap.entrySet()) {
					if (dataResulMapTemp.containsKey(objConfMap.getValue())) {
						boolean existeErrorValor = dataResulMapTemp.get(objConfMap.getValue()) != null
								&& dataResulMapTemp.get(objConfMap.getValue()).toString().contains("${ERROR}");
						if (!existeErrorValor && dataResulMapTemp.get(objConfMap.getValue()) != null) {
							dataResulMap.put(objConfMap.getKey(), dataResulMapTemp.get(objConfMap.getValue()));
						} else {
							if (configuracionTramaDetalleMap.containsKey(objConfMap.getValue())) {
								ConfiguracionTramaDetalle objDet = configuracionTramaDetalleMap
										.get(objConfMap.getValue());
								Object dataDefault = obtenerValorDefautProcesamientoError(objDet);
								dataResulMap.put(objConfMap.getKey(), new ValueDataVO(dataDefault));
							}
						}
					}
				}
				if (dataResulMap != null) {
					resultado = TransferDataUtil.transferObjetoEntityTrama(dataResulMap, TasmeErrorMpe.class);
				}
				TasmeErrorMpePK tasmeErrorMpePKDTO = new TasmeErrorMpePK();
				if (dataResulMap != null) {
					tasmeErrorMpePKDTO = TransferDataUtil.transferObjetoEntityTrama(dataResulMap,
							TasmeErrorMpePK.class);
				}
				tasmeErrorMpePKDTO.setFechaLote(fechaLote);
				tasmeErrorMpePKDTO.setNumeroLote(numeroLote);
				resultado.setTasmeErrorMpePK(tasmeErrorMpePKDTO);
				resultado.setIdConfiguradorTrama(idConfiguradorTrama);
				resultado.setNombreCampo(nombreCampo);
				resultado.setNivelError(NivelErrorType.LECTURA_TRAMA.getKey());
				resultado.setTextoError(desripcionError);
				if (!StringUtil.isNullOrEmptyNumeriCero(fila)) {
					resultado.setFila(Long.valueOf(fila));
				}
				String keyCatalogo = StringUtil.generarKey(valErrorMap, ConstanteConfiguracionTramaUtil.CAMPO_NAME,
						ConstanteConfiguracionTramaUtil.TABLA_NAME, ConstanteConfiguracionTramaUtil.TIPO_ERROR);
				resultado.setCodigoError(CatalogoCacheErrorUtil.getInstance().getCataloErrorMap().get(keyCatalogo)); // codigoError
																														// del
																														// catalogo
																														// de
																														// error
				resultado.setTipoError(
						ObjectUtil.objectToString(valErrorMap.get(ConstanteConfiguracionTramaUtil.TIPO_ERROR_TRAMA))); // tipo
																														// del
																														// error
																														// en
																														// tasmeerr
																														// interno
																														// o
																														// externo
				Long numHoja = ObjectUtil.objectToLong(
						valErrorMap.get(ConstanteConfiguracionTramaUtil.NUMERO_HOJA + "_" + idConfiguradorTrama));
				if (numHoja > 0) {
					resultado.setNumeroHoja(numHoja);
				} else {
					resultado.setNumeroHoja(null);
				}
				if (resultado.getTasmeErrorMpePK().getNumeroRiesgo() == null) {
					resultado.getTasmeErrorMpePK().setNumeroRiesgo(BigDecimal.ZERO);
				}
				if (StringUtil.isNullOrEmpty(resultado.getTasmeErrorMpePK().getFechaLote())
						|| StringUtil.isNullOrEmpty(resultado.getTasmeErrorMpePK().getNumeroLote())
						|| StringUtil.isNullOrEmpty(resultado.getTasmeErrorMpePK().getNumeroLote())
						|| StringUtil.isNullOrEmpty(resultado.getTasmeErrorMpePK().getNumeroUnicoItem())
						|| StringUtil.isNullOrEmpty(resultado.getTasmeErrorMpePK().getCodigoCia())
						|| StringUtil.isNullOrEmpty(resultado.getTasmeErrorMpePK().getNumeroRiesgo())) {
					resultado = null;
					// log.error("Error : procesando data " + desripcionError);
				}
			} else {
				resultado = null;
			}
		} catch (Exception e) {
			log.error("Error ", e);
			resultado = null;
		}

		return resultado;
	}

	protected Map<String, Object> obtenerCampoMappingPosicionMap(ConfiguracionTrama configuracionTrama)
			throws Exception {
		Map<String, Object> resultado = new HashMap<>();
		TipoArchivoProcesarType tipoArchivoProcesarType = TipoArchivoProcesarType
				.get(configuracionTrama.getTramaNomenclaturaArchivo().getTipoArchivo());
		switch (tipoArchivoProcesarType) {
		case EXCEL_XLS:
		case EXCEL_XLSX:
		case CSV:
			for (ConfiguracionTramaDetalle objDet : configuracionTrama
					.getConfiguracionTramaConfiguracionTramaDetalleList()) {
				String nombeCampoTabla = obtenerCampoPersistenteAsociado(objDet);
				if (!RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
					resultado.put(nombeCampoTabla, objDet.getPosicionCampoInicial() - 1);// - 1 para
																							// el
																							// usuario
																							// empieza
																							// en 1 para
																							// java en 0
				}
				if (RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
					resultado.put(nombeCampoTabla, -1L);
				}
			}
			break;

		case TXT:
			boolean isByCoordenada = verificarProcesamientoByCoordenada(configuracionTrama);
			if (!isByCoordenada) {
				if (RespuestaNaturalType.SI.getKey().equals(configuracionTrama.getTieneSeparador())) {
					for (ConfiguracionTramaDetalle objDet : configuracionTrama
							.getConfiguracionTramaConfiguracionTramaDetalleList()) {
						String nombeCampoTabla = obtenerCampoPersistenteAsociado(objDet);
						if (!RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
							resultado.put(nombeCampoTabla, objDet.getPosicionCampoInicial() - 1);// -
																									// 1
																									// para
																									// el
																									// usuario
																									// empieza
																									// en
																									// 1
																									// para
																									// java
																									// en
																									// 0
						}
						if (RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
							resultado.put(nombeCampoTabla, -1L);
						}
					}
				} else {
					for (ConfiguracionTramaDetalle objDet : configuracionTrama
							.getConfiguracionTramaConfiguracionTramaDetalleList()) {
						String nombeCampoTabla = obtenerCampoPersistenteAsociado(objDet);
						if (!RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
							String posicionTexto = "" + (objDet.getPosicionCampoInicial() - 1) + ";"
									+ (objDet.getPosicionCampoFinal());// Mejora Lectura Posicion
																		// Final
							resultado.put(nombeCampoTabla, posicionTexto);
						}
						if (RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
							resultado.put(nombeCampoTabla, -1L);
						}
					}
				}
			} else {
				for (ConfiguracionTramaDetalle objDet : configuracionTrama
						.getConfiguracionTramaConfiguracionTramaDetalleList()) {
					String nombeCampoTabla = obtenerCampoPersistenteAsociado(objDet);
					if (!RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
						String posicionTexto = (objDet.getFilaData() - 1) + ";" + (objDet.getPosicionCampoInicial() - 1)
								+ ";" + (objDet.getPosicionCampoFinal());// Mejora Lectura Posicion Final
						resultado.put(nombeCampoTabla, posicionTexto);
					}
					if (RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama())) {
						resultado.put(nombeCampoTabla, -1L);
					}
				}
			}
			break;

		default:
			break;
		}
		return resultado;
	}

	protected TipoCampoType obtenerTipoCampo(ConfiguracionTramaDetalle configuracionTramaDetalle) throws Exception {
		TipoCampoType tipoCampoType = TipoCampoType.get(configuracionTramaDetalle.getTipoCampo());

		CampoFijoType campoFijoType = CampoFijoType.get(configuracionTramaDetalle.getCampoFijo());
		if (campoFijoType != null) {
			switch (campoFijoType) {
			case CAMPO_CONTADOR:
			case CAMPO_SUMA:
			case CAMPO_PROMEDIO:
				tipoCampoType = TipoCampoType.NUMERICO;
				return tipoCampoType;
			default:
				break;
			}
		}

		if (configuracionTramaDetalle.getCampoAsociado() != null) {
			tipoCampoType = TipoCampoType.get(configuracionTramaDetalle.getCampoAsociado().getTipoCampo());
		}
		if (tipoCampoType == null && configuracionTramaDetalle.getCampoFijo() != null) {
			campoFijoType = CampoFijoType.get(configuracionTramaDetalle.getCampoFijo());

			switch (campoFijoType) {
			case NUMERO_RIESGO:
			case NUMERO_SECUENCIAL:
				tipoCampoType = TipoCampoType.NUMERICO;
				break;

			case NUMERO_LOTE:
				tipoCampoType = TipoCampoType.TEXTO;
				break;

			case FECHA_LOTE:
				tipoCampoType = TipoCampoType.FECHA;
				break;

			default:
				break;
			}
		}
		if (tipoCampoType == null) {
			tipoCampoType = TipoCampoType.TEXTO;
		}
		return tipoCampoType;
	}

	protected Long obtenerLongitud(ConfiguracionTramaDetalle configuracionTramaDetalle) throws Exception {
		Long longitud = configuracionTramaDetalle.getLongitud();
		if (configuracionTramaDetalle.getCampoAsociado() != null) {
			longitud = configuracionTramaDetalle.getCampoAsociado().getLongitud();
		}
		return longitud;
	}

	protected Map<String, String> obtenerCampoMappingTypeMap(ConfiguracionTrama configuracionTrama) throws Exception {
		Map<String, String> resultado = new HashMap<>();
		for (ConfiguracionTramaDetalle objDet : configuracionTrama
				.getConfiguracionTramaConfiguracionTramaDetalleList()) {
			String nombeCampoTabla = obtenerCampoPersistenteAsociado(objDet);
			TipoCampoType tipoCampoType = obtenerTipoCampo(objDet);
			switch (tipoCampoType) {
			case FECHA:
				resultado.put(nombeCampoTabla, "java.util.Date");
				break;
			case NUMERICO:
				resultado.put(nombeCampoTabla, "java.math.BigDecimal");
				break;
			case TEXTO:
				resultado.put(nombeCampoTabla, "java.lang.String");
				break;
			default:
				break;
			}

		}
		return resultado;
	}

	protected String obtenerNomenclatura(ConfiguracionTrama configuracionTrama, Map<String, Object> parametrosMap,
			boolean isManual, boolean esSimulacion) throws Exception {
		StringBuilder resultado = new StringBuilder();
		for (TramaNomenclaturaArchivoDetalle obj : configuracionTrama.getTramaNomenclaturaArchivo()
				.getTramaNomenclaturaArchivoTramaNomenclaturaArchivoDetalleList()) {
			boolean esDate = false;
			if (TipoVariableType.FECHA.getKey().equals(obj.getTipo())) {
				esDate = true;
			}
			if (esDate) {
				try {
					boolean isParametro = true;
					if (!isManual) {
						if (!esSimulacion) {
							resultado.append("@@${PARAMETRO}:" + obj.getTipo() + ":" + obj.getNumeroDigito() + ":"
									+ obj.getItemValor() + "@@");
						} else {
							resultado.append(FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
									obj.getItemValor()));
						}
					} else {
						if (parametrosMap.containsKey(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM)) {
							if (!StringUtil.isNullOrEmpty(
									parametrosMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM))) {
								isParametro = false;
								resultado.append(parametrosMap
										.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM));
							}
						}
					}
					if (isParametro) {
						if (!esSimulacion) {
							resultado.append("@@${PARAMETRO}:" + obj.getTipo() + ":" + obj.getNumeroDigito() + ":"
									+ obj.getItemValor() + "@@");
						} else {
							resultado.append(FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(),
									obj.getItemValor()));
						}
					}
				} catch (Exception e) {
					log.error("Error ", e);
				}
			} else if (TipoVariableType.PARAMETRO.getKey().equals(obj.getTipo())) {
				if (!isManual) {
					if (obj.getItemValor() != null) {
						TipoParametroType tipoParametroType = TipoParametroType.get(obj.getItemValor().trim());
						if (tipoParametroType != null) {
							if (!esSimulacion) {
								resultado.append("@@{PARAMETRO}:" + obj.getTipo() + ":" + obj.getNumeroDigito() + ":"
										+ obj.getItemValor() + "@@");
							} else {
								resultado.append(StringUtil.completeLeft("9", '9', obj.getNumeroDigito().intValue()));
							}
						}
					}
				} else {
					boolean isParametro = true;
					if (obj.getItemValor() != null) {
						TipoParametroType tipoParametroType = TipoParametroType.get(obj.getItemValor().trim());
						if (tipoParametroType != null) {

							switch (tipoParametroType) {
							case NUMERO_POLIZA:
								if (parametrosMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)) {
									if (!StringUtil.isNullOrEmpty(
											parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM))) {
										isParametro = false;
										resultado.append(
												parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM));
									}
								}
								break;
							case NUMERO_REFERENCIA:
								if (parametrosMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)) {
									if (!StringUtil.isNullOrEmpty(
											parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD))) {
										isParametro = false;
										resultado.append(
												parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD));
									}
								}
								break;

							default:
								break;
							}
						}
					}
					if (isParametro) {
						if (!esSimulacion) {
							resultado.append("@@${PARAMETRO}:" + obj.getTipo() + ":" + obj.getNumeroDigito() + ":"
									+ obj.getItemValor() + "@@");
						} else {
							resultado.append(StringUtil.completeLeft("9", '9', obj.getNumeroDigito().intValue()));
						}
					}
				}
			} else {
				if (!StringUtil.isNullOrEmpty(obj.getItemValor())) {
					resultado.append(obj.getItemValor().trim());
				}
			}
		}
		resultado.append(".");
		TipoArchivoProcesarType tipoArchivoProcesarType = TipoArchivoProcesarType
				.get(configuracionTrama.getTramaNomenclaturaArchivo().getTipoArchivo());
		resultado.append(tipoArchivoProcesarType.getExtension());
		return resultado.toString();
	}

	// (cantidad, longitud, tipo y obligatoriedad de campos) //TODO FINAL
	// NOMENCLATURA
	protected RespuestaLecturaArchivoVO validarPrimerNivelEstructruraTramaNomenclatura(String nomenclatura,
			ConfiguracionTrama configuracionTrama, Map<String, Object> parametrosMap, boolean isManual)
			throws Exception {
		String rutaRelativaTemp = parametrosMap.get("rutaRelativaTemp") + "";
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		resultado.setListaLogConfiguracionTramaAyuda(new BigMemoryManager<String, LogConfiguracionTrama>(
				rutaRelativaTemp, "validar_primer_nivel_data_log_ayuda"));
		boolean existeError = false;
		String mensajeError = "";
		try {
			if (StringUtil.isNullOrEmpty(nomenclatura)) {
				nomenclatura = obtenerNomenclatura(configuracionTrama, parametrosMap, isManual, false);
			}
			FTPVO fTPVO = new FTPVO();
			fTPVO = obtenerFTP(fTPVO, configuracionTrama, TipoDestinatarioFTPType.ORIGEN);
			String ruta = fTPVO.getRutaFTP();
			boolean existeArchivo = false;

			fTPVO.setRutaLocal(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
			fTPVO.setNombreArchivoLocal(nomenclatura);
			fTPVO.setRutaFTP(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
			fTPVO.setNombreArchivoFTP(nomenclatura);
			fTPVO.setIdJuegoTrama(configuracionTrama.getJuegoTrama().getIdJuegoTrama());
			existeArchivo = FTPClienteUtil.descargarArchivo(fTPVO, parametrosMap);

			resultado.setNomenclaturaLeer(nomenclatura);

			resultado.setNomenclatura(nomenclatura);
			if (RespuestaNaturalType.SI.getKey().equals(configuracionTrama.getJuegoTrama().getCopiarDestino())) {
				fTPVO = obtenerFTP(fTPVO, configuracionTrama, TipoDestinatarioFTPType.DESTINO);
				ruta = fTPVO.getRutaFTP();
				String usuario = "manual" + fTPVO.getIdJuegoTrama();
				fTPVO.setRutaLocal(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				String rutaArchivo = ConstanteConfigUtil.generarRuta(ArchivoUtilidades.RUTA_RECURSOS,
						ArchivoUtilidades.RUTA_REPORTE_GENERADO, usuario) + fTPVO.getRutaLocal();
				fTPVO.setRutaLocal(rutaArchivo);
				fTPVO.setNombreArchivoLocal(nomenclatura);
				fTPVO.setRutaFTP(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoFTP(nomenclatura);
				if (!ResourceUtil.esSimulacion(parametrosMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
					FTPClienteUtil.subirArchivo(fTPVO);
				}
			}
			if (!existeArchivo) {
				mensajeError = "Archivo con la nomenclatura " + nomenclatura + " juego trama = "
						+ configuracionTrama.getJuegoTrama().getIdJuegoTrama() + " - "
						+ configuracionTrama.getJuegoTrama().getNombre() + " no se encontro";
				if (ResourceUtil.esSimulacion(parametrosMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
					mensajeError = "No coincide los parametros de referencia ejemplo " + nomenclatura;
				}
				existeError = true;
			}
		} catch (Exception e) {
			log.error("Error ", e);
			mensajeError = "Exception Archivo con la nomenclatura " + nomenclatura + " juego trama = "
					+ configuracionTrama.getJuegoTrama().getIdJuegoTrama() + " - "
					+ configuracionTrama.getJuegoTrama().getNombre() + "  no se encontro";
			existeError = true;
		}
		if (existeError) {
			// log.info("TRAM_LECT_0001 --> Error : " + mensajeError);
			LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "", "Archivo Nomenclatura",
					mensajeError, nomenclatura, configuracionTrama.getIdConfiguradorTrama(),
					ComponenteValidadoLogTramaType.CONF_TRAMA_NO_EXISTE_ARCHIVO, TipoErrorLogTramaType.GENERAL);
			resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
			resultado.setEsError(true);
		}
		resultado.setMensajeError(mensajeError);
		return resultado;
	}

	public RespuestaFTPVO testConexion(ConfiguracionFtpTrama configuracionFtpTramaDTO) {
		FTPVO fTPVO = new FTPVO();
		fTPVO = obtenerFTP(fTPVO, configuracionFtpTramaDTO);
		return FTPClienteUtil.testFTP(fTPVO);
	}

	public FTPVO obtenerFTP(FTPVO fTPVO, ConfiguracionTrama configuracionTrama,
			TipoDestinatarioFTPType tipoDestinatarioFTPType) throws Exception {
		ConfiguracionFtpTrama configuracionFtpTramaDTO = obtenerConfiguracionFtpTrama(configuracionTrama,
				tipoDestinatarioFTPType);
		fTPVO = obtenerFTP(fTPVO, configuracionFtpTramaDTO);
		return fTPVO;

	}

	public FTPVO obtenerFTP(FTPVO fTPVO, ConfiguracionFtpTrama configuracionFtpTrama) {
		fTPVO.setServer(configuracionFtpTrama.getServidor());
		if (configuracionFtpTrama.getPuerto() != null) {
			fTPVO.setPort(configuracionFtpTrama.getPuerto().intValue());
		}
		fTPVO.setUsuario(configuracionFtpTrama.getUsuario());
		fTPVO.setClave(configuracionFtpTrama.getPassword());
		fTPVO.setRutaFTP(configuracionFtpTrama.getRuta());
		// Inicio 18/12/2017 ==>
		fTPVO.setProtocolo(configuracionFtpTrama.getProtocolo());
		// Fin 18/12/2017
		return fTPVO;
	}

	public ConfiguracionFtpTrama obtenerConfiguracionFtpTrama(ConfiguracionTrama configuracionTrama,
			TipoDestinatarioFTPType tipoDestinatarioFTPType) throws Exception {
		ConfiguracionFtpTrama resultado = new ConfiguracionFtpTrama();
		for (ConfiguracionFtpTrama obj : configuracionTrama.getJuegoTrama().getJuegoTramaConfiguracionFtpTramaList()) {
			if (tipoDestinatarioFTPType.getKey().equals(obj.getTipo())) {
				resultado = obj;
				break;
			}
		}
		return resultado;
	}

	protected RespuestaLecturaArchivoVO obtenerGrupoNomenclatura(ConfiguracionTrama configuracionTrama,
			Map<String, Object> parametrosMap, boolean isManual) throws Exception {
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		String rutaRelativaTemp = parametrosMap.get("rutaRelativaTemp") + "";
		resultado.setListaLogConfiguracionTramaAyuda(new BigMemoryManager<String, LogConfiguracionTrama>(
				rutaRelativaTemp, "obtenerGrupoNomenclatura_data_log_ayuda"));
		boolean esSimulacion = ResourceUtil
				.esSimulacion(parametrosMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION));
		String mensajeError = "";
		boolean existeError = false;
		boolean existeErrorFTP = false;
		resultado.setKeyGrupo(configuracionTrama.getJuegoTrama().getIdJuegoTrama() + "");
		String nomenclatura = "";
		String nomenclaturaSimulacion = "";
		FTPVO fTPVO = new FTPVO();
		try {
			nomenclatura = obtenerNomenclatura(configuracionTrama, parametrosMap, isManual, false);
			if (esSimulacion) {
				nomenclaturaSimulacion = obtenerNomenclatura(configuracionTrama, parametrosMap, isManual, true);
				fTPVO.setNomenclaturaSimulacion(nomenclaturaSimulacion);
			}
			List<Map<String, Map<String, String>>> resultadoFTPList = null;
			fTPVO = obtenerFTP(fTPVO, configuracionTrama, TipoDestinatarioFTPType.ORIGEN);
			if (parametrosMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)) {
				fTPVO.setNumeroReferencia(parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD) + "");
			}
			String ruta = fTPVO.getRutaFTP();
			fTPVO.setIdTramaNomenclaturaArchivo(
					configuracionTrama.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo());
			if (nomenclatura.contains("${PARAMETRO}")) {
				fTPVO.setRutaLocal(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoLocal(nomenclatura);
				fTPVO.setRutaFTP(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoFTP(nomenclatura);
				fTPVO.setIdJuegoTrama(configuracionTrama.getJuegoTrama().getIdJuegoTrama());
				RespuestaFTPVO respuestaFTPVO = FTPClienteUtil.descargarArchivoListFilterPersonalizadoGrupo(fTPVO,
						parametrosMap);
				existeErrorFTP = respuestaFTPVO.isError();
				mensajeError = respuestaFTPVO.getMensajeError();
				resultado.setNombreArchivoFTPMap(respuestaFTPVO.getNombreArchivoFTPMap());
				resultadoFTPList = respuestaFTPVO.getListaGrupoArchivoMap();
				for (Map<String, Map<String, String>> resultadoFTPMap : resultadoFTPList) {
					for (Map.Entry<String, Map<String, String>> resultadoFTPDataMap : resultadoFTPMap.entrySet()) {
						Map<String, String> resultadoFTPDataProcesarMap = resultadoFTPDataMap.getValue();
						if (parametrosMap.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD)) {// cuando se
																											// tiene
																											// numero de
																											// solicitud
							if (!resultadoFTPDataProcesarMap
									.containsKey(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)) {
								resultadoFTPDataProcesarMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM,
										null);
								if (!StringUtil.isNullOrEmpty(
										parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM))) {
									resultadoFTPDataProcesarMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM,
											parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM)
													+ "");
								}
							}
						}
						// Inicio requerimiento fecha calculada //
						if (!resultadoFTPDataProcesarMap
								.containsKey(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO)) {
							resultadoFTPDataProcesarMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO,
									null);
							if (!StringUtil.isNullOrEmpty(
									parametrosMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO))) {
								resultadoFTPDataProcesarMap.put(
										ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO,
										parametrosMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO)
												+ "");
							}
						}
						// Fin requerimiento fecha calculada //
						RespuestaLecturaArchivoVO resultadoTempGrupoData = new RespuestaLecturaArchivoVO();
						String resultadoFTP = resultadoFTPDataProcesarMap
								.get(ConstanteConfiguracionTramaUtil.NOMENCLATURA);
						if (!(resultadoFTPList == null || resultadoFTPList.contains("${ERROR}"))) {
							resultadoTempGrupoData.setNumeroReferencia(resultadoFTPDataMap.getKey());
							resultadoTempGrupoData.setNomenclatura(nomenclatura);
							resultadoTempGrupoData.setNomenclaturaLeer(resultadoFTP);
							resultadoTempGrupoData.setPropiedadMap(resultadoFTPDataProcesarMap);
							resultado.getListaRespuestaLecturaArchivoVOGrupo().add(resultadoTempGrupoData);
						}
					}
				}
			} else {
				fTPVO.setRutaLocal(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoLocal(nomenclatura);
				fTPVO.setRutaFTP(FTPClienteUtil.obtenerVariableFechaFormateada(ruta));
				fTPVO.setNombreArchivoFTP(nomenclatura);
				fTPVO.setIdJuegoTrama(configuracionTrama.getJuegoTrama().getIdJuegoTrama());
				RespuestaFTPVO respuestaFTPVO = FTPClienteUtil.descargarArchivoListFilterPersonalizadoGrupo(fTPVO,
						parametrosMap);
				existeErrorFTP = respuestaFTPVO.isError();
				mensajeError = respuestaFTPVO.getMensajeError();
				resultado.setNombreArchivoFTPMap(respuestaFTPVO.getNombreArchivoFTPMap());

				RespuestaLecturaArchivoVO resultadoTempGrupoData = new RespuestaLecturaArchivoVO();
				resultadoTempGrupoData.setNomenclaturaLeer(nomenclatura);
				resultadoTempGrupoData.setNomenclatura(nomenclatura);
				Map<String, String> dataValueMap = new HashMap<>();
				dataValueMap.put(ConstanteConfiguracionTramaUtil.NOMENCLATURA, nomenclatura);
				dataValueMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM, null);
				dataValueMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM, null);
				if (!StringUtil.isNullOrEmpty(parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM))) {
					dataValueMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM,
							parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM) + "");
				}
				if (!StringUtil.isNullOrEmpty(
						parametrosMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM))) {
					dataValueMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM,
							parametrosMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM) + "");
				}
				// Inicio requerimiento fecha calculada //
				if (!StringUtil
						.isNullOrEmpty(parametrosMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO))) {
					dataValueMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO,
							parametrosMap.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO) + "");
				}
				// Fin requerimiento fecha calculada //
				resultadoTempGrupoData.getPropiedadMap().putAll(dataValueMap);
				if (!StringUtil.isNullOrEmpty(parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD))) {
					resultadoTempGrupoData.setNumeroReferencia(
							parametrosMap.get(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD) + "");
				}
				resultado.getListaRespuestaLecturaArchivoVOGrupo().add(resultadoTempGrupoData);
			}
			if (existeErrorFTP) {
				LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "",
						"Archivo Nomenclatura", mensajeError, nomenclatura, configuracionTrama.getIdConfiguradorTrama(),
						ComponenteValidadoLogTramaType.CONF_TRAMA_OBLIGATORIDAD, TipoErrorLogTramaType.GENERAL);
				resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
				resultado.setEsError(true);
			}
		} catch (Exception e) {
			log.error("Error ", e);
			mensajeError = "Error al intentar obtener la nomenclatura FTP : " + fTPVO.getRutaFTP() + " ; "
					+ nomenclatura + " juego trama = " + configuracionTrama.getJuegoTrama().getIdJuegoTrama() + " - "
					+ configuracionTrama.getJuegoTrama().getNombre() + "  no se encontro";
			existeError = true;
			log.error(mensajeError);
		}
		if (existeError) {
			// log.info("TRAM_LECT_0001 --> Error : " + mensajeError);
			LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "", "Archivo Nomenclatura",
					mensajeError, nomenclatura, configuracionTrama.getIdConfiguradorTrama(),
					ComponenteValidadoLogTramaType.CONF_TRAMA_OBLIGATORIDAD, TipoErrorLogTramaType.GENERAL);
			resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
			resultado.setEsError(true);
		}
		// log.error("INFO --> NOMENCLATURA("+ nomenclatura +") LEER : " +
		// resultado.getNomenclaturaLeer());
		return resultado;
	}

	protected LogConfiguracionTrama generarLogConfiguracionTrama(String nombreCampo, String fila, String nombreError,
			String mensajeError, String nombreComponenteValidado, Long idConfiguradorTrama,
			ComponenteValidadoLogTramaType componenteValidadoLogTramaType,
			TipoErrorLogTramaType tipoErrorLogTramaType) {
		LogConfiguracionTrama resultado = new LogConfiguracionTrama();
		resultado.setIdLogConfiguracionTrama(UUIDUtil.generarUUID());
		resultado.setDescripcionError(obtenerDescripcionError(mensajeError));
		resultado.setConponenteValidado(componenteValidadoLogTramaType.getKey());
		resultado.setNombreConponenteValidado(nombreComponenteValidado);
		resultado.setConfiguracionTrama(idConfiguradorTrama);
		resultado.setFechaError(FechaUtil.obtenerFechaActual());
		resultado.setNombreError(nombreError);
		resultado.setTipoError(tipoErrorLogTramaType.getKey());
		resultado.setFila(fila);
		resultado.setNombreCampo(nombreCampo);
		return resultado;
	}

	protected ValidarNomenclaturaVO generarNomenclaturaValidar(ValidarNomenclaturaVO resultado,
			RespuestaLecturaArchivoVO dataNomenclaturaGrupo,
			Map<String, ConfiguracionTrama> listaGrupoNomenClaturaProcesar, String keyGrupo, boolean validadExclusion)
			throws Exception {
		Map<String, Map<String, Map<String, Object>>> valueGrupoMap = new HashMap<String, Map<String, Map<String, Object>>>();
		if (dataNomenclaturaGrupo.getListaRespuestaLecturaArchivoVOGrupo().size() > 0) {
			for (RespuestaLecturaArchivoVO objGrupo : dataNomenclaturaGrupo.getListaRespuestaLecturaArchivoVOGrupo()) {
				if (!valueGrupoMap.containsKey(objGrupo.getNumeroReferencia())) {
					Map<String, Map<String, Object>> valueNomenclaturaMap = new HashMap<String, Map<String, Object>>();
					for (Map.Entry<String, ConfiguracionTrama> grupoNomenClaturaProcesarMap : listaGrupoNomenClaturaProcesar
							.entrySet()) {
						Map<String, Object> parametroTempMap = new HashMap<>();
						if (!StringUtil.isNullOrEmpty(
								objGrupo.getPropiedadMap().get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM))) {
							parametroTempMap.put(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM, objGrupo
									.getPropiedadMap().get(ConstanteConfiguracionTramaUtil.NUMERO_POLIZA_PARAM));
						}
						if (!StringUtil.isNullOrEmpty(objGrupo.getPropiedadMap()
								.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM))) {
							parametroTempMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM,
									objGrupo.getPropiedadMap()
											.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_FORMATO_PARAM));
						}
						// inicio mejora fecha calculada
						if (!StringUtil.isNullOrEmpty(objGrupo.getPropiedadMap()
								.get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO))) {
							parametroTempMap.put(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO, objGrupo
									.getPropiedadMap().get(ConstanteConfiguracionTramaUtil.FECHA_CALCULADA_PARAMETRO));
						}
						// fin mejora fecha calculada
						if (!StringUtil.isNullOrEmpty(objGrupo.getNumeroReferencia())) {
							parametroTempMap.put(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD,
									objGrupo.getNumeroReferencia());
						}
						String nommenclatura = obtenerNomenclatura(grupoNomenClaturaProcesarMap.getValue(),
								parametroTempMap, true, false);
						if (dataNomenclaturaGrupo.getNombreArchivoFTPMap().containsKey(nommenclatura.toUpperCase())) {
							nommenclatura = dataNomenclaturaGrupo.getNombreArchivoFTPMap()
									.get(nommenclatura.toUpperCase());
						}
						parametroTempMap.put(ConstanteConfiguracionTramaUtil.NOMENCLATURA, nommenclatura);
						valueNomenclaturaMap.put(grupoNomenClaturaProcesarMap.getKey(), parametroTempMap);
					}
					if (validadExclusion) {
						String nommenclaturaValidar = objGrupo.getNomenclaturaLeer();
						String keyNomenclaturaExcluir = keyGrupo + ";" + objGrupo.getNumeroReferencia();
						if (!resultado.getNomenclaturaExcluirMap().containsKey(keyNomenclaturaExcluir)) {
							resultado.getNomenclaturaExcluirMap().put(keyNomenclaturaExcluir, nommenclaturaValidar);
						}
					}
					valueGrupoMap.put(objGrupo.getNumeroReferencia(), valueNomenclaturaMap);
				}
			}
		}
		resultado.setValueGrupoMap(valueGrupoMap);
		return resultado;
	}

	protected ConfiguracionTramaIdProcesarVO obtenerIdProcesar(ConfiguracionTramaIdProcesarVO resultado) {
		for (ConfiguracionTrama configuracionTrama : resultado.getListaConfiguracionTrama()) {
			if (!resultado.getListaIdConfiguracionTrama().contains(configuracionTrama.getIdConfiguradorTrama())) {
				resultado.getListaIdConfiguracionTrama().add(configuracionTrama.getIdConfiguradorTrama());
			}
			if (!resultado.getListaIdTramaNomenclaturaArchivo()
					.contains(configuracionTrama.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo())) {
				resultado.getListaIdTramaNomenclaturaArchivo()
						.add(configuracionTrama.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo());
			}
			if (!resultado.getListaNombreEsquemaTabla().contains(configuracionTrama.getNombreTabla())) {
				resultado.getListaNombreEsquemaTabla().add(configuracionTrama.getNombreTabla());
			}
			if (!resultado.getListaIdJuegoTrama().contains(configuracionTrama.getJuegoTrama().getIdJuegoTrama())) {
				resultado.getListaIdJuegoTrama().add(configuracionTrama.getJuegoTrama().getIdJuegoTrama());
			}
			// filtro equivalencia
			if (!resultado.getListaCanal().contains(configuracionTrama.getJuegoTrama().getCanal())) {
				resultado.getListaCanal().add(configuracionTrama.getJuegoTrama().getCanal());
			}
			if (!resultado.getListaProducto().contains(configuracionTrama.getJuegoTrama().getProducto())) {
				resultado.getListaProducto().add(configuracionTrama.getJuegoTrama().getProducto());
			}
		}
		return resultado;
	}

	protected ConfiguracionTramaIdProcesarVO complentarConfiguracionTrama(ConfiguracionTramaIdProcesarVO objVO) {
		for (ConfiguracionTrama obj : objVO.getListaConfiguracionTrama()) {
			obj.setConfiguracionTramaConfiguracionTramaDetalleList(new ArrayList<ConfiguracionTramaDetalle>());
			if (objVO.getListaConfiguracionTramaDetalleMap().containsKey(obj.getIdConfiguradorTrama())) {
				obj.setConfiguracionTramaConfiguracionTramaDetalleList(
						objVO.getListaConfiguracionTramaDetalleMap().get(obj.getIdConfiguradorTrama()));
			}
			obj.getTramaNomenclaturaArchivo().setTramaNomenclaturaArchivoTramaNomenclaturaArchivoDetalleList(
					new ArrayList<TramaNomenclaturaArchivoDetalle>());
			if (objVO.getListaTramaNomenclaturaArchivoDetalleMap()
					.containsKey(obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo())) {
				obj.getTramaNomenclaturaArchivo().setTramaNomenclaturaArchivoTramaNomenclaturaArchivoDetalleList(
						objVO.getListaTramaNomenclaturaArchivoDetalleMap()
								.get(obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo()));
			}
			obj.getJuegoTrama().setJuegoTramaJuegoTramaCorreoList(new ArrayList<JuegoTramaCorreo>());
			obj.getJuegoTrama().setJuegoTramaConfiguracionFtpTramaList(new ArrayList<ConfiguracionFtpTrama>());
			if (objVO.getListaConfiguracionFtpTramaMap().containsKey(obj.getJuegoTrama().getIdJuegoTrama())) {
				obj.getJuegoTrama().setJuegoTramaConfiguracionFtpTramaList(
						objVO.getListaConfiguracionFtpTramaMap().get(obj.getJuegoTrama().getIdJuegoTrama()));
			}
			if (!objVO.getListaConfiguracionTramaCampoNoPersistenteMap().containsKey(obj.getIdConfiguradorTrama())) {
				Map<String, String> value = new HashMap<>();
				for (ConfiguracionTramaDetalle objDet : obj.getConfiguracionTramaConfiguracionTramaDetalleList()) {
					boolean isCampoPersistente = !RespuestaNaturalType.NO.getKey().equals(objDet.getEsPersistente());
					if (!isCampoPersistente) {
						String nombeCampoTabla = objDet.getNombreCampo();
						value.put(nombeCampoTabla, nombeCampoTabla);
					}
				}
				objVO.getListaConfiguracionTramaCampoNoPersistenteMap().put(obj.getIdConfiguradorTrama(), value);
			}
		}
		return objVO;
	}

	protected Map<String, ConfiguracionTrama> validarExlusionNumeroSolicitud(
			ConfiguracionTramaIdProcesarVO configuracionTramaIdProcesarVO, List<String> listaKeyNumeroSolicitudOrden,
			boolean validadExclusion, String keyGrupoOrden,
			Map<String, Map<String, Map<String, Object>>> grupoArchivoProcesarMap) {
		Map<String, ConfiguracionTrama> listaKeyNumeroSolicitudOrdenExcluirMap = new HashMap<String, ConfiguracionTrama>();
		if (validadExclusion) {
			for (String keyOrden : listaKeyNumeroSolicitudOrden) {
				for (ConfiguracionTrama obj : configuracionTramaIdProcesarVO.getJuegoConfiguracionTramaMap()
						.get(keyGrupoOrden)) {
					String keyNomenclatura = obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo() + "";
					Map<String, Map<String, Object>> listaArchivoNomenclaturaProcesarTempMap = grupoArchivoProcesarMap
							.get(keyOrden);
					Map<String, Object> parametroProcesadoMap = listaArchivoNomenclaturaProcesarTempMap
							.get(keyNomenclatura);
					String nomenclaturaExcluir = parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NOMENCLATURA)
							+ "";
					if (configuracionTramaIdProcesarVO.getNomenclaturaExcluirByFechaMap()
							.containsKey(nomenclaturaExcluir)) {
						listaKeyNumeroSolicitudOrdenExcluirMap.put(keyOrden, obj);
					}
					break;// buscamos solo el primero, ya que esto se registra
				}
			}
		}
		return listaKeyNumeroSolicitudOrdenExcluirMap;
	}

	protected ConfiguracionTramaIdProcesarVO completarIdProcesamiento(ConfiguracionTramaIdProcesarVO objConf)
			throws Exception {
		objConf = obtenerIdProcesar(objConf);
		objConf.setCamposDiponibleTablaMap(
				campoTablaDisponibleDao.obtenerCampoTablaDisponibleMap(objConf.getListaNombreEsquemaTabla()));
		objConf.setListaConfiguracionTramaDetalleMap(
				configuracionTramaDao.listarMap(objConf.getListaIdConfiguracionTrama()));
		objConf.setListaConfiguracionTramaErrorMap(
				configuracionTramaDao.listarConfiguracionTramaErrorMap(objConf.getListaIdConfiguracionTrama()));
		objConf.setListaTramaNomenclaturaArchivoDetalleMap(
				configuracionTramaDao.listarNomenclaturaDetMap(objConf.getListaIdTramaNomenclaturaArchivo()));
		// Inicio obtener equivalencia
		List<EquivalenciaDato> listaEquivalencia = configuracionTramaDao
				.listarEquivalenciaListCanalProducto(objConf.getListaProducto(), objConf.getListaCanal());
		objConf = generarEquivalenciaMap(objConf, listaEquivalencia);
		// Fin obtener equivalencia
		// Map<Long,List<JuegoTramaCorreoDTO>> listaJuegoTramaCorreoMap =
		// configuracionTramaAutomaticoDaoImpl.listarJuegoTramaCorreoMap(listaIdJuegoTrama,
		// sessionHibernate);
		objConf.setListaConfiguracionFtpTramaMap(
				configuracionTramaDao.listarConfiguracionFTPTramaMap(objConf.getListaIdJuegoTrama()));

		objConf.setListaGrupoConfiguracionTramaValidarObigatoridadMap(
				obtenerListaGrupoConfiguracionTramaValidarObligatoridadMap(objConf.getListaConfiguracionTrama()));
		objConf.setListaConfiguracionTramaTablaMap(new HashMap<String, String>());
		objConf = complentarConfiguracionTrama(objConf);

		objConf.setListaDepenciaDataMap(obtenerListaDependenciaDataMap(objConf.getListaConfiguracionTrama()));
		objConf.setListaDepenciaTablaMixtaDataMap(
				obtenerListaDependenciaTablaMixtaDataMap(objConf.getListaConfiguracionTrama()));
		objConf.setListaTablaMixtaMap(obtenerListaProcesamientoMixto());
		// generado juego trama
		objConf.setJuegoConfiguracionTramaMap(new HashMap<String, List<ConfiguracionTrama>>());
		objConf.setNomenclaturaJuegoConfiguracionTramaMap(new HashMap<String, Map<String, ConfiguracionTrama>>());
		objConf.setListaRespuestaLecturaArchivoTemporalGrupoMap(
				new HashMap<String, Map<String, Map<String, Map<String, Object>>>>());
		objConf.setNomenclaturaExcluirMap(new HashMap<String, String>());
		objConf.setNomenclaturaExcluirVO(new ValidarNomenclaturaVO());
		objConf.getNomenclaturaExcluirVO().setNomenclaturaExcluirMap(objConf.getNomenclaturaExcluirMap());
		return objConf;
	}

	protected ConfiguracionTramaIdProcesarVO generarEquivalenciaMap(
			ConfiguracionTramaIdProcesarVO configuracionTramaIdProcesarVO, List<EquivalenciaDato> listaEquivalencia)
			throws Exception {
		for (EquivalenciaDato obj : listaEquivalencia) {
			// Inicio aqui se aumento ramo y polizagrupo
			String keySimpleSinDepencia = generarKey(obj.getProducto(), obj.getCanal(), obj.getCodigoCampo(),
					obj.getValorOrigen(), obj.getCodigoRamo(), obj.getPolizaGrupo());
			// Fin aqui se aumento ramo y polizagrupo
			if (!configuracionTramaIdProcesarVO.getEquivalenciaHomologacionSimpleSinDependenciaMap()
					.containsKey(keySimpleSinDepencia)) {
				configuracionTramaIdProcesarVO.getEquivalenciaHomologacionSimpleSinDependenciaMap()
						.put(keySimpleSinDepencia, obj);
			}
			if (obj.getEquivalenciaDato() != null) {
				String keySimpleConDepencia = generarKey(obj.getProducto(), obj.getCanal(), obj.getCodigoCampo(),
						obj.getValorOrigen(), obj.getEquivalenciaDato().getIdEquivalencia() + "");
				if (!configuracionTramaIdProcesarVO.getEquivalenciaHomologacionSimpleConDependenciaMap()
						.containsKey(keySimpleConDepencia)) {
					configuracionTramaIdProcesarVO.getEquivalenciaHomologacionSimpleConDependenciaMap()
							.put(keySimpleConDepencia, obj);
				}
			}
			if (obj.getEquivalenciaDato() != null) {
				String keyCalculado = generarKey(obj.getProducto(), obj.getCanal(), obj.getCodigoCampo(),
						obj.getEquivalenciaDato().getIdEquivalencia() + "");
				if (!configuracionTramaIdProcesarVO.getEquivalenciaHomologacionSimpleConDependenciaMap()
						.containsKey(keyCalculado)) {
					configuracionTramaIdProcesarVO.getEquivalenciaHomologacionSimpleConDependenciaMap()
							.put(keyCalculado, obj);
				}
			}
		}
		return configuracionTramaIdProcesarVO;
	}

	private String generarKey(Object... keys) {
		return StringUtil.generarKey(keys);
	}

	protected ConfiguracionTramaIdProcesarVO generarJuegoTramaProcesar(ConfiguracionTramaIdProcesarVO objConf,
			Map<String, Object> parametroMap, boolean isManual, boolean validadExclusion) throws Exception {
		for (ConfiguracionTrama obj : objConf.getListaConfiguracionTrama()) {
			String keyGrupo = obj.getJuegoTrama().getIdJuegoTrama() + "";
			String keyNomenclatura = obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo() + "";
			if (!objConf.getNomenclaturaJuegoConfiguracionTramaMap().containsKey(keyGrupo)) {
				Map<String, ConfiguracionTrama> valueNomnenclaturaMap = new HashMap<>();
				valueNomnenclaturaMap.put(keyNomenclatura, obj);
				objConf.getNomenclaturaJuegoConfiguracionTramaMap().put(keyGrupo, valueNomnenclaturaMap);
			} else {
				Map<String, ConfiguracionTrama> valueNomnenclaturaMap = objConf
						.getNomenclaturaJuegoConfiguracionTramaMap().get(keyGrupo);
				if (!valueNomnenclaturaMap.containsKey(keyNomenclatura)) {
					valueNomnenclaturaMap.put(keyNomenclatura, obj);
					objConf.getNomenclaturaJuegoConfiguracionTramaMap().put(keyGrupo, valueNomnenclaturaMap);
				}
			}
		}
		objConf.setListaKeyGrupo(new ArrayList<String>());
		Map<String, Long> nomenclaturaValidaSimulacionMap = new HashMap<>();
		Map<String, Object> parametroSimulacionMap = new HashMap<>();
		for (ConfiguracionTrama obj : objConf.getListaConfiguracionTrama()) {
			String keyGrupo = obj.getJuegoTrama().getIdJuegoTrama() + "";
			if (!objConf.getJuegoConfiguracionTramaMap().containsKey(keyGrupo)) {
				List<ConfiguracionTrama> value = new ArrayList<ConfiguracionTrama>();
				value.add(obj);
				objConf.getJuegoConfiguracionTramaMap().put(keyGrupo, value);
				objConf.getListaKeyGrupo().add(keyGrupo);
			} else {
				List<ConfiguracionTrama> value = objConf.getJuegoConfiguracionTramaMap().get(keyGrupo);
				value.add(obj);
				objConf.getJuegoConfiguracionTramaMap().put(keyGrupo, value);
			}
			// agrupando archivos a procesar
			if (!objConf.getListaRespuestaLecturaArchivoTemporalGrupoMap().containsKey(keyGrupo)) {
				Map<String, ConfiguracionTrama> listaGrupoNomenClaturaProcesar = objConf
						.getNomenclaturaJuegoConfiguracionTramaMap().get(keyGrupo);
				// TODO: APOYO A ORQUESTADOR SE CONECTA AL FTP
				RespuestaLecturaArchivoVO dataNomenclaturaGrupo = obtenerGrupoNomenclatura(obj, parametroMap, isManual);
				if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
					String keyValidaNomenclatura = keyGrupo + ""
							+ obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo();
					nomenclaturaValidaSimulacionMap.put(keyValidaNomenclatura, 0L);
					parametroSimulacionMap.putAll(parametroMap);
					parametroSimulacionMap.putAll(dataNomenclaturaGrupo.getPropiedadMap());
				}
				if (dataNomenclaturaGrupo.getListaLogConfiguracionTramaAyuda().getListaKey().size() > 0) {
					objConf.getListaLogConfiguracionTramaAyuda()
							.addAll(dataNomenclaturaGrupo.getListaLogConfiguracionTramaAyuda());
				}
				Map<String, Map<String, Map<String, Object>>> valueGrupoMap = new HashMap<String, Map<String, Map<String, Object>>>();
				if (dataNomenclaturaGrupo.getListaRespuestaLecturaArchivoVOGrupo().size() > 0) {
					if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
						String nroReferenciaSimulacion = dataNomenclaturaGrupo.getListaRespuestaLecturaArchivoVOGrupo()
								.get(0).getNumeroReferencia();
						parametroSimulacionMap.put(ConstanteConfiguracionTramaUtil.NUMERO_SOLICITUD,
								nroReferenciaSimulacion);
						parametroSimulacionMap.putAll(dataNomenclaturaGrupo.getListaRespuestaLecturaArchivoVOGrupo()
								.get(0).getPropiedadMap());
					}
					objConf.setNomenclaturaExcluirVO(generarNomenclaturaValidar(objConf.getNomenclaturaExcluirVO(),
							dataNomenclaturaGrupo, listaGrupoNomenClaturaProcesar, keyGrupo, validadExclusion));
					valueGrupoMap = objConf.getNomenclaturaExcluirVO().getValueGrupoMap();
					objConf.setNomenclaturaExcluirMap(objConf.getNomenclaturaExcluirVO().getNomenclaturaExcluirMap());
				}
				objConf.getListaRespuestaLecturaArchivoTemporalGrupoMap().put(keyGrupo, valueGrupoMap);
				// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
				EjecutarActividadParaleloInstance.getInstance().putActividad(parametroMap.get("idControlProceso") + "",
						"TRAMAFTP_DATA", objConf);
				// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
			} else {
				// VALIDANDO SIMULAACION NOMENCLATURA
				if (ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
					String keyValidaNomenclatura = keyGrupo + ""
							+ obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo();
					if (!nomenclaturaValidaSimulacionMap.containsKey(keyValidaNomenclatura)) {
						RespuestaLecturaArchivoVO dataNomenclaturaGrupo = obtenerGrupoNomenclatura(obj,
								parametroSimulacionMap, isManual);
						if (dataNomenclaturaGrupo.getListaLogConfiguracionTramaAyuda().getListaKey().size() > 0) {
							objConf.getListaLogConfiguracionTramaAyuda()
									.addAll(dataNomenclaturaGrupo.getListaLogConfiguracionTramaAyuda());
							nomenclaturaValidaSimulacionMap.put(keyValidaNomenclatura, 0L);
						}
					}
				}
			}
		}
		objConf.setNomenclaturaExcluirByFechaMap(new HashMap<String, String>());
		if (validadExclusion) {
			objConf.setNomenclaturaExcluirByFechaMap(
					configuracionTramaDao.obtenerNomenclaturaExcluirMap(objConf.getNomenclaturaExcluirMap()));
		}
		return objConf;
	}

	protected ConfiguracionTramaIdProcesarVO procesarGrupoJuegoTramaExlusion(
			ConfiguracionTramaIdProcesarVO configuracionTramaIdProcesarVO, Map<String, Object> parametroMap,
			boolean isManual, boolean validadExclusion) throws Exception {
		for (String keyGrupoOrden : configuracionTramaIdProcesarVO.getListaKeyGrupo()) {
			// Procesando juegos y grupos de archivos
			Map<String, Map<String, Map<String, Object>>> grupoArchivoProcesarMap = configuracionTramaIdProcesarVO
					.getListaRespuestaLecturaArchivoTemporalGrupoMap().get(keyGrupoOrden);
			List<String> listaKeyNumeroSolicitudOrdenTemp = new ArrayList<String>(grupoArchivoProcesarMap.keySet());
			Collections.sort(listaKeyNumeroSolicitudOrdenTemp);
			for (String keyOrden : listaKeyNumeroSolicitudOrdenTemp) {
				for (ConfiguracionTrama obj : configuracionTramaIdProcesarVO.getJuegoConfiguracionTramaMap()
						.get(keyGrupoOrden)) {
					String keyNomenclatura = obj.getTramaNomenclaturaArchivo().getIdTramaNomenclaturaArchivo() + "";
					Map<String, Map<String, Object>> listaArchivoNomenclaturaProcesarTempMap = grupoArchivoProcesarMap
							.get(keyOrden);
					Map<String, Object> parametroProcesadoMap = listaArchivoNomenclaturaProcesarTempMap
							.get(keyNomenclatura);
					String nomenclaturaExcluir = parametroProcesadoMap.get(ConstanteConfiguracionTramaUtil.NOMENCLATURA)
							+ "";
					if (!configuracionTramaIdProcesarVO.getNomenclaturaExcluirByFechaMap()
							.containsKey(nomenclaturaExcluir)) {
						Map<String, Object> nomenclaturaExcluirDataMap = new HashMap<>();
						nomenclaturaExcluirDataMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_NUM_REF,
								keyOrden);
						nomenclaturaExcluirDataMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_NOM_ARCHIVO,
								nomenclaturaExcluir);
						nomenclaturaExcluirDataMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_N_ID_JUEGO,
								keyGrupoOrden);
						if (!parametroMap.containsKey(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_ORIGEN)) {
							nomenclaturaExcluirDataMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_ORIGEN,
									"M");
						} else {
							nomenclaturaExcluirDataMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_ORIGEN,
									parametroMap.get(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_ORIGEN));
						}
						if (!parametroMap.containsKey(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_USUARIO)) {
							nomenclaturaExcluirDataMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_USUARIO,
									"mytron".toUpperCase());
						} else {
							nomenclaturaExcluirDataMap.put(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_USUARIO,
									parametroMap.get(ConstanteConfiguracionTramaUtil.CAMPO_EXCLUSION_C_USUARIO));
						}
						// TODO:VER_SIMULACION
						if (!ResourceUtil
								.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
							configuracionTramaDao.registrarNomenclaturaExcluirMap(nomenclaturaExcluirDataMap);
						}
					}
					break;// buscamos solo el primero, ya que esto se registra
				}
			}
		}
		return configuracionTramaIdProcesarVO;
	}

	protected RespuestaLecturaArchivoVO registrarTasmctrLoteMpe(Map<String, Object> parametroMap,
			TasmctrLoteMpe tasmctrlMpe, Long idConfiguracionTrama, EntityManager sessionHibernate) {
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		String rutaRelativaTemp = parametroMap.get("rutaRelativaTemp") + "";
		resultado.setListaLogConfiguracionTramaAyuda(new BigMemoryManager<String, LogConfiguracionTrama>(
				rutaRelativaTemp, "registrarTasmctrLoteMpe_data_log_ayuda"));
		try {
			tasmctrlMpe.setFechaHoraFinCargaTrama(FechaUtil.obtenerFechaActual());
			if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				tasmctrlMpeDao.saveNative(tasmctrlMpe);
			}
		} catch (Exception e) {
			log.error("Error ", e);
			String mensajeError = e.getMessage();
			LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "",
					"Exception:No Registro TASMCTRL_MPE",
					obtenerDescripcionError(
							"Fecha Lote = " + tasmctrlMpe.getTasmctrLoteMpePK().getFechaLote() + " numero Lote = "
									+ tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote() + " " + mensajeError),
					"Exception:No Registro TASMCTRL_MPEs", idConfiguracionTrama,
					ComponenteValidadoLogTramaType.CONF_TRAMA, TipoErrorLogTramaType.GENERAL);
			resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
			resultado.setEsError(true);
		}
		// Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
		EjecutarActividadParaleloInstance.getInstance().putActividad(parametroMap.get("idControlProceso") + "",
				"TRAMAFTP", new ConfiguracionTramaIdProcesarVO());
		// Fin BuildSoft Mejora Orquestador flujo 11/09/2019
		return resultado;
	}

	protected RespuestaLecturaArchivoVO registrarTasmctrTramaMpe(Map<String, Object> parametroMap,
			TasmctrLoteMpe tasmctrlMpe, Map<String, TasmctrTramaMpe> tasmctrTramaMpeMap,
			EntityManager sessionHibernate) {
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		String rutaRelativaTemp = parametroMap.get("rutaRelativaTemp") + "";
		resultado.setListaLogConfiguracionTramaAyuda(new BigMemoryManager<String, LogConfiguracionTrama>(
				rutaRelativaTemp, "registrarTasmctrTramaMpe_data_log_ayuda"));
		Long idConfiguracionTrama = 0L;
		for (Map.Entry<String, TasmctrTramaMpe> objMap : tasmctrTramaMpeMap.entrySet()) {
			try {
				TasmctrTramaMpe tasmctrTramaMpe = objMap.getValue();
				tasmctrTramaMpe.setFechaHoraFinCarga(FechaUtil.obtenerFechaActual());
				idConfiguracionTrama = tasmctrTramaMpe.getIdConfiguradorTrama();
				// persistiendo TasmctrlMpe
				if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
					tasmctrTramaMpeAutomaticoDaoLocal.saveNative(tasmctrTramaMpe);
				}
			} catch (Exception e) {
				log.error("Error ", e);
				String mensajeError = e.getMessage();
				LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "",
						"Exception:No Registro TASMCTRT_MPE",
						obtenerDescripcionError(
								"Fecha Lote = " + tasmctrlMpe.getTasmctrLoteMpePK().getFechaLote() + " numero Lote = "
										+ tasmctrlMpe.getTasmctrLoteMpePK().getNumeroLote() + " " + mensajeError),
						"Exception:No Registro TASMCTRT_MPE", idConfiguracionTrama,
						ComponenteValidadoLogTramaType.CONF_TRAMA, TipoErrorLogTramaType.GENERAL);
				resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);
				resultado.setEsError(true);
			}
		}
		return resultado;

	}

	protected void registrarLogConfiguracionDataGrupo(GrupoConfiguracionTramaVO configuracionTramaCnfVO,
			TasmctrLoteMpe tasmctrlMpe, Map<String, Object> parametroMap,
			BigMemoryManager<String, TasmeErrorMpe> listaLongConfiguracionTramaData, EntityManager sessionHibernate) {
		if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
			int index = 0;
			Map<String, String> camposEDGPMap = obtenerListaCamposActualizarEDGP();
			Map<String, Boolean> juegoConfiguracionTramaErrorMap = new HashMap<String, Boolean>();
			Map<String, Map<String, Object>> actualizarEDGPMap = new HashMap<String, Map<String, Object>>();
			for (String logConfiguracionTramaDTOKey : listaLongConfiguracionTramaData.getListaKey()) {
				TasmeErrorMpe logConfiguracionTrama = listaLongConfiguracionTramaData.get(logConfiguracionTramaDTOKey);
				index++;
				try {
					logConfiguracionTrama.getTasmeErrorMpePK().setNumeroSecuencia(new BigDecimal(index));
					registrarLogConfiguracionTrama(logConfiguracionTrama, sessionHibernate);
					// actualizando nuevo requerimiento actualizando la tabla intermedia EDGP
					// Inicio
					Map<String, Object> parametrosDataMap = TransferDataUtil.toVOMap(logConfiguracionTrama);
					if (parametrosDataMap != null) {
						StringBuilder key = new StringBuilder();
						boolean existeError = false;
						for (Map.Entry<String, String> camposMap : camposEDGPMap.entrySet()) {
							if (parametrosDataMap.containsKey(camposMap.getKey())
									&& !StringUtil.isNullOrEmpty(parametrosDataMap.get(camposMap.getKey()))) {
								key.append(parametrosDataMap.get(camposMap.getKey()));
							} else {
								existeError = true;
								break;
							}
						}
						if (!existeError) {
							if (!actualizarEDGPMap.containsKey(key.toString())) {
								Map<String, Object> value = new HashMap<>();
								for (Map.Entry<String, String> camposMap : camposEDGPMap.entrySet()) {
									value.put(camposMap.getKey(), parametrosDataMap.get(camposMap.getKey()));
									juegoConfiguracionTramaErrorMap.put(parametrosDataMap.get(camposMap.getKey()) + "",
											true);
								}
								actualizarEDGPMap.put(key.toString(), value);
							}
						}
					}
					// Fin
				} catch (Exception e) {
					log.error("Error ", e);
				}
			}
			// actualizando nuevo requerimiento actualizando la tabla intermedia EDGP
			// Inicio
			for (Map.Entry<String, Map<String, Object>> dataEDGPMap : actualizarEDGPMap.entrySet()) {
				try {
					configuracionTramaDao.registrarFlagTasmeEDGP(dataEDGPMap.getValue(),
							EstadoDetalleProcesoType.CON_ERROR.getKey());
				} catch (Exception e) {
					log.error("Error ", e);
				}
			}
			// Fin
			// TODO: generar el detalle control Proceso - MARTIN REALIZADO.
			// TODO:VER_SIMULACION
			if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				controlProcesoDetalleAutomaticoDaoLocal.registrar(tasmctrlMpe, configuracionTramaCnfVO,
						juegoConfiguracionTramaErrorMap, parametroMap);
				controlProcesoAutomaticoDetalleHistDaoLocal.registrar(tasmctrlMpe, configuracionTramaCnfVO,
						juegoConfiguracionTramaErrorMap, parametroMap);
			}
			// Fin
		}
		listaLongConfiguracionTramaData = null;
	}

	protected void registrarLogConfiguracionDataAyudaGrupo(Map<String, Object> parametroMap,
			BigMemoryManager<String, LogConfiguracionTrama> listaLongConfiguracionTramaDataAyuda,
			EntityManager sessionHibernate) {
		if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
			for (String key : listaLongConfiguracionTramaDataAyuda.getListaKey()) {
				LogConfiguracionTrama logConfiguracionTrama = listaLongConfiguracionTramaDataAyuda.get(key);
				try {
					registrarLogConfiguracionTramaAyuda(logConfiguracionTrama);
				} catch (Exception e) {
					log.error("Error ", e);
				}
			}
			// listaLongConfiguracionTramaDataAyuda.clean();
		}
		listaLongConfiguracionTramaDataAyuda = null;
	}

	protected void registrarLogConfiguracionTrama(TasmeErrorMpe logConfiguracionTrama, EntityManager sessionHibernate) {
		try {
			tasmeErrorMpeDao.saveNative(logConfiguracionTrama);
		} catch (Exception e) {
			log.error("Error ", e);
		}
	}

	protected void registrarLogConfiguracionTramaAyuda(LogConfiguracionTrama obj) {
		try {
			logConfiguracionTramaDao.saveNative(obj);
		} catch (Exception e) {
			log.error("Error ", e);
		}
	}

	protected Integer obtenerCantidad(List<ConfiguracionTramaDataVO> listaCampoValue) {
		Integer resultado = 0;
		for (ConfiguracionTramaDataVO objValue : listaCampoValue) {
			if (!StringUtil.isNullOrEmpty(objValue.getAtributeValue())) {
				resultado++;
			}
		}
		return resultado;
	}

	protected RespuestaLecturaAgrupadoVO obtenerAgrupacionMap(
			ConfiguracionTramaIdProcesarVO configuracionTramaEquivalenciaMapVO, ConfiguracionTrama configuracionTrama,
			List<Map<String, ValueDataVO>> listaDataResulMap,
			Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap,
			Map<String, String> campoMappingTypeMap, Map<String, String> configuracionTramaErrorMap,
			Map<String, RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoTemporalMap, String nombreTabla,
			String nomenclatura, Date fechaLote, String numeroLote, Map<String, Object> parametroMap) throws Exception {
		String rutaRelativaTemp = parametroMap.get("rutaRelativaTemp") + "";
		Map<String, Object> numeroLoteMap = new HashMap<>();
		Map<String, Object> fechaMap = new HashMap<>();
		RespuestaLecturaAgrupadoVO resultado = new RespuestaLecturaAgrupadoVO();
		resultado.setListaLogConfiguracionTramaAyuda(
				new BigMemoryManager<String, LogConfiguracionTrama>(rutaRelativaTemp, "agrupacion_data_log_ayuda"));
		Map<String, List<Integer>> grupoMap = new HashMap<String, List<Integer>>();
		List<Map<String, ValueDataVO>> listaDataResulGrupoMap = new ArrayList<Map<String, ValueDataVO>>();

		Map<Long, EquivalenciaDato> configuracionDetalleEquivalenciaMap = new HashMap<Long, EquivalenciaDato>();
		int index = 0;
		for (Map<String, ValueDataVO> map : listaDataResulMap) {
			StringBuilder key = new StringBuilder();
			for (Map.Entry<String, ValueDataVO> mapValue : map.entrySet()) {
				// validar
				ConfiguracionTramaDetalle configuracionTramaDetalle = configuracionTramaDetalleMap
						.get(mapValue.getKey());
				boolean isAgrupador = RespuestaNaturalType.SI.getKey()
						.equals(configuracionTramaDetalle.getFlagCampoAgrupador());
				if (isAgrupador) {
					key.append(mapValue.getValue() + "");
				}
			}
			// para agrupar
			if (StringUtil.isNullOrEmpty(key)) {
				if (!grupoMap.containsKey(key.toString())) {
					List<Integer> listaDataResulGrupoValue = new ArrayList<Integer>();
					listaDataResulGrupoValue.add(index);
					grupoMap.put(key.toString(), listaDataResulGrupoValue);
				} else {
					List<Integer> listaDataResulGrupoValue = grupoMap.get(key.toString());
					listaDataResulGrupoValue.add(index);
					grupoMap.put(key.toString(), listaDataResulGrupoValue);
				}
			} else {
				if (!grupoMap.containsKey(key.toString())) {
					List<Integer> listaDataResulGrupoValue = new ArrayList<Integer>();
					listaDataResulGrupoValue.add(index);
					grupoMap.put(key.toString(), listaDataResulGrupoValue);
				}
			}
			index++;
		}
		// modificando valores de grupos de variables estaticas
		Map<String, Object> secuenciaNumeroRiesgoMAp = new HashMap<>();
		Map<String, Object> secuenciaNumeroSecuencialMAp = new HashMap<>();
		for (Map.Entry<String, List<Integer>> objGrupoMap : grupoMap.entrySet()) {
			for (Integer indexMap : objGrupoMap.getValue()) {
				Map<String, ValueDataVO> listKeyValueMap = listaDataResulMap.get(indexMap.intValue());
				List<ConfiguracionTramaDetalle> listaConfiguracionDetalleOrdenar = new ArrayList<>();
				for (Map.Entry<String, ValueDataVO> mapValue : listKeyValueMap.entrySet()) {
					ConfiguracionTramaDetalle configuracionTramaDetalle = configuracionTramaDetalleMap
							.get(mapValue.getKey());
					listaConfiguracionDetalleOrdenar.add(configuracionTramaDetalle);
				}
				CollectionUtil.ordenador(false, listaConfiguracionDetalleOrdenar, "orden");// odenando para leer
																							// equivalencia simple y
																							// compuesta
				for (ConfiguracionTramaDetalle objDet : listaConfiguracionDetalleOrdenar) {
					String mapValueKey = objDet.getNombeCampoTabla();
					boolean existeTipoHomologacion = objDet.getTipoHomologacion() != null
							&& !objDet.getTipoHomologacion().equals("");
					// verificando campo fijo
					boolean isLecturaTrama = RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama());
					ConfiguracionTrama configuracionTramaAsociada = getConfiguracionTramaAsociada(objDet);
					boolean isTramaAsociado = configuracionTramaAsociada != null;
					boolean existeCampoFijo = !StringUtil.isNullOrEmptyNumeric(objDet.getCampoFijo());
					CampoFijoType campoFijoType = CampoFijoType.get(objDet.getCampoFijo());
					if (campoFijoType == null) {
						existeCampoFijo = false;

					}
					if (existeCampoFijo) {
						campoFijoType = CampoFijoType.get(objDet.getCampoFijo());
						Object valor = null;
						switch (campoFijoType) {
						case NUMERO_RIESGO:
							BigDecimal numeroRiesgo = null;
							if (!secuenciaNumeroRiesgoMAp
									.containsKey(ConstanteConfiguracionTramaUtil.GENERA_SECUENCIA)) {
								numeroRiesgo = BigDecimal.ONE;
								secuenciaNumeroRiesgoMAp.put(ConstanteConfiguracionTramaUtil.GENERA_SECUENCIA,
										numeroRiesgo);
							} else {
								numeroRiesgo = (BigDecimal) secuenciaNumeroRiesgoMAp
										.get(ConstanteConfiguracionTramaUtil.GENERA_SECUENCIA);
								numeroRiesgo = numeroRiesgo.add(BigDecimal.ONE);
								secuenciaNumeroRiesgoMAp.put(ConstanteConfiguracionTramaUtil.GENERA_SECUENCIA,
										numeroRiesgo);
							}
							valor = numeroRiesgo;
							listKeyValueMap.put(mapValueKey, new ValueDataVO(valor));
							break;
						case NUMERO_SECUENCIAL:
							BigDecimal numeroSecuencial = null;
							if (!secuenciaNumeroSecuencialMAp.containsKey(objGrupoMap.getKey())) {
								numeroSecuencial = BigDecimal.ONE;
								secuenciaNumeroSecuencialMAp.put(objGrupoMap.getKey(), numeroSecuencial);
							} else {
								numeroSecuencial = (BigDecimal) secuenciaNumeroSecuencialMAp.get(objGrupoMap.getKey());
								numeroSecuencial = numeroSecuencial.add(BigDecimal.ONE);
								secuenciaNumeroSecuencialMAp.put(objGrupoMap.getKey(), numeroSecuencial);
							}
							valor = numeroSecuencial;
							listKeyValueMap.put(mapValueKey, new ValueDataVO(valor));
							break;
						default:
							break;
						}
					}
					if (existeCampoFijo) {
						campoFijoType = CampoFijoType.get(objDet.getCampoFijo());
						Object valor = null;
						switch (campoFijoType) {
						case NUMERO_LOTE:
							String keyNumeroLote = configuracionTrama.getJuegoTrama().getIdJuegoTrama() + "";
							if (!numeroLoteMap.containsKey(keyNumeroLote)) {
								RespuestaLecturaArchivoVO respuestaNumeroLote = obtenerNumeroLote(
										configuracionTrama.getJuegoTrama().getNombreFuncion(),
										configuracionTrama.getJuegoTrama().getProducto(), mapValueKey, nombreTabla,
										nomenclatura, fechaLote, numeroLote,
										configuracionTrama.getIdConfiguradorTrama(), parametroMap);
								if (!respuestaNumeroLote.isEsError()) {
									numeroLoteMap.put(keyNumeroLote, respuestaNumeroLote.getValor());
									valor = respuestaNumeroLote.getValor();
								} else {
									// procesamiento con error
									resultado.setErrorNumeroLote(respuestaNumeroLote.isExisteErrorNumLote());// ERROR
																												// NUMERO
																												// DE
																												// LOTE
																												// NO
																												// GENERO
									numeroLoteMap.put(keyNumeroLote, listKeyValueMap.get(mapValueKey));
									resultado.getListaLogConfiguracionTramaAyuda()
											.addAll(respuestaNumeroLote.getListaLogConfiguracionTramaAyuda());
									valor = "${ERROR}{PROCESADO}";
								}
							} else {
								valor = numeroLoteMap.get(keyNumeroLote);
							}
							ValueDataVO valueTrasfer = TransferDataUtil.obtenerValueParse(valor.toString(),
									campoMappingTypeMap.get(mapValueKey), objDet.getFormatoCampo(), 0, parametroMap);
							listKeyValueMap.put(mapValueKey, valueTrasfer);
							// resultadoCampoValue.setAtributeValue(valueTrasfer);

							break;
						case FECHA_LOTE:
							String keyFecha = configuracionTrama.getJuegoTrama().getIdJuegoTrama() + "";
							if (!fechaMap.containsKey(keyFecha)) {
								Date fechaLoteFormato = FechaUtil.obtenerFechaFormatoPersonalizado(FechaUtil
										.obtenerFechaFormatoPersonalizado(FechaUtil.obtenerFechaActual(), "yyy/MM/dd"),
										"yyy/MM/dd");
								fechaMap.put(keyFecha, fechaLoteFormato);
							}
							valor = fechaMap.get(keyFecha);
							listKeyValueMap.put(mapValueKey, new ValueDataVO(valor));
							// resultadoCampoValue.setAtributeValue(valor);
							break;
						// MEJORA 23/11/2017
						case CAMPO_ASOCIAR_CANAL:
							String canalKey = configuracionTrama.getJuegoTrama().getCanal();
							log.error("CAMPO_ASOCIAR_CANAL" + canalKey);
							valor = canalKey;
							listKeyValueMap.put(mapValueKey, new ValueDataVO(canalKey));
							break;
						// MEJORA 23/11/2017
						case CAMPO_UUID:
							String uuid = UUIDUtil.generarElementUUID();
							valor = uuid;
							listKeyValueMap.put(mapValueKey, new ValueDataVO(uuid));
							break;
						case CAMPO_ASOCIADO:
							// opteniendo valores de campo asociado
							if (isTramaAsociado && isLecturaTrama) {
								RespuestaLecturaArchivoVO listaDataResulAsociado = listaRespuestaLecturaArchivoTemporalMap
										.get(configuracionTramaAsociada.getIdConfiguradorTrama() + "");
								if (listaDataResulAsociado != null) {
									List<Map<String, ValueDataVO>> listaDataResulAsociadoMap = listaDataResulAsociado
											.getListaDataResulMap();
									if (listaDataResulAsociadoMap != null) {
										ValueDataVO valueCampoAsociado = obtenerValorCampoAsociado(
												listaDataResulAsociadoMap,
												objDet.getCampoAsociado().getNombeCampoTabla());
										listKeyValueMap.put(mapValueKey, valueCampoAsociado);
									}
								}
								// resultadoCampoValue.setAtributeValue(valueCampoAsociado);
							}
							break;
						case CAMPO_ASOCIADO_JOIN:
							// opteniendo valores de campo asociado
							if (isTramaAsociado && isLecturaTrama) {
								List<Map<String, ValueDataVO>> listaDataResulAsociadoMap = listaRespuestaLecturaArchivoTemporalMap
										.get(configuracionTramaAsociada.getIdConfiguradorTrama() + "")
										.getListaDataResulMap();
								ValueDataVO valueCampoAsociadoJoin = obtenerValorCampoAsociadoJoin(
										listaDataResulAsociadoMap, listKeyValueMap, objDet);
								listKeyValueMap.put(mapValueKey, valueCampoAsociadoJoin);
								// resultadoCampoValue.setAtributeValue(valueCampoAsociadoJoin);
							}
							break;
						default:
							break;
						}
					}
					// INICIO HOMOLOGACION
					// verificando programa homologacion
					if (existeTipoHomologacion) {
						if (!StringUtil.isNullOrEmptyNumeric(objDet.getTipoHomologacion())) {
							Object valor = null;
							TipoHomologacionType tipoHomologacionType = TipoHomologacionType
									.get(objDet.getTipoHomologacion());// TipoHomologacionType.get(configuracionTramaDetalle.getTipoHomologacion())
							String producto = configuracionTrama.getJuegoTrama().getProducto();
							String canal = configuracionTrama.getJuegoTrama().getCanal();
							// Inicio aqui se aumento ramo y polizagrupo
							String campoRamo = ConfiguracionCacheUtil.getInstance()
									.getPwrConfUtil("equivalencia.data.campo.ramo");
							String campopolizaGrupo = ConfiguracionCacheUtil.getInstance()
									.getPwrConfUtil("equivalencia.data.campo.poliza.grupo");
							String ramo = listKeyValueMap.containsKey(campoRamo) ? listKeyValueMap.get(campoRamo) + ""
									: null;
							String polizaGrupo = listKeyValueMap.containsKey(campopolizaGrupo)
									? listKeyValueMap.get(campopolizaGrupo) + ""
									: null;
							// Fin aqui se aumento ramo y polizagrupo
							String nombreCampo = objDet.getNombreCampo();
							String valorTrama = listKeyValueMap.get(mapValueKey) + "";
							switch (tipoHomologacionType) {
							case SIMPLE:
								if (StringUtil.isNullOrEmpty(objDet.getCampoDependiente()) || StringUtil
										.isNullOrEmpty(objDet.getCampoDependiente().getIdConfiguradorTramaDetalle())) {
									// Inicio aqui se aumento ramo y polizagrupo
									String key = generarKey(producto, canal, nombreCampo, valorTrama, ramo,
											polizaGrupo);
									// Fin aqui se aumento ramo y polizagrupo

									EquivalenciaDato equivalenciaDato = configuracionTramaEquivalenciaMapVO
											.getEquivalenciaHomologacionSimpleSinDependenciaMap().get(key);
									if (equivalenciaDato != null) {
										valor = equivalenciaDato.getValorTronador();
										configuracionDetalleEquivalenciaMap.put(objDet.getIdConfiguradorTramaDetalle(),
												equivalenciaDato);
									}
								}
								break;
							case COMPUESTO:
								if (!StringUtil.isNullOrEmpty(objDet.getCampoDependiente()) && !StringUtil
										.isNullOrEmpty(objDet.getCampoDependiente().getIdConfiguradorTramaDetalle())) {
									EquivalenciaDato equivalenciaDatoPadre = configuracionDetalleEquivalenciaMap
											.get(objDet.getCampoDependiente().getIdConfiguradorTramaDetalle());
									if (equivalenciaDatoPadre != null) {
										String key = generarKey(producto, canal, nombreCampo, valorTrama,
												equivalenciaDatoPadre.getIdEquivalencia() + "");
										EquivalenciaDato equivalenciaDatoDTO = configuracionTramaEquivalenciaMapVO
												.getEquivalenciaHomologacionSimpleConDependenciaMap().get(key);
										if (equivalenciaDatoDTO != null) {
											valor = equivalenciaDatoDTO.getValorTronador();
											configuracionDetalleEquivalenciaMap
													.put(objDet.getIdConfiguradorTramaDetalle(), equivalenciaDatoDTO);
										}
									}
								}
								break;
							case CALCULADO:
								if (!StringUtil.isNullOrEmpty(objDet.getCampoDependiente()) && !StringUtil
										.isNullOrEmpty(objDet.getCampoDependiente().getIdConfiguradorTramaDetalle())) {
									EquivalenciaDato equivalenciaDatoPadre = configuracionDetalleEquivalenciaMap
											.get(objDet.getCampoDependiente().getIdConfiguradorTramaDetalle());
									if (equivalenciaDatoPadre != null) {
										String key = generarKey(producto, canal, nombreCampo,
												equivalenciaDatoPadre.getIdEquivalencia() + "");
										EquivalenciaDato equivalenciaDatoDTO = configuracionTramaEquivalenciaMapVO
												.getEquivalenciaHomologacionSimpleConDependenciaMap().get(key);
										if (equivalenciaDatoDTO != null) {
											valor = equivalenciaDatoDTO.getValorTronador();
											configuracionDetalleEquivalenciaMap
													.put(objDet.getIdConfiguradorTramaDetalle(), equivalenciaDatoDTO);
										}
									}
								}
								break;
							default:
								break;
							}
							listKeyValueMap.put(mapValueKey, new ValueDataVO(valor));
						}
					}
					// FIN HOMOLOGACION
				}
				listaDataResulGrupoMap.add(listKeyValueMap);
			}
		}
		resultado.setListaAgrupadoDataResulMap(listaDataResulGrupoMap);
		listaDataResulMap = null;
		resultado.setListaDataResulMap(listaDataResulMap);
		grupoMap = null;
		configuracionDetalleEquivalenciaMap = null;
		return resultado;
	}

	protected List<Map<String, ValueDataVO>> calcularCampoFijo(
			ConfiguracionTramaIdProcesarVO configuracionTramaEquivalenciaMapVO, ConfiguracionTrama configuracionTrama,
			List<Map<String, ValueDataVO>> listaDataResulMap,
			Map<String, ConfiguracionTramaDetalle> configuracionTramaDetalleMap,
			Map<String, String> campoMappingTypeMap, Map<String, String> configuracionTramaErrorMap,
			Map<String, RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoTemporalMap, String nombreTabla,
			String nomenclatura, Date fechaLote, String numeroLote) throws Exception {
		for (Map<String, ValueDataVO> listKeyValueMap : listaDataResulMap) {
			for (Map.Entry<String, ValueDataVO> mapValue : listKeyValueMap.entrySet()) {
				ConfiguracionTramaDetalle objDet = configuracionTramaDetalleMap.get(mapValue.getKey());
				String mapValueKey = objDet.getNombeCampoTabla();
				ConfiguracionTrama configuracionTramaAsociada = getConfiguracionTramaAsociada(objDet);
				// verificando campo fijo
				boolean isLecturaTrama = RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNoLeidoTrama());
				boolean isTramaAsociado = configuracionTramaAsociada != null;
				boolean existeCampoFijo = !StringUtil.isNullOrEmptyNumeric(objDet.getCampoFijo());
				if (existeCampoFijo) {
					CampoFijoType campoFijoType = CampoFijoType.get(objDet.getCampoFijo());
					switch (campoFijoType) {
					case CAMPO_SUMA:
					case CAMPO_PROMEDIO:
					case CAMPO_CONTADOR:
						// opteniendo valores de campo asociado
						if (isTramaAsociado && isLecturaTrama) {
							// Si el la misma trama
							RespuestaLecturaArchivoVO listaDataResulAsociado = null;
							boolean isMismaTrama = configuracionTrama.getIdConfiguradorTrama()
									.equals(configuracionTramaAsociada.getIdConfiguradorTrama());
							if (isMismaTrama) {
								listaDataResulAsociado = new RespuestaLecturaArchivoVO();
								listaDataResulAsociado.setListaDataResulMap(listaDataResulMap);
							} else {
								listaDataResulAsociado = listaRespuestaLecturaArchivoTemporalMap
										.get(configuracionTramaAsociada.getIdConfiguradorTrama() + "");
							}
							if (listaDataResulAsociado != null) {
								List<Map<String, ValueDataVO>> listaDataResulAsociadoMap = listaDataResulAsociado
										.getListaDataResulMap();
								if (listaDataResulAsociadoMap != null) {
									ValueDataVO valueCampoAsociado = obtenerValorCampoCalculado(
											listaDataResulAsociadoMap, objDet.getCampoAsociado().getNombeCampoTabla(),
											campoFijoType);
									listKeyValueMap.put(mapValueKey, valueCampoAsociado);
								}
							}
						}
						break;
					default:
						break;
					}
				}
			}
		}
		return listaDataResulMap;
	}

	// OBTENER NUMERO DE LOTE MODIFICADO.
	protected RespuestaLecturaArchivoVO obtenerNumeroLote(String funncionLote, String codigoTratamiento, String campo,
			String nombreTabla, String nomenclatura, Date fechaLote, String numeroLote, Long idConfiguracionTrama,
			Map<String, Object> parametrosMap) {
		String rutaRelativaTemp = parametrosMap.get("rutaRelativaTemp") + "";
		RespuestaLecturaArchivoVO resultado = new RespuestaLecturaArchivoVO();
		resultado.setListaLogConfiguracionTramaAyuda(
				new BigMemoryManager<String, LogConfiguracionTrama>(rutaRelativaTemp, "agrupacion_data_log_ayuda"));
		resultado.setEsError(false);
		try {
			// EntityManager sessionHibernate = sessionFactory.createEntityManager();
			String resultadoTemp = configuracionTramaDao.obtenerNumeroLote(funncionLote, codigoTratamiento);
			resultado.setValor(resultadoTemp);
		} catch (Exception e) {
			log.error("Error ", e);
			boolean isPrintError = true;
			String mensajeError = "" + campo + "Error:Numero Lote " + e.getMessage();
			if (isPrintError) {
				String mensajeErrorMostrar = idConfiguracionTrama + "," + nombreTabla + " fechaLote = " + fechaLote
						+ "  nomenclatura (" + nomenclatura + ") -->" + mensajeError;
				LogConfiguracionTrama logConfiguracionTrama = generarLogConfiguracionTrama("", "",
						"Exception:Numero Lote", obtenerDescripcionError(mensajeErrorMostrar), "Exception Numero Lote",
						idConfiguracionTrama, ComponenteValidadoLogTramaType.CONF_TRAMA, TipoErrorLogTramaType.GENERAL);
				resultado.getListaLogConfiguracionTramaAyuda().add(logConfiguracionTrama);

			}
			resultado.setExisteErrorNumLote(true);
			resultado.setEsError(true);
			resultado.setEstadoProcesamientoConfiguracionTrama(
					EstadoProcesamientoConfiguracionTramaState.PROCESADO_CON_ERROR);
		}

		return resultado;
	}

	protected boolean isCampoNegocio(ConfiguracionTrama configuracionTrama) {
		boolean resultado = false;
		for (ConfiguracionTramaDetalle objDet : configuracionTrama
				.getConfiguracionTramaConfiguracionTramaDetalleList()) {
			if (RespuestaNaturalType.SI.getKey().equals(objDet.getFlagCampoNegocio())) {
				resultado = true;
				break;
			}
		}
		return resultado;
	}

	protected void registrarVariablesProceso(List<ResultadoProcesoConfiguracionTramaVO> resultado,
			String idControlProceso, Long indicadorInternoActividad, String userName) {
		for (ResultadoProcesoConfiguracionTramaVO objeto : resultado) {
			if (!objeto.isEsError()) {
				Map<String, Object> procesoVariableInstanciaMap = TransferDataUtil.toVOMap(objeto);
				String agrupador = UUIDUtil.generarElementUUID();
				for (Entry<String, Object> dataMap : procesoVariableInstanciaMap.entrySet()) {
					ProcesoVariableInstancia procesoVariableInstanciaEntity = new ProcesoVariableInstancia();
					ControlProceso controlProcesoEntity = new ControlProceso();
					controlProcesoEntity.setIdControlProceso(idControlProceso);
					procesoVariableInstanciaEntity.setCodigoVariableInstancia(UUIDUtil.generarElementUUID());
					procesoVariableInstanciaEntity.setControlProceso(controlProcesoEntity);
					procesoVariableInstanciaEntity.setAgrupadorInstancia(agrupador);
					procesoVariableInstanciaEntity.setCodigoActividadInterno(indicadorInternoActividad);
					procesoVariableInstanciaEntity.setKey(dataMap.getKey());
					procesoVariableInstanciaEntity.setValor(ObjectUtil.objectToString(dataMap.getValue()));
					procesoVariableInstanciaEntity.setFechaActualizacion(new Date());
					procesoVariableInstanciaEntity.setUsuarioCrea(userName);
					procesoVariableInstanciaDaoLocal.saveNative(procesoVariableInstanciaEntity);
				}
			}
		}
	}

	protected void registrarProcesoFlujoError(List<ResultadoProcesoConfiguracionTramaVO> resultado,
			String idControlProceso, Long indicadorInternoActividad, Map<String, Object> parametroMap) {
		// GUARDAR LOG DE ERRORES - NUEVO REQUERIMIENTO GUARDAR LA TIPIFICACION DEL
		// ERROR SIN TRAMA
		for (ResultadoProcesoConfiguracionTramaVO objDet : resultado) {
			if (objDet.isEsError()) {
				ProcesoFlujoError procesoFlujoErrorEntity = new ProcesoFlujoError();
				procesoFlujoErrorEntity.setIdError(UUIDUtil.generarElementUUID());
				procesoFlujoErrorEntity.setCodigoError(objDet.getCodigoError());
				procesoFlujoErrorEntity.setIdControlProceso(idControlProceso);
				procesoFlujoErrorEntity.setIdIndicadorInterno(indicadorInternoActividad);
				procesoFlujoErrorEntity.setMensajeError(StringUtil.obtenerCadenaMaxima(objDet.getMensajeError(),
						ConstanteTramaUtil.TAMANIO_MAXIMO_MENSAJE_TECNICO));
				procesoFlujoErrorEntity.setFechaOcurrioError(new Date());
				procesoFlujoErrorEntity.setFechaActualizacion(new Date());
				if (TipoErrorOrquestadorType.TECNICO.getKey().equals(objDet.getTipoError())) {
					procesoFlujoErrorEntity.setTipoError(objDet.getTipoError());
				} else {
					if ("H".equals(objDet.getTipoError())) { // NO HAY TRAMAS QUE PROCESAR EN EL FTP
						procesoFlujoErrorEntity.setTipoError(objDet.getTipoError());
					} else {
						if (!StringUtil.isNullOrEmpty(objDet.getCodigoError())
								&& objDet.getCodigoError().contains(ERROR_TECNICO)) {
							procesoFlujoErrorEntity.setTipoError(TipoErrorOrquestadorType.TECNICO.getKey());
						} else {
							procesoFlujoErrorEntity.setTipoError(TipoErrorOrquestadorType.SIN_TRAMA.getKey()); // APOYO
																												// ORQUESTADOR
						}
					}
				}
				procesoFlujoErrorEntity
						.setUsuarioCrea((String) parametroMap.get(ConstanteConfiguracionTramaUtil.USUARIO));
				procesoFlujoErrorAutomaticoDaoLocal.saveNative(procesoFlujoErrorEntity);
			}
		}
	}

	protected boolean registrarControlProcesoJuego(List<String> listaIdJuegoTrama, Map<String, Object> parametroMap)
			throws Exception {
		boolean resultado = true;
		try {
			if (!ResourceUtil.esSimulacion(parametroMap.get(ConstanteConfiguracionTramaUtil.ES_SIMULACION))) {
				controlProcesoJuegoDaoLocal.registrar(listaIdJuegoTrama, parametroMap);
			}
		} catch (Exception e) {
			log.error("Error ", e);
		}
		listaIdJuegoTrama = null;
		return resultado;
	}
}