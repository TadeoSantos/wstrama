package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.jms;


/**
 * La Class UUIDUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class UUIDUtil {

	/**
	 * Generar uuid.
	 *
	 * @return the string
	 */
	public static String generarUUID() {
		return java.util.UUID.randomUUID().toString();
	}
	
	/**
	 * Generar element uuid.
	 *
	 * @return the string
	 */
	public static String generarElementUUID() {
		return java.util.UUID.randomUUID().toString().replaceAll("-", "");
	}
}
