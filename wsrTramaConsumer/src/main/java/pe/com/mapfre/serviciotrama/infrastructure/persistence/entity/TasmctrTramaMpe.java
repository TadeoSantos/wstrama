package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class TasmctrTramaMpe.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "TASMCTRT_MPE", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class TasmctrTramaMpe implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El tasmctr trama mpePK. */
    @Id
    @EmbeddedId
    private  TasmctrTramaMpePK tasmctrTramaMpePK;
    /** El nombre trama input. */
    @Column(name = "NOM_TRAMA_INPUT" , length = 400)
    private String nombreTramaInput;
   
    /** El nro registros trama. */
    @Column(name = "NRO_REGISTROS_TRAMA" , length = 18)
    private BigDecimal nroRegistrosTrama;
   
    /** El nro altas trama. */
    @Column(name = "NRO_ALTAS_TRAMA" , length = 18)
    private BigDecimal nroAltasTrama;
   
    /** El nro bajas trama. */
    @Column(name = "NRO_BAJAS_TRAMA" , length = 18)
    private BigDecimal nroBajasTrama;
   
    /** El nro cambios trama. */
    @Column(name = "NRO_CAMBIOS_TRAMA" , length = 18)
    private BigDecimal nroCambiosTrama;
   
    /** El nro renovaciones trama. */
    @Column(name = "NRO_RENOVACIONES_TRAMA" , length = 18)
    private BigDecimal nroRenovacionesTrama;
   
    /** El fecha hora inicio carga. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_INIC_CARGA")
    private Date fechaHoraInicioCarga;
   
    /** El fecha hora fin carga. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_FIN_CARGA")
    private Date fechaHoraFinCarga;
   
    /** El nro registros leidos. */
    @Column(name = "NRO_REG_LEIDOS" , length = 18)
    private BigDecimal nroRegistrosLeidos;
   
    /** El nro registros error val. */
    @Column(name = "NRO_REG_ERROR_VAL" , length = 18)
    private BigDecimal nroRegistrosErrorVal;
   
    /** El mca transferido. */
    @Column(name = "MCA_TRANSFERIDO" , length = 1)
    private String mcaTransferido;
   
    /** El fecha hora inicio transf. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_INIC_TRANSF")
    private Date fechaHoraInicioTransf;
   
    /** El fecha hora fin transf. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_HORA_FIN_TRANSF")
    private Date fechaHoraFinTransf;
   
    /** El nro rehabilitados. */
    @Column(name = "NRO_REHABILITADOS" , precision = 6 , scale = 0)
    private BigDecimal nroRehabilitados;
    
    /** La id juego trama. */
    @Column(name = "N_ID_CONFIG_TRAMA" , length = 18)
    private Long idConfiguradorTrama;
   
    /**
     * Instancia un nuevo tasmctr trama mpe.
     */
    public TasmctrTramaMpe() {
    }
   
   
    /**
     * Instancia un nuevo tasmctr trama mpe.
     *
     * @param nombreTramaInput el nombre trama input
     * @param nroRegistrosTrama el nro registros trama
     * @param nroAltasTrama el nro altas trama
     * @param nroBajasTrama el nro bajas trama
     * @param nroCambiosTrama el nro cambios trama
     * @param nroRenovacionesTrama el nro renovaciones trama
     * @param fechaHoraInicioCarga el fecha hora inicio carga
     * @param fechaHoraFinCarga el fecha hora fin carga
     * @param nroRegistrosLeidos el nro registros leidos
     * @param nroRegistrosErrorVal el nro registros error val
     * @param mcaTransferido el mca transferido
     * @param fechaHoraInicioTransf el fecha hora inicio transf
     * @param fechaHoraFinTransf el fecha hora fin transf
     * @param nroRehabilitados el nro rehabilitados
     * @param idConfiguradorTrama el id juego trama
     */
    public TasmctrTramaMpe(String nombreTramaInput, BigDecimal nroRegistrosTrama, BigDecimal nroAltasTrama, BigDecimal nroBajasTrama, BigDecimal nroCambiosTrama, BigDecimal nroRenovacionesTrama, Date fechaHoraInicioCarga, Date fechaHoraFinCarga, BigDecimal nroRegistrosLeidos, BigDecimal nroRegistrosErrorVal, String mcaTransferido, Date fechaHoraInicioTransf, Date fechaHoraFinTransf, BigDecimal nroRehabilitados, Long idConfiguradorTrama) {
        super();
        this.nombreTramaInput = nombreTramaInput;
        this.nroRegistrosTrama = nroRegistrosTrama;
        this.nroAltasTrama = nroAltasTrama;
        this.nroBajasTrama = nroBajasTrama;
        this.nroCambiosTrama = nroCambiosTrama;
        this.nroRenovacionesTrama = nroRenovacionesTrama;
        this.fechaHoraInicioCarga = fechaHoraInicioCarga;
        this.fechaHoraFinCarga = fechaHoraFinCarga;
        this.nroRegistrosLeidos = nroRegistrosLeidos;
        this.nroRegistrosErrorVal = nroRegistrosErrorVal;
        this.mcaTransferido = mcaTransferido;
        this.fechaHoraInicioTransf = fechaHoraInicioTransf;
        this.fechaHoraFinTransf = fechaHoraFinTransf;
        this.nroRehabilitados = nroRehabilitados;
        this.idConfiguradorTrama = idConfiguradorTrama;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        TasmctrTramaMpe other = (TasmctrTramaMpe) obj;
        if (tasmctrTramaMpePK== null) {
            if (other.tasmctrTramaMpePK != null) {
                return false;
            }
        } else if (!tasmctrTramaMpePK.equals(other.tasmctrTramaMpePK)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "TasmctrTramaMpe [tasmctrTramaMpePK=" + tasmctrTramaMpePK + "]";
    }
   
}