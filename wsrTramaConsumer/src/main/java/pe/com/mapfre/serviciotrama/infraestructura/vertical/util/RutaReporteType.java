package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Enum RutaReporteType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum  RutaReporteType {

	/** EL JASPER. */
	JASPER("jasper","jasper"),
	/** EL img. */
	IMG("images","images"),
	/** LA CABECERA. */
	CABECERA("cabecera","cabecera");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, RutaReporteType> LOO_KUP_MAP = new HashMap<String, RutaReporteType>();

	static {
		for (RutaReporteType s : EnumSet.allOf(RutaReporteType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}	

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;

	
	/**
	 * Instancia un nuevo ruta reporte type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private RutaReporteType(String key,String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the ruta reporte type
	 */
	public static RutaReporteType get(String key) {
		return LOO_KUP_MAP.get(key);
	}
	
	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Establece el key.
	 *
	 * @param key el new key
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Establece el value.
	 *
	 * @param value el new value
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
