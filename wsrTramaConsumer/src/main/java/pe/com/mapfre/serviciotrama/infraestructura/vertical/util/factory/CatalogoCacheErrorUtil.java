/*
 * 
 */
package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.GenericJDBC;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ObjectUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.ScriptSqlResulJDBCVO;


public class CatalogoCacheErrorUtil {
	
	private static final Logger log = Logger.getLogger(CatalogoCacheErrorUtil.class);
	
	private static CatalogoCacheErrorUtil catalogoErrorUtil = null;
	
	private static Map<String, Long> cataloErrorMap = new HashMap<String, Long>();
	
	private boolean flagCargoListado = false;
	
	/**
	 * Instancia un nuevo administracion cache utl.
	 */
	public CatalogoCacheErrorUtil() {
		
	}
	

	public static CatalogoCacheErrorUtil getInstance() {
		if (catalogoErrorUtil == null) {
			createInstance();
		} else if (!catalogoErrorUtil.isFlagCargoListado()) {
			catalogoErrorUtil.sincronizarData();
		}
		return catalogoErrorUtil;
	}
	 
    private static synchronized void createInstance() {
    	if (catalogoErrorUtil == null) {
			catalogoErrorUtil = new CatalogoCacheErrorUtil();
			catalogoErrorUtil.sincronizarData();
		}
    }

	public  String sincronizarData() {
		return sincronizarProperties();
	}

	private synchronized String sincronizarProperties() {
		try {
			cataloErrorMap.putAll(obtenerDatosCatalogoError());
			flagCargoListado = true;
		} catch (Exception e) {
			flagCargoListado = false;
			return e.toString();
		}
		return null;
	}
	
	public Map<String, Long> obtenerDatosCatalogoError() throws Exception {
		log.error("CatalogoErrorUtil.CATALOGO_ERROR_DATA.Inicio ---> " + new Date());
		Map<String, Object> parametros = new HashMap<String, Object>();
		Map<String, Long> respuestaMap = new HashMap<String, Long>();
		Integer cantidadData = 1;
		Integer cantidadDataReal = 0;
		if (cantidadData > 0) {
			Integer startRow = 0;
			Integer offset = 0;
			Integer siguientesRegistros = 5000;
			while (cantidadData > 0) {
				StringBuilder sql = new StringBuilder();
				offset = (offset + siguientesRegistros);
				parametros.put("offset", offset);
				parametros.put("startRow", startRow);
				sql.append(SqlMapingUtil.obtenerSqlSentenciaTron200(ConstanteQueryTron2000Util.CATALOGO_ERROR_DATA));
				ScriptSqlResulJDBCVO objData = GenericJDBC.executeQuery(sql, parametros);
				cantidadData = objData.getListaData().size();
				log.error("CatalogoErrorUtil.CATALOGO_ERROR_DATA.proceso ---> " + objData.getListaData().size() + " " + new Date());
				for (Map<String, Object> dataMap : objData.getListaData()) {
					String catalogoKey = generarKey(dataMap, "C_NOM_CAMP", "C_TABLA_INTER", "C_TIPO_ERROR");
					if (!respuestaMap.containsKey(catalogoKey)) {
						Long codigoError = ObjectUtil.objectToLong(dataMap.get("NUM_COD_ERROR"));
						respuestaMap.put(catalogoKey, codigoError);
					}
				}
				cantidadDataReal = cantidadDataReal + cantidadData;
				startRow = offset;
				if (cantidadData < siguientesRegistros) {
					cantidadData = 0; 
				}
				objData = null;
			}
		}
		log.error("CatalogoErrorUtil.CATALOGO_ERROR_DATA.fin ---> " + new Date());
		return respuestaMap;
	}
	
	public String generarKey(Map<String, Object> dataMap, String... key) {
		return StringUtil.generarKey(dataMap, key);
	}
	
	public boolean isFlagCargoListado() {
		return flagCargoListado;
	}

	public static Map<String, Long> getCataloErrorMap() {
		return cataloErrorMap;
	}
}
