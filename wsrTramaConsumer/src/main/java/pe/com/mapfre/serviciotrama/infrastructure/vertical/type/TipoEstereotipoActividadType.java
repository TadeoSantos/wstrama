package pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class TipoEstereotipoActividadType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum TipoEstereotipoActividadType {

    /** El INICIO. */
 	INICIO(1L , "tipoEstereotipoActividad.inicio", "ui-diagram-element-start-per"),
	
    /** El FIN. */
 	FIN(2L , "tipoEstereotipoActividad.fin", "ui-diagram-element-end-per"),
	
    /** El ACTIVIDAD. */
 	ACTIVIDAD(3L , "tipoEstereotipoActividad.actividad", "" );
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<Long, TipoEstereotipoActividadType> LOO_KUP_MAP = new HashMap<Long, TipoEstereotipoActividadType>();
	
	static {
		for (TipoEstereotipoActividadType s : EnumSet.allOf(TipoEstereotipoActividadType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private Long key;
	
	/** El value. */
	private String value;
	
	private String style;

	/**
	 * Instancia un nuevo tipo estereotipo actividad type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private TipoEstereotipoActividadType(Long key, String value, String style) {
		this.key = key;
		this.value = value;
		this.style = style;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the tipo estereotipo actividad type
	 */
	public static TipoEstereotipoActividadType get(Long key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public Long getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	public String getStyle() {
		return style;
	}
}
