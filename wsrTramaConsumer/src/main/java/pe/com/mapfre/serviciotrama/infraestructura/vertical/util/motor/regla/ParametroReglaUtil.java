package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.motor.regla;

public class ParametroReglaUtil {
	
	
	public static final String ACRONIMO_REGLA_CONF_TRAMA_DETALLE = "cnfdet";
	public static final String ACRONIMO_REGLA_CONF_TRAMA_DETALLE_CARGA_DATA = "cnfdetCargaData";
	public static final String NOMBRE_ARCHIVO_FUNCION = "funcionesNegocio.txt";
	public static final String NOMBRE_ARCHIVO_IMPORTACIONES = "importaciones.txt";
	public static final String BASE_CONOCIMIENTO = "reglaconfiguradortramabase.dsl";
	public static final String BASE_CONOCIMIENTO_FUNCION_UNION = "reglaconfiguradortramabase.dslr";

}

 