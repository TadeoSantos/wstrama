package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory;

import java.io.Serializable;

/**
 * La Class SqlMapingUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class SqlMapingUtil implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Obtener sql sentencia tron200.
	 *
	 * @param nombreSQL el nombre sql
	 * @return the string
	 */
	public static String obtenerSqlSentenciaTron200(String nombreSQL) {
		return SqlMapingCacheUtil.getInstance().getSqlSentenciaTron2000Map().get(nombreSQL.trim());
	}
	
	public static String obtenerSqlSentenciaSystem(String nombreSQL) {
		return SqlMapingCacheUtil.getInstance().getSqlSentenciaSystemMap().get(nombreSQL.trim());
	}
	

}
