package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;
/**
 * La Class UsuarioPermiso.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */	
@Getter
@Setter
@Entity
@Table(name = "SGSM_USUARIO_EJECUCION", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class UsuarioPermiso implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id usuario. */
    @Id
    @Column(name = "N_ID_USUARIO" , length = 18)
    private Long idUsuario;
   
    /** El nombre usuario. */
    @Column(name = "C_USUARIO" , length = 100)
    private String nombreUsuario;
   
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
   
    /** El usuario permiso ejecucion manual list. */
    @OneToMany(mappedBy = "usuarioPermiso", fetch = FetchType.LAZY)
    private List<PermisoEjecucionManual> usuarioPermisoEjecucionManualList = new ArrayList<PermisoEjecucionManual>();
    
    /**
     * Instancia un nuevo usuario permiso.
     */
    public UsuarioPermiso() {
    }
   
   
    
    /**
     * @param idUsuario
     * @param nombreUsuario
     * @param usuarioCrea
     * @param fechaActualizacion
     */
    public UsuarioPermiso(Long idUsuario, String nombreUsuario, String usuarioCrea, Date fechaActualizacion) {
        super();
        this.idUsuario = idUsuario;
        this.nombreUsuario = nombreUsuario;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
       
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idUsuario == null) ? 0 : idUsuario.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        UsuarioPermiso other = (UsuarioPermiso) obj;
        if (idUsuario == null) {
            if (other.idUsuario != null) {
                return false;
            }
        } else if (!idUsuario.equals(other.idUsuario)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "UsuarioPermiso [idUsuario=" + idUsuario + "]";
    }
   
}