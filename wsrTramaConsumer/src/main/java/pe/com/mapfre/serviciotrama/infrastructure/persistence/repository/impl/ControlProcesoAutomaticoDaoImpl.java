package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteTramaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ControlProceso;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ControlProcesoAutomaticoDaoLocal;

/**
 * La Class ControlProcesoAutomaticoDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ControlProcesoAutomaticoDaoImpl extends  GenericRepository<String, ControlProceso> implements ControlProcesoAutomaticoDaoLocal  {

    @Override
    public String generarUUID() throws Exception {
    	String resultado = FechaUtil.obtenerFechaActualConcatenada() + ConstanteTramaUtil.EMPEZAR_CONCATENACION;
    	Map<String, Object> parametros = new HashMap<String, Object>();
    	StringBuilder jpaql = new StringBuilder("select max(o.idControlProceso) from ControlProceso o where 1 = 1 ");
    	jpaql.append(" and SUBSTR(o.idControlProceso, 0, 4) = :anho ");
    	jpaql.append(" and SUBSTR(o.idControlProceso, 5, 2) = :mes ");
    	jpaql.append(" and SUBSTR(o.idControlProceso, 7, 2) = :dia ");
    	parametros.put("anho", FechaUtil.obtenerFechaFormatoPersonalizado(new Date(), "yyyy"));
    	parametros.put("mes", FechaUtil.obtenerFechaFormatoPersonalizado(new Date(), "MM"));
    	parametros.put("dia", FechaUtil.obtenerFechaFormatoPersonalizado(new Date(), "dd"));
        Query query = createQuery(jpaql.toString(), parametros);
        List<Object> listLong =  query.getResultList();
        if (listLong != null && listLong.size() > 0 && listLong.get(0) != null)  {
            String ultimoIdGenerado = listLong.get(0).toString();
            if (!StringUtil.isNullOrEmpty(ultimoIdGenerado)) {
            	Long ultimo = Long.valueOf(ultimoIdGenerado.substring(8));
            	ultimo = ultimo + 1;
                resultado = FechaUtil.obtenerFechaActualConcatenada()  + StringUtil.completeLeft(ultimo, ConstanteTramaUtil.PARAMETRO_COMPLETAR, ConstanteTramaUtil.VECES_COMPLETAR) ;
            }
        }
    	return resultado;
    }    
}