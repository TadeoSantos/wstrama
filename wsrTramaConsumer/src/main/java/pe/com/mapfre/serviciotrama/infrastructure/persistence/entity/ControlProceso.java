package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ControlProceso.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "TASMPROC", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ControlProceso implements Serializable {
 
    /** La Constant serialVersionUID. */				
    private static final long serialVersionUID = 1L;
   
    /** El id control proceso. */
    @Id
    @Column(name = "NUM_PROC" , length = 32)
    private String idControlProceso;
   
    /** El id tipo proceso. */
    @Column(name = "TIP_PROC" , length = 18)
    private Long idTipoProceso;
   
    /** El id modo proceso. */
    @Column(name = "COD_MODO_PROC" , length = 18)
    private Long idModoProceso;
   
    /** El id origen proceso. */
    @Column(name = "COD_ORIGEN_PROC" , length = 5)
    private String idOrigenProceso;
   
    /** El usuario proceso. */
    @Column(name = "COD_USR_PROC" , length = 50)
    private String usuarioProceso;
   
    /** El fecha inicio. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_INI_PROC")
    private Date fechaInicio;
   
    /** El fecha fin. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_FIN_PROC")
    private Date fechaFin;
   
    /** El estado. */
    @Column(name = "COD_EST_PROC" , length = 2)
    private String estado;
   
    /** El codigo usuario. */
    @Column(name = "COD_USR" , length = 50)
    private String codigoUsuario;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "FEC_ACTU")
    private Date fechaActualizacion;
   
    /** El control proceso control proceso detalle list. */
    @OneToMany(mappedBy = "controlProceso", fetch = FetchType.LAZY)
    private List<ControlProcesoDetalle> controlProcesoControlProcesoDetalleList = new ArrayList<ControlProcesoDetalle>();
    
    /** El control proceso control proceso detalle list. */
    @OneToMany(mappedBy = "controlProceso", fetch = FetchType.LAZY)
    private List<ProcesoVariableInstancia> procesoVariableInstanciaList = new ArrayList<ProcesoVariableInstancia>();
    
    /**
     * Instancia un nuevo control proceso.
     */
    public ControlProceso() {
    }
   
   
    /**
     * Instancia un nuevo control proceso.
     *
     * @param idControlProceso el id control proceso
     * @param idTipoProceso el id tipo proceso
     * @param idModoProceso el id modo proceso
     * @param idOrigenProceso el id origen proceso
     * @param usuarioProceso el usuario proceso
     * @param fechaInicio el fecha inicio
     * @param fechaFin el fecha fin
     * @param estado el estado
     * @param codigoUsuario el codigo usuario
     * @param fechaActualizacion el fecha actualizacion
     */
    public ControlProceso(String idControlProceso, Long idTipoProceso, Long idModoProceso, String idOrigenProceso, String usuarioProceso, Date fechaInicio, Date fechaFin, String estado, String codigoUsuario, Date fechaActualizacion ) {
        super();
        this.idControlProceso = idControlProceso;
        this.idTipoProceso = idTipoProceso;
        this.idModoProceso = idModoProceso;
        this.idOrigenProceso = idOrigenProceso;
        this.usuarioProceso = usuarioProceso;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.estado = estado;
        this.codigoUsuario = codigoUsuario;
        this.fechaActualizacion = fechaActualizacion;
    }
   
	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idControlProceso == null) ? 0 : idControlProceso.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ControlProceso other = (ControlProceso) obj;
        if (idControlProceso == null) {
            if (other.idControlProceso != null) {
                return false;
            }
        } else if (!idControlProceso.equals(other.idControlProceso)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ControlProceso [idControlProceso=" + idControlProceso + "]";
    }
}
