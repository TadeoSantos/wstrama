package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class ValidadorVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ValidadorVO implements Serializable {
 
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La numero secuencial. */
	private BigDecimal numeroSecuencial;
	
	/** La tabla intermedia. */
	private String tablaIntermedia;
	
	/** La numero unico. */
	private String numeroUnico;
	
	/** La nombre campo. */
	private String nombreCampo;
	
	/** La numero fila. */
	private String numeroFila;
	
	/** La mensaje error. */
	private String mensajeError;
	
	/**
	 * Instancia un nuevo validador vo.
	 */
	public ValidadorVO() {
	}

}