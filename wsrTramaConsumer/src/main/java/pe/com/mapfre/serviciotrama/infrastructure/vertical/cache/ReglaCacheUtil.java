package pe.com.mapfre.serviciotrama.infrastructure.vertical.cache;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.drools.compiler.compiler.io.File;
import org.drools.compiler.compiler.io.memory.MemoryFileSystem;
import org.drools.compiler.kie.builder.impl.InternalKieModule;
import org.drools.compiler.kie.builder.impl.KieBuilderImpl;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.KieModule;
import org.kie.api.builder.ReleaseId;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.conf.EqualityBehaviorOption;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;

import pe.com.mapfre.serviciotrama.domain.service.IConfiguradorTramaService;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConstanteConfigUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.StringUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.motor.regla.ParametroReglaUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ConfiguracionTramaDetalle;

/**
 * La Class ReglaCacheUtil.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class ReglaCacheUtil {
	
	/** La log. */
	private final static Logger log = Logger.getLogger(ReglaCacheUtil.class);

	/** La Constante SEPARADOR_FILE. */
	public static final String SEPARADOR_FILE = ConstanteConfigUtil.SEPARADOR_FILE;
	
	/** La Constante RUTA_RECURSOS_REGLA. */
	public static final String RUTA_RECURSOS_REGLA = ConstanteConfigUtil.RUTA_RECURSOS_REGLA;
	
	/** La regla cache util. */
	private static ReglaCacheUtil reglaCacheUtil;

	/** El required map. */
	private static Map<String, KieContainer> reglaMap = new HashMap<String, KieContainer>();
	
	/** La regla dls map. */
	private static Map<String, String> reglaDlsMap = new HashMap<String, String>();
	
	/** La regla dls cantidad linea map. */
	private static Map<String, Integer> reglaDlsCantidadLineaMap = new HashMap<String, Integer>();
	
	/** La configurador trama service local. */
	private static IConfiguradorTramaService configuradorTramaServiceLocal = null;

	/**
	 * Obtiene la instancia de ReglaCacheUtil.
	 *
	 * @return instancia de ReglaCacheUtil
	 */
	public synchronized static ReglaCacheUtil getInstance() {
		if (configuradorTramaServiceLocal == null) {
			configuradorTramaServiceLocal = Referencia.getReference(IConfiguradorTramaService.class);
		}
		if (reglaCacheUtil == null) {
			reglaCacheUtil = new ReglaCacheUtil();
			reglaCacheUtil.init();
		}
		return reglaCacheUtil;
	}

	/**
	 * Actualizar regla cache.
	 *
	 * @param nombreRegla el nombre regla
	 * @param ruleBase el rule base
	 */
	public void actualizarReglaCache(String nombreRegla,KieBase ruleBase) {
		
	}

	/**
	 * Inits the.
	 *
	 * @return the string
	 */
	public synchronized String init() {
		String resultado = null;
		// aqui cargar la regla
		leerRegla(ParametroReglaUtil.NOMBRE_ARCHIVO_IMPORTACIONES, RUTA_RECURSOS_REGLA + generarRuta ("rule_base") + ParametroReglaUtil.NOMBRE_ARCHIVO_IMPORTACIONES);
		try {
			//Inicio BuildSoft
			reglaMap.clear();
			reglaMap = new HashMap<String, KieContainer>();
			//Fin BuildSoft
			if (configuradorTramaServiceLocal != null) {
				List<ConfiguracionTramaDetalle> listaReglaConfDetalle = configuradorTramaServiceLocal.obtenerReglasConfDetalle();
				if (listaReglaConfDetalle == null) {
					listaReglaConfDetalle = new ArrayList<ConfiguracionTramaDetalle>();
				}
				for (ConfiguracionTramaDetalle configuracionTramaDetalle : listaReglaConfDetalle) {
					parsearRegla(configuracionTramaDetalle.getReglaNegocio(), ParametroReglaUtil.ACRONIMO_REGLA_CONF_TRAMA_DETALLE + configuracionTramaDetalle.getIdConfiguradorTramaDetalle());
				}
			}
		} catch (Exception e) {
			log.error("Error ", e);
			resultado = e.getMessage();
		}
		return resultado;
	}
	
	/**
	 * Generar ruta.
	 *
	 * @param ruta el ruta
	 * @return the string
	 */
	public static String generarRuta(String... ruta) {
		StringBuilder resultado = new StringBuilder();
		for (String carpeta : ruta) {
			resultado.append(carpeta);
			resultado.append("/");
		}
		return resultado.toString();
	}
	/**
	 * Parsear regla.
	 *
	 * @param reglaPersonalizado el regla personalizado
	 * @param nombreRegla el nombre regla
	 */
	public synchronized static void parsearRegla(String reglaPersonalizado, String nombreRegla) {
		try {
			reglaPersonalizado = StringUtil.quitarCaracterExtranio(reglaPersonalizado,0);
			String dslrImport = reglaDlsMap.get(ParametroReglaUtil.NOMBRE_ARCHIVO_IMPORTACIONES);
			reglaPersonalizado = dslrImport + "\n" + reglaPersonalizado;

			KieServices ks = KieServices.Factory.get();
			ReleaseId releaseId = ks.newReleaseId("org.kie.mapfre.pwr", "rule-mapfre", "1.0-SNAPSHOT");
			KieModuleModel kieModuleModel = obtenerBaseKieModuleModel(ks);
			kieModuleModel = obtenerBaseKieModuleModelPersonalizado(kieModuleModel, nombreRegla);
			KieFileSystem kfs = ks.newKieFileSystem().generateAndWritePomXML(releaseId)
			.write(generarRuta("src","main","resources","KBase",nombreRegla,"org","mapfre","pkg") + nombreRegla + ".drl", reglaPersonalizado)
			.writeKModuleXML(kieModuleModel.toXML());
			ks.newKieBuilder(kfs).buildAll();
			InternalKieModule kieModule = (InternalKieModule) ks.getRepository().getKieModule(releaseId);
			byte[] jar = kieModule.getBytes();
			MemoryFileSystem mfs = MemoryFileSystem.readFromJar(jar);
			File file = mfs.getFile(KieBuilderImpl.getCompilationCachePath(releaseId, "kbase1"));
			file = mfs.getFile(KieBuilderImpl.getCompilationCachePath(releaseId, "kbase" + nombreRegla));
			Resource jarRes = ks.getResources().newByteArrayResource(jar);
			KieModule KieModule = ks.getRepository().addKieModule(jarRes);
			KieContainer kieContainer = ks.newKieContainer(KieModule.getReleaseId());
			// KieSession ksession = ks.newKieContainer(
			// KieModule.getReleaseId() ).newKieSession("KSession" +
			// nombreRegla);
			reglaMap.put("KSession" + nombreRegla, kieContainer);
		} catch (Exception e) {
			log.error("Error ", e);
		}

	}
	
	/**
	 * Obtener base kie module model.
	 *
	 * @param ks el ks
	 * @return the kie module model
	 */
	private static KieModuleModel obtenerBaseKieModuleModel(KieServices ks) {
		KieModuleModel kproj = ks.newKieModuleModel();
		kproj.newKieBaseModel("kbase1")
		.setEqualsBehavior(EqualityBehaviorOption.EQUALITY)
		.setEventProcessingMode(EventProcessingOption.STREAM)
		.addPackage("org.mapfre.pkg")
		.newKieSessionModel("KSessionBase");
	    return kproj;
	}
	
	/**
	 * Obtener base kie module model personalizado.
	 *
	 * @param kproj el kproj
	 * @param nombreRegla el nombre regla
	 * @return the kie module model
	 */
	private static KieModuleModel obtenerBaseKieModuleModelPersonalizado(KieModuleModel kproj, String nombreRegla) {
		kproj.removeKieBaseModel("kbase" + nombreRegla);
		kproj.newKieBaseModel("kbase" + nombreRegla)
		.setEqualsBehavior(EqualityBehaviorOption.EQUALITY)
		.setEventProcessingMode(EventProcessingOption.STREAM)
		.addPackage("org.mapfre.pkg")
		.addInclude("kbase1")
		.newKieSessionModel("KSession" + nombreRegla);
		return kproj;
	}
	
	/**
	 * Leer regla concatenar.
	 *
	 * @param pathFile el path file
	 * @return the string
	 */
	public static String leerReglaConcatenar(String... pathFile) {
		StringBuilder resultado = new StringBuilder();
		for (String key : pathFile) {
			if (reglaDlsMap.containsKey(key)) {
				resultado.append(reglaDlsMap.get(key) + "\n");
			}
		}
		return resultado.toString();
	}
	
	/**
	 * Leer regla cantidad linea.
	 *
	 * @param pathFile el path file
	 * @return the integer
	 */
	public static Integer leerReglaCantidadLinea(String... pathFile) {
		Integer resultado = 0;
		for (String key : pathFile) {
			if (reglaDlsCantidadLineaMap.containsKey(key)) {
				resultado = resultado + reglaDlsCantidadLineaMap.get(key);
			}
		}
		return resultado;
	}
	
	/**
	 * Leer regla.
	 *
	 * @param key el key
	 * @param pathFile el path file
	 */
	public void leerRegla(String key, String pathFile) {
		FileInputStream fis = null;
		BufferedReader bufferedReader = null;
		try {
			fis = new FileInputStream(pathFile);
			bufferedReader = new BufferedReader(new InputStreamReader(fis));
			Integer cantidadLinea = obenerCantidadLiena(bufferedReader);
			fis = new FileInputStream(pathFile);
			byte[] datosArchivo = datosArchivo = new byte[fis.available()];
			fis.read(datosArchivo);
			String regla = new String(datosArchivo);
			reglaDlsMap.put(key, regla);
			reglaDlsCantidadLineaMap.put(key, cantidadLinea);
		} catch (Exception e) {
			log.error("Error ", e);
		} finally {
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					log.error("Error ", e);
				}
			}
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (IOException e) {
					log.error("Error ", e);
				}
			}
		}
	}
	
	/**
	 * Obener cantidad liena.
	 *
	 * @param br el br
	 * @return the integer
	 * @throws Exception the exception
	 */
	public static Integer obenerCantidadLiena(BufferedReader br) throws Exception {
		int resultado = 0;
		try {
			String line = "";
			while ((line = br.readLine()) != null) {
				resultado++;
			}
		} catch (Exception e) {
			log.error("Error ", e);
		}
		return resultado;
	}	
	
	/**
	 * Obtiene regla map.
	 *
	 * @return regla map
	 */
	public Map<String, KieContainer> getReglaMap() {
		return reglaMap;
	}

	/**
	 * Obtiene regla dls map.
	 *
	 * @return regla dls map
	 */
	public Map<String, String> getReglaDlsMap() {
		return reglaDlsMap;
	}
	
	/**
	 * Obtiene regla dls cantidad linea map.
	 *
	 * @return regla dls cantidad linea map
	 */
	public static Map<String, Integer> getReglaDlsCantidadLineaMap() {
		return reglaDlsCantidadLineaMap;
	}
	
}