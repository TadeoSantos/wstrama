package pe.com.mapfre.serviciotrama.infrastructure.vertical.service;

public class ErrorDatosIngresadosIncorrectamente extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private final String codigoOperacion;

    public ErrorDatosIngresadosIncorrectamente(String exception, String codigoOperacion) {
        super(exception);
        this.codigoOperacion=codigoOperacion;
    }

    public String getCodigoOperacion() {
        return codigoOperacion;
    }
}
