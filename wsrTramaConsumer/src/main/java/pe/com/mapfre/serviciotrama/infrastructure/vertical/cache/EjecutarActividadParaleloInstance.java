/*
 * 
 */
package pe.com.mapfre.serviciotrama.infrastructure.vertical.cache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pe.com.mapfre.serviciotrama.domain.entity.vo.ConfiguracionTramaIdProcesarVO;


public class EjecutarActividadParaleloInstance {

	private static Map<String,Boolean> procesoHiloMap = new HashMap<String, Boolean>();
	private static Map<String,List<String>> actividadProcesoHiloMap = new HashMap<String, List<String>>();
	private static Map<String,Boolean> actividadHiloMap = new HashMap<String, Boolean>();
	//Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
	private static Map<String,ConfiguracionTramaIdProcesarVO> actividadHiloObjectMap = new HashMap<String, ConfiguracionTramaIdProcesarVO>();
	//Fin BuildSoft Mejora Orquestador flujo 11/09/2019
	private static Map<String,Map<String,Object>> actividadHiloVariableSalidaMap = new HashMap<String, Map<String,Object>>();
	private static Long procesoContadorHilosSP = 0L;
	
	private static EjecutarActividadParaleloInstance ejecutarActividadParaleloInstance = null;
	/**
	 * Instancia un nuevo administracion cache utl.
	 */
	public EjecutarActividadParaleloInstance() {
		
	}
	
	/**
	 * Instanciar.
	 *
	 * @return the configurador cache utl
	 */
	public static EjecutarActividadParaleloInstance getInstance() {
		if (ejecutarActividadParaleloInstance == null) {
			createInstance();
		} 
		return ejecutarActividadParaleloInstance;
	}
	 /**
     * Creates the instance.
     */
    private static synchronized void createInstance() {
    	if (ejecutarActividadParaleloInstance == null) {
			ejecutarActividadParaleloInstance = new EjecutarActividadParaleloInstance();
			ejecutarActividadParaleloInstance.sincronizarData();
		}
    }
    public static void put(String keyProcesoFlujo,String keyActividad) {
    	if (!actividadProcesoHiloMap.containsKey(keyProcesoFlujo)) {
    		List<String> value = new ArrayList<String>();
    		value.add(keyActividad);
			actividadProcesoHiloMap.put(keyProcesoFlujo, value );
    	} else {
    		List<String> value = actividadProcesoHiloMap.get(keyProcesoFlujo);
    		value.add(keyActividad);
			actividadProcesoHiloMap.put(keyProcesoFlujo, value );
			procesoHiloMap.put(keyProcesoFlujo, false);
    	}
    	
   		if (!actividadHiloMap.containsKey(keyActividad)) {
       		actividadHiloMap.put(keyActividad, false);
   		}
    	
    }
    public static void removeProcesoFlujo(String keyProcesoFlujo) {
    	if (actividadProcesoHiloMap.containsKey(keyProcesoFlujo)) {
    		List<String> value = actividadProcesoHiloMap.get(keyProcesoFlujo);
    		for (String key : value) {
    			actividadHiloMap.remove(key);
    			actividadHiloMap.remove(key);
    			actividadHiloVariableSalidaMap.remove(key);
			}
    	}
    	actividadProcesoHiloMap.remove(keyProcesoFlujo);
    	actividadHiloObjectMap.remove(keyProcesoFlujo);
    	procesoHiloMap.remove(keyProcesoFlujo);
    }
    
   
    public static  boolean terminoProceso(String idProceso) {
    	boolean resultado = true;
		try {
			while (procesoHiloMap.get(idProceso) == null || !procesoHiloMap.get(idProceso)) {//tiempo de espera de la actividad
				  resultado = false;	
				  Thread.sleep(5000);
			}
		} catch (Exception e) {
			resultado = true;
		}
		return resultado;
    }
  //Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
    public static   boolean terminoActividad(String keyProcesoFlujo,String keyActividad, int cantidadReintento) {
    	boolean resultado = true;
    	int contadorReintento = 0;
		try {
			String key = keyProcesoFlujo + keyActividad;
			while (actividadHiloObjectMap.get(key) == null) {//tiempo de espera de la actividad
				  resultado = false;
				  contadorReintento++;
				  if (cantidadReintento < contadorReintento) {
					  System.err.println("terminoActividad." + keyProcesoFlujo + ".cantidadReintento " + resultado +" ==> " + contadorReintento);
					  return resultado;
				  } else {
					  Thread.sleep(2000);//2 segundos
				  }
				 
			}
			resultado = true;
			System.err.println("terminoActividad." + keyProcesoFlujo + ".cantidadReintento " + resultado +" ==> " + contadorReintento);
		} catch (Exception e) {
			resultado = true;
		}
		return resultado;
    }
    public static  void  bloquearProceso(String keyProcesoFlujo) {
    	procesoHiloMap.put(keyProcesoFlujo + "BLOQUEO", true);
    }
    
    public static  boolean  isBloquearProceso(String keyProcesoFlujo) {
    	return procesoHiloMap.containsKey(keyProcesoFlujo + "BLOQUEO");
    }
    public static   boolean  removeBloquearProceso(String keyProcesoFlujo) {
    	return procesoHiloMap.remove(keyProcesoFlujo + "BLOQUEO");
    }
    
    public static synchronized void putActividad(String keyProcesoFlujo,String keyActividad, ConfiguracionTramaIdProcesarVO configuracionTramaIdProcesarVO) {
    	String key = keyProcesoFlujo + keyActividad;
   		if (!actividadHiloObjectMap.containsKey(key)) {
   			actividadHiloObjectMap.put(key, configuracionTramaIdProcesarVO);
   		}
    	
    }
  //Fin BuildSoft Mejora Orquestador flujo 11/09/2019
    public static synchronized void sumarHilos() {
    	procesoContadorHilosSP ++;
    }
    public static  synchronized void restarHilos() {
    	procesoContadorHilosSP --;
    }
    public static  boolean terminoEjecutarActividad(String... keys) {
    	boolean resultado = true;
    	for (String key : keys) {  //MARTIN: AGREGADO VALIDACION.
    		if(actividadHiloMap.get(key) == null || !actividadHiloMap.get(key)) {   
    			resultado = false;
    			break;
    		}
		}
    	return resultado;
    }
	
	/**
	 * Sincronizar data.
	 *
	 * @return the string
	 */
	public   String sincronizarData() {
		return sincronizarProperties();
	}

	
	
	/**
	 * Sincronizar properties.
	 *
	 * @return the string
	 */
	private String sincronizarProperties() {
		try {
			actividadHiloMap = new HashMap<String, Boolean>();
		} catch (Exception e) {
			return e.toString();
		}
		return null;
		
	}
	//Inicio BuildSoft Mejora Orquestador flujo 11/09/2019
	public static Map<String, ConfiguracionTramaIdProcesarVO> getActividadHiloObjectMap() {
		return actividadHiloObjectMap;
	}
	//Fin BuildSoft Mejora Orquestador flujo 11/09/2019
	public static Map<String, Boolean> getActividadHiloMap() {
		return actividadHiloMap;
	}
	public static Map<String, List<String>> getActividadProcesoHiloMap() {
		return actividadProcesoHiloMap;
	}

	public static Map<String, Map<String, Object>> getActividadHiloVariableSalidaMap() {
		return actividadHiloVariableSalidaMap;
	}

	public static Map<String, Boolean> getProcesoHiloMap() {
		return procesoHiloMap;
	}

	public static synchronized Long getProcesoContadorHilosSP() {
		return procesoContadorHilosSP;
	}
}
