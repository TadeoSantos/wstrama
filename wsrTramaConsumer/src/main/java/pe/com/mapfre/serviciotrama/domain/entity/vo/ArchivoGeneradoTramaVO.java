package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.BasePaginator;

/**
 * La Class ArchivoGeneradoTramaVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ArchivoGeneradoTramaVO extends BasePaginator implements Serializable  {
	
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = -8708309269033346947L;
	
	/** La tipo archivo. */
	private String tipoArchivo;
	
	/** La tipo archivo. */
	private Long tipoArchivoCodigo;
	
	/** La nombre archivo. */
	private String nombreArchivo;
	
	/**
	 * Instancia un nuevo archivo generado trama vo.
	 */
	public ArchivoGeneradoTramaVO() {
		super();
	}

	/**
	 * Instancia un nuevo archivo generado trama vo.
	 *
	 * @param tipoArchivo el tipo archivo
	 * @param tipoArchivoCodigo el tipo archivo codigo
	 * @param nombreArchivo el nombre archivo
	 * @param idJuegoTrama el id juego trama
	 */
	public ArchivoGeneradoTramaVO(String tipoArchivo, Long tipoArchivoCodigo, String nombreArchivo) {
		super();
		this.tipoArchivo = tipoArchivo;
		this.tipoArchivoCodigo = tipoArchivoCodigo;
		this.nombreArchivo = nombreArchivo;
	}

}	
