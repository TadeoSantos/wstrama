package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class EquivalenciaDato.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_EQU_DATOS", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class EquivalenciaDato implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id equivalencia. */
    @Id
    @Column(name = "N_ID_EQ" , length = 18)
    private Long idEquivalencia;
   
    /** El producto. */
    @Column(name = "C_PRODUCTO" , length = 100)
    private String producto;
   
    /** El canal. */
    @Column(name = "C_CANAL" , length = 100)
    private String canal;
   
    /** El poliza grupo. */
    @Column(name = "C_POL_GRUPO" , length = 50)
    private String polizaGrupo;
   
    /** El codigo campo. */
    @Column(name = "C_COD_CAMPO" , length = 50)
    private String codigoCampo;
   
    /** El valor origen. */
    @Column(name = "C_VAL_ORI" , length = 100)
    private String valorOrigen;
   
    /** El valor tronador. */
    @Column(name = "C_VAL_TRON" , length = 100)
    private String valorTronador;
   
    /** El estado. */
    @Column(name = "C_ESTADO" , length = 20)
    private String estado;
   
    /** El equivalencia dato. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "N_ID_PADRE", referencedColumnName = "N_ID_EQ")
    private EquivalenciaDato equivalenciaDato;
   
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El usuario crea. */
    @Column(name = "C_COD_RAMO" , length = 3)
    private String codigoRamo;

	/** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;

	@Column(name = "N_ID_TIPO_EQUIV" , length = 18)
    private Long idTipoEquivalencia;

	@Column(name = "N_ID_TIPO_PROCESO" , length = 18)
    private Long idTipoProceso;
    
    @Column(name = "C_NOMBRE_TABLA" , length = 50)
    private String nombreTabla;
   
    /** El equivalencia dato equivalencia dato list. */
    @OneToMany(mappedBy = "equivalenciaDato", fetch = FetchType.LAZY)
    private List<EquivalenciaDato> equivalenciaDatoEquivalenciaDatoList = new ArrayList<EquivalenciaDato>();
    
    /**
     * Instancia un nuevo equivalencia dato.
     */
    public EquivalenciaDato() {
    }
   
   
    /**
     * Instancia un nuevo equivalencia dato.
     *
     * @param idEquivalencia el id equivalencia
     * @param producto el producto
     * @param canal el canal
     * @param polizaGrupo el poliza grupo
     * @param codigoCampo el codigo campo
     * @param valorOrigen el valor origen
     * @param valorTronador el valor tronador
     * @param estado el estado
     * @param equivalenciaDato el equivalencia dato
     * @param usuarioCreacion el usuario creacion
     * @param fechaCreacion el fecha creacion
     * @param usuarioModifica el usuario modifica
     * @param fechaModifica el fecha modifica
     */
	public EquivalenciaDato(Long idEquivalencia, String producto, String canal, String polizaGrupo, String codigoCampo, String valorOrigen, String valorTronador, String estado, EquivalenciaDato equivalenciaDato,String usuarioCrea,  Date fechaActualizacion,
    						Long idTipoEquivalencia, Long idTipoProceso, String nombreTabla) {
        super();
        this.idEquivalencia = idEquivalencia;
        this.producto = producto;
        this.canal = canal;
        this.polizaGrupo = polizaGrupo;
        this.codigoCampo = codigoCampo;
        this.valorOrigen = valorOrigen;
        this.valorTronador = valorTronador;
        this.estado = estado;
        this.equivalenciaDato = equivalenciaDato;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
        this.idTipoEquivalencia =idTipoEquivalencia;
        this.idTipoProceso = idTipoProceso;
        this.nombreTabla = nombreTabla;
    }
   
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idEquivalencia == null) ? 0 : idEquivalencia.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        EquivalenciaDato other = (EquivalenciaDato) obj;
        if (idEquivalencia == null) {
            if (other.idEquivalencia != null) {
                return false;
            }
        } else if (!idEquivalencia.equals(other.idEquivalencia)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "EquivalenciaDato [idEquivalencia=" + idEquivalencia + "]";
    }
   
}