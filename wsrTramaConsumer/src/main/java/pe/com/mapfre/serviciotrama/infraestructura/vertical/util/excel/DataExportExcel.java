package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.excel;

import java.io.Serializable;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.FechaUtil;

/**
 * La Class DataExportExcel.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class DataExportExcel implements Serializable {
		
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1714685155450871397L;
	
	/** La Constante DD_MM_YYY. */
	private static final String DD_MM_YYYY = FechaUtil.DATE_DMY;

	/**
	 * Generar merge region border.
	 *
	 * @param range el range
	 * @param sheet el sheet
	 * @param workbook el workbook
	 */
	public static void generarMergeRegionBorder(CellRangeAddress range, Sheet sheet, Workbook workbook) {
		RegionUtil.setBorderBottom(CellStyle.BORDER_THIN, range, sheet, sheet.getWorkbook());
		RegionUtil.setBorderTop(CellStyle.BORDER_THIN, range, sheet, sheet.getWorkbook());
		RegionUtil.setBorderLeft(CellStyle.BORDER_THIN, range, sheet, sheet.getWorkbook());
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, range, sheet, sheet.getWorkbook());
		RegionUtil.setBorderRight(CellStyle.BORDER_THIN, range, sheet, sheet.getWorkbook());
	}
	
	/**
	 * Generar style data.
	 *
	 * @param workbook el workbook
	 * @return the cell style
	 */
	public static CellStyle generarStyleData(SXSSFWorkbook workbook) {
		CellStyle titleStyle = workbook.createCellStyle();
		titleStyle = generarStyleDataNormal(titleStyle);
		return titleStyle;
	}
	
	/**
	 * Generar style data normal.
	 *
	 * @param titleStyle el title style
	 * @return the cell style
	 */
	private static CellStyle generarStyleDataNormal(CellStyle titleStyle) {
		titleStyle.setBorderTop(CellStyle.BORDER_THIN);
		titleStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		titleStyle.setBorderRight(CellStyle.BORDER_THIN);
		titleStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
		titleStyle.setBorderBottom(CellStyle.BORDER_THIN);
		titleStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		titleStyle.setBorderLeft(CellStyle.BORDER_THIN);
		titleStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		return titleStyle;
	}

	
	/**
	 * Generar style date dmy.
	 *
	 * @param workbook el workbook
	 * @return the cell style
	 */
	public static CellStyle generarStyleDateDMY(SXSSFWorkbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle = generarStyleDate(cellDateStyle, format);
		return cellDateStyle;
	}
	
	/**
	 * Generar style date.
	 *
	 * @param cellDateStyle el cell date style
	 * @param format el format
	 * @return the cell style
	 */
	private static CellStyle generarStyleDate(CellStyle cellDateStyle,DataFormat format) {
		cellDateStyle.setDataFormat(format.getFormat(DD_MM_YYYY));
		return cellDateStyle;
	}
	
	/**
	 * Generar style number.
	 *
	 * @param workbook el workbook
	 * @return the cell style
	 */
	public static CellStyle generarStyleNumberDecimal(SXSSFWorkbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle = generarStyleNumberPersonalizadoDecimal(cellDateStyle, format);
		return cellDateStyle;
	}
	//Inicio BuildSoft 02/10/2019 reporte siniestro taller
	public static CellStyle generarStyleNumberDecimalSytle(SXSSFWorkbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle = generarStyleNumberPersonalizadoDecimalSytle(cellDateStyle, format);
		return cellDateStyle;
	}
	//Fin BuildSoft 02/10/2019 reporte siniestro taller
	/**
	 * Generar style number personalizado.
	 *
	 * @param cellNumberStyle el cell number style
	 * @param format el format
	 * @return the cell style
	 */
	private static CellStyle generarStyleNumberPersonalizadoDecimal(CellStyle cellNumberStyle, DataFormat format) {
		cellNumberStyle.setDataFormat(format.getFormat("#,##0.00"));
		return cellNumberStyle;
	}
	//Inicio BuildSoft 02/10/2019 reporte siniestro taller
	private static CellStyle generarStyleNumberPersonalizadoDecimalSytle(CellStyle cellNumberStyle, DataFormat format) {
		cellNumberStyle.setDataFormat(format.getFormat("###0.00"));
		return cellNumberStyle;
	}
	//Fin BuildSoft 02/10/2019 reporte siniestro taller
	/**
	 * Generar style number.
	 *
	 * @param workbook el workbook
	 * @return the cell style
	 */
	public static CellStyle generarStyleNumber(SXSSFWorkbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle = generarStyleNumberPersonalizado(cellDateStyle, format);
		return cellDateStyle;
	}
	//Inicio BuildSoft 02/10/2019 reporte siniestro taller
	public static CellStyle generarStyleNumberStyle(SXSSFWorkbook workbook) {
		DataFormat format = workbook.createDataFormat();
		CellStyle cellDateStyle = workbook.createCellStyle();
		cellDateStyle = generarStyleNumberPersonalizadoSytle(cellDateStyle, format);
		return cellDateStyle;
	}
	//Fin BuildSoft 02/10/2019 reporte siniestro taller
	/**
	 * Generar style number personalizado.
	 *
	 * @param cellNumberStyle el cell number style
	 * @param format el format
	 * @return the cell style
	 */
	private static CellStyle generarStyleNumberPersonalizado(CellStyle cellNumberStyle, DataFormat format) {
		cellNumberStyle.setDataFormat(format.getFormat("#,##0"));
		return cellNumberStyle;
	}
	//Inicio BuildSoft 02/10/2019 reporte siniestro taller
	private static CellStyle generarStyleNumberPersonalizadoSytle(CellStyle cellNumberStyle, DataFormat format) {
		cellNumberStyle.setDataFormat(format.getFormat("###0"));
		return cellNumberStyle;
	}
	//Fin BuildSoft 02/10/2019 reporte siniestro taller
}
