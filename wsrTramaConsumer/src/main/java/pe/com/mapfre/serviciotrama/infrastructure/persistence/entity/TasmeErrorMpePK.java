package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class TasmeErrorMpePK.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Embeddable
public class TasmeErrorMpePK implements Serializable {
 
	/** La Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** El fecha lote. */
	@Temporal( TemporalType.TIMESTAMP)
	@Column(name = "FEC_LOTE")
	private Date fechaLote;
	
	/** El numero lote. */
	@Column(name = "NUM_LOTE" , length = 10)
	private String numeroLote;
	
	/** El codigo cia. */
	@Column(name = "COD_CIA" , precision = 2 , scale = 0)
	private BigDecimal codigoCia;
	
	/** El numero poliza grupo. */
	@Column(name = "NUM_POLIZA_GRUPO" , length = 13)
	private String numeroPolizaGrupo;
	
	/** El numero unico item. */
	@Column(name = "NUM_UNICO_ITEM" , length = 20)
	private String numeroUnicoItem;
	
	/** El numero secuencia. */
	@Column(name = "NRO_SECUENCIA" , precision = 3 , scale = 0)
	private BigDecimal numeroSecuencia;
	
	/** El numero riesgo. */
	@Column(name = "NUM_RIESGO" , precision = 3 , scale = 0)
	private BigDecimal numeroRiesgo;
	
	
	/**
	 * Instancia un nuevo tasme error mpePK.
	 */
	public TasmeErrorMpePK() {
	}
	
	
	/**
	 * Instancia un nuevo tasme error mpePK.
	 *
	 * @param fechaLote el fecha lote
	 * @param numeroLote el numero lote
	 * @param codigoCia el codigo cia
	 * @param numeroPolizaGrupo el numero poliza grupo
	 * @param numeroUnicoItem el numero unico item
	 * @param numeroSecuencia el numero secuencia
	 * @param numeroRiesgo el numero riesgo
	 */
	public TasmeErrorMpePK(Date fechaLote, String numeroLote, BigDecimal codigoCia, String numeroPolizaGrupo, String numeroUnicoItem, BigDecimal numeroSecuencia, BigDecimal numeroRiesgo ) {
		super();
		this.fechaLote = fechaLote;
		this.numeroLote = numeroLote;
		this.codigoCia = codigoCia;
		this.numeroPolizaGrupo = numeroPolizaGrupo;
		this.numeroUnicoItem = numeroUnicoItem;
		this.numeroSecuencia = numeroSecuencia;
		this.numeroRiesgo = numeroRiesgo;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fechaLote == null) ? 0 : fechaLote.hashCode());
		result = prime * result
				+ ((numeroLote == null) ? 0 : numeroLote.hashCode());
		result = prime * result
				+ ((codigoCia == null) ? 0 : codigoCia.hashCode());
		result = prime * result
				+ ((numeroPolizaGrupo == null) ? 0 : numeroPolizaGrupo.hashCode());
		result = prime * result
				+ ((numeroUnicoItem == null) ? 0 : numeroUnicoItem.hashCode());
		result = prime * result
				+ ((numeroSecuencia == null) ? 0 : numeroSecuencia.hashCode());
		result = prime * result
				+ ((numeroRiesgo == null) ? 0 : numeroRiesgo.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		TasmeErrorMpePK other = (TasmeErrorMpePK) obj;
		if (fechaLote == null) {
			if (other.fechaLote != null) {
				return false;
			}
		} else if (!fechaLote.equals(other.fechaLote)) {
			return false;
		}
		if (numeroLote == null) {
			if (other.numeroLote != null) {
				return false;
			}
		} else if (!numeroLote.equals(other.numeroLote)) {
			return false;
		}
		if (codigoCia == null) {
			if (other.codigoCia != null) {
				return false;
			}
		} else if (!codigoCia.equals(other.codigoCia)) {
			return false;
		}
		if (numeroPolizaGrupo == null) {
			if (other.numeroPolizaGrupo != null) {
				return false;
			}
		} else if (!numeroPolizaGrupo.equals(other.numeroPolizaGrupo)) {
			return false;
		}
		if (numeroUnicoItem == null) {
			if (other.numeroUnicoItem != null) {
				return false;
			}
		} else if (!numeroUnicoItem.equals(other.numeroUnicoItem)) {
			return false;
		}
		if (numeroSecuencia == null) {
			if (other.numeroSecuencia != null) {
				return false;
			}
		} else if (!numeroSecuencia.equals(other.numeroSecuencia)) {
			return false;
		}
		if (numeroRiesgo == null) {
			if (other.numeroRiesgo != null) {
				return false;
			}
		} else if (!numeroRiesgo.equals(other.numeroRiesgo)) {
			return false;
		}
		return true;
	}
     
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TasmeErrorMpe [fechaLote= " + fechaLote + " ,numeroLote= " + numeroLote + " ,codigoCia= " + codigoCia + " ,numeroPolizaGrupo= " + numeroPolizaGrupo + " ,numeroUnicoItem= " + numeroUnicoItem + " ,numeroSecuencia= " + numeroSecuencia + " ,numeroRiesgo= " + numeroRiesgo + " ]";
	}
	
}