package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;

import javax.persistence.Transient;

/**
 * La Class BasePaginator.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class BasePaginator implements Serializable {
	
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La offset. */
	@Transient
	private int offset;
	
	/** La start row. */
	@Transient
	private int startRow;
	
	@Transient
	private boolean check;
	
	@Transient
	private String descripcionView;
	/**
	 * Establece el start row.
	 *
	 * @param startRow el new start row
	 */
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}

	/**
	 * Obtiene start row.
	 *
	 * @return start row
	 */
	public int getStartRow() {
		return startRow;
	}

	/**
	 * Establece el offset.
	 *
	 * @param offset el new offset
	 */
	public void setOffset(int offset) {
		this.offset = offset;
	}

	/**
	 * Obtiene offset.
	 *
	 * @return offset
	 */
	public int getOffset() {
		return offset;
	}

	public boolean isCheck() {
		return check;
	}

	public void setCheck(boolean check) {
		this.check = check;
	}

	public String getDescripcionView() {
		return descripcionView;
	}

	public void setDescripcionView(String descripcionView) {
		this.descripcionView = descripcionView;
	}
}
