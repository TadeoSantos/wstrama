package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;

/**
 * La Class ExcelHederTitleVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class ExcelHederTitleVO implements Serializable {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** El nameHeader. */
	private String nameHeader;

	/** El nameAtribute. */
	private short aling;

	/** La vertical alignment. */
	private short verticalAlignment;

	/** La posicion celda. */
	private Integer posicionCelda = -1;

	/** La posicion row. */
	private Integer posicionRow = -1;

	/** La cantidad agrupar. */
	private Integer cantidadAgrupar = 0;

	/** La cantidad agrupar horizontal. */
	private Integer cantidadAgruparHorizontal = 0;

	/** La font height in points. */
	private short fontHeightInPoints = 9;

	/** La column index. */
	private int columnIndex = -1;
	
	/** La width. */
	private int width = -1;
	
	/** La valor. */
	private String valor;

    /** The wrap text. */
    private boolean wrapText = false;
    
    /** The rotacion. */
    private int rotacion = 0;
    
    /** The es pie pagina. */
    private boolean esPiePagina = false;	
	
	/**
	 * Instancia un nuevo excel heder title vo.
	 */
	public ExcelHederTitleVO() {
		super();
	}

	/**
	 * Instancia un nuevo excel heder title vo.
	 *
	 * @param nameHeader el name header
	 * @param aling el aling
	 */
	public ExcelHederTitleVO(String nameHeader, short aling) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
	}

	// get y set

	/**
	 * Instancia un nuevo excel heder title vo.
	 *
	 * @param nameHeader el name header
	 * @param aling el aling
	 * @param posicionCelda el posicion celda
	 */
	public ExcelHederTitleVO(String nameHeader, short aling,Integer posicionCelda) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.posicionCelda = posicionCelda;
	}

	/**
	 * Instancia un nuevo excel heder title vo.
	 *
	 * @param nameHeader el name header
	 * @param aling el aling
	 * @param posicionCelda el posicion celda
	 * @param cantidadAgrupar el cantidad agrupar
	 */
	public ExcelHederTitleVO(String nameHeader, short aling,Integer posicionCelda, Integer cantidadAgrupar) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.posicionCelda = posicionCelda;
		this.cantidadAgrupar = cantidadAgrupar;
	}

	/**
	 * Instancia un nuevo excel heder title vo.
	 *
	 * @param nameHeader el name header
	 * @param aling el aling
	 * @param verticalAlignment el vertical alignment
	 * @param posicionCelda el posicion celda
	 * @param posicionRow el posicion row
	 * @param cantidadAgrupar el cantidad agrupar
	 * @param cantidadAgruparHorizontal el cantidad agrupar horizontal
	 * @param columnIndex el column index
	 * @param width el width
	 */
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment, Integer posicionCelda,Integer posicionRow, Integer cantidadAgrupar,Integer cantidadAgruparHorizontal, Integer columnIndex,Integer width) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.columnIndex = columnIndex;
		this.width = width;
	}

	/**
	 * Instancia un nuevo excel heder title vo.
	 *
	 * @param nameHeader el name header
	 * @param aling el aling
	 * @param verticalAlignment el vertical alignment
	 * @param posicionCelda el posicion celda
	 * @param posicionRow el posicion row
	 * @param cantidadAgrupar el cantidad agrupar
	 * @param cantidadAgruparHorizontal el cantidad agrupar horizontal
	 */
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment, Integer posicionCelda,Integer posicionRow, Integer cantidadAgrupar,Integer cantidadAgruparHorizontal) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
	}
	
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment, Integer posicionCelda,Integer posicionRow, Integer cantidadAgrupar,Integer cantidadAgruparHorizontal, String valor) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.valor = valor;
	}
	

	/**
	 * Instancia un nuevo excel heder title vo.
	 *
	 * @param nameHeader el name header
	 * @param aling el aling
	 * @param verticalAlignment el vertical alignment
	 * @param posicionCelda el posicion celda
	 * @param posicionRow el posicion row
	 * @param cantidadAgrupar el cantidad agrupar
	 * @param cantidadAgruparHorizontal el cantidad agrupar horizontal
	 * @param fontHeightInPoints el font height in points
	 * @param columnIndex el column index
	 * @param width el width
	 */
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment, Integer posicionCelda,Integer posicionRow, Integer cantidadAgrupar,Integer cantidadAgruparHorizontal, short fontHeightInPoints,Integer columnIndex, Integer width) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.fontHeightInPoints = fontHeightInPoints;
		this.columnIndex = columnIndex;
		this.width = width;
	}

	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment,
			Integer posicionCelda, Integer posicionRow, Integer cantidadAgrupar, Integer cantidadAgruparHorizontal, Integer columnIndex, Integer width,boolean wrapText,int rotacion,short fontHeightInPoints ) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.columnIndex = columnIndex;
		this.width = width;
		this.wrapText = wrapText;
		this.rotacion = rotacion;
		this.fontHeightInPoints = fontHeightInPoints ;
	}
	
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment,
			Integer posicionCelda, Integer posicionRow, Integer cantidadAgrupar, Integer cantidadAgruparHorizontal, Integer columnIndex, Integer width,boolean wrapText ) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.columnIndex = columnIndex;
		this.width = width;
		this.wrapText = wrapText;
	}
	
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment,
			Integer posicionCelda, Integer posicionRow, Integer cantidadAgrupar, Integer cantidadAgruparHorizontal, Integer columnIndex, Integer width,boolean wrapText, boolean esPiePagina ) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.columnIndex = columnIndex;
		this.width = width;
		this.wrapText = wrapText;
		this.esPiePagina = esPiePagina;
	}
	
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment,
			Integer posicionCelda, Integer posicionRow, Integer cantidadAgrupar, Integer cantidadAgruparHorizontal,boolean wrapText) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.wrapText = wrapText;
	}
	

//	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment,
//			Integer posicionCelda, Integer posicionRow, Integer cantidadAgrupar, Integer cantidadAgruparHorizontal, short fontHeightInPoints,Integer columnIndex, Integer width) {
//		super();
//		this.nameHeader = nameHeader;
//		this.aling = aling;
//		this.verticalAlignment = verticalAlignment;
//		this.posicionCelda = posicionCelda;
//		this.posicionRow = posicionRow;
//		this.cantidadAgrupar = cantidadAgrupar;
//		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
//		this.fontHeightInPoints = fontHeightInPoints;
//		this.columnIndex = columnIndex;
//		this.width = width;
//	}
	
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment,
			Integer posicionCelda, Integer posicionRow, Integer cantidadAgrupar, Integer cantidadAgruparHorizontal, short fontHeightInPoints,boolean wrapText) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.fontHeightInPoints = fontHeightInPoints;
		this.wrapText = wrapText;
	}	
	
	/**
	 * Instancia un nuevo excel heder title vo.
	 *
	 * @param nameHeader el name header
	 * @param aling el aling
	 * @param verticalAlignment el vertical alignment
	 * @param posicionCelda el posicion celda
	 * @param posicionRow el posicion row
	 * @param cantidadAgrupar el cantidad agrupar
	 * @param cantidadAgruparHorizontal el cantidad agrupar horizontal
	 * @param fontHeightInPoints el font height in points
	 */
	public ExcelHederTitleVO(String nameHeader, short aling,short verticalAlignment, Integer posicionCelda,Integer posicionRow, Integer cantidadAgrupar,Integer cantidadAgruparHorizontal, short fontHeightInPoints) {
		super();
		this.nameHeader = nameHeader;
		this.aling = aling;
		this.verticalAlignment = verticalAlignment;
		this.posicionCelda = posicionCelda;
		this.posicionRow = posicionRow;
		this.cantidadAgrupar = cantidadAgrupar;
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
		this.fontHeightInPoints = fontHeightInPoints;
	}
	/**
	 * Obtiene name header.
	 *
	 * @return name header
	 */
	public String getNameHeader() {
		return nameHeader;
	}
	/**
	 * Establece el name header.
	 *
	 * @param nameHeader el new name header
	 */
	public void setNameHeader(String nameHeader) {
		this.nameHeader = nameHeader;
	}
	/**
	 * Obtiene aling.
	 *
	 * @return aling
	 */
	public short getAling() {
		return aling;
	}
	/**
	 * Establece el aling.
	 *
	 * @param aling el new aling
	 */
	public void setAling(short aling) {
		this.aling = aling;
	}
	/**
	 * Obtiene vertical alignment.
	 *
	 * @return vertical alignment
	 */
	public short getVerticalAlignment() {
		return verticalAlignment;
	}
	/**
	 * Establece el vertical alignment.
	 *
	 * @param verticalAlignment el new vertical alignment
	 */
	public void setVerticalAlignment(short verticalAlignment) {
		this.verticalAlignment = verticalAlignment;
	}
	/**
	 * Obtiene posicion celda.
	 *
	 * @return posicion celda
	 */
	public Integer getPosicionCelda() {
		return posicionCelda;
	}
	/**
	 * Establece el posicion celda.
	 *
	 * @param posicionCelda el new posicion celda
	 */
	public void setPosicionCelda(Integer posicionCelda) {
		this.posicionCelda = posicionCelda;
	}
	/**
	 * Obtiene posicion row.
	 *
	 * @return posicion row
	 */
	public Integer getPosicionRow() {
		return posicionRow;
	}
	/**
	 * Establece el posicion row.
	 *
	 * @param posicionRow el new posicion row
	 */
	public void setPosicionRow(Integer posicionRow) {
		this.posicionRow = posicionRow;
	}
	/**
	 * Obtiene cantidad agrupar.
	 *
	 * @return cantidad agrupar
	 */
	public Integer getCantidadAgrupar() {
		return cantidadAgrupar;
	}
	/**
	 * Establece el cantidad agrupar.
	 *
	 * @param cantidadAgrupar el new cantidad agrupar
	 */
	public void setCantidadAgrupar(Integer cantidadAgrupar) {
		this.cantidadAgrupar = cantidadAgrupar;
	}
	/**
	 * Obtiene cantidad agrupar horizontal.
	 *
	 * @return cantidad agrupar horizontal
	 */
	public Integer getCantidadAgruparHorizontal() {
		return cantidadAgruparHorizontal;
	}
	/**
	 * Establece el cantidad agrupar horizontal.
	 *
	 * @param cantidadAgruparHorizontal el new cantidad agrupar horizontal
	 */
	public void setCantidadAgruparHorizontal(Integer cantidadAgruparHorizontal) {
		this.cantidadAgruparHorizontal = cantidadAgruparHorizontal;
	}
	/**
	 * Obtiene font height in points.
	 *
	 * @return font height in points
	 */
	public short getFontHeightInPoints() {
		return fontHeightInPoints;
	}
	/**
	 * Establece el font height in points.
	 *
	 * @param fontHeightInPoints el new font height in points
	 */
	public void setFontHeightInPoints(short fontHeightInPoints) {
		this.fontHeightInPoints = fontHeightInPoints;
	}
	/**
	 * Obtiene column index.
	 *
	 * @return column index
	 */
	public int getColumnIndex() {
		return columnIndex;
	}
	/**
	 * Establece el column index.
	 *
	 * @param columnIndex el new column index
	 */
	public void setColumnIndex(int columnIndex) {
		this.columnIndex = columnIndex;
	}
	/**
	 * Obtiene width.
	 *
	 * @return width
	 */
	public int getWidth() {
		return width;
	}
	/**
	 * Establece el width.
	 *
	 * @param width el new width
	 */
	public void setWidth(int width) {
		this.width = width;
	}
	/**
	 * Obtiene valor.
	 *
	 * @return valor
	 */
	public String getValor() {
		return valor;
	}
	/**
	 * Establece el valor.
	 *
	 * @param valor el new valor
	 */
	public void setValor(String valor) {
		this.valor = valor;
	}

	/**
	 * Checks if is wrap text.
	 *
	 * @return true, if is wrap text
	 */
	public boolean isWrapText() {
		return wrapText;
	}

	/**
	 * Sets the wrap text.
	 *
	 * @param wrapText the new wrap text
	 */
	public void setWrapText(boolean wrapText) {
		this.wrapText = wrapText;
	}

	/**
	 * Gets the rotacion.
	 *
	 * @return the rotacion
	 */
	public int getRotacion() {
		return rotacion;
	}

	/**
	 * Sets the rotacion.
	 *
	 * @param rotacion the new rotacion
	 */
	public void setRotacion(int rotacion) {
		this.rotacion = rotacion;
	}

	/**
	 * Checks if is es pie pagina.
	 *
	 * @return true, if is es pie pagina
	 */
	public boolean isEsPiePagina() {
		return esPiePagina;
	}

	/**
	 * Sets the es pie pagina.
	 *
	 * @param esPiePagina the new es pie pagina
	 */
	public void setEsPiePagina(boolean esPiePagina) {
		this.esPiePagina = esPiePagina;
	}
}