package pe.com.mapfre.serviciotrama.infrastructure.vertical.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

/**
 * La Class CabeceraReporteVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class CabeceraReporteVO extends BasePaginator implements Serializable {
 
	/** La Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** El usuario. */
	private String usuario;
	
	/** El usuario creacion. */
	private Date fechaGeneracion;
	
	/** El titulo. */
	private String titulo;

	/** El piePagina. */
	private String piePagina;
	
	private String rangoFechaTitle;
	
	private boolean isOnline = false;
	
	private Long idSolicitudReporte;
	
	/** El usuario. */
	private Long idUsuario;
	private String correoCorporativo;
	private String nombreCompleto;
	
	/** El opcion. */
	private Long menu;
	private Long codigoCola;
	
	private String archivoAdjunto;
	private String codigoArchivoAdjunto;
	private boolean archivoAdjuntoPersist;
	
    /**
	 * Instancia un nuevo cabecera reporte.
	 */
	public CabeceraReporteVO() {
	}

	/**
	 * Instancia un nuevo cabecera reporte vo.
	 *
	 * @param usuario el usuario
	 * @param fechaGeneracion el fecha generacion
	 * @param titulo el titulo
	 */
	public CabeceraReporteVO(String usuario, Date fechaGeneracion, String titulo) {
		super();
		this.usuario = usuario;
		this.fechaGeneracion = fechaGeneracion;
		this.titulo = titulo;
	}
	public boolean isOnline() {
		return isOnline;
	}

	public boolean isArchivoAdjuntoPersist() {
		return archivoAdjuntoPersist;
	}
	
}