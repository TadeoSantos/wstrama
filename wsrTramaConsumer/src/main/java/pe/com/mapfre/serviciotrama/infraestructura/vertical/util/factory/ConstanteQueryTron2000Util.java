package pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory;

/**
 * La Class ConstanteQueryTron2000Util.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public final class ConstanteQueryTron2000Util {

	
	/** La Constante MENSAJE_BUSQUEDA. */
	public static final String AVANCE_CORREDEODR_OBTENER_DETALLE_AVANCE_CORREDOR = "obtenerDetalleAvanceCorredor";
	
	/** La Constante CONTROL_DIARIO_EMISION_MASIVA_POLIZA_EMITIDA. */
	public static final String CONTROL_DIARIO_EMISION_MASIVA_POLIZA_EMITIDA = "obtenerPolizaEmitida";
	
	/** La Constante CONTROL_DIARIO_EMISION_MASIVA_POLIZA_SOLICITUD_ERRADA. */
	public static final String CONTROL_DIARIO_EMISION_MASIVA_POLIZA_SOLICITUD_ERRADA = "obtenerPolizaSolicitudErrada";
	
	//Reporte Ranking Decesos
	/** La Constante RANKING_DECESO_OBTENER_DETALLE_RANKING. */
	public static final String RANKING_DECESO_OBTENER_DETALLE_RANKING = "obtenerRankingDecesos";
	
	/** La Constante RANKING_DECESO_OBTENER_DETALLE_RANKING_HISTORICO. */
	public static final String RANKING_DECESO_OBTENER_DETALLE_RANKING_HISTORICO = "obtenerRankingDecesosHistorico";
	
	//Reporte Estado Cuenta Cliente
	/** La Constante ESTADO_CUENTA_CLIENTE_OBTENER_DETALLE_CUENTA_CLIENTE. */
	public static final String ESTADO_CUENTA_CLIENTE_OBTENER_DETALLE_CUENTA_CLIENTE = "obtenerDetalleEstadoCuentaCliente";
	
	/** La Constante ESTADO_CUENTA_CLIENTE_OBTENER_DETALLE_ANTICIPIO_CUENTA_CLIENTE. */
	public static final String ESTADO_CUENTA_CLIENTE_OBTENER_DETALLE_ANTICIPIO_CUENTA_CLIENTE = "obtenerDetalleAnticipioEstadoCuentaCliente";
	
	/** La Constante ESTADO_CUENTA_CLIENTE_OBTENER_DETALLE_ANULACIONES_CUENTA_CLIENTE. */
	public static final String ESTADO_CUENTA_CLIENTE_OBTENER_DETALLE_ANULACIONES_CUENTA_CLIENTE = "obtenerDetalleAnulacionesEstadoCuentaCliente";
	
	//Reporte Poliza Automovil
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_DETALLE_POLIZA_AUTOMOVIL. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_DETALLE_POLIZA_AUTOMOVIL = "obtenerDetallePolizaAutomovil";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_DETALLE_POLIZA_AUTOMOVIL_COUNT. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_DETALLE_POLIZA_AUTOMOVIL_COUNT = "obtenerDetallePolizaAutomovilCount";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_TIPO_SITUACION_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_TIPO_SITUACION_MAP = "obtenerTipoSituacionMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_PRIMAS_EMISION_SPTO_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_PRIMAS_EMISION_SPTO_MAP = "obtenerPrimaEmisionSptoMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_OFICINA_POLIZA_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_OFICINA_POLIZA_MAP = "obtenerOficinaEmitePolizaMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_APLICACION_PAGO_PRIMER_RECIBO_SPTO_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_APLICACION_PAGO_PRIMER_RECIBO_SPTO_MAP = "obtenerAplicacionPagoPrimerReciboSptoMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_RIESGO_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_RIESGO_MAP = "obtenerRiesgoMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_PRIMA_RIESGO_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_PRIMA_RIESGO_MAP = "obtenerPrimaRiesgoMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_FECHA_EFECRIGESGO_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_FECHA_EFECRIGESGO_MAP = "obtenerFechaEfecRiesgoMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_VIGENCIA_ANT_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_VIGENCIA_ANT_MAP = "obtenerVigenciaAntMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_FECHA_EFEC_POLIZA_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_FECHA_EFEC_POLIZA_MAP = "obtenerFechaEfecPolizaMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_COSTE_FECHA_CON_RIESGO_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_COSTE_FECHA_CON_RIESGO_MAP = "obtenerCosteFechaConRiesgoMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_COSTE_FECHA_SIN_RIESGO_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_COSTE_FECHA_SIN_RIESGO_MAP  = "obtenerCosteFechaSinRiesgoMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_VALOR_CAMBIO_MOMEDA_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_VALOR_CAMBIO_MOMEDA_MAP = "obtenerValorCambioMonedaMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_TIPO_NUMERO_DOCUMENTO_IDENTIDAD_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_TIPO_NUMERO_DOCUMENTO_IDENTIDAD_MAP = "obtenerTipoNumeroDocumentoIdentidadMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_TIPO_VALOR_VARIABLE_POLIZA_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_TIPO_VALOR_VARIABLE_POLIZA_MAP = "obtenerValorTipoVariablePolizaMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_TIPO_VALOR_VARIABLE_POLIZA_OTROS_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_TIPO_VALOR_VARIABLE_POLIZA_OTROS_MAP = "obtenerValorTipoVariablePolizaOtrosMap";
	//INICIO EDI26092018
	/** La Constante POLIZA_ELECTRONICA_OBTENER_TIPO_VALOR_VARIABLE_POLIZA_MAP. */
	public static final String POLIZA_ELECTRONICA_OBTENER_TIPO_VALOR_VARIABLE_POLIZA_MAP ="obtenerValorTipoVariablePolizaElectronicaMap";
	//FIN EDI26092018
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_TIPO_VALOR_VARIABLE_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_TIPO_VALOR_VARIABLE_MAP = "obtenerValorTipoVariableMap";
	
	/** La Constante POLIZA_AUTOMOVIL_OBTENER_NOMBRE_OFICINA_MAP. */
	public static final String POLIZA_AUTOMOVIL_OBTENER_NOMBRE_OFICINA_MAP = "obtenerOficinaMap";
	
	//Reporte Estado Cuenta Agente
	/** La Constante ESTADO_CUENTA_AGENTE_OBTENER_DETALLE_CUENTA_AGENTE. */
	public static final String ESTADO_CUENTA_AGENTE_OBTENER_DETALLE_CUENTA_AGENTE = "obtenerDetalleEstadoCuentaAgente";
	
	/** La Constante ESTADO_CUENTA_AGENTE_OBTENER_DETALLE_ANTICIPIO_CUENTA_AGENTE. */
	public static final String ESTADO_CUENTA_AGENTE_OBTENER_DETALLE_ANTICIPIO_CUENTA_AGENTE = "obtenerDetalleAnticipioEstadoCuentaAgente";
	
	//Reporte Estado cuenta Historico
	/** La Constante ESTADO_CUENTA_HISTORICO_OBTENER_DETALLE_ESTADO_CUENTA_HISTORICO. */
	public static final String ESTADO_CUENTA_HISTORICO_OBTENER_DETALLE_ESTADO_CUENTA_HISTORICO = "obtenerDetalleEstadoCuentaHistorico";
	
	/** La Constante ESTADO_CUENTA_HISTORICO_OBTENER_DETALLE_POLIZA_DOCUMENTO. */
	public static final String ESTADO_CUENTA_HISTORICO_OBTENER_DETALLE_POLIZA_DOCUMENTO = "obtenerPolizaDocumento";
	
	//Reporte Detalle Produccion
	/** La Constante DETALLE_PRODUCCION_OBTENER_DETALLE_REGISTRO_PRODUCCION. */
	public static final String DETALLE_PRODUCCION_OBTENER_DETALLE_REGISTRO_PRODUCCION = "obtenerRegistroProduccion";
	
	/** La Constante DETALLE_PRODUCCION_OBTENER_DETALLE_REGISTRO_PRODUCCION_COMPLETAR_SUM. */
	public static final String DETALLE_PRODUCCION_OBTENER_DETALLE_REGISTRO_PRODUCCION_COMPLETAR_SUM = "obtenerRegistroProduccionCompletarSum";
	
	//Asegurado Certificado 
	/** La Constante ASEGURADO_CERTIFICADO_OBTENER_DETALLE_ASEGURADO_CERTIFICADO. */
	public static final String ASEGURADO_CERTIFICADO_OBTENER_DETALLE_ASEGURADO_CERTIFICADO = "obtenerDetalleAseguradoCertificado";
	
	//Duplicado de Liquidación
	/** La Constante DUPLICADO_LIQUIDACION_OBTENER_DETALLE_DUPLICADO_LIQUIDACION. */
	public static final String DUPLICADO_LIQUIDACION_OBTENER_DETALLE_DUPLICADO_LIQUIDACION = "obtenerDetalleDuplicadoLiquidacion";
	
	/** La Constante DUPLICADO_LIQUIDACION_DEVUELVE_LEE_REC. */
	public static final String DUPLICADO_LIQUIDACION_DEVUELVE_LEE_REC = "devuelveLeeRec";
	
	/** La Constante DUPLICADO_LIQUIDACION_DEVUELVE_SPTO. */
	public static final String DUPLICADO_LIQUIDACION_DEVUELVE_SPTO = "obtenerSpto";
	
	/** La Constante DUPLICADO_LIQUIDACION_OBTENER_CODIGO_NIVEL3BYAGENTE. */
	public static final String DUPLICADO_LIQUIDACION_OBTENER_CODIGO_NIVEL3BYAGENTE = "obtenerCodigoNivel3ByAgente";
	
	/** La Constante DUPLICADO_LIQUIDACION_OBTENER_TIPO_NUMERO_DOCUMENTO. */
	public static final String DUPLICADO_LIQUIDACION_OBTENER_TIPO_NUMERO_DOCUMENTO = "obtenerTipoDocumentoByPoliza";
	
	//Comisiones Con Movimiento
	/** La Constante COMISION_CON_MOVIMIENTO_OBTENER_DETALLE_COMISIONES_CON_MOVIMIENTO. */
	public static final String COMISION_CON_MOVIMIENTO_OBTENER_DETALLE_COMISIONES_CON_MOVIMIENTO = "obtenerDetalleComisionesConMovimiento";
	
	//Movimiento Facultativo Poliza
	/** La Constante MOVIMIENTO_FACULTATIVO_POLIZA_OBTENER_MOVIMIENTO_FACULTATIVO_POLIZA. */
	public static final String MOVIMIENTO_FACULTATIVO_POLIZA_OBTENER_MOVIMIENTO_FACULTATIVO_POLIZA = "obtenerMovimientoFacultativoPoliza";
	
	//Constrol Cheques Procesados
	/** La Constante CONTROL_CHEQUES_PROCESADOS_OBTENER_DETALLE_CONTROL_CHEQUE_PROCESADO. */
	public static final String CONTROL_CHEQUES_PROCESADOS_OBTENER_DETALLE_CONTROL_CHEQUE_PROCESADO = "obtenerDetalleControlChequeProcesado";
	
	//Traspaso Efectivo Dia
	/** La Constante TRASPASO_EFECTIVO_DIA_OBTENER_DETALLE_REMESADO_SOBRE_POR_RESPONSABLE. */
	public static final String TRASPASO_EFECTIVO_DIA_OBTENER_DETALLE_REMESADO_SOBRE_POR_RESPONSABLE = "obtenerDetalleRemesadoSobrePorResponsable";
	
	/** La Constante TRASPASO_EFECTIVO_DIA_OBTENER_DETALLE_CARTA_DEPOSITO_CONTROLORIA. */
	public static final String TRASPASO_EFECTIVO_DIA_OBTENER_DETALLE_CARTA_DEPOSITO_CONTROLORIA = "obtenerDetalleCartaDepositoControloria";
	
	/** La Constante TRASPASO_EFECTIVO_DIA_OBTENER_DETALLE_RESUMEN. */
	public static final String TRASPASO_EFECTIVO_DIA_OBTENER_DETALLE_RESUMEN = "obtenerDetalleResumen";

	//Reporte Caja diaria
	/** La Constante CAJA_DIARIA_OBTENER_DETALLE_CAJA_DIARIA. */
	public static final String CAJA_DIARIA_OBTENER_DETALLE_CAJA_DIARIA = "obtenerDetalleCajaDiaria";
	
	/** La Constante CAJA_DIARIA_OBTENER_DETALLE_SALDO_CAJA_DIARIA. */
	public static final String CAJA_DIARIA_OBTENER_DETALLE_SALDO_CAJA_DIARIA = "obtenerDetalleSaldoCajaDiaria";
	
	/** La Constante CAJA_DIARIA_OBTENER_DETALLE_IMPORTE_CAJA_DIARIA. */
	public static final String CAJA_DIARIA_OBTENER_DETALLE_IMPORTE_CAJA_DIARIA = "obtenerDetalleCajaDiariaImporte";
	
	/** La Constante CAJA_DIARIA_OBTENER_FECHA_ASTO. */
	public static final String CAJA_DIARIA_OBTENER_FECHA_ASTO = "obtenerFechaAsto";
	
	//Reporte Caja Historica
	 /** La Constante CAJA_HISTORICA_OBTENER_DETALLE_CAJA_HISTORICA. */
	public static final String CAJA_HISTORICA_OBTENER_DETALLE_CAJA_HISTORICA = "obtenerDetalleCajaHistorica";
	
	/** La Constante SQL_SYSTEM_DATABASE_OBTENER_COLUMNA_DISPONIBLE. */
	public static final String SQL_SYSTEM_DATABASE_OBTENER_COLUMNA_DISPONIBLE = "obtenerColumnaDisponible";
	
	/* NFIRE 27/05/15 inicio*/
	/** La Constante TABLERO_SINIESTROS_OBTENER_DETALLE_SEGUIMIENTO. */
	public static final String TABLERO_SINIESTROS_OBTENER_SEGUIMIENTO = "obtenerTableroSeguimiento";
	
	/** La Constante TABLERO_SINIESTROS_OBTENER_DETALLE_SEGUIMIENTO. */
	public static final String TABLERO_SINIESTROS_OBTENER_DETALLE_SEGUIMIENTO = "obtenerDetalleTableroSeguimiento";
	
	/** La Constante TABLERO_SINIESTROS_OBTENER_GESTION. */
	public static final String TABLERO_SINIESTROS_OBTENER_GESTION = "obtenerTableroGestion";
	
	/** La Constante TABLERO_SINIESTROS_OBTENER_DETALLE_GESTION. */
	public static final String TABLERO_SINIESTROS_OBTENER_DETALLE_GESTION = "obtenerDetalleTableroGestion";
	/* NFIRE 27/05/15 fin*/
	
	/* SWFACTORY INICIO*/
	/** La Constante SINIESTRALIDAD_OBTENER_DETALLE. */
	public static final String SINIESTRALIDAD_OBTENER_DETALLE = "obtenerDetalleSiniestralidad";
	/* SWFACTORY FIN*/

	//Reporte Multiproducto
	/** La Constante REPORTE_MULTIPRODUCTO. */
	public static final String REPORTE_MULTIPRODUCTO = "obtenerReporteMultiproducto";
	
	//REPORTE DETALLE PRODUCCION
	//MigrarData
	/** La Constante OBTENER_REGISTROS_DETALLE_PRODUCCION. */
	public static final String OBTENER_REGISTROS_DETALLE_PRODUCCION = "obtenerRegistroProduccionCubos";
	// MigrarData ROWID
	/** La Constante OBTENER_REGISTROS_DETALLE_PRODUCCION_ROWID. */
	public static final String OBTENER_REGISTROS_DETALLE_PRODUCCION_ROWID = "obtenerRegistroProduccionCubosRowId";
	// Completar Datos Recibo
	/** La Constante OBTENER_DATOS_RECIBO. */
	public static final String OBTENER_DATOS_RECIBO = "obtenerDatosReciboProduccionCubos";
	// Obtener Maximo SPTO
	/** La Constante OBTENER_MAXIMO_SPTO. */
	public static final String OBTENER_MAXIMO_SPTO = "obtenerMaximoSPTO";
	// Obtener Numero Solicitud
	/** La Constante OBTENER_NUMERO_SOLICITUD. */
	public static final String OBTENER_NUMERO_SOLICITUD = "obtenerNumeroSolicitud";
	// Obtener Maximo SPTO Fecha
	/** La Constante OBTENER_MAXIMO_SPTO_FECHA. */
	public static final String OBTENER_MAXIMO_SPTO_FECHA = "obtenerMaximoNumeroSPTOFecha";
	// Obtener Nombre Eje
	/** La Constante OBTENER_NOMBRE_EJECUTIVO. */
	public static final String OBTENER_NOMBRE_EJECUTIVO = "obtenerNombreEjecutivo";
	// Obtener Nombre Eje
	/** La Constante OBTENER_NOMBRE. */
	public static final String OBTENER_NOMBRE = "obtenerNombre";
	// Obtener Nombre Eje
	/** La Constante OBTENER_NOMBRE_AGENTE. */
	public static final String OBTENER_NOMBRE_AGENTE = "obtenerNombreAgente";
	// Obtener datos contrante
	/** La Constante OBTENER_DATOS_CONTRANTES. */
	public static final String OBTENER_DATOS_CONTRANTES = "obtenerDatosContrantes";
	// Obtener tipo de emision
	/** La Constante OBTENER_TIPO_EMISION. */
	public static final String OBTENER_TIPO_EMISION = "obtenerTipoEmision";
	// Obtener tipo de poliza grupo
	/** La Constante OBTENER_NOMBRE_POLIZA_GRUPO. */
	public static final String OBTENER_NOMBRE_POLIZA_GRUPO = "obtenerNombrePolizaGrupo";
	// Obtener tipo de modalidad
	/** La Constante OBTENER_NOMBRE_MODALIDAD. */
	public static final String OBTENER_NOMBRE_MODALIDAD = "obtenerNombreModalidad";
	// Obtener datos asegurado
	/** La Constante OBTENER_DATOS_ASEGURADO. */
	public static final String OBTENER_DATOS_ASEGURADO = "obtenerDatosAsegurado";
	// Obtener indices unicos
	/** La Constante OBTENER_INDICES_UNICOS. */
	public static final String OBTENER_INDICES_UNICOS = "obtenerIndicesUnicos";
	// Obtener nombre sector
	/** La Constante OBTENER_NOMBRE_SECTOR. */
	public static final String OBTENER_NOMBRE_SECTOR = "obtenerNombreSector";
	// Obtener nombre sub sector1
	/** La Constante OBTENER_NOMBRE_SUBSECTOR1. */
	public static final String OBTENER_NOMBRE_SUBSECTOR1 = "obtenerNombreSubSector1";
	// Obtener nombre sub sector2
	/** La Constante OBTENER_NOMBRE_SUBSECTOR2. */
	public static final String OBTENER_NOMBRE_SUBSECTOR2 = "obtenerNombreSubSector2";
	// Obtener nombre ramo1
	/** La Constante OBTENER_NOMBRE_RAMO1. */
	public static final String OBTENER_NOMBRE_RAMO1 = "obtenerNombreRamo1";
	// Obtener nombre ramo2
	/** La Constante OBTENER_NOMBRE_RAMO2. */
	public static final String OBTENER_NOMBRE_RAMO2 = "obtenerNombreRamo2";
	// Obtener nombre canal
	/** La Constante OBTENER_NOMBRE_CANAL. */
	public static final String OBTENER_NOMBRE_CANAL = "obtenerNombreCanal";
	// Obtener nombre oficina
	/** La Constante OBTENER_NOMBRE_OFICINA. */
	public static final String OBTENER_NOMBRE_OFICINA = "obtenerNombreOficina";
	// Contar registros detalle produccion
	/** La Constante CONTAR_DATOS_DETALLE_PRODUCCION. */
	public static final String CONTAR_DATOS_DETALLE_PRODUCCION = "contarDatosDetalleProduccionCubos";
	// Contar numero registros
	/** La Constante CONTAR_NUMERO_REGISTROS_UNIVERSO. */
	public static final String CONTAR_NUMERO_REGISTROS_UNIVERSO = "contarNumeroRegistros";
	// Contar registros
	/** La Constante CONTAR_DATOS_RECIBO_PRODUCCION_CUBOS. */
	public static final String CONTAR_DATOS_RECIBO_PRODUCCION_CUBOS = "contarDatosReciboProduccionCubos";
	
	/** La Constante OBTENER_CABECERAS_ROWID_DETALLE_PRODUCCION. */
	public static final String OBTENER_CABECERAS_ROWID_DETALLE_PRODUCCION = "obtenerCaberasRowIdDetalleProduccion";
	//Tablero de Control 
	//Registros HEADER TABLERO CONTROL
	/** La Constante OBTENER_REGISTROS_TABLERO_CONTROL_HEADER. */
	public static final String OBTENER_REGISTROS_TABLERO_CONTROL_HEADER = "obtenerRegistroHeader";
	
	//Registros Emision Universal
	/** La Constante OBTENER_REGISTROS_TABLERO_CONTROL_EMISION_UNIVERSAL. */
	public static final String OBTENER_REGISTROS_TABLERO_CONTROL_EMISION_UNIVERSAL = "obtenerRegistroEmisionUniversal";
	
	//Count Emision Universal
	/** La Constante CONTAR_REGISTROS_TABLERO_CONTROL_EMISION_UNIVERSAL. */
	public static final String CONTAR_REGISTROS_TABLERO_CONTROL_EMISION_UNIVERSAL = "obtenerRegistroEmisionUniversalCount";
	
	/** La Constante OBTENER_IMPORTES_SUMA_UNIVERSAL. */
	public static final String OBTENER_IMPORTES_SUMA_UNIVERSAL = "obtenerImportesSumaUniversal";
	
	/** La Constante OBTENER_IMPORTES_SUMA_SINIESTRO. */
	public static final String OBTENER_IMPORTES_SUMA_SINIESTRO = "obtenerImportesSumaSiniestro";
	
	//Registros Emision Stock
	/** La Constante OBTENER_REGISTROS_TABLERO_CONTROL_EMISION_STOCK. */
	public static final String OBTENER_REGISTROS_TABLERO_CONTROL_EMISION_STOCK = "obtenerRegistroEmisionStock";
	
	//Count Emision Stock
	/** La Constante CONTAR_REGISTROS_TABLERO_CONTROL_EMISION_STOCK. */
	public static final String CONTAR_REGISTROS_TABLERO_CONTROL_EMISION_STOCK = "obtenerRegistroEmisionStockCount";
	
	//Registros Siniestro Cantidad
	/** La Constante OBTENER_REGISTROS_TABLERO_CONTROL_SINIESTRO_CANTIDAD. */
	public static final String OBTENER_REGISTROS_TABLERO_CONTROL_SINIESTRO_CANTIDAD = "obtenerRegistroSiniestroCantidad";
	
	//Filtrar registros siniestros cantidad
	/** The Constant FILTRAR_REGISTROS_SINIESTRABILIDAD. */
	public static final String FILTRAR_REGISTROS_SINIESTRABILIDAD = "filtrarRegistroSiniestroCantidad";
	
	//Count Siniestro Cantidad
	/** La Constante CONTAR_REGISTROS_TABLERO_CONTROL_SINIESTRO_CANTIDAD. */
	public static final String CONTAR_REGISTROS_TABLERO_CONTROL_SINIESTRO_CANTIDAD = "obtenerRegistroSiniestroCantidadCount";
	
	//Registros Siniestro Valor
	/** La Constante OBTENER_REGISTROS_TABLERO_CONTROL_SINIESTRO_VALOR. */
	public static final String OBTENER_REGISTROS_TABLERO_CONTROL_SINIESTRO_VALOR = "obtenerRegistroSiniestroValor";
	
	//Count Siniestro Valor
	/** La Constante CONTAR_REGISTROS_TABLERO_CONTROL_SINIESTRO_VALOR. */
	public static final String CONTAR_REGISTROS_TABLERO_CONTROL_SINIESTRO_VALOR = "obtenerRegistroSiniestroValorCount";

	//Registros Comisiones Ganadas
	/** La Constante OBTENER_REGISTROS_TABLERO_CONTROL_COMISIONES_GANADAS. */
	public static final String OBTENER_REGISTROS_TABLERO_CONTROL_COMISIONES_GANADAS = "obtenerRegistroComisionesGanadas";
	
	/** La Constante OBTENER_PRIMA_TOTAL_COMISION_GANADA. */
	public static final String OBTENER_PRIMA_TOTAL_COMISION_GANADA = "obtenerPrimaTotalComisionGanada";
	
	//Count Comisiones Ganadas
	/** La Constante CONTAR_REGISTROS_TABLERO_CONTROL_COMISIONES_GANADAS. */
	public static final String CONTAR_REGISTROS_TABLERO_CONTROL_COMISIONES_GANADAS = "obtenerRegistroComisionesGanadasCount";

	//Registros Cobros Cobranza 
	/** La Constante OBTENER_REGISTROS_TABLERO_CONTROL_COBROS_COBRANZA. */
	public static final String OBTENER_REGISTROS_TABLERO_CONTROL_COBROS_COBRANZA = "obtenerRegistroCobrosCobranza";
	
	//Count Cobros Cobranza 
	/** La Constante CONTAR_REGISTROS_TABLERO_CONTROL_COBROS_COBRANZA. */
	public static final String CONTAR_REGISTROS_TABLERO_CONTROL_COBROS_COBRANZA = "obtenerRegistroCobrosCobranzaCount";
	
	//Registros Cobros Pendiente Cobro 
	/** La Constante OBTENER_REGISTROS_TABLERO_CONTROL_COBROS_PENDIENTE_COBRO. */
	public static final String OBTENER_REGISTROS_TABLERO_CONTROL_COBROS_PENDIENTE_COBRO = "obtenerRegistroCobrosPendienteCobro";
	
	/** La Constante OBTENER_IMPORTE_FILTRO_SINIESTRO_VALOR. */
	public static final String OBTENER_IMPORTE_FILTRO_SINIESTRO_VALOR = "ObtenerImporteFiltroSiniestroValor";
	
	//Count Cobros Pendiente Cobro 
	/** La Constante CONTAR_REGISTROS_TABLERO_CONTROL_COBROS_PENDIENTE_COBRO. */
	public static final String CONTAR_REGISTROS_TABLERO_CONTROL_COBROS_PENDIENTE_COBRO = "obtenerRegistroCobrosPendienteCobroCount";
	
	/** Obtener los indicadores de siniestros segun el tipo de reporte. */
	public static final String TABLERO_SINIESTROS_OBTENER_INDICADOR = "obtenerIndicadoresTableroSiniestro"; //PBARRIOS
	
	/** Obtener el detalle de los siniestros por indicador. */
	public static final String TABLERO_SINIESTROS_OBTENER_DETALLE_POR_INDICADOR = "obtenerDetalleByIndicadorGestion"; //MSAAVEDRA 20160210
	
	/** Obtener el total del detalle de los siniestros por indicador. */
	public static final String TABLERO_SINIESTROS_OBTENER_TOTAL_DETALLE_POR_INDICADOR = "obtenerTotalByIndicadorGestion"; //MSAAVEDRA 20160211
	
	//Reportes Tecnicos
	/** La Constante SINIESTRALIDAD_OBTENER_DECLARACION_DEINFORMACION_PARA_ESPANA. */
	public static final String SINIESTRALIDAD_OBTENER_DECLARACION_DEINFORMACION_PARA_ESPANA = "obtenerDeclaracionDeInformacionParaEspana";

	/** La Constante SINIESTRALIDAD_OBTENER_DECLARACION_DE_PRODUCCION. */
	public static final String SINIESTRALIDAD_OBTENER_DECLARACION_DE_PRODUCCION = "obtenerDeclaracionDeProduccion";
	
	/** La Constante CONTAR_SINIESTRALIDAD_OBTENER_DECLARACION_DE_PRODUCCION. */
	public static final String CONTAR_SINIESTRALIDAD_OBTENER_DECLARACION_DE_PRODUCCION = "contarObtenerDeclaracionDeProduccion";
	
	/** La Constante SINIESTRALIDAD_OBTENER_DECLARACION_DE_CLIENTE. */
	public static final String SINIESTRALIDAD_OBTENER_DECLARACION_DE_CLIENTE = "obtenerDeclaracionDeCliente";
	
	/** La Constante CONTAR_SINIESTRALIDAD_OBTENER_DECLARACION_DE_CLIENTE. */
	public static final String CONTAR_SINIESTRALIDAD_OBTENER_DECLARACION_DE_CLIENTE = "contarObtenerDeclaracionDeCliente";
	
	/** La Constante OBTENER_VALOR_CAMPO. */
	public static final String OBTENER_VALOR_CAMPO = "obtenerValorCampoMap";
	
	/** La Constante OBTENER_VALOR_CAMPO_CLIENTE. */
	public static final String OBTENER_VALOR_CAMPO_CLIENTE = "obtenerValorCampoClienteMap";
	
	/** La Constante SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS. */
	public static final String SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS = "obtenerDeclaracionDeClienteDeficitarios";
	
	/** The Constant SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS_A2000030. */
	public static final String SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS_A2000030 = "obtenerDeclaracionDeClienteDeficitariosA2000030";

	/** The Constant SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS_A2000031. */
	public static final String SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS_A2000031 = "obtenerDeclaracionDeClienteDeficitariosA2000031";
	
	/** The Constant SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS_A2990700. */
	public static final String SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS_A2990700 = "obtenerDeclaracionDeClienteDeficitariosA2990700";
	
	/** The Constant SINIESTRALIDAD_OBTENER_CALCULO_TRABAJADORES_DECLARACION_DEFICITARIOS. */
	public static final String SINIESTRALIDAD_OBTENER_CALCULO_TRABAJADORES_DECLARACION_DEFICITARIOS = "obtenerCalculoTrabajadoresDeclaracionClienteDeficitario";
	
	/** The Constant SINIESTRALIDAD_OBTENER_NUMERO_SUPLEMENTO_ANULADO_N. */
	public static final String SINIESTRALIDAD_OBTENER_NUMERO_SUPLEMENTO_ANULADO_N= "obtenerNumeroSuplementoAnulado_N";
	
	/** The Constant SINIESTRALIDAD_OBTENER_MAXIMO_SUPLEMENTO_APLI_ASEGURADO. */
	public static final String SINIESTRALIDAD_OBTENER_MAXIMO_SUPLEMENTO_APLI_ASEGURADO= "obtenerMaximoSuplementoApliCalculoNumeroAsegurado";
	
	/** La Constante SINIESTRALIDAD_CLIENTE_DEFICITARIO_SINIESTROS_VIDA. */
	public static final String SINIESTRALIDAD_CLIENTE_DEFICITARIO_SINIESTROS_VIDA = "obtenerClienteDeficitarioNumeroSiniestroVida";
	
	/** La Constante SINIESTRALIDAD_CLIENTE_MONTO_SINIESTROS_VIDA. */
	public static final String SINIESTRALIDAD_CLIENTE_MONTO_SINIESTROS_VIDA = "obtenerClienteDeficitariMontoSiniestroVida";
	
	/** The Constant SINIESTRALIDAD_PRERENOVACION_MONTO_SINIESTROS_VIDA. */
	public static final String SINIESTRALIDAD_PRERENOVACION_MONTO_SINIESTROS_VIDA = "obtenerPreRenovacionMontoSiniestroVida";
	
	/** The Constant SINIESTRALIDAD_CLIENTE_MONTO_LIQUIDADO_SINIESTROS_SALUD. */
	public static final String SINIESTRALIDAD_CLIENTE_MONTO_LIQUIDADO_SINIESTROS_SALUD = "obtenerClienteDeficitariMontoLiquidadoSiniestroSalud";
	
	/** The Constant SINIESTRALIDAD_CLIENTE_PRERENOVACION_MONTO_LIQUIDADO_SINIESTROS_SALUD. */
	public static final String SINIESTRALIDAD_CLIENTE_PRERENOVACION_MONTO_LIQUIDADO_SINIESTROS_SALUD = "obtenerClientePrerenovacionMontoLiquidadoSiniestroSalud";
	
	/** The Constant SINIESTRALIDAD_CLIENTE_MONTO_PENDIENTE_SINIESTROS_SALUD. */
	public static final String SINIESTRALIDAD_CLIENTE_MONTO_PENDIENTE_SINIESTROS_SALUD = "obtenerClienteDeficitariMontoPendienteSiniestroSalud";
	
	/** The Constant SINIESTRALIDAD_PRERENOVACION_MONTO_PENDIENTE_SINIESTROS_SALUD. */
	public static final String SINIESTRALIDAD_PRERENOVACION_MONTO_PENDIENTE_SINIESTROS_SALUD = "obtenerPreRenovacionMontoPendienteSiniestroSalud";
	
	/** La Constante SINIESTRALIDAD_CLIENTE_DEFICITARIO_SINIESTROS_SALUD. */
	public static final String SINIESTRALIDAD_CLIENTE_DEFICITARIO_SINIESTROS_SALUD = "obtenerClienteDeficitarioNumeroSiniestroSalud";

	/** La Constante SINIESTRALIDAD_CLIENTE_DEFICITARIO_SINIESTROS_SILVIA. */
	public static final String SINIESTRALIDAD_CLIENTE_DEFICITARIO_SINIESTROS_SILVIA = "obtenerClienteDeficitarioNumeroSiniestroSilvia";
	
	/** La Constante CONTAR_SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS. */
	public static final String CONTAR_SINIESTRALIDAD_OBTENER_DECLARACION_DEFICITARIOS = "contarObtenerDeclaracionDeClienteDeficitarios";
	
	//Registros Ramo Vida
	/** La Constante OBTENER_REGISTROS_RAMO_VIDA. */
	public static final String OBTENER_REGISTROS_RAMO_VIDA = "obtenerRegistroRamoVida";	
	
	/** La Constante OBTENER_REGISTROS_SINIESTRALIAD_PRIMA. */
	public static final String OBTENER_REGISTROS_SINIESTRALIAD_PRIMA = "obtenerRegistroSiniestralidadPrima";
	
	/** La Constante OBTENER_REGISTROS_CONTADOR_SINIESTRALIAD_PRIMA. */
	public static final String OBTENER_REGISTROS_CONTADOR_SINIESTRALIAD_PRIMA = "contadorObtenerRegistroSiniestralidadPrima";		
	
	
	//TODO NUEVO
	/** La Constante OBTENER_REGISTROS_SILVIA_PENSION. */
	public static final String OBTENER_REGISTRO_SILVIA_RAMO_PENSION_DEFICITARIO = "obtenerRegistroSilviaRamoPensionDeficitario701";
	
	/** La Constante OBTENER_REGISTROS_CONTADOR_SILVIA_PENSION. */
	public static final String OBTENER_REGISTRO_CONTADOR_SILVIA_RAMO_PENSION_DEFICITARIO = "contadorObtenerRegistroSilviaRamoPensionDeficitario701";
	
	/** The Constant OBTENER_REGISTRO_SILVIA_RAMO_PENSION_DEFICITARIO_B. */
	public static final String OBTENER_REGISTRO_SILVIA_RAMO_PENSION_DEFICITARIO_B = "obtenerRegistroSilviaRamoPensionDeficitario701B";
	
	/** The Constant OBTENER_REGISTRO_CONTADOR_SILVIA_RAMO_PENSION_DEFICITARIO_B. */
	public static final String OBTENER_REGISTRO_CONTADOR_SILVIA_RAMO_PENSION_DEFICITARIO_B = "contadorObtenerRegistroSilviaRamoPensionDeficitario701";  //TODO FALTA CONTADOR
	
	/** The Constant OBTENER_REGISTRO_SILVIA_RAMO_PENSION_DEFICITARIO_A. */
	public static final String OBTENER_REGISTRO_SILVIA_RAMO_PENSION_DEFICITARIO_A = "obtenerRegistroSilviaRamoPensionDeficitario701A";
	
	/** The Constant OBTENER_FECHAS_DIAS_COBERTURADOS. */
	public static final String OBTENER_FECHAS_DIAS_COBERTURADOS = "ObtenerFechasDiasCoberturados";
	
	/** The Constant OBTENER_FECHAS_DIAS_COBERTURADOS_RESUL_TEMP. */
	public static final String OBTENER_FECHAS_DIAS_COBERTURADOS_RESUL_TEMP = "ObtenerFechasDiasCoberturadosResulTemp";
	
	/** La Constante OBTENER_REGISTROS_SILVIA_PENSION. */
	public static final String OBTENER_REGISTROS_SILVIA_PENSION = "obtenerRegistroSilviaRamoPension701";
	
	/** La Constante OBTENER_REGISTROS_CONTADOR_SILVIA_PENSION. */
	public static final String OBTENER_REGISTROS_CONTADOR_SILVIA_PENSION = "contadorObtenerRegistroSilviaRamoPension701";
	
	/** La Constante OBTENER_REGISTROS_CONTADOR_COSTO_PRIMERO_SILVIA_PENSION. */
	public static final String OBTENER_REGISTROS_CONTADOR_COSTO_PRIMERO_SILVIA_PENSION = "ObtenerRegistroSilviaRamoPenPrimCosto701";

	/** La Constante OBTENER_REGISTROS_CONTADOR_COSTO_SEGUNDO_SILVIA_PENSION. */
	public static final String OBTENER_REGISTROS_CONTADOR_COSTO_SEGUNDO_SILVIA_PENSION = "ObtenerRegistroSilviaRamoPenSegCosto701";
		
	/** La Constante OBTENER_REGISTROS_SILVIA_PENSION_A. */
	public static final String OBTENER_REGISTROS_SILVIA_PENSION_A = "obtenerRegistroSilviaRamoPension701A";
	
	/** La Constante OBTENER_REGISTROS_CONTADOR_SILVIA_PENSION_A. */
	public static final String OBTENER_REGISTROS_CONTADOR_SILVIA_PENSION_A = "obtenerContadorRegistroSilviaRamoPension701A";
	
	/** La Constante OBTENER_REGISTROS_SILVIA_PENSION_B. */
	public static final String OBTENER_REGISTROS_SILVIA_PENSION_B = "obtenerRegistroSilviaRamoPension701B";
	
	/** La Constante OBTENER_REGISTROS_SINIESTRALIDAD_SALUD_A. */
	public static final String OBTENER_REGISTROS_SINIESTRALIDAD_SALUD_A = "obtenerRegistroSiniestralidad702A";
	
	/** La Constante OBTENER_REGISTROS_CONTADOR_SINIESTRALIDAD_SALUD_A. */
	public static final String OBTENER_REGISTROS_CONTADOR_SINIESTRALIDAD_SALUD_A = "obtenerRegistroContadorSiniestralidad702A";
	
	/** La Constante OBTENER_REGISTROS_SINIESTRALIDAD_SALUD_B. */
	public static final String OBTENER_REGISTROS_SINIESTRALIDAD_SALUD_B = "obtenerRegistroSiniestralidad702B";
	
	//Siniestro Reporte Consolidado
	
	/** La Constante OBTENER_DIFERENCIA_MES. */
	public static final String OBTENER_DIFERENCIA_MES= "obtenerDiferenciaMes";	
	
	/** La Constante OBTENER_CONSOLIDADO_RAMO. */
	public static final String OBTENER_CONSOLIDADO_RAMO= "obtenerConsolidadoRamo";
	
	/** La Constante OBTENER_REGISTRO_PROCESADOS_CONSOLIDADO_RAMO. */
	public static final String OBTENER_REGISTRO_PROCESADOS_CONSOLIDADO_RAMO = "obtenerRegistroProcesadosConsolidadoRamo";
	
	/** La Constante OBTENER_CONSOLIDADO_RAMO_PERIODO. */
	public static final String OBTENER_CONSOLIDADO_RAMO_PERIODO = "obtenerConsolidadoRamoYPediodo";	
	
	/** The Constant OBTENER_CONSOLIDADO_RAMO_AUN_MESES_PERIODO_EMPRESA. */
	public static final String OBTENER_CONSOLIDADO_RAMO_AUN_MESES_PERIODO_EMPRESA = "obtenerConsolidadoRamoYPediodoAuxMesesEmpresa";
	
	/** The Constant OBTENER_CONSOLIDADO_RAMO_AUN_MESES_PERIODO. */
	public static final String OBTENER_CONSOLIDADO_RAMO_AUN_MESES_PERIODO = "obtenerConsolidadoRamoYPediodoAuxMeses";	
	
	/** The Constant OBTENER_CONSOLIDADO_RAMO_AUN_MESES_PERIODO_CONSOLIDADO. */
	public static final String OBTENER_CONSOLIDADO_RAMO_AUN_MESES_PERIODO_CONSOLIDADO = "obtenerConsolidadoRamoYPediodoAuxMesesConsolidado";
	
	/** La Constante OBTENER_CONSOLIDADO_RAMO_PERIODO_COUNT. */
	public static final String OBTENER_CONSOLIDADO_RAMO_PERIODO_COUNT = "obtenerConsolidadoRamoYPediodoCount";
	
	/** La Constante OBTENER_CONSOLIDADO_RAMO_GRUPO_EMPRESA. */
	public static final String OBTENER_CONSOLIDADO_RAMO_GRUPO_EMPRESA = "obtenerConsolidadoRamoGrupoEmpresa";
	
	/** La Constante OBTENER_DETALLE_RAMO_SCTR_SALUD. */
	public static final String OBTENER_DETALLE_RAMO_SCTR_SALUD = "obtenerDetalleRamoSctrSalud";
	
	/** La Constante CONTAR_DETALLE_RAMO_SCTR_SALUD. */
	public static final String CONTAR_DETALLE_RAMO_SCTR_SALUD = "contarDetalleRamoSctrSalud";
	
	/** La Constante OBTENER_DETALLE_RAMO_SCTR_PENSION_SIN_EVALUACION. */
	public static final String OBTENER_DETALLE_RAMO_SCTR_PENSION_SIN_EVALUACION = "obtenerDetalleRamoSctrPensionSinEvaluacion";
	
	/** La Constante OBTENER_DETALLE_RAMO_SCTR_PENSION_EN_EVALUACION. */
	public static final String OBTENER_DETALLE_RAMO_SCTR_PENSION_EN_EVALUACION = "obtenerDetalleRamoSctrPensionEnEvaluacion";
	
	/** La Constante OBTENER_DETALLE_RAMO_SCTR_PENSION. */
	public static final String OBTENER_DETALLE_RAMO_SCTR_PENSION = "obtenerDetalleRamoSctrPension";
	
	/** La Constante OBTENER_DETALLE_RAMO_SCTR_VIDA. */
	public static final String OBTENER_DETALLE_RAMO_SCTR_VIDA = "obtenerDetalleRamoSctrVida";
	
	//Reporte de PreRenovacion	
	/** La Constante REPORTE_PRE_RENOVACION_OBTENER_DATA. */
	public static final String REPORTE_PRE_RENOVACION_OBTENER_DATA = "obtenerDeclaracionDePreRenovacion";
	
	/** La Constante CONTAR_DECLARACION_DE_PRERENOVACION. */
	public static final String CONTAR_DECLARACION_DE_PRERENOVACION = "ContarDeclaracionDePreRenovacion";
	
	/** La Constante OBTENER_TXT_CAMPO_MAP. */
	public static final String OBTENER_TXT_CAMPO_MAP = "obtenerTxtCampoMap";
	
	/** La Constante OBTENER_SUM_IMP_PRIMA_NETA_MAP. */
	public static final String OBTENER_SUM_IMP_PRIMA_NETA_MAP = "obtenerSumImpPrimaNetaMap";
	
	/** La Constante OBTENER_ESTADO_POLIZA_MAP. */
	public static final String OBTENER_ESTADO_POLIZA_MAP = "obtenerEstadoPolizaMap";
	

	//Reporte de Emision	
	/** La Constante REPORTE_EMISION_OBTENER_DATA. */
	public static final String REPORTE_EMISION_OBTENER_DATA = "obtenerDeclaracionDeEmision";
	
	/** La Constante REPORTE_EMISION_CONTAR_DATA. */
	public static final String REPORTE_EMISION_CONTAR_DATA = "contarObtenerDeclaracionDeEmision";
	
	
	/** The Constant REPORTE_KEY_EMISION_DATA. */
	public static final String REPORTE_KEY_ASEGURADOS_EMISION_DATA = "obtenerKeyAsegurados";
	
	/** The Constant REPORTE_DATA_ASEGURADOS_EMISION_DATA. */
	public static final String REPORTE_DATA_ASEGURADOS_EMISION_DATA = "obtenerDataAsegurados";
	
	/** The Constant ELIMINAR_REGISTROS_TABLERO_CONTROL. */
	public static final String ELIMINAR_REGISTROS_TABLERO_CONTROL = "EliminarRegistrosTableroControl";
	
	/** The Constant OBTENER_REGISTROS_RERPORTE_BIENVENIDA_REBOTE. */
	public static final String OBTENER_REGISTROS_REPORTE_BIENVENIDA_REBOTE = "obtenerReporteBienvenida";
	
	/** The Constant OBTENER_REGISTROS_CONTADOR_REPORTE_BIENVENIDA_REBOTE. */
	public static final String OBTENER_REGISTROS_CONTADOR_REPORTE_BIENVENIDA_REBOTE = "obtenerContadorReporteBienvenida";
	
	/** The Constant OBTENER_REGISTROS_RERPORTE_BIENVENIDA_REBOTE. */
	public static final String OBTENER_REPORTE_CORREDORES = "obtenerReporteCorredores";
	
	/** The Constant OBTENER_REGISTROS_CONTADOR_REPORTE_BIENVENIDA_REBOTE. */
	public static final String OBTENER_CONTADOR_REPORTE_CORREDORES = "obtenerContadorReporteCorredores";
	
	//Catalogo de error
	/** The Constant CATALOGO_ERROR_DATA. */
	public static final String CATALOGO_ERROR_DATA = "catalogoErrorData";
	//completar datos cache.
	/** La Constante OBTENER_NOMBRE_NIVEL2. */
	public static final String OBTENER_NOMBRE_NIVEL2  = "obtenerNombreNivel2";
	
	/** La Constante OBTENER_NOMBRE_NIVEL1. */
	public static final String OBTENER_NOMBRE_NIVEL1 = "obtenerNombreNivel1";
	
	/** La Constante OBTENER_NOMBRE_ESTADO_PAIS. */
	public static final String OBTENER_NOMBRE_ESTADO_PAIS = "obtenerNombreEstadoPais";
	
	/** La Constante OBTENER_NOMBRE_PROVINCIA. */
	public static final String OBTENER_NOMBRE_PROVINCIA = "obtenerNombreProvincia";
	
	/** La Constante OBTENER_NOMBRE_LOCALIDAD. */
	public static final String OBTENER_NOMBRE_LOCALIDAD = "obtenerNombreLocalidad";
	
	/** La Constante OBTENER_EJECUTIVO_REPORTE. */
	public static final String OBTENER_EJECUTIVO_REPORTE = "obtenerEjecutivoReporte";
	
	/** La Constante OBTENER_DATOS_AGENTE. */
	public static final String OBTENER_DATOS_AGENTE = "obtenerDatosAgente";
	
	/** La Constante OBTENER_NOMBRE_AGENTE_REPORTE. */
	public static final String OBTENER_NOMBRE_AGENTE_REPORTE = "obtenerNombreAgente";
	
	//producto salud
	/** La Constante OBTENER_PLAN_SALUD. */
	public static final String OBTENER_PLAN_SALUD = "obtenerPlanSalud";
	
	/** La Constante OBTENER_RED_SALUD. */
	public static final String OBTENER_RED_SALUD = "obtenerRedSalud";
	
	/** La Constante CONTAR_RED_SALUD. */
	public static final String CONTAR_RED_SALUD = "contarRedSalud";
	
	/** La Constante OBTENER_PROVEEDORES_RED_SALUD. */
	public static final String OBTENER_PROVEEDORES_RED_SALUD = "obtenerProveedoresRedSalud";
	
	/** La Constante CONTAR_PROVEEDORES_RED_SALUD. */
	public static final String CONTAR_PROVEEDORES_RED_SALUD = "contarProveedoresRedSalud";
	
	/** La Constante OBTENER_COBERTURA_SALUD. */
	public static final String OBTENER_COBERTURA_SALUD = "obtenerCoberturaSalud";
	
	/** La Constante OBTENER_COBERTURA_SALUD06. */
	public static final String OBTENER_COBERTURA_SALUD06 = "obtenerCoberturaSalud06";
	
	/** La Constante CONTAR_COBERTURA_SALUD. */
	public static final String CONTAR_COBERTURA_SALUD06 = "contarCoberturaSalud06"; 
	
	/** La Constante OBTENER_CODIGO_COBERTURA. */
	public static final String OBTENER_CODIGO_COBERTURA = "obtenerCodigoCobertura";
	
	/** La Constante CONTAR_COBERTURA_SALUD. */
	public static final String CONTAR_COBERTURA_SALUD = "contarCoberturaSalud"; 
	
	/** La Constante OBTENER_COBERTURA_EQUIVALENTE_SALUD. */
	public static final String OBTENER_COBERTURA_EQUIVALENTE_SALUD = "obtenerCoberturaEquivalenteSalud";
	
	/** La Constante CONTADOR_COBERTURA_EQUIVALENTE_SALUD. */
	public static final String CONTAR_COBERTURA_EQUIVALENTE_SALUD = "contadorCoberturaEquivalenteSalud";
	
	/** La Constante OBTENER_PARENTESCO_EDAD. */
	public static final String OBTENER_PARENTESCO_EDAD = "obtenerParentescoEdad";
	
	/** La Constante CONTAR_PARENTESCO_EDAD. */
	public static final String CONTAR_PARENTESCO_EDAD = "contarParentescoEdad";
	
	/** La Constante OBTENER_PRIMA_EDAD. */
	public static final String OBTENER_PRIMA_EDAD = "obtenerPrimaEdad";
	
	/** La Constante CONTAR_PRIMA_EDAD. */
	public static final String CONTAR_PRIMA_EDAD = "contarPrimaEdad";

	/** La Constante OBTENER_PRIMA_COMPOSICION_FAMILIAR. */
	public static final String OBTENER_PRIMA_COMPOSICION_FAMILIAR = "obtenerPrimaComposicionFamiliar";
	
	/** La Constante CONTAR_PRIMA_COMPOSICION_FAMILIAR. */
	public static final String CONTAR_PRIMA_COMPOSICION_FAMILIAR = "contarPrimaComposicionFamiliar";
	
	/** La Constante OBTENER_CARENCIAS_GENERALES. */
	public static final String OBTENER_CARENCIAS_GENERALES = "obtenerCarenciasGenerales";
	
	/** La Constante CONTAR_CARENCIAS_GENERALES. */
	public static final String CONTAR_CARENCIAS_GENERALES = "contarCarenciasGenerales";
	
	/** La Constante OBTENER_GASTOS_NOCUBIERTOS. */
	public static final String OBTENER_GASTOS_NOCUBIERTOS = "obtenerGastosNoCubiertos";
	
	/** La Constante CONTAR_GASTOS_NOCUBIERTOS. */
	public static final String CONTAR_GASTOS_NOCUBIERTOS = "contarGastosNoCubiertos";
	
	/** La Constante OBTENER_FORMAS. */
	public static final String OBTENER_FORMAS = "obtenerFormas";
	
	/** La Constante CONTAR_FORMAS. */
	public static final String CONTAR_FORMAS = "contarFormas";
	
	/** The Constant OBTENER_ASEGURADO. */
	public static final String OBTENER_ASEGURADO = "obtenerAsegurado";
	
	/** The Constant CONTAR_ASEGURADO. */
	public static final String CONTAR_ASEGURADO = "contarAsegurado";
	
	//cartera
	/** La Constante OBTENER_ASEGURADO_A200030. */
	public static final String OBTENER_ASEGURADO_A200030 = "obtenerCartaSeguroMasivoA200030";
	
	/** La Constante CONTAR_ASEGURADO_A200030. */
	public static final String CONTAR_ASEGURADO_A200030 = "contarCartaSeguroMasivoA200030";
	
	/** La Constante OBTENER_PRIMER_RECIBO_POLIZA. */
	public static final String OBTENER_PRIMER_RECIBO_POLIZA = "obtenerPrimerReciboPoliza";
	
	/** La Constante OBTENER_ULTIMO_RECIBO_POLIZA. */
	public static final String OBTENER_ULTIMO_RECIBO_POLIZA = "obtenerUltimoReciboPoliza";
	
	/** La Constante OBTENER_DESTINO_CARTERA. */
	public static final String OBTENER_DESTINO_CARTERA = "obtenerDestinoCartera";

	/** La Constante OBTENER_REGISTROS_REPORTE_POLIZA_CARTERA. */
	public static final String OBTENER_REGISTROS_REPORTE_POLIZA_CARTERA = "obtenerReportePolizaCartera";
	
	/** La Constante OBTENER_REGISTROS_CONTADOR_POLIZA_CARTERA. */
	public static final String OBTENER_REGISTROS_CONTADOR_POLIZA_CARTERA = "obtenerContadorPolizaCartera";
	
	/** La Constante OBTENER_RED_SALUD. */
	public static final String OBTENER_REGISTROS_CONTADOR_RECIBO_CARTERA = "obtenerRegistrosContadorReciboCartera";
	
	/** La Constante CONTAR_RED_SALUD. */
	public static final String OBTENER_REGISTROS_REPORTE_RECIBO_CARTERA = "obtenerRegistrosReporteReciboCartera";
	
	//recaudacion masivo
	/** La Constante OBTENER_RAMO_ALL. */
	public static final String OBTENER_RAMO_ALL = "ObtenerRamoAll";
	
	/** La Constante OBTENER_RAMO_ALL. */
	public static final String OBTENER_MONEDA_ALL = "ObtenerMonedaAll";
	
	/** La Constante OBTENER_RED_SALUD. */
	public static final String OBTENER_RECAUDACION_MASIVO = "ObtenerRecaudacionMasivo";
	
	/** La Constante CONTAR_RED_SALUD. */
	public static final String CONTAR_RECAUDACION_MASIVO = "ContarRecaudacionMasivo";
	
	/** La Constante OBTENER_PARAMETROS_MANTENIBLES. */
	public static final String OBTENER_PARAMETROS_MANTENIBLES = "ObtenerParametrosMantenibles";
	
	/** La Constante OBTENER_PARAMETRO_USO_VEHICULO. */
	public static final String OBTENER_PARAMETRO_USO_VEHICULO = "ObtenerParametroUsoVehiculo";
	
	/** La Constante OBTENER_DATOS_SOAT_DELIVERY. */
	public static final String OBTENER_DATOS_SOAT_DELIVERY = "ObtenerDatoSoatDelivery";
	
	/** La Constante CONTAR_DATOS_SOAT_DELIVERY. */
	public static final String CONTAR_DATOS_SOAT_DELIVERY = "ContarDatoSoatDelivery";
	
	/** La Constante OBTENER_CONFIGURACION_PRODUCTO_DISPONIBLE. */
	public static final String  OBTENER_CONFIGURACION_PRODUCTO_DISPONIBLE = "ObtenerConfiguracionProductoDisponible";
	
	/** La Constante OBTENER_TIPO_DOMICILIO. */
	public static final String  OBTENER_TIPO_DOMICILIO = "ObtenerTipoDomicilio";
	
	/** La Constante OBTENER_TIPO_NUMERO_DOMICILIO. */
	public static final String  OBTENER_TIPO_NUMERO_DOMICILIO = "ObtenerTipoNumeroDomicilio";
	
	/** La Constante OBTENER_TIPO_INTERIOR. */
	public static final String  OBTENER_TIPO_INTERIOR = "ObtenerTipoInterior";
	
	/** La Constante OBTENER_TIPO_ZONA. */
	public static final String  OBTENER_TIPO_ZONA = "ObtenerTipoZona";
	
	/** La Constante OBTENER_CIUDAD. */
	public static final String  OBTENER_CIUDAD = "ObtenerCiudad";
	
	/** La Constante OBTENER_PROVINCIA. */
	public static final String  OBTENER_PROVINCIA = "ObtenerProvincia";
	
	/** La Constante OBTENER_DISTRITO. */
	public static final String  OBTENER_DISTRITO = "ObtenerDistrito";
	
	/** La Constante OBTENER_OPCION_TABLA. */
	public static final String OBTENER_OPCION_TABLA = "ObtenerOpcionTabla";
	
	/**********************************/
	/** La Constante OBTENER_RED_SALUD. */
	public static final String OBTENER_RED_NUEVO_SALUD = "obtenerRedNuevoSalud";
	
	/** La Constante CONTAR_RED_SALUD. */
	public static final String CONTAR_RED_NUEVO_SALUD = "contarRedNuevoSalud";
	
	/** La Constante OBTENER_PROVEEDORES_RED_SALUD. */
	public static final String OBTENER_PROVEEDOR_RED_NUEVO_SALUD = "obtenerProveedorRedNuevoSalud";
	
	/** La Constante CONTAR_PROVEEDORES_RED_SALUD. */
	public static final String CONTAR_PROVEEDOR_RED_NUEVO_SALUD = "contarProveedorRedNuevoSalud";
	//
	/** La Constante OBTENER_CODIGO_COBERTURA. */
	public static final String OBTENER_CODIGO_COBERTURA_NUEVO_SALUD = "obtenerCodigoCoberturaNuevoSalud";
	
	/** La Constante OBTENER_COBERTURA_SALUD. */
	public static final String OBTENER_COBERTURA_NUEVO_SALUD = "obtenerCoberturaNuevoSalud";
	
	/** La Constante CONTAR_COBERTURA_SALUD. */
	public static final String CONTAR_COBERTURA_NUEVO_SALUD = "contarCoberturaNuevoSalud"; 
	
	/** La Constante OBTENER_PARENTESCO_EDAD. */
	public static final String OBTENER_PARENTESCO_EDAD_NUEVO_SALUD = "obtenerParentescoEdadNuevoSalud";
	
	/** La Constante CONTAR_PARENTESCO_EDAD. */
	public static final String CONTAR_PARENTESCO_EDAD_NUEVO_SALUD = "contarParentescoEdadNuevoSalud";
	
	/** La Constante OBTENER_PRIMA_EDAD. */
	public static final String OBTENER_PRIMA_EDAD_NUEVO_SALUD = "obtenerPrimaEdadNuevoSalud";
	
	/** La Constante CONTAR_PRIMA_EDAD. */
	public static final String CONTAR_PRIMA_EDAD_NUEVO_SALUD = "contarPrimaEdadNuevoSalud";

	/** La Constante OBTENER_PRIMA_COMPOSICION_FAMILIAR. */
	public static final String OBTENER_PRIMA_COMPOSICION_FAMILIAR_NUEVO_SALUD = "obtenerPrimaComposicionFamiliarNuevoSalud";
	
	/** La Constante CONTAR_PRIMA_COMPOSICION_FAMILIAR. */
	public static final String CONTAR_PRIMA_COMPOSICION_FAMILIAR_NUEVO_SALUD = "contarPrimaComposicionFamiliarNuevoSalud";
	
	/** La Constante OBTENER_CARENCIAS_GENERALES. */
	public static final String OBTENER_CARENCIAS_GENERALES_NUEVO_SALUD = "obtenerCarenciasGeneralesNuevoSalud";
	
	/** La Constante CONTAR_CARENCIAS_GENERALES. */
	public static final String CONTAR_CARENCIAS_GENERALES_NUEVO_SALUD = "contarCarenciasGeneralesNuevoSalud";
	
	/** La Constante OBTENER_GASTOS_NOCUBIERTOS. */
	public static final String OBTENER_GASTOS_NOCUBIERTOS_NUEVO_SALUD = "obtenerGastosNoCubiertosNuevoSalud";
	
	/** La Constante CONTAR_GASTOS_NOCUBIERTOS. */
	public static final String CONTAR_GASTOS_NOCUBIERTOS_NUEVO_SALUD = "contarGastosNoCubiertosNuevoSalud";
	
	/** La Constante OBTENER_FORMAS. */
	public static final String OBTENER_FORMAS_NUEVO_SALUD = "obtenerFormasNuevoSalud";
	
	/** La Constante CONTAR_FORMAS. */
	public static final String CONTAR_FORMAS_NUEVO_SALUD = "contarFormasNuevoSalud";

	/** La Constante CONTAR_COBRANZA_MOROSIDAD. */
	public static final String CONTAR_COBRANZA_MOROSIDAD = "ContarCobranzaMorosidad";

	/** La Constante OBTENER_COBRANZA_MOROSIDAD. */
	public static final String OBTENER_COBRANZA_MOROSIDAD = "ObtenerCobranzaMorosidad";
	
	/**
	 * Instancia un nuevo constante query tron2000 util.
	 */
	private ConstanteQueryTron2000Util() {
	}
}
