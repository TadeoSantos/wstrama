package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.LogConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmeErrorMpe;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.cache.BigMemoryManager;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.state.EstadoProcesamientoConfiguracionTramaState;

/**
 * La Class RespuestaLecturaArchivoVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class RespuestaLecturaArchivoVO implements Serializable  {
	
	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La log configuracion trama . */
	private TasmeErrorMpe logConfiguracionTrama = new TasmeErrorMpe();
	
	/** La lista log configuracion trama . */
	private BigMemoryManager<String,TasmeErrorMpe> listaLogConfiguracionTrama = new BigMemoryManager<>();
	
	/** La lista configuracion trama data vo. */
	private BigMemoryManager<String,ConfiguracionTramaDataVO> listaConfiguracionTramaDataVO = new BigMemoryManager<>();
	
	/** La estado procesamiento configuracion trama. */
	private EstadoProcesamientoConfiguracionTramaState estadoProcesamientoConfiguracionTrama = EstadoProcesamientoConfiguracionTramaState.NO_PROCESADO;
    
    /** La es error. */
    private boolean esError;
    
    /** La nomenclatura. */
    private String nomenclatura;
    
    /** La nomenclatura leer. */
    private String nomenclaturaLeer;
    
    /** La valor. */
    private Object valor;
    
    /** La lista data resul map. */
    private List<Map<String,ValueDataVO>> listaDataResulMap = new ArrayList<Map<String,ValueDataVO>>();
    
    /** La mensaje error. */
    private String mensajeError;
    
    /** La id configuracion trama. */
    private Long idConfiguracionTrama;
	
    /** La campo mapping formato map. */
    private Map<String,String> campoMappingFormatoMap = new HashMap<String, String>();
    
    /** La lista log configuracion trama ayuda . */
    private BigMemoryManager<String,LogConfiguracionTrama> listaLogConfiguracionTramaAyuda = new BigMemoryManager<>();
    
    /** La log configuracion trama ayuda . */
    private LogConfiguracionTrama logConfiguracionTramaAyuda = new LogConfiguracionTrama();
    
    /** La lista respuesta lectura archivo vo grupo. */
    private List<RespuestaLecturaArchivoVO> listaRespuestaLecturaArchivoVOGrupo = new ArrayList<RespuestaLecturaArchivoVO>();
    
    /** La propiedad map. */
    private Map<String,String> propiedadMap = new HashMap<>();
    
    /** La key grupo. */
    private String keyGrupo = "";
    
    /** La error tupla procesamiento. */
    private boolean errorTuplaProcesamiento = false;
    
    /** La numero referencia. */
    private String numeroReferencia = "";
    
    /** La nombre archivo ftp map. */
    private Map<String,String> nombreArchivoFTPMap = new HashMap<>();
    
    /** La error personalizado map. */
    private Map<String,String> errorPersonalizadoMap = new HashMap<>();
    
    //separando logica de negocio
    private boolean existeError;
    
    private boolean existeErrorNumLote;
    
    private  Map<String,ValueDataVO> map = new HashMap<>();
	/**
	 * Instancia un nuevo respuesta lectura archivo vo.
	 */
	public RespuestaLecturaArchivoVO() {
		super();
	}

	/**
	 * Comprueba si es es error.
	 *
	 * @return true, si es es error
	 */
	public boolean isEsError() {
		return esError;
	}

	/**
	 * Comprueba si es error tupla procesamiento.
	 *
	 * @return true, si es error tupla procesamiento
	 */
	public boolean isErrorTuplaProcesamiento() {
		return errorTuplaProcesamiento;
	}

	public boolean isExisteError() {
		return existeError;
	}

	public boolean isExisteErrorNumLote() {
		return existeErrorNumLote;
	}

}
