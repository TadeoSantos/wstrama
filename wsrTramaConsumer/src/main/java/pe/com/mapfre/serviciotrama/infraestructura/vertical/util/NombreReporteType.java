package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class NombreReporteType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum NombreReporteType {

	 /** El RPT_ESTADO_CUENTA_CLIENTE. */	
	 RPT_ESTADO_CUENTA_CLIENTE("rptEstadoCuentaCliente.jasper","rptEstadoCuentaCliente","rptEstadoCuentaCliente","REPR0017"),
	 
	 /** The rpt estado cuenta cliente excel. */
 	RPT_ESTADO_CUENTA_CLIENTE_EXCEL("rptEstadoCuentaClienteExcel.jasper","rptEstadoCuentaCliente","rptEstadoCuentaCliente","REPR0017"),
	 
	 /** The rpt estado cuenta cliente mail. */
 	RPT_ESTADO_CUENTA_CLIENTE_MAIL("rptEstadoCuentaClienteMail.jasper","rptEstadoCuentaCliente","rptEstadoCuentaCliente","REPF1104"),//Ficticio -->genera el reporte REPR0017:RPT_ESTADO_CUENTA_CLIENTE

	RPT_ESTADO_CUENTA_CLIENTE_MAIL_EXCEL("rptEstadoCuentaClienteMailExcel.jasper","rptEstadoCuentaCliente","rptEstadoCuentaCliente","REPF1104"),//Ficticio -->genera el reporte REPR0017:RPT_ESTADO_CUENTA_CLIENTE
 	
	 /** El RPT_ESTADO_CUENTA_AGENTE. */
	 RPT_ESTADO_CUENTA_AGENTE("rptEstadoCuentaAgente.jasper","rptEstadoCuentaAgente","rptEstadoCuentaAgente","REPR0018"),
	 
	 /** The rpt estado cuenta agente excel. */
 	RPT_ESTADO_CUENTA_AGENTE_EXCEL("rptEstadoCuentaAgenteExcel.jasper","rptEstadoCuentaAgente","rptEstadoCuentaAgente","REPR0018"),
	 	 
	 /** El RPT_ESTADO_CUENTA_HISTORICA. */	 
	 RPT_ESTADO_CUENTA_HISTORICA("rptEstadoCuentaHistorica.jasper","rptEstadoCuentaHistorica","rptEstadoCuentaHistorica","REPR0290"),

	 RPT_ESTADO_CUENTA_HISTORICA_EXCEL("rptEstadoCuentaHistoricaExcel.jasper","rptEstadoCuentaHistorica","rptEstadoCuentaHistorica","REPR0290"),
	 
	 /** El RPT_DETALLE_COMISION_MOVIMIENTO. */	 	 
	 RPT_DETALLE_COMISION_MOVIMIENTO("rptDetalleComisionMovimiento.jasper","rptDetalleComisionMovimiento","rptDetalleComisionMovimiento","REPR0097"),
	 
	 /** El RPT_DUPLICADO_LIQUIDACION_COMISION. */	 	 	 
	 RPT_DUPLICADO_LIQUIDACION_COMISION("rptDuplicadoLiquidacion.jasper","rptDuplicadoLiquidacion","rptDuplicadoLiquidacion","REPR0215"),
	 
	 /** El RPT_RANKING_DECESOS. */	 	 	 	 
	 RPT_RANKING_DECESOS("rptRankingDecesos.jasper","rptRankingDecesos","rptRankingDecesos",""),

	 /** El RPT_CONTROL_DIARIO_EMISION_MASIVA_POLIZA. */	 	 	 	 	 
	 RPT_CONTROL_DIARIO_EMISION_MASIVA_POLIZA("rptControlDiarioEmisionMasivaPoliza.jasper","rptControlDiarioEmisionMasivaPoliza","rptControlDiarioEmisionMasivaPoliza",""),
	 
	 /** El RPT_CONTROL_DIARIO_EMISION_MASIVA_POLIZA. */	 	 	 	 	 
	 RPT_POLIZA_MIGRACION_INTERNA("rptPolizaMigracionesInternas.jasper","rptPolizaMigracionesInternas","rptPolizaMigracionesInternas",""),
	 
	 

	 /** El RPT_DETALLE_AVANCE_CORREDOR. */	 	 	 	 	 	 
	 RPT_DETALLE_AVANCE_CORREDOR("rptAvanceCorredor.jasper","rptAvanceCorredor", "rptAvanceCorredor",""),

	 /** El RPT_ASEGURADO_DE_CERTIFICADO. */	 	 	 	 	 	 	 
	 RPT_ASEGURADO_DE_CERTIFICADO("rptRelacionAseguradoCertificado.jasper","rptRelacionAseguradoCertificado","rptRelacionAseguradoCertificado",""),

//	 /** El RPT_ESTADO_CUENTA_PENDIENTE. */	 	 	 	 	 	 	 	 
//	 RPT_ESTADO_CUENTA_PENDIENTE("rptEstadoCuentaPendiente.jasper","rptEstadoCuentaPendiente","rptEstadoCuentaPendiente"),

	 /** El RPT_DETALLE_MOVIMIENTO_FACULTATIVO_POLIZA. */	 	 	 	 	 	 	 	 	 
	 RPT_DETALLE_MOVIMIENTO_FACULTATIVO_POLIZA("rptMovimientoFacultativoPoliza.jasper","rptMovimientoFacultativoPoliza","rptMovimientoFacultativoPoliza",""),

	 /** El RPT_CONTROL_CHEQUE_PROCESADO. */	 	 	 	 	 	 	 	 	 	 
	 RPT_CONTROL_CHEQUE_PROCESADO("rptControlChequeProcesado.jasper","rptControlChequeProcesado","rptControlChequeProcesado",""),
	 	 
	 /** El RPT_POLIZA_AUTOMOVIL. */	 	 	 	 	 	 	 	 	 	 
	 RPT_POLIZA_AUTOMOVIL("rptPolizaAutomovil.jasper","rptPolizaAutomovil","rptPolizaAutomovil",""),
	 
	 /** The rpt poliza automovil exel. */
 	RPT_POLIZA_AUTOMOVIL_EXEL("rptPolizaAutomovil.xlsx","rptPolizaAutomovil","rptPolizaAutomovil",""),
	 
	 /** El RPT_POLIZA_AUTOMOVIL. */	 	 	 	 	 	 	 	 	 	 
	 RPT_TRASPASO_EFECTIVO_DIA("rptTraspasoEfectivoDia.jasper","rptTraspasoEfectivoDia","rptTraspasoEfectivoDia",""),
	 
	 
	 /** The rpt carga trabajo exel. */
	 	RPT_CARGA_TRABAJO_EXCEL("rptCargaTrabajo.xlsx","rptCargaTrabajo","rptCargaTrabajo",""),
	 
	 /** El RPT_EMISION_GESTOR. */	 	 	 	 	 	 	 	 	 	 
	 RPT_EMISION_GESTOR("rptEmisionGestor.jasper","rptEmisionGestor","rptEmisionGestor",""),	 
	 
	 /** El RPT_EMISION_GESTOR. */	 	 	 	 	 	 	 	 	 	 
	 RPT_CAJA_DIARIA("rptCajaDiaria.jasper","rptCajaDiaria","rptCajaDiaria",""),	 	 
	 
	 /** El RPT_CAJA_HISTORICA. */	 	 	 	 	 	 	 	 	 	 	 
	 RPT_CAJA_HISTORICA("rptCajaHistorica.jasper","rptCajaHistorica","rptCajaHistorica",""),
	 
	 /** El RPT_ASEGURADO_DE_CERTIFICADO. */	 	 	 	 	 	 	 
	 RPT_DETALLE_PRODUCCION("rptRegistroProduccion.jasper","rptRegistroProduccion","rptRegistroProduccion",""),	 
	 
	//Inicio BUIDSOFT 02 Requerimiento reporte detalle produccion excel 
	 RPT_DETALLE_PRODUCCION_XLSX("rptRegistroProduccion.xlsx","rptRegistroProduccion","rptRegistroProduccion",""),	 
	//Fin BUIDSOFT 02 Requerimiento reporte detalle produccion excel 
	 RPT_REGISTRO_OPERACIONES_XLSX("rptRegistroOperaciones.xlsx","rptRegistroOperaciones","rptRegistroOperaciones",""),
	 RPT_REGISTRO_OPERACIONES_TXT("rptRegistroOperaciones.txt","rptRegistroOperaciones","rptRegistroOperaciones",""),
	 
	 /** El RPT_NUEVA_METRICA_CASA_MATRIZ. */	 	 	 	 	 	 	 
	 RPT_NUEVA_METRICA_CASA_MATRIZ("rptNuevaMetricaCasaMatriz.jasper","rptNuevaMetricaCasaMatriz","rptNuevaMetricaCasaMatriz",""),	 
	 
	/** The rpt cotizaciones exel. */
	 RPT_COTIZACIONES_EXEL("rptCotizaciones.xlsx","rptCotizaciones","rptCotizaciones",""), 
	 
	 
	 
	 
	 
	 /**  SWFACTORTY *. */	 	 	 	 	 	 	 	 	 	 	 	
	 RPT_SINIESTRALIDAD("rptSiniestralidad.jasper","rptSiniestralidad","rptSiniestralidad",""),
	 
 	/** The rpt siniestros. */
 	RPT_SINIESTROS("rptSiniestros.jasper","rptSiniestros","rptSiniestros",""),//EXCEL
	
 	/** The rpt siniestros pdf. */
 	RPT_SINIESTROS_PDF("rptSiniestrosPdf.jasper","rptSiniestros","rptSiniestros",""),//PDF
	 
	 /** The rpt declaracion cliente. */
 	RPT_DECLARACION_CLIENTE("rptDeclaracionCliente.jasper","rptDeclaracionCliente","rptDeclaracionCliente",""),
	 
 	/** The rpt produccion. */
 	RPT_PRODUCCION("rptProduccion.jasper","rptProduccion","rptProduccion",""),
	 
 	/** The rpt emision. */
 	RPT_EMISION("rptEmision.jasper","rptEmision","rptEmision",""),
	 
 	/** The rpt pre renovacion. */
 	RPT_PRE_RENOVACION("rptPreRenovacion.jasper","rptPreRenovacion","rptPreRenovacion",""),
	 
 	/** The rpt deficitario. */
 	RPT_DEFICITARIO("rptDeficitario.jasper","rptDeficitario","rptDeficitario",""),	 
	 
 	/** The rpt inf espana. */
 	RPT_INF_ESPANA("rptInformacionEspa.jasper","rptInformacionEspa","rptInformacionEsp",""),
	 
 	/**  SWFACTORTY FIN *. */
	 
	 /** El NULO. */
	 NULO("","","",""),
	 
	 /** The rpt poliza electronica exel. */
 	RPT_POLIZA_ELECTRONICA_EXEL("rptPolizaElectronica.xlsx","rptPolizaElectronica","rptPolizaElectronica",""),
	 
 	/** The rpt poliza bienvenida rebote excel. */
 	RPT_POLIZA_BIENVENIDA_REBOTE_EXCEL("rptBienvenidaRebote.xlsx","rptBienvenidaRebote","rptBienvenidaRebote",""),
	
 	//INICIO INDRA 15012019
 	/** The rpt bandeja abono. */
	RPT_BANDEJA_ABONO("rptBandejaAbono.xlsx","rptBandejaAbono","rptBandejaAbono",""),
	
	RPT_BANDEJA_VISITAVIRTUAL("rptReporteRRCSCTR.xlsx","rptReporteRRCSCTR","rptReporteRRCSCTR",""),
 	//FIN INDRA 15012019
 	 
 	/** The rpt poliza corredores excel. */
 	RPT_POLIZA_CORREDORES_EXCEL("rptCorredores.xlsx","rptCorredores","rptCorredores",""),
	
 	RPT_BANDEJA_EFECTIVIDAD_EXCEL("rptEfectividad.xlsx","rptEfectividad","rptEfectividad",""),//INICIO INDRA 25/07/2019
 	
 	RPT_BANDEJA_REPORTE_RECAUDO("rptReporteRecaudo.xlsx","rptReporteRecaudo","rptReporteRecaudo",""),
 	
 	//INICIO INDRA 12032019
 	RPT_COBRO_RECURRENTE("rptCobroRecurrente.xlsx","rptCobroRecurrente","rptCobroRecurrente",""),
 	//INICIO INDRA 12032019
 	
 	/** The rpt producto salud. */
 	RPT_PRODUCTO_SALUD("rptProductoSalud.xlsx","rptProductoSalud","rptProductoSalud",""),
	 
 	/** The rpt asegurado exel. */
 	RPT_ASEGURADO_EXEL("rptAsegurado.xlsx","rptAsegurado","rptAsegurado",""),
	 
 	/** The rpt cartera recibo. */
 	RPT_CARTERA_RECIBO("rptCarteraRecibo.xlsx","rptCarteraRecibo","CarteraRecibo",""),
	 
 	/** El Reporte de Taridfario Ramo 100. */	 	 	 	 	 
	 RPT_DETALLE_TARIFARIO_100("rptDetalleTarifario100.jasper","rptDetalleTarifario100","rptDetalleTarifario100",""),
	
 	/** The rpt reporte oim. */
 	RPT_REPORTE_OIM("rptReporteOIm.xlsx","rptReporteOIm","rptReporteOIm",""),
	 
 	/** The rpt cobranza dudosa. */
 	RPT_COBRANZA_DUDOSA("rptReporteCobranzaDudosa.xlsx","rptReporteCobranzaDudosa","rptReporteCobranzaDudosa",""),
	
	// Reporte de Asegurados 
	RPT_CARGA_ASEGURADOS_POLIZAS("rptReporteCargaAsegurados.xlsx","rptReporteCargaAsegurados","rptReporteCargaAsegurados",""),
	RPT_CARGA_ASEGURADOS_SHISTORICO("rptReporteCargaAseguradosHistorico.xlsx","rptReporteCargaAseguradosHistorico","rptReporteCargaAseguradosHistorico",""),

 	/** The rpt siniestro taller excel. */
 	RPT_SINIESTRO_TALLER_EXCEL("rptSiniestroTaller.xlsx","rptSiniestroTaller","rptSiniestroTaller",""),
 	
	// Inicio BuildSoft Reporte FonBienes
	RPT_DETALLE_FONBIENES_CSV("rptFonBienes.csv","rptFonBienes","rptFonBienes",""),
	// Fin BuildSoft Reporte FonBienes
	/* REPORTE RESCATES SALDADAS*/
	RPT_RESERVA_RESCATES("rptReporteRescates.xlsx","rptReporteRescates","rptReporteRescates",""),
	RPT_RESERVA_SALDADAS("rptReporteSaldadas.xlsx","rptReporteSaldadas","rptReporteSaldadas",""),
	/* REPORTE RESCATES SALDADAS*/
	// Titulo Reporte de Reserva Riesgos Ramo 600
	RPT_RESERVA_RIESGOS_RAMO_610("rptReporteReservaRiesgosRamo610.xlsx","rptReporteReservaRiesgosRamo610","rptReporteReservaRiesgosRamo610",""),
	RPT_RESERVA_RIESGOS_RAMO_621("rptReporteReservaRiesgosRamo621.xlsx","rptReporteReservaRiesgosRamo621","rptReporteReservaRiesgosRamo621",""),
	RPT_RESERVA_RIESGOS_RAMO_617("rptReporteReservaRiesgosRamo617.xlsx","rptReporteReservaRiesgosRamo617","rptReporteReservaRiesgosRamo617",""),
	RPT_RESERVA_RIESGOS_RAMO_617R("rptReporteReservaRiesgosRamo617R.xlsx","rptReporteReservaRiesgosRamo617R","rptReporteReservaRiesgosRamo617R",""),
	
 	// Titulo Reporte de Prestamos, Rescates y Vencimientos
	RPT_PRESTAMOS_RESCATES_VENCIMIENTOS("rptReportePrestamosRescatesVencimientos.xlsx","rptPrestamosRescatesVencimientos","rptReportePrestamosRescatesVencimientos",""),
	
 	// Titulo Reporte de Afiliados, emisiopn y Prima SBS 00420222
	RPT_AFILIADOS("rptReporteAfiliados.xlsx","rptReporteAfiliados","rptReporteAfiliados",""),
	// Titulo Reporte de Afiliados, emisiopn y Prima SBS 00420222
	RPT_EMISIONRENOVACION("rptReporteEmisionRenovacion.xlsx","rptReporteEmisionRenovacion","rptReporteEmisionRenovacion",""),
	// Titulo Reporte de Afiliados, emisiopn y Prima SBS 00420222
	RPT_PRIMASBS("rptReportePrimasbs.xlsx","rptReportePrimasbs","rptReportePrimasbs","");
		
	 
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, NombreReporteType> LOO_KUP_MAP = new HashMap<String, NombreReporteType>();
	
	static {
		for (NombreReporteType s : EnumSet.allOf(NombreReporteType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;
	
	/** The carperta. */
	private String carperta;
	
	/** The codigo reporte. */
	private String codigoReporte;

	
	/**
	 * Instancia un nuevo nombre reporte type.
	 *
	 * @param key el key
	 * @param value el value
	 * @param carperta el carperta
	 * @param codigoReporte el codigo reporte
	 */
	private NombreReporteType(String key, String value,String carperta,String codigoReporte) {
		this.key = key;
		this.value = value;
		this.carperta = carperta;
		this.codigoReporte = codigoReporte;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the accion type
	 */
	public static NombreReporteType get(String key) {
		if (LOO_KUP_MAP.containsKey(key)) {
			return LOO_KUP_MAP.get(key);
		} 
		return NULO;
		
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Obtiene carpeta.
	 *
	 * @return carpeta
	 */
	public String getCarpeta() {
		return carperta;
	}

	/**
	 * Obtiene codigo reporte.
	 *
	 * @return codigo reporte
	 */
	public String getCodigoReporte() {
		return codigoReporte;
	}	
	
}