package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmeErrorMpe;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.TasmeErrorMpePK;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.TasmeErrorMpeAutomaticoDaoLocal;

/**
 * La Class TasmeErrorMpeDaoImpl.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class TasmeErrorMpeAutomaticoDaoImpl extends  GenericRepository<TasmeErrorMpePK, TasmeErrorMpe> implements TasmeErrorMpeAutomaticoDaoLocal  {

	}