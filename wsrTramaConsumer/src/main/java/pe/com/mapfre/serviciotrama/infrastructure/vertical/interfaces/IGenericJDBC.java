package pe.com.mapfre.serviciotrama.infrastructure.vertical.interfaces;

import java.util.List;
import java.util.Map;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.vo.ScriptSqlResulJDBCVO;

public  interface IGenericJDBC {
	<T> T executeQuery(StringBuilder sql,Map<String, Object> parametros,Class<T> entityClassEntity) throws Exception;
	<T> T executeQueryPreparedStatement(StringBuilder sql,Map<String, Object> parametros,Class<T> entityClassEntity) throws Exception;
	ScriptSqlResulJDBCVO  executeQuerySP(String sp,String jndiConexion,List<Integer> parametroOutType,List<Object> parametroInType,Map<String, Object> parametrosType) throws Exception;
	ScriptSqlResulJDBCVO  executeQuerySP(StringBuilder sp) throws Exception;
	<T> T executeQuery(StringBuilder sql,Map<String, Object> parametros,String jndiConexion,Class<T> entityClassEntity) throws Exception;
	<T> T executeQueryPreparedStatement(StringBuilder sql,Map<String, Object> parametros,String jndiConexion,Class<T> entityClassEntity) throws Exception ;
	<T> List<T> executeQueryList(StringBuilder sql,Map<String, Object> parametros,Class<T> entityClassEntity) throws Exception;
	<T> List<T> executeQueryListPreparedStatement(StringBuilder sql,Map<String, Object> parametros,Class<T> entityClassEntity) throws Exception;
	<T> List<T> executeQueryList(StringBuilder sql,Map<String, Object> parametros,Class<T> entityClassEntity,Map<String,String> formatoMap) throws Exception;
	<T> List<T> executeQueryListPreparedStatement(StringBuilder sql,Map<String, Object> parametros,Class<T> entityClassEntity,Map<String,String> formatoMap) throws Exception ;
	List<Object[]> executeQueryList(StringBuilder sql,Map<String, Object> parametros) throws Exception;
	List<Object[]> executeQueryListPreparedStatement(StringBuilder sql,Map<String, Object> parametros) throws Exception ;
	<T> List<T> executeQueryList(StringBuilder sql,Map<String, Object> parametros,String jndiConexion,Class<T> entityClassEntity) throws Exception;
	<T> List<T> executeQueryListPreparedStatement(StringBuilder sql,Map<String, Object> parametros,String jndiConexion,Class<T> entityClassEntity) throws Exception;
	ScriptSqlResulJDBCVO executeQuery(StringBuilder sql,Map<String, Object> parametros) throws Exception ;
	ScriptSqlResulJDBCVO executeQueryPreparedStatement(StringBuilder sql,Map<String, Object> parametros) throws Exception;
	ScriptSqlResulJDBCVO executeQuery(StringBuilder sql,Map<String, Object> parametros,String jndiConexion) throws Exception;
	ScriptSqlResulJDBCVO executeQueryPreparedStatement(StringBuilder sql,Map<String, Object> parametros,String jndiConexion) throws Exception;
	ScriptSqlResulJDBCVO executeUpdate(StringBuilder sql,Map<String, Object> parametros) throws Exception;
	ScriptSqlResulJDBCVO executeUpdatePreparedStatement(StringBuilder sql,Map<String, Object> parametros) throws Exception;
	ScriptSqlResulJDBCVO executeUpdate(StringBuilder sql,Map<String, Object> parametros,String jndiConexion) throws Exception;
	ScriptSqlResulJDBCVO executeUpdatePreparedStatement(StringBuilder sql,Map<String, Object> parametros,String jndiConexion) throws Exception;
}
