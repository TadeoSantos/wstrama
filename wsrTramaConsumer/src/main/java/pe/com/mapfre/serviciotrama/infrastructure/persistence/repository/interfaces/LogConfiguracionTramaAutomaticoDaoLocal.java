package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces;

import javax.ejb.Local;

import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.LogConfiguracionTrama;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.IGenericRepository;

/**
 * La Class LogConfiguracionTramaAutomaticoDaoLocal.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Local
public interface LogConfiguracionTramaAutomaticoDaoLocal  extends IGenericRepository<String,LogConfiguracionTrama> {
	
}