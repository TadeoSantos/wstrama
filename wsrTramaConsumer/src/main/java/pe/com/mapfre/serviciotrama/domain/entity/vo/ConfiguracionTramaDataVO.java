package pe.com.mapfre.serviciotrama.domain.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.entity.ValueDataVO;

/**
 * La Class ConfiguracionTramaDataVO.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
public class ConfiguracionTramaDataVO implements Serializable  {

	/** La Constante serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** La schema name. */
	private String schemaName;
	
	/** La table name. */
	private String tableName;
	
	/** La atribute name. */
	private String atributeName;
	
	/** La atribute value. */
	private ValueDataVO atributeValue;
	
	/** La atribute type. */
	private String atributeType;
	
	/** La es campo negocio. */
	private boolean esCampoNegocio;
	
	/** La lista campo value. */
	private List<ConfiguracionTramaDataVO> listaCampoValue = new ArrayList<ConfiguracionTramaDataVO>();
	
	/**
	 * Instancia un nuevo configuracion trama data vo.
	 */
	public ConfiguracionTramaDataVO() {
		super();
	}

   /**
    * Instancia un nuevo configuracion trama data vo.
    *
    * @param schemaName el schema name
    * @param tableName el table name
    * @param atributeName el atribute name
    * @param atributeValue el atribute value
    * @param atributeType el atribute type
    */
   public ConfiguracionTramaDataVO(String schemaName, String tableName,
			String atributeName, ValueDataVO atributeValue,String atributeType) {
		super();
		this.schemaName = schemaName;
		this.tableName = tableName;
		this.atributeName = atributeName;
		this.atributeValue = atributeValue;
		this.atributeType = atributeType;
	}

}
