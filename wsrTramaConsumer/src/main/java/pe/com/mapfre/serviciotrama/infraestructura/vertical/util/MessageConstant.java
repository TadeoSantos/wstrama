package pe.com.mapfre.serviciotrama.infraestructura.vertical.util;
/**
 * La Class MessageConstant.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public class MessageConstant {

	private MessageConstant() {
		
	}
    
	//Mensaje de response 
    public static final String OK = "Operación exitosa";
    public static final String NO_CONTENT = "Sin contenido";
    public static final String BAD_REQUEST = "Datos de entrada incorrectos";
    public static final String UNAUTHORIZED = "No autorizado";
    public static final String FORBIDDEN = "Prohibido";
    public static final String NOT_FOUND = "No encontrado";
    public static final String INTERNAL_SERVER_ERROR = "Error incontrolado";
    
	//Mensaje de error excepciones
	public static final String AUT_A057 = "AUT-A057";
	public static final String MESSAGE_ERROR_BAD_REQUEST_EXCEPTION = "Datos ingresados incorrectamente";
	
	public static final String AUT_A002 = "AUT_A002";
	public static final String MESSAGE_ERROR_EXCEPTION = "Ha ocurrido un error al procesar la información";
    
}
