package pe.com.mapfre.serviciotrama.infrastructure.vertical.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
public class FieldValidationError {
    private String campo;
    private String error;
}
