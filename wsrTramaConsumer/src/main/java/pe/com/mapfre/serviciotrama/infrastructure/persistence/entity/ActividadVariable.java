package pe.com.mapfre.serviciotrama.infrastructure.persistence.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class ActividadVariable.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Getter
@Setter
@Entity
@Table(name = "SGSM_ORQ_VAR_ACT", schema = ConfiguracionEntityManagerUtil.ESQUEMA_INTEGRATION_TRON2000)
public class ActividadVariable implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id variable actividad. */
    @Id
    @Column(name = "N_ID_VAR_ACT" , length = 18)
    private Long idVariableActividad;
   
    /** El actividad flujo. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "C_ID_ACT", referencedColumnName = "C_ID_ACT_FLUJO")
    private ActividadFlujo actividadFlujo;
   
    /** El tipo variable actividad. */
    @Column(name = "C_TIP_VAR_ACT" , length = 1)
    private String tipoVariableActividad;
   
    /** El nombre variable actividad. */
    @Column(name = "C_NOM_VAR_ACT" , length = 150)
    private String nombreVariableActividad;
   
    /** El valor variable actividad. */
    @Column(name = "C_VAL_VAR_ACT" , length = 150)
    private String valorVariableActividad;
   
    /** El tipo dato actividad. */
    @Column(name = "N_TIPO_DATO_ACT" , length = 1)
    private Long tipoDatoActividad;
   
    /** El variable asociado. */
    @Column(name = "C_VAL_ASOC_ACT" , length = 1)
    private String variableAsociado;
   
    /** El usuario crea. */
    @Column(name = "C_COD_USU" , length = 50)
    private String usuarioCrea;
   
    /** El fecha actualizacion. */
    @Temporal( TemporalType.TIMESTAMP)
    @Column(name = "D_FEC_ACT")
    private Date fechaActualizacion;
    
    @Column(name = "N_ID_ORD_VAR" , length = 2)
    private Long idOrdenEjecucionVariable;
   
    /**
     * Instancia un nuevo actividad variable.
     */
    public ActividadVariable() {
    }
   
   
    /**
     * Instancia un nuevo actividad variable.
     *
     * @param idVariableActividad el id variable actividad
     * @param actividadFlujo el actividad flujo
     * @param tipoVariableActividad el tipo variable actividad
     * @param nombreVariableActividad el nombre variable actividad
     * @param valorVariableActividad el valor variable actividad
     * @param tipoDatoActividad el tipo dato actividad
     * @param variableAsociado el variable asociado
     * @param usuarioCrea el usuario crea
     * @param fechaActualizacion el fecha actualizacion
     */
    public ActividadVariable(Long idVariableActividad, ActividadFlujo actividadFlujo,String tipoVariableActividad, String nombreVariableActividad, String valorVariableActividad, Long tipoDatoActividad, String variableAsociado, String usuarioCrea, Date fechaActualizacion, Long idOrdenEjecucionVariable) {
        super();
        this.idVariableActividad = idVariableActividad;
        this.actividadFlujo = actividadFlujo;
        this.tipoVariableActividad = tipoVariableActividad;
        this.nombreVariableActividad = nombreVariableActividad;
        this.valorVariableActividad = valorVariableActividad;
        this.tipoDatoActividad = tipoDatoActividad;
        this.variableAsociado = variableAsociado;
        this.usuarioCrea = usuarioCrea;
        this.fechaActualizacion = fechaActualizacion;
        this.idOrdenEjecucionVariable = idOrdenEjecucionVariable;
    }

	/* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idVariableActividad == null) ? 0 : idVariableActividad.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ActividadVariable other = (ActividadVariable) obj;
        if (idVariableActividad == null) {
            if (other.idVariableActividad != null) {
                return false;
            }
        } else if (!idVariableActividad.equals(other.idVariableActividad)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ActividadVariable [idVariableActividad=" + idVariableActividad + "]";
    }
   
}