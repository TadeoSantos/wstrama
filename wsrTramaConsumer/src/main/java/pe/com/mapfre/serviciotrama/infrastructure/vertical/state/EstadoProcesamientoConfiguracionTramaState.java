package  pe.com.mapfre.serviciotrama.infrastructure.vertical.state;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La enum EstadoProcesamientoConfiguracionTramaState.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum EstadoProcesamientoConfiguracionTramaState {

    /** El NO_PROCESADO. */
 	NO_PROCESADO("1" , "estadoProcesamientoConfiguracionTrama.noprocesado"),
	
    /** El PROCESADO_CON_EXITO. */
 	PROCESADO_CON_EXITO("2" , "estadoProcesamientoConfiguracionTrama.procesadoconexito"),
 	
 	 /** El PROCESADO_CON_ERROR. */
 	PROCESADO_CON_ERROR("3" , "estadoProcesamientoConfiguracionTrama.procesadoconerror");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, EstadoProcesamientoConfiguracionTramaState> LOO_KUP_MAP = new HashMap<String, EstadoProcesamientoConfiguracionTramaState>();
	
	static {
		for (EstadoProcesamientoConfiguracionTramaState s : EnumSet.allOf(EstadoProcesamientoConfiguracionTramaState.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo estado configuracion trama state.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private EstadoProcesamientoConfiguracionTramaState(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the estado configuracion trama state
	 */
	public static EstadoProcesamientoConfiguracionTramaState get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
	
}
