package  pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class TipoOrigenType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum TipoOrigenType {

    /** El WEBSERVICE. */
 	WEBSERVICE("WS", "tipoOrigen.webservice",false),
	
    /** El GESTION_TRAMAS. */
 	GESTION_TRAMAS("GT" , "tipoOrigen.gestion_tramas",false),
	
    /** El MASIVO_AUTOMATICO. */
 	MASIVO_AUTOMATICO("MA", "tipoOrigen.masivo_automatico",false),
	
    /** El MASIVO_DEMANDA. */
 	MASIVO_DEMANDA("MD", "tipoOrigen.masivo_demanda",true),
 	
 	/** El DEMONIO_AUTOMATICO. */
 	DEMONIO_AUTOMATICO("DA", "tipoOrigen.demonio_automatico",true);
 	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, TipoOrigenType> LOO_KUP_MAP = new HashMap<String, TipoOrigenType>();
	
	static {
		for (TipoOrigenType s : EnumSet.allOf(TipoOrigenType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private String key;
	
	/** El value. */
	private String value;
	
	/** La item combo. */
	private boolean itemCombo;

	/**
	 * Instancia un nuevo tipo origen type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private TipoOrigenType(String key, String value, boolean itemCombo) {
		this.key = key;
		this.value = value;
		this.itemCombo = itemCombo;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the tipo origen type
	 */
	public static TipoOrigenType get(String key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Comprueba si es item combo.
	 *
	 * @return true, si es item combo
	 */
	public boolean isItemCombo() {
		return itemCombo;
	}
}
