package pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciotrama.domain.entity.vo.EjecucioneManualVO;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.ObjectUtil;
import pe.com.mapfre.serviciotrama.infraestructura.vertical.util.factory.CollectionUtil;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.entity.ProcesoFlujoError;
import pe.com.mapfre.serviciotrama.infrastructure.persistence.repository.interfaces.ProcesoFlujoErrorAutomaticoDaoLocal;
import pe.com.mapfre.serviciotrama.infrastructure.vertical.type.ClaseErrorOrquestadorType;

/**
 * La Class ProcesoFlujoErrorDaoImpl.
 * <ul>
 * <li>2022 BuildSoft - MAPFRE -
 * MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
@TransactionManagement(TransactionManagementType.BEAN)
public class ProcesoFlujoErrorAutomaticoDaoImpl extends  GenericRepository<String, ProcesoFlujoError> implements ProcesoFlujoErrorAutomaticoDaoLocal  {
    	
	@Override
	public List<ProcesoFlujoError> listar(EjecucioneManualVO filtro, List<String> codigoErrorList, List<String> numeroLoteList) {
		List<ProcesoFlujoError> respuesta = new ArrayList<ProcesoFlujoError>();
		Query query = generarQuery(filtro, codigoErrorList, numeroLoteList, false);
		query.setFirstResult(filtro.getStartRow());
		query.setMaxResults(filtro.getOffset());
		List<Object[]> listaObjects = query.getResultList();
		for (Object[] objects : listaObjects) {
			ProcesoFlujoError procesoFlujoError = new ProcesoFlujoError();
			procesoFlujoError.setIdError(ObjectUtil.objectToString(objects[0]));
			procesoFlujoError.setIdControlProceso(ObjectUtil.objectToString(objects[1]));
			procesoFlujoError.setIdIndicadorInterno(ObjectUtil.objectToLong(objects[2]));
			procesoFlujoError.setCodigoError(ObjectUtil.objectToString(objects[3]));
			procesoFlujoError.setMensajeError(ObjectUtil.objectToString(objects[4]));
			procesoFlujoError.setDescripcionError(ObjectUtil.objectToString(objects[5]));
			procesoFlujoError.setFechaOcurrioError((Date) objects[6]);
			procesoFlujoError.setTipoError(ObjectUtil.objectToString(objects[7]));
			if (procesoFlujoError.getMensajeError().contains("${componente}")) {
				procesoFlujoError.setMensajeError(procesoFlujoError.getMensajeError().replace("${componente}", ObjectUtil.objectToString(objects[8])));
			}
			respuesta.add(procesoFlujoError);
		}
		return respuesta;
	}
	
	@Override
	public int contarList(EjecucioneManualVO filtro, List<String> codigoErrorList, List<String> numeroLoteList ) {
		int resultado = 0;
		try {
			Query query = generarQuery(filtro, codigoErrorList, numeroLoteList, true);
			resultado =  ((BigDecimal)query.getSingleResult()).intValue();
		} catch (Exception e) {
			resultado = 0;
		}
		return resultado;
	}
	
	private Query generarQuery(EjecucioneManualVO filtro, List<String> codigoErrorList, List<String> numeroLoteList, boolean esContador) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();	
		if (esContador) {
			jpaql.append(" SELECT COUNT(*) FROM ( ");
		}
		jpaql.append(" SELECT o.C_ID_ERROR, o.NUM_PROC, o.N_INDIC_INTERNO, o.C_COD_ERROR, em_k_sm_sgsm_generador_tramas.equivalencia_error(o.C_MEN_ERROR), o.C_MEN_DESCRIP, ");
		jpaql.append(" o.D_FEC_ERROR, o.C_TIP_ERROR, o.C_TAREA_EJEC_FLUJO FROM TRON2000.SGSM_LOG_ORQ o where NUM_PROC = :numeroProceso ");
		parametros.put("numeroProceso", filtro.getIdProcesoControl());
		if (!CollectionUtil.isEmpty(codigoErrorList)) {
			jpaql.append(" and o.C_COD_ERROR NOT IN (:codigoErrorList) ");
			parametros.put("codigoErrorList", codigoErrorList);
		}
		if (!CollectionUtil.isEmpty(numeroLoteList)) {
			jpaql.append(" UNION ALL ");
			jpaql.append(" SELECT e.NUM_LOTE,e.NUM_LOTE,e.NRO_SECUENCIA,TO_CHAR(e.COD_NIVEL_PROCESO),e.TXT_ERROR,e.TXT_ERROR,e.FEC_LOTE,'N',''");
			jpaql.append(" FROM TRON2000.TASMEERR_MPE e WHERE NUM_LOTE IN :numeroLoteList AND TO_CHAR(COD_NIVEL_PROCESO) IN ('1','2','3','4') ");
			jpaql.append(" ORDER BY 7, 3 ");  //ORDENAR POR TIPO DE ERROR Y POR CLASE
			parametros.put("numeroLoteList", numeroLoteList);
		}

		if (esContador) {
			jpaql.append(" ) ");
		}

		return createNativeQuery(jpaql.toString(), parametros);
	}

	@Override
	public int contar(String numeroProceso, boolean incluirError) throws Exception {
		Integer resultado = 0;
		List<String> listaCodigoExcluir = new ArrayList<String>();
		listaCodigoExcluir.add(ClaseErrorOrquestadorType.ERROR_GENERAL_NO_CONTROLADO_WEBSERVICE_ENVIO_CORREO_ORQE002.getKey() + "T");
		listaCodigoExcluir.add(ClaseErrorOrquestadorType.ERROR_GENERAL_NO_CONTROLADO_WEBSERVICE_ENVIO_CORREO_CONSOLIDADO_ORQE003.getKey() + "T");
		listaCodigoExcluir.add(ClaseErrorOrquestadorType.TRAMA_PROCESADA_ANTERIORMENTE.getKey() + "S");
		if (incluirError) {
			listaCodigoExcluir.add(ClaseErrorOrquestadorType.NO_CONEXION_FTP_ARCHIVO.getKey() + "H");
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		
		jpaql.append(" select count(p.idError) ");
		jpaql.append(" from ProcesoFlujoError p where p.idControlProceso = :numeroProceso ");
		jpaql.append(" and concat(p.codigoError,p.tipoError) not in (:listaCodigoExcluir) ");
		
		parametros.put("numeroProceso", numeroProceso);
		parametros.put("listaCodigoExcluir", listaCodigoExcluir);
		
		try {
			Query query = createQuery(jpaql.toString(), parametros);
			resultado =  ((Long)query.getSingleResult()).intValue();
		} catch (Exception e) {
			resultado = 0;
		}
		
		return resultado;
	}
	
	@Override
	public Integer verificarTipoSinTramaError(String numeroProceso, String codigoErrorSinTrama) throws Exception {
		Map<String, Object> parametros = new HashMap<String, Object>();
		StringBuilder jpaql = new StringBuilder();
		Integer resultado = 0;
		jpaql.append(" select count(p.codigoError) ");
		jpaql.append(" from ProcesoFlujoError p where p.idControlProceso = :numeroProceso ");
		jpaql.append(" and p.codigoError = :codigoErrorSinTrama ");
		parametros.put("numeroProceso",numeroProceso);
		parametros.put("codigoErrorSinTrama",codigoErrorSinTrama);
		try {
			Query query = createQuery(jpaql.toString(), parametros);
			resultado =  ((Long)query.getSingleResult()).intValue();
		} catch (Exception e) {
			resultado = 0;
		}
		return resultado;
	}
}