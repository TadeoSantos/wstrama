package pe.com.mapfre.serviciotrama.infrastructure.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * La Class ErrorCatalogoType.
 * <ul>
 * <li>Copyright 2022 BuildSoft - MAPFRE. Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft; S.A.C.
 * @version 1.0, 05/12/2022
 * @since LectorTrama 1.0
 */
public enum ErrorCatalogoType {
	
	/** La interno. */
	DATOS("D", "Datos"),
	
	/** La externo. */
	OBLIGATORIO("O", "Obligatoriedad"),
	
	/** La longitud. */
	LONGITUD("L", "Longitud");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<String, ErrorCatalogoType> LOO_KUP_MAP = new HashMap<String, ErrorCatalogoType>();

	static {
		for (ErrorCatalogoType s : EnumSet.allOf(ErrorCatalogoType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** La key. */
	private String key;

	/** La value. */
	private String value;

	/**
	 * Instancia un nuevo error tipo type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private ErrorCatalogoType(String key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
}
