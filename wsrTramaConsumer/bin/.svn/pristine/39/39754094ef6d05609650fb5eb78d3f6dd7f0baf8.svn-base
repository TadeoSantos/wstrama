package pe.com.mapfre.serviciosctr.infraestructura.datos.repositorios;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import pe.com.mapfre.serviciosctr.infraestructura.datos.entidades.EntidadDatos1DTO;
import pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.IPruebaRepository;

@Repository
public class PruebaRepository implements IPruebaRepository{
	
	final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final EntityManager entityManager;

    @Autowired
    public PruebaRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    @Override
    public Long procedimientoUno(){
 
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("TRON2000.PCK_PRUEBAS1.CREAR_DISCO");
        
        // Registrar los parámetros de entrada y salida
        storedProcedureQuery.registerStoredProcedureParameter("PTITULO", String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("PINTERPRETE", String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("PID_DISCO", Long.class, ParameterMode.OUT);
        logger.info("Declarando parametros");
        
        // Configuramos el valor de entrada
        storedProcedureQuery.setParameter("PTITULO", "Titulo 1");
        storedProcedureQuery.setParameter("PINTERPRETE", "Interprete 1");
        logger.info("Configurando parametros");

        // Realizamos la llamada al procedimiento
        storedProcedureQuery.execute();
        logger.info("Despues de ejecucion de query");
        // Obtenemos los valores de salida
        final Long outputValue1 = (Long) storedProcedureQuery.getOutputParameterValue("PID_DISCO");
        
        return outputValue1;
    }
    
    @Override
    public List<EntidadDatos1DTO> procedimientoDos(){
    	 
        StoredProcedureQuery storedProcedureQuery = entityManager.createStoredProcedureQuery("TRON2000.PCK_PRUEBAS1.OBTIENE_DISCOS_INTERPRETE");
        
        // Registrar los parámetros de entrada y salida        
        storedProcedureQuery.registerStoredProcedureParameter("PINTERPRETE", String.class, ParameterMode.IN);
        storedProcedureQuery.registerStoredProcedureParameter("PMENS_ERROR", String.class, ParameterMode.OUT);
        storedProcedureQuery.registerStoredProcedureParameter("PDISCOS", Class.class, ParameterMode.REF_CURSOR);
        logger.info("Declarando parametros");
        
        // Configuramos el valor de entrada
        storedProcedureQuery.setParameter("PINTERPRETE", "Interprete 1");

        // Realizamos la llamada al procedimiento
        storedProcedureQuery.execute();

        // Obtenemos el resultado del cursos en una lista
        List<Object[]> results = storedProcedureQuery.getResultList();
     
        // Recorremos la lista con map y devolvemos un List<BusinessObject>
        List<EntidadDatos1DTO> resultado = results.stream().map(result -> new EntidadDatos1DTO(
            ((BigDecimal) result[0]).longValue(),
    	    ((BigDecimal) result[1]).longValue(),
    	    (String) result[2]
        )).collect(Collectors.toList());        
        
        return resultado;
    }

}
