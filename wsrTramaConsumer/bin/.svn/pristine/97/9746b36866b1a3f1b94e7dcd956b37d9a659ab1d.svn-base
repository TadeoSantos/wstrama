package pe.com.mapfre.serviciosctr.infraestructura.datos.repositorios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import oracle.jdbc.OracleTypes;
import pe.com.mapfre.serviciosctr.aplicacion.entidades.PolizaDTO;
import pe.com.mapfre.serviciosctr.aplicacion.entidades.ProductoDTO;
import pe.com.mapfre.serviciosctr.aplicacion.entidades.RecibosDTO;
import pe.com.mapfre.serviciosctr.aplicacion.entidades.RespuestaClienteDTO;
import pe.com.mapfre.serviciosctr.aplicacion.entidades.RespuestaEmisionSctrDTO;
import pe.com.mapfre.serviciosctr.aplicacion.entidades.SolicitudSctrDeclararDTO;
import pe.com.mapfre.serviciosctr.infraestructura.datos.interfaces.ISctrDeclaracionRepository;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.entidades.ScriptSqlResulJDBCVO;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.FechaUtil;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.GenericJDBC;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.ProcesoNegocioUtil;
import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.StringUtil;

@Repository
public class ScrtDeclaracionRepository implements ISctrDeclaracionRepository{
	
	private static final String PKG_SCTR_SO_GEST_ASEG_PER = "PKG_SCTR_SO_GEST_ASEG_PER";

	final Logger log = LoggerFactory.getLogger(this.getClass());

    private final EntityManager entityManager;

    @Autowired
    private GenericJDBC genericJDBC;

    
    @Autowired
    public ScrtDeclaracionRepository(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }
    
    @Override
	public String obtenerMcaPolizaAdelantadaDeclarar(SolicitudSctrDeclararDTO solicitud) {
		String resultado = null;
		String er1 = "Proceso.obtenerMcaPolizaAdelantada.inicio." + solicitud.getUuid() + " : " + solicitud.getPoliza().getNumPoliza() + "  --> "
				+ FechaUtil.obtenerFechaActual();
		log.error(er1);
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" select em_k_val_trama_rt.f_obt_mca_pol_adelantada(" + solicitud.getProducto().getCodCia() + ",");
			
			String numPoliza = ProcesoNegocioUtil.obtenerPoliza(solicitud.getProducto(), solicitud.getPoliza());
			sql.append(" '" + numPoliza + "') as RESULTADO, null as valor from dual ");
			Map<String, Object> parametros = new HashMap<>();
			ScriptSqlResulJDBCVO sqEjecutado = genericJDBC.executeQuery(sql, parametros);
			if (!sqEjecutado.isTieneError() && sqEjecutado.isTieneData()) {
				// Obtenemos los valores de salida
				resultado = sqEjecutado.getListaData().get(0).get("RESULTADO") + "";
			} else {
				String er2 = "Proceso.obtenerMcaPolizaAdelantada." + solicitud.getUuid() + " : " + sqEjecutado.getMensajeError();
				log.error(er2);
			}
		} catch (Exception e) {
			log.error("Error.ScrtDeclaracionRepository.obtenerMcaPolizaAdelantadaDeclarar." + solicitud.getUuid(), e);
			resultado = "${ERROR}" + e.getMessage();
		}
		String er3 = "Proceso.obtenerMcaPolizaAdelantada.fin." + solicitud.getUuid() + " : " + solicitud.getPoliza().getNumPoliza() + "  --> "
				+ FechaUtil.obtenerFechaActual();
		log.error(er3);
		return resultado;
	}
   
	 /**
	    * 
	    Renovación:
			TRON2000.DC_K_RENOVACION_BATCH_SCTR                                                              
	    */
	@Override
	public RespuestaEmisionSctrDTO renovar(SolicitudSctrDeclararDTO solicitud) {
		sessionEnable(solicitud.getUuid());
		RespuestaEmisionSctrDTO resultado = new RespuestaEmisionSctrDTO();
		List<Object> parametroInType = agregarParametroDinamicoRenovar(solicitud);
		List<Integer> parametroOutType = new ArrayList<>();
		parametroOutType.add(OracleTypes.VARCHAR);//p_cod_respuesta
		parametroOutType.add(OracleTypes.VARCHAR);//p_mensaje_batch
		Map<String, Object> parametrosType = new HashMap<>();
		log.error("Proceso.declarar.TRON2000.DC_K_RENOVACION_BATCH_SCTR.P_PROCESA_RENOVACION.inicio." + solicitud.getUuid() +"." + solicitud.getPoliza().getNumPoliza() + "  --> " + FechaUtil.obtenerFechaActual());
		try {
			ScriptSqlResulJDBCVO sqEjecutado  = genericJDBC.executeQuerySP("TRON2000.DC_K_RENOVACION_BATCH_SCTR.P_PROCESA_RENOVACION", null, parametroOutType, parametroInType,parametrosType);
			if (!sqEjecutado.isTieneError() && !sqEjecutado.getListaData().isEmpty()) {
				 // Obtenemos los valores de salida
				String pCodRespuesta = sqEjecutado.getListaData().get(0).get("resultado1") + "";
		        String pMensajeBatch = sqEjecutado.getListaData().get(1).get("resultado2") + "";
		        resultado.setCodError(pCodRespuesta + "");
		        resultado.setDescError(pMensajeBatch);
			} else {
				log.error("TRON2000.DC_K_RENOVACION_BATCH_SCTR.P_PROCESA_RENOVACION." + solicitud.getUuid() ,sqEjecutado.getMensajeError());
			}
		} catch (Exception e) {
			resultado.setCodError("99");
			resultado.setDescError("Error general");
			log.error("TRON2000.DC_K_RENOVACION_BATCH_SCTR.P_PROCESA_RENOVACION.renovar." + solicitud.getUuid() ,e);
		}
		log.error("Proceso.TRON2000.DC_K_RENOVACION_BATCH_SCTR.P_PROCESA_RENOVACION.fin."+  solicitud.getUuid() +"." + solicitud.getPoliza().getNumPoliza() + "  --> " + FechaUtil.obtenerFechaActual());
		return resultado;
	}
	
	private List<Object> agregarParametroDinamicoRenovar(SolicitudSctrDeclararDTO solicitud){
		List<Object> resultado = new ArrayList<>();
		//filtro dinamico
		resultado.add(solicitud.getNroMovimientoCarga());//p_num_mvto 
		resultado.add(solicitud.getProducto().getCodCia());//p_cod_cia
		resultado.add(ProcesoNegocioUtil.obtenerPoliza(solicitud.getProducto(), solicitud.getPoliza()));//p_num_poliza
		resultado.add(solicitud.getCabecera().getCodigoUsuario());//p_cod_usr
		resultado.add(solicitud.getCabecera().getCodigoAplicacion());//p_cod_sistema
		return resultado;
	}

	@Override
	public RespuestaClienteDTO<Boolean>  requiereRenovacion(PolizaDTO poliza, ProductoDTO producto, String uuid) {
		RespuestaClienteDTO<Boolean> resultado = new RespuestaClienteDTO<>();
		resultado.setCodError("0");
		boolean isRequired = false;
		try {
			List<Object> parametroInType = agregarParametroDinamicoRequiereRenovar(poliza,producto);
			List<Integer> parametroOutType = new ArrayList<>();
			parametroOutType.add(OracleTypes.NUMBER);//pRespuesta
			parametroOutType.add(OracleTypes.VARCHAR);//pMensaje
			Map<String, Object> parametrosType = new HashMap<>();
		
			
			String msg = "Proceso.declarar.TRON2000." + PKG_SCTR_SO_GEST_ASEG_PER + ".SP_REQUIERE_RENOVACION." + uuid + ": " + poliza.getNumPoliza() + "  --> " + FechaUtil.obtenerFechaActual();
			log.error(msg);
			
			ScriptSqlResulJDBCVO sqEjecutado  = genericJDBC.executeQuerySP("TRON2000." + PKG_SCTR_SO_GEST_ASEG_PER + ".SP_REQUIERE_RENOVACION", null, parametroOutType, parametroInType,parametrosType);
			if (!sqEjecutado.isTieneError() && !sqEjecutado.getListaData().isEmpty()) {
				int pCodRespuesta = Integer.parseInt(sqEjecutado.getListaData().get(0).get("resultado1") + "");
				if (pCodRespuesta == 0) {
					isRequired = true;
				} else {
					if (pCodRespuesta == -1) {
						resultado.setCodError("1");
						resultado.setDescError(sqEjecutado.getListaData().get(1).get("resultado2") + "");
					}
				}
				
			}
		} catch (Exception e) {
			String er = "Error requiereRenovacion." + uuid + ": " + e.getMessage();
			log.error(er);
		}
		resultado.setObjtoResultado(isRequired);
		return resultado;
	}
	
	private List<Object> agregarParametroDinamicoRequiereRenovar(PolizaDTO poliza, ProductoDTO producto) throws Exception {
		List<Object> parametroInType = new ArrayList<>();
		parametroInType.add(ProcesoNegocioUtil.obtenerPoliza(producto, poliza));
		parametroInType.add(poliza.getFecEfecSpto());
		parametroInType.add(poliza.getFecVctoSpto());
		return parametroInType;
	}
	
	@Override 
	public String isPrimaMinInclu(BigDecimal codCia, String numPoliza, String campo, BigDecimal numRamo, String cod, String uuid) {
		String resultado = "";
		try {
			Map<String, Object> parametros = new HashMap<>();
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT tron2000.sf_recupera_dv_vig( ");
			sql.append("" + codCia + ", '" + numPoliza + "', 0, 1, '" + campo + "', ");
			sql.append("" + numRamo + ", '" + cod + "' ");
			sql.append(") AS valor FROM DUAL ");

			String msg = String.format("tron2000.sf_recupera_dv_vig." + uuid + " --> %s", FechaUtil.obtenerFechaActual().toString());
			log.error(msg);
			ScriptSqlResulJDBCVO sqEjecutado = genericJDBC.executeQuery(sql, parametros); 
			if (!sqEjecutado.isTieneError() && !sqEjecutado.getListaData().isEmpty()) {
				if (!StringUtil.isNullOrEmpty(sqEjecutado.getListaData().get(0).get("VALOR"))) {
					resultado = sqEjecutado.getListaData().get(0).get("VALOR") + "";
				}
			}
		} catch (Exception e) {
			log.error("ScrtDeclaracionRepository.isPrimaMinInclu." + uuid, e);
		}
		return resultado;
	}
	
	@Override
	public RespuestaClienteDTO<Map<String,Object>> recuperarRCENASEG0002Temp(String numPoliza, String nroMovimiento, String fecSpto, String fecVcto, String uuid) {
		RespuestaClienteDTO<Map<String,Object>> resultado = new RespuestaClienteDTO<>();
		resultado.setCodError("0");
		Map<String,Object> lstValidar = new HashMap<>();
		try {
			List<Object> parametroInType = new ArrayList<>();
			parametroInType.add(numPoliza);
			parametroInType.add(nroMovimiento);
			parametroInType.add(fecSpto);
			parametroInType.add(fecVcto);
			parametroInType.add(numPoliza.substring(0, 3));
			List<Integer> parametroOutType = new ArrayList<>();
			parametroOutType.add(OracleTypes.CURSOR);/* o_cursor */
			parametroOutType.add(OracleTypes.VARCHAR);/* pRespuesta */
			parametroOutType.add(OracleTypes.VARCHAR);

			Map<String, Object> parametrosType = new HashMap<>();

			String s = String.format("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_GET_RCEN_ASEG0002_TEMP." + uuid + " --> %s", FechaUtil.obtenerFechaActual().toString());
			log.error(s);

			ScriptSqlResulJDBCVO sqEjecutado = genericJDBC.executeQuerySP("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_GET_RCEN_ASEG0002_TEMP", null, parametroOutType, parametroInType,parametrosType);
			if (!sqEjecutado.isTieneError() && sqEjecutado.isTieneDataCursor()) {
				ScriptSqlResulJDBCVO objCursor = sqEjecutado.getResulCursor();	
				if (objCursor.isTieneData()) {
					lstValidar = objCursor.getListaData().get(0);
				}
			
			}
			if (!sqEjecutado.isTieneError() && sqEjecutado.isTieneData()) {
				resultado.setCodError(sqEjecutado.getListaData().get(0).get("resultado2") + "");
				resultado.setDescError(sqEjecutado.getListaData().get(1).get("resultado3") + "");
				if ("-1".equals(resultado.getCodError())) {
					resultado.setCodError("1");//error que entiede el proceso
				}
			}
		} catch (Exception e) {
			resultado.setCodError("99");
			resultado.setDescError("Error general");
			log.error("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_GET_RECIBO." + uuid + ": ", e);
		}
		resultado.setObjtoResultado(lstValidar);
		
		return resultado;
	}
	
	@Override
	public List<RecibosDTO> obtenerRecibos(SolicitudSctrDeclararDTO solicitud,String numRecibo) {
		List<RecibosDTO> lstValidar = new ArrayList<>();
		try {
			List<Object> parametroInType = new ArrayList<>();
			parametroInType.add(numRecibo);
			parametroInType.add(solicitud.getProducto().getCodCia());
			parametroInType.add(ProcesoNegocioUtil.obtenerPoliza(solicitud.getProducto(), solicitud.getPoliza()));
			parametroInType.add(solicitud.getPoliza().getNumSpto());
			parametroInType.add(solicitud.getPoliza().getNumApli());
			parametroInType.add(solicitud.getPoliza().getNumSptoApli());
			List<Integer> parametroOutType = new ArrayList<>();
			parametroOutType.add(OracleTypes.CURSOR);/* o_cursor */
			parametroOutType.add(OracleTypes.VARCHAR);/* pRespuesta */

			Map<String, Object> parametrosType = new HashMap<>();

			String s = String.format("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_GET_RECIBO." + solicitud.getUuid() + " --> %s", FechaUtil.obtenerFechaActual().toString());
			log.error(s);

			ScriptSqlResulJDBCVO sqEjecutado = genericJDBC.executeQuerySP("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_GET_RECIBO", null, parametroOutType, parametroInType,parametrosType);
			if (!sqEjecutado.isTieneError() && sqEjecutado.isTieneDataCursor()) {
				ScriptSqlResulJDBCVO objCursor = sqEjecutado.getResulCursor();	
				if (objCursor.isTieneData()) {
					lstValidar = parsearRecibo(objCursor);
				}
			}
		} catch (Exception e) {
			log.error("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_GET_RECIBO." + solicitud.getUuid(), e);
		}
		return lstValidar;
	}
	
	private List<RecibosDTO> parsearRecibo(ScriptSqlResulJDBCVO objCursor) throws Exception {
		List<RecibosDTO> listRecibo = new ArrayList<>();
		if (objCursor.getListaData() == null) {
			return listRecibo;
		}
		
		for (Map<String, Object> data : objCursor.getListaData()) {
			RecibosDTO recibo = new RecibosDTO();
			recibo.setFecEfecRecibo(FechaUtil.obtenerFechaFormatoPersonalizado((Date)data.get("FEC_EFEC_RECIBO"), FechaUtil.DATE_DMY));
			recibo.setFecVctoRecibo(FechaUtil.obtenerFechaFormatoPersonalizado((Date)data.get("FEC_VCTO_RECIBO"), FechaUtil.DATE_DMY));
			recibo.setImpRecibo(new BigDecimal(data.get("IMP_RECIBO") + ""));
			recibo.setNumCuota(new BigDecimal(data.get("NUM_CUOTA") + ""));
			recibo.setNumRecibo(new BigDecimal(data.get("NUM_RECIBO") + ""));
			//recibo.setCodMon(new BigDecimal(data.get("COD_MON") + ""));
			listRecibo.add(recibo);
		}
		return listRecibo;
	}
	
	@Override
	public Map<String, Object> sessionEnable(String uuid) {
		Map<String, Object> resultadoMap = new HashMap<>();
		String pNRetorno = "";
		String cResult = "";
		try {
			List<Object> parametroInType = new ArrayList<>();
			List<Integer> parametroOutType = new ArrayList<>();
			parametroOutType.add(OracleTypes.VARCHAR);/* p_nRetorno */
			parametroOutType.add(OracleTypes.VARCHAR);/* p_cMensaje */

			Map<String, Object> parametrosType = new HashMap<>();

			String a = String.format("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_SESSION_ENABLE." + uuid +" --> %s", FechaUtil.obtenerFechaActual());
			log.error(a);

			ScriptSqlResulJDBCVO sqEjecutado = genericJDBC.executeQuerySP("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_SESSION_ENABLE", null, parametroOutType, parametroInType, parametrosType);
			if (!sqEjecutado.isTieneError() && !sqEjecutado.getListaData().isEmpty()) {
				if (!StringUtil.isNullOrEmpty(sqEjecutado.getListaData().get(0).get("resultado1"))) {
					pNRetorno = sqEjecutado.getListaData().get(0).get("resultado1") + "";
				}
				
				if (!StringUtil.isNullOrEmpty(sqEjecutado.getListaData().get(1).get("resultado2"))) {
					cResult = sqEjecutado.getListaData().get(1).get("resultado2") + "";
				}
			}

			resultadoMap.put("Retorno", pNRetorno);
			resultadoMap.put("cResult", cResult);
		} catch (Exception e) {
			log.error("TRON2000.PKG_SCTR_SO_GEST_ASEG_PER.P_SESSION_ENABLE." + uuid, e);
			resultadoMap.put("Retorno", "-1");
			resultadoMap.put("cResult", "Error: algo fue mal.");
		}
		return resultadoMap;
	}
}
