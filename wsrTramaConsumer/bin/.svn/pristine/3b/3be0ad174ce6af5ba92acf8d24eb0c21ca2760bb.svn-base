package pe.com.mapfre.serviciosctr.infraestructura.vertical.type;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum TipoInvocacionType {

	/** El SI. */
 	ENTRADA('E' , "respuesta.entrada"),
    /** El NO. */
  	SALIDA('S' , "respuesta.salida");
	
	/** La Constante LOO_KUP_MAP. */
	private static final Map<Character, TipoInvocacionType> LOO_KUP_MAP = new HashMap<>();
	
	static {
		for (TipoInvocacionType s : EnumSet.allOf(TipoInvocacionType.class)) {
			LOO_KUP_MAP.put(s.getKey(), s);
		}
	}

	/** El key. */
	private Character key;
	
	/** El value. */
	private String value;

	/**
	 * Instancia un nuevo respuesta natural type.
	 *
	 * @param key el key
	 * @param value el value
	 */
	private TipoInvocacionType(Character key, String value) {
		this.key = key;
		this.value = value;
	}
	
	/**
	 * Gets the.
	 *
	 * @param key el key
	 * @return the respuesta natural type
	 */
	public static TipoInvocacionType get(Character key) {
		return LOO_KUP_MAP.get(key);
	}

	/**
	 * Obtiene key.
	 *
	 * @return key
	 */
	public Character getKey() {
		return key;
	}

	/**
	 * Obtiene value.
	 *
	 * @return value
	 */
	public String getValue() {
		return value;
	}
}
