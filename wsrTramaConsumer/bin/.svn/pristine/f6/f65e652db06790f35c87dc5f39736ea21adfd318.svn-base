package pe.com.mapfre.serviciosctr.dominio.entidades.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import pe.com.mapfre.serviciosctr.infraestructura.vertical.util.ConfiguracionEntityManagerUtil;

/**
 * La Class DetalleLogOrquestadorVariable.
 * <ul>
 * <li>Copyright 2020 BuildSoft - Todos los derechos reservados.</li>
 * </ul>
 *
 * @author BuildSoft
 * @version 1.0, Mon Apr 27 10:28:20 COT 2020
 */
@Entity
@Table(name = "SCTR_SO_DET_LOG_ORQ_VAR", schema = ConfiguracionEntityManagerUtil.ESQUEMA_TRON2000)
public class DetalleLogOrquestadorVariable implements Serializable {
 
    /** La Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;
   
    /** El id det cotizacion. */
    @Id
    @Column(name = "ID_LOG_DET_VAR" , length = 32)
    private String idLogDetVariable;
   
    /** El id cotizacion. */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_LOG_DET", referencedColumnName = "ID_LOG_DET")
    private DetalleLogOrquestador detalleLogOrquestador;
   
    /** El grupo variable. */
    @Column(name = "GRUPO" , length = 200)
    private String grupo;
   
    /** El key grupo. */
    @Column(name = "KEY" , length = 200)
    private String key;
   
    /** El value grupo. */
    @Column(name = "VALUE" , length = 300)
    private String value;
   
    /**
     * Instancia un nuevo detalle cotizacion.
     */
    public DetalleLogOrquestadorVariable() {
		super();
    }
   
    /**
     * Instancia un nuevo detalle cotizacion.
     *
     * @param idLogDetVariable el id det cotizacion
     * @param idCotizacion el id cotizacion
     * @param grupoVariable el grupo variable
     * @param keyGrupo el key grupo
     * @param valueGrupo el value grupo
     */
    public DetalleLogOrquestadorVariable(String idLogDetVariable, DetalleLogOrquestador detalleLogOrquestador, String grupoVariable, String keyGrupo, String valueGrupo ) {
        super();
        this.idLogDetVariable = idLogDetVariable;
        this.detalleLogOrquestador = detalleLogOrquestador;
        this.grupo = grupoVariable;
        this.key = keyGrupo;
        this.value = valueGrupo;
    }
   
    //get y set
    /**
     * Obtiene id det cotizacion.
     *
     * @return id det cotizacion
     */
     public String getIdLogDetVariable() {
        return this.idLogDetVariable;
    }
    /**
     * Establece el id det cotizacion.
     *
     * @param idLogDetVariable el new id det cotizacion
     */
    public void setIdLogDetVariable(String idLogDetVariable) {
        this.idLogDetVariable = idLogDetVariable;
    }
    /**
     * Obtiene id cotizacion.
     *
     * @return id cotizacion
     */
     public DetalleLogOrquestador getDetalleLogOrquestador() {
        return this.detalleLogOrquestador;
    }
    /**
     * Establece el id cotizacion.
     *
     * @param idCotizacion el new id cotizacion
     */
    public void setDetalleLogOrquestador(DetalleLogOrquestador detalleLogOrquestador) {
        this.detalleLogOrquestador = detalleLogOrquestador;
    }
    /**
     * Obtiene grupo variable.
     *
     * @return grupo variable
     */
     public String getGrupo() {
        return this.grupo;
    }
    /**
     * Establece el grupo variable.
     *
     * @param grupoVariable el new grupo variable
     */
    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }
    /**
     * Obtiene key grupo.
     *
     * @return key grupo
     */
     public String getKey() {
        return this.key;
    }
    /**
     * Establece el key grupo.
     *
     * @param keyGrupo el new key grupo
     */
    public void setKey(String key) {
        this.key = key;
    }
    /**
     * Obtiene value grupo.
     *
     * @return value grupo
     */
     public String getValue() {
        return this.value;
    }
    /**
     * Establece el value grupo.
     *
     * @param valueGrupo el new value grupo
     */
    public void setValue(String value) {
        this.value = value;
    }
    /* (non-Javadoc)
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((idLogDetVariable == null) ? 0 : idLogDetVariable.hashCode());
        return result;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DetalleLogOrquestadorVariable other = (DetalleLogOrquestadorVariable) obj;
        if (idLogDetVariable == null) {
            if (other.idLogDetVariable != null) {
                return false;
            }
        } else if (!idLogDetVariable.equals(other.idLogDetVariable)) {
            return false;
        }
        return true;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "DetalleLogOrquestadorVariable [idLogDetVariable=" + idLogDetVariable + "]";
    }
   
}